<?php 
ini_set('display_errors','on');
error_reporting(E_ALL);

define('BASEPATH','');
//include_once '../system/application/libraries/LogUtil.php';
include_once '../system/application/config/database.php';


define('STORAGE_IN_PATH','/storage2/ENGINES/IN/');
define('STORAGE_OUT_PATH','/storage2/ENGINES/OUT/');

$cdev = mysql_connect($db[$active_group]['hostname'],$db[$active_group]['username'],$db[$active_group]['password']) or die('nao conectou');

mysql_select_db($db[$active_group]['database'],$cdev);

$query = 'SELECT * FROM TB_EXCEL_OPI';
$queryUpdate = 'UPDATE TB_EXCEL_OPI SET PROCESSADO_EXCEL_OPI = %d, DATA_TERMINO_EXCEL_OPI = "%s" WHERE ID_EXCEL_OPI = %d';
$queryDelete = 'DELETE FROM TB_EXCEL_OPI WHERE ID_EXCEL_OPI = %d';

$rs = mysql_query($query,$cdev);
echo '<pre>------------------ CRON - VERIFICA ARQUIVO OPI ------------------',PHP_EOL;

while ($row = mysql_fetch_assoc($rs)) {
	$path = STORAGE_OUT_PATH.$row['ID_EXCEL_OPI'].'_.pdf.pdf';
	echo 'Verificando: ',$path,PHP_EOL;
	
	//touch($path);
	
	if(file_exists($path)){
		$diff = time() - filemtime($path);
		
		$inUse = file_exists(STORAGE_OUT_PATH.$row['ID_EXCEL_OPI'].'_.pdf-log.txt') 
				&& ( file_exists($path));//shell_exec("fuser -f -c ".escapeshellarg($path));
		
//		echo "fuser -f -c ".escapeshellarg($path).PHP_EOL;
		echo $inUse.PHP_EOL;
		
		if(empty($inUse)){
			$periodo = 3600*48; // 2 dias
			if($diff > $periodo && is_writable($path)){
				echo 'Encontrou acima do tempo: ',$row['ARQUIVO_EXCEL_OPI'],PHP_EOL;
				//chmod($path,0777);
				if(unlink($path)){
					$q = sprintf($queryDelete,$row['ID_EXCEL_OPI']);
					echo 'Executando:', $q, PHP_EOL;
					mysql_query($q,$cdev);
				}
			}else{
				if($row['PROCESSADO_EXCEL_OPI'] != 1){
					echo 'Encontrou no tempo: ',$row['ARQUIVO_EXCEL_OPI'],' atualizando como processado.',PHP_EOL;
					$q = sprintf($queryUpdate,1,date('Y-m-d H:i:s'),$row['ID_EXCEL_OPI']);
					echo 'Executando:' , $q, PHP_EOL;
					mysql_query($q,$cdev);
				}
			}
		}else{
			echo 'Encontrou no tempo: ',$row['ARQUIVO_EXCEL_OPI'],' atualizando como NAO processado.',PHP_EOL;
			$q = sprintf($queryUpdate,0,$row['ID_EXCEL_OPI']);
			echo 'Executando:' , $q, PHP_EOL;
			mysql_query($q,$cdev);
		}
	}else{
		echo 'Arquivo ', $path,' não encontrado.' , PHP_EOL;
		if($row['PROCESSADO_EXCEL_OPI'] == 1){
			$q = sprintf($queryDelete,$row['ID_EXCEL_OPI']);
			echo 'Executando:' , $q, PHP_EOL;
			mysql_query($q,$cdev);
		}
	}
	
	echo PHP_EOL;
}

