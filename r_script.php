<?php

//  sampleclient_php_nusoap.php
//
// ADOBE SYSTEMS INCORPORATED
//   Copyright 2007 Adobe Systems Incorporated
//   All Rights Reserved.
//
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance with the
//  terms of the Adobe license agreement accompanying it.  If you have received this file from a
//  source other than Adobe, then your use, modification, or distribution of it requires the prior
//  written permission of Adobe.
//


	// Pull in the NuSOAP code - note that php.ini defines 'include_path'
	// *** NOTE:  To avoid a naming conflict with PHP:SOAP's soapclient, I have edited my
	//			  nusoap.php file to use the classname "nusoapclient" instead of "soapclient"
	//			  This is only required when Apache Server is setup to use PHP_soap extension.
	require_once('lib/nusoap.php');


	// NuSoap's 'call' method accepts either an XML-formatted string or a php array for the parameters
	$useXMLstringAsParam = false;


	// ---------------------------------------------------------------------------------------------------------
	// phpRunScript
	//
	// wsdl - the URI of the IDSP.wsdl file.  (e.g.  "http://localhost/IDSP/IDSP.wsdl")
	// host - the URI of the targeted instance of InDesign Server.  If not specified, the host URI is defined
	//			by the "SOAP:address location=" section of the wsdl.
	// scriptFile - the path to the script to execute.  The path is based on the file system
	//				of the targeted InDesign Server instance.
	// scriptLanguage - the language used to program the script:  "javascript", "visual basic", or "applescript"
	// scriptArgs - an array containing the arguments to the script.  Example:
	//					$myArgs = array(array('name' => "arg0", 'value' => 100),
	// 									array('name' => 'arg1', 'value' => 'This is some text.'));
	// printDebugInfo - true to write the result, request, response, and/or exception to the output
	// ---------------------------------------------------------------------------------------------------------
	function phpRunScript($wsdl, $host, $scriptFile, $scriptText, $scriptLanguage, $scriptArgs, $printDebugInfo=false)
	{
		global $useXMLstringAsParam;

		$result = '';

		// WSDL
		// create a wsdl object using the specified wsdl file
		$idsp_wsdl = new wsdl($wsdl);

		if (!$idsp_wsdl->getError())
		{
			// create the client using the wsdl object (note the use of modified class name 'nusoapclient')
			$client = new nusoap_client($idsp_wsdl, true);

			// Alexandre de Silva Cunha
			// alemac@mac.com
			// 2009/03/04 - 14:00 - v1
			$client->timeout=600; //10minutos
			$client->response_timeout=600; //10minutos
			//

			if (!$client->getError())
			{
				// set the encoding to the proper setting
				$client->soap_defencoding = "ISO-8859-1";

				// target a specific instance of InDesign Server if specified - uses wsdl host otherwise
				if ($host != '') {
					$client->setEndpoint($host);
				}

				// get the namespace of the RunScript method from the wsdl
				$targetNamespace = $idsp_wsdl->currentSchema->schemaInfo['targetNamespace'];

				// PASS XML STRING to RunScript
				if ($useXMLstringAsParam)
				{
					// Call RunScript with params formatted as an XML string defining the RunScript XML packet...
					// We define the RunScript portion of the packet, NuSoap defines the rest.
					$scriptArgsXML = '';
					for ($i = 0; $i < sizeof($scriptArgs); $i++) {
						$scriptArgsXML = $scriptArgsXML .
									'<scriptArgs>' .
										'<name>' . $scriptArgs[$i]['name'] . '</name>' .
										'<value>' . $scriptArgs[$i]['value'] . '</value>' .
									'</scriptArgs>';
					}
					$runScriptXML = '<IDSP:RunScript>' .
									'<runScriptParameters>' .
									'<scriptText>' . $scriptText . '</scriptText>' .
									'<scriptLanguage>' . $scriptLanguage . '</scriptLanguage>' .
									'<scriptFile>'. $scriptFile . '</scriptFile>' .
									$scriptArgsXML .
									'</runScriptParameters>' .
									'</RunScript>';

					// CALL RunScript
					$result = $client->call("RunScript", $runScriptXML, $targetNamespace);
				}

				// PASS PHP OBJECTS to RunScript
				else
				{
					// Call RunScript with params formatted as a php array object.  NuSoap serializes the array
					// and builds the packet for us...

					// *** NOTE:  InDesign Server will only recognize the RunScript SOAP call if the proper namespace is
					//   		  specified as a prefix to the method name, or as the xmlns attribute of the RunScript element:
					//					<IDSP:RunScript>  or  <RunScript xmlns="http://ns.adobe.com/InDesign/soap/">
					// 			  When NuSoap (0.7.2) serializes a php structure, it only adds the "xmlns" attribute to an element
					//    		  if the schema in the wsdl defines the element's "form" attribute as "qualified".  This poses a
					//    		  problem when making a call to RunScript because IDSP.wsdl defines RunScript's form attribute as
					//	  		  "unqualified".  To get around this problem, either:
					//					* modify the wsdl object's RunScript form attribute to "qualified" as below, or
					// 					* modify IDSP.wsdl, setting the "http://ns.adobe.com/InDesign/soap/" schema's
					//    				  "elementFormDefault" to "qualified" rather than "unqualified".
					// 			  Note that this is only required when using a php array for the params.
					$idsp_wsdl->schemas[$targetNamespace][0]->elements['RunScript']['form'] = 'qualified';

					// create the parameter arrays
					$scriptParams = array(	'scriptText'		=> $scriptText,
											'scriptLanguage'	=> $scriptLanguage,
											'scriptFile'		=> $scriptFile,
											'scriptArgs'		=> $scriptArgs);
					$runScriptParams = array ('runScriptParameters' => $scriptParams);

					$params = array('parameters' => $runScriptParams);

					// CALL RunScript
					$result = $client->call('RunScript', $params, $targetNamespace);
				}
			}

			// Handle the results
			if ($client->fault) {
				$result = $client->fault;
				if ($printDebugInfo) {
					echo '<h2>---------Fault---------</h2><pre>';
					print_r($client->fault);
					echo '</pre>';
				}
			} else {
				if ($client->getError()) {
					$result = $client->getError();
				}
				// output Debugging infomation:  Result (error), Request, Response, and NuSoap Debug info
				if ($printDebugInfo) {
					echo '<h2>----------Result---------</h2><pre>';
					print_r($result);
					echo '</pre>';
					echo '<h2>---------Request---------</h2><pre>';
					echo '<pre>' . $client->request. '</pre></pre>';
					echo '<h2>---------Response--------</h2><pre>';
					echo '<pre>' . $client->response . '</pre></pre>';
					echo '<h2>---------Debug--------</h2><pre>';
					echo '<pre>' . $client->getDebug() . '</pre></pre>';
				}
			}

			// release the client
			unset($client);
		}
		else
		{
			$result = $idsp_wsdl->getError();
			if ($printDebugInfo) {
				echo '<h2>---------Error---------</h2><pre>' . $result . '</pre>';
			}
		}

		return $result;
	}
?>