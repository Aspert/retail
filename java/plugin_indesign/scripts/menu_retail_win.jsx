#targetengine "session"

//DECLARA AS VARIAVEIS QUE SERAO USADAS NO SCRIPT
var file, folder;
var jobinfo, idJob, idPraca, idExcel, praca, versao, servidor, pluginPath, tipo, linkPath, linkData, vbScript, boo;
var fileName, filePath, stringXml, path1, path2;
var objXmlRootExterno, objXmlFichasExternas;
var pluginPath = "c:\\247id\\";
var _pluginPath = "c:\\\\247id\\\\";

// FUNCAO REPLACEALL IGUAL DO JAVA
function replaceAll(str, de, para){
    var pos = str.indexOf(de);
    while (pos > -1){
		str = str.replace(de, para);
		pos = str.indexOf(de);
	}
    return (str);
}

// FUNCAO PARA PEGAR O NOME DO ARQUIVO EM UM ENDERECO (PATH)
function getNomeArquivo(path){
	if(path.indexOf("\/") > -1){
		var arr = path.split("\/");
		if(arr.length > 0){
			return arr[arr.length-1];
		}
	}
	return null;
}

// FUNCAO PARA RETORNAR O NOME DO ARQUIVO EU UM ENDERECO (PATH)
function getPathArquivo(path){
	if(path.indexOf("\/") > -1){
		var arr = path.split("\/");
		if(arr.length > 0){
			var retorno = "";
			for(var i = 0; i <= arr.length - 2; i++){
				retorno = retorno + "/" + arr[i];
			}
			return retorno.replace("//", "/");
		}
	}
	return null;
}

// FUNCAO PARA IMPORTAR UM XML
function importaXml(caminho){
	var xml = new File(caminho);
	xml.open ("r","0777","0777");
	var xmlConteudo= "";
	var xmlLinha = "";

	while(xmlLinha = xml.readln()){
		xmlConteudo += xmlLinha;
	}

	xml.close();

	var objXml = new XML(xmlConteudo);

	return objXml;
}

// FUNCAO PARA DELETAR UM DIRETORIO INTEIRO
function deletaDiretorio(folder){
	if(folder.exists){
		for(var i = folder.getFiles("*").length - 1; i >= 0; i--){
			 folder.getFiles("*")[i].remove();
		}
		return folder.remove();
	}
	else{
		return true;
	}
}

// FUNCAO PARA REMOVER ARQUIVOS REPETIDOS DE UMA ARRAY
function removeRepetidosArray(arr){
	var arrRetorno = new Array();
	arr.sort();
	for(var i = arr.length - 1; i >= 0; i--){
		try{
			if(arr[i] != arr[i-1]){
				arrRetorno.push(arr[i]);
			}
		}
		catch(e){}
	}
	return arrRetorno.sort();
}


// REMOVE EXTENSAO
function removeExtensao(fileName){
	var reg = /(.*?)\.([a-z,A-Z,0-9,_,-]+)$/ig;
	reg.exec(fileName);
	return RegExp.$1;
}

//FUNCAO QUE VERIFICA SE O NUMERO DE PAGINAS DO TEMPLATE É O MESMO NUMERO DE PAGINAS DO INDESIGN
function verificaCompatibilidadePaginas(objDocumento, objXml){
	var objXmlFichas = objXml.xpath("//ficha");
	var count = objXmlFichas.length()-1;
	
	do{
		var objXmlUltimaFicha = objXmlFichas[count];
		count--;
	}
	while(isNaN(objXmlUltimaFicha.attribute("pagina")))
	
	if(objXmlUltimaFicha.attribute("pagina").toString() > objDocumento.pages.length.toString()){
		return false;
	}

	return true;
}
//--------------------------------------------------------------------------------------------------------------------------------------------

// AO CLICAR NO MENU, EXECUTA TAREFAS ABAIXO
function openApp() {
	var arrLinksInternos = new Array();
	var arrLinksExternos = new Array();
	var arrLinksBaixar = new Array();
	var arr = new Array();

	if(app.documents.length > 0){
		folder = new Folder(pluginPath);
		folder.create();

		if(folder.exists){
			try{jobinfo = replaceAll(app.activeDocument.xmlElements.item(0).xmlElements.item("jobinfo").markupTag.name, " ", "_");}
			catch(e){jobinfo = ""}
			try{idJob = app.activeDocument.xmlElements.item(0).xmlElements.item("jobinfo").xmlAttributes.item("id_job").value;}
			catch(e){idJob = ""}
			try{idPraca = app.activeDocument.xmlElements.item(0).xmlElements.item("jobinfo").xmlAttributes.item("id_praca").value;}
			catch(e){idPraca = ""}
			try{idExcel = app.activeDocument.xmlElements.item(0).xmlElements.item("jobinfo").xmlAttributes.item("id_excel").value;}
			catch(e){idExcel = ""}
			try{servidor = app.activeDocument.xmlElements.item(0).xmlElements.item("jobinfo").xmlAttributes.item("servidor").value;}
			catch(e){servidor = ""}

			if( (jobinfo != "") && (idJob != "") && (idPraca != "") && (idExcel != "") && (servidor != "") ){
				folder = new Folder(pluginPath + "\\" + idJob + "_" + idPraca);
				deletaDiretorio(  folder ) ;
				folder.create();
				
				if(folder.exists){
					folder = new Folder(pluginPath + "\\" + idJob + "_" + idPraca + "\\img");
					deletaDiretorio( folder ) ;
					folder.create();
					
					if(folder.exists){
						// VARRE ELEMENTOS DENTRO DO XML DO INDESIGN
						for(var i = 0; i <= app.activeDocument.xmlElements.item(0).xmlElements.length - 1; i++){
							// SE NO FOR DO TIPO FICHA
							if(app.activeDocument.xmlElements.item(0).xmlElements.item(i).markupTag.name == "ficha"){
								
								// VARRE ELEMENTOS DO OBJETO FICHA
								for(var w = 0; w <= app.activeDocument.xmlElements.item(0).xmlElements.item(i).xmlElements.length - 1; w++){
									tipo = app.activeDocument.xmlElements.item(0).xmlElements.item(i).xmlElements.item(w).xmlContent.constructor.name;
									tipo = tipo.toUpperCase();
									
									// VERIFICA SE TIPO � EPS, IMAGEM OU RETANGULO
									if( ( tipo == "EPS" ) || (tipo == "IMAGE") || (tipo == "RECTANGLE") ){
										//RETORNA PATH DO ARQUIVO
										linkPath = decodeURIComponent(app.activeDocument.xmlElements.item(0).xmlElements.item(i).xmlElements.item(w).xmlAttributes.item("href").value);
										linkPath = linkPath.replace("file:///Volumes", "");
										
										try{
											//RETORNA ATRIBUDO DE DATA
											var linkData = app.activeDocument.xmlElements.item(0).xmlElements.item(i).xmlElements.item(w).xmlAttributes.item("data_alteracao").value;
										} catch(e){
											 linkData = new Date().getTime();
										}
										// ADICIONA NA ARRAY DE LINKS INTERNOS CASO O PATH SEJA DO BANCO DE TEMPLATES
										var _linkPath = linkPath.toUpperCase();
										if(  _linkPath.indexOf("BANCO DE TEMPLATES/TEMPLATE_") == -1  ){
											arrLinksInternos.push(linkPath + "###" + linkData);
										}
									}
								}
							}
						}
						// REMOVE ELEMENTOS REPETIDOS
						arrLinksInternos = removeRepetidosArray(arrLinksInternos);
					
						// RETORNA O XML, SCRIPT DE UPDATE VIA JAVA
						var batText   = "@echo off\n";
							batText += "echo Atualizando XML\n";
							batText += "@java -jar " + pluginPath + "\\plugin.jar  \"" +  _pluginPath + "\" \"" + idJob + "\" \"" + idPraca + "\" \"" +idExcel + "\"";
						var batFile = new File( pluginPath + idJob + "_" + idPraca + "\\xml.bat");
						batFile.open("w");
						batFile.write(batText);
						batFile.close();
						batFile.execute();

						// EXPORTA XML DO INDESIGN PARA O PATH LOCAL
						var file = new File(pluginPath + "\\" + idJob + "_" + idPraca + "\\1.xml");
						app.activeDocument.exportFile(ExportFormat.xml,  file);

						// AGUARDA FINALIZACAO DAS TAREFAS DO JAVA. CASO O ARQUIVO local.pid SEJA CRIADO, FORAM FINALIZADA AS TAREFAS
						do{
							var pidFile = new File( pluginPath + "\\" + idJob + "_" + idPraca + "\\local.pid"  );
							var errFile = new File(  pluginPath + "\\" + idJob + "_" + idPraca + "\\local.xml.err"  );
							var errControle = new File(  pluginPath + "\\controle.err"  );

							if( errFile.exists ){
								alert("Nao foi possivel importar o XML externo" );
								return;
							}
							errFile.close();

							if( errControle.exists ){
								alert("Nao foi possivel setar ambiente" );
								return;
							}
							errControle.close();
						} while(  !pidFile.exists  ) ;

						batFile.remove();
						
						// IMPORTA XML EXTERNO VINDO DO JAVA
						try{
							objXmlRootExterno = importaXml( pluginPath + "\\" + idJob + "_" + idPraca + "\\local.xml" );

							if( objXmlRootExterno.xpath("//jobinfo")  == "" ){
								alert("XML invalido!!!");
								return;
							}
						}
						catch(e){
							alert("XML invalido!!!");
							return;
						}
						
						if(!verificaCompatibilidadePaginas(app.activeDocument, objXmlRootExterno)){
							alert("XML contem mais paginas doque o Documento InDesign");
							return false;
						}
						
						// ABRE COMO ARQUIVO XML VINDO DO JAVA EM MODO LEITURA
						file = new File(pluginPath + "\\" + idJob + "_" + idPraca + "\\local.xml");
						file.open("r");
						stringXml = file.read();
						file.close();

						// RETORNA OBJETOS NAO QUAL O ATRIBUTO ACAO SEJA VAZIO
						objXmlFichasExternas = objXmlRootExterno.xpath("//ficha[@acao='']");

						// VARRE OBJETO COM FICHAS VAZIAS
						for(var i = 0; i <= objXmlFichasExternas.length() - 1; i++){
							// VARRE ELEMENTOS DO OBJETO
							for(var y = 0; y <= objXmlFichasExternas[i].elements().length() - 1; y++){
								//RETORNA PATH
								linkPath = objXmlFichasExternas[i].elements()[y].attribute("link").toString();
								linkData = objXmlFichasExternas[i].elements()[y].attribute("data_alteracao").toString();
								// CASO O ARQUIVO TENHA PATH CORRETO
								if(linkPath != ""){
									// VERIFICA SE � DIFERENTE DE BANCO DE TEMPLATES E ADICIONA NA ARRAY DE L INKS EXTERNOS
									var _linkPath = linkPath.toUpperCase();
									if(_linkPath.indexOf("BANCO DE TEMPLATES/TEMPLATE_") == -1){
										arrLinksExternos.push(linkPath + "###" + linkData);
									}
								}
							}
						}
						// REMOTE REPETIDOS
						arrLinksExternos = removeRepetidosArray(arrLinksExternos);

						// FAZ COMPARACAO ENTRE LINK INTERNO E EXTERNO. CASO O OBJETO EXISTA NO LINK INTERNO E N�O NO EXTERNO, BAIXA VIA JAVA
						var arquivosDownload = pluginPath + idJob + "_" + idPraca + "\\arquivos.txt" 
						var fileArquivos = new File( arquivosDownload );
						fileArquivos.open("w");
						
						for( var i = 0; i <= arrLinksExternos.length - 1; i++ ){
								var arr = arrLinksExternos[i].split("###");
								if( arr.length == 2 ){
									linkPath = arr[0];
									linkData = arr[1];
									
									//RETORNA A PATH DO ARQUIVO E REMOVE FPO
									filePath = getPathArquivo(linkPath);
									filePath = filePath.replace("_FPO", "");
									
									//RETORNA NOME DE ARQUIVO
									fileName = getNomeArquivo(linkPath);
									
									// REMOVE EXTENSAO DO ARQUIVO
									fileNameEps = removeExtensao( fileName ) + ".eps";
									fileNameErr = removeExtensao( fileName ) + ".err";
									
									var fileError = new File( pluginPath + "\\" + idJob + "_" + idPraca + "\\img\\" + fileNameErr );
									fileError.remove();
									
									fileArquivos.writeln(  filePath + "/" + fileName );
								}
						}

						var fileBat = new File( pluginPath + idJob + "_" + idPraca + "\\arquivos.bat" );
						fileBat.open("w");
						fileBat.writeln(  "@java -jar " + pluginPath + "\\plugin.jar  \""+_pluginPath+"\" \"" + arquivosDownload+ "\" \"" +idJob + "\" \"" +idPraca + "\" \"" + idExcel + "\" \"true\" \n" );
						fileBat.close();
						fileArquivos.close();

						if( fileBat.exists ){
							fileBat.execute();
						} else{
							alert("Nao foi possivel efetuar downloads");
							return;
						}
						for(var i = 0; i <= arrLinksExternos.length - 1; i++){
							//CONTROLE DE COMPARACAO
							var boo = false;
							
							// ARRAY DE LINK INTERNO FAZENDO COMPARACAO COM EXTERNO
							for(var y = 0; y <= arrLinksInternos.length - 1; y++){
								if(arrLinksExternos[i] == arrLinksInternos[y]){
									boo = true;
									break;
								}
							}
							
							// CASO NAO EXISTA NO INTERNO
							//if( boo == false ){
								// ARRAY DE LINKS INTERNOS
								var arr = arrLinksExternos[i].split("###");
								
								// CASO O NOME DO ARQUIVO TENHA ### E DATA
								if( arr.length == 2 ){
									linkPath = arr[0];
									linkData = arr[1];
									
									fileName = getNomeArquivo(linkPath);
									fileNameEps = removeExtensao( fileName ) + ".eps";
									fileNameErr = removeExtensao( fileName ) + ".err";
									
									do{
											var file = new File( pluginPath + "\\" + idJob + "_" + idPraca + "\\img\\" + fileNameEps );
											var fileError = new File( pluginPath + "\\" + idJob + "_" + idPraca + "\\img\\" + fileNameErr );
											var fileErrorTotal = new File( pluginPath + "\\" + idJob + "_" + idPraca + "\\erro_total.err" );

											if( fileError.exists ){
												alert( "Arquivo: " +  fileNameEps + " nao foi baixado" );
												return;
											}
											if( fileErrorTotal.exists ){
												alert( "Nao foi possivel baixar arquivos");
												return;
											}
									} while( !file.exists );

									if( file.exists ){
										// REPLACE DE ARQUIVO EXTERNO PARA INTERNO
										var path1 = replaceAll( getPathArquivo(linkPath) + "\\" + getNomeArquivo(linkPath), "\\", "/" );
										var path2 = replaceAll( pluginPath + "\\" + idJob + "_" + idPraca + "\\img\\" + fileNameEps, "\\", "/" );
										var stringXml = replaceAll(stringXml, path1, path2);
									} else{
										alert( "Arquivo: " + pluginPath + "\\" + idJob + "_" + idPraca + "\\img\\" + fileNameEps + " nao existe" );
										return ;
									}
								}
							//}
						}
						fileBat.close();
						fileBat.remove();
						fileArquivos.close();
						fileArquivos.remove();

						// CRIA XML FINAL PARA O SCRIPT update.jsx FAZER AS MUDAN�AS ( gorgonza )
						var fileXml = new File( pluginPath + "\\" + idJob + "_" + idPraca + "\\2_.xml" );
							fileXml.open("w", "0777","0777");

						try{
							for( wz = 0; wz < stringXml.length; wz++){
								fileXml.write(stringXml[wz]);
							}
						}catch(e){
							alert("Nao foi possivel salvar XML local com as alteracoes");
							return;
						}
						fileXml.close();
						
						// CHAMA update.jsx
						try{
							xml_2 = pluginPath + "\\" + idJob + "_" + idPraca + "\\2_.xml";
							#include "c:\\247id\\jsx\\update_java.jsx";
						}catch( e ){
							alert( "Nao foi possivel carregar informacoes essenciais: " + e.description );
							eliminaBarraProgresso();
							return;
						}
					}
					else{
						alert("Diretorio nao existe '" + pluginPath + idJob + "_" + idPraca + "\\img'!");
					}
				}
				else{
					alert("Diretorio nao existe '" + pluginPath + "\\job\\" + idJob + "_" + idPraca + "'!");
				}
			}
			else{
				alert("Informacoes corrompidas na tag 'jobinfo'!");
			}
		}
		else{
			alert("Diretorio '" + pluginPath + "' nao encontrado!");
		}
	}
	else{
		alert("Nenhum documento aberto");
	}
}

try{
	app.scriptMenuActions.item("Update Retail").remove();
}
catch(e){}

var menuActionUpdateRetail = app.scriptMenuActions.add("Update Retail");
var eventListenerUpdateRetail = menuActionUpdateRetail.eventListeners.add("onInvoke", openApp, false);

try{
	var scriptMenu247 = app.menus.item("$ID/Main").submenus.item("24\\7 ID");
	scriptMenu247.title;
}
catch (myError){
	var scriptMenu247 = app.menus.item("$ID/Main").submenus.add("24\\7 ID");
}

scriptMenu247.menuItems.add(menuActionUpdateRetail);