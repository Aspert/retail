<?php

function trace($data)
{
	echo '<pre style="padding:10px; border: 1px solid #F00; color:#f00; background: #ccc;">';
	print_r($data);
	echo '</pre>';
}

function getJson()
{
	if ( isset($_REQUEST['json']) )
		return json_decode(stripslashes($_REQUEST['json']));
	else
		return null;
} 

function cript($str){
	return strtr(base64_encode($str), '/+=', '-~_');
}

function getCorTempoJob($tempoRestante){
	$MINUTE = 60;
	$HOUR = $MINUTE*60;
	$DAY = $HOUR*24;

	if($tempoRestante > $DAY){
		return CoresTempo::$cores['COR_ACIMA_24HORAS'];
	}else if($tempoRestante <= $DAY && $tempoRestante > 0){
		return CoresTempo::$cores['COR_ABAIXO_24HORAS'];
	}else if($tempoRestante < 0){
		return CoresTempo::$cores['COR_ATRASADO'];
	}
}

function valor_to_save($str)
{
 	if( !is_numeric($str) ){
 		return str_replace(array('.', ','), array('', '.'), $str);
 	} else {
 		return number_format($str, 2, '.', '');
 	}
}

function setJson($data)
{
	exit(json_encode($data));
}

function readonly($set = true){
	return $set ? ' readonly="readonly" ' : '';
}

function disabled($set = true){
	return $set ? ' disabled="disabled" ' : '';
}

function str_xinet($str, $decode=false){
	$str = str_replace(':', '%', str_replace_xinet($str) );

	if( $decode==true ){
		$str = urldecode( $str  );
	}

	return iconv('macintosh', 'UTF-8', ( ( $str ) ) ) ;
}

function str_to_xinet($str, $decode=false){
	$str = str_replace(':', '%', str_replace_xinet($str) );

	if( $decode==true ){
		$str = urldecode( $str  );
	}

	return iconv('UTF-8', 'macintosh//IGNORE', ( ( $str ) ) ) ;
}

function str_replace_xinet($string){
	$DE = array( chr(127) );
	$PARA = array( ' ' );
	return str_replace( $DE,$PARA,$string );
}

function pre($value){

	    echo '<pre style="border: 1px black solid; padding: 0px; margin: 10px;">';

		ob_start();
		print_r($value);
		$str = ob_get_contents();
		ob_clean();

		$str = str_replace('<',        '&lt;', $str);
		$str = str_replace('[',        '<span style="color: red; font-weight: bold;">[', $str);
		$str = str_replace(']',        ']</span>', $str);
		$str = str_replace('=>',    '<span style="color: blue; font-weight: bold;">=></span>', $str);
		$str = str_replace('Array', '<span style="color: purple; font-weight: bold;">Array</span>', $str);
		echo $str;
		echo '</pre>';

		exit();
}

# Assim ja manda direto pro select do code
#$this->data['lojas'] = arraytoselect($this->loja->getAll(null),'loj_id','loj_nome');

function arraytoselect($rows, $value, $label, $first=null)
{
 	$retorno = null;
 	/**
 	 * Modificacao efetuada pois o metodo getAll ja efetua o result_array();
 	 */
 	if ( $first != null ){
 		$retorno[''] = $first;
 	}
 	foreach ( $rows as $row){
 		$retorno[$row[$value]] = $row[$label];
 	}
 	return $retorno;
}
function verifica($variavel)
{
	return (isset($variavel)&&$variavel!=''&&$variavel!=null&&strlen($variavel)>0&&$variavel>0);
}
 function clear_str($StrAux)
 {
   	$StrAux      	= trim(str_replace("'","",$StrAux));
	$StrAux 		= str_replace("'","''",$StrAux);
	$StrAux 		= str_replace("#","''",$StrAux);
	$StrAux 		= str_replace("$","''",$StrAux);
	$StrAux 		= str_replace("%","''",$StrAux);
	$StrAux 		= str_replace("�","''",$StrAux);
	$StrAux 		= str_replace("&","''",$StrAux);
	$StrAux 		= str_replace("'or'1'='1'","''",$StrAux);
	$StrAux 		= str_replace("--","''",$StrAux);
	$StrAux 		= str_replace("insert","''",$StrAux);
	$StrAux 		= str_replace("drop","''",$StrAux);
	$StrAux 		= str_replace("delet","''",$StrAux);
	$StrAux 		= str_replace("xp_","''",$StrAux);
	$StrAux 		= str_replace("select","''",$StrAux);
	$StrAux 		= str_replace("*","''",$StrAux);
	$StrAux 		= str_replace("'","''",$StrAux);
	$StrAux 		= str_replace("#","''",$StrAux);
	$StrAux 		= str_replace("$","''",$StrAux);
	$StrAux 		= str_replace("%","''",$StrAux);
	$StrAux 		= str_replace("�","''",$StrAux);
	$StrAux 		= str_replace("&","''",$StrAux);
	$StrAux 		= str_replace("'or'1'='1'","''",$StrAux);
	$StrAux 		= str_replace("--","''",$StrAux);
	$StrAux 		= str_replace("insert","''",$StrAux);
	$StrAux 		= str_replace("drop","''",$StrAux);
	$StrAux 		= str_replace("delet","''",$StrAux);
	$StrAux 		= str_replace("xp_","''",$StrAux);
	$StrAux 		= str_replace("select","''",$StrAux);
	$StrAux 		= str_replace("*","''",$StrAux);

	return $StrAux;
 }
function format_date_to_db($str,$tipo="")
{
 	if ( !empty($str) && $str != '' && $str != "--" )
 	{
 		clear_str($str);

 		if(strpos($str,"/"))
 		{
	 		$x = explode( "/",$str);
	 		$dia = $x[0];
	 		$mes = $x[1];
	 		$ano = $x[2];

	 		if(!in_array($dia,inteiros()))
	 		{
	 			return null;
	 		}

	 		if(!in_array($mes,inteiros()))
	 		{
	 			return null;
	 		}

	 		if($tipo==2){return $ano."-".$mes."-".$dia." 00:00:00";	}
	 		if($tipo==3){return $ano."-".$mes."-".$dia." 23:59:59";	}
 			if($tipo==4){return $ano."-".$mes."-".$dia;	}
	 		return $ano."-".$mes."-".$dia." ".date("H:i:s");
 		}
 	}
 	else{
 		return null;
 	}
}

function getCurl($url, $argumento){
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $argumento);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION , 1);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$out = curl_exec($ch);
	curl_close($ch);
	return $out;
}

function format_date_to_form($str,$saida="d/m/Y")
{
 	if(!empty($str)and $str!="0000-00-00 00:00:00")
 	{
 		/*$dt = explode(" ",$str);
 		$dt = $dt[0];

 		$dt = explode("-",$dt);

 		$data = $dt[2]."/".$dt[1]."/".$dt[0];*/

 		return date($saida, strtotime($str));

 	}else{
 		if(!empty($saida)){
 			return;
 		}else{
 			return "--";
 		}

 	}
}
function do_paginacao($url, $totalPagina, $paginaAtual, $nIntervalo=20)
{
	if ( $totalPagina <= 1 ) return "";

	if ( $totalPagina < $nIntervalo)
	{
		$inicio = 0;
		$final = $totalPagina;
	}
	else
	{
		$inicio = $paginaAtual - (floor($nIntervalo/2));
		$final = $inicio + $nIntervalo;

		if ( $inicio < 0 )
		{
			$inicio = 0;
			$final = $nIntervalo;
		}
		if ( $final > $totalPagina )
		{
			$final = $totalPagina;
			$inicio = $totalPagina-$nIntervalo;
		}
	}

	$saida = '';

	$saida.= '<table border="0" style="width:600px;"><tr><td align="right" width="130">';

	if ( $paginaAtual != 0 )
		$saida.= anchor($url . '/0', 'primeiro') . ' | ' . anchor($url . '/' . ($paginaAtual-1), 'anterior');
	else
		$saida.= 'primeiro | anterior';

	$saida.= '</td><td align="center">';
	for ( $i = $inicio; $i < $final; $i++ )
		$saida.= ($paginaAtual==$i) ?"<b>" . ($i+1) . "</b> " :anchor($url . '/' . $i, ($i+1)) . " ";
	$saida.= '</td><td align="left" width="130">';

	if ( $paginaAtual != $totalPagina-1 )
		$saida.= anchor($url . '/' . ($paginaAtual+1), 'pr&oacute;ximo') . ' | ' . anchor($url . '/' . ($totalPagina-1), '&uacute;ltimo');
	else
		$saida.= 'pr&oacute;ximo | &uacute;ltimo';

	$saida.= '</td></tr></table>';

	return $saida;
}

function linkpagination($data, $limit = 20, $seg = 3, $urlExtra = ''){

	$CI =& get_instance();

	$CI->load->library('pagination');

	$config['base_url'] = base_url().index_page().'/'.$CI->uri->segment(1).'/'.$CI->uri->segment(2).$urlExtra.'/';
	$config['total_rows'] = $data['total'];
	$config['num_links'] = 5;
	if($limit == 0){
		$limit = 5;
	}
	$config['per_page'] = $limit;

	$num_pages = ceil($data['total'] / $limit);

	//cria o ir para
	$irpara = '<span class="primeiro">Ir para: <input type="text" size="4" onkeypress="var page = ((parseInt(this.value)-1) < 0 || isNaN(parseInt(this.value)-1) ? 0 : (parseInt(this.value)-1));(page >= '.$num_pages.') ? page = '.$num_pages.'-1 : 0 ;event.keyCode == 13? location.href = \''.$config['base_url'].'\'+page*'.$limit.' : true"></span>';

	$config['uri_segment'] = $seg;

	$config['full_tag_open'] = '<div id="contentPaginacao"><p class="paginacao link">';
	$config['full_tag_close'] = $irpara.'</p></div>';

	$config['first_link'] = 'Primeira';
	$config['first_tag_open'] = '<span class="primeiro">';
	$config['first_tag_close'] = '</span>';

	$config['last_link'] = '&Uacute;ltima('.$num_pages.')';
	$config['last_tag_open'] = '<span class="ultimo">';
	$config['last_tag_close'] = '</span>';

	$config['next_link'] = 'Pr&oacute;ximo';
	$config['next_tag_open'] = '<span class="proximo">';
	$config['next_tag_close'] = '</span>';

	$config['prev_link'] = 'Anterior';
	$config['prev_tag_open'] = '<span class="anterior">';
	$config['prev_tag_close'] = '</span>';

	$config['cur_tag_open'] = '| <strong class="med">';
	$config['cur_tag_close'] = ' </strong>';

	$config['num_tag_open'] = '|';
	$config['num_tag_close'] = '';

	$CI->pagination->initialize($config);

	$pag = $CI->pagination->create_links();

	return $pag;
}

function cortatexto($texto,$tamanho,$bold=""){
 	$str = substr($texto,0,$tamanho);

 	if($bold!=""){
 		$str = "<b>".$str."</b>";
 	}

 	if(strlen($texto)<$tamanho){
 		return $str;
 	}

 	return $str."..";
}



function hasPermissao($perm_type, $module=null)
{
	$perm_type = explode(',', $perm_type);

	$CI =& get_instance();

	if ( $module == null )
		$module = $CI->uri->segment(1);

	return $CI->grupo->getPermissao($CI->session->userdata[USUARIO]['ID_USER'], $module, $perm_type);
}

function redirect_default($redirect, $registro )
{
	$CI =& get_instance();

	$controller = $CI->uri->segment(1);
	$method = $CI->uri->segment(2)=='' ?'index' :$CI->uri->segment(2);


	redirect('home/redirect_default/' . $controller . '/' . $method . '/' . $redirect . '/' . $registro );
}
 function inteiros()
 {
 	for($i=1;$i<32;$i++)
 	{
 		$dados[$i] = $i;
 	}
 	return $dados;
 }
 function htmlCheckList($paginacao,$carrinho, $QTD_CATEGORIA_PAGINACAO,$fichas)
 {
 		$html ='';
		$html.=	'<style>
					.titulo_view_pdf {color: #F6821F; font-weight: bold; font-size: 20px; margin: 0 0 10px 70px; width:500px;border:1px solid #ffffff;}
					.titulo_pdf { color:#333; font-weight: bold; width: 180px;	float:left; border-top: 1px solid #BDBDBD;}
					.resposta_pdf { margin-left:10px; }
					.titulo_form{ color: #F6821F;float:left;}
					.title{	color: #F6821F;	font-weight: bold;font-size: 24px;text-align:center;}
					th{ color: #F6821F; }
				</style>' ;


		$html.= '<br /><br /><div class="title">RELAT&Oacute;RIO DE CHECKLIST</div><br />';


		$html.=	'<div style="width:600px;"> ' .
					'<div class="titulo_view_pdf">N&uacute;mero do Job '.(isset($paginacao['ID_PAGINACAO']) ?$paginacao['ID_PAGINACAO'] :'&nbsp;'). '</div><br /><br />' .
					'<span class="titulo_pdf">Descri&ccedil;&atilde;o:</span>' .
					'<span class="resposta_pdf">' . ( (isset($paginacao['DESC_PAGINACAO']))?$paginacao['DESC_PAGINACAO']:'&nbsp;' ) . '</span><br /><br />'.
					'<span class="titulo_pdf">Data de Veicula&ccedil;&atilde;o:</span>'.
					'<span>'. ( (isset($paginacao['DT_VEICULACAO_PAGINACAO']))?format_date_to_form($paginacao['DT_VEICULACAO_PAGINACAO']):'&nbsp;') . '</span><br /><br />' .
					'<span class="titulo_pdf">Tipo:</span>' .
		    		'<span>' . ( (isset($paginacao['DESC_TIPO_PAGINACAO']))?$paginacao['DESC_TIPO_PAGINACAO']:'&nbsp;' ).' </span><br /><br />'.
					'<span class="titulo_pdf">Pra&ccedil;a:</span>' .
		    		'<span>' . ( (isset($paginacao['DESC_PRACA']))?$paginacao['DESC_PRACA']:'&nbsp;' ). '</span><br /><br />' .
					'<span class="titulo_pdf">M&iacute;dia:</span>' .
		    		'<span>' . ( (isset($paginacao['DESC_MIDIA']))?$paginacao['DESC_MIDIA']:'&nbsp;' ) .' </span><br /><br />' .
					'<span class="titulo_pdf">Target:</span>' .
			 		'<span>' . ((isset($paginacao['DESC_CLASSIFICACAO']))?$paginacao['DESC_CLASSIFICACAO']:'&nbsp;') . '</span><br /><br />'.
					'<span class="titulo_pdf">Ve&iacute;culo:</span>' .
					'<span>' . ((isset($paginacao['DESC_VEICULO']))?$paginacao['DESC_VEICULO']:'&nbsp;' ) . '</span><br /><br />' .
					'<span class="titulo_pdf">Formato:</span>' .
					'<span>'. ((isset($paginacao['DESC_FORMATO']))?$paginacao['DESC_FORMATO']:'&nbsp;' ) . '</span><br /> <br />'.
					'<span class="titulo_pdf">N&ordm; de P&aacute;ginas:</span> '.
					'<span>'. ((isset($paginacao['NUMERO_PAGINACAO']))?$paginacao['NUMERO_PAGINACAO']:'&nbsp;' ).'</span><br /><br />'.
					'<span class="titulo_pdf">Observa&ccedil;&atilde;o:</span> '.
					'<span>' . ((isset($paginacao['OBS_PREPAGINACAO']))?$paginacao['OBS_PREPAGINACAO']:'&nbsp;' ) . '</span><br /><br /> '.
					'<span class="titulo_pdf">Situa&ccedil;&atilde;o:</span>'.
					'<span> '.$paginacao['FLAG_ACTIVE_PAGINACAO']. ' </span><br /> ' .
			'</div>' ;


		//	dados da configuracao
		$html.='<div style="width:500px;">' .
					'<div class="titulo_view_pdf">Configura&ccedil;&atilde;o</div>' .
					'<br />' .
					'<table>' .
						'<tr>' .
							'<th>Descri&ccedil;&atilde;o</th>' .
							'<th>Por categoria</th>' .
							'<th>Por p&aacute;gina</th>' .
						'</tr>' ;
						foreach($QTD_CATEGORIA_PAGINACAO as $configcategoria)
						{
							$html .='<tr>' .
										'<td> '. $configcategoria['DESC_CATEGORIA'].' :</td>' .
										'<td align="center">&nbsp;' . $configcategoria['QTD_Q_C_P'].' </td> '.
										'<td align="center">&nbsp;' . $configcategoria['PAGINA_Q_C_P'] . '</td>'.
									'</tr>';
						}
			$html.= '</table>' .
				'</div>';



	//	dados do carrinho


		$html.='<div style="width:500px;margin-top:15px;">' .
				'<div class="titulo_view_pdf">Carrinho '. (isset($carrinho['ID_PAGINACAO']) ?$carrinho['ID_PAGINACAO'] :'&nbsp;') . '</div> <br/>' .
				'<div class="titulo_pdf">Descri&ccedil;&atilde;o:</div>' .
				((isset($carrinho['DESC_CARRINHO']))?$carrinho['DESC_CARRINHO']:'&nbsp; ').
				'<br/>	<br/>'.
				'<div class="titulo_pdf">Respons&aacute;vel:</div>' .
				((isset($carrinho['RESPONSAVEL_CARRINHO']))? $carrinho['RESPONSAVEL_CARRINHO']:'&nbsp;') .
				'<br/>	<br/>' .
				'<div class="titulo_pdf">Coment&aacute;rios:</div>' .
				((isset($carrinho['COMENTARIO_CARRINHO'])?$carrinho['COMENTARIO_CARRINHO']:'') .
				'<br/>	<br/>' .
				'<div class="titulo_pdf">Data de validade: </div>' .
				(isset($carrinho['DT_EXPIRA_CARRINHO']))? format_date_to_form($carrinho['DT_EXPIRA_CARRINHO']) : '');

		if( (isset($fichas)) && is_array($fichas) )
		{
				$html.='<table width="100%">' .
				'<tr>' .
				'<td colspan="6"><div class="titulo_view_pdf">Lista de fichas selecionadas</div></td>'.
				'</tr>' ;

				foreach($fichas as $f)
				{
					$html.='<tr>' .
							'<th>Sku</th>' .
							'<th>Categoria</th>' .
							'<th>Subcategoria</th>' .
							'<th>Fabricante</th>' .
							'<th>Marca</th>' .
							'<th>Observa&ccedil;&atilde;o:</th>' .
							'</tr>';
					$html.='<tr>' .
							'<td>'.((isset($f['FICHA']))?$f['FICHA']['COD_FICHA']:$f['COD_FICHA']).'</td>' .
							'<td>'.$f['DESC_CATEGORIA'].'</td>' .
							'<td>'.$f['DESC_SUBCATEGORIA'].'</td>' .
							'<td>'.$f['DESC_FABRICANTE'].'</td>' .
							'<td>'.$f['DESC_MARCA'].'</td>' .
							'<td>'.$f['DESC_FICHA'].'</td>' .
							'</tr>';
					$html.='<tr>' .
							'<th>&Aacute; Vista:</th>' .
							'<th>Pre&ccedil;o de:</th>' .
							'<th>Pre&ccedil;o por:</th>' .
							'<th>Economize:</th>' .
							'<th>Entrada:</th>' .
							'<th>Parcelas:</th>' .
							'</tr>';
					$html.='<tr>' .
							'<td>'.$f['PRECO_AVISTA_PRICING'].'</td>' .
							'<td>'.$f['PRECO_DE_PRICING'].'</td>' .
							'<td>'.$f['PRECO_POR_PRICING'].'</td>' .
							'<td>'.$f['ECONOMIZE_PRICING'].'</td>' .
							'<td>'.$f['ENTRADA_PRICING'].'</td>' .
							'<td>'.$f['PARCELAS_PRICING'].'</td>' .
							'</tr>';
				}



				$html.='</table>';

		}

		/*
		$html.= 	'<div style="width:500px;margin-top:15px; ">' .
						'<div class="titulo_view_pdf">Lista de fichas selecionadas</div>' .
						'' ;

								if( (isset($fichas)) && is_array($fichas) )
								{

									foreach($fichas as $f)
									{
										$html.= '<div class="titulo_form">Sku:</div>' .
												((isset($f['FICHA']))?$f['FICHA']['COD_FICHA']:$f['COD_FICHA']).
												'<br/>	<br/>'.
												'<div class="titulo_form">Categoria:</div>' .
												$f['DESC_CATEGORIA'].
												'<br/>	<br/>'.
												'<div class="titulo_form">SubCategoria:</div>' .
												$f['DESC_SUBCATEGORIA'].
												'<br/>	<br/>'.
												'<div class="titulo_form">Fabricante:</div>' .
												$f['DESC_FABRICANTE'].
												'<br/>	<br/>'.
												'<div class="titulo_form">Marca:</div>' .
												$f['DESC_MARCA'].
												'<br/>	<br/>'.
												'<div class="titulo_form">Observa&ccedil;&atilde;o:</div>' .
												$f['DESC_FICHA'].
												'<br/>	<br/>'.
												'<div class="titulo_form">&Aacute; Vista:</div>' .
												$f['PRECO_AVISTA_PRICING'].
												'<br/>	<br/>'.
												'<div class="titulo_form">Pre&ccedil;o de:</div>' .
												$f['PRECO_DE_PRICING'].
												'<br/>	<br/>'.
												'<div class="titulo_form">Pre&ccedil;o por:</div>' .
												$f['PRECO_POR_PRICING'].
												'<br/>	<br/>'.
												'<div class="titulo_form">Economize:</div>' .
												$f['ECONOMIZE_PRICING'].
												'<br/>	<br/>'.
												'<div class="titulo_form">Entrada:</div>' .
												$f['ENTRADA_PRICING'].
												'<br/>	<br/>'.
												'<div class="titulo_form">Parcelas:</div>' .
												$f['PARCELAS_PRICING'].
												'<br/>	<br/>'.
												'<div class="titulo_form">Total:</div>' .
												$f['TOTAL_PRICING'].
												'<br/>	<br/>'.
												'<div class="titulo_form">Juros AM:</div>' .
												$f['JUROS_AM_PRICING'].
												'<br/>	<br/>'.
												'<div class="titulo_form">Juros AA:</div>' .
												$f['JUROS_AA_PRICING'].
												'<br/>	<br/>'.
												'<div class="titulo_form">Juros AA:</div>' .
												$f['JUROS_AA_PRICING'].
												'<br/>	<br/>'.
												'<div class="titulo_form">CET:</div>' .
												$f['CET_PRICING'].
												'<br/>	<br/>';


			}
	}
		$html.='</div>';*/


			return $html;
 }


 /**
 * funcao que formata uma data de entrada
 * @param string $date    Data de a ser formatada
 * @param string $format  Formato de saida da data
 * @return string         Data formatada
 */
function format_date($date,$format='Y-m-d') {
	$tokens = array();
	$hora = '( (\d{2}):(\d{2}):(\d{2}))?';

	$tokens[4] = 0;
	$tokens[5] = 0;
	$tokens[6] = 0;

	if(preg_match('@^(\d{4})-(\d{2})-(\d{2})'.$hora.'$@',$date,$reg)){
		$tokens[1] = $reg[3];
		$tokens[2] = $reg[2];
		$tokens[3] = $reg[1];

	} else if(preg_match('@^(\d{2})/(\d{2})/(\d{4})'.$hora.'$@',$date,$reg)){
		$tokens[1] = $reg[1];
		$tokens[2] = $reg[2];
		$tokens[3] = $reg[3];
	} else {
		throw new Exception('Formato de data invalida: ' . $date);
	}

	if( !empty($reg[4]) ){
		$tokens[4] = $reg[5];
		$tokens[5] = $reg[6];
		$tokens[6] = $reg[7];
	}

	return date($format, mktime($tokens[4],$tokens[5],$tokens[6],$tokens[2],$tokens[1],$tokens[3]));
}

/**
 * Retorna cores zebradas para montar tabelas
 * @param  boolean        $retorna indica se e para retornar (true) ou escrever direto na tela (false)
 * @return string | void  void se escrever direto na tela, string se for para retornar
 */
function zebrado($retorna=false){
	static $cor;

	$cor = $cor == '#FFFFFF' ? '#CCCCCC' : '#FFFFFF';

	if($retorna){
		return $cor;
	}

	echo $cor;
}

/**
 * monta uma lista de opcoes a partir de um array simples
 * @param array   $lista
 * @param string  $selectedVal
 * @param boolean $useKeyAsValue
 * @return string
 */
function montaOptionsSimples($lista, $selectedVal = '', $useKeyAsValue=false){
	$str = '';
	if(!empty($lista) && is_array($lista)){
		foreach($lista as $key => $value){
			$model = '<option value="%s"%s>%s</option>'.PHP_EOL;

			if($useKeyAsValue) {
				$str .= sprintf($model,$key,$key==$selectedVal ? ' selected="selected"' : '',$value);
			} else {
				$str .= sprintf($model,$value,$value==$selectedVal ? ' selected="selected"' : '',$value);
			}
		}
	}
	return $str;
}

/**
 * monta uma lista de opcoes a partir de um array associativo
 * @author Hugo Silva
 * @param array $lista
 * @param string $chave
 * @param string $label
 * @param string $selectedVal
 * @return string
 */
function montaOptions($lista, $chave, $label, $selectedVal = '',$filtrar = false){
	$str = array();

	if(!empty($lista) && is_array($lista))	{
		if(count($lista) > 1){

			if($chave == "ID_TIPO_JOB"){
				if($selectedVal == ''){
					$selectedVal = 3;
				}

				$model = '<option value="%s"%s>%s</option>'.PHP_EOL;
				foreach($lista as $item){
					$str[] = sprintf(
						$model,
						$item[$chave],
						$item[$chave]==$selectedVal ? ' selected="selected"' : '',
						$item[$label]
					);
				}
				
			}else{
				$model = '<option value="%s"%s>%s</option>'.PHP_EOL;
				foreach($lista as $item){
					$str[] = sprintf(
						$model,
						$item[$chave],
						$item[$chave]==$selectedVal ? ' selected="selected"' : '',
						$item[$label]
					);
				}
			}
			
		}else{
			if($filtrar){
				$model = '<option value="%s"%s>%s</option>'.PHP_EOL;
				foreach($lista as $item){
					$str[] = sprintf(
						$model,
						$item[$chave],
						' selected="selected"',
						$item[$label]
					);
				}
			}else{
				$model = '<option value="%s"%s>%s</option>'.PHP_EOL;
				foreach($lista as $item){
					$str[] = sprintf(
						$model,
						$item[$chave],
						$item[$chave]==$selectedVal ? ' selected="selected"' : '',
						$item[$label]
					);
				}
			}
			
		}
		

	}
	return implode($str);
}






function montaOptionsMult($lista, $chave, $label, $selectedVal = array()){
	$str = '';
	if(!empty($lista) && is_array($lista)){
		$model = '<option value="%s"%s>%s</option>'.PHP_EOL;
		foreach($lista as $item){
			$teste = array_search($item[$chave], $selectedVal);
			$str .= sprintf(
				$model,
				$item[$chave],
				$teste ? ' selected="selected"' : '',
				$item[$label]
			);
		}
	}
	return $str;
}

/**
 * escreve na tela uma mensagem de erro, colocada na variavel global $_REQUEST
 * @param string $key chave do erro
 * @return void
 */
function showError($key){
	if(!empty($_REQUEST['erros'][$key])){
		echo '<span class="erros">'.$_REQUEST['erros'][$key].'</span>';
	}

	echo '';
}

/**
 * Verifica se uma determinada chave existe nas variaveis superglobais
 * Caso ela exista, imprime o resultado
 *
 * @author Hugo Silva
 * @param string $key
 * @param boolean $return
 * @return mixed
 */
function post($key,$return=false){
	$val = '';

	if(isset($_POST[$key])) $val = $_POST[$key];
	else if(isset($_GET[$key])) $val = $_GET[$key];
	else if(isset($_REQUEST[$key])) $val = $_REQUEST[$key];

	if($return){
		return $val;
	}

	echo $val;
}

/**
 * Marca uma option como selecionada
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 * @param string $key
 * @param string $val
 * @return void
 */
function selected($key, $val=1){
	if(post($key,true) == $val){
		echo ' selected="selected"';
	}
}

/**
 * Marca uma checkbox como selecionada
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 * @param string $key
 * @param string $val
 * @return void
 */
function checked($key, $val=1){
	if(post($key,true) == $val){
		echo ' checked="checked"';
	}
}

/**
 * Move um arquivo enviado por upload
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 * @param string $key chave do arquivo no array $_FILES
 * @param string $path Diretorio para onde sera movido o arquivo
 * @param string $newname Novo nome do arquivo sem extensao
 * @return boolean
 */
function moveFile($key, $path, $newname){
	if(empty($_FILES[$key])){
		return false;
	}

	if(!is_uploaded_file($_FILES[$key]['tmp_name'])){
		return false;
	}

	if(!is_dir($path)){
		mkdir($path, 0777);
	}

	$parts = explode('.', $_FILES[$key]['name']);
	$ext = array_pop($parts);

	$fullnewname = $newname . '.' . $ext;

	if(@move_uploaded_file($_FILES[$key]['tmp_name'], $path . DIRECTORY_SEPARATOR . $fullnewname)){
		chmod($path . DIRECTORY_SEPARATOR . $fullnewname, 0777);
		return true;
	}

	return false;
}

/**
 * Retorna um array com os arquivos que existem no diretório
 *
 * @author Bruno de Oliveira
 * @link http://www.247id.com.br
 * @param string $dir
 * @return array
 */
function arquivosDir($dir){
		
	/* Abre o diretório */
	
	$pasta = opendir($dir);
	$arquivos = array();
	/* Loop para ler os arquivos do diretorio */
	while ($arquivo = readdir($pasta)){
		/* Verificacao para exibir apenas os arquivos e nao os caminhos para diretorios superiores */
		if ($arquivo != "." && $arquivo != ".." && $arquivo != ".HSancillary" && $arquivo != ".HSResource"){
			$key = explode('_', $arquivo);
			if(isset($key[1])){
				$key = explode('.', $key[1]);
				$key = $key[0] - 1;
				$arquivos[$key] = $arquivo;
			} else{
				$arquivos[] = $arquivo;
			}
		}
	}
	ksort($arquivos);
	
	return $arquivos;
}

function cmp($a, $b)
{
	if ($a == $b) {
		return 0;
	}
	return ($a < $b) ? -1 : 1;
}

/**
 * Recupera um logotipo pela pasta e nome do arquivo sem extensao
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 * @param string $path
 * @param string $name
 * @return string
 */
function getLogo($path, $name){
	$logo = '';

	if(empty($name)){
		return $logo;
	}

	$files = glob($path . '/' . $name . '.*');

	if(!empty($files)){
		$file = array_shift($files);
		$logo = substr($file, strrpos($file,'/')+1);
	}

	return $logo;
}

/**
 * Monta uma SELECT com os itens ou um input hidden se tiver somente um elemento
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 * @param array $lista
 * @param string $chave
 * @param string $label
 * @param strig $selectedVal
 * @param strig $name
 * @return string
 */
function selectOrHidden($lista, $chave, $label, $selectedVal = '', $name = ''){
	if(empty($lista)){
		return '';
	}

	$str = '';

	if(empty($name)){
		$name = $chave;
	}

	if(count($lista) == 1){
		$str = sprintf('<input type="hidden" name="%s" id="%s" value="%s" /> %s',
			$name, $name, $lista[0][$chave], $lista[0][$label]
		);
	} else {
		$str = sprintf('<select name="%s" id="%s">', $name, $name);
		$str .= '<option value=""></option>';
		$str .= montaOptions($lista,$chave,$label,$selectedVal);
		$str .= '</select>';
	}

	return $str;
}

/**
 * Escreve um campo hidden com um valor da sessao
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 * @param string $key
 * @param string $desc
 * @return void
 */
function sessao_hidden($key, $desc, $show = true){
	$usuario = Sessao::get('usuario');
	printf('<input type="hidden" name="%s" id="%s" value="%s" /> %s',
		$key, $key, $usuario[$key], ($show ? $usuario[$desc] : '')
	);
}

function sessao_hidden_name($name, $key, $desc){
	$usuario = Sessao::get('usuario');
	printf('<input type="hidden" name="%s" id="%s" value="%s" /> %s',
		$name, $key, $usuario[$key], $usuario[$desc]
	);
}
/**
 * Executa um print_r ja com a tag <pre> e um exit opcional
 *
 * @author Juliano Polito
 * @param $data Qualquer tipo de dado para passar ao print_r
 * @param $exit boolean Indica se deve executar um exit
 * @return void
 */
function print_rr($data, $exit = true){
	echo "<pre>";
	print_r($data);
	if($exit){
		exit;
	}
}

/**
 * Recupera $var[$key] caso seja um indice valido ou $default
 * @author juliano.polito
 * @param $var Array
 * @param $key Chave do array
 * @param $default Valor padrao a ser retornado caso a chave nao exista no array
 * @return mixed
 */
function val($var,$key,$default = ''){
	if(!isset($var[$key])){
		return $default;
	}else{
		return $var[$key];
	}
}


/**
 * Converte os valores para UTF-8
 *
 * @author Hugo Ferreira da Silva
 * @param mixed $o Dado a ser convertido
 * @return mixed Dados convertidos
 */
function toUTF8( $o ) {
	if(is_string($o)) {
		$o = utf8_encode($o);
		return $o;
	}
	if(is_array($o)) {
		foreach($o as $k=>$v) {
			$o[$k] = toUTF8($o[$k]);
		}
		return $o;
	}
	if(is_object($o)) {
		$l = get_object_vars($o);
		foreach($l as $k=>$v) {
			$o->$k = toUTF8( $v );
		}
	}
	// padrao
	return $o;
}

/**
 * Converte os valores de UTF-8
 *
 * @author Hugo Ferreira da Silva
 * @param mixed $o Dado a ser convertido
 * @return mixed Dados convertidos
 */
function fromUTF8( $o ) {
	if(is_string($o)) {
		$o = utf8_decode($o);
		return $o;
	}
	if(is_array($o)) {
		foreach($o as $k=>$v) {
			$o[$k] = fromUTF8($o[$k]);
		}
		return $o;
	}
	if(is_object($o)) {
		$l = get_object_vars($o);
		foreach($l as $k=>$v) {
			$o->$k = fromUTF8( $v );
		}
	}
	// padrao
	return $o;
}

/**
 * Converte para carateres de mac
 *
 * <p>Principalmente usado por conta dos diretorios</p>
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 * @param string $str
 * @return string
 */
function utf8MAC($str){
	$ent = mb_convert_encoding($str, 'HTML-ENTITIES', "UTF-8");

	$list = array(
		'grave' => '&#768',
		'acute' => '&#769',
		'circ' => '&#770',
		'tilde' => '&#771',
		'cedil' => '&#807'//,
		//'uml' => '&#168'
	);

	foreach($list as $key => $target){
		$ent = preg_replace('@&(\w{1})'.$key.';@', '$1'.$target.';', $ent);
		$ent = preg_replace('@&'.$key.';@', $target.';', $ent);
	}

	return mb_convert_encoding($ent, "UTF-8",'HTML-ENTITIES');
}


function fromUtf8MAC($str){
	$ent = mb_convert_encoding($str, 'HTML-ENTITIES', "UTF-8");

	$list = array(
		'grave' => '#768',
		'acute' => '#769',
		'circ' => '#770',
		'tilde' => '#771',
		'cedil' => '#807'//,
		//'uml' => '#168'
	);

	foreach($list as $key => $target){
		$ent = preg_replace('@(\w{1})&'.$target.';@', '&$1'.$key.';', $ent);
	}

	return mb_convert_encoding($ent, "UTF-8",'HTML-ENTITIES');
}

/**
 * Remove todos os acentos e caracteres especiais
 * @author juliano.polito
 * @param $msg
 * @return string
 */
function removeAcentos($msg){
	$DE = array('á','é','í','ó','ú',
				'à','è','ì','ò','ù',
				'â','ê','î','ô','û',
				'ä','ë','ï','ö','ü',
				'ã','õ','ç','ñ',
				'ý','ÿ',
				'Á','É','Í','Ó','Ú',
				'À','È','Ì','Ò','Ù',
				'Ä','Ë','Ï','Ö','Ü',
				'Â','Ê','Î','Ô','Û',
				'Ã','Õ','Ç','Ñ',
				'Ý',
				' ','{§£}');
	$PA = array('a','e','i','o','u',
				'a','e','i','o','u',
				'a','e','i','o','u',
				'a','e','i','o','u',
				'a','o','c','n',
				'y','y',
				'A','E','I','O','U',
				'A','E','I','O','U',
				'A','E','I','O','U',
				'A','E','I','O','U',
				'A','O','C','N',
				'Y',
				'_','/');

	$msg = str_replace($DE, $PA, $msg);

	$msg = preg_replace('/[^a-zA-Z0-9_\/]/','',$msg);

	return $msg;
}

/**
 * ajusta uma string para ser usada como chave do storage
 * @author Hugo Ferreira da Silva
 * @param string $string
 * @return string
 */
function ajustaChaveStorage($string){
	$string = removeAcentos($string);
	$string = preg_replace('@(\\|\/|/|\s)@','_', $string);
	$string = strtoupper($string);

	return $string;
}

/**
 * formata valor em dinheiro
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 * @param float $vlr
 * @return string
 */
function money($vlr,$cifrao = ''){
	if(empty($vlr)){
		return $cifrao.'';
	}

	return $cifrao.number_format($vlr, 2, ',','.');
}

/**
 * transforma uma notacao de moeda para float
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 * @param string $vlr
 * @return float
 */
function money2float($vlr){
	return (float)str_replace(',','.', str_replace('.','',$vlr));
}

/**
 * prepara um float para usar em campos de centimetragem
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 * @param float $vlr
 * @return string
 */
function centimetragem($vlr){
	return number_format($vlr, 2, ',','');
}

function marcaAlterado($data){
	echo ' class="' . ($data['novo'] == $data['antigo'] ? '' : 'alteracao') . '" ';

	if($data['novo'] != $data['antigo']){
		printf(' onmouseover="valorAntigo(this, %s)" onmouseout="hideValorAntigo()"', "'" . htmlentities($data['antigo'],ENT_QUOTES) . "'");
	}
}

function showIcon($url){
	$url = preg_replace('@\?img_path=(.*?)$@e', '"?img_path=".rawurlencode("$1")', $url);
	return $url;
}

function escapaStringIndesign($string){
	// remove caracteres de controle
	// @see http://www.w3schools.com/tags/ref_urlencode.asp
	$controlChars = explode(',' ,'%00,%01,%02,%03,%04,%05,%06,%07,%08,%09,%0B,%0C,%0E,%0F,%10,%11,%12,%13,%14,%15,%16,%17,%18,%19,%1A,%1B,%1C,%1D,%1E,%1F');
	// transforma a string para url
	$string = urlencode($string);

	// para cada caracter de controle
	foreach($controlChars as $char){
		// tira o caracter de controle
		$string = str_replace($char, '', $string);
	}

	// volta a string ao normal
	$string = urldecode($string);

	$from = array('<','>','&');
	$to = array('&lt;','&gt;','&amp;');
	$string = str_replace($from,$to, $string);

	return $string;
}


/**
 * cabecalho para envio de excel
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 * @param string $filename
 * @return void
 */
function header_excel($filename){
	header("Content-Type: application/excel");
	header('Content-disposition: attachment; filename="'.$filename.'.xls"');
}

/**
 * Transforma os segundos em dias, horas, mins e segundos
 *
 * @author Hugo Ferreira da Silva
 * @param int $segundos
 * @return string
 */
function segundosToHora($segundos){
	$days = floor($segundos / (3600*24));
	$hours = floor( ($segundos-($days*3600*24)) / 3600 );
	$secs = floor( $segundos % 60 );
	$mins = floor( ($segundos - ($days*3600*24) - ($hours*3600)) / 60 );

	if($days > 0){
		return sprintf('%02d dias, %02d:%02d:%02d', $days, $hours, $mins, $secs);
	} else {
		return sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
	}
}

/**
 * valida se o logotipo enviado
 * @author Hugo Ferreira da Silva
 * @param string $filename nome do arquivo
 * @return void
 */
function validaLogotipo($filename){
	if( !preg_match('@\.png$@i', $filename) ){
		return false;
	}
	list($width, $height, $type) = getimagesize($filename);

	if( $width != 118 && $height != 50 ){
		return false;
	}

	return true;
}

/**
 * Pega os valores de uma determinada chave de um array
 *
 * @author Hugo Ferreira da Silva
 * @param array $lista
 * @param string $chave
 * @return array
 */
function getValoresChave(array $lista, $chave){
	$res = array();
	foreach($lista as $item){
		if( isset($item[$chave]) ){
			$res[] = $item[$chave];
		}
	}
	return $res;
}

/**
 * Altera as chaves da lista com os valores indicados no campo $campoNovaChave
 * @author Hugo Ferreira da Silva
 * @param array $lista
 * @param string $campoNovaChave
 * @return array
 */
function alteraChaveLista(array $lista, $campoNovaChave){
	$new = array();
	foreach($lista as $item){
		$new[$item[$campoNovaChave]] = $item;
	}

	return $new;
}

/**
 * Metodo auxiliar para recuperar os produtos que um usuario tem acesso
 *
 * Ja verifica na sessao se o usuario e um cliente ou uma agencia.
 *
 * Se for um cliente, lista somente os produtos que o grupo dele tem acesso. Se
 * caso uma agencia for informada, lista a intersecao dos produtos que a agencia informada
 * e o usuario logado podem ter acesso de produtos.
 *
 * Se o usuario logado for de uma agencia, pega todos os produtos que esta agencia
 * tem acesso a um determinado cliente.
 *
 * @author Hugo Ferreira da Silva
 * @param int $idcliente
 * @param int $idagencia
 * @return array Lista de produtos que o usuario logado tem acesso
 */
function getProdutosUsuario($idcliente = null, $idagencia = null){
	$ci     = &get_instance();
	$user   = Sessao::get('usuario');
	$result = array();

	// se for cliente
	if( !empty($user['ID_CLIENTE']) ){
		// se informou uma agencia
		if( !is_null($idagencia) && $idagencia !== '' ){
			$result = $ci->produto->getByAgenciaUsuario($idagencia, $user['ID_USUARIO'], $user['ID_CLIENTE']);

		// se nao informou agencia
		} else {
			$result = $ci->produto->getByClienteUsuario($user['ID_CLIENTE'], $user['ID_USUARIO']);
		}
	}

	// se for agencia
	if( !empty($user['ID_AGENCIA']) ){
		// se informou um cliente
		if( !is_null($idcliente) && $idcliente !== '' ){
			$result = $ci->produto->getByAgencia($user['ID_AGENCIA'], $idcliente);

		// se nao informou cliente
		} else {
			$result = $ci->produto->getByAgencia($user['ID_AGENCIA']);
		}
	}

	return $result;
}

/**
 * Recupera os codigos das categorias de um usuario
 *
 * @author Hugo Ferreira da Silva
 * @return array
 */
function getCategoriasUsuario($idcliente = '', $somenteChaves=true){
	$user = Sessao::get('usuario');
	$result = array();
	$ci = &get_instance();

	if( !empty($user['ID_CLIENTE']) ){
		$result = $ci->categoria->getByUsuarioCliente($user['ID_USUARIO'],$user['ID_CLIENTE']);
	}

	if( !empty($user['ID_AGENCIA']) && !empty($idcliente) ){
		$result = $ci->categoria->getByCliente($idcliente);
	}

	if( $somenteChaves ){
		$result = getValoresChave($result,'ID_CATEGORIA');
	}

	return $result;
}

/**
 * Recupera o valor de uma chave de array associativo
 *
 * Se quisermos pegar o valor de
 * $_POST['teste']['de']['sistema']['com']['niveis']
 *
 * Fazemos
 * ref($_POST,'teste','de','sistema','com','niveis')
 *
 * Ela ja faz a checagem para ver se existe, evitando erros
 *
 * @author Hugo Ferreira da Silva
 * @param array $ref
 * @return mixed
 */
function ref($ref){
	$val = $ref;
	$args = func_get_args();
	for($i=1; $i<count($args); $i++){
		if(isset($val[$args[$i]]) && is_array($val)){
			$val = $val[$args[$i]];
		}
	}
	// se nao encontrou (eh o mesmo do inicio) ou  ainda é array
	if( is_array($val) || $val == $ref ){
		// retorna vazio
		return '';
	}
	return $val;
}

/**
 * Exibe uma imagem indicando se um job sofreu ou nao alteracoes
 *
 * Se passar o mouse sobre a imagem, mostra tambem
 * quantas vezes foram solicitadas alteracoes
 *
 * @author Hugo Ferreira da Silva
 * @param array $job Dados do job
 * @return void
 */
function sinalizarAlteracoes($job){
	if( !empty($job['ALTERACOES_JOB']) ){
		echo '<a href="#" onclick="Ficha.HistoricoJob(', $job['ID_JOB'], '); return false;">';
		if($job['ALTERACOES_JOB'] == 1){
			echo '<img src="', base_url(), THEME, 'img/information.png" title="', $job['ALTERACOES_JOB'], ' alteração solicitada" />';
		} else {
			echo '<img src="', base_url(), THEME, 'img/information.png" title="', $job['ALTERACOES_JOB'], ' alterações solicitadas" />';
		}
		echo '</a>';
	} else {
		echo '<img src="', base_url(), THEME, 'img/accept.png" title="Nenhuma alteração solicitada" />';
	}
}

/**
 * Exibe um icone de alerta para sinalizar pendencias
 *
 * Somente exibe o item se:
 * - A ficha for temporaria OU nao estiver aprovada
 * - Forem informadas pendencias de dados basicos OU pendencias de campos
 *
 * @author Hugo Ferreira da Silva
 * @param array $ficha Array associativo com todos os dados da ficha
 * @return void
 */
function alerta_ficha($ficha){
	if( (!empty($ficha['PENDENCIAS_DADOS_BASICOS']) || !empty($ficha['PENDENCIAS_CAMPOS'])) ){
		printf('<a href="#" onclick="return exibePendencias(\'%s\', \'%s\', \'%s\')">
				<img src="%simg/alerta-amarelo.jpg" title="Pend&ecirc;ncias" /></a>',
			normalize_for_js($ficha['NOME_FICHA']),
			normalize_for_js($ficha['PENDENCIAS_DADOS_BASICOS']),
			normalize_for_js($ficha['PENDENCIAS_CAMPOS']),
			base_url().THEME
		);
	}
}

/**
 * Normaliza um string para ser usada por javascript
 *
 * Transforma aspas duplas em entidade HTML, tira quebras de linha e transforma
 * as quebras de linha em <br>
 *
 * @author Hugo Ferreira da Silva
 * @param string $str
 * @return string
 */
function normalize_for_js($str){
	$str = nl2br($str);
	$str = preg_replace("@(\r|\n)@",'',$str);
	$str = str_replace('"','&quot;', $str);
	$str = addslashes($str);
	return $str;
}

/**
 * Verifica se um upload foi realizado com sucesso
 * @author juliano.polito
 * @param $field Nome do campo de upload
 * @return boolean
 */
function verificaUpload($field, $ext){
	if(!empty($_FILES[$field])
		&& is_uploaded_file($_FILES[$field]['tmp_name'])
		&& $_FILES[$field]['error'] == UPLOAD_ERR_OK
		&& strripos($_FILES[$field]['name'],$ext) !== false
		){
			return true;
		}

	return false;
}

/**
 * Faz o stream de um arquivo qualquer pelo PHP utilizando fread em pedacos de 1024
 *
 * @author juliano.polito
 * @param string $path caminho completo para o arquivo
 * @return void
 */
function streamFile($path, $originalName = "", $attach = 1){
	//headers
	contentType($path,$originalName,$attach);
	//Faz o stream do arquivo
	if(is_file($path)){
		$fhandle = fopen($path,'r');
		while(!feof($fhandle)){
			echo fread($fhandle,1024);
		}
		fclose($fhandle);
	}
}

function contentType($path, $originalName, $attach = 1){
	// Grab the file extension
	$filename = pathinfo($path, PATHINFO_FILENAME);
	$extension = pathinfo($path, PATHINFO_EXTENSION);

	// Load the mime types
	@include(APPPATH.'config/mimes'.EXT);

	// default = octet
	if ( ! isset($mimes[$extension])){
		$mime = 'application/octet-stream';
	}else{
		$mime = (is_array($mimes[$extension])) ? $mimes[$extension][0] : $mimes[$extension];
	}

	$disposition = 'attachment';
	if($attach == 0){
		$disposition = 'inline';
	}

	// headers
	header('Content-Type: "'.$mime.'"');
	header('Content-Disposition: '.$disposition.'; filename="'.$originalName.'"');
	header('Expires: 0');
	header("Content-Transfer-Encoding: binary");
	//header("Content-Length: ".filesize($path));

	if (strstr($_SERVER['HTTP_USER_AGENT'], "MSIE")){
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
	}else{
		header('Pragma: no-cache');
	}


}


/**
 * Paginacao personalizada
 *
 * Usada principalmente em modais.
 * Escreve a saida diretamente na tela
 *
 * @author Hugo Ferreira da Silva
 * @param int $total
 * @param int $limit
 * @param int $pagina
 * @param int $numLinks
 * @param string $modelo
 * @return void
 */
function paginacao($total, $limit, $pagina, $numLinks = 5, $modelo = '%d'){
	$pagina = $pagina < 0 ? 0 : $pagina;

	$inicial = ($pagina/$limit) - $numLinks;

	if( $inicial < 1 ){
		$inicial = 1;
	}

	$totalPaginas = ceil($total/$limit);
	$final = ($pagina/$limit) + $numLinks;

	if( $final > $totalPaginas ){
		$final = $totalPaginas;
	}

	$list = array();
    for($i=$inicial; $i<=$final; $i++){
		if( $pagina == ($i-1)*$limit){
			$list[] = '<strong class="linkAtivoPaginacao">'.$i.'</strong>';
		} else {
			$list[] = sprintf('<a href="'.$modelo.'"> %s </a>',
				($i-1)*$limit,
				$i
			);
		}
    }

	if( $pagina > 0 ){
		printf('<a href="'.$modelo.'">Primeira</a> ', 0);
		printf('<a class="anterior" href="'.$modelo.'">Anterior</a> ',($pagina-$limit));
	}

	echo ' | ' . implode(' | ', $list) . ' | ';

	if( $pagina/$limit < $totalPaginas-1 ){
		printf('<a class="proximo" href="'.$modelo.'">Próxima</a> ',($pagina == 0 ? $limit : $pagina+$limit));
		printf('<a href="'.$modelo.'">Última</a> ', ($totalPaginas-1)*$limit);
	}
}

/**
 * Exibe a revisao em que esta o job
 * @author Hugo Ferreira da Silva
 * @param array $jobItem Dados do job
 * @return string
 */
function getRevisaoJob($jobItem){
	if( !empty($jobItem['PROVAS']) ){
		return '<br><strong>(Prova '.$jobItem['PROVAS'].')</strong>';
	}
}


/**
 * Retorna uma lista com as extensoes proibidas de fazer upload no sistema
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 * @return array lista com as extensoes proibidas.
 */
function getExtensoesProibidas(){
	static $list;

	if( empty($list) ){
		$strList = '.exe, .dll, .com, .ocx, .reg, .vbs, .wsh, .pif, .scr, .wsc, .bat, .vbx, .tlb, .wsf, .ce, ceo, .chm, .cmd, .inf, .lnk, .mid, .midi, .shs, .uue, .vcpl';
		$list = explode(',', $strList);
		foreach($list as &$item){
			$item = str_replace('.', '', trim($item));
		}
	}

	return $list;
}

/**
 * Verifica se o arquivo possui uma extensao valida
 *
 * Verifica se a extensao do arquivo esta entre as extensoes proibidas.
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 * @param string $filename
 * @return boolean true se for uma extensao valida, do contrario false
 */
function checaExtensaoValida($filename){
	$list = getExtensoesProibidas();
	$info = pathinfo($filename);

	// se nao tiver extensao
	if( empty($info['extension']) ){
		// nao pode
		return false;
	}

	return !in_array(strtolower($info['extension']), $list);
}

/**
 * Remove um diretorio recursivamente
 * 
 * @param $dir path do diretorio
 */
function rrmdir($dir) {
	if (!file_exists($dir)) return true;
	if (!is_dir($dir) || is_link($dir)) return unlink($dir);
	foreach (scandir($dir) as $item) {
		if ($item == '.' || $item == '..') continue;
		if (!rrmdir($dir . "/" . $item)) {
			chmod($dir . "/" . $item, 0777);
			if (!rrmdir($dir . "/" . $item)) return false;
		};
	}
	return rmdir($dir);
} 


function PegarChave( $item, $indice ){
	return $item[$indice];
}

/**
 * Soma quantidade de horas em uma data informada. Baseando-se no periodo util diario.
 * @author Nelson Martucci
 * @param $dataHoraASerSomada	= data/hora a ser somada no formato mktime(hora, minuto, segundo, mes, dia, ano) (default 00:00:00 01/01/1970)
 * @param $qtdHorasASerSomada 	= qtd de horas a ser somada na data/hora a ser somada no formato hh:mm
 * @param $periodoUtilInicial	= periodo util inicial no formato hh:mm
 * @param $periodoUtilFinal		= periodo util final no formato hh:mm
 * @param $consideraDiasUteis	= indicador para considerar apenas dias util
 * @param $consideraFeriados	= indicador para considerar feriados
 * @param $arrayFeriados		= array com os feriados no formato array ( mktime( 0, 0, 0, mes, dia, ano ), mktime( 0, 0, 0, mes, dia, ano ) );
 * @return unknown_type			= data somada no formato mktime ( hora, minuto, segundo, mes, dia, ano );
 * 								= retorna 'mktime(0, 0, 0, 1, 1, 1970)', caso haja inconsistencia de alguma informacao passada no parametro de entrada
 */
function horasUteis(	$dataHoraASerSomada=10800, 
						$qtdHorasASerSomada='0:00',
						$periodoUtilInicial='8:00',
						$periodoUtilFinal='17:00',
						$consideraDiasUteis='S',
						$consideraFeriados='N',
						$arrayFeriados='' ) {

	//Verifica se o array foi passado corretamente
	if ($arrayFeriados == '' || !is_array($arrayFeriados)) {
		$arrayFeriados = array();	
	}
							
	$horasMinutosPeriodoUtilInicial = explode(':', $periodoUtilInicial );
	
	//Verifica se horas do periodo util inicial foram passadas corretamente
	if (!empty($horasMinutosPeriodoUtilInicial[0]) && is_numeric($horasMinutosPeriodoUtilInicial[0]) ) {
		$horasUtilInicial = $horasMinutosPeriodoUtilInicial[0];
	} else {
		$horasUtilInicial = 00;
	}
	
	//Verifica se minutos do periodo util inicial foram passadas corretamente
	if (!empty($horasMinutosPeriodoUtilInicial[1]) && is_numeric($horasMinutosPeriodoUtilInicial[1]) && $horasMinutosPeriodoUtilInicial[1] >= 0 && $horasMinutosPeriodoUtilInicial[1] < 60) {
		$minutosUtilInicial = $horasMinutosPeriodoUtilInicial[1];
	} else {
		$minutosUtilInicial = 00;
	}
	
	$horasMinutosPeriodoUtilFinal = explode(':', $periodoUtilFinal );
	
	//Verifica se horas do periodo util final foram passadas corretamente
	if (!empty($horasMinutosPeriodoUtilFinal[0]) && is_numeric($horasMinutosPeriodoUtilFinal[0]) ) {
		$horasUtilFinal = $horasMinutosPeriodoUtilFinal[0];
	} else {
		$horasUtilFinal = 00;
	}
	//Verifica se minutos do periodo util final foram passadas corretamente
	if (!empty($horasMinutosPeriodoUtilFinal[1]) && is_numeric($horasMinutosPeriodoUtilFinal[1]) && $horasMinutosPeriodoUtilFinal[1] >= 0 && $horasMinutosPeriodoUtilFinal[1] < 60) {
		$minutosUtilFinal = $horasMinutosPeriodoUtilFinal[1];
	} else {
		$minutosUtilFinal = 00;
	}
	
	//Verifica se o periodo final eh maior que o periodo inicial
	if ( segundos( $horasUtilInicial, $minutosUtilInicial ) >= segundos( $horasUtilFinal, $minutosUtilFinal ) ) {
		//echo 'Periodo Inicial eh maior ou igual ao Periodo Final';
		return mktime(0, 0, 0, 1, 1, 1970);
	}

	//Verifica se a hora/minuto da dataHoraASerSomada estah fora do periodo inicial e final
	if ( segundos( $horasUtilInicial, $minutosUtilInicial ) > segundos( date( 'H', $dataHoraASerSomada ), date('i', $dataHoraASerSomada ) ) ||
		 segundos( $horasUtilFinal, $minutosUtilFinal ) < segundos( date( 'H', $dataHoraASerSomada ), date('i', $dataHoraASerSomada ) ) ) {
		echo 'A data/hora a ser somada esta fora do periodo inicial e final';
		return mktime(0, 0, 0, 1, 1, 1970);
	}

	//Calcula quantidade de horas diarias
	//Aqui o resultado sera comparado com outros horarios em segundos
	//Entao temos que fazer o calculo em segundos
	$horasDiaEmSegundos = segundos( $horasUtilFinal, $minutosUtilFinal ) - segundos( $horasUtilInicial, $minutosUtilInicial );
	
	$horasMinutosASerSomada = explode(':', $qtdHorasASerSomada );
	//Verifica se horas foram passadas corretamente
	if (!empty($horasMinutosASerSomada[0]) && is_numeric($horasMinutosASerSomada[0]) ) {
		$horasASerSomada = $horasMinutosASerSomada[0];
	} else {
		$horasASerSomada = 0;
	}
	
	//Verifica se minutos foram passadas corretamente
	if (!empty($horasMinutosASerSomada[1]) && is_numeric($horasMinutosASerSomada[1]) && $horasMinutosASerSomada[1] >= 0 && $horasMinutosASerSomada[1] < 60) {
		$minutosASerSomado = $horasMinutosASerSomada[1];
	} else {
		$minutosASerSomado = 00;
	}
	
	//Soma a quantidade de horas na dataHoraASerSomada.
	//Aqui o resultado pode ser fora de uma hora normal (Ex: 28:15)
	//Entao temos que fazer o calculo em segundos
	$horaMinutoSomadoEmSegundos = segundos( date('H', $dataHoraASerSomada ) + $horasASerSomada, date('i', $dataHoraASerSomada ) + $minutosASerSomado );
	
	//Calcula a hora/minuto inicial do periodo em Segundos
	$horaMinutoUtilInicialEmSegundos = segundos($horasUtilInicial, $minutosUtilInicial);
	
	//Calcula a hora/minuto final do periodo em Segundos
	$horaMinutoUtilFinalEmSegundos = segundos($horasUtilFinal, $minutosUtilFinal);
	
	//Comeca a formatar data de retorno
	$diaRetorno = date('d',$dataHoraASerSomada);
	$mesRetorno = date('m',$dataHoraASerSomada);
	$anoRetorno = date('Y',$dataHoraASerSomada);

	//Verifica se a soma da qtd de horas na data a ser somada, ultrapassou o periodo final
	//Aqui o resultado pode ser fora de uma hora normal (Ex: 28:15)
	//Entao temos que fazer o calculo em segundos
	while ( $horaMinutoSomadoEmSegundos > $horaMinutoUtilFinalEmSegundos ) {
		//Significa que teremos que acumular horas para outros dias de trabalho
		$diaRetorno++;
		
		//Verifica se teremos que considerar apenas dias uteis
		if ( $consideraDiasUteis == 'S' || $consideraDiasUteis == 's' ) {
			if ( strftime('%w', mktime( 0, 0, 0, $mesRetorno, $diaRetorno, $anoRetorno ) ) == 6	||	//Sabado
				 strftime('%w', mktime( 0, 0, 0, $mesRetorno, $diaRetorno, $anoRetorno ) ) == 0 ) {	//Domingo
				//Ignora o dia e volta para o inicio do loop para acumular outro dia
	 			continue;
			}
		}
		
		//Verifica se teremos que considerar os feriados como dia de trabalho
		if ( $consideraFeriados == 'N' || $consideraFeriados == 'n' ) {
			if (in_array( mktime( 0, 0, 0, $mesRetorno, $diaRetorno, $anoRetorno ), $arrayFeriados) ) {
				//Ignora o dia e volta para o inicio do loop para acumular outro dia
				continue;
			}
		}
		
		//Subtrai horas uteis diárias da quantidade de horas somadas em Segundos
		$horaMinutoSomadoEmSegundos -= $horasDiaEmSegundos;
	}
	
	$horaRetorno = 0;
	$minutoRetorno = 0;
	$segundoRetorno = 0;
	
	//Formata data hora final para montar retorno (horas x minutos quebrados que restaram do dia)
	while($horaMinutoSomadoEmSegundos > 59){
		$horaMinutoSomadoEmSegundos -= 60;
		$minutoRetorno += 1;
		if($minutoRetorno >= 60){
		  $minutoRetorno = 0;
		  $horaRetorno += 1;
		}
	}
	
	return mktime( $horaRetorno, $minutoRetorno, $segundoRetorno, $mesRetorno, $diaRetorno, $anoRetorno);
}

function segundos ( $hora=0, $minuto=0, $segundo=0) {
	return ($hora * 3600) + ($minuto * 60) + $segundo;
}

/**
 * Transforma os segundos recebidos em horas e minutos, no formato hh:mm
 *
 * @author Sidnei Tertuliano Junior
 * @link http://www.247id.com.br
 * @param int $segundos
 * @return string
 */
function sgundosParaHoras($segundos) {
    $horas = intval(intval($segundos) / 3600);
    $minutos = intval(($segundos / 60) % 60); 
          
	$hm = str_pad($horas, 2, "0", STR_PAD_LEFT);
	$hm .= ":";
    $hm .= str_pad($minutos, 2, "0", STR_PAD_LEFT);
	
    return $hm;
}

/**
 * Deleta coisas desnecessarias em um array
 *
 * @author Sidnei Tertuliano Junior
 * @link http://www.247id.com.br
 * @param array $arrayIntegral array inteira
 * @return array $itensManter intens para manter na array
 */
function limpaArray($arrayIntegral, $itensManter) {
	foreach($arrayIntegral as $key=>$val){
		if(!isset($itensManter[$key])){
			unset($arrayIntegral[$key]);
		}
	}
	return $arrayIntegral;
}


/**
 * Funcao para fazer a substituicao de algumas labels da GH
 *
 * @author Sidnei Tertuliano Junior
 * @link http://www.247id.com.br
 * @param array $sessao array
 * @return String $palavra palavra aser alterada
 */
function ghDigitalReplace($sessao, $palavra){
	if(count($sessao) > 0){
		if( (strtolower($sessao['DESC_CLIENTE']) == "grupo gh digital") || 
			(strtolower($sessao['DESC_AGENCIA']) == "agencia grupo gh digital")){
			switch ($palavra) {
			    case "Categoria":
			        echo "Cliente";
			        break;
			    case "Categorias":
			        echo "Clientes";
			        break;
			    case "Cliente":
			        echo "Ag&ecirc;ncia";
			        break;
			    case "Sub Categoria":
			        echo "Categoria";
			        break;
			    case "SubCategoria":
			        echo "Categoria";
			        break;
			    case "Sub-categoria":
			        echo "Categoria";
			        break;
			    case "Subcategoria":
			        echo "Categoria";
			        break;
			    case "Sub-Categoria":
			        echo "Categoria";
			        break;
			    case "subcategoria":
			        echo "Categoria";
			        break;
			    case "swf/upload_objetos.swf":
			    	echo "swf/upload_objetos_gh.swf";
			    	break;
				default:
					echo $palavra;
			}
		}
		else if( (strtolower($sessao['DESC_CLIENTE']) == "hermes") || 
			(strtolower($sessao['DESC_AGENCIA']) == "hermes") ||
			(strtolower($sessao['DESC_AGENCIA']) == "comprafacil") ||
			(strtolower($sessao['DESC_CLIENTE']) == "comprafacil")){
				switch ($palavra) {
			    case "Objetos":
			        echo "Cromo";
			        break;
			    case "objetos":
			        echo "cromo";
			        return "cromo";
			        break;
		        case "Fichas":
		        	echo "Produtos";
		        	break;
		        case "Fichas cadastradas":
		        	echo "Produtos cadastrados";
		        	break;
	        	case "Código de Barras":
	        		echo "Referência";
	        		break;
        		case "SubFichas":
        			echo "SubProdutos";
        			break;
        		case "A ficha":
        			echo "O produto";
        			break;
        		case "Subficha":
        			echo "SubProduto";
        			break;
        		case "Nome da ficha":
        			echo "Nome do Produto";
        			break;
        		case "Consulta de Fichas":
        			echo "Consulta de Produtos";
        			break;
        		case "Tipo de Ficha":
        			echo "Tipo de Produto";
        			break;
        		case "SUBFICHA":
        			echo "SUBPRODUTO";
        			break;
        		case "Fichas-Combo em que esta ficha está presente":
        			echo "Produtos-Combo em que este produto está presente";
        			break;
        		case "Nenhuma ficha-combo associada.":
        			echo "Nenhum produto-combo associado.";
        			break;
        		case "Pesquisar Subficha":
        			echo "Pesquisar SubProduto";
        			break;
        		case "Adicionar Subficha":
        			echo "Adicionar SubProduto";
        			break;
        		case "Buscar Fichas":
        			echo "Buscar Produtos";
        			break;
        		case "subficha":
        			echo "subproduto";
        			break;
		        case "ficha":
		        	echo "produto";
		        	break;
	        	case "FICHA":
	        		echo "PRODUTO";
	        		break;
        		case "Pendências da Ficha":
        			echo "Pendências do Produto";
        			break;
				default:
					echo $palavra;
			}
		}
		else{
			echo $palavra;
		}
	}
	else{
		echo $palavra;
	}
}

// transforma qualquer objeto em array
function objectToArray($d) {
	if (is_object($d)) {
		$d = get_object_vars($d);
	}

	if (is_array($d)) {
		return array_map(__FUNCTION__, $d);
	}
	else {
		return $d;
	}
}

// transform tudo oque for quebra de linha em <br>
function quebraLinha($str){
    $lineEnds = array("\r\n", "\n\r", "\r", "<br>", "<br/>", "&lt;br/&gt;");
    $str = trim(htmlspecialchars_decode($str));
    $str = str_replace($lineEnds, "\n", $str);	
    return str_replace("\n", "<br>", $str);
}

// tira as quebras de linha
function removeQuebraLinha($str){
    $lineEnds = array("\r\n", "\n\r", "\r", "<br>", "<br/>", "&lt;br/&gt;");
    // $str = trim(htmlspecialchars_decode($str));
    $str = str_replace($lineEnds, "\n", $str);	
    $str = str_replace("\n", "", $str);
    return $str;
}

// tira as quebras de linha
function removeBr($str){
    $lineEnds = array("<br />", "<br>", "<br/>", "&lt;br/&gt;");
    $str = trim(htmlspecialchars_decode($str));
    $str = str_replace($lineEnds, "\n", $str);	
    return str_replace("\n", "", $str);
}

// mascara para colocar os pontos nas referencias de Compra Facil
function mascaraReferenciaCompraFacil($ref){
	$ref = strrev($ref);
	$count = 0;
	$retorno = "";

	for($i = 0; $i <= strlen($ref) -1; $i++){		
		if( ($count == 2) && (($i+1) < strlen($ref)) ){
			$retorno = "." . substr($ref, $i, 1) . $retorno;
			$count = 0;
		}
		else{
			$retorno = substr($ref, $i, 1) . $retorno;
			$count++;
		}
	}		
	
	return $retorno;
}

// verifica se ja existem elementos para agrupar elementos na de subficha
function verificaElementosDuplicados($arr, $elemento, $valor){
	if(strtolower($elemento) != 'parcelas'){
		foreach($arr as $a){
			if(isset($a[$elemento]) && $a[$elemento] == $valor){
				return true;
			}
		}
	}
	
	return false;
}

function realocarArray($arr){
	$novaArray = array();

	foreach($arr as $key => $val){
		$primeiroItem = $key;
		break;
	}
	
	foreach($arr as $a){
		$novaArray[$primeiroItem] = $a;
		$primeiroItem++;
	}
	
	return $novaArray;
}

function zeraPricingArray($arr){
	$fields = explode(',','PRECO_UNITARIO,PRECO_CAIXA,PRECO_VISTA,PRECO_DE,PRECO_POR,ECONOMIZE,ENTRADA,PRESTACAO,TOTAL_PRAZO,JUROS_AM,JUROS_AA,PARCELAS');
	
	foreach($fields as $key){
		if(isset($arr[$key])){
			$arr[$key] = '';
		}
	}
	
	return $arr;
}

//função para adicionar digito verificador e mascara
function digitoVerificadorHermes($numero1){
	
	$nume = str_split($numero1);
	$nume = array_reverse($nume);
	$nu = "";
	
	//se tiver fora dos padrões impostos pela Hermes não faz nada e retorna o numero
	if( (!is_numeric($numero1)) || (count($nume) >= 7) || (count($nume) <= 4) ){
		return $numero1;
	}

	foreach ($nume as $key => $num) {
		$n = ($key+2) * $num;
		$nu = $nu + $n;
	}
	
	$resto = $nu % 11;
	$nume = 11 - $resto;
	
	if($nume == 10 || $nume == 11){
		$nume = 0;
	}
	
	$numero2 = $numero1 . $nume;
	
	$numero = str_split($numero2);
	
	//caso tenha 6 numeros
	if (isset($numero[7])) {
		$numero_com_mascara = $numero[0] . $numero[1] . $numero[2] . "." .$numero[3] . $numero[4] . $numero[5] . "." . $numero[6] . $numero[7];
	} //caso tenha 5 numeros
	else if (isset($numero[6])) {
		$numero_com_mascara = $numero[0] . $numero[1] . "." .$numero[2] . $numero[3] . $numero[4] . "." . $numero[5] . $numero[6];
	} //caso tenha 4 numeros, inclui o 0 na frente
	else if (!isset($numero[6])) {
		$numero_com_mascara = '0' . $numero[0] . "." . $numero[1] . $numero[2] . $numero[3] . "." . $numero[4] . $numero[5];
	}
	
	return $numero_com_mascara;
}

// funcao que recebe array com fichas e subfichas e aplica o numero de gupo a cada uma delas
// sempre se baseando na LINHA_EXCEL que a linha da planilha que o truta subiu
// isso serve para fazer toda aquela gorgonza de agrupamentos de referencias com campos e pricing
// ISSO FOI UM TRAUMA MEU AMIGO!
function aplicaGruposHermes($arr){
	$linhaAtual = 1;
	$linhaAnterior = 1;

	for($f = 0; $f <= count($arr)-1; $f++){
		$linhaAtual = $arr[$f]['LINHA_EXCEL'];
		$linhaAnterior = $linhaAtual;

		$countGrupo = 1;
		$arr[$f]['GRUPO'] = $countGrupo;

		if(isset($arr[$f]['SUBFICHAS'])){
			for($sf = 0; $sf <= count($arr[$f]['SUBFICHAS'])-1; $sf++){
				$linhaAtual = $arr[$f]['SUBFICHAS'][$sf]['LINHA_EXCEL'];

				if($linhaAnterior == $linhaAtual){
					$arr[$f]['SUBFICHAS'][$sf]['GRUPO'] = $countGrupo;
				}
				else{
					$countGrupo++;
					$arr[$f]['SUBFICHAS'][$sf]['GRUPO'] = $countGrupo;
				}

				$linhaAnterior = $linhaAtual;
			}
		}
	}

	return $arr;
}

//função para retornar a porcentagem sobre um valor
function calcPorcentagem($valor,$total){
	$porcentagem = ($valor / $total) * 100;
	$porcentagem = number_format($porcentagem,2);
	
	return $porcentagem;
}

//função para verificar qual a praça com maior porcentagem e ordenar por ordem decrescente
function ordenarMaiorPorcentagemDesc($relatorio){
	
	$porcentagens = array();
	foreach ($relatorio as $idPraca => $relat){
		$porcentagens[$idPraca] = $relat['TOTAL_PORCENTAGEM'];
	}
	
	arsort($porcentagens);
	
	$relatorioOrdenado = array();
	foreach ($porcentagens as $idPraca => $por){
		$relatorioOrdenado[$idPraca] = $relatorio[$idPraca];
	}
	
	return $relatorioOrdenado;
}

function dataFormatoBrasil($data,$retira_segundos=false){
	if( !empty($data) ){

		if(strpos($data,'-')){
			$data = explode('-', $data);
			$data1 = explode(' ', $data[2]);

			if($retira_segundos){
				$data1[1] = substr($data1[1], 0,5);
			}

			$data = $data1[0].'/'.$data[1].'/'.$data[0] . ' ' . $data1[1];
		}

	}
	return $data;
}

//função para inserir quebra de linha por uma determinada quantidade de caracteres
function limitarQtoCaracteresPorLinha($str,$limit){
	
	$novaStr = wordwrap($str,$limit,"<br />\n",true);
	
	return $novaStr;
}

// funcao para retirar caracteres especiais do nome do arquivo
function removeCaracteresEspeciaisNomeArquivo($nomeArquivo){
	$arrExtensao = explode('.', $nomeArquivo);
	$retorno = "";
			
	if(count($arrExtensao) > 1){
		for($i = 0; $i <= count($arrExtensao) - 2; $i++){
			$retorno = $retorno . $arrExtensao[$i];
		}
				
		$retorno = removeAcentos($retorno) . "." . $arrExtensao[count($arrExtensao)-1];
		return $retorno;
	}
	
	return $nomeArquivo;
}













