<?php

require_once 'system/application/libraries/XinetKiwi.php';

/**
 * Classe de gerenciamento da sessao
 *
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */

class Sessao {
	/**
	 * indica que nao informado usuario
	 * @var int
	 */
	const LOGIN_VAZIO           = 1;
	/**
	 * indica que a senha esta vazia
	 * @var int
	 */
	const SENHA_VAZIA           = 2;
	/**
	 * indica que o cartao esta vazio
	 * @var int
	 */
	const CARTAO_VAZIO          = 3;
	/**
	 * os valores informados nao permitem o login
	 * @var int
	 */
	const CREDENCIAIS_INVALIDAS = 4;
	/**
	 * indica que a sessao foi encerrada
	 * @var int
	 */ 
	const SESSAO_ENCERRADA       = 5;

	/**
	 * instancia do CodeIgniter
	 * @var CI_Base
	 */
	protected $ci;

	/**
	 * Caminhos que nao precisam de checagem
	 * @var array
	 */
	private $allowed = array(
		'json/admin/getClientesByAgencia',
		'usuario/login',
		'usuario/do_login',
		'usuario/do_logout',
		'usuario/recuperar_senha',
		'usuario/requisitos',
		'usuario/trocar_senha',
		'home/sem_permissao',
		'home/modal_contato',
		'home/modal_about',
		'home/modal_help',
		'plano_midia/espelho',
		'ficha/detalhe',
		'idskiwi/callback',
		'download_plugin/getFile1',
		'importacao_excel/atualiza_xml', // para atualizar o xml pelo plugin
		'importacao_excel/get_pracas', // para pegar a lista de pracas
		'download_plugin/getLayout',
		'download_plugin/getLayoutSize',
		'download_plugin/getLayoutData',
		'paginacao/copiar_paginacao',
		'processos/modal_regra_email',
		'checklist/saveSolicitacao',
		'download_plugin/getPathTemplate', 
		'download_plugin/getPathTemplate',
		'download_plugin/login',
		'download_plugin/geraXmlPrimeiroIndd',
		'paginacao/subfichas',
		'ficha/relatorio_cf',
		'carrinho/criarEmLote',
		'carrinho/proximaEtapaSemItem',
		'objeto/validaArquivos',
		'encartes/index',
		'encartes/preview',
		'encartes/form',
		'encartes/uploadEncarte',
		'encartes/download'
	);

	/**
	 * endereco da pagina de login
	 * @var string
	 */
	private $loginPage = 'usuario/login';

	/**
	 * Instancia da sessao
	 * @var Sessao
	 */
	private static $instance;

	/**
	 * Construtor
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Sessao
	 */
	function __construct(){
		$this->ci = &get_instance();
		session_start();
		self::$instance = $this;
	}

	/**
	 * Seta um dado na sessao
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $key
	 * @param mixed $val
	 * @return void
	 */
	public static function set($key, $val){
		$_SESSION[ $key ] = $val;
	}

	/**
	 * Recupera um dado da sessao
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $key
	 * @return mixed
	 */
	public static function get($key){
		if(isset($_SESSION[ $key ])){
			return $_SESSION[ $key ];
		}
		return null;
	}

	/**
	 * Limpa um dado da sessao
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $key nome da chave
	 * @return void
	 */
	public static function clear($key){
		unset($_SESSION[ $key ]);
	}

	/**
	 * Limpa a sessao
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return unknown_type
	 */
	public static function clearAll(){
		$_SESSION = array();
	}

	/**
	 * Efetua o login do usuario
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $user Nome de usuario
	 * @param string $pass Senha
	 * @param string $coordenada Coordenada do cartao
	 * @return void
	 */
	public function login($user, $pass, $coordenada='',$valor_informado = ''){
		$CI =& get_instance();
		if( empty($user) || empty($pass) || empty($coordenada) || empty($valor_informado) ){
			$this->logout(self::CREDENCIAIS_INVALIDAS);
		}

		// recupera o usuario
		$usuario = $CI->usuario->getByLogin($user);
		
		$usuario['IS_HERMES'] = 0;
		$usuario['IS_CLIENTE_HERMES'] = 0;
		$usuario['IS_CLIENTE_COMPRAFACIL'] = 0;
		$usuario['IS_CLIENTE_PAODEACUCAR'] = 0;
		
		
		if(strtolower($usuario['DESC_AGENCIA']) == 'hermes' ||
			strtolower($usuario['DESC_CLIENTE']) == 'comprafacil' || 
			strtolower($usuario['DESC_AGENCIA']) == 'comprafacil' ||
			strtolower($usuario['DESC_CLIENTE']) == 'hermes'
		){
			$usuario['IS_HERMES'] = 1;
		}
		
		if(strtolower($usuario['DESC_CLIENTE']) == 'hermes'){
			$usuario['IS_CLIENTE_HERMES'] = 1;
		}
		
		if(strtolower($usuario['DESC_CLIENTE']) == 'comprafacil'){
			$usuario['IS_CLIENTE_COMPRAFACIL'] = 1;
		}
		
		if(strtolower($usuario['DESC_CLIENTE']) == 'grupo pa'){
			$usuario['IS_CLIENTE_PAODEACUCAR'] = 1;
		}
		

		// se nao encontrou ou esta inativo
		if(!empty($usuario) && $usuario['STATUS_USUARIO']==1){

			// forca zerar os itens do menu na sessao ao efetuar o login
			self::set('_itensMenu', array());

			// recupera o adaptador de login
			$adapter = $this->getLoginAdaptor();

			// tenta efetuar o login
			// os dados retornados sao o do usuario
			// no sistema de controle de usuarios
			$res = $adapter->login($user, $pass, $coordenada, $valor_informado);

			// se passou no login (tem dados)
			if( !empty($res) ){

				// tem que trocar a senha ou os dias chegaram no limite
				if( $res['trocarSenha'] == 1 || $adapter->diasParaExpiracao($user) <= 0 ){

					// marca o usuario para trocar a senha
					$usuario['trocarSenha'] = 1;
				}

				// seta os dados do usuario
				self::set('usuario',$usuario);

				// redireciona para a home
				redirect('home');
			}
		}

		$this->logout(self::CREDENCIAIS_INVALIDAS);
	}

	/**
	 * retorna a quantidade de dias que faltam para a senha expirar
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $login Login do usuario
	 * @return int quantidade de dias que faltam
	 */
	public function diasParaExpiracao($login){
		return $this->getLoginAdaptor()->diasParaExpiracao($login);
	}

	/**
	 * Efetua a troca de senha do usuario
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $login
	 * @param string $senhaAntiga
	 * @param string $senhaNova
	 * @return boolean
	 */
	public function trocarSenha($login, $senhaAntiga, $senhaNova){
		return $this->getLoginAdaptor()->trocarSenha($login, $senhaAntiga, $senhaNova);
	}

	/**
	 * Reseta a senha do usuario
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param unknown_type $login
	 * @return
	 */
	public function resetarSenha($login){
		return $this->getLoginAdaptor()->resetarSenha($login);
	}

	/**
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function logout($code = -1){
		self::clearAll();
		redirect('usuario/login/'.$code);
	}

	/**
	 * Caso o usuario nao tenha permissao é direcionado para uma pagina padrao
	 * @author juliano.polito
	 * @return unknown_type
	 */
	public function semPermissao($controller,$method){
		Sessao::set('__controller',$controller);
		Sessao::set('__method',$method);
		redirect('home/sem_permissao');
	}

	/**
	 * Verifica se o usuario tem acesso ao recurso acessado
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function checaSessao(){
		$isJson = false;
		$controller = $this->ci->uri->segment(1);
		$method = $this->ci->uri->segment(2);

		if($controller == 'json'){
			$isJson = true;
			$controller = $this->ci->uri->segment(2);
			$method = $this->ci->uri->segment(3);
		}

		if($method == ''){
			$method = 'index';
		}

		// monta a url para checagem
		$url = $isJson ? 'json/'.$controller.'/'.$method : $controller . '/' . $method;

		self::set('__controller_atual', $controller);
		self::set('__method_atual', $method);

		$this->limpaDadosBusca();
		$this->ci->data['_sessao'] = self::get('usuario');
		$this->ci->data['menu'] = $this->getItensMenu();

		//IP Usado pelo cliente para acessar o sistema
		//Info usada no rodape de todas as paginas
		$ip = $_SERVER['REMOTE_ADDR'];
		$this->ci->data['link247'] = IPUtil::is247($ip);
		$this->ci->data['linkIP'] = $ip;

		// FIXME - por enquando nao checamos JSON
		if( ! $isJson ){

			// se nao tiver permissao
			// faz o logout
			if( ! self::hasPermission($controller,$method)
				&& !in_array("$controller/$method",$this->allowed)){
				$user = self::get('usuario');
				if(isset($user)){
					$this->semPermissao($controller,$method);
				}else{
					$this->logout();
				}
			}
		}
		
		XinetKiwi::getInstance()->setXinetAddress(XINET_KIWI_IP);
		XinetKiwi::getInstance()->setXinetPort(XINET_KIWI_PORT);
		
		$user = self::get('usuario');
		if( !empty($user) ){
			XinetKiwi::getInstance()->setUsername( $user['LOGIN_USUARIO'] );
		}
		
		XinetKiwi::getInstance()->setSystem('retail');

		$this->checaTrocarSenha();

	}

	/**
	 * recupera as mensagens para troca de senha do usuario
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return array
	 */
	public function getMensagensTroca(){
		$user = self::get('usuario');
		return $this->getLoginAdaptor()->getMensagensTrocaSenha($user['LOGIN_USUARIO']);
	}

	/**
	 * checa se o usuario logado tem que trocar a senha
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	protected function checaTrocarSenha(){
		$user = self::get('usuario');

		$controller = $this->ci->uri->segment(1);
		$method = $this->ci->uri->segment(2);

		// URL's permitidas
		$allowed = array(
			'usuario/trocar_senha',
			'usuario/do_logout',
		);

		if( !empty($user['trocarSenha']) && !in_array("$controller/$method",$allowed) ){
			redirect('usuario/trocar_senha');
		}
	}

	/**
	 * Limpa os dados da busca quando mudar de controller
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	protected function limpaDadosBusca(){

		//metodos anterior/atual
		$m_atual = self::get('__method_atual');

		// pega a url anterior
		$anterior = self::get('__controller_anterior');
		$atual = self::get('__controller_atual');

		if( $anterior != $atual && get_instance()->uri->segment(1) != 'json' ){
			self::set('__controller_anterior', $atual);
			self::set('__method_anterior', $m_atual);
			self::clear('busca');

			$this->ci->session->unset_userdata('BUSCA');
			$this->ci->session->unset_userdata('__BUSCA');
		}

	}

	/**
	 * Verifica se o usuario logado tem permissao de acessar uma funcao
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $controller Nome da controller
	 * @param string $method Nome do metodo
	 * @return boolean falso se nao tiver permissao, do contrario true
	 */
	public static function hasPermission($controller,$method){
		$ci = self::getInstance()->ci;
		$user = self::get('usuario');

		if(empty($user['ID_USUARIO'])){
			return false;
		}

		$idusuario = $user['ID_USUARIO'];

		return $ci->usuario->checaPermissao($idusuario, $controller,$method);
	}

	/**
	 * Retorna a instancia da sessao
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Sessao
	 */
	public static function getInstance(){
		return self::$instance;
	}

	/**
	 * Recupera os itens do menu do usuario
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return array
	 */
	public static function getItensMenu(){
		$user = self::get('usuario');
		// pega os itens da sessao
		$itens = self::get('_itensMenu');
		// se ainda nao gravou na sessao
		if(empty($itens)){
			$itens = self::getInstance()->ci->itemMenu->getItemsByUsuario($user['ID_USUARIO']);
			self::set('_itensMenu', $itens);
		}

		return $itens;
	}

	/**
	 * Recupera o adaptador de login
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return ILoginAdaptor
	 */
	private function getLoginAdaptor(){
		// inclui a fabrica de logins
		require_once APPPATH . 'libraries/adaptadores/LoginAdaptorFactory.php';

		// pega o adaptador
		$adapter = LoginAdaptorFactory::getInstance()
			->load('Default');

		// retorna o adaptador
		return $adapter;
	}
	
	/**
	 * Efetua o login do usuario
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $user Nome de usuario
	 * @param string $pass Senha
	 * @param string $coordenada Coordenada do cartao
	 * @return boolean
	 */
	public function login_plugin($user, $pass, $coordenada='',$valor_informado = ''){
		$CI =& get_instance();
		
		$usuario = $CI->usuario->getByLogin($user);

		if( empty($user) || empty($pass) || empty($coordenada) || empty($valor_informado) ){
			return false;
		}

		// recupera o adaptador de login
		$adapter = $this->getLoginAdaptor();

		// tenta efetuar o login
		// os dados retornados sao o do usuario
		// no sistema de controle de usuarios
		$res = $adapter->login($user, $pass, $coordenada, $valor_informado);

		// se passou no login (tem dados)
		if( !empty($res) ){
			return $usuario['ID_USUARIO'];
		}

		return false;
	}

}

/**
 * Altera o nome da sessao quando vier por flash
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 * @return void
 */
function _setSessionId(){
	if(!empty($_POST['_flashSessionID'])){
		session_id($_POST['_flashSessionID']);
	}

}

