<?php
//require_once(APPPATH.'libraries/LogUtil'.EXT);
require_once(APPPATH.'hooks/Sessao'.EXT);
class LogSQL{
	
	private static $conn;
	private static $database = "logsd";
	private static $excludeTables = array();
	
	
	/**
	 * Grava um registro de log de cada requisicao feita para as controlers
	 * Dados gravados no log:
	 * 		id do usuario
	 * 		login do usuario
	 * 		user agent da requisicao
	 * 		uri
	 * 		dados do cookie, get e post e objeto request
	 * 		timestamp
	 * 		
	 */
	public static function log($sql){
		
		include(APPPATH.'config/database'.EXT);
		
		$cfg = $db[$active_group];
		
		$sql = trim($sql);
		
		//remove backticks
		$sql = str_replace("`","",$sql);
		
		//recupera a instancia do objeto codeigniter atual
		$CI =& get_instance();
		
		$user = Sessao::get('usuario');
		
		//caso o usuario esteja logado
		if(isset($user['ID_USUARIO'])){
			
			$regex = "@^(DELETE|INSERT|UPDATE|REPLACE)\s*(FROM|INTO)?\s*(\w+)@i";
			$isValid = preg_match($regex,$sql,$reg);

			if($isValid && !self::isExcludeTable($reg)){

				//recupera o id do usuario da sessao
				$idUsuario = $user['ID_USUARIO'];
				
				//recupera o login do usuario
				$login = $user['LOGIN_USUARIO'];
				
				//recupera o IP
				$ip = (isset($_SERVER['X_FORWARDED_FOR'])) ? $_SERVER['X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
				
				$serverName = $_SERVER["SERVER_NAME"];
				
				//recupera o user agent requisitando
				$userAgent = $_SERVER["HTTP_USER_AGENT"];
				
				//recupera a URI completa
				$uri = $_SERVER["REQUEST_URI"];
				
				if(!isset(self::$conn)){
					//conecta ao servidor do banco
					self::$conn = mysql_connect($cfg["hostname"],$cfg['username'],$cfg['password'],true);
				}
				
				$conn =& self::$conn;
				
				//seleciona banco
				mysql_select_db(self::$database,$conn);
				
				//preparamos a query para insercao do log no banco
				$query = "INSERT INTO TB_LOG_SQL (ID_USUARIO, LOGIN, SERVER_NAME, IP, USER_AGENT, URI, `SQL`)
							VALUES ('%d','%s','%s','%s','%s','%s','%s')";
				
				$query = sprintf($query, mysql_escape_string($idUsuario), 
											mysql_escape_string($login), 
											mysql_escape_string($serverName), 
											mysql_escape_string($ip), 
											mysql_escape_string($userAgent), 
											mysql_escape_string($uri), 
											mysql_escape_string($sql));
				
						
											
				//Insere um registro de log no banco de dados
				@mysql_query($query,$conn);
			}
		}
		
	}
	
	
	/**
	 * Recebe um array de nomes e verifica se este nome esta no array excludeTables
	 * @param $tables
	 * @return unknown_type
	 */
	private static function isExcludeTable($tables){
		foreach ($tables as $table) {
			foreach (self::$excludeTables as $exclude) {
				if(strtoupper($table) == strtoupper($exclude)){
					return true;
				}
			};
		}
		return false;
	}
}
























