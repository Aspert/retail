<?php
require_once(APPPATH.'libraries/LogUtil'.EXT);
//require_once(APPPATH.'hooks/Sessao'.EXT);

class LogAcoes{
	
	private static $conn;
	private static $database = "logsd";
	private static $excludePaths = array();
	
	/**
	 * Grava um registro de log de cada requisicao feita para as controlers
	 * Dados gravados no log:
	 * 		id do usuario
	 * 		login do usuario
	 * 		user agent da requisicao
	 * 		uri
	 * 		dados do cookie, get e post e objeto request
	 * 		timestamp
	 * 		
	 */
	public function log(){
		
		//recupera a URI completa
		$uri = $_SERVER["REQUEST_URI"];
		
		//caso seja um path excluido cai fora da funcao
		if(self::isExcludedPath($uri)){
			return;
		}
		
		include(APPPATH.'config/database'.EXT);
		
		$cfg = $db[$active_group];
		
		//recupera a instancia do objeto codeigniter atual
		$CI =& get_instance();
		
		$user = Sessao::get('usuario');
		
		//LogUtil::log("retail_log.txt",print_r($user,true));
		
		//caso o usuario esteja logado
		if(isset($user['ID_USUARIO'])){
			
			//recupera o id do usuario da sessao
			$idUsuario = $user['ID_USUARIO'];
			
			//recupera o login do usuario
			$login = $user['LOGIN_USUARIO'];
			
			$serverName = $_SERVER["SERVER_NAME"];
			
			//recupera o IP
			$ip = (isset($_SERVER['X_FORWARDED_FOR'])) ? $_SERVER['X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
			
			//recupera o user agent requisitando
			$userAgent = $_SERVER["HTTP_USER_AGENT"];
			
			//recupera uma string representando o cookie
			$cookie = print_r($_COOKIE,true);
			//recupera uma string representando o get
			$get = print_r($_GET,true);
			//recupera uma string representando o post
			$post = print_r($_POST,true);
			//recupera uma string representando o request
			$request = print_r($_REQUEST,true);
			
			if(!isset(self::$conn)){
				//conecta ao servidor do banco
				self::$conn = @mysql_connect($cfg["hostname"],$cfg['username'],$cfg['password'],true);
			}
			
			$conn =& self::$conn;
			
			//seleciona banco
			mysql_select_db(self::$database,$conn);
			
			//preparamos a query para insercao do log no banco
			$query = "INSERT INTO TB_LOG_ACOES (ID_USUARIO, LOGIN, SERVER_NAME, IP, USER_AGENT, URI, COOKIE, GET, POST, REQUEST)
						VALUES ('%d','%s','%s','%s','%s','%s','%s','%s','%s','%s')";
			
			$query = sprintf($query, mysql_real_escape_string($idUsuario), 
										mysql_real_escape_string($login), 
										mysql_real_escape_string($serverName), 
										mysql_real_escape_string($ip), 
										mysql_real_escape_string($userAgent), 
										mysql_real_escape_string($uri), 
										mysql_real_escape_string($cookie), 
										mysql_real_escape_string($get), 
										mysql_real_escape_string($post), 
										mysql_real_escape_string($request));
			
			//Insere um registro de log no banco de dados
			@mysql_query($query,$conn);
		}
		
		
	}
	
	/**
	 * Recebe um array de nomes e verifica se este nome esta no array excludePaths
	 * @param $tables
	 * @return unknown_type
	 */
	private static function isExcludedPath($path){
		foreach (self::$excludePaths as $exclude) {
			$pos = stripos($path,$exclude);
			if( $pos !== false ){
				return true;
			}
		};
		return false;
	}
}
























