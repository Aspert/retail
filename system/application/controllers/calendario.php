<?php
/**
 * Controller para gerenciar os cadastros de processos
 *
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class Calendario extends MY_Controller {

	/**
	 * Construtor
	 *
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @return Calendario
	 */
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/calendario/';
	}

	/**
	 * Lista os calendario cadastrados
	 *
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @param int $pagina Numero da pagina atual
	 * @return void
	 */
	public function lista($pagina = 0) {	
		if ( !empty($_POST) )  {
			Sessao::set('busca',$_POST);
		}

		$busca = (array) Sessao::get('busca');
		$msg = Sessao::get('_mensagem');
		Sessao::clear('_mensagem');

		$this->loadFilters( $busca );

		$limit = val($busca,'pagina',5);
		$result = $this->calendario->listItems($busca, $pagina, $limit, 'NOME_CALENDARIO','ASC');

		$this->assign('podeAlterar', Sessao::hasPermission('calendario', 'save'));
		$this->assign('busca', $busca);
		$this->assign('lista', $result['data']);
		$this->assign('paginacao', linkpagination($result, $limit));
		$this->assign('msg', $msg);

		$this->display('index');
	}

	/**
	 * Exibe o formulario de edicao
	 *
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do calendario quando editando
	 * @return void
	 */
	public function form($id=null) {
		$feriados = array();
			
		if( ($id != null) && ($_SERVER['REQUEST_METHOD'] == 'GET')) {
			$_POST = $this->calendario->getById($id);
			$feriados = $this->calendario->getFeriadosByIdCalendario($id);			
		}
		
		$this->assign('feriados', $feriados);
		$this->display('form');
	}

	/**
	 * Persiste os dados de um fabricante
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do fabricante
	 * @return void
	 */
	public function save($id=null) {
		$_POST['ID_CALENDARIO'] = $id;
		$erros = $this->calendario->validate($_POST);
		
		if(empty($erros)){
			$id = $this->calendario->save($_POST, $id);

			//redirect('calendario/lista');
			
			// Apaga os feriados
			if( ($id != null) && (is_numeric($id))){
				$this->calendario->deletaFeriados($_POST['ID_CALENDARIO'], $_POST['ID_CALENDARIO_FERIADO']);
			}
			
			// salva os feriados do calendario
			if(count($_POST['ID_CALENDARIO_FERIADO']) > 0){
				for($i = 0; $i <= count($_POST['ID_CALENDARIO_FERIADO']) - 1; $i++){				
					$feriado = array(
								'ID_CALENDARIO_FERIADO' => $_POST['ID_CALENDARIO_FERIADO'][$i],
								'NOME_FERIADO' => $_POST['NOME_FERIADO'][$i], 
								'DATA_FERIADO' => format_date_to_db($_POST['DATA_FERIADO'][$i], 4), 
								'FLAG_TODOS_ANOS' => $_POST['FLAG_TODOS_ANOS'][$i]
					);
					
					$this->calendario->saveFeriado($id, $feriado);
				}
			}
			
			redirect('calendario/lista');
		}

		$_REQUEST['erros'] = $erros;
		$this->assign('erros', $erros);
		$this->form($id);
	}

	/**
	 * Altera o status do processo
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id codigo do processo
	 * @return void
	 */
	public function alterar_status($id=null) {
		$pro = $this->processo->getById($id);

		if( !empty($pro) ){

			// flag para indicar se pode mudar
			$podeAlterar = true;

			// nova flag
			$pro['STATUS_PROCESSO'] = empty($pro['STATUS_PROCESSO']) ? 1 : 0;

			// se esta ativando
			if( $pro['STATUS_PROCESSO'] == 1 ){
				// vamos pegar processos
				// com o mesmo tipo de peca
				$filters = array();
				$filters['ID_TIPO_PECA'] = $pro['ID_TIPO_PECA'];
				$filters['STATUS_PROCESSO'] = 1;

				$list = $this->processo->listItems($filters);

				// para cada resultado encontrado
				if( !empty($list['data']) ){
					foreach( $list['data'] as $item ){
						// se encontrou outro processo que nao e o mesmo
						if( $item['ID_PROCESSO'] != $pro['ID_PROCESSO'] ){
							// nao pode alterar
							Sessao::set('_mensagem', 'Você não pode ter mais de um processo ativo para o mesmo tipo de peça');
							$podeAlterar = false;
							break;
						}
					}
				}
			}

			if( $podeAlterar ){
				$this->processo->save($pro, $id);
			}
		}

		header("Location: " . $_SERVER['HTTP_REFERER']);
		exit;
	}

	/**
	 * Modal com as regras de email do processo
	 *
	 * @author Esdras Eduardo
	 * @link http://www.247id.com.br
	 * @param int $idProcessoEtapa codigo do processo de etapa
	 * @return void
	 */
	public function modal_regra_email( $idProcessoEtapa ) {
		$this->loadFiltersRegra($_POST);
		$this->assign('idProcessoEtapa', $idProcessoEtapa);
		$this->assign('grupos_selecionados', $this->regraEmailProcessoEtapaUsuario->getGruposByRegra($idProcessoEtapa));
		$this->assign('regras', $this->regraEmailProcessoEtapa->findAll($idProcessoEtapa));
		$this->display('modal_regra_email');
	}
	
	/**
	 * Carrega os filtros padroes
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	protected function loadFilters( &$data = array() ){
		$user = Sessao::get('usuario');

		$this->assign('clientes',array());
		$this->assign('tiposPeca',array());

		if( !empty($user['ID_AGENCIA']) ){
			$list = $this->cliente->getByAgencia($user['ID_AGENCIA']);
			$this->assign('clientes', $list);

			foreach( $list as $item ) {
				$data['CLIENTES'][] = $item['ID_CLIENTE'];
			}

		} else if(!empty($user['ID_CLIENTE'])) {
			$data['ID_CLIENTE'] = $user['ID_CLIENTE'];

		}

		if( !empty($data['ID_CLIENTE']) ){
			$this->assign('tiposPeca', $this->tipo_peca->getTiposAtivos($data['ID_CLIENTE']));
		}
	}

	/**
	 * Verifica se o usuario pode alterar o processo
	 *
	 * usado somente na
	 *
	 * @see system/application/libraries/MY_Controller::checaPermissaoAlterar()
	 */
	protected function checaPermissaoAlterar($data){
		$filters = array();
		$filters['ID_PROCESSO'] = sprintf('%d', $data);

		$res = $this->processo->listItems($filters);

		// se encontrou
		if( !empty($res['data']) ){
			// pega o primeiro registro
			$first = reset($res['data']);

			// se houver jobs
			if( $first['QTD_JOB'] > 0 ){
				// nao pode editar, entao redirecionamos
				redirect('processos/lista');
			}
		}
	}
	
	/**
	 * Popular combo box de filtro
	 * @author esdras.filho
	 * @param $data
	 */
	private function loadFiltersRegra($data){
		$this->assign('clientes', array());
		$this->assign('agencias', array());
		$this->assign('grupos',   array());
		
		// recupera as agencias
		$rs = $this->agencia->listItems(array('STATUS_AGENCIA'=>1),0,100000);
		$this->assign('agencias', $rs['data']);
		
		// recupera os clientes
		$rs = $this->cliente->listItems(array('STATUS_CLIENTE'=>1),0,100000);
		$this->assign('clientes', $rs['data']);
		
		
		if(!empty($data['ID_AGENCIA'])){
			$this->assign('grupos', $this->grupo->getByAgencia($data['ID_AGENCIA']));
		}
		
		if(!empty($data['ID_CLIENTE'])){
			$this->assign('grupos', $this->grupo->getByCliente($data['ID_CLIENTE']));
		}
	}	
}


