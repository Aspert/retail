<?php
/**
 * Controller para gerenciar clientes
 * @author Hugo Silva
 *
 */
class Cliente extends MY_Controller {
	/**
	 * Construtor
	 * @author Hugo Silva
	 * @return Cliente
	 */
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/cliente/';
	}
	
	/**
	 * Metodo inicial da controller
	 * Acessado quando nao especificado um metodo
	 * @author Hugo Silva
	 * @return void
	 */
	public function index(){
		$this->lista();
	}
	
	/**
	 * Lista os itens cadastrados
	 * 
	 * @author Hugo Silva
	 * @param int $pagina Pagina inicial da listagem
	 * @return void
	 */
	public function lista($pagina=0){
		if (!empty($_POST)) {
			$dados = $_POST;
			$this->session->set_userdata('__BUSCA', $dados);			
		}
		
		$data = $this->session->userdata('__BUSCA');
		if(!is_array($data)){
			$data = array($data);
		}
		
		$limit = !empty($data['pagina']) ? $data['pagina'] : 5;
		$lista = $this->cliente->listItems($data, $pagina, $limit);
		
		$this->data['lista']      = $lista['data'];
		$this->data['paginacao'] = linkpagination($lista, $limit);
		$this->data['busca'] = $data;
		$this->data['podeAlterar'] = Sessao::hasPermission('cliente','save');
		$this->display('index');		
	}
	
	/**
	 * Exibe o formulario para edicao ou insercao de clientes
	 * 
	 * @author Hugo Silva
	 * @param int $id codigo do cliente
	 * @return void
	 */
	public function form($id=null){
		if(!is_null($id) && $_SERVER['REQUEST_METHOD'] == 'GET'){
			$_POST = $this->cliente->getById($id);
			
			$this->data['categoriaLote'] = "";
			if(is_numeric($_POST['SUBCATEGORIA_LOTE']) && $_POST['SUBCATEGORIA_LOTE'] != 0){
				$subcategoriaLote = $this->subcategoria->getById($_POST['SUBCATEGORIA_LOTE']);
				$this->data['categoriaLote'] = $subcategoriaLote['ID_CATEGORIA'];
				$this->data['subcategorias'] = $this->subcategoria->getByCategoria($subcategoriaLote['ID_CATEGORIA']);
			}
			
			$this->data['categorias'] = $this->categoria->getByCliente($_POST['ID_CLIENTE']);
		}

		$transf = $this->transformador->listItems(array(), 0, 10000);
		$agencias = $this->cliente->getAllAgenciasByIdCliente($id);
		
		$this->data['logo'] = getLogo('img/cliente', $id);
		$this->data['agencias']  = $agencias;
		$this->data['transformadores']  = $transf['data'];
		
		//$this->data['categorias'] = $this->categoria->getByCliente($data['ID_CLIENTE']);
		//$this->data['subcategorias'] = $this->categoria->getSubcategorias($data['CATEGORIA']);
		
		$this->data['ID_CLIENTE'] = $id;
		$this->display('form');
	}
	
	/**
	 * Grava os dados do formulario
	 * 
	 * @author Hugo Silva
	 * @param int $id
	 * @return void
	 */
	public function save($id=null){
		$_POST['ID_CLIENTE'] = $id;
		
		if( empty($id) ){
			$_POST['CHAVE_STORAGE_CLIENTE'] = ajustaChaveStorage($_POST['DESC_CLIENTE']);
		}
		
		if( empty($_POST['ID_TRANSFORMADOR']) ){
			$_POST['ID_TRANSFORMADOR'] = null;
		}
		
		//Verifica o que foi informado no campo QTD_DIAS_INATIVACAO_FICHA
		if ( !empty($_POST['QTD_DIAS_INATIVACAO_FICHA']) ) {
			if (!is_numeric($_POST['QTD_DIAS_INATIVACAO_FICHA'])) {
				$_POST['QTD_DIAS_INATIVACAO_FICHA'] = null;
			}
		}
		
		$erros = $this->cliente->validate($_POST);

		if( isset($_FILES['logotipo']) && is_uploaded_file($_FILES['logotipo']['tmp_name']) ){
			$temp = 'files/' . $_FILES['logotipo']['name'];
			copy($_FILES['logotipo']['tmp_name'], $temp);
			$res = validaLogotipo($temp);
			unlink($temp);
			
			if( !$res ){
				$erros[] = 'Arquivo de logotipo inválido';
			}
		}
		
		if(empty($erros)){
			$id = $this->cliente->save($_POST,$id);
			$this->tipo_objeto->criarTiposPadrao($id);
			
			$this->fornecedor_cliente->salvarAgencias($id, $_POST['ID_AGENCIA']);
			$this->xinet->createPath($this->cliente->getPath($id));
			moveFile('logotipo', 'img/cliente' , $id);
			redirect('cliente/lista');
		}
		
		$this->data['erros'] = $erros;
		$this->form($id);
	}
	
}