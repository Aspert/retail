<?php

/**
 * Controller de pricing
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 */
class Pricing extends MY_Controller{
	
	/**
	 * Construtor
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Pricing
	 */
	public function __construct(){
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/pricing/';
	}
	
	/**
	 * Lista os jobs cadastrados
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $pg
	 * @return void
	 */
	public function index($pg = 0){
		// se enviou dados
		if(!empty($_POST)){
			// altera a sessao
			Sessao::set('busca', $_POST);
		}
		
		$dados = (array) Sessao::get('busca');
		$dados['STATUS_CAMPANHA'] = 1;
		
		$this->setSessionData( $dados );
		
		// somente os desta etapa
		$dados['ID_ETAPA'] = $this->etapa->getIdByKey('PRICING');
		$dados['PRODUTOS'] = getValoresChave(getProdutosUsuario(),'ID_PRODUTO');
		
		if(empty($dados['ORDER'])){
			$dados['ORDER'] = 'DATA_INICIO';
		}
		if(empty($dados['ORDER_DIRECTION'])){
			$dados['ORDER_DIRECTION'] = 'DESC';
		}
		
		//Retira os bloqueados
		$dados['NOT_ID_STATUS'] = array ($this->status->getIdByKey('BLOQUEADO'));
		
		
		$limit = empty($dados['pagina']) ? 0 : $dados['pagina'];
		$lista = $this->job->listItems($dados, $pg, $limit, $dados['ORDER'], $dados['ORDER_DIRECTION']);
		
		$this->loadFilters($dados);
		
		$this->data['podeAlterar'] = Sessao::hasPermission('pricing','save');
		$this->data['busca'] = $dados;
		$this->data['lista'] = $lista['data'];
		$this->data['paginacao'] = linkpagination($lista, $limit);
		
		$this->display('index');
	}
	
	/**
	 * Exibe o formulario de edicao do pricing
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id
	 * @return void
	 */
	public function form($id = null, $idpraca = null){
		$this->checaPermissaoAlterar($id);
		if( !is_null($idpraca) ){
			$this->assign('praca', $this->praca->getById($idpraca));
			$this->assign('totalProdutos', $this->excel->countItensForJobPraca($id,$idpraca));
		}
		
		$pracas = $this->job->getPracas($id);
		
		$this->assign('podePassarEtapa', Sessao::hasPermission('pricing','proxima_etapa'));
		$this->assign('podeVoltarEtapa', Sessao::hasPermission('pricing','voltar_etapa'));
		$this->assign('pracas', $pracas);
		$this->assign('job', $this->job->getById($id));
		$this->display('form');
		
	}
	
	/**
	 * Salva as alteracoes do pricing
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id
	 * @return void
	 */
	public function save($id = null){
		$this->checaPermissaoAlterar($id);
		// se foi enviado por post
		if( !empty($_POST['item']) ){
			
			// coloca o job em andamento novamente
			$this->job->setStatus($id, $this->status->getIdByKey('EM_APROVACAO'));
			
			$this->pricing->savePricing( $_POST['item'] );
			
		}
		
		$this->form($id);
	}
	
	/**
	 * Envia um job para proxima etapa
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do job
	 * @return void
	 */
	public function proxima_etapa($id){
		
		$errosMudaEtapa = array();
		
		$this->checaPermissaoAlterar($id);
		
		$this->save($id);
		
		$errosMudaEtapa = $this->job->mudaEtapa($id, Sessao::get('usuario'));
		
		if(!empty($errosMudaEtapa)){
			$this->assign('erros', $errosMudaEtapa );
			$this->form($id);
		}else{
			redirect('pricing/index');
		}
	}
	
	/**
	 * carrega os filtros padrao
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data Dados para indicar quais filtros usar
	 * @return void
	 */
	protected function loadFilters($data = null){
		
		$user = Sessao::get('usuario');
		
		if(!empty($user['ID_PRODUTO']) ){
			$data['ID_PRODUTO'] = $user['ID_PRODUTO'];
		}
		
		if(!empty($user['ID_AGENCIA']) ){
			$data['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}
		
		if(!empty($user['ID_CLIENTE']) ){
			$data['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}
		
		$tipos = $this->tipoPeca->getAll(0,1000,'DESC_TIPO_PECA');
		
		if(!empty($data['ID_AGENCIA'])){
			$this->assign('clientes', $this->cliente->getByAgencia($data['ID_AGENCIA']));
		} else {
			$this->assign('clientes',array());
		}
		
		if(!empty($data['ID_CLIENTE'])){
			$this->assign('agencias', $this->agencia->getByCliente($data['ID_CLIENTE']));
		} else {
			$this->assign('agencias',array());
		}
		
		if(!empty($data['ID_PRODUTO'])){
			$filters = array(
				'ID_PRODUTO' => $data['ID_PRODUTO'],
				'STATUS_CAMPANHA' => 1
			);
			$campanha = $this->campanha->listItems($filters,0,1000);
			$this->data['campanhas'] = $campanha['data'];
			
		} else {
			$this->data['campanhas'] = array();
		}
		
		$this->assign('produtos',array());
		if( !empty($data['ID_CLIENTE']) ){
			$this->assign('produtos', getProdutosUsuario(val($data,'ID_CLIENTE'), val($data,'ID_AGENCIA')));
		}
		
		$this->data['tipos'] = $tipos['data'];
	}

	/**
	 * Checa se o usuario logado tem permissao ou nao de alterar o job acessado
	 * 
	 * Se nao tiver permissao, redireciona para a listagem
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idjob
	 * @return void
	 */
	protected function checaPermissaoAlterar($idjob){
		
		$user = Sessao::get('usuario');
		$job = $this->job->getById($idjob);
		$produtos = getValoresChave(getProdutosUsuario($job['ID_CLIENTE'],$job['ID_AGENCIA']),'ID_PRODUTO');
		$goto = 'pricing/index';
		
		//Verifica se o job nao esta bloqueado
		if ( $job['ID_STATUS'] == $this->status->getIdByKey('BLOQUEADO') ) {
			redirect($goto);
		}
		
		//Verifica se esta na etapa correta
		if( $job['ID_ETAPA'] != $this->etapa->getIdByKey('PRICING') ){
			redirect($goto);
		}
		
		//Verifica se o cliente do usuario eh o mesmo cliente do job
		if(!empty($user['ID_CLIENTE'])){
			if($job['ID_CLIENTE'] != $user['ID_CLIENTE']){
				redirect($goto);
			}
			
			//Verifica se o produto do job esta na lista permitida para o cliente x agencia
			if(!in_array($job['ID_PRODUTO'],$produtos)){
				redirect($goto);
			}
		}
		
		//Verifica se a agencia do usuario eh a mesma agencia do job
		if(!empty($user['ID_AGENCIA'])){
			if($user['ID_AGENCIA'] != $job['ID_AGENCIA']){
				redirect($goto);
			}
			
			$clientes = getValoresChave($this->cliente->getByAgencia($user['ID_AGENCIA']),'ID_CLIENTE');
			if(!in_array($job['ID_PRODUTO'],$produtos)){
				redirect($goto);
			}
			
			if(!in_array($job['ID_CLIENTE'],$clientes)){
				redirect($goto);
			}
		}
	}
	
}
