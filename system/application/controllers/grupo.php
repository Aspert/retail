<?php
/**
 * controller para gerenciar os grupos
 * 
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class Grupo extends MY_Controller
{
	/**
	 * Construtor
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Grupo
	 */
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/grupo/';
	}
	
	/**
	 * Lista os grupos cadastrados
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $pagina Pagina atual de listagem
	 * @return void
	 */
	function lista($pagina=0) {
		
		if(!empty($_POST)){
			Sessao::set('busca', $_POST);
		}
		
		$dados = Sessao::get('busca');
		
		if(!is_array($dados)){
			$dados = array();
		}
		
		$limit = empty($dados['pagina']) ? 0 : $dados['pagina'];
		if ((isset($dados['STATUS_GRUPO'])) && ($dados['STATUS_GRUPO'] == 2)){
			unset($dados['STATUS_GRUPO']);
		}
		$grupos = $this->grupo->listItems($dados, $pagina, $limit);
		
		$this->data['podeAlterar'] = Sessao::hasPermission('grupo','save');
		$this->data['busca'] = $dados;
		$this->data['lista'] = $grupos['data'];
		$this->data['paginacao'] = linkpagination($grupos, $limit);
		
		$this->display('index');	
	}
	
	/**
	 * Exibe o formulario de edicao do grupo
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do grupo quando estiver editando
	 * @return void
	 */
	function form($id=null) {
		$agencias = array();
		$clientes = array();
		$categorias = array();
		$produtos = array();
		
		$user = Sessao::get('usuario');
		
		if( !is_null($id) && $_SERVER['REQUEST_METHOD'] == 'GET' ){
			// dados do grupo
			$_POST = $this->grupo->getGrupo($id);
			
			// lista de categorias
			$lista_cat = $this->categoria->getByGrupo($id);
			// para cada categoria
			foreach($lista_cat as $item){
				// marca as selecionadas
				$_POST['categorias'][] = $item['ID_CATEGORIA'];
			}
			
			// lista de produtos
			$lista = $this->produto->getByGrupo($id);
			// para cada produto
			foreach($lista as $item){
				// marca as selecionadas
				$_POST['produtos'][] = $item['ID_PRODUTO'];
			}
		}
		
		// se tem ID de cliente
		if( !empty($_POST['ID_CLIENTE']) ){
			// pega todas as categorias do cliente
			$categorias = $this->categoria->getByCliente($_POST['ID_CLIENTE']);
			// pega todos os produtos do cliente
			$produtos = $this->produto->getByCliente($_POST['ID_CLIENTE']);
		}
		
		if(empty($_POST['categorias'])){
			$_POST['categorias'] = array();
		}
		
		if(empty($_POST['produtos'])){
			$_POST['produtos'] = array();
		}
		
		$agencias = $this->agencia->listItems(array('STATUS_AGENCIA'=>1),0,10000);
		$clientes = $this->cliente->listItems(array('STATUS_CLIENTE'=>1),0,10000);

        $this->assign('categorias', $categorias);	
        $this->assign('produtos',   $produtos);	
        $this->assign('agencias',   $agencias['data']);	
        $this->assign('clientes',   $clientes['data']);

        //print_rr($_POST);die;
		$this->display('form');
	}
	
	/**
	 * Persiste os dados informados no formulario no banco
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do grupo quando estiver editando
	 * @return void
	 */
	function save($id=null) {
		$erros = $this->grupo->validate($_POST);
		
		if( empty($erros) ) {
			
			if( empty($_POST['ID_AGENCIA']) ){
				$_POST['ID_AGENCIA'] = null;
			}
			
			if( empty($_POST['ID_CLIENTE']) ){
				$_POST['ID_CLIENTE'] = null;
			}
			
			$id = $this->grupo->save($_POST,$id);
			redirect('grupo/lista');
		}
		
		$this->data['erros'] = $erros;
		$this->form($id);
	}
		
}
