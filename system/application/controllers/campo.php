<?php
/**
 * Controller para gerenciar campos de fichas
 * 
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class Campo extends MY_Controller {
	
	/**
	 * Construtor
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Categoria
	 */
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/campo/';
	}
	
	/**
	 * Lista os campos cadastrados
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $pagina Numero da pagina atual
	 * @return void
	 */
	function lista($pagina = 0) {
		if ( !empty($_POST) ) {
			Sessao::set('busca', $_POST);
		}
		
		$user = Sessao::get('usuario');
		$busca = (array) Sessao::get('busca');
		$limit = empty($busca['pagina']) ? 0 : $busca['pagina'];
		
		if(!empty($user['ID_CLIENTE'])){
			$busca['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}
		
		// carrega os clientes
		$clientes = $this->cliente->listItems( array('STATUS_CLIENTE'=>1), 0, 100000);
		$this->data['clientes'] = $clientes['data'];

		$campos = $this->campo->listItems($busca, $pagina, $limit);

		$this->data['busca'] = $busca;
		$this->data['campos'] = $campos['data'];
		$this->data['paginacao'] = linkpagination($campos,$limit);
		$this->data['podeAlterar'] = Sessao::hasPermission('categoria','save');
		
		$this->display('index');
	}
	
	/**
	 * Exibe o formulario de edicao de categoria
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo da categoria quando editando
	 * @return void
	 */
	function form($id=null) {
		if( !is_null($id) && $_SERVER['REQUEST_METHOD'] == 'GET' ){
			$_POST = $this->campo->getById($id);
		}
		
		// carrega os clientes
		$clientes = $this->cliente->listItems(array('STATUS_CLIENTE'=>1), 0, 100000);
		
		$this->assign('clientes', $clientes['data']);
		$this->display('form');
	}
	
	/**
	 * Grava uma nova categoria ou as alteracoes de uma existente
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo da categoria a ser alterada ou null para uma nova
	 * @return void
	 */
	function save($id=null) {
		// faz a validacao dos dados
		$res = $this->campo->validate($_POST);
		//print_rr($_POST);die;
		// se nao houve erros
		if( empty($res) ){
			
			//grava os dados no banco
			$this->campo->save($_POST,$id);
			$this->campo_ficha->updateCampoFicha($id, $_POST['NOME_CAMPO']);
			//echo $this->db->last_query();die;
			// redireciona para a pagina
			redirect('campo/lista');
		}
		
		// indica os erros
		$_REQUEST['erros'] = $res;
		// carrega o formulario
		$this->form($id);
	}
}
