<?php

/**
 * classe para tratar o retorno do IDS Kiwi
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 *
 */

class Idskiwi extends MY_Controller {
	
	/**
	 * funcao que trata o retorno do ids kiwi e envia o email conforme a regra cadastrada
	 * @author Hugo Ferreira da Silva
	 * @param int $idexcel codigo do excel
	 * @param int $idpraca codigo da praca que foi gerada o arquivo
	 * @return void
	 */
	public function callback( $idexcel, $idpraca, $idTemplateItem = null ){
		
		$dados = $this->excel->getById($idexcel);
		$praca = $this->praca->getById($idpraca);
		$dados['praca'] = $praca['DESC_PRACA'];
		$item = $this->templateItem->getById($idTemplateItem);
		
		$dados['item'] = $item;
		$job = $this->job->getById($dados['ID_JOB']);
		
		$status = empty($_POST['status']) ? 4  : $_POST['status'];
		
		// STATUS = 3 ==> Tudo ok
		// STATUS = 4 ==> ocorreram erros
		
		switch( $status ){
			case 3:
				$file = '';
				// no callback, vem o nome do indesign salvo
				// pegamos a ultima parte, que eh o nome do arquivo
				foreach($_POST as $idx => $item){
					if( isset($item['name']) && $item['name'] == 'save_indd' ){
						$parts = explode('/', rawurldecode($item['value']));
						$file = end($parts);
					}
				}
				
				
				$dados['arquivo'] = 'files/layout/'. rawurlencode($file);
				$dados['texto'] = '';
			break;
			
			case 4:
				$dados['texto'] = 'O arquivo INDD não pode ser gerado.<br />';
				$dados['texto'] .= 'Mensagem gerada: ' . (!empty($_POST['msg_retorno']) ? $_POST['msg_retorno'] : '');
			break;
		}
		
		//envia emails de notificacao associado a regra
		$erros = $this->email->sendEmailRegra('importacao_processado', 
												'Pré-diagramação do Job '.$dados['TITULO_JOB'], 
												'ROOT/templates/importacao_processado',
												array(
													'objeto'=>$dados,
													'job'=>$job
												),
												$dados['ID_AGENCIA'],
												$dados['ID_CLIENTE']);
		
	}
	
	/**
	 * funcao que trata o retorno do ids kiwi para preview PDF de template
	 * @author Hugo Ferreira da Silva
	 * @param int $idtemplate codigo do template
	 * @return void
	 */
	public function callback_preview_pdf($idtemplate){
		
	}
	
}