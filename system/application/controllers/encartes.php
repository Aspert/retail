<?php

class Encartes extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('DBXinet');
	}
	
	function index(){
				
		$this->data['encartes'] = $this->encarte->getAll();
		
		
		foreach ($this->data['encartes'] as $key => $encartes){
			
			$nomeJPG = explode('.', $encartes['FILE']);
			
			//produção
			$nomeJPG = $nomeJPG[0] . '_1.jpg';
			//homologação
			//$nomeJPG = $nomeJPG[0] . '_1-1.jpg';
			
			$path = $this->config->item('caminho_storage') . '/ENCARTES/' . $encartes['ID_ENCARTE'] . '/JPG_BAIXA/' ;
			
			$this->xinet->sincroniza($path . $nomeJPG);
			
			$this->data['encartes'][$key]['path'] = $path . $nomeJPG;
			
			$this->data['encartes'][$key]['FileID'] = $this->xinet->getFilePorPath($path, $nomeJPG);
		}
		
		$this->load->view('/ROOT/encartes/index.php',$this->data);
	}
	
	function form($idEncarte){
	
		
		$info = $this->encarte->getByFileID($idEncarte);
		
		$path = $this->config->item('caminho_storage') . '/ENCARTES/' . $info[0]['ID_ENCARTE'] . '/JPG_ALTA/' ;
		//$this->xinet->sincroniza($this->config->item('caminho_storage') . '/ENCARTES/');
		
		$arquivos = arquivosDir($path);
		
		$encartes = array();
		
		foreach ($arquivos as $key => $arquivo){
			
			$encartes[$key]['path'] = $path . $arquivo;
			$encartes[$key]['FileID'] = $this->xinet->getFilePorPath($path, $arquivo);
			
			/*if($key == 11)
				break;*/
			
		}
		
		$this->data['ID_ENCARTE'] = $idEncarte;
		$this->data['ENCARTES'] = $encartes;
		$this->data['TOTAL_PAGINAS'] = $key;
		
		$this->data['INFO'] = $info[0];
		/*$file = $this->xinet->getImgFilePorID($FileID, 'bigwebdata', '256');
		$tabela = ( isset($file[0]['tabela']) && $file[0]['tabela'] == 'bigwebdata'."_000" ) ? 'bigwebdata' : $file[0]['tabela'];
		$total = $this->xinet->getTotalSpread($FileID, $tabela);*/
		
		$this->load->view('/ROOT/encartes/form.php',$this->data);
	}
	
	function preview($FileID=0, $img_size='small', $spread=0, $type=false){

		header("Content-type: image/gif");
		$out = $this->getImgData($FileID, $spread, $img_size);
		print $out['Data'];
	}
	
	function getImgData($FileID=0, $spread=0, $img_size='big'){
		
		$bigWebData = false;
		$type = "";
		if($FileID != null ){
			$out = "";
	
			if($img_size == 'big'){
				$img_size = '256';
				$prefix_db = 'bigwebdata';
			}else{
				$img_size = '64';
				$prefix_db = 'smallwebdata';
			}
	
			$file = $this->xinet->getImgFilePorID($FileID, $prefix_db, $img_size);
			$tabela = ( isset($file[0]['tabela']) && $file[0]['tabela'] == $prefix_db."_000" ) ? $prefix_db : $file[0]['tabela'];
			$webData = $this->xinet->getWebData( $FileID, $tabela, $spread );
			
			if($webData)
				return $webData[0];
			
		}else{
			$file[0]['FileName'] = 'nopreview.gif';
			$out = file_get_contents('img/nopreview.gif');
		}
		
		return false;
	}
	
	function uploadEncarte(){
		
		if(!empty($_FILES['encarte'])) {
			
			$data = array_shift($_FILES);
			$novoNome = removeCaracteresEspeciaisNomeArquivo($data['name']);
			$origem = $data['tmp_name'];
			$nomeSemExtesao = explode('.', $novoNome);
			$nomeSemExtesao = $nomeSemExtesao[0];

			//recupera a configuracao do site
			$site = $this->config->item("site");
			
			$destino = TWIST_IN . 'PDF_para_JPEG/' . $novoNome;
			copy($origem, $destino);
			
			//aqui salvamos as informações do encarte no banco
			$dados = array();
			$dados['FILE'] = $novoNome;
			$dados['OBSERVACAO'] = $_POST['observacao'];
			$id = $this->encarte->save($dados);
			$path = utf8MAC($this->config->item('caminho_storage') . '/ENCARTES/' . $id);
			
			// cria o diretorio
			if(!is_dir($path)){
				$this->xinet->createPath($path);
				$this->xinet->sincroniza($path);
			}
			
			$pathPDF = $path . '/PDF/';
			// cria o diretorio onde será inserido o PDF
			if(!is_dir($pathPDF)){
				$this->xinet->createPath($pathPDF);
				
				copy($origem, $pathPDF . $novoNome);
				$this->xinet->sincroniza($pathPDF);
			}
			
			
			$pathJPG_ALTA = $path . '/JPG_ALTA/';
			// cria o diretorio onde será inserido o JPG em ALTA
			if(!is_dir($pathJPG_ALTA)){
				
				$this->xinet->createPath($pathJPG_ALTA);
				
				$origemJPG_ALTA = TWIST_OUT .$nomeSemExtesao.'/JPEG-ALTA/';
				
				while(!is_dir($origemJPG_ALTA)){
					sleep(1);
				}
				
				$arquivos = arquivosDir($origemJPG_ALTA);
				if(empty($arquivos)){
					sleep(10);
					$arquivos = arquivosDir($origemJPG_ALTA);
				}
				
				foreach ($arquivos as $arquivo){
					copy($origemJPG_ALTA . $arquivo, $pathJPG_ALTA . $arquivo);
					chmod($pathJPG_ALTA . $arquivo, 0777);
				}
				
				$this->xinet->sincroniza($pathJPG_ALTA);

			}
			
			
			$pathJPG_BAIXA = $path . '/JPG_BAIXA/';
			// cria o diretorio onde será inserido o JPG em BAIXA
			if(!is_dir($pathJPG_BAIXA)){
				$this->xinet->createPath($pathJPG_BAIXA);
				
				$origemJPG_BAIXA = TWIST_OUT .$nomeSemExtesao.'/JPEG-BAIXA/';
				
				while(!is_dir($origemJPG_ALTA)){
					sleep(1);
				}
				
				$arquivos = arquivosDir($origemJPG_BAIXA);
				if(empty($arquivos)){
					sleep(10);
					$arquivos = arquivosDir($origemJPG_BAIXA);
				}
				
				foreach ($arquivos as $arquivo){
					copy($origemJPG_BAIXA . $arquivo, $pathJPG_BAIXA . $arquivo);
					chmod($pathJPG_BAIXA . $arquivo, 0777);
				}
				
				$this->xinet->sincroniza($pathJPG_BAIXA);
				
			}
			
		}
		
		redirect('encartes/index');
		
	}
	
	function download(){
		
		$id = $_REQUEST['id'];
		$nome = 'ENCARTE#'.$id.'.pdf';
		$path = utf8MAC($this->config->item('caminho_storage') . '/ENCARTES/' . $id . '/PDF/');
		$file = arquivosDir($path);
		$arquivo = $path . $file[0];
		
		if(is_file($arquivo)){
			header("Cache-control: private");
			header("Content-type:application/pdf");
			header("Content-Length: ".filesize($arquivo));
			header("Content-Disposition: attachment; filename=\"$nome\"");
	
			$arq=fopen($arquivo, 'r');
			fpassthru($arq);
			fclose($arq);
		}
		
	}	
}