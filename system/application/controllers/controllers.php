<?php
/**
 * controller para gerenciar o cadastro de controllers
 * 
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class Controllers extends MY_Controller
{
	/**
	 * Construtor
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Controllers
	 */
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/controllers/';
	}
	
	/**
	 * Lista as controllers cadastradas
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $pagina Pagina atual de listagem
	 * @return void
	 */
	function lista($pagina=0) {
		
		if(!empty($_POST)){
			Sessao::set('busca', $_POST);
		}
		
		$dados = (array) Sessao::get('busca');
		
		$limit = empty($dados['pagina']) ? 0 : $dados['pagina'];
		$grupos = $this->controller->listItems($dados, $pagina, $limit);
		
		$this->data['busca'] = $dados;
		$this->data['lista'] = $grupos['data'];
		$this->data['podeAlterar'] = Sessao::hasPermission('controllers','save');
		$this->data['paginacao'] = linkpagination($grupos,$limit);
		
		$this->display('index');	
	}
	
	/**
	 * Exibe o formulario de edicao
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo da controller quando estiver editando
	 * @return void
	 */
	function form($id=null) {
		
		if( !is_null($id) && $_SERVER['REQUEST_METHOD'] == 'GET' ){
			$_POST = $this->controller->getById($id);
		}		
					
		$this->display('form');
	}
	
	/**
	 * Persiste os dados informados no formulario no banco
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo da controller quando estiver editando
	 * @return void
	 */
	function save($id=null) {
		$erros = $this->controller->validate($_POST);
		
		if( empty($erros) ) {
			$id = $this->controller->save($_POST,$id);
			redirect('controllers/lista');
		}
		
		$this->data['erros'] = $erros;
		$this->form($id);
	}
}
