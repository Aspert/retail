<?php
class Home extends MY_Controller{

	function __construct(){
		parent::__construct();
		$prefs['template'] = $this->config->item('calendario_template');
		$this->load->library('calendar', $prefs);
	}

	private function criarFichas(){
		set_time_limit(0);
		$total = 10000;
		$criadas = 0;
		$codigoFicha = 1000;

		$lista = $this->objeto->listItems(array(), 0, 100000);

		while( $criadas < $total ){
			foreach($lista['data'] as $objeto){
				if(!empty($objeto['ID_SUBCATEGORIA'])){
					$ficha = array(
						'ID_USUARIO' => 1,
						'COD_FICHA' => $codigoFicha,
						'COD_BARRAS_FICHA' => $codigoFicha,
						'NOME_FICHA' => $objeto['FILENAME'],
						'ID_CATEGORIA' => $objeto['ID_CATEGORIA'],
						'ID_SUBCATEGORIA' => $objeto['ID_SUBCATEGORIA'],
						'FLAG_ACTIVE_FICHA' => 1,
						'ID_APROVACAO_FICHA' => 1,
						'DT_INSERT_FICHA' => date('Y-m-d H:i:s'),
						'FLAG_TEMPORARIO' => 1,
						'ID_CLIENTE' => $objeto['ID_CLIENTE'],
						'DATA_ALTERACAO' => date('Y-m-d H:i:s'),
					);

					$id = $this->ficha->save($ficha);

					// inserindo o objeto ficha
					$of = array(
						'ID_OBJETO' => $objeto['ID_OBJETO'],
						'ID_FICHA' => $id,
						'PRINCIPAL' => 1
					);

					$this->objeto_ficha->save($of);

					$codigoFicha++;
					$criadas++;
				}
			}
		}

	}

	function index($param = ''){
		
		if(isset($_SESSION['ID_JOB'])){
			unset($_SESSION['ID_JOB']);
		} else if(isset($_SESSION['idJob'])){
			unset($_SESSION['idJob']);
		} else if(isset($_SESSION['relatorioObjetoPendente'])){
			unset($_SESSION['relatorioObjetoPendente']);
		}

		$usuario = Sessao::get('usuario');

		if($param == '___COMPILAR___'){
			$this->reestruturarModels();
		}
		if($param == '___MANDA_BALA___'){
			$this->criarFichas();
			exit;
		}

		$this->load->library('rss_php');

	    $rss = new rss_php;

	    $feedURL = '';

	    if(!empty($usuario['FEED_CLIENTE'])){
	    	$feedURL = $usuario['FEED_CLIENTE'];
	    }else if(!empty($usuario['FEED_AGENCIA'])){
	    	$feedURL = $usuario['FEED_AGENCIA'];
	    }else{
	    	$feedURL = 'http://rss.noticias.uol.com.br/economia/ultnot/index.xml';
	    }

   		@$rss->load($feedURL,false);
    	@$this->data['rss'] = $rss->getRSS();

	    // dias que faltam para expirar
	    $this->assign('dias_expirar_senha', Sessao::getInstance()->diasParaExpiracao($usuario['LOGIN_USUARIO']));

		$this->load->view('ROOT/home/index', $this->data);
	}

	function sem_permissao(){
		$controller = Sessao::get('__controller');
		$method = Sessao::get('__method');

		$this->data['controller'] = $controller;
		$this->data['method'] = $method;

		$this->load->view('ROOT/home/sem_permissao',$this->data);
	}

	public function modal_contato(){
		$this->load->view('ROOT/layout/contato',$this->data);
	}

	public function modal_about(){
		$data = $this->session->userdata('__BUSCA');
		if(!is_array($data)){
			$data = array($data);
		}
		
		$this->data['podeVer'] = Sessao::hasPermission('conexao','index');
		$this->load->view('ROOT/layout/about',$this->data);
	}

	public function modal_help(){

		$user = Sessao::get('usuario');

		$this->data['agencias'] = array();
		$this->data['clientes'] = array();
		$this->data['tags'] = array();


		if(empty($user['ID_CLIENTE'])){
			$this->data['clientes'] = $this->cliente->getByAgencia($user['ID_AGENCIA']);
		}else{
			$this->data['tags'] = $this->template->getTemplateTags($user['ID_CLIENTE']);
		}

		$this->load->view('ROOT/layout/help',$this->data);
	}

	/**
	 * Metodo para compilar as classes e atualizar o mapeamento
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	private function reestruturarModels(){
		// se estiver no servidor
		if(strpos($_SERVER['HTTP_HOST'], '.247id.com.br') !== false) {
			// nao executa
			return;
		}

		$onlyTables = array();


		$path = dirname(FCPATH) . '/system/application/models/';
		$dh = opendir($path);

		$startTag = "\t### START";
		$endTag = "\t### END";

		while(($file=readdir($dh)) !== false){
			if( preg_match('@DB\.php$@', $file)){
				$string = file_get_contents($path.$file);

				$code = array($startTag);

				$spos = strpos($string,'$tableName');

				if($spos > 0){

					$epos = strpos($string,"\n",$spos)-$spos;
					$line = substr($string,$spos,$epos);

					if(preg_match("@tableName\s*=\s*'(\w+)'@i",$line, $reg)){

						if( !$this->db->table_exists($reg[1]) ){
							continue;
						}

						if(!empty($onlyTables) && !in_array($reg[1], $onlyTables)){
							continue;
						}

						$fields = $this->db->field_data($reg[1]);

						$code[] = "\tprotected function _initialize(){";

						foreach($fields as $field){
							$code[] = sprintf("\t\t\$this->addField('%s','%s','%s',%d,%d);",
								$field->name,
								$field->type,
								$field->default,
								$field->max_length,
								$field->primary_key
							);
						}

						$code[] = "\t}";
						$code[] = $endTag . PHP_EOL .PHP_EOL;
					}

					$newCode = implode(PHP_EOL, $code);

					// agora, vamos ver se existem as tags de
					// abertura e fechamento no arquivo
					if(strpos($string, $startTag) !== false && strpos($string,$endTag) !== false){
						// entao fazemos um replace
						$current = substr($string, strpos($string,$startTag), strpos($string,$endTag) + strlen($endTag) - strpos($string,$startTag) + 4);
						$newString = str_replace($current, $newCode, $string);

						file_put_contents($path.$file, $newString);

					// se nao existe, adicionamos
					} else {
						$open = strpos($string,'{');
						$topo = substr($string, 0, $open+1);
						$final = substr($string, $open+2);

						$novoConteudo = $topo . PHP_EOL . $newCode . $final;
						file_put_contents($path.$file, $novoConteudo);
					}

					echo "Feito para " . $reg[1] . "<BR>\n";
				}


			}
		}

		closedir($dh);
		exit;
	}
}
