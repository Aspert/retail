<?php
class Permissao extends MY_Controller {
	
	/**
	 * Construtor
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Permissao
	 */
	function __construct()
	{
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/permissao/';
	}
	
	/**
	 * Lista os grupos cadastrados
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param $pagina
	 * @return void
	 */
	function lista($pagina=0) {
		if(!empty($_POST)) {
			Sessao::set('busca',$_POST);
		}
		
		$busca = (array) Sessao::get('busca');
		$limit = empty($busca['pagina']) ? 0 : $busca['pagina'];

		if(empty($busca['ID_AGENCIA'])){
			unset($busca['ID_AGENCIA']);
		}
		if(empty($busca['ID_GRUPO'])){
			unset($busca['ID_GRUPO']);
		}
		if(empty($busca['ID_CLIENTE'])){
			unset($busca['ID_CLIENTE']);
		}
		
		$agencias = $this->agencia->listItems(array('STATUS_AGENCIA'=>1),0,100000);
		$clientes = $this->cliente->listItems(array('STATUS_CLIENTE'=>1),0,100000);

		if( empty($busca['ID_CLIENTE']) && empty($busca['ID_AGENCIA']) ){
			$this->assign('lista_grupos', array());
		}
		else{			
			$this->assign('lista_grupos', $this->grupo->listItems($busca, $pagina, $limit));
		}
		
		$grupos = $this->grupo->listItems($busca, $pagina, $limit);
		
		$this->data['agencias']    = $agencias['data'];
		$this->data['clientes']    = $clientes['data'];
		$this->data['lista']       = $grupos['data'];
		$this->data['busca']       = $busca;
		$this->data['paginacao']   = linkpagination($grupos, $limit);
		$this->data['podeAlterar'] = Sessao::hasPermission('permissao','salvar');
		$this->display('index');
		
	}
	
	/**
	 * Exibe o formulario de edicao
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do grupo quando editando ou null quando for um novo
	 * @return void
	 */
	function form($id=null) {
		$grupo = $this->grupo->getById($id);
		
		// se nao achou o grupo
		if( empty($grupo) ){
			redirect('permissao');
		}
		
		$list = $this->grupo->getPermissoes($grupo['ID_GRUPO']);
		foreach($list as $item){
			$_POST['functions'][$item['ID_FUNCTION']] = 1;
		}
		
		$this->data['grupo'] = $grupo;
		$this->data['controllers'] = $this->controller->getAllWithFunctions();
		$this->display('form');
	}
	
	/**
	 * Grava as permissoes selecionadas
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do GACP
	 * @return void
	 */
	function salvar($id) {
		
		$erros = array();
		
		try {
			$functions = array();
			
			if(!empty($_POST['functions'])){
				foreach($_POST['functions'] as $idFunction => $flag){
					$functions[] = $idFunction;
				}
			}
			
			$this->grupo->salvarPermissoes($id, $functions);
			
			redirect('permissao/lista');
			
		} catch(Exception $e) {
			$erros[] = $e->getMessage();
		}
		
		$this->form($id);
	}
}
