<?php
/**
 * Controller para previes do xinet
 * @author Juliano Polito
 *
 */
class Preview extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/agencia/';
	}
	
	function getPreview($FileID=0, $spread=0, $img_size='big'){
		if($FileID != null ){
			$out = "";
	
			if($img_size == 'big'){
				$img_size = '256';
				$prefix_db = 'bigwebdata';
			}else{
				$img_size = '64';
				$prefix_db = 'smallwebdata';
			}
	
			$file = $this->xinet->getImgFilePorID($FileID, $prefix_db, $img_size);
	
			$tabela = ( isset($file[0]['tabela']) && $file[0]['tabela'] == $prefix_db."_000" ) ? $prefix_db : $file[0]['tabela'];
	
			if( $webdata = $this->xinet->getWebData( $FileID, $tabela, $spread ) ){
				foreach($webdata as $data){
					$out .= $data['Data'];
				}
			}else{
				if( is_movie($file[0]['Type']) )
					$out = file_get_contents('img/icones/filme.png');
	
				else if( is_music($file[0]['Type']) )
					$out = file_get_contents('img/icones/musica.png');
	
				else if( is_doc($file[0]['Type']) )
					$out = file_get_contents('img/icones/texto.png');
	
				else if( is_image($file[0]['Type']) )
					$out = file_get_contents('img/icones/imagem.png');
	
				else
					$out = file_get_contents('img/nopreview.gif');
			}
		}else{
			$file[0]['FileName'] = 'nopreview.gif';
			$out = file_get_contents('img/nopreview.gif');
		}
	
		return array( 'FileName' => $file[0]['FileName'], 'Data' => $out );
	}
	
}