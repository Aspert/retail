<?php
/**
 * Controller para o modulo de upload de pdf para FPO/OPI
 * @author juliano.polito
 */
class Pdf extends MY_Controller{
	
	/**********************************************************/
	/************************* CONSTANTS **********************/
	/**********************************************************/
	
	const STORAGE_ENGINES_PATH = "/storage2/ENGINES/";
	const STORAGE_IN_PATH = "/storage2/ENGINES/IN/";
	const STORAGE_OUT_PATH = "/storage2/ENGINES/OUT/";
	
	/**********************************************************/
	/*********************** CONSTRUCTOR **********************/
	/**********************************************************/
	
	function Pdf(){
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/pdf/';
	}
	
	/**********************************************************/
	/************************* METHODS ************************/
	/**********************************************************/
	
	/**
	 * Carrega a view inicial do módulo
	 */
	function index(){
		$this->load->view('ROOT/pdf/index',$this->data);
	}
	
	function lista($pagina = 0){
		if(!empty($_POST)){
			Sessao::set('busca', $_POST);
		}
		
		$busca = (array) Sessao::get('busca');
		$user = Sessao::get('usuario');
		
		// verificamos se tem algo na sessao
		// nestes casos independem do formulario
		if(!empty($user['ID_AGENCIA'])){
			$busca['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}
		
		if(!empty($user['ID_CLIENTE'])){
			$busca['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}
		
		if(!empty($user['ID_PRODUTO'])){
			$busca['ID_PRODUTO'] = $user['ID_PRODUTO'];
		}
		
		if(empty($busca['ORDER'])){
			$busca['ORDER'] = 'DATA_MUDANCA_ETAPA';
		}

		if(empty($busca['ORDER_DIRECTION'])){
			$busca['ORDER_DIRECTION'] = 'DESC';
		}
		
		// carrega os filtros
		$this->loadFilters($busca);
		// somente os produtos que usuario tem acesso
		$busca['PRODUTOS'] = getValoresChave(getProdutosUsuario(),'ID_PRODUTO');
		
		$limit = empty($busca['pagina']) ? 5 : $busca['pagina'];
		$lista = $this->excel->listItems($busca, $pagina, $limit, $busca['ORDER'], $busca['ORDER_DIRECTION']);
		
		$this->data['busca'] = $busca;
		$this->data['lista'] = $lista['data'];
		$this->data['paginacao'] = linkpagination($lista, $limit);
		$this->data['podeUpload'] = Sessao::hasPermission('pdf','upload');
		
		//pre($this->data);
		
		$this->display('index');
	}
	
	function opi($id=null){
		if( !empty($id) && $_SERVER['REQUEST_METHOD'] == 'GET' ){
			Sessao::set('__excel','');
			$_POST = $this->excel->getById($id);
			$_POST['DATA_INICIO'] = format_date_to_form($_POST['DATA_INICIO']);
			$_POST['DATA_TERMINO'] = format_date_to_form($_POST['DATA_TERMINO']);
		}
		
		$xls = $this->excel->getById($id);
		$this->checaPermissaoAlterar($xls['ID_JOB']);
		
		$erros = array();
		$filename = Sessao::get('__excel');
		
		$this->data['agencias'] = array();
		$this->data['clientes'] = array();
		$this->data['produtos'] = array();
		$this->data['campanhas'] = array();
		$this->data['templates'] = array();
		
		$user = Sessao::get('usuario');
		
		if(!empty($user['ID_AGENCIA'])) {
			$_POST['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}
		if(!empty($user['ID_CLIENTE'])) {
			$_POST['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}
		if(!empty($user['ID_PRODUTO'])) {
			$_POST['ID_PRODUTO'] = $user['ID_PRODUTO'];
		}
		
		if(empty($user['ID_AGENCIA'])){
			$ag = $this->agencia->getAll(0,1000,'DESC_AGENCIA');
			$this->data['agencias'] = $ag['data'];
		}
		
		if(empty($user['ID_CLIENTE']) && !empty($_POST['ID_AGENCIA'])){
			$rs = $this->cliente->listItems(array('ID_AGENCIA'=>$_POST['ID_AGENCIA'],'STATUS_CLIENTE'=>1),0,10000);
			$this->data['clientes'] = $rs['data'];
		}
		
		if(empty($user['ID_PRODUTO']) && !empty($_POST['ID_CLIENTE'])){
			$rs = $this->produto->listItems(array('ID_CLIENTE'=>$_POST['ID_CLIENTE'],'STATUS_PRODUTO'=>1),0,10000);
			$this->data['produtos'] = $rs['data'];
		} 
		
		if(!empty($_POST['ID_PRODUTO'])){
			$rs = $this->campanha->listItems(array('ID_PRODUTO'=>$_POST['ID_PRODUTO'],'STATUS_CAMPANHA'=>1),0,10000);
			$this->data['campanhas'] = $rs['data'];
		}
		
		if(!empty($_POST['ID_CAMPANHA'])){
			$rs = $this->template->listItems(array('ID_CAMPANHA'=>$_POST['ID_CAMPANHA'],'STATUS_TEMPLATE'=>1),0,10000);
			$temps = $rs['data'];
			//Cria os labels dos tenmplates com formato - Template 18 - 10.00cm x 15.00cm
			foreach ($temps as &$t ) {
				if(!empty($t['LARGURA_TEMPLATE']) && !empty($t['ALTURA_TEMPLATE'])){
					$label = sprintf('%s - %1.2fcm x %1.2fcm',$t['DESC_TEMPLATE'],$t['LARGURA_TEMPLATE'],$t['ALTURA_TEMPLATE']);
					$t['DESC_TEMPLATE'] = $label;
				}
			}
			$this->data['templates'] = $temps;
		}
		
		$dpiList = $this->filaOPI->listItems(array());
		$this->data['dpiList'] = $dpiList['data'];
		
		$rs = $this->tipoPeca->getAll(0,1000,'DESC_TIPO_PECA');
		$this->data['tiposPeca'] = $rs['data'];
		
		$rs = $this->tipoJob->getAll(0,1000,'DESC_TIPO_JOB');
		$this->data['tiposJob'] = $rs['data'];
		
		$this->data['filename'] = $filename;
		$this->data['id'] = $id;
		$this->data['podeDownload'] = Sessao::hasPermission('pdf','download');
		$this->data['erros'] = $erros;
		$this->display('opi');
	}
	
	function save($id=null){

	}
	
	/**
	 * Recebe o upload do PDF para OPI e 
	 * grava o arquivo em uma pasta no storage STORAGE_IN_PATH.
	 * Após gravar no storage_in redirecionamos para uma view que 
	 * verificará através de ajax se o arquivo está pronto para download
	 */
	function upload($idExcel){
		
		error_reporting(E_ALL);
		
		$xls = $this->excel->getById($idExcel);
		$this->checaPermissaoAlterar($xls['ID_JOB']);
		
		//flag que indica se o upload deu certo (utilizada na view)
		$uploadReturn = false;
		
		//padrao de nomencaltura da pasta IN
		$inPattern = 'IN_%d_A/';
		
		//Nome do folder da fila
		$inFolder = sprintf($inPattern,$_POST['FILA_OPI_DPI']);
		
		// Nome fixo a pasta de pois as filas sempre geram com a DPI de 150, inclusive a IN_300_A
		$inFolder = 'IN_300_A/';
		
		//guarda o nome do arquivo gerado no server
		$fileName = "";
		$isPDF = preg_match('@\.pdf$@i', $_FILES['file']['name']) == 1;
		if(isset($_FILES['file'])){
			if($isPDF && is_uploaded_file($_FILES['file']['tmp_name'])){
				//Recupera o id do usuario logado
				$user = Sessao::get('usuario');
				$userID = $user['ID_USUARIO'];
				
				//pre(self::STORAGE_ENGINES_PATH.$inFolder.$fileName);
				
				//recupera o id do arquivo registrado ou do existente
				$fileOPI_ID = $this->excelopi->getNewFileID($idExcel,$_POST['FILA_OPI_DPI'],$_FILES['file']['name']);
				
				//filename para gravacao do arquivo no storage in
				$fileName = $this->excelopi->getFileName($fileOPI_ID);
				
				//mantem o nome do arquivo original para o download
				//$originalFileName = $_FILES['arquivo']['name'];
				
				//apaga o arquivo se ja existir no in
				if(is_file(self::STORAGE_OUT_PATH.$fileName.'.pdf') 
					&& is_writable(self::STORAGE_OUT_PATH.$fileName.'.pdf')){
					chmod(self::STORAGE_OUT_PATH.$fileName.'.pdf',0777);
					unlink(self::STORAGE_OUT_PATH.$fileName.'.pdf');
				}
				
	 			//verifica se o arquivo for movido com sucesso
				if(move_uploaded_file($_FILES['file']['tmp_name'],
					self::STORAGE_ENGINES_PATH.$inFolder.$fileName)){
					chmod(self::STORAGE_ENGINES_PATH.$inFolder.$fileName.'.pdf',0777);
					$uploadReturn = true;
				}
			}			
		}
		
		//dados para  a view
		//nome do arquivo enviado para OPI
		$this->data['fileName'] = $fileName;
		
		//nome original do arquivo
		//$this->data['originalFileName'] = $originalFileName;
		
		//flag indicando se o upload deu certo
		$this->data['uploadReturn'] = $uploadReturn;
		
		redirect('pdf/opi/'.$idExcel);
	}
	
	public function download($idPdf){
		ob_end_clean();
		//filename para gravacao do arquivo no storage in
		$fileName = $this->excelopi->getFileName($idPdf);
		$originalName = $this->excelopi->getById($idPdf);
		$originalName = $originalName['ARQUIVO_EXCEL_OPI'];
		$path = self::STORAGE_OUT_PATH.$fileName;
		if(is_file($path)){
			header('Content-type: application/pdf');
			header('Content-Disposition: attachment; filename="'.$originalName.'"');
			
			// indicamos o tamanho do arquivo
	
			// adicionamos mais alguns headers
			// porque da problema para baixar via HTTPS pelo IE
			if (strstr($_SERVER['HTTP_USER_AGENT'], "MSIE")) {
				header('Expires: 0');
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header("Content-Transfer-Encoding: binary");
				header('Pragma: public');
			}
	
			header("Content-Length: " . filesize($path));
			
//			readfile($path);
			$fhandle = fopen($path,'r');
			while(!feof($fhandle)){
				echo fread($fhandle,1024);
			}
			fclose($fhandle);
		}
	}
	
	
	private function loadFilters(array $data){
		$user = Sessao::get('usuario');
		// verificamos se tem algo na sessao
		// nestes casos independem do formulario
		if(!empty($user['ID_AGENCIA'])){
			$data['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}
		
		if(!empty($user['ID_CLIENTE'])){
			$data['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}
		
		if(!empty($user['ID_PRODUTO'])){
			$data['ID_PRODUTO'] = $user['ID_PRODUTO'];
		}
		
		//////////////////////////////////////////////////////
		// filtros
		//////////////////////////////////////////////////////
		$this->assign('clientes',array());
		$this->assign('produtos',array());
		$this->assign('agencias',array());
		
		if(!empty($user['ID_AGENCIA'])){
			$this->assign('clientes', $this->cliente->getByAgencia($user['ID_AGENCIA']));
		}
		
		if(!empty($user['ID_CLIENTE'])){
			$this->assign('agencias', $this->agencia->getByCliente($user['ID_CLIENTE']));
		}
		
		if(!empty($data['ID_CLIENTE']) && !empty($data['ID_AGENCIA'])){
			$this->data['produtos'] = getProdutosUsuario($data['ID_CLIENTE'], $data['ID_AGENCIA']);
		}
		
		if(!empty($data['ID_PRODUTO'])){
			$cl = $this->campanha->listItems(array("ID_PRODUTO"=>$data['ID_PRODUTO'],'STATUS_CAMPANHA'=>1),0,10000);
			$this->data['campanhas'] = $cl['data'];
		} else {
			$this->data['campanhas'] = array();
		}
		
		$tipo = $this->tipoJob->getAll(0,1000,'DESC_TIPO_JOB');
		$this->data['tiposJob'] = $tipo['data'];
	}

	
	/**
	 * Checa se o usuario logado tem permissao ou nao de alterar o job acessado
	 * 
	 * Se nao tiver permissao, redireciona para a listagem
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idjob
	 * @return void
	 */
	protected function checaPermissaoAlterar($idjob){
		$user = Sessao::get('usuario');
		$job = $this->job->getById($idjob);
		$produtos = getValoresChave(getProdutosUsuario($job['ID_CLIENTE'],$job['ID_AGENCIA']),'ID_PRODUTO');
		$goto = 'pdf/lista';
		
		if(!empty($user['ID_CLIENTE'])){
			if($job['ID_CLIENTE'] != $user['ID_CLIENTE']){
				redirect($goto);
			}
			
			if(!in_array($job['ID_PRODUTO'],$produtos)){
				redirect($goto);
			}
		}
		
		if(!empty($user['ID_AGENCIA'])){
			if($user['ID_AGENCIA'] != $job['ID_AGENCIA']){
				redirect($goto);
			}
			
			$clientes = getValoresChave($this->cliente->getByAgencia($user['ID_AGENCIA']),'ID_CLIENTE');
			if(!in_array($job['ID_PRODUTO'],$produtos)){
				redirect($goto);
			}
			
			if(!in_array($job['ID_CLIENTE'],$clientes)){
				redirect($goto);
			}
		}
	}
	
}
