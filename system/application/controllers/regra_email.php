<?php
/**
 * Controller para gerenciar regras de email
 * 
 * @author Juliano Polito
 * @link http://www.247id.com.br
 */
class Regra_email extends MY_Controller {
	
	/**
	 * Construtor
	 * 
	 * @author Juliano Polito
	 * @link http://www.247id.com.br
	 * @return Categoria
	 */
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/regra_email/';
	}
	
	/**
	 * Lista as categorias cadastradas
	 * 
	 * @author Juliano Polito
	 * @link http://www.247id.com.br
	 * @param int $pagina Numero da pagina atual
	 * @return void
	 */
	function lista($pagina = 0) {
		//recupera usuario da sessao
		$user = Sessao::get('usuario');
		
		//carrega dados de busca na sessao
		if(!empty($_POST)){
			Sessao::set('busca', $_POST);
		}
		
		//recupera a busca da sessao
		$busca = (array) Sessao::get('busca');
		
		//carrega informacoes e filtros de busca
		$this->loadFilters($busca);
		
		//offset de paginacao
		$limit = isset($busca['pagina']) ? $busca['pagina']:null ;
		
		//recupera lista de regras de email
		$regras = $this->regraEmail->listItems($busca, $pagina, $limit);
		
		//prepara dados para a view
		$this->data['busca'] = $busca;
		$this->data['regras'] = $regras['data'];
		$this->data['paginacao'] = linkpagination($regras, $limit);
		$this->data['podeAlterar'] = Sessao::hasPermission('regra_email','save');
		$this->data['podeExcluir'] = Sessao::hasPermission('regra_email','delete');
		
		$this->display('index');
	}
	
	/**
	 * Exibe o formulario de edicao de categoria
	 * 
	 * @author Juliano Polito
	 * @link http://www.247id.com.br
	 * @param int $id Codigo da regra quando editando
	 * @return void
	 */
	function form($id=null) {
		$user = Sessao::get('usuario');
		
		//carrega o registro solicitado para edicao
		if( !is_null($id) && $_SERVER['REQUEST_METHOD'] == 'GET' ){
			$_POST = $this->regraEmail->getById($id);
		}
		
		//carrega a lista de usuarios da regra para view
		$this->loadUsuariosRegra($id);
		
		//carrega outras informacoes
		$this->loadFilters($_POST);
		
		//carrega a view
		$this->display('form');
	}
	
	/**
	 * Exclui uma regra por id
	 * 
	 * @author juliano.polito
	 * @param $id id da regra de email
	 * @return void 
	 */
	public function delete($id){
		//exclui o registro
		if( !is_null($id) ){
			$this->regraEmail->delete($id);
		}
		// redireciona para a pagina
		redirect('regra_email/lista');
	}
	
	/**
	 * Grava uma nova regra ou as alteracoes de uma existente
	 * 
	 * @author Juliano Polito
	 * @link http://www.247id.com.br
	 * @param int $id Codigo da regra a ser alterada ou null para uma nova
	 * @return void
	 */
	function save($id=null) {
		$_POST['ID_REGRA_EMAIL'] = sprintf('%d', $id);

		// faz a validacao dos dados
		$res = $this->regraEmail->validate($_POST);
		
		//verifica chave duplicada
		if(!isset($id) && empty($res)){
			//se for inserção, verifca chave duplicada
			$regra = $this->regraEmail->getByChave($_POST['CHAVE_REGRA_EMAIL']);
			if(!empty($regra)){
				$res['CHAVE_REGRA_EMAIL'] = 'Chave duplicada';
			}
		}
		
		if( empty($res) ){
			// se nao houve erros
			//grava os dados no banco e recupera o id em caso de insert
			$id = $this->regraEmail->save($_POST,$id);
			
			//exclui os relacionamentos de usuarios com a regra
			$this->regraUsuario->deleteByRegraEmail($id);
			
			if(isset($_POST["usuarios"])){
				//recupera usuarios enviados
				$usuarios = $_POST["usuarios"];
				
				//salva os novos relacionamentos
				foreach ($usuarios as $idUsuario) {
					$data = array();
					$data['ID_USUARIO'] = $idUsuario;
					$data['ID_REGRA_EMAIL'] = $id;
					$this->regraUsuario->save($data);
				}
			}
			// redireciona para a pagina
			redirect('regra_email/lista');
		}
		
		// indica os erros
		$_REQUEST['erros'] = $res;
		// carrega o formulario
		$this->form($id);
	}
	
	private function loadUsuariosRegra($id){
		$this->assign('grupos_selecionados', array());
		if(!empty($id)){
			//recupera lista de usuarios associados a regra de email
			$usuariosRegra = $this->regraUsuario->getByRegraEmail($id);
			//recupera a lista de grupos dos usuarios associados
			$GruposRegra = $this->regraUsuario->getGruposByRegra($id);
			// coloca os grupos na view
			$this->assign('grupos_selecionados', $GruposRegra);
		}
	}
	
	private function loadFilters($data){
		$this->assign('clientes', array());
		$this->assign('agencias', array());
		$this->assign('grupos',   array());
		
		// recupera as agencias
		$rs = $this->agencia->listItems(array('STATUS_AGENCIA'=>1),0,100000);
		$this->assign('agencias', $rs['data']);
		
		// recupera os clientes
		$rs = $this->cliente->listItems(array('STATUS_CLIENTE'=>1),0,100000);
		$this->assign('clientes', $rs['data']);
		
		
		if(!empty($data['ID_AGENCIA'])){
			$this->assign('grupos', $this->grupo->getByAgencia($data['ID_AGENCIA']));
		}
		
		if(!empty($data['ID_CLIENTE'])){
			$this->assign('grupos', $this->grupo->getByCliente($data['ID_CLIENTE']));
		}
	}
	
	
}
