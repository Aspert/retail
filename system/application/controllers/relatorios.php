<?php

/**
 * Controller para geracao de relatorios
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 *
 */
class Relatorios extends MY_Controller {

	/**
	 * Construtor
	 * @author Hugo Ferreira da Silva
	 * @return Relatorios
	 */
	public function __construct(){
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/relatorios/';

	}

	/**
	 * Lista os relatorios disponiveis para o usuario
	 *
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function index(){
		
		if(isset($_SESSION['ID_JOB'])){
			unset($_SESSION['ID_JOB']);
		} else if(isset($_SESSION['idJob'])){
			unset($_SESSION['idJob']);
		} else if(isset($_SESSION['relatorioObjetoPendente'])){
			unset($_SESSION['relatorioObjetoPendente']);
		}
		
		$sess = Sessao::get('usuario');
		$lista = $this->relatorio->getRelatoriosByUsuario($sess['ID_USUARIO']);

		$this->assign('relatorios', $lista);
		$this->display('index');
	}

	public function relatorio_fluxo($acao = null, $tipo = 'excel'){
		$user = Sessao::get('usuario');

		switch($acao){
			case 'gerar':

				if( !empty($user['ID_CLIENTE']) ){
					$_POST['AGENCIAS'] = getValoresChave($this->agencia->getByCliente($user['ID_CLIENTE']),'ID_AGENCIA');
				}
				if( !empty($user['ID_AGENCIA']) ){
					$_POST['CLIENTES'] = getValoresChave($this->cliente->getByAgencia($user['ID_AGENCIA']),'ID_CLIENTE');
				}
				if( empty($_POST['DATA_INICIO']) ) {
					$_POST['DATA_INICIO'] = date('d/m/Y', strtotime('-30 days'));
				}

				if( empty($_POST['DATA_TERMINO']) ) {
					$_POST['DATA_TERMINO'] = date('d/m/Y');
				}

				$_POST['PRODUTOS'] = getValoresChave(getProdutosUsuario($_POST['ID_CLIENTE'],$_POST['ID_AGENCIA']),'ID_PRODUTO');

				$data = $this->relatorio->relatorioFluxo($_POST);

				$dataCabecalho = array();

				foreach($data as $d ){
					$dataCabecalho['ID_JOB'][] = $d['ID_JOB'];
					$dataCabecalho['TITULO_JOB'][] = $d['TITULO_JOB'];
					$dataCabecalho['DESC_AGENCIA'][] = $d['DESC_AGENCIA'];
					$dataCabecalho['DESC_CLIENTE'][] = $d['DESC_CLIENTE'];
					$dataCabecalho['DESC_PRODUTO'][] = $d['DESC_PRODUTO'];
					$dataCabecalho['DESC_CAMPANHA'][] = $d['DESC_CAMPANHA'];
				}

				$this->assign('agencia', $this->agencia->getById($_POST['ID_AGENCIA']));
				$this->assign('cliente', $this->cliente->getById($_POST['ID_CLIENTE']));
				$this->assign('produto', $this->produto->getById($_POST['ID_PRODUTO']));
				$this->assign('campanha', $this->campanha->getById($_POST['ID_CAMPANHA']));
				$this->assign('cabecalho', $dataCabecalho);
				$this->assign('lista', $data);

				if($tipo == 'excel'){
					header_excel('relatorio_fluxo_'.date('Ymd-His'));
				}

				$this->display('relatorio_fluxo_html');

				break;

			default:
				$this->loadFilters();
				$this->display('relatorio_fluxo');
		}
	}

	/**
	 * Exibe a tela de filtros e monta o relatorio de jobs
	 *
	 * @author Hugo Ferreira da Silva
	 * @param string $acao
	 * @param string $tipo
	 * @return void
	 */
	public function relatorio_jobs($acao = null, $tipo = 'excel'){
		$user = Sessao::get('usuario');

		//print_rr($acao);die;
		switch($acao){
			case 'gerar':
				if( !empty($user['ID_CLIENTE']) ){
					$_POST['AGENCIAS'] = getValoresChave($this->agencia->getByCliente($user['ID_CLIENTE']),'ID_AGENCIA');
				}
				if( !empty($user['ID_AGENCIA']) ){
					$_POST['CLIENTES'] = getValoresChave($this->cliente->getByAgencia($user['ID_AGENCIA']),'ID_CLIENTE');
				}
				if( empty($_POST['DATA_INICIO']) ) {
					$_POST['DATA_INICIO'] = date('d/m/Y', strtotime('-30 days'));
				}

				if( empty($_POST['DATA_TERMINO']) ) {
					$_POST['DATA_TERMINO'] = date('d/m/Y');
				}

				$_POST['PRODUTOS'] = getValoresChave(getProdutosUsuario($_POST['ID_CLIENTE'],$_POST['ID_AGENCIA']),'ID_PRODUTO');

				$data = $this->relatorio->relatorioJobs($_POST);
						
				// prepara os dados para o cabecalho
				$dataCabecalho = array();
				$idJobsRepetidos = array();
				$dataNova = array();
				foreach ( $data as $key => $d ){
					if( !in_array( $d['ID_JOB'], $idJobsRepetidos ) ){
						$dataCabecalho['ID_JOB'][] = $d['ID_JOB'];
						$dataCabecalho['TITULO_JOB'][] = $d['TITULO_JOB'];
						$dataCabecalho['DESC_AGENCIA'][] = $d['DESC_AGENCIA'];
						$dataCabecalho['DESC_CLIENTE'][] = $d['DESC_CLIENTE'];
						$dataCabecalho['DESC_PRODUTO'][] = $d['DESC_PRODUTO'];
						$dataCabecalho['DESC_CAMPANHA'][] = $d['DESC_CAMPANHA'];
						$idJobsRepetidos[] = $d['ID_JOB'];
					}
					unset($d['ID_JOB']);
					unset($d['TITULO_JOB']);
					unset($d['DESC_AGENCIA']);
					unset($d['DESC_CLIENTE']);
					unset($d['DESC_PRODUTO']);
					unset($d['DESC_CAMPANHA']);
					
					$dataNova[$key] = $d;
				}

				$data = $dataNova;
				
				$this->assign('agencia', $this->agencia->getById($_POST['ID_AGENCIA']));
				$this->assign('cliente', $this->cliente->getById($_POST['ID_CLIENTE']));
				$this->assign('produto', $this->produto->getById($_POST['ID_PRODUTO']));
				$this->assign('campanha', $this->campanha->getById($_POST['ID_CAMPANHA']));
				$this->assign('cabecalho', $dataCabecalho);
				$this->assign('lista', $data);

				if($tipo == 'excel'){
					header_excel('relatorio_jobs_'.date('Ymd-His'));
				}
				$this->display('relatorio_jobs_html');

				break;

			default:
				$this->loadFilters();
				$this->display('relatorio_jobs');
		}
	}
	
	/**
	 * Exibe a tela de filtros e monta o relatorio de objetos pendentes
	 *
	 * @author Bruno de Oliveira
	 * @param string $acao
	 * @param string $tipo
	 * @return void
	 */
	public function relatorio_objetos_pendentes($acao = null){
		$user = Sessao::get('usuario');
	
		if(!empty($user['ID_CLIENTE']))
			$filtro['ID_CLIENTE'] = $user['ID_CLIENTE'];
		else
			$filtro['ID_AGENCIA'] = $user['ID_AGENCIA'];
	
		if(isset($_POST['NOME']) && !empty($_POST['NOME']))
			$filtro['NOME'] = $_POST['NOME'];
		else if ( isset($_SESSION['relatorioObjetoPendente']['NOME']) && !empty($_SESSION['relatorioObjetoPendente']['NOME']))
			$filtro['NOME'] = $_SESSION['relatorioObjetoPendente']['NOME'];
	
		if(isset($_POST['ID_PRACA']) && !empty($_POST['ID_PRACA']))
			$filtro['ID_PRACA'] = $_POST['ID_PRACA'];
		else if ( isset($_SESSION['relatorioObjetoPendente']['ID_PRACA']) && !empty($_SESSION['relatorioObjetoPendente']['ID_PRACA']))
			$filtro['ID_PRACA'] = $_SESSION['relatorioObjetoPendente']['ID_PRACA'];
	
		$pracas = array();
		if(isset($_POST['ID_JOB']) && !empty($_POST['ID_JOB'])){
			$filtro['ID_JOB'] = $_POST['ID_JOB'];
			$pracas = $this->praca->getByIdJob($_POST['ID_JOB']);
		} else if ( isset($_SESSION['relatorioObjetoPendente']['ID_JOB']) && !empty($_SESSION['relatorioObjetoPendente']['ID_JOB'])){
			$filtro['ID_JOB'] = $_SESSION['relatorioObjetoPendente']['ID_JOB'];
			$pracas = $this->praca->getByIdJob($filtro['ID_JOB']);
		}
		//validação dos campos
		$erros = array();
		if($acao != null)
			if(isset($filtro['NOME']) && !empty($filtro['NOME']))
				if(strlen($filtro['NOME']) < 4)
					$erros[] = "O campo 'Nome do arquivo' deve conter 4 ou mais carecteres.";
	
		$msg = '';
		if($acao != null && empty($erros) ){
			$lista = $this->relatorio->relatorioObjetosPendentes($filtro);
			$this->assign('lista', $lista);
				
			if( (isset($_POST['ID_JOB'])) && (!empty($_POST['ID_JOB'])) && empty($lista)){
	
				if( (strtolower($user['DESC_CLIENTE']) == "hermes") ||
				(strtolower($user['DESC_AGENCIA']) == "hermes") ||
				(strtolower($user['DESC_AGENCIA']) == "comprafacil") ||
				(strtolower($user['DESC_CLIENTE']) == "comprafacil")){
					$msg = 'O job '.$_POST['ID_JOB'].' não possui produtos com objetos pendentes.';
				} else {
					$msg = 'O job '.$_POST['ID_JOB'].' não possui fichas com objetos pendentes.';
				}
	
			} else if(empty($lista)){
				$msg = 'Não há arquivos cadastrados conforme dados inseridos.';
			}
		}
	
		$jobs = $this->job->getByIdClienteOrIdAgencia($filtro);
	
		$this->assign('erros', $erros);
		$this->assign('msg', $msg);
		$this->assign('podeEditarFicha', Sessao::hasPermission('ficha','form'));
		$this->assign('jobs', $jobs);
		$this->assign('pracas', $pracas);
	
	
	
		if($acao == 'excel' && empty($erros)){
			header_excel('relatorio_objetos_pendentes_'.date('Y-m-d-H:i:s'));
			$this->display('relatorio_objetos_pendentes_html');
		} else {
			$this->display('relatorio_objetos_pendentes');
		}
	}
	
	/**
	 * Exibe a tela de filtros e monta o relatorio de comparação de produtos entre praças
	 *
	 * @author Bruno de Oliveira
	 * @param string $acao
	 * @param string $tipo
	 * @return void
	 */
	public function relatorio_ComparacaoProdutosPracas($acao = null){
		$user = Sessao::get('usuario');
		
		if(!empty($user['ID_CLIENTE'])){
			$filtro['ID_CLIENTE'] = $user['ID_CLIENTE'];
		} else {
			$filtro['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}
		
		if(isset($_POST['ID_PRACA']) && !empty($_POST['ID_PRACA'])){
			$filtro['ID_PRACA'] = $_POST['ID_PRACA'];
		} else if ( isset($_SESSION['RelatorioComparacaoProdutosPracas']['ID_PRACA']) && !empty($_SESSION['RelatorioComparacaoProdutosPracas']['ID_PRACA'])){
			$filtro['ID_PRACA'] = $_SESSION['RelatorioComparacaoProdutosPracas']['ID_PRACA'];
		}
		
		
		$pracas = array();
		if(isset($_POST['ID_JOB']) && !empty($_POST['ID_JOB'])){
			$filtro['ID_JOB'] = $_POST['ID_JOB'];
			$pracas = $this->praca->getByIdJob($_POST['ID_JOB']);
			$dadosJob = $this->job->getNomeById($_POST['ID_JOB']);
			$filtro['ID_EXCEL'] = $this->excel->getIdExcelByIdJob($filtro['ID_JOB']);
		} else if ( isset($_SESSION['RelatorioComparacaoProdutosPracas']['ID_JOB']) && !empty($_SESSION['RelatorioComparacaoProdutosPracas']['ID_JOB'])){
			$filtro['ID_JOB'] = $_SESSION['RelatorioComparacaoProdutosPracas']['ID_JOB'];
			$pracas = $this->praca->getByIdJob($filtro['ID_JOB']);
			$dadosJob = $this->job->getNomeById($filtro['ID_JOB']);
		}
		
		//validação dos campos
		$erros = array();
		if($acao != null){
			if( (isset($_POST['ID_JOB'])) && (!empty($_POST['ID_JOB'])) && (isset($_POST['ID_PRACA'])) && (empty($_POST['ID_PRACA'])) ){
				$erros[] = 'Por gentileza, informe a praça.';
			} else if ( (isset($_POST['ID_JOB'])) && (empty($_POST['ID_JOB'])) && (isset($_POST['ID_PRACA'])) && (empty($_POST['ID_PRACA'])) ){
				$erros[] = 'Por gentileza, preencha os campos de pesquisa.';
			} else if(count($pracas) < 2){
				$erros[] = 'Por gentileza, selecione algum job que contenha 2 ou mais praças.';
			}
		}
		
		$pracaSelecionada = array();
		if(isset($pracas) && !empty($pracas) && isset($filtro['ID_PRACA'])){
			foreach($pracas as $praca){
				if($filtro['ID_PRACA'] == $praca['ID_PRACA']){
					$pracaSelecionada = $praca;
				}
			}
			$this->assign('pracaSelecionada', $pracaSelecionada);
		}
		//consulta os dados do relatório
		if($acao != null && empty($erros) ){
			$lista = $this->relatorio->relatorioComparacaoProdutosPracas($filtro);
			$this->assign('lista', $lista);
			$this->assign('pracas', $pracas);
			$this->assign('dadosJob', $dadosJob);
		}
		
		$jobs = $this->job->getByIdClienteOrIdAgencia($filtro);
		
		$this->assign('erros', $erros);
		$this->assign('jobs', $jobs);
		
		if($acao == 'imprimir' && empty($erros)){
			$html = $this->load->view('ROOT/relatorios/relatorio_ComparacaoProdutosPracas_html', $this->data, true);
			echo $html;
			if( isset($html) && !empty($html) ){
				echo '<script>window.print();</script>';
			} else{
				echo "<script>alert('".lang('nao_ha_resultado')."');window.close();</script>";
				echo "<script>setTimeout('window.close()',500);</script>";
			}
		} else {
			$this->display('relatorio_ComparacaoProdutosPracas');
		}
	}

	/**
	 * Exibe a tela de filtros e monta o relatorio de jobs
	 *
	 * @author Juliano Polito
	 * @param string $acao
	 * @param string $tipo
	 * @return void
	 */
	public function relatorio_pricing($acao = null, $tipo = 'excel'){

		switch($acao){
			case 'gerar':
				if( !empty($user['ID_CLIENTE']) ){
					$_POST['AGENCIAS'] = getValoresChave($this->agencia->getByCliente($user['ID_CLIENTE']),'ID_AGENCIA');
				}
				if( !empty($user['ID_AGENCIA']) ){
					$_POST['CLIENTES'] = getValoresChave($this->cliente->getByAgencia($user['ID_AGENCIA']),'ID_CLIENTE');
				}
				if( empty($_POST['DATA_INICIO']) ) {
					$_POST['DATA_INICIO'] = date('d/m/Y', strtotime('-30 days'));
				}

				if( empty($_POST['DATA_TERMINO']) ) {
					$_POST['DATA_TERMINO'] = date('d/m/Y');
				}

				$_POST['PRODUTOS'] = getValoresChave(getProdutosUsuario($_POST['ID_CLIENTE'],$_POST['ID_AGENCIA']),'ID_PRODUTO');

				$data = $this->relatorio->relatorioPricing($_POST);

				// prepara os dados para o cabecalho
				$dataCabecalho = array();
				$idJobsRepetidos = array();
				foreach ( $data as $d ){
					if( !in_array( $d['ID_JOB'], $idJobsRepetidos ) ){
						$dataCabecalho['ID_JOB'][] = $d['ID_JOB'];
						$dataCabecalho['TITULO_JOB'][] = $d['TITULO_JOB'];
						$dataCabecalho['DESC_AGENCIA'][] = $d['DESC_AGENCIA'];
						$dataCabecalho['DESC_CLIENTE'][] = $d['DESC_CLIENTE'];
						$dataCabecalho['DESC_PRODUTO'][] = $d['DESC_PRODUTO'];
						$dataCabecalho['DESC_CAMPANHA'][] = $d['DESC_CAMPANHA'];
						$dataCabecalho['DESC_PRACA'][] = $d['DESC_PRACA'];
						$idJobsRepetidos[] = $d['ID_JOB'];
					}
				}

				$itensPai = array();
				$itensFilha = array();

				foreach($data as $item){
					if(isset($item['ID_FICHA_PAI']) && ($item['ID_FICHA_PAI'] > 0)){
						$item['TIPO'] = 'Filha';
						array_push($itensFilha, $item);
					}
					else{
						$filhos = $this->excelItem->getFilhasByVersaoPraca($item['ID_EXCEL_VERSAO'], $item['ID_PRACA'], $item['ID_FICHA']);

						if(count($filhos) > 0){
							$item['TIPO'] = 'Pai';
						}
						else{
							$item['TIPO'] = '';
						}

						array_push($itensPai, $item);
					}
				}

				$this->assign('agencia', $this->agencia->getById($_POST['ID_AGENCIA']));
				$this->assign('cliente', $this->cliente->getById($_POST['ID_CLIENTE']));
				$this->assign('produto', $this->produto->getById($_POST['ID_PRODUTO']));
				$this->assign('campanha', $this->campanha->getById($_POST['ID_CAMPANHA']));
				$this->assign('praca', $this->praca->getById(post('ID_PRACA',true)));
				$this->assign('lista', $itensPai);
				$this->assign('listaFilha', $itensFilha);
				$this->assign('cabecalho', $dataCabecalho);

				if($tipo == 'excel'){
					header_excel('relatorio_pricing_'.date('Ymd-His'));
				}

				$this->display('relatorio_pricing_html');

				break;

			default:
				$this->loadFilters();
				$this->display('relatorio_pricing');
		}
	}

	/**
	 * Tela para filtrar os dados do relatorio de pendencias do job
	 *
	 * Tambem efetua o relatorio em si, dependendo da acao
	 * que for enviada como parametro.
	 *
	 * Permite tambem que o usuario gere o relatorio em dois formatos:
	 * - HTML
	 * - Excel (XLS)
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $acao Acao a ser tomada. O padrao e exibir a tela de filtros
	 * @param string $tipo Tipo de saida do relatorio
	 * @return
	 */
	public function divergencias_espelho_job($acao = null, $tipo = 'excel'){
		switch( $acao ){
			case 'gerar':
				if( !empty($user['ID_CLIENTE']) ){
					$_POST['AGENCIAS'] = getValoresChave($this->agencia->getByCliente($user['ID_CLIENTE']),'ID_AGENCIA');
				}
				if( !empty($user['ID_AGENCIA']) ){
					$_POST['CLIENTES'] = getValoresChave($this->cliente->getByAgencia($user['ID_AGENCIA']),'ID_CLIENTE');
				}

				if( empty($_POST['DATA_INICIO']) ) {
					$_POST['DATA_INICIO'] = date('d/m/Y', strtotime('-30 days'));
				}

				if( empty($_POST['DATA_TERMINO']) ) {
					$_POST['DATA_TERMINO'] = date('d/m/Y');
				}

				$_POST['PRODUTOS'] = getValoresChave(getProdutosUsuario($_POST['ID_CLIENTE'],$_POST['ID_AGENCIA']),'ID_PRODUTO');

				$data = $this->relatorio->divergenciasEspelhoJob($_POST);

				$this->assign('agencia', $this->agencia->getById($_POST['ID_AGENCIA']));
				$this->assign('cliente', $this->cliente->getById($_POST['ID_CLIENTE']));
				$this->assign('produto', $this->produto->getById($_POST['ID_PRODUTO']));
				$this->assign('campanha', $this->campanha->getById($_POST['ID_CAMPANHA']));
				$this->assign('praca', $this->praca->getById(post('ID_PRACA',true)));
				$this->assign('lista', $data);


				if($tipo == 'excel'){
					header_excel('divergencias_espelho_espelho_'.date('Ymd-His'));
				}

				$this->display('divergencias_espelho_job_html');
				break;

			default:
				$this->loadFilters();
				$this->display('divergencias_espelho_job');
		}
	}

	/**
	 * Exibe a tela de filtros e grid e monta o relatorio de jobs
	 *
	 *
	 * @author Nelson Martucci
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $pagina Numero da pagina atual
	 * @param string $acao Acao a ser tomada (pesquisar | gerar)
	 * @param string $tipo Tipo de saida do relatorio (excel | tela)
	 * @param int $idJob Codigo do job para ser gerado o relatorio
	 * @return void
	 */
	public function comparar_pracas($pagina = 0, $acao = 'pesquisar', $tipo = null, $idJob = null){

		switch($acao){
			case 'gerar':
				// se nao informou o job
				if( empty($idJob) ){
					redirect('relatorios/comparar_pracas');
				}

				// pegando os dados do job
				$job = $this->job->getById($idJob);

				// pracas do job
				$pracas = $this->job->getPracas($idJob);

				// pegando os dados do relatorio
				$relatorio = $this->relatorio->comparativoPracas($idJob);

				// colocando os dados na view
				$this->assign('relatorio', $relatorio);
				$this->assign('pracas', $pracas);
				$this->assign('job', $job);
				$this->assign('tipo', $tipo);

				// se for para exportar em excel
				if($tipo == 'excel'){
					header_excel('relatorio_comparar_pracas_'.date('Ymd-His'));
				}

				// exibe o resultado
				$this->display('relatorio_comparar_pracas_html');

				break;

			case 'pesquisar':
				// se enviou dados
				if(!empty($_POST)){
					// altera a sessao
					Sessao::set('busca', $_POST);
				}

				$filters = (array) Sessao::get('busca');
				$user = Sessao::get('usuario');

				$field = val($filters,'ORDER','TB_JOB.ID_JOB');
				$direction = val($filters,'ORDER_DIRECTION','DESC');

				$dados = $filters;
				$dados['STATUS_CAMPANHA'] = 1;
				$dados['PRODUTOS'] = getValoresChave(getProdutosUsuario(),'ID_PRODUTO');

				$pagina = sprintf('%d',$pagina);
				$limit = val($filters,'pagina',5);

				if( !empty($user['ID_AGENCIA']) ){
					$dados['ID_AGENCIA'] = $user['ID_AGENCIA'];
				}

				if( !empty($user['ID_CLIENTE']) ){
					$dados['ID_CLIENTE'] = $user['ID_CLIENTE'];
				}

				$lista = $this->job->listItems($dados, $pagina, $limit, $field, $direction);

				$this->assign('lista', $lista['data']);
				$this->assign('busca', $filters);
				$this->assign('paginacao', linkpagination($lista, $limit));
				$this->assign('field', $field);
				$this->assign('direction', $direction);

			default:
				$this->loadFilters( $filters );
				$this->display('relatorio_comparar_pracas');
		}
	}



	/**
	 * Exibe a tela de filtros e monta o relatorio de relação de usuários
	 *
	 * @author Vanessa G. de Carvalho
	 * @param string $acao Acao a ser tomada. O padrao e exibir a tela de filtros
	 * @param string $tipo Tipo de saida do relatorio
	 * @return void
	 */

	public function relatorio_relacao_usuarios($acao = null, $tipo = 'excel'){
		$user = Sessao::get('usuario');
		$status = array();
		
		if(is_numeric($user['ID_CLIENTE'])){
			$this->assign('usuario_cliente', 1);
		}
		else{
			$this->assign('usuario_cliente', 0);
		}

		switch($acao){
			case 'gerar':
				if( !empty($user['ID_CLIENTE']) ){
					$_POST['AGENCIAS'] = getValoresChave($this->agencia->getByCliente($user['ID_CLIENTE']),'ID_AGENCIA');
				}
				if( !empty($user['ID_AGENCIA']) ){
					$_POST['CLIENTES'] = getValoresChave($this->cliente->getByAgencia($user['ID_AGENCIA']),'ID_CLIENTE');
				}
				//se não informou a data inicial traz a data final menos 30 dias
				if( empty($_POST['DATA_INICIO']) ) {
					$_POST['DATA_INICIO'] = date('d/m/Y', strtotime('-30 days'));
				}
				//se não informou a data final traz a data atual do sistema
				if( empty($_POST['DATA_TERMINO']) ) {
					$_POST['DATA_TERMINO'] = date('d/m/Y');
				}

				$data = $this->relatorio->relatorioRelacaoUsuarios($_POST);

				$this->assign('agencia', $this->agencia->getById($_POST['ID_AGENCIA']));
				$this->assign('cliente', $this->cliente->getById($_POST['ID_CLIENTE']));
				$this->assign('status', $_POST['STATUS_USUARIO']);
				$this->assign('lista', $data);

				if($tipo == 'excel'){
					header_excel('relatorio_relacao_usuarios_'.date('Ymd-His'));
				}

				$this->display('relatorio_relacao_usuarios_html');
				break;

			default:
				$this->loadFilters();
				$this->display('relatorio_relacao_usuarios');
		}
	}


	/**
	 * Carrega os dados que vao ser usados no formulario
	 *
	 * @author Hugo Ferreira da Silva
	 * @param array $data
	 * @return void
	 */
	protected function loadFilters(array $data = array()){
		$sess = Sessao::get('usuario');

		$this->assign('agencias', array());
		$this->assign('clientes', array());
		$this->assign('produtos', array());
		$this->assign('campanhas', array());
		$this->assign('campanhas2', array());
		$this->assign('pracas', array());
		$this->assign('tipopeca', array());
		
		// Carrega os Tipos de Peça
		if(!empty($sess['ID_CLIENTE']))	{
			$this->assign('tipopeca',$this->tipo_peca->getTiposAtivos($sess['ID_CLIENTE']));
		}
		
		// se o usuario logado esta vinculado
		// a uma agencia
		if(!empty($sess['ID_AGENCIA'])){
			// carrega so os clientes dele
			$this->assign('clientes', $this->cliente->getByAgencia($sess['ID_AGENCIA']));
			$this->assign('agencias', array($this->agencia->getById($sess['ID_AGENCIA'])));
		}

		// se o usuario logado esta vinculado
		// a um cliente
		if(!empty($sess['ID_CLIENTE'])){
			// carrega as agencias
			$this->assign('agencias', $this->agencia->getByCliente($sess['ID_CLIENTE']));
			$this->assign('clientes', array($this->cliente->getById($sess['ID_CLIENTE'])));
		}

		if( empty($data['ID_AGENCIA']) ){
			// carrega os produtos
			$this->assign('produtos', getProdutosUsuario());

		} else {
			$this->assign('produtos', $this->produto->getByAgencia($data['ID_AGENCIA'], $data['ID_CLIENTE']));

		}

		$pracas = array();

		// pega as pracas que tem acesso
		$codigos = getValoresChave( getProdutosUsuario(), 'ID_PRODUTO');
		if( !empty($codigos) ){
			$pracas = $this->praca->getByProdutos($codigos);
		}

		if( !empty($data['ID_PRODUTO']) ){
			$this->assign('campanhas', $this->campanha->getByProduto($data['ID_PRODUTO']));
			$this->assign('campanhas2', $this->campanha->getByProduto2($data['ID_PRODUTO']));
		}

		$this->assign('pracas', $pracas);
	}

/**
* Relatório de job por campanhas
*
* @author Gabriel Silva
* @param string $acao
* @return void
*/
	public function relatorio_campanha($acao = null, $tipo = null)	{
	// Pega dados do Usuário
		$user = Sessao::get('usuario');
	
	// Verifica se o usuário é Cliente ou agência e pega o ID
		if(!empty($user['ID_CLIENTE']))	{
			$filtro['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}	else	{
			$filtro['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}
	
		// Vê se selecionou pelo menos a campanha
		$erros = array();
		if($acao != null)	{
			if(isset($_POST['ID_CAMPANHA']) && empty($_POST['ID_CAMPANHA']))	{
				$erros[] = "Por favor, selecione ao menos 1 campanha";
			}
		}
	
		// Gera o Excel
		if($acao == "gerar" && empty($erros))	{
			
			// Pega o ID de Post da campanha para filtro
			if(isset($_POST['ID_CAMPANHA']) && !empty($_POST['ID_CAMPANHA']))	{
				$filtro['ID_CAMPANHA'] = $_POST['ID_CAMPANHA'];
			}
	
			// Pega o ID de Tipo de Peça
			if(isset($_POST['ID_TIPO_PECA']) && !empty($_POST['ID_TIPO_PECA']))	{
				$filtro['ID_TIPO_PECA'] = $_POST['ID_TIPO_PECA'];
			}	else if(isset($_POST['ID_TIPO_PECA']) && empty($_POST['ID_TIPO_PECA']))	{
				$filtro['ID_TIPO_PECA'] = 0;
			}
			$dados = array();
			$dados = $this->relatorio->RelatorioProdutosCampanhaJob($filtro['ID_CAMPANHA'],$filtro['ID_TIPO_PECA']);
			
			if(empty($dados))	{
				$msg = "Nada para consultar";
			}
			
			$this->assign('msg', $msg);
			$this->assign('dados', $dados);
			
			header_excel('relatorio_campanha_'.date('Ymd-His'));
			$this->display('relatorio_campanha_html');
		}
		$this->assign('erros', $erros);
		$this->loadFilters();
		$this->display('relatorio_campanha');
	
	}


}

