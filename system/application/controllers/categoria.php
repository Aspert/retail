<?php
/**
 * Controller para gerenciar categorias
 * 
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class Categoria extends MY_Controller {
	
	/**
	 * Construtor
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Categoria
	 */
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/categoria/';
	}
	
	/**
	 * Lista as categorias cadastradas
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $pagina Numero da pagina atual
	 * @return void
	 */
	function lista($pagina = 0) {
		if ( !empty($_POST) ) {
			Sessao::set('busca', $_POST);
		}
		
		$busca = (array) Sessao::get('busca');
		$limit = empty($busca['pagina']) ? 0 : $busca['pagina'];
		
		// carrega as agencias
		$res = $this->agencia->listItems(array('STATUS_AGENCIA'=>1),0,10000);

		$user = Sessao::get('usuario');
		
		// carrega os clientes
		$filters = array(
			'STATUS_CLIENTE' => 1 
		);
		
		if( !empty($user['ID_CLIENTE']) ){
			$busca['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}
		
		$clientes = $this->cliente->listItems($filters, 0, 1000);
		$this->data['clientes'] = $clientes['data'];
		
		$categorias = $this->categoria->listItems($busca, $pagina, $limit);
		
		$this->data['busca'] = $busca;
		$this->data['agencias'] = $res['data'];
		$this->data['categorias'] = $categorias['data'];
		$this->data['paginacao'] = linkpagination($categorias,$limit);
		$this->data['podeAlterar'] = Sessao::hasPermission('categoria','save');
		
		$this->display('index');
	}
	
	/**
	 * Exibe o formulario de edicao de categoria
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo da categoria quando editando
	 * @return void
	 */
	function form($id=null) {
		if( !is_null($id) && $_SERVER['REQUEST_METHOD'] == 'GET' ){
			$_POST = $this->categoria->getById($id);
		}
		
		// carrega as agencias
		//$res = $this->agencia->listItems(array('STATUS_AGENCIA'=>1),0,10000);
		
		$user = Sessao::get('usuario');

		$filters = array(
			'STATUS_CLIENTE' => 1
		);
		
		$clientes = $this->cliente->listItems($filters, 0, 1000);

		$this->data['clientes'] = $clientes['data'];

		$this->display('form');
	}
	
	/**
	 * Grava uma nova categoria ou as alteracoes de uma existente
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo da categoria a ser alterada ou null para uma nova
	 * @return void
	 */
	function save($id=null) {
		$_POST['ID_CATEGORIA'] = sprintf('%d', $id);
		// faz a validacao dos dados
		$res = $this->categoria->validate($_POST);
		// se nao houve erros

		if( empty($res) ){
			//grava os dados no banco
			$this->categoria->save($_POST,$id);
			// redireciona para a pagina
			redirect('categoria/lista');
		}
		
		// indica os erros
		$_REQUEST['erros'] = $res;
		// carrega o formulario
		$this->form($id);
	}
}
