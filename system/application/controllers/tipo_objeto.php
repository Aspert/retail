<?php
/**
 * Controller para gerenciar agencias
 * @author Hugo Silva
 *
 */
class Tipo_objeto extends MY_Controller {
	/**
	 * Construtor
	 * @author Hugo Silva
	 * @return Cliente
	 */
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/tipo_objeto/';
	}
	
	/**
	 * Lista os itens cadastrados
	 * 
	 * @author Hugo Silva
	 * @param int $pagina Pagina inicial da listagem
	 * @return void
	 */
	public function lista($pagina=0){
		if (!empty($_POST)) {
			Sessao::set('busca',$_POST);			
		}
		
		$data = (array) Sessao::get('busca');
		
		$limit = !empty($data['pagina']) ? $data['pagina'] : 5;
		
		$lista = $this->tipo_objeto->listItems($data, $pagina, $limit);
		$this->loadFilters($data);
		
		$this->data['podeAlterar'] = Sessao::hasPermission('tipo_objeto','save');
		$this->data['busca'] = $data;
		$this->data['lista'] = $lista['data'];
		$this->data['paginacao'] = linkpagination($lista, $limit);
		$this->display('index');		
	}
	
	/**
	 * Exibe o formulario para edicao ou insercao
	 * 
	 * @author Hugo Silva
	 * @param int $id codigo da agencia
	 * @return void
	 */
	public function form($id=null){
		if(!is_null($id) && $_SERVER['REQUEST_METHOD'] == 'GET') {
			$_POST = $this->tipo_objeto->getById($id);
		}
		
		$this->loadFilters($_POST);
		$this->display('form');
	}
	
	/**
	 * Grava os dados do formulario
	 * 
	 * @author Hugo Silva
	 * @param int $id
	 * @return void
	 */
	public function save($id=null){
		
		if( empty($id) ){
			$_POST['CHAVE_STORAGE_TIPO_OBJETO'] = ajustaChaveStorage($_POST['DESC_TIPO_OBJETO']);
		}
		
		$_POST['ID_TIPO_OBJETO'] = $id;
		$erros = $this->tipo_objeto->validate($_POST);
		
		if(empty($erros)){
			$id = $this->tipo_objeto->save($_POST,$id);
			redirect('tipo_objeto/lista');
		}
		
		$_REQUEST['erros'] = $erros;
		$this->data['erros'] = $erros;
		$this->form($id);
	}
	
	/**
	 * carrega os filtros padrao
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data
	 * @return void
	 */
	private function loadFilters($data){
		// pega os dados da sessao
		$user = Sessao::get('usuario');
		
		$filters = array();
		$filters['STATUS_CLIENTE'] = 1;
		if( !empty($user['ID_CLIENTE']) ){
			$filters['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}
		
		$rs = $this->cliente->listItems($filters, 0, 100000);
		$this->data['clientes'] = $rs['data'];
	}
	
}