<?php

class Gerar_indd extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/importacao_excel/';
	}

	/**
	 * Lista os jobs que estao com a agencia
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $pagina
	 * @return void
	 */
	public function listar_agencia($pagina = 0){
		if(!empty($_POST)){
			Sessao::set('busca', $_POST);
		}

		$busca = (array) Sessao::get('busca');
		$busca['ID_ETAPA'] = array($this->etapa->getIdByKey('ENVIADO_AGENCIA'),$this->etapa->getIdByKey('APROVACAO'));;
		$busca['PRODUTOS'] = getValoresChave(getProdutosUsuario(),'ID_PRODUTO');

		$user = Sessao::get('usuario');

		$this->loadFilters($busca);

		// verificamos se tem algo na sessao
		// nestes casos independem do formulario
		$this->setSessionData($busca);

		if(empty($busca['ORDER'])){
			$busca['ORDER'] = 'DATA_INICIO';
		}

		if(empty($busca['ORDER_DIRECTION'])){
			$busca['ORDER_DIRECTION'] = 'DESC';
		}

		$busca['STATUS_CAMPANHA'] = 1;

		$limit = empty($busca['pagina']) ? 0 : $busca['pagina'];
		$lista = $this->excel->listItems($busca, $pagina, $limit, $busca['ORDER'], $busca['ORDER_DIRECTION']);

		$this->data['busca'] = $busca;
		$this->data['lista'] = $lista['data'];
		$this->data['paginacao'] = linkpagination($lista , $limit);
		$this->data['podeGerar'] = Sessao::hasPermission('gerar_indd','form');
		$this->data['podeBaixar'] = Sessao::hasPermission('importacao_excel','download');
		$this->data['verHistorico'] = Sessao::hasPermission('importacao_excel','historico');

		$this->data['podeObjeto'] = Sessao::hasPermission('gerar_indd','objetos');

		$this->display('index_indd');
	}

	/**
	 * Salva o template escolhido pelo usuario
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $id Codigo do excel
	 * @return void
	 */
	public function save($id){

		// se escolheu o template
		if(!empty($_POST['ID_TEMPLATE'])){
			$this->excel->save($_POST, $id);
			redirect('gerar_indd/gera_indd/' . $id);
		}

		redirect('gerar_indd/form/'.$id);
	}

	/**
	 * Exibe a tela para escolha do template para gerar o indd
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idexcel
	 * @return void
	 */
	public function form($idexcel){
		$xls = $this->excel->getById($idexcel);

		$this->checaPermissaoAlterar($xls['ID_JOB']);

		$filtro = array('ID_CAMPANHA' => $xls['ID_CAMPANHA']);
		$filtro['TEMPLATE_EXISTS']  = 1;
		$filtro['STATUS_TEMPLATE']  = 1;
		$filtro['NUMERO_PAGINAS']   = $xls['PAGINAS_JOB'];
		$filtro['ALTURA_TEMPLATE']  = $xls['ALTURA_JOB'];
		$filtro['LARGURA_TEMPLATE'] = $xls['LARGURA_JOB'];

		$templates = $this->template->listItems($filtro, 0, 3000);

		$this->assign('job', $this->job->getById($xls['ID_JOB']));

		foreach ($templates['data'] as &$item) {
			$path = $this->template->getPath($item['ID_TEMPLATE']);
			$file = $item['ID_TEMPLATE'].'.pdf';
			$filePath = utf8MAC($path.$file);
			$preview = '';

			if(is_file($filePath)){
				$fileID = $this->xinet->getFilePorPath($path,$file);
				$preview = $fileID;
			}

			$item['PREVIEW'] = $preview;
		}

		$this->assign('templates', $templates['data']);
		$this->display('escolher_template');
	}

	/**
	 * Gera os arquivos Indd do Excel enviado
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idExcel codigo do excel
	 * @return void
	 */
	public function gera_indd($idExcel, $acao = null, $idpraca = null, $idTemplateItem = null){

		$versao = $this->excelVersao->getVersaoAtual($idExcel);
		$this->data['pracas'] = $this->excel->getPracas($idExcel, $versao['ID_EXCEL_VERSAO']);
		$excel = $this->excel->getById($idExcel);

		$this->checaPermissaoAlterar($excel['ID_JOB']);

		// partes dos templates
		$this->assign('arquivos', $this->templateItem->getByTemplate($excel['ID_TEMPLATE']));

		// nesta parte, sera chamado por
		if($acao == 'gerar' && !is_null($idpraca) && !is_null($idTemplateItem)){
			$praca     = $this->praca->getById($idpraca);

			// pega o item
			$item = $this->templateItem->getById($idTemplateItem);

			$nomePraca = $praca['DESC_PRACA'];

			$xmlName   = sprintf('%d-%s-%d-%d-%d',
				$excel['ID_EXCEL'],
				$excel['ID_JOB'],
				$idpraca,
				$item['PAGINA_INICIO_TEMPLATE_ITEM'],
				$item['PAGINA_FIM_TEMPLATE_ITEM']
			);

			$inddName  = sprintf('%d-%s-%d-%d',
				$excel['ID_EXCEL'],
				$nomePraca,
				$item['PAGINA_INICIO_TEMPLATE_ITEM'],
				$item['PAGINA_FIM_TEMPLATE_ITEM']
			);

			$job       = $excel['NUMERO_JOB_EXCEL'];
			$template  = $this->templateItem->getFile($idTemplateItem);
			$file 	   = 'files/layout/'.$inddName.'.indd';

			// apaga se existir
			if(file_exists($file) && is_writable($file)){
				unlink($file);
			}

			// se nao encontrar o arquivo de template
			if(empty($template) || !file_exists($template)){
				die('Arquivo de template não encontrado: ' . $template);
			}

			// configuracao de site atual
			$site = $this->config->item("site");
			// caminho do indesign original
			$indd = $template;
			// caminho do indesign alvo
			$indd_save = $this->config->item('ponto_montagem') . $this->config->item('files_path') . "/layout/$inddName.indd";
			// caminho que o indesign server deve procurar o xml
			$xml = $this->config->item('ponto_montagem') . $this->config->item('files_path') . "/xml/$xmlName.xml";

			$fpo = str_replace(
				$this->config->item('caminho_storage'),
				$this->config->item('caminho_storage_fpo'),
				substr($indd, 0, strrpos($indd,'/')).'/'
			);

			$argumentos = array(
				array('name' => 'base_indd', 'value' => rawurlencode($indd)),
				array('name' => "base_xml", 'value' => rawurlencode($xml)),
				array('name' => "save_indd", 'value' => rawurlencode($indd_save)),
				array('name' => 'path_fpo', 'value' => rawurlencode($fpo)),
			);

			// agenda a chamada para o IDS Kiwi
			$pid = $this->ids->kiwi('retail', 'retail', $argumentos, site_url('idskiwi/callback/'.$idExcel.'/'.$idpraca.'/'.$idTemplateItem));
			die('Processo agendado: '.$pid);
		}

		// se for so para gerar o xml
		if( $acao == 'xml' ){
			// pega o item
			$item = $this->templateItem->getById($idTemplateItem);
			// gera o xml da praca indicada
			$this->excel->gerarXML($idExcel, false, $idpraca, $item['PAGINA_INICIO_TEMPLATE_ITEM'], $item['PAGINA_FIM_TEMPLATE_ITEM']);
			// termina a execucao
			die('xml gerado');
		}

		// se ja terminou de todas as pracas
		if($acao == 'final'){
			$dados['texto'] = empty($_POST['texto']) ? '' : $_POST['texto'];
			$this->excel->gerarXML($idExcel, $excel['NOVO_EXCEL'] == 0);

			$job = $this->job->getById($excel['ID_JOB']);
			$dados = array_merge($excel,$dados);

			$dados['DATA_INICIO'] = format_date_to_form($excel['DATA_INICIO']);
			$dados['DATA_TERMINO'] = format_date_to_form($excel['DATA_TERMINO']);

			//envia emails de notificacao associado a regra
			$erros = $this->email->sendEmailRegra('importacao_processar',
													'Pré-diagramação do Job '.$dados['TITULO_JOB'],
													'ROOT/templates/importacao_processar',
													array(
														'objeto'=>$dados,
														'job'=>$job,
													),
													$excel['ID_AGENCIA'],
													$excel['ID_CLIENTE']
												);

			// $this->email->sendEmail('ROOT/templates/email_importacao_excel', $dados);
			Sessao::clear('busca');
			die('Processo finalizado');
		}

		$this->display('gera_indd');
	}

	/**
	 * Exibe os objetos associados ao job
	 *
	 * @author juliano.polito
	 * @param int $id
	 * @return void
	 */
	public function objetos($id = null,$pagina = 0, $xml = 0){
		if(is_null($id)){
			redirect('gerar_indd','listar_agencia');
		}

		// template a ser mostrado
		$tpl = 'objetos';
		// pega o job
		$job = $this->excel->getById($id);

		// se for para gerar o xml
		if( $xml == 1 ){
			// objetos
			$objs = array();
			// pega as fichas do job
			$fichasJob = $this->job->getFichasUnicas($job['ID_JOB']);
			// lista de fichas
			$fichas = array();
			// para cada ficha encontrada
			foreach($fichasJob as $ficha){
				// guarda o id da ficha
				$fichas[] = $ficha['ID_FICHA'];
			}
			// pega os fileID's dos objetos das fichas
			$objs = $this->objeto_ficha->getByFichas($fichas);

			// atribuicao de valores
			$this->data['objetos'] = $objs;

			// muda o template
			$tpl = 'xml_downloader';
			// muda o cabecalho
			header("Content-Type: text/xml");
		}

		// atribuicao de valores
		$this->data['job'] = $job['TITULO_JOB'];
		$this->data['podeDownloadObjeto'] = Sessao::hasPermission('objeto','download');

		$this->display($tpl);
	}

	private function trocaCodigosPorNomes(&$lista){

		if(empty($lista)){
			return;
		}

		$cats = array(0);
		$subs = array(0);
		$mars = array(0);
		$tipos = array(0);

		foreach($lista['data'] as $key => $item){
			$cats[] = $item['CATEGORIA'];
			$subs[] = $item['SUBCATEGORIA'];
			$mars[] = $item['MARCA'];
			$tipos[] = $item['TIPO'];
		}

		$cats = array_unique($cats);
		$subs = array_unique($subs);
		$mars = array_unique($mars);
		$tipos = array_unique($tipos);

		$cats_nomes = $this->categoria->getCategorias($cats);
		$subs_nomes = $this->subcategoria->getSubcategorias($subs);
		$mars_nomes = $this->marca->getMarcas($mars);
		$tipos_nomes = $this->tipo_objeto->getTipos($tipos);

		foreach($lista['data'] as $key => $item){
			$lista['data'][$key]['CATEGORIA'] = !empty($cats_nomes[$item['CATEGORIA']]) ? $cats_nomes[$item['CATEGORIA']] : '';
			$lista['data'][$key]['SUBCATEGORIA'] = !empty($subs_nomes[$item['SUBCATEGORIA']]) ? $subs_nomes[$item['SUBCATEGORIA']] : '';
			$lista['data'][$key]['MARCA'] = !empty($mars_nomes[$item['MARCA']]) ? $mars_nomes[$item['MARCA']] : '';
			$lista['data'][$key]['TIPO'] = !empty($tipos_nomes[$item['TIPO']]) ? $tipos_nomes[$item['TIPO']] : '';
			$lista['data'][$key]['ID_TIPO'] = $item['TIPO'];
		}

	}

	/**
	 * Mostra o historico de um excel
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id
	 * @return void
	 */
	public function historico($id=null){
		if(is_null($id)){
			redirect('gerar_indd','lista');
		}

		$excel = $this->excel->getById($id);
		$historico = $this->excelVersao->getLastDifferences($id);

		$this->data['excel'] = $excel;
		$this->data['historico'] = $historico;
		$this->display('historico');
	}

	/**
	 * Carrega os filtros para exibir na pagina
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data
	 * @return void
	 */
	private function loadFilters(array $data){
		$user = Sessao::get('usuario');
		// verificamos se tem algo na sessao
		// nestes casos independem do formulario
		if(!empty($user['ID_AGENCIA'])){
			$data['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}

		if(!empty($user['ID_CLIENTE'])){
			$data['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}

		if(!empty($user['ID_PRODUTO'])){
			$data['ID_PRODUTO'] = $user['ID_PRODUTO'];
		}

		//////////////////////////////////////////////////////
		// filtros
		//////////////////////////////////////////////////////
		$user['STATUS_AGENCIA'] = 1;
		$this->assign('produtos', array());

		if(!empty($data['ID_AGENCIA'])){
			$this->assign('clientes', $this->cliente->getByAgencia($data['ID_AGENCIA']));
		} else {
			$this->assign('clientes', array());
		}

		if(!empty($data['ID_CLIENTE'])){
			$this->assign('agencias', $this->agencia->getByCliente($data['ID_CLIENTE']));
			$this->assign('produtos', getProdutosUsuario(val($data,'ID_CLIENTE'), val($data,'ID_AGENCIA')));
		} else {
			$this->assign('agencias', array());
		}

		if(!empty($data['ID_PRODUTO'])){
			$cl = $this->campanha->listItems(array("ID_PRODUTO"=>$data['ID_PRODUTO'],'STATUS_CAMPANHA'=>1),0,10000);
			$this->data['campanhas'] = $cl['data'];
		} else {
			$this->data['campanhas'] = array();
		}


		$tipo = $this->tipoPeca->getAll(0,1000,'DESC_TIPO_PECA');
		$this->data['tipos'] = $tipo['data'];

		$tipo = $this->tipoJob->getAll(0,1000,'DESC_TIPO_JOB');
		$this->data['tiposJob'] = $tipo['data'];
	}


	/**
	 * Checa se o usuario logado tem permissao ou nao de alterar o job acessado
	 *
	 * Se nao tiver permissao, redireciona para a listagem
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idjob
	 * @return void
	 */
	protected function checaPermissaoAlterar($idjob){
		$user = Sessao::get('usuario');
		$job = $this->job->getById($idjob);
		$produtos = getValoresChave(getProdutosUsuario($job['ID_CLIENTE'],$job['ID_AGENCIA']),'ID_PRODUTO');
		$goto = 'gerar_indd/listar_agencia';

		if( $job['ID_ETAPA'] != $this->etapa->getIdByKey('ENVIADO_AGENCIA') ){
			redirect($goto);
		}

		if(!empty($user['ID_CLIENTE'])){
			if($job['ID_CLIENTE'] != $user['ID_CLIENTE']){
				redirect($goto);
			}

			if(!in_array($job['ID_PRODUTO'],$produtos)){
				redirect($goto);
			}
		}

		if(!empty($user['ID_AGENCIA'])){
			if($user['ID_AGENCIA'] != $job['ID_AGENCIA']){
				redirect($goto);
			}

			$clientes = getValoresChave($this->cliente->getByAgencia($user['ID_AGENCIA']),'ID_CLIENTE');
			if(!in_array($job['ID_PRODUTO'],$produtos)){
				redirect($goto);
			}

			if(!in_array($job['ID_CLIENTE'],$clientes)){
				redirect($goto);
			}
		}
	}
}
