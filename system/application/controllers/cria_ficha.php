<?php
class Cria_ficha extends MY_Controller {
	
	function __construct(){
		parent::__construct();
		$this->_templatesBasePath = "ROOT/cria_ficha/";
		
		$this->load->library('DBXinet');
	}

	function form(){
		
		$agencias = $this->agencia->getAll();
		$this->data['agencias'] = $agencias['data'];
		
		$this->display('form');
	}
	
	function create(){
		$user = Sessao::get('usuario');
		$idUsuario = $user['ID_USUARIO'];
		
		$idTipo = $_POST['ID_TIPO_OBJETO'];
		$usaKeyword = isset($_POST['USA_KEYWORD']) ? 1 : 0;
		
		$objFound = 0;
		
		$fichasFound = array();
		$fichasCreated = array();
		$fichasNotCreated = array();
		
		$agencia = array();
		$cliente = array();
		$tipo = array();
		
		if(!empty($idTipo)){
			
			$tipo = $this->tipo_objeto->getById($idTipo);
			
			if(!empty($tipo)){
				
				$data = array();
				
				if(!empty($tipo)){
					
					if( ( !isset($_POST['ID_AGENCIA']) || empty($_POST['ID_AGENCIA']) ) || ( !isset($_POST['ID_CLIENTE']) || empty($_POST['ID_CLIENTE']) )){
						$this->data['erros'][] = 'Agencia ou cliente nao informados';
						$this->form();
						return;
					}
					
					$cliente = $this->cliente->getById($_POST['ID_CLIENTE']);
					$agencia = $this->agencia->getById($_POST['ID_AGENCIA']);
					$data['ID_TIPO_OBJETO'] = $idTipo;
					
					
					$path = $this->config->item('caminho_storage');
					
					//$path = $this->agencia->getPath($agencia['ID_AGENCIA']);
					//alteração chamado 33325
					$path = $this->cliente->getPath($cliente['ID_CLIENTE']);
					
					$limit = 10000000000;
					$objetos = $this->general->getByLike($data, 0, $limit,$path,'NOME');
					
					$objetos = $objetos['data'];
					
					if(!empty($objetos)){
						$objFound = count($objetos);
						
						foreach ($objetos as $obj) {
							//segundo solicitação do Rodney Silva o campo código do objeto foi substituido pelo nome
							if(!empty($obj['CATEGORIA']) && !empty($obj['SUBCATEGORIA']) && !empty($obj['NOME'])){
								
								$ficha = $this->ficha->getByClienteCodigo($_POST['ID_AGENCIA'],$obj['NOME']);
								if(empty($ficha)){
									$ficha = array();
									$ficha['ID_AGENCIA'] = $_POST['ID_AGENCIA'];
									$ficha['ID_CLIENTE'] = $_POST['ID_CLIENTE'];
									$ficha['ID_USUARIO'] = $idUsuario;
									
									/*if($usaKeyword == 0 || empty($obj['KEYWORDS'])){
										$nome = explode('.',$obj['NOME']);
										$ficha['NOME_FICHA'] = $nome[0];
									}else{
										$ficha['NOME_FICHA'] = utf8_encode($obj['KEYWORDS']);
									}*/
									
									$ficha['NOME_FICHA'] = trim($obj['NOME']);
									$ficha['COD_FICHA'] = trim($obj['NOME']);
									$ficha['COD_BARRAS_FICHA'] = trim($obj['NOME']);
									$ficha['ID_CATEGORIA'] = $obj['CATEGORIA'];
									$ficha['ID_SUBCATEGORIA'] = $obj['SUBCATEGORIA'];
									$ficha['TIPO_FICHA'] = $tipo['DESC_TIPO_OBJETO'];
									$ficha['FLAG_TEMPORARIO'] = 1;
									$ficha['FLAG_ACTIVE_FICHA'] = 1;
									$ficha['ID_APROVACAO_FICHA'] = 3;
									$ficha['TIPO_COMBO_FICHA'] = 1;
									
									if(!empty($obj['FABRICANTE']) && !empty($obj['MARCA'])){
										$ficha['FABRICANTE_FICHA'] = $obj['FABRICANTE'];
										$ficha['MARCA_FICHA'] = $obj['MARCA'];
									}
									
									$idFicha = $this->ficha->save($ficha);
									
									if($idFicha){
										// grava o historico
										$historico = array(
											'ID_USUARIO' => $idUsuario,
											'ID_FICHA'=>$idFicha,
											'NEW_STATUS'=>3,
											'COMENT_HISTORICO_FICHA'=>'--'
										);
										
										$this->ficha_historico->save($historico);
										
										/*
										$mainObj = array();
										$mainObj['ID_FICHA'] = $idFicha;
										$mainObj['FILE_OBJ_FICHA'] = $obj['NOME'];
										$mainObj['PATH_OBJ_FICHA'] = $obj['PATH'];
										//$mainObj['THUMB_OBJ_FICHA'] = $obj['THUMB'];
										$mainObj['FILEID_OBJ_FICHA'] = $obj['FILE_ID'];
										$mainObj['FLAG_MAIN_OBJ_FICHA'] = 1;
										$this->obj_ficha->save($mainObj);
										*/
										
										$objeto = $this->objeto->getByFileId($obj['FILE_ID']);
										
										if(!empty($objeto)){
											$mainObj = array();
											$mainObj['ID_FICHA'] = $idFicha;
											$mainObj['ID_OBJETO'] = $objeto['ID_OBJETO'];
											$this->objeto_ficha->save($mainObj);
										}
										
										$fichasCreated[] = $obj['NOME'];
										
									}else{
										$fichasNotCreated[] = $obj['NOME'];
									}
								}else{
										$fichasFound[] = $obj['NOME'];
								}
								
							}else{
								$fichasNotCreated[] = $obj['NOME'];
							}
						}
						
					}else{
						$this->data['erros'][] = 'Nenhum objeto encontrado';
						$this->form();
						return;
					}
				}else{
					$this->data['erros'][] = 'Tipo nao encontrado';
					$this->form();
					return;
				}
			}
		}else{
			$this->data['erros'][] = 'Tipo nao informado';
			$this->form();
			return;
		}
		
		$this->data['objFound'] = $objFound;
		$this->data['fichasCreated'] = $fichasCreated;
		$this->data['fichasFound'] = $fichasFound;
		$this->data['fichasNotCreated'] = $fichasNotCreated;
		$this->data['agencia'] = $agencia;
		$this->data['cliente'] = $cliente;
		$this->data['tipo'] = $tipo;
		
		$this->display('result');
	}
}
















