<?php
/**
 * Controller para gerenciar campanhas
 * @author Juliano Polito
 *
 */
class Campanha extends MY_Controller {
	/**
	 * Construtor
	 * @author Juliano Polito
	 * @return Campanha
	 */
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/campanha/';
	}
	
	/**
	 * Metodo inicial da controller
	 * Acessado quando nao especificado um metodo
	 * @author Juliano Polito
	 * @return void
	 */
	public function index(){
		$this->lista();
	}
	
	/**
	 * Lista os itens cadastrados
	 * 
	 * @author Juliano Polito
	 * @param int $pagina Pagina inicial da listagem
	 * @return void
	 */
	public function lista($pagina=0){
		
		if (!empty($_POST)) {
			Sessao::set('busca', $_POST);
		}
		
		//Usuario
		$user = Sessao::get('usuario');
		
		//Dados da sessao em questao
		$data = Sessao::get('busca');
		
		if(!is_array($data)){
			$data = array($data);
		}
		
		//se o usuario estiver ligado a alguma agencia
		if(!empty($user['ID_AGENCIA'])){
			$data['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}
		
		//se o usuario estiver ligado a algum cliente
		if(!empty($user['ID_CLIENTE'])){
			$data['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}
		
		if( isset($data['STATUS_CAMPANHA']) && $data['STATUS_CAMPANHA'] === '' ){
			unset($data['STATUS_CAMPANHA']);
		}
		
		// aqui filtramos para mostrar somente os produtos que o usuario tem acesso
		$data['PRODUTOS'] = getValoresChave(getProdutosUsuario(),'ID_PRODUTO');
		
		//se o usuario nao selecionou uma ordenacao por coluna, usa DESC_CAMPANHA como default
		if(empty($data['ORDER'])){
			$data['ORDER'] = 'DATA_ALTERACAO';
		}

		//se o usuario nao selecionou uma ordenacao por coluna, usa ASC como default
		if(empty($data['ORDER_DIRECTION'])){
			$data['ORDER_DIRECTION'] = 'DESC';
		}
		
		$limit = !empty($data['pagina']) ? $data['pagina'] : 5;
		$lista = $this->campanha->listItems($data, $pagina, $limit, $data['ORDER'], $data['ORDER_DIRECTION']);
		
		$clientes = array();
		$produtos = array();
		
		// se tiver codigo de agencia
		if( !empty($user['ID_AGENCIA']) ){
			// pega os clientes da agencia
			$clientes = $this->cliente->getByAgencia($data['ID_AGENCIA']);
			
		} else {
			$clientes = $this->cliente->getAll();
		}
		
		if(!empty($data['ID_CLIENTE'])){
			$produtos = $this->produto->getByClienteUsuario($data['ID_CLIENTE'], $user['ID_USUARIO']);
		}
		
		$this->data['clientes'] = $clientes;
		$this->data['produtos'] = $produtos;
		
		$this->data['podeAlterar'] = Sessao::hasPermission('campanha','save');
		$this->data['busca'] = $data;
		$this->data['lista'] = $lista['data'];
		$this->data['total'] = $lista['total'];
		$this->data['paginacao'] = linkpagination($lista,$limit);
		
		$this->display('index');
	}
	
	/**
	 * Exibe o formulario para edicao ou insercao
	 * 
	 * @author Juliano Polito
	 * @param int $id codigo do produto
	 * @return void
	 */
	public function form($id=null){
		if(!empty($id)){
			$this->checaPermissaoAlterar($id);
		}
		
		if(!is_null($id) && $_SERVER['REQUEST_METHOD'] == 'GET') {
			$_POST = $this->campanha->getByIdJoin($id);
			$_POST['DT_INICIO_CAMPANHA'] = format_date_to_form($_POST['DT_INICIO_CAMPANHA']);
			$_POST['DT_FIM_CAMPANHA'] = format_date_to_form($_POST['DT_FIM_CAMPANHA']);
		}
		
		$user = Sessao::get('usuario');
		
		if(!empty($user['ID_AGENCIA'])){
			$_POST['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}
		
		if(!empty($user['ID_CLIENTE'])){
			$_POST['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}
		
		if(!empty($user['ID_PRODUTO'])){
			$_POST['ID_PRODUTO'] = $user['ID_PRODUTO'];
		}
		
		$clientes = array();
		$produtos = array();
		
		if(!empty($_POST['ID_AGENCIA'])){
			$clientes = $this->cliente->getByAgencia($_POST['ID_AGENCIA']);
		} else {
			$rs = $this->cliente->listItems(array('STATUS_CLIENTE'=>1),0,100000);
			$clientes = $rs['data'];
		}
		
		if(!empty($_POST['ID_CLIENTE'])){
			$produtos = getProdutosUsuario(post('ID_CLIENTE',true), post('ID_AGENCIA',true));
		}
		
		$res = $this->agencia->getAll();
		$agencias = $res['data'];
		
		$this->data['agencias'] = $agencias;
		$this->data['clientes'] = $clientes;
		$this->data['produtos'] = $produtos;
		
		//print_rr($_POST);die;
		$this->data['ID_CAMPANHA'] = $id;
		
		$this->display('form');
	}
	
	/**
	 * Grava os dados do formulario
	 * 
	 * @author Juliano Polito
	 * @param int $id
	 * @return void
	 */
	public function save($id=null){
		if(!empty($id)){
			$this->checaPermissaoAlterar($id);
		}
		$_POST['ID_CAMPANHA'] = $id;
		$user = Sessao::get('usuario');
		if( empty($id) ){
			$_POST['CHAVE_STORAGE_CAMPANHA'] = ajustaChaveStorage($_POST['DESC_CAMPANHA']);
		}
		
		//validação somente para cliente hermes
		if (($user['IS_CLIENTE_HERMES'] == 1) || (isset($_POST['ID_CLIENTE']) && $_POST['ID_CLIENTE'] == 38)) {
			$this->campanha->addValidation('COD_CATALOGO','requiredNumber','Informe o Código do Catalogo');
		}
		$erros = $this->campanha->validate($_POST);
		
		if(empty($erros)){
			$_POST['DT_INICIO_CAMPANHA'] = format_date($_POST['DT_INICIO_CAMPANHA']);
			$_POST['DT_FIM_CAMPANHA'] = format_date($_POST['DT_FIM_CAMPANHA']);
			$_POST['DATA_ALTERACAO'] = date('Y-m-d H:i:s',time());

			$id = $this->campanha->save($_POST,$id);
			
			$idsRetorno = $this->xinet->createPath($this->campanha->getPath($id));
			
			if( file_exists($this->campanha->getPath($id))) {
				$this->campanha->save(array( 'DIRETORIO_CRIADO' => '1' ), $id);
			}
			
			// grava o log de alteracoes
			$this->campanha_historico->gravarAlteracoes($id, $user['ID_USUARIO'], $_POST);
			
			redirect('campanha/lista');
		}
		
		$this->data['erros'] = $erros;
		$this->form($id);
	}

	
	/**
	 * Checa se o usuario logado tem permissao ou nao de alterar o job acessado
	 * 
	 * Se nao tiver permissao, redireciona para a listagem
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idjob
	 * @return void
	 */
	protected function checaPermissaoAlterar($id){
		$user = Sessao::get('usuario');
		$campanha = $this->campanha->getByIdJoin($id);
		
		$categorias = getCategoriasUsuario($campanha['ID_CLIENTE'],true);
		$goto = 'campanha/lista';
		
		if(!empty($user['ID_CLIENTE'])){
			if($campanha['ID_CLIENTE'] != $user['ID_CLIENTE']){
				redirect($goto);
			}
			
			$produtos = getValoresChave(getProdutosUsuario($user['ID_CLIENTE']),'ID_PRODUTO');
			
			if(!in_array($campanha['ID_PRODUTO'],$produtos)){
				redirect($goto);
			}
		}
		
		if(!empty($user['ID_AGENCIA'])){
			$clientes = getValoresChave($this->cliente->getByAgencia($user['ID_AGENCIA']),'ID_CLIENTE');
			
			if(!in_array($campanha['ID_CLIENTE'],$clientes)){
				redirect($goto);
			}
			
			$produtos = getValoresChave(getProdutosUsuario($campanha['ID_CLIENTE'],$user['ID_AGENCIA']),'ID_PRODUTO');
			
			if(!in_array($campanha['ID_PRODUTO'],$produtos)){
				redirect($goto);
			}
		}
	}
}