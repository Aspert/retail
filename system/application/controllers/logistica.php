<?php

/**
 * Controller de logistica
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 */
class Logistica extends MY_Controller{
	
	/**
	 * Construtor
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Logistica
	 */
	public function __construct(){
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/logistica/';
	}
	
	/**
	 * Lista os jobs cadastrados
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $pg
	 * @return void
	 */
	public function index($pg = 0){
		// se enviou dados
		if(!empty($_POST)){
			// altera a sessao
			Sessao::set('busca', $_POST);
		}
		
		$user = Sessao::get('usuario');
		$dados = (array) Sessao::get('busca');
		$dados['STATUS_CAMPANHA'] = 1;
		
		$this->setSessionData( $dados );
		
		// somente os desta etapa
		$dados['ID_ETAPA'] = $this->etapa->getIdByKey('LOGISTICA');
		$dados['PRODUTOS'] = getValoresChave(getProdutosUsuario(),'ID_PRODUTO');
		
		if(empty($dados['ORDER'])){
			$dados['ORDER'] = 'DATA_INICIO';
		}
		if(empty($dados['ORDER_DIRECTION'])){
			$dados['ORDER_DIRECTION'] = 'DESC';
		}
		
		//Retira os bloqueados
		$dados['NOT_ID_STATUS'] = array ($this->status->getIdByKey('BLOQUEADO'));
		
		$limit = empty($dados['pagina']) ? 0 : $dados['pagina'];
		$lista = $this->job->listItems($dados, $pg, $limit, $dados['ORDER'], $dados['ORDER_DIRECTION']);
		
		$this->loadFilters($dados);
		
		$this->data['podeAlterar'] = Sessao::hasPermission('logistica','save');
		$this->data['busca'] = $dados;
		$this->data['lista'] = $lista['data'];
		$this->data['paginacao'] = linkpagination($lista, $limit);
		
		$this->display('index');
	}
	
	/**
	 * Exibe o formulario de edicao do carrinho
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id
	 * @return void
	 */
	public function form($id = null, $idpraca = null){
		$this->checaPermissaoAlterar($id);
		$job = $this->job->getById($id);
		
		if( !empty($idpraca) ){
			$this->assign('praca', $this->praca->getById($idpraca));
		}
		
		$pracas = $this->job->getPracas($id);
		
		$this->loadFilters( $_POST );
		$this->assign('job', $job);
		$this->assign('podePassarEtapa', Sessao::hasPermission('logistica','proxima_etapa'));
		$this->assign('pracas', $pracas);
		
		$this->display('form');
	}
	
	/**
	 * Salva as alteracoes do plano de midia
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id
	 * @return void
	 */
	public function save($id = null, $redirect = true){
		$this->checaPermissaoAlterar($id);
		// desabilita o tempo limite
		set_time_limit(0);
		// pega os itens do post
		$itens = empty($_POST['item']) ? array() : $_POST['item'];
		
		// salva o carrinho
		$this->job->saveLogistica($id, $itens);
		
		// altera a etapa
		$this->job->setStatus($id, $this->status->getIdByKey('EM_APROVACAO'));
		
		if($redirect){
			// rediciona
			redirect('logistica/form/' . $id);
		}
	}
	
	/**
	 * Envia um job para proxima etapa
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do job
	 * @return void
	 */
	public function proxima_etapa($id){
		
		$erros = array();
		
		$this->checaPermissaoAlterar($id);
		
		// vamos checar se ha itens nao marcados
		if( $this->job->hasItemIndisponivel($id) == true ){
			$erros[] = 'Existem ítens indisponíveis para este Job. O Job não mudou de Etapa.';
			$this->assign('erros', $erros );
			$this->form($id);
			
		} else {
			$erros = $this->job->mudaEtapa($id, Sessao::get('usuario'));
			
			if(!empty($erros)){
				$this->assign('erros', $erros );
				$this->form($id);
			}else{
				redirect('logistica/index');
			}
		}
	}
	
	/**
	 * Exibe o formulario de pesquisa de fichas
	 * 
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function form_pesquisa(){
		$user = Sessao::get('usuario');

		$filters = array();
		$this->setSessionData($filters);
		
		
		$this->loadFilters($filters);
		$this->assign('busca', $filters);
		$this->display('form_pesquisa');
	}
	
	/**
	 * carrega os filtros padrao
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data Dados para indicar quais filtros usar
	 * @return void
	 */
	protected function loadFilters($data = null){
		
		$user = Sessao::get('usuario');
		
		$this->assign('produtos', array());
		$agencias = array();
		
		if(!empty($user['ID_PRODUTO']) ){
			$data['ID_PRODUTO'] = $user['ID_PRODUTO'];
		}
		
		if(!empty($user['ID_AGENCIA']) ){
			$data['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}
		
		if(!empty($user['ID_CLIENTE']) ){
			$data['ID_CLIENTE'] = $user['ID_CLIENTE'];
			$agencias = $this->agencia->getByCliente($data['ID_CLIENTE']);
		}
		
		if(!empty($data['ID_AGENCIA'])){
			$this->data['clientes'] = $this->cliente->getByAgencia($data['ID_AGENCIA']);
			$agencias = array();
		} else {
			$this->data['clientes'] = array();
		}
		
		if(!empty($data['ID_PRODUTO'])){
			$this->data['campanhas'] = $this->campanha->getByProduto($data['ID_PRODUTO']);
		} else {
			$this->data['campanhas'] = array();
		}
		
		$this->assign('produtos',array());
		if( !empty($data['ID_CLIENTE']) ){
			$this->assign('produtos', getProdutosUsuario(val($data,'ID_CLIENTE'), val($data,'ID_AGENCIA')));
		}
		
		$this->assign('subcategorias', array() );
		$this->assign('agencias', $agencias);
	}

	/**
	 * Checa se o usuario logado tem permissao ou nao de alterar o job acessado
	 * 
	 * Se nao tiver permissao, redireciona para a listagem
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idjob
	 * @return void
	 */
	protected function checaPermissaoAlterar($idjob){
		
		$user = Sessao::get('usuario');
		$job = $this->job->getById($idjob);
		$produtos = getValoresChave(getProdutosUsuario($job['ID_CLIENTE'],$job['ID_AGENCIA']),'ID_PRODUTO');
		$goto = 'logistica/index';
		
		//Verifica se o job nao esta bloqueado
		if ( $job['ID_STATUS'] == $this->status->getIdByKey('BLOQUEADO') ) {
			redirect($goto);
		}
		
		//Verifica se esta na etapa correta
		if( $job['ID_ETAPA'] != $this->etapa->getIdByKey('LOGISTICA') ){
			redirect($goto);
		}
		
		//Verifica se o cliente do usuario eh o mesmo cliente do job
		if(!empty($user['ID_CLIENTE'])){
			if($job['ID_CLIENTE'] != $user['ID_CLIENTE']){
				redirect($goto);
			}
			
			//Verifica se o produto do job esta na lista permitida para o cliente x agencia
			if(!in_array($job['ID_PRODUTO'],$produtos)){
				redirect($goto);
			}
		}
		
		//Verifica se a agencia do usuario eh a mesma agencia do job
		if(!empty($user['ID_AGENCIA'])){
			if($user['ID_AGENCIA'] != $job['ID_AGENCIA']){
				redirect($goto);
			}
			
			$clientes = getValoresChave($this->cliente->getByAgencia($user['ID_AGENCIA']),'ID_CLIENTE');
			if(!in_array($job['ID_PRODUTO'],$produtos)){
				redirect($goto);
			}
			
			if(!in_array($job['ID_CLIENTE'],$clientes)){
				redirect($goto);
			}
		}
	}
	
}
