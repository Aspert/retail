<?php
/**
 * Controller para gerenciamento de usuarios
 *
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class Usuario extends MY_Controller
{
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/usuario/';
	}

	/**
	 * Lista os usuarios cadastrados
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $pagina Pagina atual
	 * @return void
	 */
	function lista($pagina=0) {
		if ( !empty($_POST))  {
			Sessao::set('busca', $_POST);
		}

		$dados = Sessao::get('busca');
		if(!is_array($dados)){
			$dados = array();
		}

		$limit = empty($dados['pagina']) ? 0 : $dados['pagina'];

		// carregando filtros
		$grupos = array();
		$clientes = array();
		$agencias = array();

		$rs = $this->agencia->getAll(0,77777,'DESC_AGENCIA','ASC');
		$agencias = $rs['data'];

		$rs = $this->grupo->getAll(0,77777,'DESC_GRUPO','ASC');
		$grupos = $rs['data'];

		if(!empty($dados['ID_AGENCIA'])){
			$clientes = $this->cliente->getByAgencia($dados['ID_AGENCIA']);
		}

		$this->assign('agencias', $agencias);
		$this->assign('clientes', $clientes);
		$this->assign('grupos', $grupos);

		$usuarios = $this->usuario->listItems($dados, $pagina, $limit);

		$this->assign('total', $usuarios['total']);

		$this->data['usuarios'] = $usuarios['data'];
		$this->data['paginacao'] = linkpagination($usuarios, $limit);
		$this->data['total'] = $usuarios['total'];
		$this->data['busca'] = $dados;
		$this->data['podeAlterar'] = Sessao::hasPermission('usuario','salvar');
		$this->display('index');
	}

	/**
	 * Exibe o formulario para edicao ou cadastro de usuario
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do usuario
	 * @return void
	 */
	function form($id = null) {
		if( !is_null($id) && $_SERVER['REQUEST_METHOD'] == 'GET' ) {
			$_POST = $this->usuario->getById($id);
		}
		
		$idCliente = 0;
		if(isset($_POST['ID_CLIENTE'])){
			$idCliente = $_POST['ID_CLIENTE'];
		}
		
		// vamos listar todas as agencias e clientes
		// ativos, pois aqui servem somente como filtro
		$rs_grupos   = $this->grupo->getAll(0, 1000, 'DESC_GRUPO');
		$rs_agencias = $this->agencia->listItems(array('STATUS_AGENCIA'=>1),0,1000);
		$rs_clientes = $this->cliente->listItems(array('STATUS_CLIENTE'=>1),0,1000);
		
		$pracas = array();
		$pracasDoUsuario = array();
		$pracaUser = array();
		if($idCliente > 0){
			$pracas = $this->praca->getByCliente($idCliente);
			$pracasDoUsuario = $this->praca->getByUsuario($_POST['ID_USUARIO']);
		}
		foreach($pracasDoUsuario as $p){
			array_push($pracaUser,$p['ID_PRACA']);
		}
		
		$this->data['pracaUsuario'] = $pracaUser;
		$this->data['pracas'] = $pracas;
		$this->data['agencias'] = $rs_agencias['data'];
		$this->data['clientes'] = $rs_clientes['data'];
		$this->data['grupos']   = $rs_grupos['data'];

		$this->load->view('ROOT/usuario/form', $this->data);
	}

	/**
	 * Salva os dados do usuario
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do usuario quando editando
	 * @return void
	 */
	function salvar($id = null) {
		$_POST['ID_USUARIO'] = $id;
		$erros = array();
		$erros = $this->usuario->validate($_POST);
		$user = Sessao::get('usuario');

		if(empty($erros)){
			
			$id = $this->usuario->save($_POST, $id);
			
			//Deleta Praças do Usuário
			$this->usuario->DeleteAllPracas($id);
			foreach($_POST['lstPracas'] as $p){
				
				$dados = array(
					'ID_PRACA' => $p,
					'ID_USUARIO' => $id
				);
					
				$this->usuario->savePracas($dados);
			}

			// se o id do usuario eh o mesmo do usuario logado
			if( $id == $user['ID_USUARIO'] ){
				// atualiza as informacoes
				$data = $this->usuario->getByLogin($_POST['LOGIN_USUARIO']);
				Sessao::set('usuario', $data);
				Sessao::clear('_itensMenu');

			}
			redirect('usuario/lista');
		}

		$this->data['erros'] = $erros;
		$this->form($id);

	}

	/**
	 * Exibe a tela de login
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	function login( $view = 'login', $setado = 0 ) {
		$allowed = array('login','test_page');
        if( !in_array($view,$allowed) ){
			$view = 'login';
		}

		if( $view == 'test_page' && $setado == 0 ){
			redirect('usuario/login/test_page/1');
		}

		$this->data['cordenada_cartao'] = chr(rand(65,69)).rand(1,9);

		$this->data['alertMsg'] = $this->config->item('sys_alert_msg');
		$this->data['alertActive'] = $this->config->item('sys_alert_active');
		$this->data['alertAllowLogin'] = $this->config->item('sys_alert_allow_login');



		$this->display($view);
	}

	/**
	 * Chama o metodo de login
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	function do_login() {
        $erros = array();

		if( empty($_POST['usuario']) ){
			$erros[] = Sessao::LOGIN_VAZIO;
		}

		if( empty($_POST['senha']) ){
			$erros[] = Sessao::SENHA_VAZIA;
		}

		if( empty($_POST['val_cartao']) ){
			$erros[] = Sessao::CARTAO_VAZIO;
		}

		if( !empty($erros) ){
			$this->assign('erros', $erros);
			$this->login();

		} else {
			// inativamos as campanhas expiradas
			$this->campanha->inativarPorPeriodo();

			Sessao::getInstance()->login($_POST['usuario'], $_POST['senha'],$_POST['cordenada_cartao'],$_POST['val_cartao']);

			redirect('');
		}
	}

	/**
	 * Chama o metodo de saida do sistema
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	function do_logout() {
		Sessao::getInstance()->logout(Sessao::SESSAO_ENCERRADA);
	}

	function recuperar_senha($type = null){
		if($type == 'json'){
			//$json = getJson();

			$msg = 'Olá!

			O usuário %s esqueceu a senha e pediu para que uma nova fosse encaminhada para ele.
			Os dados fornecidos por ele foram:
			- Motivo: %s
			- Nome completo: %s
			- E-mail: %s
			- Observações: %s
			';

			$msg = sprintf(
				nl2br($msg),
				$_REQUEST['json']['nome_completo'],
				$_REQUEST['json']['motivo'],
				$_REQUEST['json']['nome_completo'],
				$_REQUEST['json']['email'],
				strip_tags($_REQUEST['json']['observacoes'])
			);

			// colocamos o endereco do help desk "hardcode"
			$usuario = array(
				array(
					'NOME_USUARIO' => 'Suporte 247',
					'EMAIL_USUARIO' => 'suporte@247id.com.br'
				)
			);

			$data = array(
				'titulo' => 'Solicitacao de recuperacao de senha',
				'texto' => $msg,
				'url' => ''
			);
			//print_rr($data);die;

			// envia o email para o service desk
			$this->email->emailPadrao($data, $usuario);

			setJson('ok');
		}

		// se for para resetar a senha do usuario
		if( $type == 'resetar' ){

			// se nao informou nada
			if( post('login_recuperar_senha',true) == '' || post('email_recuperar_senha',true) == '' ){
				echo 'Informe o login e o e-mail';
				exit;
			}

			// vamos procurar pelo login e email
			$user = $this->usuario->getByLogin(post('login_recuperar_senha',true));
			//print_rr($user);die;

			// se encontrou e o email e o mesmo
			if( !empty($user) && $user['EMAIL_USUARIO'] == post('email_recuperar_senha',true) ){

				//print_rr($user);die;
				// resetamos a senha
				//$senhaNova = "mudar123456";
				$senhaNova = Sessao::getInstance()->resetarSenha($user['LOGIN_USUARIO']);

				//print_rr($senhaNova);die;
				// se conseguiu resetar
				if( !empty($senhaNova) ){

					// dados que vao por email
					$dados = array(
						'usuario' => $user,
						'senhaNova' => $senhaNova
					);

					// envia o email para o usuario
					$this->email->sendEmail($user['EMAIL_USUARIO'], 'ROOT/templates/resetar_senha', $dados);

					// mensagem de retorno
					echo 'ok';
					exit;

				} else {
					// mensagem para quando nao for possivel trocar
					echo 'Não foi possível trocar a senha. Entre em contato com o suporte.';
					exit;
				}
			}

			// se nao bateu algum dado, mostra a mensagem abaixo
			echo 'Dados inválidos.';
			exit;
		}

		$this->load->view('ROOT/layout/recuperar_senha');
	}

	/**
	 * Exibe a tela de requisitos do sistema
	 *
	 * @link http://www.247id.com.br
	 * @return void
	 */
	function requisitos(){
		$this->load->view('ROOT/requisitos/index');
	}

	/**
	 * Exibe a tela para trocar a senha do usuario
	 *
	 * Tambem permite que o usuario troque a senha
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function trocar_senha($acao = ''){

		switch($acao) {
			case 'trocar':
				// pega os dados do usuario logado
				$user = Sessao::get('usuario');

				// senha antiga
				$senhaAntiga = post('senha_antiga',true);

				// senha nova
				$senhaNova = post('senha_nova',true);

				// faz a troca
				$res = Sessao::getInstance()->trocarSenha($user['LOGIN_USUARIO'], $senhaAntiga, $senhaNova);

				// se deu certo
				if( $res ){

					// tira a checagem de troca de senha
					unset($user['trocarSenha']);

					// atualiza a sessao do usuario
					Sessao::set('usuario', $user);

					$this->assign('status','Senha trocada com sucesso');

					// se nao deu
				} else {
					$this->assign('status','Não foi possível trocar a senha');

				}
				break;
		}

		// mensagens de trocar a senha
		$this->assign('mensagens', Sessao::getInstance()->getMensagensTroca());

		// exibe a tela
		$this->display('trocar_senha');
	}

}
