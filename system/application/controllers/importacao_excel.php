<?php
require_once("r_script.php");

/**
 * Controller de importacao de Excel
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class Importacao_excel extends MY_Controller {

	/**
	 * marca o inicio do processamento da planilha
	 * @var float
	 */
	private $start;
	/**
	 * marca o final do processamento da planilha
	 * @var float
	 */
	private $end;

	private $start_memory;
	private $end_memory;

	/**
	 * Construtor
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Importacao_excel
	 */
	function __construct(){
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/importacao_excel/';
		ini_set('memory_limit', '2048M');

	}

	/**
	 * Exibe a lista de itens ja enviados
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $pagina Pagina atual da listagem
	 * @return void
	 */
	public function lista($pagina=0){
		if(!empty($_POST)){
			Sessao::set('busca', $_POST);
		}

		$busca = (array) Sessao::get('busca');

		// filtrando somente o que estiver para agencia
		$busca['ID_ETAPA'] = $this->etapa->getIdByKey('ENVIADO_AGENCIA');

		$user = Sessao::get('usuario');

		$this->loadFilters($busca);


		if(empty($busca['ORDER'])){
			$busca['ORDER'] = 'TITULO_JOB';
		}

		if(empty($busca['ORDER_DIRECTION'])){
			$busca['ORDER_DIRECTION'] = 'ASC';
		}

		// verificamos se tem algo na sessao
		// nestes casos independem do formulario
		if(!empty($user['ID_AGENCIA'])){
			$busca['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}

		if(!empty($user['ID_CLIENTE'])){
			$busca['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}

		if(!empty($user['ID_PRODUTO'])){
			$busca['ID_PRODUTO'] = $user['ID_PRODUTO'];
		}

		$limit = empty($busca['pagina']) ? 0 : $busca['pagina'];
		$lista = $this->excel->listItems($busca, $pagina, $limit,  $busca['ORDER'], $busca['ORDER_DIRECTION']);

		$this->data['busca'] = $busca;
		$this->data['lista'] = $lista['data'];
		$this->data['paginacao'] = linkpagination($lista , $limit);
		$this->data['podeAlterar'] = Sessao::hasPermission('importacao_excel','importar');
		$this->data['podeBaixar'] = Sessao::hasPermission('importacao_excel','download');
		$this->data['verHistorico'] = Sessao::hasPermission('importacao_excel','historico');
		$this->display('index');
	}

	/**
	 * Carrega os filtros para exibir na pagina
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data
	 * @return void
	 */
	private function loadFilters(array $data){
		$user = Sessao::get('usuario');
		// verificamos se tem algo na sessao
		// nestes casos independem do formulario
		if(!empty($user['ID_AGENCIA'])){
			$data['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}

		if(!empty($user['ID_CLIENTE'])){
			$data['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}

		if(!empty($user['ID_PRODUTO'])){
			$data['ID_PRODUTO'] = $user['ID_PRODUTO'];
		}

		//////////////////////////////////////////////////////
		// filtros
		//////////////////////////////////////////////////////
		$user['STATUS_AGENCIA'] = 1;
		$ag = $this->agencia->listItems($user, 0, 10000);

		if(!empty($data['ID_AGENCIA'])){
			$cl = $this->cliente->listItems(array("ID_AGENCIA"=>$data['ID_AGENCIA'],'STATUS_CLIENTE'=>1),0,10000);
			$this->data['clientes'] = $cl['data'];
		} else {
			$this->data['clientes'] = array();
		}

		if(!empty($data['ID_CLIENTE'])){
			$cl = $this->produto->listItems(array("ID_CLIENTE"=>$data['ID_CLIENTE'],'STATUS_PRODUTO'=>1),0,10000);
			$this->data['produtos'] = $cl['data'];
		} else {
			$this->data['produtos'] = array();
		}

		if(!empty($data['ID_PRODUTO'])){
			$cl = $this->campanha->listItems(array("ID_PRODUTO"=>$data['ID_PRODUTO'],'STATUS_CAMPANHA'=>1),0,10000);
			$this->data['campanhas'] = $cl['data'];
		} else {
			$this->data['campanhas'] = array();
		}

		$tipo = $this->tipoJob->getAll(0,1000,'DESC_TIPO_JOB');
		$this->data['tiposJob'] = $tipo['data'];

		$this->data['agencias'] = $ag['data'];
	}

	/**
	 * Tela que exibe o formulario de importacao do excel
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function importar($id=null, $acao='', $continuar=false){
		
		$this->start = microtime(true);
		$this->start_memory = memory_get_usage(true);

		if( !empty($id) && $_SERVER['REQUEST_METHOD'] == 'GET' ){
			//Sessao::set('__excel','');
			$_POST = $this->excel->getById($id);
			$_POST['DATA_INICIO'] = format_date_to_form($_POST['DATA_INICIO']);
			$_POST['DATA_TERMINO'] = format_date_to_form($_POST['DATA_TERMINO']);
		}

		$erros = array();
		$filename = Sessao::get('__excel');

		Sessao::set('idJob', $id);
		
		/* -------------------------- TESTA SE A ACAO EH PARA ENVIAR -------------------- */
		if($acao == 'executar'){
			try {
				// vejamos se foi enviado um arquivo
				if( !empty($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name']) ){
					// partes do arquivo
					$parts = pathinfo($_FILES['file']['name']);
					// pegamos a extensao do arquivo
					$ext = $parts['extension'];

					// se nao esta na lista de itens suportados
					if( !in_array($ext,array('xlsx')) ){
						// mostra o erro
						$erros[] = 'Extensão do arquivo não suportado: ' . $ext .
							'Por favor, insira um arquivo com extensão .xlsx';

					} else {

						// se ja existir um na sessao
						if( Sessao::get('__excel') != '' && file_exists(Sessao::get('__excel')) ){
							// remove o anterior
							unlink(Sessao::get('__excel'));
						}

						// novo nome do excel
						$newname = md5($id . rand(0,time())) . '.' . $parts['extension'];
						// nome com a pasta
						$filename = 'files/'.$newname;
						if( file_exists($filename) ){
							unlink($filename);
						}
						// colocamos o arquivo na pasta files
						move_uploaded_file($_FILES['file']['tmp_name'], $filename);

						// pega os dados do excel
						// $data = $this->excel->getByJobCliente($number,$_POST['ID_CLIENTE']);
						$data = $this->excel->getByJob($id);

						// se tiver , complementa os dados do post
						if(!empty($data)){
							foreach($data as $key => $val){
								if(empty($_POST[$key])){
									$_POST[$key] = $val;
									if(strpos($key,'DATA_') === 0){
										$_POST[$key] = format_date_to_form($val);
									}
								}
							}
						}

						Sessao::set('__excel', $filename);
					}
				}

				// pega regioes da bandeira
				$regioes = $this->regiao->getByProduto(val($_POST,'ID_PRODUTO',0));

				// se houver
				if( !empty($regioes) ){

					// pega a primeira regiao
					$regiao = array_shift($regioes);

					// coloca o ID da regiao no array
					$_POST['ID_REGIAO'] = $regiao['ID_REGIAO'];
				}

				// copia do titulo do job para o excel
				$_POST['DESC_EXCEL'] = empty($_POST['TITULO_JOB']) ? '' : $_POST['TITULO_JOB'];
				$erros = $this->excel->validate($_POST);
				
				//exibe a mensagem de erro caso o cliente não tenha subcategoria setada
				if($_POST['CADASTRAR_EM_LOTE'] == 1){
					$c = $this->cliente->getById($_POST['ID_CLIENTE']);
					if(!is_numeric($c['SUBCATEGORIA_LOTE']) || $c['SUBCATEGORIA_LOTE'] == 0){
						$erros['SUBCATEGORIA'] = 'Não existe subcategoria cadastrada para o cliente para realizar a inserção em lote.';
					}
				}
				
				if(empty($erros)){
					if(!empty($filename)){
						$this->_iniciarImportacao($_POST, $filename, $continuar);
						return;
					} else {
						$erros[] = 'Arquivo de Excel não enviado';
					}
				}

			} catch(Exception $e){
				// se o arquivo nao for o correto para o adaptador
				if( $e->getCode() == ExcelImporterFactory::ARQUIVO_INVALIDO ){
					$erros[] = 'Verifique se o arquivo é um XLSX válido para este cliente';

				} else {
					$erros[] = $e->getMessage();

				}
			}
		}

		$this->data['agencias'] = array();
		$this->data['clientes'] = array();
		$this->data['produtos'] = array();
		$this->data['campanhas'] = array();
		$this->data['templates'] = array();

		$user = Sessao::get('usuario');

		if(!empty($user['ID_AGENCIA'])) {
			$_POST['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}
		if(!empty($user['ID_CLIENTE'])) {
			$_POST['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}
		if(!empty($user['ID_PRODUTO'])) {
			$_POST['ID_PRODUTO'] = $user['ID_PRODUTO'];
		}

		if(!empty($user['ID_AGENCIA'])){
			$this->assign('clientes', $this->cliente->getByAgencia($user['ID_AGENCIA']));
		}

		if(!empty($user['ID_CLIENTE'])){
			$this->assign('agencias', $this->agencia->getByCliente($user['ID_CLIENTE']));
		}

		if(!empty($_POST['ID_CLIENTE']) && !empty($_POST['ID_AGENCIA'])){
			$this->assign('produtos', getProdutosUsuario(post('ID_CLIENTE',true),post('ID_AGENCIA',true)));
		}

		if(!empty($_POST['ID_PRODUTO'])){
			$rs = $this->campanha->listItems(array('ID_PRODUTO'=>$_POST['ID_PRODUTO'],'STATUS_CAMPANHA'=>1),0,10000);
			$this->assign('campanhas', $rs['data']);
		}

		if(!empty($_POST['ID_CAMPANHA'])){
			$rs = $this->template->listItems(array('ID_CAMPANHA'=>$_POST['ID_CAMPANHA'],'STATUS_TEMPLATE'=>1),0,10000);
			$temps = $rs['data'];
			//Cria os labels dos tenmplates com formato - Template 18 - 10.00cm x 15.00cm
			foreach ($temps as &$t ) {
				if(!empty($t['LARGURA_TEMPLATE']) && !empty($t['ALTURA_TEMPLATE'])){
					$label = sprintf('%s - %1.2fcm x %1.2fcm',$t['DESC_TEMPLATE'],$t['LARGURA_TEMPLATE'],$t['ALTURA_TEMPLATE']);
					$t['DESC_TEMPLATE'] = $label;
				}
			}
			$this->data['templates'] = $temps;
		}

		$this->data['PREVIEW_TEMPLATE'] = '';
		if(!empty($_POST['ID_TEMPLATE'])){
			$t = $this->template->getById($_POST['ID_TEMPLATE']);
			if(!empty($t)) {
				$tpath = $this->template->getPath($t['ID_TEMPLATE']);
				$tfile = $t['ID_TEMPLATE'].'.jpg';
				$tfilePath = utf8MAC($tpath.$tfile);
				if(is_file($tfilePath)){
					$tfileID = $this->xinet->getFilePorPath($tpath,$tfile);
					$this->data['PREVIEW_TEMPLATE'] = $tfileID;
				}
			}
		}

		$this->assign('tiposPeca', $this->tipo_peca->getTiposAtivos( val($_POST,'ID_CLIENTE',0) ));

		$rs = $this->tipoJob->getAll(0,1000,'DESC_TIPO_JOB');
		$this->data['tiposJob'] = $rs['data'];

		$this->data['filename'] = $filename;
		$this->data['id'] = $id;
		$this->data['erros'] = $erros;
		$this->display('importar');
	}

	/**
	 * Metodo que inicia o processo de importacao da planilha
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data dados vindos do formulario
	 * @param string $file nome do arquivo para importacao
	 * @return void
	 * @throws Exception
	 */
	private function _iniciarImportacao($data, $file, $continuar=false){
		$user = Sessao::get('usuario');

		try {


			$data['ID_USUARIO_AUTOR'] = $user['ID_USUARIO'];
			$result = $this->excel->validateImport($data, $file);
			$this->end = microtime(true);

			$this->assign('tempo_decorrido', $this->end - $this->start);
			$this->assign('start_memory', $this->start_memory);
			$this->assign('end_memory', memory_get_usage(true));

			// agora, vamos ver se todas as pracas nao tiveram erros
			$passou = true;
			$comErros = false;
			$fichasNaoEncontradas = array();

			foreach($result as $nomePraca => $res){
						//print_rr($nomePraca,false);
				// se tiver erros
				if(isset($res['erros']) && !empty($res['erros'])){
					// nao continua
					$comErros = true;
					$passou = false;
					
					foreach ($res['erros'] as $chave => $err){
						$f = $this->ficha->getFichasClienteProdutos($data['ID_CLIENTE'], array('0'=>$err['PRODUTO']), $data['ID_REGIAO']);
						if(count($f) == 0){
							if(isset($err['MENSAGEM']) && (substr($err['MENSAGEM'], -16) == 'nao possui ficha') && (substr(@$res['erros'][$chave+1]['MENSAGEM'], -18) != 'nao foi encontrada')){
								$fichasNaoEncontradas[] = $err['PRODUTO'];
							}
						}
					}
					
					
					//break;
				} else if (isset($res[0]['MENSAGEM'])){
					// nao continua
					$passou = false;
				}

				if((!empty($res['avisos']) && !$continuar)){
					// nao continua
					$passou = false;
				}
			}

			// se passou
			if($passou){
				// salva os dados
				$idExcel = $this->excel->saveData( Sessao::get('idJob') );
				
				$excel = $this->excel->getById($idExcel);
				$idJob = $excel['ID_JOB'];

				// remove o arquivo de excel
				unlink($file);
				Sessao::clear('__excel');

				// redireciona para a tela onde
				// monta os arquivos INDD
				// redirect('importacao_excel/gera_indd/'.$id);

				// Alterado em 2009-11-17
				// agora quando terminar, volta para o plano de midia
				redirect('plano_midia/index');

			} else {
				
				$jaEnviou = 0;
				if($jaEnviou == 0){
					
					$jaEnviou = 1;
					$this->data['comErros'] = $comErros;
					$this->data['problemas'] = $result;
					$this->data['fichasNaoEncontradas'] = $fichasNaoEncontradas;
					
					// dados para colocar no email
					$dados = $_POST;
					$ag = $this->agencia->getById($data['ID_AGENCIA']);
					$cl = $this->cliente->getById($data['ID_CLIENTE']);
					$pr = $this->produto->getById($data['ID_PRODUTO']);
					$ca = $this->campanha->getById($data['ID_CAMPANHA']);
	
					$dados['DESC_AGENCIA'] = empty($ag['DESC_AGENCIA']) ? '' : $ag['DESC_AGENCIA'];
					$dados['DESC_CLIENTE'] = empty($cl['DESC_CLIENTE']) ? '' : $cl['DESC_CLIENTE'];
					$dados['DESC_PRODUTO'] = empty($pr['DESC_PRODUTO']) ? '' : $pr['DESC_PRODUTO'];
					$dados['DESC_CAMPANHA'] = empty($ca['DESC_CAMPANHA']) ? '' : $ca['DESC_CAMPANHA'];
					$dados['problemas'] = $result;
					$dados['NOME_USUARIO'] = $user['NOME_USUARIO'];
					
					if(is_numeric( $_POST['ID_USUARIO_RESPONSAVEL'] ) && $_POST['ID_USUARIO_RESPONSAVEL'] > 0 ){
						$usu = $this->usuario->getById($_POST['ID_USUARIO_RESPONSAVEL']);
						$_POST['NOME_USUARIO_RESPONSAVEL'] = $usu['NOME_USUARIO'];
						$dados['NOME_USUARIO_RESPONSAVEL'] = $usu['NOME_USUARIO'];
					}
					else{
						$_POST['NOME_USUARIO_RESPONSAVEL'] = '';
						$dados['NOME_USUARIO_RESPONSAVEL'] = '';
					}
					//envia emails de notificacao associado a regra
	
					$erros = $this->email->sendEmailRegra('importacao_salvar',
															'Lista de pendencias do Job '.$_POST['TITULO_JOB'],
															'ROOT/templates/importacao_salvar',
															array('objeto'=>$_POST,
																	'pendencias'=>$dados),
															$data['ID_AGENCIA'],
															$data['ID_CLIENTE']
															);
					//somente envia email com as fichas cadastradas em lote se o usuario tiver permissão
					if(Sessao::hasPermission('ficha','emailFichasCadastradasEmLote') && $_POST['CADASTRAR_EM_LOTE'] == 1){

						//aqui deixamos somente as fichas que foram adicionadas
						$adicionadas = $result;
						if(!isset($result['erros'])){
							foreach ($result as $tipo => $res){
								unset($adicionadas[$tipo]['erros']);
								unset($adicionadas[$tipo]['avisos']);
							}
						}	
						//se for hermes ou compra facil chama-se produto e não ficha
						$user = Sessao::get('usuario');
						if( (strtolower($user['DESC_CLIENTE']) == "hermes") ||
						(strtolower($user['DESC_AGENCIA']) == "hermes") ||
						(strtolower($user['DESC_AGENCIA']) == "comprafacil") ||
						(strtolower($user['DESC_CLIENTE']) == "comprafacil")){
							$titulo = 'Lista de produtos cadastrados em lote';
						} else {
							$titulo = 'Lista de fichas cadastradas em lote';
						}
							
						$emailData = array_merge($dados, array('problemas'=>$adicionadas));
							
						$erros = $this->email->sendEmailRegra('importacao_fichasCriadasEmLote',
																$titulo,
																'ROOT/templates/importacao_fichasCriadasEmLote',
																array(
																		'objeto' => $emailData,
																		'pendencias' => $emailData,
																),
																$emailData['ID_AGENCIA'],
																$emailData['ID_CLIENTE']
						);
					}
				}
				
				$this->importar(Sessao::get('idJob'));
			}


		} catch(Exception $e) {
			throw $e;
		}
	}

	/**
	 * Metodo para download de arquivos gerados
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id codigo do excel importado
	 * @return void
	 */
	public function download($id=null){
		if(empty($id)){
			$this->showError('Código de importação não informado');
		}

		$versao = $this->excelVersao->getVersaoAtual($id);
		$excel = $this->excel->getById($id);
		$user = Sessao::get('usuario');

		if(!empty($user['ID_AGENCIA']) && $user['ID_AGENCIA'] != $excel['ID_AGENCIA']){
			$this->showError('Você não pertence a mesma agência da importação');

		} else if(!empty($user['ID_CLIENTE']) && $user['ID_CLIENTE'] != $excel['ID_CLIENTE']){
			$this->showError('Você não pertence ao mesmo cliente da importação');

		} else {

			$this->data['lista'] = $this->excel->getTemplateFilesAndPracas($excel['ID_EXCEL'], $versao['ID_EXCEL_VERSAO']);
			$this->display('download');

		}
	}

	/**
	 * Mostra o historico de um excel
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id
	 * @return void
	 */
	public function historico($id=null){
		if(is_null($id)){
			redirect('importacao_excel','lista');
		}

		$excel = $this->excel->getById($id);
		$historico = $this->excelVersao->getLastDifferences($id);

		$this->data['excel'] = $excel;
		$this->data['historico'] = $historico;
		$this->display('historico');
	}

	/**
	 * Metodo para o plugin do indd atualizar o xml
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idExcel Codigo do excel
	 * @param int $idPraca Codigo da praca
	 * @return void
	 */
	public function atualiza_xml($tipoAtualizacao, $idExcel, $idPraca = null, $paginaInicio = 0, $paginaFim = 0, $argumentos = null){
		$excel = $this->excel->getById($idExcel);	

		if ( (strtolower($excel['DESC_CLIENTE']) == 'hermes' || strtolower($excel['DESC_CLIENTE']) == 'comprafacil') ) {
			$this->excel->gerarXMLHermes($tipoAtualizacao, $idExcel, false, $idPraca, $paginaInicio, $paginaFim, $argumentos);
		}
		else{
			$this->excel->gerarXML($tipoAtualizacao, $idExcel, false, $idPraca, $paginaInicio, $paginaFim, $argumentos);
		}

		if(!empty($idPraca)){
			$xls = $this->excel->getById($idExcel);
			$file = 'files/xml/'.$xls['ID_EXCEL'].'-'.$xls['ID_JOB'].'-'.$idPraca.'-'.$paginaInicio.'-'.$paginaFim.'.xml';
			echo file_get_contents($file);
		} else {
			echo 1;
		}
	}

	/**
	 * Recupera as pracas relacionadas a um excel / job
	 *
	 * <p>Escreve um XML para utilizacao do plugin</p>
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idExcel
	 * @return void
	 */
	public function get_pracas($idExcel){
		header('Content-Type: text/xml');

		$xml = '<Pracas>';

		$list = $this->praca->getByExcel($idExcel);

		foreach($list as $item){
			$xml .= '<Praca id="'.$item['ID_PRACA'].'" nome="' . $item['DESC_PRACA'] . '" />';
		}

		$xml .= '</Pracas>';

		echo $xml;

		exit;

	}
	

}





