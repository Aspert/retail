<?php
class Ficha extends MY_Controller {

	function __construct(){
		parent::__construct();

		$this->_templatesBasePath = 'ROOT/ficha/';

		$this->load->library('DBXinet');

		$this->data['categorias'][''] = '[Selecione]';
		$categorias = $this->categoria->getAll();
		foreach ($categorias['data'] as $key => $value){
			$this->data['categorias'][$value['ID_CATEGORIA'] . '#' . $value['DESC_CATEGORIA']] = $value['DESC_CATEGORIA'];
		}
	}

	function index() {
		$this->load->view('ROOT/ficha/menu', $this->data);
	}

	function menu()	{
		$this->data['status_aprovacao'] = $this->aprovacao_ficha->getAll();
		$categorias = $this->categoria->getAll(null, null, 'DESC_CATEGORIA');
		$this->data['categorias'] = arraytoselect($categorias['data'], 'ID_CATEGORIA', 'DESC_CATEGORIA','[Selecione]');

		$fabricantes = $this->fabricante->getAll(null, null, 'DESC_FABRICANTE');
		$this->data['fabricantes'] = arraytoselect($fabricantes['data'], 'ID_FABRICANTE', 'DESC_FABRICANTE','[Selecione]');

		$marcas = $this->marca->getAll(null, null, 'DESC_MARCA');

		$this->data['marcas'] = arraytoselect($marcas['data'], 'ID_MARCA', 'DESC_MARCA','[Selecione]');
		$this->data['podeAlterar'] = Sessao::hasPermission('ficha','salvar');
		$this->data['podeAlterarLayout'] = Sessao::hasPermission('ficha','ficha_flex');

		$this->load->view('ROOT/ficha/index', $this->data);
	}


	function lista($pagina = 0) {
		
		if(isset($_SESSION['ID_JOB'])){
			unset($_SESSION['ID_JOB']);
		} else if(isset($_SESSION['idJob'])){
			unset($_SESSION['idJob']);
		} else if(isset($_SESSION['relatorioObjetoPendente'])){
			unset($_SESSION['relatorioObjetoPendente']);
		}
		
		$user = Sessao::get('usuario');

		if(!empty($_POST)){
			Sessao::set('busca', $_POST);
		}

		$busca = (array) Sessao::get('busca');
		$user = Sessao::get('usuario');

		if(empty($busca['ORDER'])){
			$busca['ORDER'] = 'NOME_FICHA';
		}

		if(empty($busca['ORDER_DIRECTION'])){
			$busca['ORDER_DIRECTION'] = 'ASC';
		}

		if(!empty($user['ID_AGENCIA'])){
			$busca['ID_AGENCIA'] = $user['ID_AGENCIA'];

		}
		if(!empty($user['ID_CLIENTE'])){
			$busca['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}

		if(!isset($busca['ID_APROVACAO_FICHA'])){
			// padrao eh "em aprovacao"
			//$busca['ID_APROVACAO_FICHA'] = 3;
		}

		$this->loadFilters($busca);

		$fabricantes = $this->fabricante->getAll(null, null, 'DESC_FABRICANTE');
		$this->data['fabricantes'] = arraytoselect($fabricantes['data'], 'ID_FABRICANTE', 'DESC_FABRICANTE','[Selecione]');

		$marcas = $this->marca->getAll(null, null, 'DESC_MARCA');
		$this->data['marcas'] = arraytoselect($marcas['data'], 'ID_MARCA', 'DESC_MARCA','[Selecione]');

		$this->data['status_aprovacao'] = $this->aprovacao_ficha->getAll();

		$limit = empty($busca['pagina']) ? 10 : $busca['pagina'];

		$busca['CATEGORIAS'] = getCategoriasUsuario();

		//print_rr($busca);die;
		if(!isset($busca['TIPO_FICHA']) || $busca['TIPO_FICHA'] == ""){
			if((!isset($busca['CODIGOS_BARRA'][0]) || $busca['CODIGOS_BARRA'][0] == "") && (!isset($busca['COD_BARRAS_FICHA']))){
				$busca['TIPO_FICHA'] = array('PRODUTO', 'COMBO');
			}
			else{
				$busca['TIPO_FICHA'] = array('PRODUTO', 'COMBO', 'SUBFICHA');
			}
		}

		$fichas = $this->ficha->listItems($busca,$pagina,$limit,$busca['ORDER'],$busca['ORDER_DIRECTION']);

		$fichasCombo = $this->ficha->listCombosIn(getValoresChave($fichas['data'], 'ID_FICHA'));

		$this->data['busca'] = $busca;
		$this->data['podeEditar'] = Sessao::hasPermission('ficha','form');
		$this->data['podeRelatorio'] = Sessao::hasPermission('ficha','relatorio');
		$this->data['verHistorico'] = Sessao::hasPermission('ficha','historico');
		$this->data['combos'] = $fichasCombo['data'];
		$this->data['combosTotal'] = $fichasCombo['total'];
		$this->data['campos'] = $fichas['data'];
		$this->data['total'] = $fichas['total'];
		$this->data['paginacao'] = linkpagination($fichas, $limit);
		$this->data['isHermes'] = $user['IS_HERMES'];
		$this->data['podeExcluir'] = Sessao::hasPermission('ficha','excluir');
		//print_rr($this->data['campos']);die;
		$this->load->view('ROOT/ficha/index', $this->data);
	}

	/**
	 * Formulario de edicao da ficha
	 * @param int $id id da ficha
	 * @return void
	 */
	function form($id = null){
		if(!empty($id)){
			$this->checaPermissaoAlterar($id);
		}

		$user = Sessao::get('usuario');

		$filters = $_POST;
		
		if(!empty($user['ID_AGENCIA'])){
			$filters['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}

		if(!empty($user['ID_CLIENTE'])){
			$filters['ID_CLIENTE'] = $user['ID_CLIENTE'];
			$_POST['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}
		
		if($user['IS_CLIENTE_COMPRAFACIL'] == 1){
			$c = $this->categoria->getByDesc('PADRÃO');
			$_POST['ID_CATEGORIA'] = $c['ID_CATEGORIA'];
			$filters['ID_CATEGORIA'] = $c['ID_CATEGORIA'];
			$fabricantes['data'] = $this->fabricante->getByDescricao('PADRÃO');
			if(count($fabricantes['data']) > 0){
				$_POST['ID_FABRICANTE'] = $fabricantes['data'][0]['ID_FABRICANTE'];
			}
		}
		else{
			$fabricantes = $this->fabricante->getAll(null,null,'DESC_FABRICANTE');
		}

		$cenarios = array();

		if(!is_null($id) && $_SERVER['REQUEST_METHOD'] == 'GET'){
			$_POST = $this->ficha->getById($id);

			$_POST['DATA_INICIO'] = format_date_to_form($_POST['DATA_INICIO']);
			$_POST['DATA_VALIDADE'] = format_date_to_form($_POST['DATA_VALIDADE']);


			$filters = $_POST;

			$codigos = $this->ficha->getCodigosRegionais($id);
			foreach($codigos as $codigo){
				$_POST['codigos'][$codigo['ID_REGIAO']] = $codigo['CODIGO'];
			}

			if(isset($_POST['ficha']['ID_CLIENTE'])){
				$filters['ID_CLIENTE'] = $_POST['ficha']['ID_CLIENTE'];
			}

			if(isset($_POST['ficha']["ID_CATEGORIA"])){
				$filters['ID_CATEGORIA'] = $_POST['ficha']["ID_CATEGORIA"];
			}

			// objeto principal
			$principal = $this->ficha->getMainObjectByFichaId($id);
			if( !empty($principal) ){
				$_POST['PRINCIPAL'] = $principal['FILE_ID'];
			}
			
			// carrega os cenarios
			$cenarios = $this->cenario->getCenariosByIdFicha( $id );

			// armazena as informacoes dos objetos vinculado a ficha
			$lista = $this->objeto_ficha->getByFicha($id);
			$_POST['objetos'] = $lista;

			// carrega os campos das fichas
			$lista = $this->campo_ficha->getAllCampoFicha($id);
					
			if(!empty($lista)){
				foreach($lista as $item){
					$_POST['campo'][] = $item['LABEL_CAMPO_FICHA'];
					$_POST['valor'][] = $item['VALOR_CAMPO_FICHA'];
					$_POST['subficha'][] = $item['PERTENCE_SUBFICHA'];
					if($item['PRINCIPAL'] == 1){
						$_POST['Principalcampo'] = $item['LABEL_CAMPO_FICHA'];
					}
				}
			}

			// se for uma ficha combo
			if( $_POST['TIPO_FICHA'] == 'COMBO' ){
				// pega as fichas combos atreladas
				$_POST['fichasCombo'] = $this->ficha->getFichasInCombo($_POST['ID_FICHA']);

				foreach($_POST['objetos'] as &$obj){
					$obj['fichas'] = $this->ficha->getCodigosByComboObjeto($_POST['ID_FICHA'], $obj['ID_OBJETO']);
				}
			}

			// pega as fichas pai caso exista
			$_POST['fichaPai'] = $this->ficha->getFichasPai($id);
			
			// pega as fichas filhas caso exista
			$_POST['fichaFilha'] = $this->ficha->getFichasFilha($id);
		}

		// se for uma ficha produto
		if( post('TIPO_FICHA',true) == 'PRODUTO' && !is_null($id) ){
			$this->assign('fichasComboAssociadas', $this->ficha->getFichasComboByFichaProduto($id));
		}

		if(isset($_POST['ID_FABRICANTE'])){
			if( $marcas = $this->marca->getAllByFabricante($_POST['ID_FABRICANTE'])){
		 		$this->data['marcas']= arraytoselect($marcas,'ID_MARCA', 'DESC_MARCA','[Selecione]');
			}
		}

		$this->data['fabricantes'] = arraytoselect($fabricantes['data'] , 'ID_FABRICANTE','DESC_FABRICANTE','[Selecione]');

		$this->data['regioes'] = array();
		if(!empty($_POST['ID_CLIENTE'])){
			$this->data['campos'] = $this->campo->getByCliente($_POST['ID_CLIENTE']);
			$this->data['regioes'] = $this->regiao->getByCliente($_POST['ID_CLIENTE']);
		}

		$subfichas = array();
		if($id != null){
			$subfichas = $this->subficha->getByFichaPrincipal($id);
		}

		$this->data['subfichas'] = $subfichas;

		$this->loadFilters($filters);

		$this->data['podeImportar'] = Sessao::hasPermission('ficha','importaCompraFacil');
		$this->data['podeEmail'] = Sessao::hasPermission('ficha','salvarComEmail');
		$this->data['podeSalvar'] = Sessao::hasPermission('ficha','salvar') || $id == null;
		$this->data['podeExcluir'] = Sessao::hasPermission('ficha','excluir');
		$this->data['podeStatus'] = Sessao::hasPermission('ficha','inner_status') || $id == null;
		$this->data['podeTemporario'] = Sessao::hasPermission('ficha','inner_temporario') || $id == null;
		$this->data['podeIncluirCampo'] = Sessao::hasPermission('ficha','inner_incluir_campo') || $id == null;
		$this->data['podeExcluirCampo'] = Sessao::hasPermission('ficha','inner_excluir_campo') || $id == null;
		$this->data['podeIncluirObjeto'] = Sessao::hasPermission('ficha','inner_incluir_objeto') || $id == null;
		$this->data['podeExcluirObjeto'] = Sessao::hasPermission('ficha','inner_excluir_objeto') || $id == null;
		$this->data['podeAlterarPrincipal'] = Sessao::hasPermission('ficha','inner_alterar_principal') || $id == null;
		$this->data['podeIncluirSubficha'] = Sessao::hasPermission('ficha','form_subficha');
		$this->data['cenarios'] = $cenarios;
		$this->data['fichasCombo'] = empty($_POST['fichasCombo']) ? array() : $_POST['fichasCombo'];
		$this->data['fichaPai'] = empty($_POST['fichaPai']) ? array() : $_POST['fichaPai'];
		$this->data['fichaFilha'] = empty($_POST['fichaFilha']) ? array() : $_POST['fichaFilha'];
		$this->data['isHermes'] = $user['IS_HERMES'];
		$this->data['isClienteHermes'] = $user['IS_CLIENTE_HERMES'];
		$this->data['isClienteCompraFacil'] = $user['IS_CLIENTE_COMPRAFACIL'];
		
		//print_rr($_POST);die;

		$this->load->view('ROOT/ficha/form', $this->data);

	}

	public function detalhe($id){
		$_POST = $this->ficha->getById($id);

		$aprovadoPor = $this->ficha_historico->getLastByFicha($id);

		// armazena as informacoes dos objetos vinculado a ficha
		$lista = $this->objeto_ficha->getByFicha($id);
	
		$_POST['objetos'] = $lista;
		$_POST['codigos'] = $this->ficha->getCodigosRegionais($id);
		$_POST['campos'] = $this->campo_ficha->getByFicha($id);
		$_POST['aprovacao'] = $aprovadoPor;

		$this->load->view('ROOT/ficha/detalhe', $this->data);
	}
	
	public function pendencia($id) {
		
		$ficha = $this->ficha->getById($id);
		$this->data['PENDENCIA'] = $ficha['PENDENCIAS_DADOS_BASICOS'];
		$this->data['ID_FICHA'] = $ficha['ID_FICHA'];
		
		//print_rr($ficha);die;
		$this->load->view('ROOT/ficha/pendencia', $this->data);
	}

	public function form_pesquisa(){
		$user = Sessao::get('usuario');

		$filters = array();
		$filters['ID_CLIENTE'] = $user['ID_CLIENTE'];

		$this->loadFilters($filters);

		$this->load->view('ROOT/ficha/form_pesquisa', $this->data);
	}
		
	/**
	 * Salva os dados da ficha
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id
	 * @param boolean $email
	 * @return void
	 */
	public function salvar($id=null, $email = false){	
		
		$newFicha = false;
		if(!empty($id)){
			$this->checaPermissaoAlterar($id);
		}else{
			$newFicha = true;
		}
		
		if(isset($_POST['NOME_FICHA'])){
			$_POST['NOME_FICHA'] = trim($_POST['NOME_FICHA']);
		}
		
		$user = Sessao::get('usuario');
		
		if(!isset($_POST['FLAG_ACTIVE_FICHA'])){
			$_POST['FLAG_ACTIVE_FICHA'] = 1;
		}

		$this->ficha->addValidation('ID_CLIENTE','requiredNumber','Selecione um cliente');
		$this->ficha->addValidation('COD_BARRAS_FICHA','function', array($this->ficha, 'checaBarrasDuplicado'));
		$this->ficha->addValidation('ID_CATEGORIA','requiredNumber','Selecione uma categoria');
		$this->ficha->addValidation('ID_SUBCATEGORIA','requiredNumber','Selecione uma sub-categoria');
		if((isset($_POST['TIPO_FICHA'])) && ($_POST['TIPO_FICHA'] == 'SUBFICHA')){
			$_POST['ID_APROVACAO_FICHA'] = 1;
			if(!empty($_POST['COD_BARRAS_FICHA']) && empty($_POST['COD_BARRAS_FICHA'])){
				$erros[] = 'Informe o Código de barras';
			}
			$erros = $this->ficha->validate($_POST);
		}
		else{
			if($user['IS_HERMES']){
				if(isset($_POST['ID_CLIENTE']) && is_numeric($_POST['ID_CLIENTE'])){
					$cliente = $this->cliente->getById($_POST['ID_CLIENTE']);
					if(strtoupper($cliente['DESC_CLIENTE']) != 'HERMES'){
						$this->ficha->addValidation('NOME_FICHA','requiredString','Insira o nome do produto');					
					}
				}
			}
			else{
				$this->ficha->addValidation('NOME_FICHA','requiredString','Insira o nome da ficha');
			}
			$this->ficha->addValidation('DATA_INICIO','function', array($this->ficha, 'validateDataInicio'));
			$this->ficha->addValidation('DATA_VALIDADE','function', array($this->ficha, 'validateDataValidade'));
		
			$erros = $this->ficha->validate($_POST);

			//Caso exista apenas 1 objeto, ele sera o principal
			if( !empty($_POST['objetos']) && is_array($_POST['objetos'])
				&& count($_POST['objetos']) == 1
				&& empty($_POST['PRINCIPAL'])){
	
				$vals = array_values($_POST['objetos']);
				$_POST['PRINCIPAL'] = $vals[0]['FILE_ID'];
			}
	
			//caso exista mais de um objeto, deve ser informado o principal
			if(!empty($_POST['objetos']) && empty($_POST['PRINCIPAL'])){
				$erros[] = 'Informe o objeto principal';
			}
	
			//se for uma nova ficha ela entra em status  "em aprovacao"
			if(is_null($id)){
				$_POST['ID_APROVACAO_FICHA'] = 3;
			}
	
			if(!empty($_POST['NOME_FICHA'])){
				$_POST['NOME_FICHA'] = htmlspecialchars_decode($_POST['NOME_FICHA']);
			}
	
			if(!empty($_POST['MODELO_FICHA'])){
				$_POST['MODELO_FICHA'] = htmlspecialchars_decode($_POST['MODELO_FICHA']);
			}
	
			// se informou objetos
			if( !empty($_POST['objetos']) ){
				foreach($_POST['objetos'] as $idx => $item){
					$_POST['objetos'][$idx]['PRINCIPAL'] = $item['FILE_ID'] == post('PRINCIPAL',true) ? 1 : 0;
				}
			} else {
				// PA 4502 -> Nao permite que a ficha seja salva sem a associacao de um objeto
				if($user['IS_HERMES']){
					$erros[] = 'Por favor, associe um cromo ao produto';
				}
				else{
					$erros[] = 'Por favor, associe um objeto a ficha';
				}
			}
	
			// validando os codigos regionais
			if( !empty($_POST['codigos']) ){
				$erros = array_merge($erros, $this->ficha->validarCodigosRegionais($id, $_POST['ID_CLIENTE'], $_POST['codigos']));
			}
	
			// se for uma ficha produto e estiver associada a fichas combo
			if( post('TIPO_FICHA',true) == 'PRODUTO' && count($this->ficha->getFichasComboByFichaProduto($id)) > 0 ){
				// neste caso, a ficha DEVE estar como
				// - Aprovada
				// - NAO temporaria
				// - ativa
				// se nao estiver nestes casos, nao pode salvar
				if( $_POST['FLAG_TEMPORARIO'] == 1 ){
					$erros[] = 'Esta ficha está sendo utilizada por uma ficha combo, por esse motivo ela não pode ser temporária';
				}
	
				if( $_POST['FLAG_ACTIVE_FICHA'] == 0 ){
					$erros[] = 'Esta ficha está sendo utilizada por uma ficha combo, por esse motivo ela não pode ser inativada';
				}
			}
		
		}

		//print_rr($erros);die;
		// se nao houve erros
		if(empty($erros)){
			// grava o log da ficha
			$this->ficha->snapshot($id, $user['ID_USUARIO']);

			$_POST['ID_USUARIO'] = $user['ID_USUARIO'];
			$_POST['DATA_ALTERACAO'] = date('Y-m-d H:i:s');

			// se for uma ficha combo e for nova
			if( empty($id) && post('TIPO_FICHA',true) == 'COMBO' ){
				$_POST['COD_BARRAS_FICHA'] = $this->cliente->getNovoCodigoBarras(post('ID_CLIENTE',true));
			}

			$id = $this->ficha->save($_POST, $id);
			
			if($_POST['FLAG_ACTIVE_FICHA'] == 0){
				$this->ficha->excluirDoJob($id);
			}
	
			// se a ficha estiver inativa tira os registros associados aos cenarios
			if( $_POST['FLAG_ACTIVE_FICHA'] == '0' && !is_null($id) ){
				$this->cenario->desassociaFichaCenarioByIdFicha( $id );
			}

			// salvando os codigos da ficha
			if(!empty($_POST['codigos'])){
				$this->ficha->salvarCodigosRegionais($id, $_POST['codigos']);
				//echo $this->db->last_query();die;
			}

			//altera os dados de busca da sessao pra marcar a ficha alterada na pauta
			$busca = (array) Sessao::get('busca');
			$busca['COD_BARRAS_FICHA'] = $_POST['COD_BARRAS_FICHA'];
			$busca['CODIGOS_BARRA'] = array($_POST['COD_BARRAS_FICHA']);
			$busca = (array) Sessao::set('busca',$busca);

			$user = Sessao::get('usuario');

			if($_POST['TIPO_FICHA'] == 'SUBFICHA'){
				$this->campo_ficha->deleteByIdFicha($id);
				$arrCampo = $this->input->post("campo");
				$arrValor = $this->input->post("valor");
				$principal = $this->input->post("Principalcampo");
				$this->campo_ficha->deleteByIdFicha($id);

				if(!empty($arrCampo)){
					foreach($arrCampo as $k=>$campo){
						$idcampo = $this->campo->getIdByName($campo);
						$idcampo = $idcampo[$k];
						
						if($principal != "" && $principal == $campo){
							$this->campo_ficha->saveCampoSubficha($id, $campo, $arrValor[$k], $idcampo, 1);
						}
						else{
							$this->campo_ficha->saveCampoSubficha($id, $campo, $arrValor[$k], $idcampo, 0);
						}
					}
				}
				
				if( isset($_POST['COD_CIP_FICHA']) && is_numeric($_POST['COD_CIP_FICHA']) ){
					$idFichaCip = $this->ficha->getCipExistente($_POST['COD_CIP_FICHA']);
					if(is_array($idFichaCip)){
						$idFichaCip = $idFichaCip['ID_FICHA'];
						$vinculo = $this->subficha->getByFichas($id, $idFichaCip);
						if(count($vinculo) <= 0){
							$this->subficha->vincular($idFichaCip, $id);
						}						
					}
				}
			}
			else{
				if($newFicha == true){
					// salvamos todos os campos remanescentes
					if(is_array($this->input->post("campo"))){
						$arrCampo = $this->input->post("campo");
						$arrValor = $this->input->post("valor");
						$arrSubficha = $this->input->post("subficha");
		
						foreach($arrCampo as $k=>$campo){
							$idcampo = $this->campo->getIdByName($campo);
							$idcampo = $idcampo[$k];
							$this->campo_ficha->saveCampoFicha($campo, $arrValor[$k], $arrSubficha[$k], $id, $idcampo);
						}
					}
				}
				else{
					$list = $this->campo_ficha->getByFicha($id);
					if(is_array($this->input->post("campo"))){
						$arrCampo = $this->input->post("campo");
						$arrValor = $this->input->post("valor");
						$arrSubficha = $this->input->post("subficha");
						$list = $this->campo_ficha->getByFicha($id);

						foreach($list as $item){
	
							$existe = false;
							if(is_array($this->input->post("campo"))){	
								foreach($arrCampo as $k=>$campo){

								}
							}

							if($existe == false){
								$this->campo_ficha->delete($item['ID_CAMPO_FICHA']);
							}
							//$this->campo_ficha->delete($item['ID_CAMPO_FICHA']);
						}
					}
					else{
						$this->campo_ficha->deleteByIdFicha($id);
					}
	
					// salvamos todos os campos remanescentes
					if(is_array($this->input->post("campo"))){	
						foreach($arrCampo as $k=>$campo){
	
							$idcampo = $this->campo->getIdByName($campo);
							$idcampo = $idcampo[0];
	
							$cf = $this->campo_ficha->getByFichaCampo($id, $idcampo['ID_CAMPO']);
	
							if(count($cf) > 0){
								$cf = $cf[0];
								$idCampoFicha = $cf['ID_CAMPO_FICHA'];
								unset($cf['ID_CAMPO_FICHA']);
								$this->campo_ficha->save( 
									array('VALOR_CAMPO_FICHA'=>$arrValor[$k], 
										'LABEL_CAMPO_FICHA'=>$campo, 
										'PERTENCE_SUBFICHA'=>$arrSubficha[$k] 
									),
									$idCampoFicha
								);
							}
							else{
								$cf['VALOR_CAMPO_FICHA'] = $arrValor[$k];
								$cf['LABEL_CAMPO_FICHA'] = $campo;
								$cf['PERTENCE_SUBFICHA'] = $arrSubficha[$k];
								$cf['ID_FICHA'] = $id;
								$cf['ID_CAMPO'] = $idcampo['ID_CAMPO'];
								$this->campo_ficha->save($cf);
							}
						}
					}
	
				}
			}
			
			$this->campo_ficha->updateIdCampoFicha();

			// grava o historico
			$historico = array(
				'ID_USUARIO' => $user['ID_USUARIO'],
				'ID_FICHA'=>$id,
				'NEW_STATUS'=> empty($_POST['ID_APROVACAO_FICHA']) ? null : $_POST['ID_APROVACAO_FICHA'],
				'COMENT_HISTORICO_FICHA'=>'--'
			);

			$this->ficha_historico->save($historico);
			// remove objetos antigos
			$this->objeto_ficha->deleteByFicha($id);


			// salvando os objetos escolhidos
			$salvos = array();
			if(!empty($_POST['objetos'])){
				foreach($_POST['objetos'] as $item){
					if(in_array($item['ID_OBJETO'],$salvos)){
						continue;
					}

					$item['PRINCIPAL'] = post('PRINCIPAL', true) == $item['FILE_ID'] ? 1 : 0;
					$item['ID_FICHA'] = $id;

					$salvos[] = $item['ID_OBJETO'];
					
					$this->objeto_ficha->save($item);
				}
			}

			// se for uma ficha combo
			if( $_POST['TIPO_FICHA'] == 'COMBO' ){
				// remove as fichas combo associadas anteriormente
				$this->ficha->removeFichasInCombo($id);

				// se tiver fichasCombo associadas
				if( !empty($_POST['fichasCombo']) ){
					foreach( $_POST['fichasCombo'] as $dados ){
						$this->ficha->addFichaInCombo($id, $dados['ID_FICHA']);
					}
				}
			}
			
			// remove todas fichas filhas
			$this->ficha->removeFichasFilhas($id);
			
			// remove todas fichas pais
			$this->ficha->removeFichasPais($id);
				
			// se for uma ficha do tipo 'PRODUTO' enbtao adiciona 'PAIS E FILHAS'
			if(post('TIPO_FICHA', true) == 'PRODUTO'){
				// adiciona as fichas filhas caso houver
				if((isset($_POST['fichaFilha'])) && (count($_POST['fichaFilha']) > 0)){		
					foreach($_POST['fichaFilha'] as $dados){
						$this->ficha->addFichasPaisFilhas($id, $dados['ID_FICHA']);
					}
				}
				
				// adiciona as fichas pais caso houver
				if((isset($_POST['fichaPai'])) && (count($_POST['fichaPai']) > 0)){		
					foreach($_POST['fichaPai'] as $dados){
						$this->ficha->addFichasPaisFilhas($dados['ID_FICHA'], $id);
					}
				}
			}
			
			//caso receba true booleano, ou seja,
			//for passado por outra funcao interna
			if($email === true){
				//recupera a ficha inteira com joins
				$ficha = $this->ficha->getById($id);

				//recupera o main object pra preview
				$main = $this->ficha->getMainObjectByFichaId($id);

				$ficha['DESC_APROVACAO_FICHA'] = $this->aprovacao_ficha->getDescricao($ficha['ID_APROVACAO_FICHA']);

				//Busca nome das regioes e monta array para exibicao na view
				foreach ($_POST['codigos'] as $codigoRegiao => $codigoRegionalFicha) {
					//Verifica se a regiao possui codigo da ficha
					if ( !empty($codigoRegionalFicha) && !empty($codigoRegiao) ) {

						//Busca Nome da Regiao
						$nomeRegiao = $this->regiao->getNomeById($codigoRegiao);
						if (!empty($nomeRegiao)) {
							$regiao = array ($nomeRegiao => $codigoRegionalFicha);
							$ficha['REGIOES'][] = $regiao;
						}
					}
				}

				//envia emails de notificacao associado a regra
				$erros = $this->email->sendEmailRegra('ficha_salvar',
													'Alteração de ficha ',
													'ROOT/templates/ficha_salvar',
													array('objeto'=>$ficha,'main'=>$main),
													0,
													$ficha['ID_CLIENTE']
													);
			}
			
			if(isset($_SESSION['ID_JOB']) && !empty($_SESSION['ID_JOB'])){
				redirect('carrinho/form/'.$_SESSION['ID_JOB']);
			} else if(isset($_SESSION['idJob']) && !empty($_SESSION['idJob'])){
				redirect('checklist/form/'.$_SESSION['idJob']);
			} else if( (isset($_SESSION['relatorioObjetoPendente'])) && (!empty($_SESSION['relatorioObjetoPendente'])) ){
				redirect('relatorios/relatorio_objetos_pendentes/tela');
			}
			//Se tiver permissao de aprovar e for nova ficha, volta pro form, pra poder aprovar
			else if(Sessao::hasPermission('ficha','modal_aprovacao') && $newFicha){
				redirect('ficha/form/'.$id);
			}else{
				redirect('ficha/lista/');
			}
		}

		// se chegou aqui, eh porque nao salvou
		$this->data['erros'] = $erros;
		$this->form($id);
	}


	/******************************************************************************/
	/*************************** funcao antiga ************************************/
	/******************************************************************************/

	/**
	 * Salva uma ficha com envio de email
	 * @author juliano.polito
	 * @param $id
	 * @return void
	 */
	function salvarComEmail($id=null){
		//o segundo parametro e booleano para evitar que alguem passe algo na url
		$this->salvar($id,true);
	}

	function modal_aprovacao($id=null){
		if($id!=null){
			$this->data['historico'] = $this->ficha->getHistoryByFichaId($id);
			$this->data['ficha'] = $this->ficha->getById($id);
		}

		//array status para gerar os radios
		$this->data['status_aprovacao'] = $this->aprovacao_ficha->getAll();

		$this->load->view('ROOT/ficha/modal_aprovacao',$this->data);
	}

	function saveHistory($id=null) {
		$user = Sessao::get('usuario');

		//prepara o array
		$historico = array(
			'ID_USUARIO'=>$user['ID_USUARIO'],
			'ID_FICHA'=>$id,
			'NEW_STATUS'=>$_POST['ID_APROVACAO_FICHA'],
			'COMENT_HISTORICO_FICHA'=>$_POST['COMENT_HISTORICO_FICHA']
		);

		//salva o historico de status da ficha
		$this->historico_ficha->save($historico);

		//atualiza o ID_APROVACAO_FICHA
		$ficha['ID_APROVACAO_FICHA'] = $_POST['ID_APROVACAO_FICHA'];
		
		if( $ficha['ID_APROVACAO_FICHA'] != '1' ){
			$this->cenario->desassociaFichaCenarioByIdFicha( $id );
		}		
		$this->ficha->save($ficha,$id);

		//recupera a ficha para usar no email
		$ficha = $this->ficha->getById($id);
		$ficha['DESC_APROVACAO_FICHA'] = $this->aprovacao_ficha->getDescricao($ficha['ID_APROVACAO_FICHA']);

		//recupera o main object pra preview
		$main = $this->ficha->getMainObjectByFichaId($id);

		//cria o titulo do email
		$titulo = sprintf('Alteração status ficha para %s', $ficha['DESC_APROVACAO_FICHA']);

		//Busca códigos regionais para colocar no email de aprovacao
		$codigosRegionais = $this->ficha->getCodigosRegionais($id);

		//Busca nome das regioes e monta array para exibicao no email
		foreach ($codigosRegionais as $codigoRegiao) {

			//Verifica se a regiao possui codigo da ficha
			if ( !empty($codigoRegiao['CODIGO']) ) {

				//Recupera nome da Regiao
				if (!empty($codigoRegiao['NOME_REGIAO'])) {
					$regiao = array ($codigoRegiao['NOME_REGIAO'] => $codigoRegiao['CODIGO']);
					$ficha['REGIOES'][] = $regiao;
				}
			}
		}

		//envia o email de acordo com a regra
		$this->email->sendEmailRegra('ficha_aprovar',
									'Alteração status ficha ',
									'ROOT/templates/ficha_aprovar',
									array('objeto'=>$ficha,'main'=>$main),
									0,
									$ficha['ID_CLIENTE']
		);

		//volta pra listagem após mudar o status
		if(isset($_SESSION['ID_JOB']) && !empty($_SESSION['ID_JOB'])){
			redirect('carrinho/form/'.$_SESSION['ID_JOB']);
		} else if(isset($_SESSION['idJob']) && !empty($_SESSION['idJob'])){
			redirect('checklist/form/'.$_SESSION['idJob']);
		}else if( (isset($_SESSION['relatorioObjetoPendente'])) && (!empty($_SESSION['relatorioObjetoPendente'])) ){
			redirect('relatorios/relatorio_objetos_pendentes/tela');
		}
		else{
			redirect('ficha/lista/');
		}
	}

	/**
	 * Exibe o historico de alteracoes da ficha
	 * @param int $idficha
	 * @return void
	 */
	public function historico($idficha = null){
		$this->data['ficha_atual'] = $this->ficha->getById($idficha);
		$this->data['alteracoes'] = $this->ficha->getHistory($idficha);

		$this->display('historico_ficha');
	}


	public function relatorio(){
		$user = Sessao::get('usuario');

		if(!empty($_POST)){
			Sessao::set('busca', $_POST);
		}

		$busca = (array) Sessao::get('busca');

		$user = Sessao::get('usuario');

		if(empty($busca['ORDER'])){
			$busca['ORDER'] = 'NOME_FICHA';
		}

		if(empty($busca['ORDER_DIRECTION'])){
			$busca['ORDER_DIRECTION'] = 'ASC';
		}

		if(!empty($user['ID_AGENCIA'])){
			$busca['ID_AGENCIA'] = $user['ID_AGENCIA'];

		}
		if(!empty($user['ID_CLIENTE'])){
			$busca['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}

		$this->loadFilters($busca);

		// $fichas = $this->ficha->listItems($busca,0,9999999999,$busca['ORDER'],$busca['ORDER_DIRECTION']);
		$fichas = $this->ficha->relatorioFichas($busca);

		$this->data['fichas'] = $fichas;
		$this->data['busca'] = $this->trocaCodigosPorNomes($busca);

		$this->display("relatorio");

	}
	
	public function form_subficha($idFichaPrincipal=null, $idSubficha=0){
		$user = Sessao::get('usuario');
		
		$fichaPrincipal = $this->ficha->getById($idFichaPrincipal);
		
		$this->data['subficha'] = array();
		if($idSubficha != 0){
			$this->data['subficha'] = $this->ficha->getById($idSubficha);
		}

		$filters = $_POST;
			
		if(!empty($user['ID_AGENCIA'])){
			$filters['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}

		if(!empty($user['ID_CLIENTE'])){
			$filters['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}
		
		$fichaPrincipal = $this->ficha->getById($idFichaPrincipal);		
		$_POST['ID_CLIENTE'] = $fichaPrincipal['ID_CLIENTE'];
		
		$this->data['campos'] = array();
		if(!empty($_POST['ID_CLIENTE'])){
			$this->data['campos'] = $this->campo->getByCliente($_POST['ID_CLIENTE']);
		}

		// Lista todos os campos da ficha principal que podem ser ralacionados com suas subfichas
		$this->data['relacionados'] = array();
		if(!empty($idFichaPrincipal)){
			$this->data['relacionados'] = $this->campo_ficha->getAllCamposFichaRelacionadoSubficha($idFichaPrincipal, $idSubficha);
		}
		
		// armazena as informacoes dos objetos vinculado a ficha
		$lista = $this->objeto_ficha->getByFicha($idSubficha);
		$_POST['objetos'] = $lista;

		// carrega os campos das fichas
		$lista = $this->campo_ficha->getAllCampoFicha($idSubficha);
		
		if(!empty($lista)){
			foreach($lista as $item){
				$_POST['id_campo'][] = $item['ID_CAMPO'];
				$_POST['campo'][] = $item['LABEL_CAMPO_FICHA'];
				$_POST['valor'][] = $item['VALOR_CAMPO_FICHA'];
				$_POST['principal'][] = $item['PRINCIPAL'];
			}
		}

		$_POST['ID_FICHA'] = $idFichaPrincipal;
		$this->data['podeIncluirCampo'] = Sessao::hasPermission('ficha','inner_incluir_campo') || $id == null;
		$this->data['podeExcluirCampo'] = Sessao::hasPermission('ficha','inner_excluir_campo') || $id == null;
		$this->data['podeIncluirObjeto'] = Sessao::hasPermission('ficha','inner_incluir_objeto') || $id == null;
		$this->data['podeExcluirObjeto'] = Sessao::hasPermission('ficha','inner_excluir_objeto') || $id == null;
		$this->data['podeAlterarPrincipal'] = Sessao::hasPermission('ficha','inner_alterar_principal') || $id == null;

		//print_rr($_POST);die;
		$this->load->view('ROOT/ficha/form_subficha', $this->data);
	}
	
	private function trocaCodigosPorNomes($item){
		$it = array();

		if(!empty($item['ID_CLIENTE'])){
			$cat = $this->cliente->getById($item['ID_CLIENTE']);
			$it['Cliente'] = $cat['DESC_CLIENTE'];
		}
		if(!empty($item['ID_CATEGORIA'])){
			$cat = $this->categoria->getById($item['ID_CATEGORIA']);
			$it['Categoria'] = $cat['DESC_CATEGORIA'];
		}
		if(!empty($item['ID_SUBCATEGORIA'])){
			$sub = $this->subcategoria->getById($item['ID_SUBCATEGORIA']);
			$it['Subcategoria'] = $sub['DESC_SUBCATEGORIA'];
		}
		if(!empty($item['ID_MARCA'])){
			$marca = $this->marca->getById($item['ID_MARCA']);
			$it['Marca'] = $marca['DESC_MARCA'];
		}
		if(!empty($item['ID_FABRICANTE'])){
			$marca = $this->fabricante->getById($item['ID_FABRICANTE']);
			$it['Fabricante'] = $marca['DESC_MARCA'];
		}
		if(!empty($item['ID_TIPO'])){
			$tipo = $this->tipo_objeto->getById($item['ID_TIPO']);
			$it['Tipo'] = $marca['DESC_MARCA'];
		}
		if(!empty($item['FLAG_ACTIVE_FICHA'])){
			$it['Status'] = $item['FLAG_ACTIVE_FICHA'] == 1 ? 'Ativo' : 'Inativo';
		}
		if(!empty($item['CODIGO_REGIONAL'])){
			$it['Código Regional'] = $item['CODIGO_REGIONAL'];
		}
		if(!empty($item['NOME_FICHA'])){
			$it['Nome da ficha'] = $item['NOME_FICHA'];
		}
		if(!empty($item['PENDENTES'])){
			$it['Com pendência?'] = $item['PENDENTES'] == 1 ? 'Sim' : '--';
		}
		if(!empty($item['TIPO_FICHA'])){
			$it['Tipo de ficha:'] = $item['TIPO_FICHA'];
		}
		if(!empty($item['CODIGOS_BARRA'])){
			$ap = array();
			foreach($item['CODIGOS_BARRA'] as $apr){
				$it['Códigos de barra'][] = $apr;
			}
		}
		if(!empty($item['ID_APROVACAO_FICHA'])){
			$ap = array();
			foreach($item['ID_APROVACAO_FICHA'] as $apr){
				$ap = $this->aprovacao_ficha->getById($apr);
				$it['Situação'][] = $ap['DESC_APROVACAO_FICHA'];
			}
		}

		return $it;


	}
	
	public function relatorio_cf($idJob){
		$idPraca = 450;
		$offset  = null;
		$limit   = null;
		
		$filtros = array();
	
		$job = $this->job->getById($idJob);
		
		if(count($job) > 0){
			if($job['ID_CLIENTE'] != 43){
				echo "Acesso negado";
				die;
			}
		}
		else{
			echo "Job não existe";
			die;
		}
		
		$praca = $this->praca->getById($idPraca);
		$itens = $this->excel->getItensForJobPraca($idJob, $idPraca, $offset, $limit, $filtros);
			
		$itensPai = array();
		$itensFilha = array();

		foreach($itens as $item){
			$item['OBJETO'] = array();
			$item['CAMPOS'] = array();
			$item['PRICING'] = array();
			$item['SUBFICHAS'] = array();
			
			$item['OBJETO'] = $this->objeto_ficha->getByFicha($item['ID_FICHA']);
			$item['CAMPOS'] = $this->campo_ficha->getByFicha($item['ID_FICHA']);

			$filhas = $this->excel->getItensFilhosForJobPraca($idJob, $idPraca, $item['ID_FICHA']);

			foreach($filhas as $filha){
				$filha['CAMPOS'] = array();
				$filha['CAMPOS'] = $this->campo_ficha->getByFicha($filha['ID_FICHA']);	
				$item['SUBFICHAS'][] = $filha;
			}

			$itensPai[] = $item;
		}
		
		$this->data['job'] = $job;
		$this->data['itens'] = $itensPai;
		$this->load->view('ROOT/ficha/relatorio_cf', $this->data);
	}
	
	public function excluir($idFicha,$pagina=""){
		if(!Sessao::hasPermission('ficha','excluir')){
			echo 'Permissão negada.';
		}
		else{
			$user = Sessao::get('usuario');
			$this->ficha->excluir($idFicha);
			redirect('ficha/lista/'.$pagina);
		}
	}

	/**
	 * Carrega os filtros para exibir na pagina
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data
	 * @return void
	 */
	private function loadFilters($data){
		$user = Sessao::get('usuario');

		$this->data['clientes']      = array();
		$this->data['categorias']    = array();
		$this->data['subcategorias'] = array();
		$this->data['marcas']        = array();
		$this->data['tipos']         = array();
		$this->data['fabricantes']   = array();
		$this->data['segmentos']     = array();
		$this->data['campos']        = array();
		$this->data['regioes']       = array();

		if( !empty($user['ID_CLIENTE']) ){
			$data['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}

		// recupera as agencias
		$rs = $this->segmento->getAll(0,10000,'DESC_SEGMENTO');
		$this->data['segmentos'] = $rs['data'];
		
		// recupera as fabricantes
		if($user['IS_HERMES'] == 1){
			$rs['data'] = $this->fabricante->getByDescricao('PADRÃO');
			if(count($rs['data']) > 0){
				$this->data['marcas'] = $this->marca->getByFabricante($rs['data'][0]['ID_FABRICANTE']);
			}
		}
		else{
			$rs = $this->fabricante->getAll(0,10000,'DESC_FABRICANTE');
		}
		$this->data['fabricantes'] = $rs['data'];

		// marcas
		if(!empty($data['ID_FABRICANTE'])){
			$this->data['marcas'] = $this->marca->getByFabricante($data['ID_FABRICANTE']);
		}

		////////////////////////////////////////////////////////////////
		////////////////// LISTAGEM DE CLIENTES ////////////////////////
		////////////////////////////////////////////////////////////////
		// se for uma agencia logada
		if( !empty($user['ID_AGENCIA']) ){
			$this->data['clientes'] = $this->cliente->getByAgencia($user['ID_AGENCIA']);

		// se for um cliente logado
		} else if( !empty($user['ID_CLIENTE']) ) {
			$item = $this->cliente->getById($user['ID_CLIENTE']);
			$this->data['clientes'] = array($item);
			$this->data['ID_CLIENTE'] = $user['ID_CLIENTE'];

		// se nao for nenhum dos dois
		} else {
			$list = $this->cliente->listItems($data, 0, 1000000);
			$this->data['clientes'] = $list['data'];
		}

		if(!empty($data['ID_CLIENTE'])){
			if( empty($user['ID_CLIENTE']) ){
				$this->data['categorias'] = $this->categoria->getByCliente($data['ID_CLIENTE']);
			} else {
				$this->data['categorias'] = $this->categoria->getByUsuarioCliente($user['ID_USUARIO'], $user['ID_CLIENTE']);
			}

			$this->data['campos'] = $this->campo->getByCliente($data['ID_CLIENTE']);

			// carregando as regioes
			$this->data['regioes'] = $this->regiao->getByCliente($data['ID_CLIENTE']);
		}

		if(!empty($data['ID_CLIENTE'])){
			$this->data['tipos'] = $this->tipo_objeto->getByCliente($data['ID_CLIENTE']);
		}

		if(isset($data['CATEGORIA']) && $data['CATEGORIA'] != null){
			$this->data['subcategorias'] = $this->categoria->getSubcategorias($data['CATEGORIA']);
		}

		if(isset($data['ID_CATEGORIA']) && $data['ID_CATEGORIA'] != null){
			$this->data['subcategorias'] = $this->categoria->getSubcategorias($data['ID_CATEGORIA']);
		}
	}

	/**
	 * Checa se o usuario logado tem permissao ou nao de alterar o job acessado
	 *
	 * Se nao tiver permissao, redireciona para a listagem
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idjob
	 * @return void
	 */
	protected function checaPermissaoAlterar($idficha){
		$user = Sessao::get('usuario');
		$ficha = $this->ficha->getById($idficha);

		$categorias = getCategoriasUsuario($ficha['ID_CLIENTE'],true);
		$goto = 'ficha/lista';

		if(!empty($user['ID_CLIENTE'])){
			if($ficha['ID_CLIENTE'] != $user['ID_CLIENTE']){
				redirect($goto);
			}

			if(!in_array($ficha['ID_CATEGORIA'],$categorias)){
				redirect($goto);
			}
		}

		if(!empty($user['ID_AGENCIA'])){
			$clientes = getValoresChave($this->cliente->getByAgencia($user['ID_AGENCIA']),'ID_CLIENTE');

			if(!in_array($ficha['ID_CLIENTE'],$clientes)){
				redirect($goto);
			}
		}
	}

}
