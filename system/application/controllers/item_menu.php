<?php
/**
 * controller para gerenciar os itens do menu
 *
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class Item_menu extends MY_Controller
{
	/**
	 * Construtor
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Item_menu
	 */
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/item_menu/';
	}

	/**
	 * Lista os itens do menu cadastrados
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $pagina Pagina atual de listagem
	 * @return void
	 */
	function lista($pagina=0) {

		if(!empty($_POST)){
			Sessao::set('busca', $_POST);
		}

		$dados = Sessao::get('busca');

		if(!is_array($dados)){
			$dados = array();
		}

		$limit = empty($dados['pagina']) ? 0 : $dados['pagina'];
		$lista = $this->itemMenu->listItems($dados, $pagina, $limit);
		$controllers = $this->controller->getAll(0,10000,'DESC_CONTROLLER');

		if(!empty($dados['ID_CONTROLLER'])){
			$this->data['functions'] = $this->function->getByController($dados['ID_CONTROLLER']);
		} else {
			$this->data['functions'] = array();
		}

		$this->data['podeAlterar'] = Sessao::hasPermission('item_menu','save');
		$this->data['podeRemover'] = Sessao::hasPermission('item_menu','remover');
		$this->data['busca'] = $dados;
		$this->data['controllers'] = $controllers['data'];
		$this->data['lista'] = $lista['data'];
		$this->data['paginacao'] = linkpagination($lista, $limit);

		$this->display('index');
	}

	/**
	 * Exibe o formulario de edicao do grupo
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do grupo quando estiver editando
	 * @return void
	 */
	function form($id=null) {

		if( !is_null($id) && $_SERVER['REQUEST_METHOD'] == 'GET' ){
			$_POST = $this->itemMenu->getById($id);
		}

		$controllers = $this->controller->getAll(0,10000,'DESC_CONTROLLER');

		if(!empty($_POST['ID_CONTROLLER'])){
			$this->data['functions'] = $this->function->getByController($_POST['ID_CONTROLLER']);
		} else {
			$this->data['functions'] = array();
		}

		$this->data['icone'] = $this->itemMenu->getIcon($id);
		$this->data['controllers'] = $controllers['data'];
		$this->display('form');
	}

	/**
	 * Persiste os dados informados no formulario no banco
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do grupo quando estiver editando
	 * @return void
	 */
	function save($id=null) {
		$erros = $this->itemMenu->validate($_POST);

		if(is_uploaded_file($_FILES['file']['tmp_name']) && !checaExtensaoValida($_FILES['file']['name']) ){
			$erros[] = 'Extensão de arquivo não permitida';
		}

		if( empty($erros) ) {
			$id = $this->itemMenu->save($_POST,$id);

			if(is_uploaded_file($_FILES['file']['tmp_name'])){
				$this->itemMenu->saveIcon($id, $_FILES['file']['tmp_name'], $_FILES['file']['name']);
			}

			redirect('item_menu/lista');
		}

		$this->data['erros'] = $erros;
		$this->form($id);
	}

	/**
	 * Remove um item cadastrado de menu
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $id
	 * @return void
	 */
	public function remover($id){
		$this->itemMenu->delete($id);
		redirect('item_menu/lista');
	}
}
