<?php
/**
 * Controle de subcategorias
 * 
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class Subcategoria extends MY_Controller {
	
	/**
	 * Construtor
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Subcategoria
	 */
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/subcategoria/';
	}
	
	/**
	 * Lista as subcategorias cadastradas
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $pagina Pagina atual
	 * @return void
	 */
	function lista($pagina=0) {
		
		if(!empty($_POST))  {
			Sessao::set('busca',$_POST);			
		}
		
		$busca = (array) Sessao::get('busca');

		$limit = empty($busca['pagina']) ? 0 : $busca['pagina'];

		$subcategorias = $this->subcategoria->listItems($busca,$pagina,$limit);

		// carrega as agencias
		$agencias = $this->agencia->getAll(0,1000,'DESC_AGENCIA');
		
		$user = Sessao::get('usuario');
		
		if( !empty($user['ID_AGENCIA']) ){
			$busca['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}
		if( !empty($user['ID_CLIENTE']) ){
			$busca['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}
		
		// carrega os clientes
		$filters = array(
			'STATUS_CLIENTE' => 1
		);
			
		$clientes = $this->cliente->listItems($filters, 0, 1000);
		$this->data['clientes'] = $clientes['data'];


		// carrega as categorias
		if(!empty($busca['ID_CLIENTE'])) {
			$filters = array(
				'ID_CLIENTE' => $busca['ID_CLIENTE']
			);
			
			$categorias = $this->categoria->listItems($filters, 0, 1000);
			$this->data['categorias'] = $categorias['data'];
		} else {
			$this->data['categorias'] = array();
		}
		
		
		$subcategorias = $this->subcategoria->listItems($busca, $pagina, $limit);
		
		$this->data['busca'] = $busca;
		$this->data['agencias'] = $agencias['data'];
		$this->data['subcategorias'] = $subcategorias['data'];
		$this->data['paginacao'] = linkpagination($subcategorias, $limit);
		$this->data['podeAlterar'] = Sessao::hasPermission('subcategoria','save');
		$this->display('index');
	}
	
	/**
	 * Exibe o formulario para edicao
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo da subcategoria ou null para uma nova
	 * @return void
	 */
	function form($id=null) {

		$categorias = $this->categoria->getAll(null, null, 'DESC_CATEGORIA');
		$this->data['categorias'] = arraytoselect($categorias['data'], 'ID_CATEGORIA', 'DESC_CATEGORIA','[Selecione]');
		
		if(!is_null($id) && $_SERVER['REQUEST_METHOD'] == 'GET'){
			$_POST = $this->subcategoria->getById($id);
		}

		$user = Sessao::get('usuario');
		
		// carrega os clientes
		$filters = array(
			'STATUS_CLIENTE' => 1
		);
		
		$clientes = $this->cliente->listItems($filters, 0, 1000);
		$this->data['clientes'] = $clientes['data'];

		if(!isset($_POST['ID_CLIENTE'])){
			$usuario = Sessao::get('usuario');
			
			if( !empty($usuario['ID_CLIENTE']) ){
				$_POST['ID_CLIENTE'] = $usuario['ID_CLIENTE'];
			}
		}

		// carrega as categorias
		if(!empty($_POST['ID_CLIENTE'])) {
			$filters = array(
				'ID_CLIENTE' => $_POST['ID_CLIENTE']
			);
			
			$categorias = $this->categoria->listItems($filters, 0, 1000);
			$this->data['categorias'] = $categorias['data'];
		} else {
			$this->data['categorias'] = array();
		}

		$this->display('form');
	}
	
	/**
	 * Grava uma nova subcategoria ou as alteracoes de uma existente
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo da subcategoria a ser alterada ou null para uma nova
	 * @return void
	 */
	function save($id=null) {
		$_POST['ID_SUBCATEGORIA'] = sprintf('%d', $id);
		$res = $this->subcategoria->validate($_POST);
		
		// se nao houve erros no preenchimento do formulario
		if( empty($res) ){
			$this->subcategoria->save($_POST,$id);
			redirect('subcategoria/lista');			
		}
		
		$_REQUEST['erros'] = $res;
		$this->form($id);
	}
	
}
