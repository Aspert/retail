<?php
class Veiculo extends MY_Controller
{
	function Veiculo()
	{
		parent::Controller();		
	}
	
	function index()
	{
		$this->load->view('ROOT/veiculo/index', $this->data);
	}
	
	function lista($pagina = 0)
	{
		if (isset ($_POST['DESC_VEICULO'])) 
		{
			$dados['veiculo'] = $_POST;
			$this->session->set_userdata('BUSCA', $dados);			
		}
					
		$limit = isset($this->session->userdata['BUSCA']['veiculo']['pagina']) ? $this->session->userdata['BUSCA']['veiculo']['pagina']:null ;
		
		$veiculos = $this->veiculo->getByLike($this->session->userdata['BUSCA']['veiculo'], $pagina,$limit);
		
		$this->data['veiculos'] = $veiculos['data'];
		
		$this->data['paginacao'] = linkpagination($veiculos, $limit);
		
		$this->load->view('ROOT/veiculo/index',$this->data);			
	}
	
	function form($id=null)
	{		
		
		if($midias = $this->midia->getAll(null, null, 'DESC_MIDIA'))
			$this->data['midias'] = arraytoselect($midias['data'], 'ID_MIDIA', 'DESC_MIDIA','[Selecione]');
			
	
		if($classificao = $this->classificacao->getAll(null, null, 'DESC_CLASSIFICACAO'))
			$this->data['target'] = arraytoselect($classificao['data'], 'ID_CLASSIFICACAO', 'DESC_CLASSIFICACAO','[Selecione]');
		
		if($id!=null)	
			$this->data['veiculo'] = $this->veiculo->getById($id);
	
		$this->load->view('ROOT/veiculo/form',$this->data);	
	}	
	function save($id=null)
	{				
		
		$_POST['data']['ID_CENTIMETRAGEM'] = $this->centimetragem->save($_POST['centimetragem'],$_POST['ID_CENTIMETRAGEM']);
				
		$this->veiculo->save($_POST['data'],$id);
	
		redirect('veiculo/lista');
	}
	
	function excluir($id=null)
	{
		$this->veiculo->deleteLogico($id);
		
		redirect('veiculo/lista');  
	}
}
