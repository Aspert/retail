<?php
/**
 * Controller para gerenciar agencias
 * @author Hugo Silva
 *
 */
class Agencia extends MY_Controller {
	/**
	 * Construtor
	 * @author Hugo Silva
	 * @return Cliente
	 */
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/agencia/';
	}
	
	/**
	 * Metodo inicial da controller
	 * Acessado quando nao especificado um metodo
	 * @author Hugo Silva
	 * @return void
	 */
	public function index(){
		$this->lista();
	}
	
	/**
	 * Lista os itens cadastrados
	 * 
	 * @author Hugo Silva
	 * @param int $pagina Pagina inicial da listagem
	 * @return void
	 */
	public function lista($pagina=0){
		if (!empty($_POST)) {
			$dados = $_POST;
			$this->session->set_userdata('__BUSCA', $dados);			
		} 
		
		$data = $this->session->userdata('__BUSCA');
		
		if(empty($_POST)){
			$data['STATUS_AGENCIA'] ='1';
		}
		
		if(!is_array($data)){
			$data = array($data);
		}
		$limit = !empty($data['pagina']) ? $data['pagina'] : 5;
		
		$busca = $data;
		if ((isset($data['STATUS_AGENCIA'])) && ($data['STATUS_AGENCIA'] == 2)){
			unset($data['STATUS_AGENCIA']);
		}
		$lista = $this->agencia->listItems($data, $pagina, $limit);
		$lista['limit'] = $limit;
		$this->data['podeAlterar'] = Sessao::hasPermission('agencia','save');
		$this->data['busca'] = $busca;
		$this->data['lista'] = $lista['data'];
		$this->data['paginacao'] = linkpagination($lista, $limit);
		$this->display('index');		
	}
	
	/**
	 * Exibe o formulario para edicao ou insercao
	 * 
	 * @author Hugo Silva
	 * @param int $id codigo da agencia
	 * @return void
	 */
	public function form($id=null){
		if(!is_null($id) && $_SERVER['REQUEST_METHOD'] == 'GET') {
			$_POST = $this->agencia->getById($id);
		}

		$this->data['logo'] = getLogo('img/agencia', $id);
		$this->data['ID_AGENCIA'] = $id;
		$this->display('form');
	}
	
	/**
	 * Grava os dados do formulario
	 * 
	 * @author Hugo Silva
	 * @param int $id
	 * @return void
	 */
	public function save($id=null){
		
		$_POST['ID_AGENCIA'] = $id;
		
		if( empty($id) ){
			$_POST['CHAVE_STORAGE_AGENCIA'] = ajustaChaveStorage($_POST['DESC_AGENCIA']);
		}
		
		$erros = $this->agencia->validate($_POST);
		
		if( isset($_FILES['logotipo']) && is_uploaded_file($_FILES['logotipo']['tmp_name']) ){
			$temp = 'files/' . $_FILES['logotipo']['name'];
			copy($_FILES['logotipo']['tmp_name'], $temp);
			$res = validaLogotipo($temp);
			unlink($temp);
			
			if( !$res ){
				$erros[] = 'Arquivo de logotipo inválido';
			}
		}
		
		if(empty($erros)){
			$id = $this->agencia->save($_POST,$id);
			
			$this->xinet->createPath($this->agencia->getPath($id));
			moveFile('logotipo', 'img/agencia' , $id);
			redirect('agencia/lista');
		}
		
		$this->data['erros'] = $erros;
		$this->form($id);
	}
	
}