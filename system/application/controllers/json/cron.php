<?php

/**
 * Tarefas a serem executadas pelo cron job
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 */
class Cron extends MY_Controller {

	protected $debug = true;
	protected $debugFile = '';


	public function __construct(){
		parent::__construct();
		$this->_templatesBasePath = '';
	}

	/**
	 * Chamado pelo cron 
	 * Metodo para disparar os emails de vencimento de etapas.
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return
	 */
	public function vencimento_etapa(){
		
		//Array com os jobs
		$listaJobs = array();
		
		//Array com as etapas do job
		$etapasJob = array();
		
		//Id da regra 'vencimento_etapa'
		$idRegraProcesso = $this->regraEmailProcessoEtapa->getIdByChave('vencimento_etapa');

		// pega os jobs
		$listaJobs = $this->job->getJobsAtivos();
		
		$this->debug('Jobs encontrados para envio --------------------------');
		
		//Verifica cada job
		foreach ($listaJobs as $job) {

			//Busca as etapas com datas do job
			$etapasJob = $this->job->getDataEtapasJob($job['ID_JOB']);
			
			//Indicador para busca da proxima etapa a ser enviada no email
			$iProximaEtapa=1;
			$proximaEtapa = array();
			
			if (!empty($etapasJob['ETAPA'])) {
				$proximaEtapa = $etapasJob['ETAPA'];
			}
			
			//Percorre as etapas
			foreach ($etapasJob['ETAPA'] as $etapa) {
				
				//Verifica se existe regra 'vencimento_etapa' cadastrada para essa etapa
				$qtdHorasAntesEnvio = $this->regraEmailProcessoEtapaUsuario->findHorasByRegraProcesso($idRegraProcesso, $job['ID_PROCESSO_ETAPA']);
				if (!empty($qtdHorasAntesEnvio) && $qtdHorasAntesEnvio != '00:00') {
					
					//qtd horas antes em segundos. Despreza os segundos da hora
					$qtdHoras = explode(':',$qtdHorasAntesEnvio);
					$horasAntesEnvioEmSegundos = $etapa['DATA_FINAL_ETAPA'] - segundos( $qtdHoras[0], $qtdHoras[1] );
					$horasAtualEmSegundos = mktime( date('H'), date('i'), 0, date('m'), date('d'), date('Y') );
					
					//Dispara o email de vencimento da etapa se a data de termino da etapa - qtd horas antes for igual a hora atual.
					if ($horasAtualEmSegundos == $horasAntesEnvioEmSegundos) {
						//Verifica se existe proxima etapa
						if (!empty($proximaEtapa[$iProximaEtapa])) {
							//A data da proxima etapa eh sempre a data final da etapa atual
							$job['DATA_PROXIMA_ETAPA'] = date('d/m/Y H:i',$etapa['DATA_FINAL_ETAPA']);
							//Recupera nome da proxima etapa
							$job['PROXIMA_ETAPA'] = $proximaEtapa[$iProximaEtapa]['DESC_ETAPA'];
					
							//Envia o email para os usuarios cadastrados na regra
							$this->email->sendEmailRegraProcesso(
								'vencimento_etapa'
								, $job['ID_PROCESSO_ETAPA']
								, 'Aviso de recebimento do job ' . $job['TITULO_JOB']
								, 'ROOT/templates/email_vencimento_etapa'
								, array('job' => $job)
							);
						}
					}
				}
				$iProximaEtapa++;
			}
		}
	}

	/**
	 * Chamado pelo cron 
	 * Metodo para disparar os emails de solicitacao de template
	 * @author Nelson Martucci Junior
	 * @link http://www.247id.com.br
	 * @return
	 */
	public function solicitar_template(){
		
		//Array com os jobs
		$listaJobs = array();
		
		//Array com as etapas do job
		$etapasJob = array();
		
		//Id da regra 'solicitar_template'
		$idRegraProcesso = $this->regraEmailProcessoEtapa->getIdByChave('solicitar_template');

		// pega os jobs
		$listaJobs = $this->job->getJobsAtivos();
		
		$this->debug('Jobs encontrados para envio --------------------------');
		
		//Verifica cada job
		foreach ($listaJobs as $job) {

			//Busca as etapas com datas do job
			$etapasJob = $this->job->getDataEtapasJob($job['ID_JOB']);
			
			//Percorre as etapas
			foreach ($etapasJob['ETAPA'] as $etapa) {
				
				//Verifica se existe regra 'solicitar_template' cadastrada para essa etapa
				$qtdHorasAntesEnvio = $this->regraEmailProcessoEtapaUsuario->findHorasByRegraProcesso($idRegraProcesso, $job['ID_PROCESSO_ETAPA']);
				if (!empty($qtdHorasAntesEnvio) && $qtdHorasAntesEnvio != '00:00') {
					
					//qtd horas antes em segundos. Despreza os segundos da hora
					$qtdHoras = explode(':',$qtdHorasAntesEnvio);
					$horasAntesEnvioEmSegundos = $etapa['DATA_INICIO_ETAPA'] - segundos( $qtdHoras[0], $qtdHoras[1] );
					$horasAtualEmSegundos = mktime( date('H'), date('i'), 0, date('m'), date('d'), date('Y') );
					
					//Dispara o email de solicitacao de template se a data de inicio da etapa - qtd horas antes for igual a hora atual.
					if ($horasAtualEmSegundos == $horasAntesEnvioEmSegundos) {
						//Envia o email para os usuarios cadastrados na regra
						$this->email->sendEmailRegraProcesso(
							'solicitar_template'
							, $job['ID_PROCESSO_ETAPA']
							, 'Providenciar template para o job ' . $job['TITULO_JOB']
							, 'ROOT/templates/solicitar_template'
							, array('job' => $job)
						);
						die;
					}
				}
			}
		}
	}
	
	/**
	 * Atualiza as fichas baseadas na data de vencimento
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return
	 */
	public function vencimento_fichas(){
		$this->ficha->atualizarStatusPorValidade();
		$this->debug( $this->db->queries[ count($this->db->queries) - 2] );
		$this->debug( end($this->db->queries) );
	}
	
	/**
	 * Atualiza os cenários baseados na data de vencimento
	 *
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @return
	 */
	public function vencimento_cenarios(){
		$this->cenario->atualizarStatusPorValidade();
		//$this->debug( $this->db->queries[ count($this->db->queries) - 2] );
		//$this->debug( end($this->db->queries) );
	}

	/**
	 * Cron para verificar se OPI foi processado
	 *
	 * @author Juliano Polito
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function opi(){
		$query = 'SELECT * FROM TB_EXCEL_OPI';
		$queryUpdate = 'UPDATE TB_EXCEL_OPI SET PROCESSADO_EXCEL_OPI = %d, DATA_TERMINO_EXCEL_OPI = "%s" WHERE ID_EXCEL_OPI = %d';
		$queryDelete = 'DELETE FROM TB_EXCEL_OPI WHERE ID_EXCEL_OPI = %d';

		// pega os itens do opi
		$rs = $this->db
			->query($query)
			->result_array();

		echo '<pre>------------------ CRON - VERIFICA ARQUIVO OPI ------------------',PHP_EOL;

		// para cada item encontrado no banco
		foreach($rs as $row){

			// arquivo de saida
			$path = STORAGE_OUT_PATH.$row['ID_EXCEL_OPI'].'_.pdf';
			echo 'Verificando: ', $path, PHP_EOL;

			// se existe
			if(file_exists($path)){

				// tempo decorrido
				$diff = time() - filemtime($path);

				// verifica se o arquivo esta em uso
				$inUse = file_exists(STORAGE_OUT_PATH.$row['ID_EXCEL_OPI'].'_.pdf-log.txt')
						&& ( file_exists($path));

				echo $inUse.PHP_EOL;

				// se nao estiver em uso
				if(empty($inUse)){

					// periodo para apagar o arquivo
					$periodo = 3600*48; // 2 dias

					// se a diferenca e maior que o periodo
					if($diff > $periodo && is_writable($path)){

						echo 'Encontrou acima do tempo: ', $row['ARQUIVO_EXCEL_OPI'], PHP_EOL;

						// tenta apagar o arquivo
						if(unlink($path)){
							$q = sprintf($queryDelete,$row['ID_EXCEL_OPI']);
							echo 'Executando:', $q, PHP_EOL;
							$this->db->query($q);
						}

					}else{

						// se ainda nao foi processado
						if($row['PROCESSADO_EXCEL_OPI'] != 1){

							// indica que encontrou no tempo
							echo 'Encontrou no tempo: ',$row['ARQUIVO_EXCEL_OPI'],' atualizando como processado.',PHP_EOL;
							$q = sprintf($queryUpdate,1,date('Y-m-d H:i:s'),$row['ID_EXCEL_OPI']);

							echo 'Executando:' , $q, PHP_EOL;
							$this->db->query($q);
						}
					}

				// se estiver em uso
				}else{

					echo 'Encontrou no tempo: ',$row['ARQUIVO_EXCEL_OPI'],' atualizando como NAO processado.',PHP_EOL;
					$q = sprintf($queryUpdate,0,$row['ID_EXCEL_OPI']);

					echo 'Executando:' , $q, PHP_EOL;
					$this->db->query($q);
				}

			// se nao encontrar o arquivo
			} else {

				echo 'Arquivo ', $path,' não encontrado.' , PHP_EOL;
				if($row['PROCESSADO_EXCEL_OPI'] == 1){
					$q = sprintf($queryDelete,$row['ID_EXCEL_OPI']);

					echo 'Executando:' , $q, PHP_EOL;
					$this->db->query($q);
				}
			}

			echo PHP_EOL;
		}
	}

	protected function debug($msg){
		if( $this->debug ){
			$bt = array_shift( debug_backtrace( true ) );
			$msg = '<pre>'
				. $msg
				. ' ('
				. $bt['file']
				. ' - '
				. $bt['line'] . ')'
				. PHP_EOL
				. '</pre>';

			echo $msg;

			if( !empty($this->debugFile) ){
				file_put_contents($this->debugFile, $msg, FILE_APPEND);
			}
		}

	}
	
	/**
	 * Chamado pelo cron.
	 * Bloqueia os jobs que nao estao na etapa correta
	 * @author Nelson Martucci
	 * @return unknown_type
	 */
	public function bloquear_job () {

		//Array com os jobs
		$listaJobs = array();
		
		//Array com as etapas do job
		$etapasJob = array();
		
		// pega os jobs
		$listaJobs = $this->job->getJobsAtivos();
		
		//Verifica cada job
		foreach ($listaJobs as $job) {
			
			//verifica se existe delay ativo
			//se houver delay, verificamos se ele expirou
			if($job['DT_DELAY_BLOQUEIO'] != '0000-00-00 00:00:00'){
				$delayedTime = strtotime($job['DT_DELAY_BLOQUEIO']);
				if(time() < $delayedTime){
					//se a hora do bloqueio com delay não estiver expirado pula o bloqueio do job
					//die($job['ID_JOB'].date('Y-m-d H:i:s', $delayedTime));
					continue;
				}else{
					//die('save');
					//se o bloqueio expirou, atualizamos o campo delay pra zero e permitimos a verificação de bloqueio normal
					$this->job->save(array('DT_DELAY_BLOQUEIO'=>'0000-00-00 00:00:00'), $job['ID_JOB']);
				}
			}
			
			//Verifica se o job nao estah bloqueado e nao estah finalizado
			if ( $job['ID_STATUS'] != $this->status->getIdByKey('BLOQUEADO') && 
				 $job['ID_ETAPA']  != $this->etapa->getIdByKey('FINALIZADO') ) {
			
				//Busca as etapas com datas do job
				$etapasJob = $this->job->getDataEtapasJob($job['ID_JOB']);
				
				//Percorre as etapas
				foreach ($etapasJob['ETAPA'] as $etapa) {
					//Verifica qual etapa o job deve estar
					if ($etapa['DATA_INICIO_ETAPA'] <= time() && $etapa['DATA_FINAL_ETAPA'] > time() ){
						//o job deve estar aqui
						//Verifica se o job estah na etapa correta
						if ($etapa['ID_PROCESSO_ETAPA'] != $job['ID_PROCESSO_ETAPA']) {
							//job nao esta na etapa correta
							
							//verifica se o job estah atrasado
							if ($job['ORDEM'] < $etapa['ORDEM']) {
								//Atrasado, vamos bloquear
								//Busca informacoes do status bloqueado
								
								$status = $this->status->getByKey('BLOQUEADO');
								
								//Grava Historico
								$data = array(
								'ID_ETAPA' => $job['ID_ETAPA'],
								'ID_JOB' => $job['ID_JOB'],
								'DATA_HISTORICO' => date('Y-m-d H:i:s'),
								'ID_PROCESSO_ETAPA' => $job['ID_PROCESSO_ETAPA'],
								'ID_STATUS' => $status['ID_STATUS']
								);
								
								//Envia email para a regra
								$this->email->sendEmailRegraProcesso('bloqueio_job',
											$job['ID_PROCESSO_ETAPA'],
											'O Job ' . $job['TITULO_JOB'] . ' foi bloqueado',
											'ROOT/templates/bloqueio_job',
											array(
												'job'=>$job,
												'etapa'=>$etapa,
												'status'=>$status
											)
								);
						
								//Salva historico
								$this->db->insert('TB_JOB_HISTORICO', $data);
				
								//Salva os dados do job
								$data = array(
									'ID_STATUS' => $status['ID_STATUS']
								);
								$this->job->save($data, $job['ID_JOB']);
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * Chamado pelo cron.
	 * Caso no momento em que for criada uma campanha ocorra algum erro na criacao da pasta da mesma
	 * essa cron fica encarregada de cria-la quando estiver tudo OK
	 * @author Sidnei Tertuliano Junior
	 * @return unknown_type
	 */
	public function criar_estrutura_campanha(){
		 $campanhasSemDiretorio = $this->campanha->getByDiretorioCriado( 0 );

		foreach( $campanhasSemDiretorio as $c ){
			$id = $c['ID_CAMPANHA'];

			if( !$this->campanha->verificaPathCampanha( $id ) ){
				$idsRetorno =  $this->xinet->createPath( $this->campanha->getPath( $id ) );
			}

			if( $idsRetorno['scriptResult']['data'] == "1" ){
				$this->campanha->save(array( 'DIRETORIO_CRIADO' => '1' ), $id);
			}
		}
	}
}