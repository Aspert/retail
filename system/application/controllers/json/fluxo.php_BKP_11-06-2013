<?php


class Fluxo extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/';
	}
	
	/**
	 * Recupera os itens de uma praca dentro do checklist
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function get_itens_checklist(){
		$usuario = Sessao::get('usuario');
		$idjob   = post('idjob',true);
		$idpraca = post('idpraca',true);
		$offset  = post('offset',true);
		$limit   = post('limit',true);
		
		$filtros = array();
		
		if(!empty($_POST['nome'])) $filtros['F.NOME_FICHA'] = $_POST['nome'];
		if(!empty($_POST['codigo'])) $filtros['F.COD_BARRAS_FICHA'] = $_POST['codigo'];
		
		$praca = $this->praca->getById($idpraca);
		$itens = $this->excel->getItensForJobPraca($idjob,$idpraca,$offset,$limit,$filtros);
		
		$itensPai = array();
		$itensFilha = array();

		foreach($itens as $key => $item){
			
			if ( ($usuario['IS_CLIENTE_HERMES'] ) || ($item['ID_CLIENTE'] == 38)) {
				$numero = $item['COD_BARRAS_FICHA'].$item['COD_CATALOGO'];
				$numero = digitoVerificadorHermes($numero);
			
				$itens[$key]['COD_BARRAS_FICHA'] = $numero;
				$item['COD_BARRAS_FICHA'] = $numero;
			}
			
			$filtros = array();
			$filhas = $this->excel->getItensFilhosForJobPraca($idjob, $idpraca, $item['ID_FICHA']);

			foreach($filhas as $key => $filha){
				$itensFilha[] = $filha;
				
				if ( ($usuario['IS_CLIENTE_HERMES']) || ($filha['ID_CLIENTE'] == 38)) {
					$numero_filha = $filha['COD_BARRAS_FICHA'].$filha['COD_CATALOGO'];
					$numero_filha = digitoVerificadorHermes($numero_filha);
					
					$itensFilha[$key]['COD_BARRAS_FICHA'] = $numero_filha;
					$filha['COD_BARRAS_FICHA'] = $numero_filha;
				}
			}

			array_push($itensPai, $item);
		}
		
		$total = $this->excel->countItensForJobPraca($idjob,$idpraca,$filtros);
		
		$this->assign('praca', $praca);
		$this->assign('itensPai', $itensPai);
		$this->assign('itensFilha', $itensFilha);
		$this->assign('offset', $offset);
		$this->assign('limit', $limit);
		$this->assign('totalProdutos', $total);
		$this->display('checklist/praca_itens');
	}
	
	/**
	 * Recupera os itens para exibir no carrinho
	 * 
	 * escreve os elementos em notacao json
	 * 
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function get_itens_carrinho(){
		$idjob   = post('idjob',true);
		$offset  = post('offset',true);
		$limit   = post('limit',true);

		$json    = array();
		
		$a    = array();
		
		$fichas = $this->job->getFichasUnicas($idjob,$offset,$limit);

		$pracas_cenarios = array();

		foreach($fichas as $item){	
			$cenarios = array();
			$cenarios = $this->cenario->getCenariosByIdFicha( $item['ID_FICHA'] );
						
			$item['CENARIOS'] = $cenarios;
			
			$pracas = $item['PRACAS'];

			if( count($pracas) > 0 ){
				foreach( $pracas as $p ){
					$pracas_cenarios[$item['ID_FICHA']][$p] = $this->excelItem->getCenarioByIdExcelPracaVersaoFicha($item['ID_EXCEL'], $p, $item['ID_EXCEL_VERSAO'], $item['ID_FICHA']);
					$item['CENARIO_PRACA_' . $p] = $this->excelItem->getCenarioByIdExcelPracaVersaoFicha($item['ID_EXCEL'], $p, $item['ID_EXCEL_VERSAO'], $item['ID_FICHA']);
				}
			}

			if(count($this->job->getFichasUnicasFilhas($idjob, $item['ID_FICHA'], null, null)) > 0){
				$item['TEM_FICHA_FILHA'] = 1;
				array_push($json, $item);
				
				foreach($this->job->getFichasUnicasFilhas($idjob, $item['ID_FICHA'], null, null) as $i){
					array_push($json, $i);
					array_push($a, $i);
				}
			}
			else{
				$item['TEM_FICHA_FILHA'] = 0;
				array_push($json, $item);
			}
						
		}	
		
		if( empty($json) ){
			exit;
		}

		echo json_encode($json);
	}
	
	/**
	 * Recupera os itens da logistica
	 * 
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function get_itens_logistica(){
		$idjob   = post('idjob',true);
		$idpraca = post('idpraca',true);
		$offset  = post('offset',true);
		$limit   = post('limit',true);

		$filtros = array();
		
		if(post('nome',true)!='') $filtros['F.NOME_FICHA'] = post('nome',true);
		if(post('codigo',true)!='') $filtros['F.COD_BARRAS_FICHA'] = post('codigo',true);
		
		$job = $this->job->getById($idjob);
		$filtros['F.ID_CATEGORIA'] = getCategoriasUsuario($job['ID_CLIENTE']);
		
		$praca = $this->praca->getById($idpraca);
		$itens = $this->excel->getItensForJobPraca($idjob,$idpraca,$offset,$limit,$filtros);

		$itensPai = array();
		$itensFilha = array();

		foreach($itens as $item){
			$filtros = array();
			$filhas = $this->excel->getItensFilhosForJobPraca($idjob, $idpraca, $item['ID_FICHA']);

			foreach($filhas as $filha){
				$itensFilha[] = $filha;
			}
			
			$item['COR_CENARIO'] = $this->cenario->pegarCor( $item['ID_CENARIO'] );

			array_push($itensPai, $item);
		}

		$total = $this->excel->countItensForJobPraca($idjob,$idpraca,$filtros);

		$this->assign('total', $total);
		$this->assign('praca', $praca);
		$this->assign('itensPai', $itensPai);
		$this->assign('itensFilha', $itensFilha);
		$this->assign('offset', $offset);
		$this->assign('limit', $limit);

		$this->display('logistica/praca_itens');
	}
	
	/**
	 * Recupera os itens da logistica
	 * 
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function get_itens_logistica_copia(){
		$idjob   = post('idjob',true);
		$idpraca = post('idpraca',true);		
		
		$filtros = array();
		if( !empty($_POST['codigos']) ){
			//$filtros['F.ID_FICHA'] = $_POST['codigos'];
		}
		
		$job = $this->job->getById($idjob);
		$filtros['F.ID_CATEGORIA'] = getCategoriasUsuario($job['ID_CLIENTE']);
		
		$itens = $this->excel->getItensForJobPraca($idjob,$idpraca,null,null,$filtros,true);

		echo json_encode($itens);
		exit;
	}

	public function salvar_itens_logistica_apagados(){
		// desabilita o tempo limite
		set_time_limit(0);

		// id do job
		$idJob = post('idjob', true);

		if(isset($idJob) && is_numeric($idJob)){
			// dados do job
			$job = $this->job->getById($idJob);
			
			// itens apagados
			$itensApagados   = post('apagados', true);

			// verifica se tem itens apagados
			if(count($itensApagados) > 0){
				// cria array pra armazenar os apagados pais e filhas
				$apagados = array();
				
				// armazena os dados para enviar no email
				$dadosEmail = array();
		
				// caso uma ficha pai fox apagada, ira pegar as filhas para apagar tbm
				foreach($itensApagados as $itemApagado){
					if($itemApagado != ""){
						$item = $this->excelItem->getById($itemApagado);
						if(isset($item['ID_FICHA']) && isset($item['ID_PRACA']) && isset($item['ID_EXCEL'])){
							$idFicha = $item['ID_FICHA'];
							$idPraca = $item['ID_PRACA'];
							$idExcelVersao = $item['ID_EXCEL'];
										
							array_push($apagados, $itemApagado);
							
							$filhosApagados = $this->excelItem->getFilhasByVersaoPraca($idExcelVersao, $idPraca, $idFicha);
							
							if(count($filhosApagados) > 0){
								$item['TIPO'] = 'Pai';
							}
							else{
								$item['TIPO'] = '';
							}

							if($item['ID_FICHA_PAI'] > 0){
								$item['TIPO'] = 'Filha';
							}
							
							array_push($dadosEmail, $item);
							
							foreach($filhosApagados as $filhoApagado){
								$filhoApagado['TIPO'] = 'Filha';
								array_push($apagados, $filhoApagado['ID_EXCEL_ITEM']);
								array_push($dadosEmail, $filhoApagado);
							}
						}
					}
				}

				$apagados = (array_fill_keys($apagados, array('STATUS_ITEM' => '0')));

				if(count($apagados) > 0){
					$this->job->saveLogistica($id, $apagados);
										
					$erros = $this->email->sendEmailRegra('produto_excluido',
									'Produto Excluido do Job - '.$job['TITULO_JOB'],
									'ROOT/templates/produto_excluido',
									array(
										'apagados' => $dadosEmail,
										'objeto' => $job,
									),
									$job['ID_AGENCIA'],
									$job['ID_CLIENTE']
								);
				}
			}
		}
		
	}
	
	public function salvar_itens_logistica(){
		// desabilita o tempo limite
		set_time_limit(0);
		// id do job
		$id = post('idjob',true);
		// pega os itens do post
		$itens = empty($_POST['item']) ? array() : $_POST['item'];
		// salva o carrinho
		$this->job->saveLogistica($id, $itens);
		// envia o retorno
		echo 'ok';
	}
	
	/**
	 * Recupera os itens do pricing
	 * 
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function get_itens_pricing(){
		$idjob   = post('idjob',true);
		$idpraca = post('idpraca',true);
		$offset  = post('offset',true);
		$limit   = post('limit',true);
		
		$filtros = array();
		if( post('nome_ficha',true)!= '') {
			$filtros['F.NOME_FICHA'] = post('nome_ficha',true);	
		}
		
		if( post('codigo_ficha',true)!= '') {
			$filtros['F.COD_BARRAS_FICHA'] = post('codigo_ficha',true);
		}
		
		$job = $this->job->getById($idjob);
		
		$parcelaDinamica = false;
		if(count($this->parcela_dinamica->listByCliente($job['ID_CLIENTE'])) > 0){
			$parcelaDinamica = true;
		}
		

		$filtros['F.ID_CATEGORIA'] = getCategoriasUsuario($job['ID_CLIENTE']);

		$praca = $this->praca->getById($idpraca);
		$itens = $this->excel->getItensForJobPraca($idjob, $idpraca, $offset, $limit, $filtros);

		$itensPai = array();
		$itensFilha = array();

		foreach($itens as $item){
			$filtros = array();
			$filhas = $this->excel->getItensFilhosForJobPraca($idjob, $idpraca, $item['ID_FICHA']);
			foreach($filhas as $filha){
				$itensFilha[] = $filha;
			}
			
			$item['COR_CENARIO'] = $this->cenario->pegarCor( $item['ID_CENARIO'] );

			array_push($itensPai, $item);
		}
	
		$total = $this->excel->countItensForJobPraca($idjob, $idpraca, $filtros);

		$this->assign('idCliente', $job['ID_CLIENTE']);
		$this->assign('totalProdutos', $total);
		$this->assign('parcelaDinamica', $parcelaDinamica);
		$this->assign('praca', $praca);
		$this->assign('itensPai', $itensPai);
		$this->assign('itensFilha', $itensFilha);
		$this->assign('offset', $offset);
		$this->assign('limit', $limit);
		$this->display('pricing/praca_itens');
	}
	
	/**
	 * recupera os itens em notacao json para copia de valores
	 * 
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function get_itens_pricing_copia(){
		$idjob   = post('idjob',true);
		$idpraca = post('idpraca',true);
		
		$filtros = array();
		if( post('nome_ficha',true)!= '') {
			$filtros['F.NOME_FICHA'] = post('nome_ficha',true);	
		}
		
		if( post('codigo_ficha',true)!= '') {
			$filtros['F.COD_FICHA'] = post('codigo_ficha',true);
		}
		
		if( !empty($_POST['codigos']) ){
			//$filtros['F.ID_FICHA'] = $_POST['codigos'];
		}
		
		$job = $this->job->getById($idjob);
		$filtros['F.ID_CATEGORIA'] = getCategoriasUsuario($job['ID_CLIENTE']);
		
		$praca = $this->praca->getById($idpraca);
		$itens = $this->excel->getItensForJobPraca($idjob, $idpraca, null, null, $filtros, true);
		$total = $this->excel->countItensForJobPraca($idjob, $idpraca, $filtros);
		
		$resultado = array();
		$campos = explode(',','PRECO_UNITARIO,PRECO_CAIXA,PRECO_VISTA,PRECO_DE,PRECO_POR,ECONOMIZE,ENTRADA,PRESTACAO,TOTAL_PRAZO');
		foreach($itens as $item){
			foreach($campos as $campo){
				$item[$campo] = money($item[$campo]);
			}
			$resultado[] = $item;
		}
		
		echo json_encode($resultado);
	}
	
	/**
	 * Salva os dados de uma pagina do pricing
	 * 
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function salvar_pricing(){
		if(!empty($_POST['item'])){
			$this->pricing->savePricing( $_POST['item'] );
			
			foreach($_POST['item'] as $item){
				if(!empty($item['ID_EXCEL_ITEM'])){
					$this->excelItem->save($item, $item['ID_EXCEL_ITEM']);
				}
			}
		}
	}
	
	/**
	 * escreve os itens pertencentes a uma pagina 
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function get_itens_paginacao(){
		$idjob   = post('idjob',true);
		$idpraca = post('idpraca',true);
		 
		$filtros = array();
		$filtros['I.PAGINA_ITEM'] = post('pagina',true);
		
		
		$praca = $this->praca->getById($idpraca);
		$itens = $this->excel->getItensForJobPraca($idjob, $idpraca, null, null, $filtros);
		
		$destaques = $this->session->userdata('destaque_paginacao');
		
		$resultado = array();
		$campos = explode(',','PRECO_UNITARIO,PRECO_CAIXA,PRECO_VISTA,PRECO_DE,PRECO_POR,ECONOMIZE,ENTRADA,PRESTACAO,TOTAL_PRAZO');
		foreach($itens as $item){
			foreach($campos as $campo){
				$item[$campo] = money($item[$campo]);
			}
			if( $destaques ){
				$arrDestaques = json_decode($destaques, true);
				if ( isset($arrDestaques[$idjob][$idpraca]) ) {
					$item['DESTAQUE'] = ( ( ( array_search($item['ID_FICHA'], $arrDestaques[$idjob][$idpraca] ) ) === false )?'':'_alterada');
				}
			}
			
			$item['COR_CENARIO'] = $this->cenario->pegarCor( $item['ID_CENARIO'] );
			
			$resultado[] = $item;
		}
		if ( isset($arrDestaques[$idjob][$idpraca]) ){
			unset($arrDestaques[$idjob][$idpraca]);
//			$this->session->set_userdata('destaque_paginacao', json_encode($arrDestaques));
		}

		echo json_encode($resultado);
	}
	
	/**
	 * Salva a paginacao atual
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function salvar_paginacao(){
		// se foi enviado por post
		if( !empty($_POST['item']) ){
			
			// coloca o job em andamento novamente
			$this->job->setStatus($id, $this->status->getIdByKey('EM_APROVACAO'));
			$this->paginacao->savePaginacao( $_POST['item'] );
		}
	}
	
	/**
	 * Remove um item da paginacao
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function remove_item_paginacao(){
		$id = post('iditem',true);
		
		$item = array();
		$item[$id]['PAGINA_ITEM'] = 0;
		$item[$id]['ORDEM_ITEM'] = 0;
		
		$this->paginacao->savePaginacao( $item );
		
		$ei = $this->excelItem->getById($id);
		$subficha = $this->excelItem->getFilhasByVersaoPraca($ei['ID_EXCEL'], $ei['ID_PRACA'], $ei['ID_FICHA']);
		foreach($subficha as $sf){
			$item = array();
			$item[$sf['ID_EXCEL_ITEM']]['PAGINA_ITEM'] = 0;
			$item[$sf['ID_EXCEL_ITEM']]['ORDEM_ITEM'] = 0;
			$this->paginacao->savePaginacao( $item );	
		}
	}
	
	/**
	 * Verifica se a paginacao esta de acordo com o plano de marketing do job
	 * 
	 * E enviado por post o codigo do job (ID). Atraves dele verificamos
	 * se todas categorias foram preenchidas corretamente conforme as paginas.
	 * 
	 * Se houver resultado, escrevemos um html para ser recebido via JSON.
	 * Se nao houver resultado, indica que esta tudo certo e escreve a palavra
	 * "ok".
	 * 
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function checar_paginacao(){
		// pega o codigo do job
		$id = post('id', true);
		// se nao estiver vazio
		if( !empty($id) ){
			// pega o resultado da checagem
			$resultado = $this->job->checarPaginacao($id);
			
			// se estiver vazio
			if( empty($resultado) ){
				// indica que passou
				echo 'ok';
			// mas se tiver resultado
			} else {
				// coloca o resultado na view
				$this->assign('resultado', $resultado);
				// exibe o resultado da view
				$this->display('paginacao/resultado_checagem');
			}
		// se nao informou o id do job
		} else {
			// indica que nao informou o job
			echo 'Indique o numero do job';
		}
	}

	// calcula a parcela dinamica a apartir do cliente e valor a vista
	public function calculaParcelaDinamica(){
		$idCliente = $_POST['cliente'];
		$valor = $_POST['valor'];
		$pa = $this->parcela_dinamica->calculaParcelaDinamica($idCliente, money2float($valor), true);
		if(count($pa) > 0){
			echo json_encode($pa);
		}
		else{
			echo '';
		}
	}
	
	
	
}