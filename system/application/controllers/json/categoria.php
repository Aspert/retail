<?php
class Categoria extends MY_Controller
{
	function Categoria()
	{
		parent::Controller();
	}
	
	function getAllSub()
	{		
		$json = getJson();
		
		$response['data'] = $this->categoria->getAllSub($json->id);						
				
		$response['combo'] = $json->combo;
		
		$response['value'] = $json->value;
		
		setJson($response);			
	}
	
	function getSubByCategoria(){
		
		$json = getJson();
		
		$response['data'] = $this->subcategoria->getByCategoria($json->id);
		
		$response['combo'] = $json->combo;
		
		$response['value'] = $json->value;
		
		setJson($response);
	}
	
	function getAllByUsuario()
	{
		$json = getJson();
		
		if($sel=$this->categoria->getSelecionado($json->id))
			$response['selecionados'] =$sel; 
		else
			$response['selecionados'] ='';
		
		if($disp = $this->categoria->getDisponivel($json->id))
			$response['disponiveis'] = $disp; 
		else
			$response['disponiveis'] ='';
		
		setJson($response);
	}
	
	function getAll()
	{
		$categoria = $this->categoria->getAll();
		$response['categorias'] = $categoria['data']; 		
		setJson($response);
	}
	
	function getAllBySearch()
	{
		$json = getJson();		
		$categoria = $this->categoria->getAllByUserAndCarrinhoInCategoria($json->id_carrinho);
		$response['categorias'] = $categoria;  		
		setJson($response);		
	}
	
	
	public function getSubcategorias(){
		$list = array();
		if(!empty($_REQUEST['id'])){
			$list = $this->categoria->getAllSub($_REQUEST['id']);
		}
		
		echo json_encode($list);
		exit;
	}
	
	public function getSubcategoriaByDescCategoria(){
		$descricao = $_POST['descricao'];
		$idCategoria = $_POST['idCategoria'];
		$subcategoria = $this->subcategoria->getByDescCategoria($descricao, $idCategoria);
		setJson($subcategoria);		
	}
}
?>
