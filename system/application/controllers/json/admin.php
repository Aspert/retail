<?php
/**
 * Acoes a serem efetuadas via Ajax na parte de administracao
 *
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class Admin extends MY_Controller
{
	/**
	 * Construtor
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Admin
	 */
	function __construct(){
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/';
	}

	/**
	 * Retorna uma representacao JSON dos clientes de uma agencia
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function getClientesByAgencia(){
		$idAgencia = isset($_POST['id']) ? $_POST['id'] : 0;
		$data = array();

		if( empty($idAgencia) ){
			$data = $this->cliente->getAll();
		} else {
			$data = $this->cliente->getByAgencia($idAgencia);
		}

		echo json_encode($data);
		exit;
	}

	/**
	 * Retorna uma representacao JSON dos clientes
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function getClientes(){
		$user = Sessao::get('usuario');
		$clientes = array();

		if( !empty($user['ID_AGENCIA']) ){
			$clientes = $this->cliente->getByAgencia($user['ID_AGENCIA']);
		} else {
			$rs = $this->cliente->listItems(array('STATUS_CLIENTE' => 1), 0, 100000);
			$clientes = $rs['data'];
		}

		echo json_encode($clientes);
		exit;
	}

	/**
	 *
	 * @author Juliano Polito
	 * @return void
	 */
	public function getProdutosByCliente($usuarioLogado=1){
		$user = Sessao::get('usuario');
		$id = sprintf('%d', post('id',true));

		if(empty($usuarioLogado)){
			$data = $this->produto->getByCliente($id);
		} else {
			$data = getProdutosUsuario($id,$user['ID_AGENCIA']);
		}

		echo json_encode($data);
		exit;
	}

	/**
	 *
	 * @author Juliano Polito
	 * @return void
	 */
	public function getProdutosByAgencia(){
		$user = Sessao::get('usuario');
		$id = sprintf('%d', post('id',true));
		$data = getProdutosUsuario($user['ID_CLIENTE'], $id);

		echo json_encode($data);
		exit;
	}

	/**
	 * recupera as pracas de uma regiao
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function getPracasByRegiao(){
		$idregiao = post('id',true);
		$lista = $this->praca->getByRegiao($idregiao);

		echo json_encode($lista);
		exit;
	}

	/**
	 * Recupera as campanhas de um cliente
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return unknown_type
	 */
	public function getCampanhasByCliente(){
		$idCliente = isset($_POST['id']) ? $_POST['id'] : 0;
		$data = $this->campanha->getByCliente($idCliente);

		echo json_encode($data);
		exit;
	}

	/**
	 * Recupera as campanhas de um produto
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function getCampanhasByProduto(){
		$idProduto = isset($_POST['id']) ? $_POST['id'] : 0;
		$data = $this->campanha->getByProduto($idProduto);

		echo json_encode($data);
		exit;
	}

	/**
	 * Recupera as functions a partir de uma controller
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return string
	 */
	public function getFunctionsByController(){
		$idController = isset($_POST['id']) ? $_POST['id'] : 0;
		$data = $this->function->getByController($idController);

		echo json_encode($data);
		exit;

	}

	/**
	 * Atualiza a ordem de um item do menu
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function updateItemMenuOrder(){
		if(!empty($_POST['lista'])){
			foreach($_POST['lista'] as $ordem => $id){
				$this->itemMenu->updateOrder($id, $ordem);
			}
		}
	}

	/**
	 * Recupera as categorias de um cliente
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function getCategoriasByCliente(){
		$user = Sessao::get('usuario');
		$id = empty($_POST['id']) ? 0 : $_POST['id'];

		if( empty($user['ID_CLIENTE']) ){
			$lista = $this->categoria->getByCliente($id);
		} else {
			$lista = $this->categoria->getByUsuarioCliente($user['ID_USUARIO'], $user['ID_CLIENTE']);
		}

		echo json_encode($lista);
		exit;
	}

	/**
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function getTiposObjetosByCliente(){
		$id = empty($_POST['id']) ? 0 : $_POST['id'];
		$lista = $this->tipo_objeto->getByCliente($id);

		echo json_encode($lista);
		exit;
	}

	public function getCamposByCliente(){
		$filters = array();
		$filters['ID_CLIENTE'] = $_POST['idCliente'] == '' ? -1 : $_POST['idCliente'];

		$lista = $this->campo->listItems($filters);

		echo json_encode($lista['data']);
		exit;
	}

	/**
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function getSubcategoriasByCategoria(){
		$id = empty($_POST['id']) ? -1 : $_POST['id'];
		$lista = $this->categoria->getSubcategorias($id);

		echo json_encode($lista);
		exit;
	}

	/**
	 * Recupera as categorias pela linha de produto
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function getCategoriasByProduto(){
		$user = Sessao::get('usuario');
		$id = empty($_POST['id']) ? -1 : $_POST['id'];

		if( empty($user['ID_CLIENTE']) ){
			$lista = $this->categoria->getByProduto($id);
		} else {
			$lista = $this->categoria->getByUsuarioCliente($user['ID_USUARIO'], $user['ID_CLIENTE']);
		}

		echo json_encode($lista);
		exit;
	}

	/**
	 * Recupera templates pelo codigo da campanha
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function getTemplatesByCampanha(){
		$id = empty($_POST['id']) ? -1 : $_POST['id'];
		$rs = $this->template->listItems(array('ID_CAMPANHA' => $id,'STATUS_TEMPLATE'=>1), 0, 1000);

		echo json_encode($rs['data']);
		exit;
	}

	/**
	 * Recupera os dados de um template
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function getTemplateData(){
		$id = empty($_POST['id']) ? -1 : $_POST['id'];

		$t = $this->template->getById($id);
		$t['PREVIEW_TEMPLATE'] = $this->template->getFileIdPreview($id);

//		$tpath = $this->template->getPath($t['ID_TEMPLATE']);
//		$tfile = $t['ID_TEMPLATE'].'.jpg';
//		$tfilePath = utf8MAC($tpath.$tfile);
//		if(is_file($tfilePath)){
//			$tfileID = $this->xinet->getFilePorPath($tpath,$tfile);
//			$t['PREVIEW_TEMPLATE'] = $tfileID;
//		}

		echo json_encode($t);
		exit;
	}

	/**
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function getMarcasByFabricante(){
		$id = empty($_POST['id']) ? 0 : $_POST['id'];
		$rs = $this->marca->getByFabricante($id);

		echo json_encode($rs);
		exit;
	}


	/**
	 *
	 * @author juliano.polito
	 * @return unknown_type
	 */
	public function getTiposByClienteComCategoria(){
		$id = empty($_POST['id']) ? 0 : $_POST['id'];
		$rs = $this->tipo_objeto->getByClienteComCategoria($id);

		echo json_encode($rs);
		exit;
	}



	/**
	 * Recupera os dados para enviar para o uploader
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function dataUploaderObjetos(){

		$user = Sessao::get('usuario');
		$data['ID_AGENCIA'] = post('ID_AGENCIA',true);
		$data['ID_CLIENTE'] = post('ID_CLIENTE',true);
		$data['ID_TIPO_OBJETO'] = post('ID_TIPO_OBJETO',true);
		$data['ID_CATEGORIA'] = post('ID_CATEGORIA',true);
		$data['ID_SUBCATEGORIA'] = post('ID_SUBCATEGORIA',true);

		if(empty($data['ID_CLIENTE'])){
			$data['ID_TIPO_OBJETO'] = '';
			$data['ID_CATEGORIA'] = '';
			$data['ID_SUBCATEGORIA'] = '';
		}

		// iniciamos o objeto de cliente
		$cl = array(
			'data' => array()
		);

		$data['STATUS_CLIENTE'] = 1;
		$data['STATUS_AGENCIA'] = 1;

		// se nao escolheu cliente
		if( empty($data['ID_CLIENTE']) ){
			// se for um cliente
			if( !empty($user['ID_CLIENTE']) ){
				// ja indicamos o cliente
				$data['ID_CLIENTE'] = $user['ID_CLIENTE'];
				$cl = $this->cliente->listItems($data, 0, 100000);

			// mas se nao for cliente
			} else {
				// eh agencia?
				if( !empty($user['ID_AGENCIA']) ){
					// pegamos todos os clientes da agencia
					$cl['data'] = $this->cliente->getByAgencia($user['ID_AGENCIA']);

				// se nao for agencia
				} else {
					// mostra todos
					$cl = $this->cliente->listItems($data, 0, 10000, 'DESC_CLIENTE', 'ASC');
				}
			}
		}

		if(empty($data['ID_CATEGORIA'])) {
			$data['ID_SUBCATEGORIA'] = -1;
		}

		if(empty($data['ID_CLIENTE'])){
			$data['ID_CLIENTE'] = -1;
		}

		$tp = $this->tipo_objeto->listItems($data,0,10000);
		
		$data['FLAG_ACTIVE_CATEGORIA'] = 1;
		$data['FLAG_ACTIVE_SUBCATEGORIA'] = 1;
		
		$sub = $this->subcategoria->listItems($data,0,10000);
		$cat['data'] = array();

		if( empty($user['ID_CLIENTE']) ){
			$cat['data'] = $this->categoria->getByCliente($data['ID_CLIENTE']);
		} else {
			$cat['data'] = $this->categoria->getByUsuarioCliente($user['ID_USUARIO'], $user['ID_CLIENTE']);
		}

		// primeiro item sempre fica vazio
		array_unshift($cl['data'], array('DESC_CLIENTE'=>'- selecione -'));
		array_unshift($tp['data'], array('DESC_TIPO_OBJETO'=>'- selecione -'));
		array_unshift($cat['data'], array('DESC_CATEGORIA'=>'- selecione -'));
		array_unshift($sub['data'], array('DESC_SUBCATEGORIA'=>'- selecione -'));

		$result['clientes'] = $cl['data'];
		$result['tipos'] = $tp['data'];
		$result['categorias'] = $cat['data'];
		$result['subcategorias'] = $sub['data'];

		header("Content-Type: text/xml");
		echo '<?xml version="1.0" encoding="UTF-8"?>';
		echo XmlUtil::array2xml($result,false);
	}

	public function getUsuariosGACP(){
		$grupo = empty($_POST['grupo']) ? null : $_POST['grupo'];
		$agencia = empty($_POST['agencia']) ? null : $_POST['agencia'];
		$cliente = empty($_POST['cliente']) ? null : $_POST['cliente'];
		$produto = empty($_POST['produto']) ? null : $_POST['produto'];

		$rs = $this->gacp->getUsuarios($grupo, $agencia,$cliente,$produto);

		echo json_encode($rs);

		exit;
	}

	/**
	 * Gera um json de um array com todas as tags disponiveis para uso em
	 * templates indesign, separados por categoria
	 * @author juliano.polito
	 * @param $idCliente
	 * @return void
	 */
	public function getTemplateTags($idCliente=null){
		if(empty($idCliente)){
			$idCliente = $_POST['idCliente'];
		}

		$data = $this->template->getTemplateTags($idCliente);

		echo json_encode($data);

		exit;

	}

	/**
	 * Recebe um array de arquivos e o tipo de objeto
	 * e verifica se ja existem no xinet, e se estão em uso em fichas,
	 * retornando um array de arquivos existentes e as fichas que eles afetam
	 *
	 * {"files":["DSC01446 copy.jpg","arquivo.jpg"],"fichas":[10,12,34]}
	 *
	 * @author juliano.polito
	 * @return string um json contendo quais arquivos existem, e em quais fichas estao sendo usados
	 */
	public function verificaArquivosExistentes(){
		//Recebe do flash os arquivos que vao subir
		$files = $_POST['files'];
		//o tipo escolhido pra esses arquivos - paz parte do path
		$tipo = $_POST['tipo'];
		//o cliente selecionado - faz parte do path
		$cliente = $_POST['cliente'];
		//obtem o path do tipo
		$pathTipo = $this->tipo_objeto->getPath($tipo);
		//obtem o path do cliente - pasta de nivel acima do tipo
		$pathCli = $this->cliente->getPath($cliente);

		//array com os arquivos encontrados
		$found = array();
		$foundCli = array();
		$foundFichas = array();

		foreach ($files as $file) {		
			//busca o arquivo na mesma pasta selecionada
			$xFileID = $this->objeto->getByPathLike($pathTipo,removeCaracteresEspeciaisNomeArquivo($file));
			
			if(!empty($xFileID)){
				//guarda o nome no array found para devolver para o flash
				$found[] = $file;
				
				//se existe na mesma pasta, vamos sobrescrever o arquivo
				//portanto antes de sobrescrever verificamos se alguma ficha ja utiliza este mesmo objeto
				//recupera fichas que usam o objeto destino
				$objs = $this->objeto_ficha->getByFileId($xFileID[0]['FILE_ID']);
				//caso encontre alguma ficha usando objeto
				if(count($objs) > 0){
					//echo "achou fichas: ".print_r($obj,true).PHP_EOL;
					foreach ($objs as $obj) {
						//monta o array com as fichas que o utilizam e devolve para o flash
						$fichaObj = $this->ficha->getById($obj['ID_FICHA']);
						$foundFichas[$fichaObj['COD_BARRAS_FICHA']] = $fichaObj['COD_BARRAS_FICHA'];
					}
				}
			}else{
				//caso nao exista na mesma pasta, buscamos se existe em alguma outra pasta do cliente
				$xFileCliID = $this->objeto->getByPathLike($pathCli,$file);
				if(count($xFileCliID) > 0){
					//caso exista em outra pasta, montamos o array com as informacoes e devolvemos para o flash
					foreach ($xFileCliID as $cliObj) {
						$fcli = array();
						$fcli['file'] = $file;
						$fcli['tipo'] = $cliObj['DESC_TIPO_OBJETO'];
						$foundCli[] = $fcli;
					}
				}
			}

		}

		$_SESSION['arquivosExistentes'] = array('files'=>$found, 'fichas'=>array_keys($foundFichas), 'files_duplicados'=>$foundCli);
		//$this->session->set_userdata('arquivosExistentes', array('files'=>$found, 'fichas'=>array_keys($foundFichas), 'files_duplicados'=>$foundCli));
		echo json_encode(array('files'=>$found, 'fichas'=>array_keys($foundFichas), 'files_duplicados'=>$foundCli));
		exit;
	}

	/**
	 * Recupera os grupos relacionados a uma agencia
	 *
	 * Dados enviados via JSON
	 *
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function getGruposByAgencia(){
		$idagencia = post('id',true);
		$grupos = $this->grupo->getByAgencia($idagencia);

		echo json_encode($grupos);
		exit;
	}

	/**
	 * Recupera os grupos relacionados a um cliente
	 *
	 * Dados enviados via JSON
	 *
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function getGruposByCliente(){
		$idcliente = post('id',true);
		$grupos = $this->grupo->getByCliente($idcliente);

		echo json_encode($grupos);
		exit;
	}

	/**
	 * recupera as regioes de um cliente
	 *
	 * envia os valores para a saida padrao com a notacao JSON
	 *
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function getRegiaoByCliente(){
		$id = post('id', true);
		$regioes = $this->regiao->getByCliente($id);

		echo json_encode($regioes);
	}
	
	/**
	 * recupera os calendarios de um cliente
	 *
	 * envia os valores para a saida padrao com a notacao JSON
	 *
	 * @author Sidnei Tertuliano Junior
	 * @return void
	 */
	public function getCalendarioByCliente(){
		$id = post('id', true);
		$calendarios = $this->calendario->getByCliente($id);

		echo json_encode($calendarios);
	}

	/**
	 * recupera as regioes de um produto
	 *
	 * envia os valores para a saida padrao com a notacao JSON
	 *
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function getRegiaoByProduto(){
		$id = post('id', true);
		$regioes = $this->regiao->getByProduto($id);

		echo json_encode($regioes);
	}

	/**
	 * Carrega os usuarios de um grupo para regras de email
	 *
	 * Carregamento feito via ajax. O HTML vai para a saida padrao
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idgrupo Grupo
	 * @param int $idregra Regra de email
	 * @return void
	 */
	public function loadGrupoRegra($idgrupo, $idregra = null){

		$grupo = $this->grupo->getById($idgrupo);
		$lista = $this->regraUsuario->getUsuariosGrupoRegra($idgrupo, $idregra);

		$this->assign('grupo', $grupo);
		$this->assign('usuarios', $lista);
		$this->display('regra_email/grupos');
	}
	
	/**
	 * Carrega os usuarios de um grupo para regras de email dos processos
	 *
	 * Carregamento feito via ajax. O HTML vai para a saida padrao
	 *
	 * @author Esdras Eduardo
	 * @param int $idgrupo Grupo
	 * @param int $idregra Regra de email
	 * @return void
	 */
	public function loadGrupoProcessoRegra($idEtapa, $idGrupo, $idRegra){

		$grupo = $this->grupo->getById($idGrupo);
		$lista = $this->regraEmailProcessoEtapaUsuario->getUsuariosGrupoRegra($idEtapa, $idGrupo, $idRegra);

		$this->assign('grupo', $grupo);
		$this->assign('usuarios', $lista);
		$this->assign('id_regra', $idRegra);
		$this->display('processos/grupos');
	}

	/**
	 * Recupera as agencias pelo codigo do cliente
	 *
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function getAgenciasByCliente(){
		$id = post('id', true);
		$dados = $this->agencia->getByCliente($id);

		echo json_encode($dados);
	}

	/**
	 * Recupera o historico de alteracoes de uma campanha
	 *
	 * Escreve na saida padrao o html contendo o historico de alteracoes
	 * relacionadas a uma campanha
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idcampanha Codigo da campanha para pegar o historico
	 * @return void
	 */
	public function getHistoricoCampanha($idcampanha=null){
		$idcampanha = sprintf('%d', $idcampanha);

		$this->assign('lista', $this->campanha_historico->getAlteracoes($idcampanha));
		$this->display('campanha/alteracoes');
	}

	/**
	 * Recupera os tipos de pecas ativos pelo codigo do cliente
	 *
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function getTipoPecaCliente(){
		$idcliente = post('id',true);

		echo json_encode( $this->tipo_peca->getTiposAtivos($idcliente) );
	}
	
	/**
	 * Salvar as regras de email
	 * 
	 * @author esdras.filho
	 */
	public function saveRegrasProcesso( $idProcessoEtapa ){
		$this->regraEmailProcessoEtapaUsuario->removeAll($idProcessoEtapa);
		if( isset( $_POST['usuarios'] ) ){
			$usuarios = $_POST['usuarios'];
			$horas = $_POST['qtaHoraEnvio'];
			foreach( $usuarios as $idRegraEmail => $users ){
				foreach( $users as $idUsuario ){
					$this->regraEmailProcessoEtapaUsuario->saveUsuario($idRegraEmail, $idProcessoEtapa, $idUsuario, $horas[$idRegraEmail]);
				}
			}
		}
	}
	
	/**
	 * Excluir etapa do processo
	 * 
	 * @author esdras.filho
	 * @param $id
	 */
	public function excluirEtapa( $id ){
		$this->processoEtapa->delete($id);
	}
	
	/**
	 * Adicionar etapa do processo
	 * 
	 * @author esdras.filho
	 * @param $id
	 */
	public function addEtapa( $idProcesso, $idEtapa, $idStatus ){
		$item = Array();
		$item['ID_PROCESSO'] = $idProcesso;
		$item['ID_ETAPA'] = $idEtapa;
		$item['QTD_DIAS'] = 1;
		$item['QTD_HORAS'] = '';
		$item['QTD_DIAS_ETAPA'] = 1;
		$item['ID_STATUS'] = $idStatus;
		$item['ORDEM'] = $this->processoEtapa->getLastOrdem($idProcesso);
		$this->processoEtapa->save($item);
		echo $this->db->insert_id();
	}
	
	/**
	 * Devolve a data de inicio do job mediante a data inicio do processo
	 * @author Nelson Martucci
	 * @return unknown_type
	 */
	public function calculaDataInicio(){
		
		//array com as etapas do processo
		$arrayEtapas = array();
		$etapas = array();
		
		//array com o processo utilizado
		$arrayProcesso = array();
		
		//feriados utilizados neste processo
		$feriados = array();
		
		//Data Inicio do Processo
		$dataInicioProcesso = '';
		
		//Tipo de Peca
		$idTipoPeca = 0;
		
		//Produto
		$idProduto = 0;
		
		//Verifica se veio data inicio do processo
		if( !empty($_POST['date']) ) {
			$dataInicioProcesso = $_POST['date'];
		} else {	
			echo '['.json_encode(array('erro'=>'Data de Inicio do Processo não informada!', 'result'=>'')).']';
			return;
		}
		
		//Verifica se veio tipo de peca
		if( !empty($_POST['idTipoPeca']) ) {
			$idTipoPeca = $_POST['idTipoPeca'];
		} else {
			echo '['.json_encode(array('erro'=>'Informe o Tipo de Peça!', 'result'=>'')).']';
			return;
		}
		
		//Verifica se veio Bandeira para busca dos feriados
		if( !empty($_POST['idProduto']) ) {
			$idProduto = $_POST['idProduto'];
		} else {
			echo '['.json_encode(array('erro'=>'Informe a bandeira!', 'result'=>'')).']';
			return;
		}
		
		//Consiste se a data enviada eh valida
		if (!checkdate(	format_date($dataInicioProcesso,'m'),
						format_date($dataInicioProcesso,'d'),
						format_date($dataInicioProcesso,'Y'))) {
			echo '['.json_encode(array('erro'=>'Data de Inicio do Processo é inválida!', 'result'=>'')).']';
			return;
		} else {
			$diaInicioProcesso = format_date($dataInicioProcesso,'d');
			$mesInicioProcesso = format_date($dataInicioProcesso,'m');
			$anoInicioProcesso = format_date($dataInicioProcesso,'Y');
		}
		
		//Verifica se a data inicio do Processo eh menor que a data atual
		if( !empty($_POST['compararDataAtual']) && $_POST['compararDataAtual'] == 1) {
			if ( mktime(0, 0, 0, $mesInicioProcesso, $diaInicioProcesso, $anoInicioProcesso) < mktime( 0, 0, 0, date('m'), date('d'), date('Y')) ) {
				echo '['.json_encode(array('erro'=>'A data de inicio do processo job não pode ser menor que a data atual!', 'result'=>'')).']';
				return;
			}
		}
		
		//Busca Processo pelo id Tipo de Peca
		$arrayProcesso = $this->processo->getByTipoPeca($idTipoPeca);
		
		//Se não encontrou processo para aquele tipo de peca
		if (empty($arrayProcesso)) {
			echo '['.json_encode(array('erro'=>'Não há processo cadastrado para o tipo de peca selecionado', 'result'=>'')).']';
			return;
		}
		
		//Faz a consulta das etapas do processo no banco 
		$arrayEtapas = $this->db
			->select('EP.ID_PROCESSO_ETAPA, EP.ID_ETAPA, EP.QTD_HORAS, EP.ORDEM')
			->select('P.HORA_INICIO, P.HORA_FINAL, P.CONSIDERAR_DIAS_UTEIS, P.CONSIDERAR_FERIADO')
			->select('P.*')
			->select('E.DESC_ETAPA')
			->from('TB_PROCESSO P')
			->where('P.ID_PROCESSO', $arrayProcesso['ID_PROCESSO'])
			->join('TB_PROCESSO_ETAPA EP','EP.ID_PROCESSO = P.ID_PROCESSO')
			->join('TB_ETAPA E','E.ID_ETAPA = EP.ID_ETAPA')
			
			->order_by('EP.ORDEM')
			->get();
			
			
		// se encontrou etapas
		if( $arrayEtapas->num_rows() > 0 ){
			
			//array para tratamento dos dados retornados
			$etapas = $arrayEtapas->result_array();
			
			//Hora Inicio
			$horaInicial = $etapas[0]['HORA_INICIO'];
			
			//Hora final
			$horaFinal = $etapas[0]['HORA_FINAL'];
			
			//Considerar Somente dias uteis ?
			$somenteDiasUteis = $etapas[0]['CONSIDERAR_DIAS_UTEIS'];
			
			//Considerar feriados ?
			$considerarFeriados = $etapas[0]['CONSIDERAR_FERIADO'];
			
			//Verifica se a data informada eh util para o processo 
			if ( strtoupper($somenteDiasUteis) == 'S' ) {
				if ( strftime('%w', mktime( 0, 0, 0, $mesInicioProcesso, $diaInicioProcesso, $anoInicioProcesso ) ) == 6	||	//Sabado
					 strftime('%w', mktime( 0, 0, 0, $mesInicioProcesso, $diaInicioProcesso, $anoInicioProcesso ) ) == 0 ) {	//Domingo
					//Dia nao util
					echo '['.json_encode(array('erro'=>'A data de ínicio do processo não é util!', 'result'=>'')).']';
					return;
				}
			}
			
			//Verifica se teremos que considerar os feriados como dia de trabalho
			if ( strtoupper($considerarFeriados) == 'N' ) {
				$feriados = $this->calendario->getFeriadosByProduto($idProduto);
				if (in_array( mktime( 0, 0, 0, $mesInicioProcesso, $diaInicioProcesso, $anoInicioProcesso ), $feriados) ) {
					//Feriado
					echo '['.json_encode(array('erro'=>'A data de ínicio do processo é feriado!', 'result'=>'')).']';
					return;
				}
			}
			
			//Formata Data de Inicio do Processo para chamar rotina de calculo de horas uteis
			$horasMinutosPeriodoUtilInicial = explode(':', $horaInicial );
			
			//Acumulador de data/hora das etapas do processo/job
			//Inicia com a data inicio do processo
			$dataAcumulada = mktime( 	$horasMinutosPeriodoUtilInicial[0], 
										$horasMinutosPeriodoUtilInicial[1], 
										0, 
										$mesInicioProcesso,
										$diaInicioProcesso,
										$anoInicioProcesso );
			
			//Para cada Etapa
			foreach ($etapas as $etapa) {
				//Calcula Data Final de cada Etapa
				$dataAcumulada = horasUteis(	$dataAcumulada, 
												$etapa['QTD_HORAS'], 
												$horaInicial,
												$horaFinal,
												$somenteDiasUteis,
												$considerarFeriados,
												$feriados );
															
			}
		} else {
			echo '['.json_encode(array('erro'=>'Não há etapas cadastradas para este processo', 'result'=>$idTipoPeca)).']';
			return;
		}
		
		//Somamos 1 minuto na data/hora acumulada para que este calcule o proximo dia util e em seguida subtraimos esse 1 minuto
		$dataAcumulada = horasUteis(	$dataAcumulada, 
										'0:01', 
										$horaInicial,
										$horaFinal,
										$somenteDiasUteis,
										$considerarFeriados,
										$feriados );

		//Subtraimos 1 minutos em segundos.
		$dataAcumulada -= 60;
										
		echo '['.json_encode(array('erro'=>'', 'result'=>date('d/m/Y',$dataAcumulada))).']';
		return;
	}

	/**
	 * Verifica se a data inicio do processo estah coerente com a Etapa escolhida
	 * @author Nelson Martucci
	 * @return unknown_type
	 */
	public function verificaDataInicioProcessoXEtapa( $idJob=0 ) {
		
		//Array com as etapas do job baseado na nova data de inicio do processo
		$etapasJob = array();
		
		//Nova Data inicio do processo
		$dataInicioProcesso = '';
		$diaInicioProcesso = 0;
		$mesInicioProcesso = 0;
		$anoInicioProcesso = 0;
		
		//Etapa escolhida
		$etapaProcessoSelecionada = 0;
		
		//Etapa que o job deve estar
		$novaEtapaProcessoJob = 0;
		
		//Verifica se veio data inicio do processo
		if( !empty($_POST['DATA_INICIO_PROCESSO']) ) {
			$dataInicioProcesso = format_date_to_db($_POST['DATA_INICIO_PROCESSO'],2);
		} else {	
			echo '['.json_encode(array('erro'=>'Data de Inicio do Processo não informada!')).']';
			return;
		}
		
		//Verifica se veio a etapa
		if( !empty($_POST['ID_PROCESSO_ETAPA']) ) {
			$etapaProcessoSelecionada = $_POST['ID_PROCESSO_ETAPA'];
		} else {	
			echo '['.json_encode(array('erro'=>'A etapa não foi informada!')).']';
			return;
		}
		
		//Verifica se veio o idJob
		if( $idJob <= 0 ) {
			echo '['.json_encode(array('erro'=>'Numero do job não informado!')).']';
			return;
		}
		
		//Consiste se a data enviada eh valida
		if (!checkdate(	format_date($dataInicioProcesso,'m'),
						format_date($dataInicioProcesso,'d'),
						format_date($dataInicioProcesso,'Y'))) {
			echo '['.json_encode(array('erro'=>'Data de Inicio do Processo é inválida!')).']';
			return;
		} else {
			$diaInicioProcesso = format_date($dataInicioProcesso,'d');
			$mesInicioProcesso = format_date($dataInicioProcesso,'m');
			$anoInicioProcesso = format_date($dataInicioProcesso,'Y');
		}
		
			
		//Calcula as etapas do job com a data inicio do processo informada
		$etapasJob = $this->job->getDataEtapasJob( $idJob, $dataInicioProcesso );
		
		//Baseado na data inicio do processo informada, verifica em qual etapa o job deve estar. Compara com a etapa escolhida
		if (!empty($etapasJob) && is_array($etapasJob)) {

			//Coloca a primeira etapa como sendo a etapa correta
			$novaEtapaProcessoJob = $etapasJob['ETAPA'][0]['ID_PROCESSO_ETAPA'];
			
			//Percorre as etapas
			foreach ($etapasJob['ETAPA'] as $etapa) {
				
				//A etapa do job deve ser aquela em que a data inicio da etapa estah mais proxima da data atual
				//Sai do loop quando a data/hora inicio da etapa ultrapassar a data atual
				 if ( time() <= $etapa['DATA_INICIO_ETAPA'] ) {
				 	break;
				 }
				 $novaEtapaProcessoJob = $etapa['ID_PROCESSO_ETAPA'];
			}
		}
		
		//Verifica se a nova etapa (etapa ideal) estah coerente com a etapa escolhida na tela. 
		if ($etapaProcessoSelecionada != $novaEtapaProcessoJob) {
			
			//Busca Nome das etapas para visualizacao
			$processoEtapa = $this->processoEtapa->getById($etapaProcessoSelecionada);
			$etapa = $this->etapa->getById($processoEtapa['ID_ETAPA']);
			$nomeEtapaSelecionada = $etapa['DESC_ETAPA'];
			
			//Busca Nome das etapas para visualizacao
			$processoEtapa = $this->processoEtapa->getById($novaEtapaProcessoJob);
			$etapa = $this->etapa->getById($processoEtapa['ID_ETAPA']);
			$nomeNovaEtapaProcessoJob = $etapa['DESC_ETAPA'];
			
			echo '['.json_encode(array('erro'=>'A etapa escolhida não está coerente com a etapa que o job deve estar de acordo com o processo utilizado! Você selecionou ' . 
												$nomeEtapaSelecionada . ' e a etapa ideal é ' . $nomeNovaEtapaProcessoJob . 
												'. Deseja Continuar?' )).']';
			return;
			
		}
		
		//Deu tudo certo
		echo '['.json_encode(array('erro'=>'ok')).']';
		return;
	}
	
	/**
	 * Recupera as categorias de um cliente
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function getCatSubByCliente(){
		
		$id = empty($_POST['id']) ? 0 : $_POST['id'];

		// agora vamos pegar as categorias e subcategorias
		$lista = $this->categoria->getByCliente($id);

		// para cada categorias
		foreach($lista as $idx => $item){
			$lista[ $idx ]['subcategorias'] = $this->subcategoria->getByCategoria($item['ID_CATEGORIA']);
		}
		
		echo json_encode($lista);
		exit;
	}
	
	/**
	 *
	 * @author Sidnei Tertuliano Junior
	 * @return usuarios
	 */
	public function getUsuariosByCliente(){
		$id = sprintf('%d', post('id',true));
		
		$rs = $this->usuario->getByCliente($id);
		echo json_encode($rs);
		
		exit;
	}
	
	/**
	 * recupera as pracas de um cliente
	 * @author Sidnei Tertuliano Junior
	 * @return void
	 */
	public function getPracasByCliente(){
		$idCliente = post('id', true);
		$lista = $this->praca->getByCliente($idCliente);

		echo json_encode($lista);
		exit;
	}
	
	/**
	 * recupera as pracas pelo tipo de peca
	 * @author Sidnei Tertuliano Junior
	 * @return void
	 */
	public function getPracasByTipoPeca(){
		$idTipoPeca = post('id', true);
		$lista = $this->tipoPecaPraca->getByTipoPeca($idTipoPeca);
		echo json_encode($lista);
		exit;
	}
	
	/**
	 * recupera as pracas de uma regiao Selecionando as que ja estiverem cadastradas no tipo de peca
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function getPracasByRegiaoTipoPecaSelecionada(){
		$idregiao = post('id',true);
		$idTipoPeca = post('idtipopeca',true);
		
		$lista = $this->praca->getByRegiao($idregiao);
		
		$pracasSelecionadas = array();		
		
		// pega as pracas faz o for para ver se ja existe pracas selecionadas
		foreach($lista as $p){
			$tipoPecaPraca = $this->tipoPecaPraca->getByTipoPecaPraca($idTipoPeca, $p['ID_PRACA']);

			if(count($tipoPecaPraca) > 0){
				$p['SELECIONADO'] = 1;
			}
			else{
				$p['SELECIONADO'] = 0;
			}
			
			$pracasSelecionadas[] = $p;
		}

		echo json_encode($pracasSelecionadas);
		exit;
	}
	
	/**
	 * Verifica se existe algum usuário atribuido ao grupo antes de inativar
	 * @author Bruno Oliveira
	 * @return void
	 */
	public function getUsuarioByGrupo($idGrupo,$status){
		
		$usuarios = $this->usuario->getByGrupo($idGrupo);
		
		if((is_array($usuarios) && (empty($usuarios))) || ($status == '1')){
			echo json_encode(1);
			exit;
		} else{
			echo json_encode(2);
			exit;
		}
	}
	
	/**
	 * Retornar as praças vinculadas ao job
	 * @author Bruno Oliveira
	 * @return void
	 */
	public function getPracasByIdJob(){
		$id = post('id', true);
		
		$pracas = $this->praca->getByIdJob($id);
	
		echo json_encode($pracas);
		exit;
	}
	
}
