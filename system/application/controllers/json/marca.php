<?php
class Marca extends MY_Controller
{
	function getAllByFabricante()
	{		
		$json = getJson();
											
		$response['veiculos'] = $this->praca_veiculo->getAllByMidiaAndPraca($json->id_midia,$json->id_praca);
		$response['obj'] = $json->obj;
				
		setJson($response);
	}
	
	function getByDescFabricante($desc, $idFabricante)
	{		
		$marca = $this->marca->getByDescFabricante($desc, $idFabricante);

		setJson($marca);
	}
		
}
?>
