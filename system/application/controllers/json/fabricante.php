<?php
class Fabricante extends MY_Controller
{	
	function getAll()
	{
		$fabricantes = $this->fabricante->getAll(null,null,'DESC_FABRICANTE');
		
		$response['fabricantes'] = 	$fabricantes['data'];			
		
		setJson($response);
	}
	function getAllMarca()
	{		
		 $json = getJson();
									
		$response['data'] = $this->marca->getAllByFabricante($json->id);						
		$response['combo'] = $json->combo;
		$response['value'] = $json->value;
		
		setJson($response);			
	}	
	
	/**
	 * Recupera as marcas de um fabricante
	 * 
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function getMarcas(){
		$fid = post('id', true);
		
		$lista = $this->marca->getByFabricante($fid);
		echo json_encode($lista);
	}
}
?>