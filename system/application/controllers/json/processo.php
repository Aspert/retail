<?php

require_once APPPATH . 'libraries/graficos/GraficoProcesso.php';

/**
 * JSON para o controle de processos
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 */
class Processo extends MY_Controller {

	/**
	 * tipo cronograma programado
	 *
	 * @var string
	 */
	const TIPO_PLANEJADO = 'planejado';
	/**
	 * tipo de cronograma atual
	 *
	 * @var string
	 */
	const TIPO_ATUAL     = 'atual';

	/**
	 * Contrutor
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Processo
	 */
	public function __construct(){
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/processos/';
	}

	/**
	 * Exibe o grafico de cronograma
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idjob
	 * @param string $tipo
	 * @param int $width
	 * @param int $height
	 * @param int $alturaEtapa
	 * @param int $larguraDia
	 * @return void
	 */
	public function getGraficoCronograma($idjob, $tipo = self::TIPO_PLANEJADO, $width = 830, $height = 350, $larguraDia = null, $alturaEtapa = null){

		$grafico = new GraficoProcesso($width, $height, $larguraDia, $alturaEtapa);

		if( $tipo == self::TIPO_PLANEJADO ){
			
			$list = $this->job->getDataEtapasJob($idjob);
			
			foreach($list['ETAPA'] as $item){
				$grafico->addEtapa(
					new GraficoProcessoEtapa(
						date('Y-m-d',$item['DATA_INICIO_ETAPA'])
						, date('Y-m-d',$item['DATA_FINAL_ETAPA'])
						, $item['COR_ETAPA']
						, $item['DESC_ETAPA']
						, $tipo
					)
				);
			}
			
		} else {

			$list = $this->job->getHistoricoEtapas($idjob);
			
			foreach($list as $item){
				$grafico->addEtapa(
					new GraficoProcessoEtapa(
						format_date($item['DATA_INICIO_ETAPA'])
						, format_date($item['DATA_FIM_ETAPA'])
						, $item['COR_ETAPA']
						, $item['PROXIMA_ETAPA']
						, $tipo
					)
				);
			}
		}

		$grafico->cronograma_gerar();
	}

	/**
	 * Mostra os detalhes do processo escolhido
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do processo
	 * @return void
	 */
	public function detalhes($id){

		$processo = $this->processo->getById($id);
		$etapas = $this->processoEtapa->getByProcesso($id);

		$this->assign('dados', $processo);
		$this->assign('etapas', $etapas);
		$this->display('detalhes');
	}
}

