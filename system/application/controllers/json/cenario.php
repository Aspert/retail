<?php
class Cenario extends MY_Controller
{	
	/**
	 * efetua uma pesquisa de objetos na tela de criacao/edicao de cenarios
	 * @author Sidnei Tertuliano Junior
	 * @return void
	 */
	public function pesquisarObjetos(){
		
		$pagina = (isset($_POST['pagina'])) ? $_POST['pagina'] : 0;
		$user = Sessao::get('usuario');
		
		$path = $this->config->item('caminho_storage');

		$resposta['ID_CLIENTE'] = post('ID_CLIENTE', true);
		$resposta['ID_AGENCIA'] = post('ID_AGENCIA', true);
		
		if(!empty($user['ID_AGENCIA'])){
			$resposta['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}
		
		if(!empty($user['ID_CLIENTE'])){
			$resposta['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}
		
		$resposta['PERIODO_INICIAL'] = post('PERIODO_INICIAL',true);
		$resposta['PERIODO_FINAL'] = post('PERIODO_FINAL',true);
		$resposta['FILENAME'] = post('NOME',true);
		$resposta['ORIGEM'] = post('ORIGEM',true);
		$resposta['CODIGO'] = post('CODIGO',true);
		$resposta['ID_CATEGORIA'] = post('CATEGORIA',true);
		$resposta['ID_SUBCATEGORIA'] = post('SUBCATEGORIA',true);
		$resposta['ID_TIPO_OBJETO'] = post('ID_TIPO_OBJETO',true);
		$resposta['ID_FABRICANTE'] = post('ID_FABRICANTE',true);
		$resposta['ID_MARCA'] = post('ID_MARCA',true);
		$resposta['KEYWORDS'] = post('KEYWORDS',true);
		
		$limit = 5;
		
		$objetos = $this->objeto->listItems($resposta, $pagina, $limit);
		
		$response['total']= $objetos['total'];
		$response['objetos']= $objetos['data'];
		$response['limit'] = $limit;
		$response['_sessao'] = Sessao::get('usuario');
		$response['pagina'] = $pagina;
		$response['totalPagina'] = $objetos['nPaginas'];
		
		$this->load->view('ROOT/cenario/resultado_pesquisa', $response);
	}
	
	/**
	 * efetua uma pesquisa de fichas na tela de criacao/edicao de cenarios
	 * @author Sidnei Tertuliano Junior
	 * @return void
	 */
	public function search_fichas($pagina=0){
		$busca = &$_POST;
		$this->setSessionData($busca);
			
		unset($busca['ID_USUARIO']);
		
		$busca['FLAG_ACTIVE_FICHA'] = 1;
		$busca['ID_APROVACAO_FICHA'] = 1;
		$busca['FLAG_TEMPORARIO'] = 0;
		
		$limit = 10;
		
		$busca['CATEGORIAS'] = getCategoriasUsuario($busca['ID_CLIENTE']);
			
		$fichas = $this->ficha->listItems($busca, $pagina*$limit, $limit);

		// adciona campos e contudos a ficha
		for ( $i = 0; $i <= count($fichas['data']) -1; $i++ ) {
			$fichas['data'][$i]['CAMPOS'] = array();
			$lista = $this->campo_ficha->getAllCampoFicha( $fichas['data'][$i]['ID_FICHA'] );
			if ( !empty($lista) ) {
				foreach ( $lista as $item ) {
					$fichas['data'][$i]['CAMPOS'][$item['LABEL_CAMPO_FICHA']] = $item['VALOR_CAMPO_FICHA'];
				}
			}
		}

		$fichasCombo = $this->ficha->listCombosIn(getValoresChave($fichas['data'], 'ID_FICHA'), true);
		
		// adciona campos e contudos a fichas combom
		for($i = sizeof($fichasCombo['data']) - 1; $i >= 0; $i--){
			$fichasCombo['data'][$i]['CAMPOS'] = array();
			$fichasCombo['data'][$i]['DT_INSERT_FICHA'] = format_date_to_form( $fichasCombo['data'][$i]['DT_INSERT_FICHA'], 'd/m/Y H:i' );
			$lista = $this->campo_ficha->getAllCampoFicha( $fichasCombo['data'][$i]['ID_FICHA'] );
			if ( !empty($lista) ) {
				foreach ( $lista as $item ) {
					$fichasCombo['data'][$i]['CAMPOS'][$item['LABEL_CAMPO_FICHA']] = $item['VALOR_CAMPO_FICHA'];
				}
			}
			if($fichasCombo['data'][$i]['CHAVE'] != 'aprovado'){
				unset($fichasCombo['data'][$i]);
			}
		}
		
		//pega as fichas filhas e seus objetos
		for($i = 0; $i <= sizeof($fichas['data']) - 1; $i++){
			$fichas['data'][$i]['DT_INSERT_FICHA'] = format_date_to_form( $fichas['data'][$i]['DT_INSERT_FICHA'], 'd/m/Y H:i' );
			$fichas['data'][$i]['fichasFilhas'] = $this->ficha->getFichasFilha( $fichas['data'][$i]['ID_FICHA'] );

			// adciona campos e contudos a fichas filhas	
			if( count($fichas['data'][$i]['fichasFilhas']) > 0 ){
				for ( $y = 0; $y <= count( $fichas['data'][$i]['fichasFilhas'] ) -1; $y++ ) {
					$fichas['data'][$i]['fichasFilhas'][$y]['CAMPOS'] = array();
					$lista = $this->campo_ficha->getAllCampoFicha( $fichas['data'][$i]['fichasFilhas'][$y]['ID_FICHA'] );

					if ( !empty($lista) ) {
						foreach ( $lista as $item ) {
							$fichas['data'][$i]['fichasFilhas'][$y]['CAMPOS'][$item['LABEL_CAMPO_FICHA']] = $item['VALOR_CAMPO_FICHA'];
						}
					}
				}
			}
		}		

		$this->assign('pagina', $pagina);
		$this->assign('lista', $fichas);
		$this->assign('fichasCombo', $fichasCombo);
		$this->assign('paginacao', linkpagination($fichas, $limit));
		
		$this->load->view('ROOT/cenario/resultado_pesquisa_fichas', $this->data);
	}
	
	public function getCor(){
		$idCenario   = post('idCenario', true);
		$cenario = $this->cenario->getById( $idCenario );
		if( $cenario['STATUS_CENARIO'] == 1 ){
			echo $this->cenario->pegarCor($idCenario);
		}
		else{
			echo '';
		}
	}
}





























































