<?php
class Objeto extends MY_Controller 
{
	function Objeto()
	{
		parent::Controller();
		$this->load->library('DBXinet');
		
	}
	function pesquisar()
	{
		$json = getJson();
		
		$pagina = (isset($json->pagina)) ?$json->pagina :0;
		
		$dados['NOME'] = $json->NOME_OBJETO;
		$dados['CODIGO'] = $json->COD_OBJETO;
		$dados['MARCA'] = $json->MARCA_OBJETO;
		$dados['PERIODO_INICIAL'] = $json->PERIODO_INICIAL;
		$dados['PERIODO_FINAL'] = $json->PERIODO_FINAL;
		$dados['TIPO'] = $json->TIPO;
		$dados['CATEGORIA'] = $json->_ID_CATEGORIA;
		$dados['SUBCATEGORIA'] = $json->_ID_SUBCATEGORIA;
		$dados['KEYWORDS'] = $json->KEYWORD_OBJETO;
		

		$limit=5;


		$objetos = $this->general->getByLike($dados, $pagina, $limit); 
		
			
		
		$data['objetos'] = $objetos['data'];
		$data['total'] = $objetos['total'];
		$data['totalPagina'] = $objetos['nPaginas'];
		$data['pagina'] = $pagina;		
		
		setJson($data);
	}
	
	public function verificaFichas(){
		
		$file = post('file',true);
		$tipo = post('tipo',true);
		$fileID = post('fileid',true);
		
		//array com as fichas encontradas
		$foundFichas = array();
		
		$foundFichas = $this->objeto_ficha->getByFileId($fileID);
		
		
		echo json_encode($foundFichas);
		
		exit;
	}
	
	
}
?>
