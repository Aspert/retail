<?php
class Carrinho extends MY_Controller
{
	function Carrinho()
	{
		parent::Controller();
	}
	function getLogByCarrinho()
	{
		$json = getJson();		
		
		$response['historico'] = $this->log->getByModuleAndIdRegistro($json->modulo,$json->id_carrinho);

		$status 				 = $this->status->getAll();
		$response['status']      = $status['data'];
		$response['id_carrinho'] = $json->id_carrinho; 	
		$response['modulo']      = $json->modulo;
		$response['mode']      = $json->mode;
		$response['sel_status']      = $json->id_status;	
		setJson($response);		
		
	}
	
	function gethistorico()
	{
		$json = getJson();
		$response['historicos'] = $this->log->getByIdCarrinho($json->carrinho);
		$response['ok'] = 'ok';
		setJson($response);
		
	}
	
	function getById()
	{		
		$ID_CARRINHO = getJson();
		
		$response['carrinho'] = $this->carrinho->getById($ID_CARRINHO->ID_CARRINHO);		
		$response['fichas'] = $this->ficha->getFichaByCarrinho($ID_CARRINHO->ID_CARRINHO);
		
		setJson($response);		
	}
	
	function saveHistory()
	{
		$json = getJson();
		
		// mudando o status do campo FLAG_APROVACAO
		$this->carrinho->setFiltro($json->id_carrinho,$json->status_log,$json->mode);
		
		// Setando automaticamente o status do campo FLAG_APROVACAO_PRESIDENCIA
		$this->carrinho->setFiltro($json->id_carrinho,$json->status_log,'PRESIDENCIA');
		
		// Pegando os dados do carrinho, para ter o ID_PAGINACAO
		$carrinho = $this->carrinho->getById($json->id_carrinho);
		
		// Setando automaticamente o status do campo ID_SITUACAO_PAGINACAO
		$this->paginacao->setSituacaoPaginacao($carrinho['ID_PAGINACAO'],$json->status_log);
		
		//pegar o id do modulo
		$modulo = $this->module->getByName($json->modulo);		
		
		// pegar a descricao do status pelo id
		$status = $this->status->getById($json->status_log);		
		
		$log = array(
					'ID_USER'=>$this->session->userdata[USUARIO]['ID_USER'],
					'ID_REGISTRO_LOG'=>$json->id_carrinho,
					'ID_MODULE'=>$modulo['ID_MODULE'],
					'STATUS_LOG'=>$status['DESC_STATUS'],
					'DESC_LOG'=>$json->desc_log
				);
				
		file_put_contents("log.txt",$log);
		
		$this->log->save($log,null);
		
		$response['status'] = 'ok'; 		
		setJson($response);				
	}
	
	/**
	 * Funcao para poder gravar a solicitacao de aprovacao/reprovacao
	 * Recupera os dados vindos de um Ajax, no formato JSON.
	 * Adaptado para substituir a funcao feita pela Savoir (Carrinho::saveHistory)
	 * 
	 * @author Hugo Silva
	 * @return void
	 */
	function salvarAprovacao()
	{
		$json = getJson();
		
		// Pegando os dados do carrinho, para ter o ID_PAGINACAO
		$carrinho = $this->carrinho->getById($json->id_carrinho);
		
		// pegar a descricao do status pelo id
		$status = $this->status->getById($json->status_log);
		
		// Setando automaticamente o status do campo ID_SITUACAO_PAGINACAO
		$this->paginacao->setStatus($carrinho['ID_PAGINACAO'],$json->status_log);
		
		//pegar o id do modulo
		$modulo = $this->module->getByName($json->modulo);
		
		// se foi enviada a etapa
		if(!empty($json->etapa)){
			// alteramos a etapa da paginacao
			$this->paginacao->setEtapa($carrinho['ID_PAGINACAO'], $json->etapa);
		}
		
		// se esta aprovando
		if($status['CHAVE_STATUS']=='APROVADO'){
			// passa para a etapa de paginacao
			$this->paginacao->setEtapa($carrinho['ID_PAGINACAO'],$this->etapa->getIdByKey('PAGINACAO'));
		}
		
		$log = array(
					'ID_USER'=>$this->session->userdata[USUARIO]['ID_USER'],
					'ID_REGISTRO_LOG'=>$json->id_carrinho,
					'ID_MODULE'=>$modulo['ID_MODULE'],
					'STATUS_LOG'=>$status['DESC_STATUS'],
					'DESC_LOG'=>$json->desc_log
				);
				
		// file_put_contents("log.txt",$log);
		
		$this->log->save($log,null);
		
		$response['status'] = 'ok'; 		
		setJson($response);				
	}

	/**
	 * Exibe o conteudo da janela popup para aprovacao
	 * Feito para substituir a funcao da Savoir de fazer o html atraves 
	 * de um JavaScript
	 * 
	 * @author Hugo Silva
	 * @return void
	 */
	function popupAprovacao(){
		if(!empty($_POST['id_carrinho']) && !empty($_POST['tipo_aprovacao'])){
			$list = $this->log->getByModuleAndIdRegistro('checklist', $_POST['id_carrinho']);
			$status = $this->status->getByKey($_POST['tipo_aprovacao']);
			
			$this->data['status'] = $status;
			$this->data['historico'] = $list;
			$this->data['id_carrinho'] = $_POST['id_carrinho'];
			
			// se esta reprovando/alterando
			if(in_array($_POST['tipo_aprovacao'], array('REPROVADO','EM_ALTERACAO'))){
				// carrega as etapas
				$list = $this->etapa->getAll();
				$this->data['etapas'] = $list['data'];
			}
			
			$this->load->view('ROOT/checklist/popup_aprovacao',$this->data);
		} else {
			die('Ops, deu alguma coisa errada');
		}
	}
	
	/**
	 * metodo chamado para aprovar uma paginacao
	 * @author Hugo Silva
	 * @return void
	 */
	function aprovar(){
		$json = getJson();
		
		$pg = $this->paginacao->getByIdCarrinho($json->id_carrinho);
		$this->paginacao->setEtapa($pg['ID_PAGINACAO'], $this->etapa->getIdByKey('PAGINACAO'));
		
		$log = array(
					'ID_USER'=>$this->session->userdata[USUARIO]['ID_USER'],
					'ID_REGISTRO_LOG'=>$json->id_carrinho,
					'ID_MODULE'=>$modulo['ID_MODULE'],
					'STATUS_LOG'=>$status['DESC_STATUS'],
					'DESC_LOG'=>$json->desc_log
				);
				
		//file_put_contents("log.txt",$log);
		
		$this->log->save($log,null);
		
		setJson(array('status'=>'ok'));
	}
}
?>