	<?php
class Ficha extends MY_Controller
{
	/*
	 * Metodo utilizado para ver detalhes da ficha.
	 */
	function getByIdAndObjeto()
	{		
		$json = getJson();
		
		/*
		 * Ficha selecionada
		 */
		$ficha  = $this->ficha->getById($json->ID_FICHA);
		$ficha['DT_INSERT_FICHA'] = format_date_to_form($ficha['DT_INSERT_FICHA']);
		$response['ficha'] =	$ficha;
			
		/*
		 * Objetos da ficha selecionada
		 */
		$response['objetos'] = $this->obj_ficha->getAllByFicha($json->ID_FICHA);
		 
		setJson($response);
	}
	function getById()
	{
		$json = getJson();
		
		$response['ficha'] = $this->ficha->getById($json->ID_FICHA); 
		
		setJson($response);
			
	}
	function getBySearch()
	{
		$json = getJson();
		
		foreach($json->fichas as $chave=>$valor)
		{
			$fichas[] = $this->ficha->getById($valor);
		}
		$response['fichas'] = $fichas;
		
		setJson($response);
			
	}
	function getByPromocao()
	{		
		$fichas_id = getJson();
	
		 $ficha= $this->ficha->getById($fichas_id->ID_FICHA);
		$response['ficha']=$ficha;
		$prom_ficha = $this->promocao->getByFicha($ficha['ID_FICHA_PROMOCAO']);

		if($prom_ficha!=null)
		{
			foreach($prom_ficha as $pro_ficha)
			{ 
				$fichas[]=$this->ficha->getById($pro_ficha['ID_FICHA_PRODUTO']);	
			}
					
		$response['fichas'] = $fichas;			
		}
	
		setJson($response);		
	}
	function getByObjeto()
	{		
		$json = getJson();
		
		$response['ficha'] = $this->ficha->getById($json->ID_FICHA);
		
		$objetos = $this->obj_ficha->getByFicha($json->ID_FICHA);
		
		if($objetos)
		{
			foreach ( $objetos as $key=>$o )
			{
				$objetos[$key]['FILE_OBJ_FICHA'] = utf8_encode($o['FILE_OBJ_FICHA']);
				$file = $this->general->getByFileNameAndPath($o['FILE_OBJ_FICHA'],utf8MAC($o['PATH_OBJ_FICHA']));
				$keywords = $this->general->getKeywordsByFileID($file['FileID']);
				$objetos[$key]['TIPO'] = $keywords['TIPO'];
			}
		}
		
		$response['objetos'] = $objetos;
		
		setJson($response);		
	}
	public function pesquisar()
	{
		$json = getJson();
		
		$resposta['ID_APROVACAO_FICHA'] = 1;
		
		
		#echo $json->ID_CARRINHO; exit;
		 
		$pagina = (isset($json->pagina)) ?$json->pagina :0;
		
		$resposta['tipo_pesquisa'] = 'completa';
		$resposta['TIPO_COMBO_FICHA'] = $json->TIPO_COMBO_FICHA;
		$resposta['NOME_FICHA'] = $json->NOME_FICHA;
		$resposta['COD_FICHA'] = $json->COD_FICHA;
		$resposta['ID_CATEGORIA'] = $json->ID_CATEGORIA;
		$resposta['ID_SUBCATEGORIA'] = $json->_ID_SUBCATEGORIA;		
		//$resposta['FLAG_SITUACAO_FICHA'] = $json->FLAG_SITUACAO_FICHA;				
		$resposta['MARCA_FICHA'] = null;	
		$resposta['FABRICANTE_FICHA']= null;
		
		if ( isset($resposta['COD_FICHA'] ) )
		{
			$dados['ficha'] = $resposta;
			$this->session->set_userdata('BUSCA', $dados);			
		}
		
		$limit = 5;
		
		if(isset($json->ID_CARRINHO))
		{
			$fichas = $this->ficha->getByLike($this->session->userdata['BUSCA']['ficha'], $pagina,$limit,$json->ID_CARRINHO);
		}else{
			$fichas = $this->ficha->getByLike($this->session->userdata['BUSCA']['ficha'], $pagina,$limit);	
		}
		
		$response['fichas']= $fichas['data'];
		$response['pagina'] = $pagina;
		$response['totalPagina'] = $fichas['nPaginas'];
		
		setJson($response);
		
	}
	function getAll()
	{
		$json = getJson();
		
		$id_carrinho = $json->id_carrinho;		
		$result = array();	
			
		if(count($json->produtos)>0)
		{
			foreach($json->produtos as $item)
			{
				# chamada da funcao que ira retornar um array com as ocupadas				
				if($this->ficha->getByCarrinhoAndPeriodo($item,$id_carrinho))
					$result[] = $this->ficha->getByCarrinhoAndPeriodo($item,$id_carrinho);	 
			}			
		}		
	
		$response['EMAIL'] = $json->EMAIL;
		$response['carrinhos'] = $result;
		setJson($response);		
	}
	
	function pesquisarFichaPricing()
	{
		$json = getJson();	
		
		$dados['ID_APROVACAO_FICHA'] = 1;
		 
		$pagina = (isset($json->pagina)) ?$json->pagina :0;
		
		$dados['tipo_pesquisa'] = 'completa';
		$dados['TIPO_COMBO_FICHA'] = $json->TIPO_COMBO_FICHA;
		$dados['DESC_CARRINHO'] = $json->DESC_CARRINHO;
		$dados['NOME_FICHA'] = $json->NOME_FICHA;
		$dados['COD_FICHA'] = $json->COD_FICHA;
		$dados['ID_CATEGORIA'] = $json->ID_CATEGORIA;
		$dados['ID_SUBCATEGORIA'] = $json->_ID_SUBCATEGORIA;		
		//$dados['FLAG_SITUACAO_FICHA'] = $json->FLAG_SITUACAO_FICHA;				
		$dados['MARCA_FICHA'] = null;	
		$dados['FABRICANTE_FICHA']= null;
	
		$dados_ficha = $dados;
		
		$limit = 5;
		
		$fichas = $this->ficha->getByLikeByCarrinho($dados_ficha, $pagina,$limit);
		
		$response['fichas'] = $fichas['data']; 
		
		$response['pagina'] = $pagina;
		$response['totalPagina'] = $fichas['nPaginas'];		
		
		setJson($response);		
	}

	public function pesquisarFichas()
	{
		$resposta['ID_APROVACAO_FICHA'] = 1;
		
		
		$pagina = (isset($_POST['pagina'])) ? $_POST['pagina'] : 0;
		
		$resposta['tipo_pesquisa'] = 'completa';
		$resposta['TIPO_COMBO_FICHA'] = $_POST['tipoficha'];
		$resposta['NOME_FICHA'] = $_POST['nome'];
		$resposta['COD_FICHA'] = $_POST['codigo'];
		$resposta['ID_CATEGORIA'] = $_POST['categoria'];
		$resposta['ID_SUBCATEGORIA'] = $_POST['subcategoria'];		
		//$resposta['FLAG_SITUACAO_FICHA'] = $_POST['situacao'];				
		$resposta['MARCA_FICHA'] = null;	
		$resposta['FABRICANTE_FICHA']= null;
		
		if ( isset($resposta['COD_FICHA'] ) )
		{
			$dados['ficha'] = $resposta;
			$this->session->set_userdata('BUSCA', $dados);			
		}
		
		$limit = 5;
		
		if(isset($_POST['carrinho']))
		{
			$fichas = $this->ficha->getByLike($this->session->userdata['BUSCA']['ficha'], $pagina,$limit, $_POST['carrinho']);
		}else{
			$fichas = $this->ficha->getByLike($this->session->userdata['BUSCA']['ficha'], $pagina,$limit);	
		}
		
		$response['fichas']= $fichas['data'];
		$response['limit'] = $limit;
		$response['pagina'] = $pagina;
		$response['totalPagina'] = $fichas['nPaginas'];
		$response['acao'] = isset($_POST['acao']) ? $_POST['acao'] : '';
		$response['idficha'] = isset($_POST['idficha']) ? $_POST['idficha'] : '';
		
		$this->load->view('ROOT/carrinho/resultado_pesquisa', $response);
		
	}
	
	/**
	 * efetua uma pesquisa de objetos na tela de criacao/edicao de fichas
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function pesquisarObjetos(){
		
		$pagina = (isset($_POST['pagina'])) ? $_POST['pagina'] : 0;
		$user = Sessao::get('usuario');
		
		$path = $this->config->item('caminho_storage');

		$resposta['ID_CLIENTE'] = post('ID_CLIENTE', true);
		$resposta['ID_AGENCIA'] = post('ID_AGENCIA', true);
		
		if(!empty($user['ID_AGENCIA'])){
			$resposta['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}
		
		if(!empty($user['ID_CLIENTE'])){
			$resposta['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}
		
		$resposta['PERIODO_INICIAL'] = post('PERIODO_INICIAL',true);
		$resposta['PERIODO_FINAL'] = post('PERIODO_FINAL',true);
		$resposta['FILENAME'] = post('NOME',true);
		$resposta['ORIGEM'] = post('ORIGEM',true);
		$resposta['CODIGO'] = post('CODIGO',true);
		$resposta['ID_CATEGORIA'] = post('CATEGORIA',true);
		$resposta['ID_SUBCATEGORIA'] = post('SUBCATEGORIA',true);
		$resposta['ID_TIPO_OBJETO'] = post('ID_TIPO_OBJETO',true);
		$resposta['ID_FABRICANTE'] = post('ID_FABRICANTE',true);
		$resposta['ID_MARCA'] = post('ID_MARCA',true);
		$resposta['KEYWORDS'] = post('KEYWORDS',true);
		
		$limit = 5;
		
		$objetos = $this->objeto->listItems($resposta, $pagina, $limit);
		
		$response['total']= $objetos['total'];
		$response['objetos']= $objetos['data'];
		$response['limit'] = $limit;
		$response['pagina'] = $pagina;
		$response['_sessao'] = Sessao::get('usuario');
		$response['totalPagina'] = $objetos['nPaginas'];
		
		$this->load->view('ROOT/ficha/resultado_pesquisa', $response);
	}
	
	public function getFichaJson($id){
		ob_end_clean();
		$ficha = $this->ficha->getById($id);
		
		if(!empty($ficha)){
			echo json_encode($ficha);
		} else {
			echo '{}';
		}
		exit;
	}
	
	public function addCampo($idFicha, $campo, $valor, $id_campo){
		$this->campo_ficha->saveCampoFicha($campo, $valor, $idFicha, $id_campo);
	}
	
	public function historico_job($idjob){
		$this->assign('historico', $this->job->getHistoricoAprovacao($idjob));
		$this->load->view('ROOT/checklist/popup_historico', $this->data);
	}
	
	public function search($pagina=0){
		$busca = &$_POST;
		$idjob = post('ID_JOB', true);

		$this->setSessionData($busca);
		
		$job = $this->job->getById($idjob);
		
		$pracas = $this->praca->getByExcel($job['ID_EXCEL']);
		
		unset($busca['ID_USUARIO']);
		
		$busca['FLAG_ACTIVE_FICHA'] = 1;
		$busca['ID_APROVACAO_FICHA'] = 1;
		$busca['FLAG_TEMPORARIO'] = 0;
		
		$limit = 10;
		
		$busca['CATEGORIAS'] = getCategoriasUsuario($job['ID_CLIENTE']);

		if(empty($_POST['TIPO_FICHA']) || $_POST['TIPO_FICHA'] == "TODOS"){
			$busca['TIPO_FICHA']  = array('PRODUTO', 'COMBO');
		}
		else {
			$busca['TIPO_FICHA']  = array($_POST['TIPO_FICHA'] );
		}

		$fichas = $this->ficha->listItems($busca, $pagina*$limit, $limit);

		// percorre todas as fichas
		for($i = 0; $i <= sizeof($fichas['data']) - 1; $i++){
			// adiciona os cenarios nas fichas
			$cenarios = array();
			$cenarios = $this->cenario->getCenariosByIdFicha( $fichas['data'][$i]['ID_FICHA'] );
			$fichas['data'][$i]['CENARIOS'] = $cenarios;
			
			// pega as fichas filhas e seus objetos
			$fichas['data'][$i]['fichasFilhas'] = $this->ficha->getFichasFilha($fichas['data'][$i]['ID_FICHA']);
			
			// pega as subfichas
			//$fichas['data'][$i]['subfichas'] = $this->subficha->getByFichaPrincipal( $fichas['data'][$i]['ID_FICHA'] );
			$fichas['data'][$i]['subfichas'] = $this->subficha->getByFichaPrincipal( $fichas['data'][$i]['ID_FICHA'] );
		}		
		
		$fichasCombo = $this->ficha->listCombosIn(getValoresChave($fichas['data'], 'ID_FICHA'), true);
		
		// percorre todas as fichas combos
		for($i = sizeof($fichasCombo['data']) - 1; $i >= 0; $i--){
			// adiciona os cenarios nas fichas combos
			$cenarios = array();
			$cenarios = $this->cenario->getCenariosByIdFicha( $fichasCombo['data'][$i]['ID_FICHA'] );
			$fichasCombo['data'][$i]['CENARIOS'] = $cenarios;
			if( $fichasCombo['data'][$i]['CHAVE'] != 'aprovado' ){
				unset($fichasCombo['data'][$i]);
			}
		}	

		$this->assign('pagina', $pagina);
		$this->assign('lista', $fichas);
		$this->assign('fichasCombo', $fichasCombo);
		$this->assign('paginacao', linkpagination($fichas, $limit));
		
		$this->load->view('ROOT/carrinho/resultado_pesquisa', $this->data);
	}
	
	public function carrega_codigos(){
		$idcliente = post('idcliente',true);
		$idficha = post('idficha',true);
		
		$regioes = $this->regiao->getByCliente($idcliente);
		$codigos = $this->ficha->getCodigosRegionais($idficha);
		
		foreach($regioes as $idr => $regiao){
			foreach($codigos as $idc => $codigo){
				if($codigo['ID_REGIAO'] == $regiao['ID_REGIAO']){
					$regioes[$idr]['CODIGO'] = $codigo['CODIGO'];
				}
			}
		}
		
		$this->assign('idficha', $idficha);
		$this->assign('regioes', $regioes);
		$this->display('ROOT/ficha/codigos_regionais');
	}
	
	/**
	 * Exibe o formulario para pesquisa de fichas combo
	 * 
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function busca_fichas_combo( $acao = '' ){
		// somente pode listar da categoria do usuario
		$lista = getCategoriasUsuario(post('ID_CLIENTE',true),false);
		
		
		// lista de fabricantes
		$fabricantes = $this->fabricante->listItems(array(),0,300000);
		
		// coloca no template
		$this->assign('fabricantes', $fabricantes['data']);
		$this->assign('categorias', $lista);
		
		switch($acao){
			case 'pesquisar':
				// limite por pagina
				$limit = 20;
				// offset atual
				$pagina = sprintf('%d', post('pagina',true));
				
				// filtros 
				$_POST['ID_APROVACAO_FICHA'] = 1;
				$_POST['FLAG_TEMPORARIO'] = 0;
				$_POST['FLAG_ACTIVE_FICHA'] = 1;
				$_POST['TIPO_FICHA'] = 'PRODUTO';

				// recupera as fichas
				$fichas = $this->ficha->listItems($_POST,$pagina,$limit);
				
				// atribui os valores
				$this->assign('limit', $limit);
				$this->assign('total', $fichas['total']);
				$this->assign('lista', $fichas['data']);
				
				// exibe o resultado
				$this->display('ROOT/ficha/busca_fichas_combo_resultado');
			break;
			
			case 'adicionar':
				// se informou uma lista de  id de ficha
				if( !empty($_POST['id']) && is_array($_POST['id']) ){
					// preparamos o retorno
					$json = array(
						'fichas' => array(),
						'objetos' => array()
					);
					
					// recupera as fichas
					$json['fichas'] = $this->ficha->getByListaId($_POST['id']);
					
					// para cada ficha encontrada
					foreach($json['fichas'] as &$ficha){
						// recupera os objetos
						$itens = $this->objeto_ficha->getByFicha($ficha['ID_FICHA']);
						// faz um merge com os itens que vao para o json
						$json['objetos'] = array_merge($json['objetos'], $itens);
						// ajusta a data
						$ficha['DT_INSERT_FICHA'] = date('d/m/Y H:i:s', strtotime($ficha['DT_INSERT_FICHA']));
					}
					
					// escrevemos a saida
					echo json_encode($json);
				}
			break;
			
			default:
				// somente carrega a tela de pesquisa
				$this->display('ROOT/ficha/busca_fichas_combo');
			break;
		}
	}
	
	/**
	 * Exibe o formulario para pesquisa de fichas pai
	 * 
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function busca_fichas_pai( $acao = '' ){
		// somente pode listar da categoria do usuario
		$lista = getCategoriasUsuario(post('ID_CLIENTE',true),false);
		
		
		// lista de fabricantes
		$fabricantes = $this->fabricante->listItems(array(),0,300000);
		
		// lista de situacoes
		$aprovacoes = $this->aprovacao_ficha->getAll();

		// coloca no template
		$this->assign('fabricantes', $fabricantes['data']);
		$this->assign('categorias', $lista);
		$this->assign('aprovacoes', $aprovacoes['data']);
		
		switch($acao){
			case 'pesquisar':
				// limite por pagina
				$limit = 20;
				// offset atual
				$pagina = sprintf('%d', post('pagina',true));
				
				// filtros 
				//$_POST['ID_APROVACAO_FICHA'] = 1;
				$_POST['FLAG_TEMPORARIO'] = 0;
				//$_POST['FLAG_ACTIVE_FICHA'] = 1;
				$_POST['TIPO_FICHA'] = 'PRODUTO';

				// recupera as fichas
				$fichas = $this->ficha->listItems($_POST,$pagina,$limit);
				
				// atribui os valores
				$this->assign('limit', $limit);
				$this->assign('total', $fichas['total']);
				$this->assign('lista', $fichas['data']);
				
				// exibe o resultado
				$this->display('ROOT/ficha/busca_fichas_pai_resultado');
			break;
			
			case 'adicionar':
				// se informou uma lista de  id de ficha
				if( !empty($_POST['id']) && is_array($_POST['id']) ){
					// preparamos o retorno
					$json = array(
						'fichas' => array(),
						'objetos' => array()
					);
					
					// recupera as fichas
					$json['fichas'] = $this->ficha->getByListaId($_POST['id']);
					
					// para cada ficha encontrada
					foreach($json['fichas'] as &$ficha){
						// recupera os objetos
						$itens = $this->objeto_ficha->getByFicha($ficha['ID_FICHA']);
						// faz um merge com os itens que vao para o json
						$json['objetos'] = array_merge($json['objetos'], $itens);
						// ajusta a data
						$ficha['DT_INSERT_FICHA'] = date('d/m/Y H:i:s', strtotime($ficha['DT_INSERT_FICHA']));
					}
					
					// escrevemos a saida
					echo json_encode($json);
				}
			break;
			
			default:
				// somente carrega a tela de pesquisa
				$this->display('ROOT/ficha/busca_fichas_pai');
			break;
		}
	}
	
	/**
	 * Exibe o formulario para pesquisa de fichas filha
	 * 
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function busca_fichas_filha( $acao = '' ){
		// somente pode listar da categoria do usuario
		$lista = getCategoriasUsuario(post('ID_CLIENTE',true),false);
		
		
		// lista de fabricantes
		$fabricantes = $this->fabricante->listItems(array(),0,300000);
		
		// lista de situacoes
		$aprovacoes = $this->aprovacao_ficha->getAll();

		// coloca no template
		$this->assign('fabricantes', $fabricantes['data']);
		$this->assign('categorias', $lista);
		$this->assign('aprovacoes', $aprovacoes['data']);
		
		switch($acao){
			case 'pesquisar':
				// limite por pagina
				$limit = 20;
				// offset atual
				$pagina = sprintf('%d', post('pagina',true));
				
				// filtros 
				//$_POST['ID_APROVACAO_FICHA'] = 1;
				$_POST['FLAG_TEMPORARIO'] = 0;
				//$_POST['FLAG_ACTIVE_FICHA'] = 1;
				$_POST['TIPO_FICHA'] = 'PRODUTO';

				// recupera as fichas
				$fichas = $this->ficha->listItems($_POST,$pagina,$limit);
				
				// atribui os valores
				$this->assign('limit', $limit);
				$this->assign('total', $fichas['total']);
				$this->assign('lista', $fichas['data']);
				
				// exibe o resultado
				$this->display('ROOT/ficha/busca_fichas_filha_resultado');
			break;
			
			case 'adicionar':
				// se informou uma lista de  id de ficha
				if( !empty($_POST['id']) && is_array($_POST['id']) ){
					// preparamos o retorno
					$json = array(
						'fichas' => array(),
						'objetos' => array()
					);
					
					// recupera as fichas
					$json['fichas'] = $this->ficha->getByListaId($_POST['id']);
					
					// para cada ficha encontrada
					foreach($json['fichas'] as &$ficha){
						// recupera os objetos
						$itens = $this->objeto_ficha->getByFicha($ficha['ID_FICHA']);
						// faz um merge com os itens que vao para o json
						$json['objetos'] = array_merge($json['objetos'], $itens);
						// ajusta a data
						$ficha['DT_INSERT_FICHA'] = date('d/m/Y H:i:s', strtotime($ficha['DT_INSERT_FICHA']));
					}
					
					// escrevemos a saida
					echo json_encode($json);
				}
			break;
			
			default:
				// somente carrega a tela de pesquisa
				$this->display('ROOT/ficha/busca_fichas_filha');
			break;
		}
	}
	
	/**
	 * Busca as fichas filhas para exibir na pauta
	 * 
	 * @author Sidnei Tertuliano Junior
	 * @return void
	 */
	public function busca_fichas_filha_pauta( $idFichaPai = null ){
		// preparamos o retorno
		$json = array(
			'fichasFilhas' => array(),
		);
		
		$fichasFilhas = $this->ficha->getFichasFilha($idFichaPai);
		
		foreach($fichasFilhas as $item){
			$item['DESC_APROVACAO_FICHA'] = ucfirst($item['DESC_APROVACAO_FICHA']);
			$item['TIPO_FICHA'] = strtolower($item['TIPO_FICHA']);
			$item['FLAG_TEMPORARIO'] = $item['FLAG_TEMPORARIO'] == "0" ? "Não" : "Sim" ;
			$item['FLAG_ACTIVE_FICHA'] = $item['FLAG_ACTIVE_FICHA'] == "0" ? "Inativa" : "Ativa" ;
			$obj = $this->objeto_ficha->getByFicha($item['ID_FICHA']);
			$fileID = 0;
			if(count($obj) > 0){
				$fileID = $obj[0]['FILE_ID'];
			}
			$item['FILE_ID'] = $fileID;
			array_push($json['fichasFilhas'], $item);
		}

		echo json_encode($json);
	}
	
	/**
	 * Exibe o formulario para pesquisa de subfichas
	 * 
	 * @author Sidnei
	 * @return void
	 */
	public function busca_subfichas( $acao = '' ){
		// somente pode listar da categoria do usuario
		$lista = getCategoriasUsuario(post('ID_CLIENTE',true),false);
		
		// lista de fabricantes
		$fabricantes = $this->fabricante->listItems(array(),0,300000);
		
		// lista de situacoes
		$aprovacoes = $this->aprovacao_ficha->getAll();

		// coloca no template
		$this->assign('fabricantes', $fabricantes['data']);
		$this->assign('categorias', $lista);
		$this->assign('aprovacoes', $aprovacoes['data']);
		
		switch($acao){
			case 'pesquisar':
				// limite por pagina
				$limit = 20;
				// offset atual
				$pagina = sprintf('%d', post('pagina',true));
				
				// filtros 
				//$_POST['ID_APROVACAO_FICHA'] = 1;
				$_POST['FLAG_TEMPORARIO'] = 0;
				//$_POST['FLAG_ACTIVE_FICHA'] = 1;
				$_POST['TIPO_FICHA'] = 'SUBFICHA';

				// recupera as fichas
				$fichas = $this->ficha->listItems($_POST,$pagina,$limit);
				
				// atribui os valores
				$this->assign('limit', $limit);
				$this->assign('total', $fichas['total']);
				$this->assign('lista', $fichas['data']);
				
				// exibe o resultado
				$this->display('ROOT/ficha/busca_subfichas_resultado');
			break;
			
			case 'adicionar':
				// se informou uma lista de  id de ficha
				if( !empty($_POST['id']) && is_array($_POST['id']) ){
					// preparamos o retorno
					$json = array(
						'fichas' => array(),
						'objetos' => array()
					);
					
					// recupera as fichas
					$json['fichas'] = $this->ficha->getByListaId($_POST['id']);
					
					// para cada ficha encontrada
					foreach($json['fichas'] as &$ficha){
						// recupera os objetos
						$itens = $this->objeto_ficha->getByFicha($ficha['ID_FICHA']);
						// faz um merge com os itens que vao para o json
						$json['objetos'] = array_merge($json['objetos'], $itens);
						// ajusta a data
						$ficha['DT_INSERT_FICHA'] = date('d/m/Y H:i:s', strtotime($ficha['DT_INSERT_FICHA']));
					}
					
					// escrevemos a saida
					echo json_encode($json);
				}
			break;
			
			default:
				// somente carrega a tela de pesquisa
				$this->display('ROOT/ficha/busca_subfichas');
			break;
		}
	}
	
	/**
	 * Metodo para atualizar os objetos de fichas combo
	 * 
	 * Chamado na parte de objetos de fichas combo.
	 * Como uma ficha combo pode ter varias fichas-produto
	 * relacionadas, este metodo traz todos os objetos
	 * de todas as fichas relacionadas a ficha combo
	 * que esta sendo editada.
	 * 
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function atualizar_objetos_combo(){
		$lista = array();
		if( !empty($_POST['id']) && is_array($_POST['id']) ){
			foreach($_POST['id'] as $id){
				$objetos = $this->objeto_ficha->getByFicha( $id );
				
				$lista = array_merge($lista, $objetos);
			}
		}
		
		echo json_encode( $lista );
	}
	
	/**
	 * Verifica se existem fichas-combo relacionadas ao objeto e ficha informados
	 * 
	 * O usuario somente podera remover objetos de fichas-produto
	 * quando o objeto que ele esta tentado remover nao estiver 
	 * relacionado a fichas-combo. 
	 * 
	 * Para saber isso na lado do cliente, utilizamos este metodo
	 * para retornar a quais fichas combo este objeto esta associado.
	 * 
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function checar_exclusao_objeto(){
		// pega os dados do formulario
		$idficha = post('ID_FICHA',true);
		$idobjeto = post('ID_OBJETO',true);
		
		// lista de fichas associadas
		$lista = $this->objeto_ficha->getFichasComboAssociadas( $idficha, $idobjeto );
		// lista de codigos
		$codigos = array();
		
		// para cada ficha encontrada
		foreach($lista as $item){
			// pegamos o codigo de barras da ficha
			$codigos[] = $item['COD_BARRAS_FICHA'];
		}
		
		// escrevemos na saida os codigos no formato JSON
		echo json_encode($codigos);
	}
	
	/**
	 * Acao para atualizar os produtos de fichas combo
	 * 
	 * Este metodo eh chamado no cadastro de fichas produto, onde o 
	 * usuario podera atualizar as fichas combo que a ficha produto
	 * que esta sendo editada pertence. 
	 * 
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function atualizar_associacoes(){
		/////////////////////// SALVANDO OS OBJETOS DA FICHA PRODUTO /////////////////////////
		// primeiro, vamos salvar os objetos enviados
		$idFichaPai = post('ID_FICHA',true);
		$objetosPai = empty($_POST['objetos']) ? array() : $_POST['objetos'];
		
		// remove objetos antigos
		$this->objeto_ficha->deleteByFicha($idFichaPai);
		// lista de objetos salvos
		$salvos = array();
		// para cada objeto
		foreach($objetosPai as $item){
			// se estiver na lista de ja salvos 
			if(in_array($item['ID_OBJETO'],$salvos)){
				// pula para o proximo
				continue;
			}
			// verifica se eh o objeto principal
			$item['PRINCIPAL'] = post('PRINCIPAL', true) == $item['FILE_ID'] ? 1 : 0;
			// indica o ID da ficha
			$item['ID_FICHA'] = $idFichaPai;
			// salva o objeto
			$this->objeto_ficha->save($item);
			// indica que foi salvo
			$salvos[] = $item['ID_OBJETO'];
		}
		
		/////////////////////// SALVANDO OS OBJETOS DAS FICHAS COMBO /////////////////////////
		// pega o codigo da ficha produto
		$idficha = post('id', true);
		// pega as fichas combo selecionadas
		$combos = post('combos',true);
		// pega os objetos da ficha produto
		$objetos = $this->objeto_ficha->getByFicha($idficha);
		// se houver combos selecionadas e for array
		if( is_array($combos) ){
			// para cada combo como id de ficha combo
			foreach( $combos as $idcombo ){
				// para cada objeto da ficha produto
				foreach($objetos as $obj){
					// adiciona na ficha combo
					$this->ficha->adicionarObjeto( $idcombo, $obj['ID_OBJETO']);
				}
			}
		}
		// envia um sinal de ok
		echo 'ok';
	}
	
	function excluirCampo(){
		$this->campo_ficha->delete($_POST['idCampoFicha']);
		echo 'ok';
	}
	
	function testeHermes($ref){
		$this->load->library('importaficha');
		$retorno = $this->importaficha->hermes($ref);
		$retorno = $retorno['return'];
echo 'teste';
		print_rr($retorno);
	}	
	
	// funcao que acessa o webservice do COMPRA FACIL e retorna os dados
	function importaCompraFacil($ref){
		$this->load->library('importaficha');
		$retorno = $this->importaficha->comprafacil($ref);
		$retorno = $retorno['return'];
		
		if(!isset($retorno['msgErro'])){
			$ficha = array();
			
			$retorno = $retorno['produtoDto'];

			$ficha['COD_CIP_FICHA'] = $retorno['cip'];
			$ficha['NOME_FICHA'] = $retorno['descricao'];
			$ficha['DESC_SUBCATEGORIA'] = $retorno['linha'];
			$ficha['MODELO_FICHA'] = $this->importaficha->retornaModelo($retorno['infoCaracteristicas']);
			$ficha['DESC_MARCA'] = $this->importaficha->retornaMarca($retorno['infoCaracteristicas']);
			$ficha['CAMPO_VOLTAGEM'] = $retorno['voltagem'];
			$ficha['CAMPO_GARANTIA'] = removeQuebraLinha($retorno['infoGarantia']);
			$ficha['CAMPO_DIMENSOES'] = removeQuebraLinha($retorno['infoDimensoes']);
			$ficha['CAMPO_DESCRICAO'] = removeQuebraLinha($retorno['infoCaracteristicas']);
			$ficha['CAMPO_COR'] = $this->importaficha->retornaCor($retorno['infoCaracteristicas']);
			setJson($ficha);
		}
		else{
			setJson($retorno);
		}
	}
	
	function importaHermes($ref){
		$this->load->library('importaficha');
		$retorno = $this->importaficha->hermes($ref);
		$retorno = $retorno['return'];
		
		if(!isset($retorno['msgErro'])){
			$ficha = array();
			
			$retorno = $retorno['referenciaHermesDto'];
			
			$ficha['DESC_CATEGORIA'] = $retorno['grupo'];
			if( (!empty($retorno['cor'])) && (trim($retorno['cor']) != "")){
				$ficha['NOME_FICHA'] = $retorno['cor'];
			}
			else{
				$ficha['NOME_FICHA'] = $retorno['voltagem'];
			}
			$ficha['DESC_SUBCATEGORIA'] = $retorno['dsc_linha'];
			$ficha['CAMPO_ETIQUETA'] = removeQuebraLinha($retorno['etiqueta']);
			$ficha['CAMPO_DESCRICAO'] = removeBr($retorno['descricao']);
			$ficha['CAMPO_CHAMADA'] = removeBr($retorno['chamadaBiblia']);
			$ficha['CAMPO_COMPOSICAO'] = removeBr($retorno['composicaoBiblia']);
			$ficha['CAMPO_DIMENSOES'] = removeQuebraLinha($retorno['tamanho']);
			$ficha['CAMPO_DIMENSOES'] = str_replace('ST,', '', $ficha['CAMPO_DIMENSOES']);
			$ficha['CAMPO_DIMENSOES'] = str_replace(',ST', '', $ficha['CAMPO_DIMENSOES']);
			$ficha['CAMPO_DIMENSOES'] = str_replace('ST', '', $ficha['CAMPO_DIMENSOES']);
			setJson($ficha);
		}
		else{
			setJson($retorno);
		}
	}	
	
	function verificaCipExistente($cip){
		$ficha = $this->ficha->getCipExistente($cip);

		setJson($ficha);
	}
	
	function updatePendencia(){
		
		$id = $_POST['idFicha'];
		$insert = array('PENDENCIAS_DADOS_BASICOS' => $_POST['pendencia']);
		
		$ficha = $this->ficha->getById($id);
		
		if ($ficha['PENDENCIAS_DADOS_BASICOS'] != $_POST['pendencia']) {
			$this->ficha->updateFicha($insert, $id);
			echo 1;
		} else {
			echo 0;
		}
		
	} 
	public function getFichaByIdComRetorno($id_ficha) {
		$ficha = $this->ficha->getById($id_ficha);
		
		$ficha['PENDENCIAS_DADOS_BASICOS'] = trim($ficha['PENDENCIAS_DADOS_BASICOS']);
		
		if (isset($ficha['PENDENCIAS_DADOS_BASICOS']) && !empty($ficha['PENDENCIAS_DADOS_BASICOS'])) {
			echo 1;
		} else {
			echo 0;
		}
	}
	
}




























































