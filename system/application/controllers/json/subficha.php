<?php
class Subficha extends MY_Controller
{
	function salvar(){
		
		$user = Sessao::get('usuario');

		if(isset($_POST['ID_SUBFICHA'])){
			$idFicha2 = $_POST['ID_SUBFICHA'];
			$sf = $this->subficha->getByFichas($_POST['ID_FICHA'], $idFicha2);
			if(count($sf) > 0){
				$idSubficha = $sf[0]['ID_SUBFICHA'];
			}
		}
		else{
			$idFicha2 = null;
		}

		$fichaPrincipal = $this->ficha->getById($_POST['ID_FICHA']);

		$subFicha = array();
		$subFicha['ID_USUARIO'] = $user['ID_USUARIO'];
		$subFicha['COD_BARRAS_FICHA'] = $_POST['COD_BARRAS_FICHA'];
		$subFicha['ID_CATEGORIA'] = $fichaPrincipal['ID_CATEGORIA'];
		$subFicha['ID_SUBCATEGORIA'] = $fichaPrincipal['ID_SUBCATEGORIA'];
		$subFicha['FLAG_ACTIVE_FICHA'] = "1";
		$subFicha['ID_APROVACAO_FICHA'] = "1";
		$subFicha['TIPO_COMBO_FICHA'] = "1";
		$subFicha['FLAG_TEMPORARIO'] = "0";
		$subFicha['ID_CLIENTE'] = $fichaPrincipal['ID_CLIENTE'];
		$subFicha['TIPO_FICHA'] = "SUBFICHA";

		$rs = $this->ficha->getByClienteCodigoBarras($fichaPrincipal['ID_CLIENTE'], $_POST['COD_BARRAS_FICHA']);

		if(count($rs) > 0){
			if($rs['ID_FICHA'] != $idFicha2){
				echo "Código de Barras já cadastrado para este Cliente";
				die;
			}
		}

		$idFichaCadastrada = $this->ficha->save($subFicha, $idFicha2);
		
		if(!isset($idSubficha)){
			$idSubficha = $this->subficha->save(array('ID_FICHA1' => $_POST['ID_FICHA'], 'ID_FICHA2' => $idFichaCadastrada));
		}
		
		$this->campoRelacionado->deleteBySubficha($idSubficha);
		
		if(isset($_POST['relacionado'])){
			foreach($_POST['relacionado'] as $campoFicha){
				$this->campoRelacionado->save(array('ID_CAMPO_FICHA' => $campoFicha, 'ID_SUBFICHA' => $idSubficha));
			}
		}
		
		$this->campo_ficha->deleteByIdFicha($idFichaCadastrada);
		
		if(isset($_POST['idcampo']) && isset($_POST['campo']) && isset($_POST['valor'])){
			for($i = 0; $i <= count($_POST['idcampo']) - 1; $i++){
				$insert = array(
					'ID_FICHA' => $idFichaCadastrada,
					'LABEL_CAMPO_FICHA' => $_POST['campo'][$i],
					'VALOR_CAMPO_FICHA' => $_POST['valor'][$i],
					'ID_CAMPO' => $_POST['idcampo'][$i],
					'PERTENCE_SUBFICHA' => '0',
					'PRINCIPAL' => '0'
				);
				if(isset($_POST['principal'])){
					if((string)$_POST['principal'] == (string)$_POST['idcampo'][$i]){
						$insert['PRINCIPAL'] = '1';
					}
				}

				$this->campo_ficha->save($insert);
			}
		}
		
		if(is_numeric($idFichaCadastrada)){
			$retorno = $this->ficha->getById($idFichaCadastrada);
			$retorno['DT_INSERT_FICHA'] = format_date_to_form($retorno['DT_INSERT_FICHA']);
		 	setJson($retorno);
		}
		else{
			echo "Erro ao cadastrar subficha.";
		}	
	}
	
	function desvincular(){
		$this->subficha->desvincular($_POST['idFicha1'], $_POST['idFicha2']);
		echo 'ok';
	}
	
	function vincular(){
		$retorno = $this->subficha->vincular($_POST['idFicha1'], $_POST['idFicha2']);
		if($retorno > 0){
			$retorno = $this->ficha->getById($_POST['idFicha2']);
			$retorno['DT_INSERT_FICHA'] = format_date_to_form($retorno['DT_INSERT_FICHA']);
		 	setJson($retorno);
		}
		else{
			echo '';
		}
	}
	
	public function busca_subfichas_pauta( $idFicha = null ){
		// preparamos o retorno
		$json = array(
			'subfichas' => array(),
		);
		
		$subfichas = $this->subficha->getByFichaPrincipal($idFicha);

		foreach($subfichas as $item){
			$item['TIPO_FICHA'] = strtolower($item['TIPO_FICHA']);
			$item['FLAG_TEMPORARIO'] = $item['FLAG_TEMPORARIO'] == "0" ? "Não" : "Sim" ;
			$item['FLAG_ACTIVE_FICHA'] = $item['FLAG_ACTIVE_FICHA'] == "0" ? "Inativa" : "Ativa" ;
			$obj = $this->objeto_ficha->getByFicha($item['ID_FICHA'],true);
			//echo $this->db->last_query();die;
			$fileID = 0;
			if(count($obj) > 0){
				$fileID = $obj[0]['FILE_ID'];
			}
			$item['FILE_ID'] = $fileID;
			array_push($json['subfichas'], $item);
		}

		echo json_encode($json);
	}
	
	//aqui limpamos todos os objetos da subficha e adicinamos somente os selecionados
	public function salvarObjetoSubficha(){
		if(isset($_POST['post']) && !empty($_POST['post'])){
			$this->objeto_ficha->deleteByFicha($_POST['post'][0]['idSubFicha']);
			if(isset($_POST['post'][0]['idObjeto'])){
				foreach ($_POST['post'] as $objeto){
					$data['ID_OBJETO'] = $objeto['idObjeto'];
					$data['ID_FICHA'] = $objeto['idSubFicha'];
					$data['PRINCIPAL'] = $objeto['principal'];
	
					$this->objeto_ficha->insertObjetoSubFicha($data);
				}
			}
		}
	}
}