<?php
/**
 * Acoes de ajax para comentarios
 * 
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class Aprovacao extends MY_Controller
{
	/**
	 * Construtor 
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Admin
	 */
	function __construct(){
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/aprovacao/';
		
	}
	
	/**
	 * Recupera os comentarios feitos em um arquivo na aprovacao
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $id_arquivo codigo do arquivo
	 * @return void
	 */
	public function get_comentarios_arquivo($id_arquivo, $podeComentar = 1) {
		
		$this->checaLogado();
		$lista = $this->aprovacao->getComentariosArquivo($id_arquivo);
		
		//alteração feita pois agora é possivel adicionar 1 comentario para varios arquivos diferentes
		foreach ($lista as $key => $list) {
			$comentarioArquivos = $this->aprovacaoComentarioArquivo->getComentarioArquivoByArquivoID($list['ID_APROVACAO_COMENTARIO'],$list['ID_APROVACAO_ARQUIVO']);
			
			if(!empty($comentarioArquivos)){
				foreach ($comentarioArquivos as $chave => $comentario){
					if($comentario['ID_APROVACAO_ARQUIVO'] != $list['ID_APROVACAO_ARQUIVO']){
						$lista[$key]['ComentariosCliente'][] = $comentarioArquivos[$chave];
					}
				}
			}
		}
		//print_rr($lista);die;
		$this->assign('podeBaixarPDF', Sessao::hasPermission('aprovacao','visualizarPDFAP'));
		$this->assign('podeComentarArquivo', Sessao::hasPermission('aprovacao','comentarArquivo') && $podeComentar == 1);
		$this->assign('comentarios', $lista);
		$this->display('json_comentarios_arquivo');
	}
	
	/**
	 * Recupera os comentarios feitos nos arquivos de uma aprovação
	 *
	 * @author Bruno de Oliveira
	 * @param int $id_ap codigo da aprovação
	 * @return void
	 */
	public function get_comentarios_aprovacao($id_ap, $podeComentar = 1) {
	
		$this->checaLogado();
		$lista = $this->aprovacaoArquivo->getcomentariosByAprovacao($id_ap);
		//print_rr($lista);die;
		//alteração feita pois agora é possivel adicionar 1 comentario para varios arquivos diferentes
		foreach ($lista as $key => $list) {
			$comentarioArquivos = $this->aprovacaoComentarioArquivo->getComentarioArquivoByArquivoID($list['ID_APROVACAO_COMENTARIO'],$list['ID_APROVACAO_ARQUIVO']);
				
			if(!empty($comentarioArquivos)){
				foreach ($comentarioArquivos as $chave => $comentario){
					if($comentario['ID_APROVACAO_ARQUIVO'] != $list['ID_APROVACAO_ARQUIVO']){
						$lista[$key]['ComentariosCliente'][] = $comentarioArquivos[$chave];
					}
				}
			}
		}
		//print_rr($lista);die;
		$this->assign('podeBaixarPDF', Sessao::hasPermission('aprovacao','visualizarPDFAP'));
		$this->assign('podeComentarArquivo', Sessao::hasPermission('aprovacao','comentarArquivo') && $podeComentar == 1);
		$this->assign('comentarios', $lista);
		$this->display('json_comentarios_arquivo');
	}

	/**
	 * Imprime os comentarios de um Job
	 *
	 * @author Hugo Ferreira da Silva
	 * @param string $tipo Tipo de impressa
	 * @param int $id Codigo da aprovacao ou do arquivo
	 * @return void
	 */
	public function imprimir_comentarios($tipo, $id){
		$this->checaLogado();
		
		$arquivos = array();
		$job = array();
		$ap = array();
		
		switch($tipo){
			case 'arquivo':
				$file = $this->aprovacaoArquivo->getById($id);
				$ap = $this->aprovacao->getById($file['ID_APROVACAO']);
				$res = $this->checaPermissaoJob($ap['ID_JOB'], false);

				if( !$res ){
					die('Sem permissao de acessar o job');
				}
				
				$job = $this->job->getById($ap['ID_JOB']);

				$file['comentarios'] = $this->aprovacao->getComentariosArquivo($id);
				$arquivos[] = $file;
			break;
			
			case 'prova':
				$ap = $this->aprovacao->getById($id);
				$res = $this->checaPermissaoJob($ap['ID_JOB'], false);

				if( !$res ){
					die('Sem permissao de acessar o job');
				}
				
				$job = $this->job->getById($ap['ID_JOB']);
				$arquivos = $this->aprovacao->getArquivosAprovacao($id, true);
			break;
			
			default:
				die('Tipo desconhecido');
		}

		$this->assign('prova', $ap);
		$this->assign('job', $job);
		$this->assign('arquivos', $arquivos);
		$this->display('json_comentarios_impressao');
	}
	
	private function checaLogado(){
		$user = Sessao::get('usuario');
		if( empty($user) ){
			die('Você tem que estar logado');
		}
	}

	/**
	 * Checa se o usuario logado tem permissao ou nao de alterar o job acessado
	 *
	 * Se nao tiver permissao, redireciona para a listagem
	 *
	 * @author juliano.polito
	 * @param int $idjob
	 * @return void
	 */
	protected function checaPermissaoJob($idjob, $redirect = true){
		$user = Sessao::get('usuario');
		$job = $this->job->getById($idjob);
		$produtos = getValoresChave(getProdutosUsuario($job['ID_CLIENTE'],$job['ID_AGENCIA']),'ID_PRODUTO');


		if(!empty($user['ID_CLIENTE'])){
			$goto = 'gerar_indd/listar_cliente';

			if($job['ID_CLIENTE'] != $user['ID_CLIENTE']){
				if($redirect){
					redirect($goto);
				}else{
					return false;
				}
			}

			if(!in_array($job['ID_PRODUTO'],$produtos)){
				if($redirect){
					redirect($goto);
				}else{
					return false;
				}
			}
		}

		if(!empty($user['ID_AGENCIA'])){
			$goto = 'gerar_indd/listar_agencia';

			if($user['ID_AGENCIA'] != $job['ID_AGENCIA']){
				if($redirect){
					redirect($goto);
				}else{
					return false;
				}
			}

			$clientes = getValoresChave($this->cliente->getByAgencia($user['ID_AGENCIA']),'ID_CLIENTE');
			if(!in_array($job['ID_PRODUTO'],$produtos)){
				if($redirect){
					redirect($goto);
				}else{
					return false;
				}
			}

			if(!in_array($job['ID_CLIENTE'],$clientes)){
				if($redirect){
					redirect($goto);
				}else{
					return false;
				}
			}
		}

		return true;
	}
	
}


