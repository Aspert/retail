<?php
/**
 * Acoes a serem efetuadas via Ajax na parte de pdf
 * 
 * @author Juliano Polito
 * @link http://www.247id.com.br
 */
class Pdf extends MY_Controller
{
	/**
	 * Construtor 
	 * 
	 * @author juliano.polito
	 * @link http://www.247id.com.br
	 * @return Pdf
	 */
	function __construct(){
		parent::__construct();
	}
	
	/**
	 * Tras a lista de arquivos cadastrados pro job
	 * 
	 * @author juliano.polito
	 * @param $file
	 */
	function verifica($id){
		$opis = $this->excelopi->getByExcel($id);
		echo json_encode($opis);
		exit;
	}
}
































