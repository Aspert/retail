<?php

class Conexao extends My_Controller	{
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/conexao/';
	}
	
	function index()	{
		$data = $this->session->userdata('__BUSCA');
		if(!is_array($data)){
			$data = array($data);
		}
		
		$this->data['podeVer'] = Sessao::hasPermission('conexao','index');
		$this->display('index');		
	}
	
}