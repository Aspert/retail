<?php
/**
 * Controller para gerenciar linhas de produto
 * @author Hugo Silva
 *
 */
class Produto extends MY_Controller {
	/**
	 * Construtor
	 * @author Hugo Silva
	 * @return Cliente
	 */
	function __construct() {
		
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/produto/';
	}
	
	/**
	 * Metodo inicial da controller
	 * Acessado quando nao especificado um metodo
	 * @author Hugo Silva
	 * @return void
	 */
	public function index(){
		$this->lista();
	}
	
	/**
	 * Lista os itens cadastrados
	 * 
	 * @author Hugo Silva
	 * @param int $pagina Pagina inicial da listagem
	 * @return void
	 */
	public function lista($pagina=0){
		
		if (!empty($_POST)) {
			$dados = $_POST;
			$this->session->set_userdata('__BUSCA', $dados);			
		}
		
		$data = $this->session->userdata('__BUSCA');
		if(!is_array($data)){
			$data = array($data);
		}
		
		$limit = !empty($data['pagina']) ? $data['pagina'] : 5;
		$lista = $this->produto->listItems($data, $pagina, $limit);
		
		$clientes = $this->cliente->getAll();

		$this->data['clientes'] = $clientes;
		$this->data['busca'] = $data;
		$this->data['lista'] = $lista['data'];
		$this->data['paginacao'] = linkpagination($lista, $limit);
		$this->data['podeAlterar'] = Sessao::hasPermission('produto','save');
		$this->display('index');		
	}
	
	/**
	 * Exibe o formulario para edicao ou insercao
	 * 
	 * @author Hugo Silva
	 * @param int $id codigo do produto
	 * @return void
	 */
	public function form($id=null){
		
		if(!is_null($id) && $_SERVER['REQUEST_METHOD'] == 'GET') {
			$_POST = $this->produto->getById($id);
			$_POST['agencias'] = array();
			
			$lista = $this->produto->getFornecedores($id);
			foreach( $lista as $item ){
				$_POST['agencias'][] = $item['ID_AGENCIA'];
			}
			
			$regioes  = $this->regiao->getByProduto($id);
			foreach($regioes as $item){
				$_POST['regioes'][] = $item['ID_REGIAO'];
			}
		}
		
		$agencias = array();
		$jobsProduto = array();
		$regioes  = array();
		$calendarios = array();
		$clientes = $this->cliente->getAll();
		
		if(!empty($_POST['ID_CLIENTE'])){
			
			$agencias = $this->agencia->getByCliente($_POST['ID_CLIENTE']);
			$regioes  = $this->regiao->getByCliente($_POST['ID_CLIENTE']);
			$calendarios  = $this->calendario->getByCliente($_POST['ID_CLIENTE']);
			
			if(!empty($_POST['ID_PRODUTO'])){
				//Busca os jobs do produto em questao para mostrar em caso de mudanca de calendario
				$jobsProduto = $this->job->getJobsByProduto($_POST['ID_PRODUTO']);
			}			
		}

		$transf = $this->transformador->listItems(array(), 0, 10000);

		$this->data['transformadores']  = $transf['data'];
		$this->assign('jobsProduto', $jobsProduto);
		$this->assign('clientes', $clientes);
		$this->assign('calendarios', $calendarios);
		$this->assign('agencias', $agencias);
		$this->assign('regioes', $regioes);
		$this->assign('ID_PRODUTO', $id);
		$this->display('form');
	}
	
	/**
	 * Grava os dados do formulario
	 * 
	 * @author Hugo Silva
	 * @param int $id
	 * @return void
	 */
	public function save($id=null){
		
		$_POST['ID_PRODUTO'] = $id;
		
		//array com os jobs que utilizam o produto
		$jobsProduto = array();

		if( empty($id) ){
			$_POST['CHAVE_STORAGE_PRODUTO'] = ajustaChaveStorage($_POST['DESC_PRODUTO']);
		}
		
		if( empty($_POST['ID_TRANSFORMADOR']) ){
			$_POST['ID_TRANSFORMADOR'] = null;
		}
		
		$erros = $this->produto->validate($_POST);
		
		if(empty($erros)){
			//Grava o produto
			$id = $this->produto->save($_POST,$id);

			//Xinet
			$this->xinet->createPath($this->produto->getPath($id));
			
			//Busca os jobs do produto em questao para mostrar em caso de mudanca de calendario
			$jobsProduto = $this->job->getJobsByProduto($id);
			if (!empty($jobsProduto) && is_array($jobsProduto)) {
				foreach ($jobsProduto as $job) {
					$this->job->atualizaDatasJob($job['ID_JOB']);
				}
			}
			
			redirect('produto/lista');
		}

		$this->data['erros'] = $erros;
		$this->form($id);
	}
	
}