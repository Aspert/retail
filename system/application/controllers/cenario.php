<?php
class Cenario extends MY_Controller {

	function __construct(){
		parent::__construct();

		$this->_templatesBasePath = 'ROOT/cenario/';

		$this->load->library('DBXinet');

		$this->data['categorias'][''] = '[Selecione]';
		$categorias = $this->categoria->getAll();
		foreach ($categorias['data'] as $key => $value){
			$this->data['categorias'][$value['ID_CATEGORIA'] . '#' . $value['DESC_CATEGORIA']] = $value['DESC_CATEGORIA'];
		}
	}

	function index() {
		$this->load->view('ROOT/cenario/menu', $this->data);
	}
	
	/**
	 * Salva as alteracoes do cenario
	 *
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @param int $id
	 * @return void
	 */
	public function salvar($id = null){
		if(!empty($id)){
			$this->checaPermissaoAlterar($id);
		}
			
		// aramzena a array de fichas do cenario em uma variavel
		$fichas = array();
		if(isset($_POST['FICHAS'])){	
			$fichas = $_POST['FICHAS'];
			unset($_POST['FICHAS']);
		}
		
		// se nao houver objeto declara o valor ZERO ao mesmo
		if( !isset($_POST['ID_OBJETO']) ){
			$_POST['ID_OBJETO'] = '0';
		}
		
		// se for um novo cenario gera  um codigo para o mesmo exemplo 'CN00000000001'
		if($id == null){
			$_POST['COD_CENARIO'] = $this->cenario->geraCodigoCenario($_POST['ID_CLIENTE']);
			$erros = $this->cenario->validate($_POST);
		}

		if(empty($erros)){
			// salva o cenario
			$id = $this->cenario->save($_POST, $id);
	
			if(count($fichas) > 0){
				$this->cenario->atulizaFichas($id, $fichas);
			}
			
			// redirect para a pauta de cenario
			redirect('cenario/listar/');
		}
		else{
			// se chegou aqui, eh porque nao salvou
			$this->data['erros'] = $erros;
			$this->form($id);
		}
	}

	function listar($pagina = 0) {
		$user = Sessao::get('usuario');

		if(!empty($_POST)){
			Sessao::set('busca', $_POST);
		}

		$busca = (array) Sessao::get('busca');

		$user = Sessao::get('usuario');

		if(empty($busca['ORDER'])){
			$busca['ORDER'] = 'COD_CENARIO';
		}

		if(empty($busca['ORDER_DIRECTION'])){
			$busca['ORDER_DIRECTION'] = 'DESC';
		}
	
		if(!empty($user['ID_CLIENTE'])){
			$busca['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}

		$this->loadFilters($busca);
		
		$limit = empty($busca['pagina']) ? 10 : $busca['pagina'];

		$cenarios = $this->cenario->listItems($busca,$pagina,$limit,$busca['ORDER'],$busca['ORDER_DIRECTION']);

		$this->data['busca'] = $busca;
		$this->data['podeEditar'] = Sessao::hasPermission('cenario','form');
		$this->data['campos'] = $cenarios['data'];
		$this->data['total'] = $cenarios['total'];
		$this->data['paginacao'] = linkpagination($cenarios, $limit);

		$this->load->view('ROOT/cenario/index', $this->data);
	}
	
	/**
	 * Formulario de edicao do cenario
	 * @param int $id id do cenario
	 * @return void
	 */
	function form($id = null){
		if(!empty($id)){
			$this->checaPermissaoAlterar($id);
		}
		
		$fichas = array();
		$objeto = array();
		$filters = array();

		$user = Sessao::get('usuario');

		$filters = $_POST;

		if(!empty($user['ID_AGENCIA'])){
			$filters['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}

		if(!empty($user['ID_CLIENTE'])){
			$filters['ID_CLIENTE'] = $user['ID_CLIENTE'];
			$_POST['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}

		if(!is_null($id) && $_SERVER['REQUEST_METHOD'] == 'GET'){
			$_POST = $this->cenario->getById($id);

			$_POST['DT_CRIACAO_CENARIO'] = format_date_to_form($_POST['DT_CRIACAO_CENARIO']);
			$_POST['DT_INICIO_CENARIO'] = format_date_to_form($_POST['DT_INICIO_CENARIO']);
			$_POST['DT_VALIDA_CENARIO'] = format_date_to_form($_POST['DT_VALIDA_CENARIO']);

			$filters = $_POST;

			// fichas relacionadas ao cenário
			$fichas = $this->cenario->getFichasByIdCenario($id);

			// campos das fichas			
			for ( $i = 0; $i <= count($fichas) -1; $i++ ) {
				$fichas[$i]['CAMPOS'] = array();
				$lista = $this->campo_ficha->getAllCampoFicha( $fichas[$i]['ID_FICHA'] );
				if ( !empty($lista) ) {
					foreach ( $lista as $item ) {
						$fichas[$i]['CAMPOS'][$item['LABEL_CAMPO_FICHA']] = $item['VALOR_CAMPO_FICHA'];
					}
				}
			}

			// objeto do cenario
			$_POST['objetos'] = array();
			if( isset($_POST['ID_OBJETO']) && is_numeric($_POST['ID_OBJETO']) && ($_POST['ID_OBJETO'] > 0) ){
				$objeto = $this->objeto->getById($_POST['ID_OBJETO']);
				$_POST['objetos'][0] = $objeto;
			}
		}

		$this->loadFilters($filters);

		$this->data['podeSalvar'] = Sessao::hasPermission('cenario','salvar') || $id == null;
		$this->data['cenario'] = $_POST;
		$this->data['fichas'] = $fichas;

		$this->load->view('ROOT/cenario/form', $this->data);
	}
	
	/**
	 * Exibe o formulario de pesquisa de objetos
	 *
	 * @author Sidnei Tertuliano Junior
	 * @return void
	 */
	public function form_pesquisa(){
		$user = Sessao::get('usuario');

		$filters = array();
		$filters['ID_CLIENTE'] = $user['ID_CLIENTE'];

		$this->loadFilters($filters);

		$this->load->view('ROOT/cenario/form_pesquisa', $this->data);
	}
	
	/**
	 * Exibe o formulario de pesquisa de fichas
	 *
	 * @author Sidnei Tertuliano Junior
	 * @return void
	 */
	public function form_pesquisa_fichas($idjob=null){
		$user = Sessao::get('usuario');

		$filters = array();
		$this->setSessionData($filters);
		$filters['ID_CLIENTE'] = $user['ID_CLIENTE'];

		$this->loadFilters($filters);

		$this->assign('categorias', getCategoriasUsuario($user['ID_CLIENTE'],false));
		$this->assign('cliente', $this->cliente->getById($user['ID_CLIENTE']));

		$this->assign('busca', $filters);
		$this->display('form_pesquisa_fichas');
	}
	
	/**
	 * Carrega os filtros para exibir na pagina
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data
	 * @return void
	 */
	private function loadFilters($data){
		$user = Sessao::get('usuario');

		$this->data['clientes'] = array();

		if( !empty($user['ID_CLIENTE']) ){
			$data['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}

		// recupera as agencias
		$rs = $this->segmento->getAll(0,10000,'DESC_SEGMENTO');
		$this->data['segmentos'] = $rs['data'];

		// recupera as agencias
		$rs = $this->fabricante->getAll(0,10000,'DESC_FABRICANTE');
		$this->data['fabricantes'] = $rs['data'];

		// marcas
		if(!empty($data['ID_FABRICANTE'])){
			$this->data['marcas'] = $this->marca->getByFabricante($data['ID_FABRICANTE']);
		}

		////////////////////////////////////////////////////////////////
		////////////////// LISTAGEM DE CLIENTES ////////////////////////
		////////////////////////////////////////////////////////////////
		// se for uma agencia logada
		if( !empty($user['ID_AGENCIA']) ){
			$this->data['clientes'] = $this->cliente->getByAgencia($user['ID_AGENCIA']);

		// se for um cliente logado
		} else if( !empty($user['ID_CLIENTE']) ) {
			$item = $this->cliente->getById($user['ID_CLIENTE']);
			$this->data['clientes'] = array($item);
			$this->data['ID_CLIENTE'] = $user['ID_CLIENTE'];

		// se nao for nenhum dos dois
		} else {
			$list = $this->cliente->listItems($data, 0, 1000000);
			$this->data['clientes'] = $list['data'];
		}
		
		if(!empty($data['ID_CLIENTE'])){
			$this->data['tipos'] = $this->tipo_objeto->getByCliente($data['ID_CLIENTE']);
		}
	}
	
	/**
	 * Checa se o usuario logado tem permissao ou nao de alterar o job acessado
	 *
	 * Se nao tiver permissao, redireciona para a listagem
	 *
	 * @author Sidnei Tertuliano Junior
	 * @param int $idcenario
	 * @return void
	 */
	protected function checaPermissaoAlterar($idCenario){
		$user = Sessao::get('usuario');
		$cenario = $this->cenario->getById($idCenario);

		$categorias = getCategoriasUsuario($cenario['ID_CLIENTE'],true);
		$goto = 'cenario/listar';

		if(!empty($user['ID_CLIENTE'])){
			if($cenario['ID_CLIENTE'] != $user['ID_CLIENTE']){
				redirect($goto);
			}
		}

		if(!empty($user['ID_AGENCIA'])){
			$clientes = getValoresChave($this->cliente->getByAgencia($user['ID_AGENCIA']),'ID_CLIENTE');

			if(!in_array($cenario['ID_CLIENTE'],$clientes)){
				redirect($goto);
			}
		}
	}
}
