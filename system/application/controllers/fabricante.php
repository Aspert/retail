<?php
/**
 * Controller para gerenciar os dados dos fabricantes
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class Fabricante extends MY_Controller {
	
	/**
	 * Construtor
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Fabricante
	 */
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/fabricante/';
	}
	
	/**
	 * Lista os fabricantes cadastrados
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $pagina Numero da pagina atual
	 * @return void
	 */
	function lista($pagina = 0) {
		if ( !empty($_POST) )  {
			Sessao::set('busca',$_POST);
		}
		
		$busca = (array) Sessao::get('busca');
		
		$limit = !empty($busca['pagina']) ? $busca['pagina'] : '';
		$fabricantes = $this->fabricante->listItems($busca, $pagina,$limit);
		
		$this->data['podeAlterar'] = Sessao::hasPermission('fabricante','save');
		$this->data['busca'] = $busca;		
		$this->data['fabricantes'] = $fabricantes['data'];		
		$this->data['paginacao'] = linkpagination($fabricantes, $limit);
		
		$this->display('index');			
	}	
	
	/**
	 * Exibe o formulario de edicao
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do fabricante quando editando
	 * @return void
	 */
	function form($id=null) {
		if($id!=null && $_SERVER['REQUEST_METHOD'] == 'GET'){
			$_POST = $this->fabricante->getById($id);
		}
		$this->display('form');
	}
	
	/**
	 * Persiste os dados de um fabricante
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do fabricante 
	 * @return void
	 */
	function save($id=null) {
		$erros = $this->fabricante->validate($_POST);
		
		if(empty($erros)){
			$this->fabricante->save($_POST,$id);
			redirect('fabricante/lista');
		}
		
		$_REQUEST['erros'] = $erros;
		$this->data['erros'] = $erros;
		$this->form($id);
	}		
} 


