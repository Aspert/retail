<?php
/**
 * controller para gerenciar o cadastro de functions
 * 
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class Functions extends MY_Controller
{
	/**
	 * Construtor
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Functions
	 */
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/functions/';
	}
	
	/**
	 * Lista as functions cadastradas
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $pagina Pagina atual de listagem
	 * @return void
	 */
	function lista($pagina=0) {
		
		if(!empty($_POST)){
			Sessao::set('busca', $_POST);
		}
		
		$dados = (array) Sessao::get('busca');
		
		$limit = empty($dados['pagina']) ? 0 : $dados['pagina'];
		$lista = $this->function->listItems($dados, $pagina, $limit);
		
		$contr = $this->controller->getAll(0, 1000, 'DESC_CONTROLLER');
		
		$this->data['busca'] = $dados;
		$this->data['lista'] = $lista['data'];
		$this->data['controllers'] = $contr['data'];
		$this->data['podeAlterar'] = Sessao::hasPermission('functions','save');
		$this->data['podeRemover'] = Sessao::hasPermission('functions','remover');
		$this->data['paginacao'] = linkpagination($lista, $limit);
		
		$this->display('index');	
	}
	
	/**
	 * Exibe o formulario de edicao
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo da function quando estiver editando
	 * @return void
	 */
	function form($id=null) {
		
		if( !is_null($id) && $_SERVER['REQUEST_METHOD'] == 'GET' ){
			$_POST = $this->function->getById($id);
		}

		$contr = $this->controller->getAll(0, 1000, 'DESC_CONTROLLER');
		$this->data['controllers'] = $contr['data'];
		$this->display('form');
	}
	
	/**
	 * Persiste os dados informados no formulario no banco
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo da functio quando estiver editando
	 * @return void
	 */
	function save($id=null) {
		$erros = $this->function->validate($_POST);
		
		if( empty($erros) ) {
			$id = $this->function->save($_POST,$id);
			redirect('functions/lista');
		}
		
		$this->data['erros'] = $erros;
		$this->form($id);
	}
	
	/**
	 * Remove uma function
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo da function
	 * @return void
	 */
	public function remover($id){
		$this->function->remove($id);
		redirect('functions/lista');
	}
}
