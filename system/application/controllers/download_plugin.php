<?php
	class Download_plugin extends MY_Controller
	{		
		function Download_plugin()
		{
			parent::Controller();
		}
		function index()
		{

		}

		function getFile($fileId=null, $fpoDB=null)
		{
			if( $fileId != null && $fpoDB != null ){
				$db_xinet = $this->load->database('xinet', TRUE);
				$db_xinet->where('FileID', $fileId);

				if( $result = $db_xinet->get($fpoDB)->result_array() ){
					$nome = "givas.eps";
					header("Cache-Control: public");
					header("Content-type: application/x-eps");
					header("Content-Transfer-Encoding: binary\n");
					header("Content-Disposition: attachment;filename=\"$nome\"");
					header("Accept-Ranges: bytes");

					print $result[0]['Data'];
				}
			}
		} 

		private function nome_arquivo($nome){
		    $arrNome = explode('.', $nome);

		    if( sizeof($arrNome) > 1 ){
			    $out = "";
			    for($i = 0; $i < sizeof($arrNome)-1; $i++ ){
			        $out .= $arrNome[$i] . '.';
			    }
			    return $out;
		    }else{
		    	return $nome . '.';
		    }
		}

		private function escreve_arquivo($conteudo){
				$filename = '/tmp/retail.log';
				$handle = fopen($filename, 'a');
				fwrite($handle, "[".date('Y-m-d H:i:s')."] " . $conteudo);
				fwrite($handle, chr(10).chr(13) );
				fwrite($handle, chr(10).chr(13) );
				fclose($handle);
		}

		function getFile1()
		{
			header("Cache-Control: public");
			header("Content-type: application/x-eps");
			header("Content-Transfer-Encoding: binary\n");
			header("Accept-Ranges: bytes");
				
			if( !empty($_REQUEST["filepath"]) && !empty($_REQUEST["filename"]) ){
				$filePath =(($_REQUEST["filepath"]) );
				$fileName =(( $_REQUEST["filename"] ));
				
				$filePath = substr($filePath, 0, strlen($filePath)-1);
				
				$nome = $this->nome_arquivo($fileName) . "eps";

				header("Content-Disposition: attachment;filename=\"$nome\"");

				$db_xinet = $this->load->database('xinet', TRUE);
				$sql = "SELECT FileID, UnixName, concat('fpodata_',right(concat('000',FileID % 256),3)) as targettable
                                FROM file  f
                                JOIN path p USING(PathID)
                                WHERE p.path = '{$filePath}' AND f.UnixName = '{$fileName}';
                                ";

				$result = $db_xinet->query($sql)->result_array();
				
				if( !empty( $result ) ){
					$db_xinet->where('FileID', $result[0]['FileID']);
					if( $result = $db_xinet->get( str_replace("_000", "", $result[0]['targettable']))->result_array() ){
						print $result[0]['Data'];
					}else{
						$fileFull = "$filePath/$fileName";
						if(is_file($fileFull)){
							//se existe o arquivo em alta utiliza
							print file_get_contents($fileFull);
							//$this->email->sendDebugEmail(print_r($_REQUEST, true));
						}else{
							//se nao, pega o sem_fpo
							print file_get_contents(base_url(). "img/sem_fpo.eps" );
							//$this->email->sendDebugEmail(print_r($_REQUEST, true));
						}
					}
				}else{
					header("Content-Disposition: attachment;filename=\"$nome\"");
					print file_get_contents( base_url(). "img/sem_fpo.eps" );
					//$this->email->sendDebugEmail(print_r($_REQUEST, true));
				}
			}
			else{
				$nome = "sem_fpo.eps";
				header("Content-Disposition: attachment;filename=\"$nome\"");
				print file_get_contents( base_url(). "img/sem_fpo.eps" );
				//$this->email->sendDebugEmail(print_r($_REQUEST, true));
			}
		}

		function getLayout()
		{
			$idTemplate = utf8MAC( utf8_encode($_REQUEST["idtemplate"]) );
			$paginaInicio = utf8MAC( utf8_encode( $_REQUEST["paginainicio"] ));
			$paginaFim = utf8MAC( utf8_encode( $_REQUEST["paginafim"] ));

			$fileName= $this->templateItem->getFilenameByTemplateRange($idTemplate, $paginaInicio, $paginaFim);
			$filePath = $this->template->getPath($idTemplate);
			$filePath = str_replace('//', '/', $filePath);

			if(substr($filePath,  strlen($filePath) - 1 ,1) == "/"){
				$filePath = substr($filePath, 0, -1);
			}

			header("Cache-Control: public");
			header("Content-type: application/x-eps");
			header("Content-Transfer-Encoding: binary\n");
			header("Accept-Ranges: bytes");
			header("Content-Disposition: attachment;filename=teste.indd");

			$db_xinet = $this->load->database('xinet', TRUE);
			$sql = "SELECT FileID, UnixName, concat('fpodata_',right(concat('000',FileID % 256),3)) as targettable
							FROM file  f
							JOIN path p USING(PathID)
							WHERE p.path = '{$filePath}' AND f.UnixName = '{$fileName}';
							";
			
			$result = $db_xinet->query($sql)->result_array();

			if( !empty( $result ) ){
				$db_xinet->where('FileID', $result[0]['FileID']);
				if( $result = $db_xinet->get( str_replace("_000", "", $result[0]['targettable']))->result_array() ){
					print $result[0]['Data'];
				}else{
					$fileFull = "$filePath/$fileName";
					if(is_file($fileFull)){
						//se existe o arquivo em alta utiliza
						print file_get_contents($fileFull);
					}
				}
			}
		}

		function getLayoutSize($idTemplate, $paginaInicio, $paginaFim)
		{
			$fileName= $this->templateItem->getFilenameByTemplateRange($idTemplate, $paginaInicio, $paginaFim);
			$filePath = $this->template->getPath($idTemplate);
			$filePath = str_replace('//', '/', $filePath);
			
			if(substr($filePath,  strlen($filePath) - 1 ,1) == "/"){
				$filePath = substr($filePath, 0, -1);
			}

			$db_xinet = $this->load->database('xinet', TRUE);
			$sql = "SELECT FileSize, FileID, UnixName, concat('fpodata_',right(concat('000',FileID % 256),3)) as targettable
							FROM file  f
							JOIN path p USING(PathID)
							WHERE p.path = '{$filePath}' AND f.UnixName = '{$fileName}';
							";

			$result = $db_xinet->query($sql)->result_array();

			if( !empty( $result ) ){
				if(is_numeric($result[0]['FileSize'])){
					echo number_format(($result[0]['FileSize'] / 1024 / 1024), 2, '.', '');
				}
			}
		}

		function getLayoutData($idTemplate, $paginaInicio, $paginaFim)
		{
			$sql = "select DATA_CADASTRO from TB_TEMPLATE_ITEM
						where ID_TEMPLATE = " .$idTemplate. "
						and PAGINA_INICIO_TEMPLATE_ITEM = " .$paginaInicio. "
						and PAGINA_FIM_TEMPLATE_ITEM = " .$paginaFim. ";";
			$result = $this->db->query($sql)->result_array();
			
			if( !empty( $result ) ){
				echo $result[0]['DATA_CADASTRO'];
			}
		}
		
		function getPathTemplate($idTemplate){
			echo $this->template->getPath($idTemplate);
		}
		
		public function login($usuario, $senha, $coordenadaCartao, $valorCartao){	
			echo Sessao::getInstance()->login_plugin($usuario, $senha, $coordenadaCartao, $valorCartao);
		}
		
		public function geraXmlPrimeiroIndd($idUsuario){
			$busca = $this->usuario->getById($idUsuario);
			
			$produtos = getProdutosUsuario($busca['ID_CLIENTE'], $busca['ID_AGENCIA']);
			$busca['PRODUTOS'] = getValoresChave($produtos, 'ID_PRODUTO');
			
			$busca['ID_ETAPA'] = array($this->etapa->getIdByKey('ENVIADO_AGENCIA'), $this->etapa->getIdByKey('APROVACAO'));
			
			$busca['STATUS_CAMPANHA'] = 1;

			$jobs = $this->excel->listItems($busca, 0, 100000);
			$jobs = $jobs['data'];

			for($i = count($jobs) - 1; $i >= 0; $i--){
				if(isset($jobs[$i])){
					$filtro = array();
					$filtro = array('ID_CAMPANHA' => $jobs[$i]['ID_CAMPANHA']);
					$filtro['TEMPLATE_EXISTS']  = 1;
					$filtro['STATUS_TEMPLATE']  = 1;
					$filtro['NUMERO_PAGINAS']   = $jobs[$i]['PAGINAS_JOB'];
					$filtro['ALTURA_TEMPLATE']  = $jobs[$i]['ALTURA_JOB'];
					$filtro['LARGURA_TEMPLATE'] = $jobs[$i]['LARGURA_JOB'];

					$jobs[$i] = limpaArray($jobs[$i], array('ID_EXCEL'=>0, 'ID_JOB'=>0, 'TITULO_JOB'=>0, 'PAGINAS_JOB'=>0));

					$versaoExcel = $this->excelVersao->getVersaoAtual($jobs[$i]['ID_EXCEL']);
					$jobs[$i]['PRACAS'] = $this->excel->getPracas($jobs[$i]['ID_EXCEL'], $versaoExcel['ID_EXCEL_VERSAO']);
					for($w = count($jobs[$i]['PRACAS']) - 1; $w >= 0; $w--){
						$jobs[$i]['PRACAS'][$w] = limpaArray($jobs[$i]['PRACAS'][$w], array('ID_PRACA'=>0, 'DESC_PRACA'=>0));
					}
				}
			}

			if(count($jobs) > 0){
				$xml = new DOMDocument('1.0','UTF-8');
				$root = $xml->createElement('Root');
				
				foreach($jobs as $j){
					$jobEl = $xml->createElement("job");
					$jobEl->setAttribute("id_job",$j["ID_JOB"]);				
					$jobEl->setAttribute("id_excel",$j["ID_EXCEL"]);
					$jobEl->setAttribute("titulo_job",$j["TITULO_JOB"]);
					$jobEl->setAttribute("paginas_job",$j["PAGINAS_JOB"]);
					if(count($j['PRACAS']) > 0){
						foreach($j['PRACAS'] as $p){
							$pracaEl = $xml->createElement("praca");
							$pracaEl->setAttribute('id_praca', $p['ID_PRACA']);
							$pracaEl->setAttribute('desc_praca', $p['DESC_PRACA']);						
							$jobEl->appendChild($pracaEl);
						}
					}
					$root->appendChild($jobEl);
				}
				
				$xml->appendChild($root);
				
				
				$fileXml = 'files/xml/inicial_indd.xml';
				$xml->save($fileXml);
				echo file_get_contents($fileXml);
				unlink($fileXml);
			}
		}		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}