<?php

/**
 * Controller de itens em paginacao
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 */
class Paginacao extends MY_Controller{
	
	/**
	 * Construtor
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Paginacao
	 */
	public function __construct(){
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/paginacao/';
	}
	
	/**
	 * Lista os jobs cadastrados
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $pg
	 * @return void
	 */
	public function index($pg = 0){
		// se enviou dados
		if(!empty($_POST)){
			// altera a sessao
			Sessao::set('busca', $_POST);
		}
		
		$dados = (array) Sessao::get('busca');
		$dados['STATUS_CAMPANHA'] = 1;
		
		$this->setSessionData( $dados );
		
		// somente os desta etapa
		$dados['ID_ETAPA'] = $this->etapa->getIdByKey('PAGINACAO');
		$dados['PRODUTOS'] = getValoresChave(getProdutosUsuario(),'ID_PRODUTO');
		
		if(empty($dados['ORDER'])){
			$dados['ORDER'] = 'DATA_INICIO';
		}
		if(empty($dados['ORDER_DIRECTION'])){
			$dados['ORDER_DIRECTION'] = 'DESC';
		}
		
		//Retira os bloqueados
		$dados['NOT_ID_STATUS'] = array ($this->status->getIdByKey('BLOQUEADO'));
		
		$limit = empty($dados['pagina']) ? 0 : $dados['pagina'];
		$lista = $this->job->listItems($dados, $pg, $limit, $dados['ORDER'], $dados['ORDER_DIRECTION']);
		
		$this->loadFilters($dados);
		
		$this->data['podeAlterar'] = Sessao::hasPermission('paginacao','save');
		$this->data['busca'] = $dados;
		$this->data['lista'] = $lista['data'];
		$this->data['paginacao'] = linkpagination($lista, $limit);
		
		$this->display('index');
	}
	
	/**
	 * Exibe o formulario de edicao da paginacao
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id
	 * @return void
	 */
	public function form($id = null){
		$this->checaPermissaoAlterar($id);
		
		$pracas = $this->job->getPracas($id);
		
		$this->assign('podePassarEtapa', Sessao::hasPermission('paginacao','proxima_etapa'));
		$this->assign('podeVoltarEtapa', Sessao::hasPermission('paginacao','voltar_etapa'));
		$this->assign('pracas', $pracas);
		$this->assign('job', $this->job->getById($id));
		$this->assign('plano_midia', $this->job->getPlanoMidia($id));

		//Verifica se foi uma copia de paginacao
		$abaCopiada = $this->session->flashdata('aba_copiada'); 
		
		if (!empty($abaCopiada)) {
			$this->assign('abaCopiada', $abaCopiada);
		}
		
		$this->display('form');
		
	}
	
	/**
	 * Copiar paginacao
	 * 
	 * @author Esdras Eduardo
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function copiar_paginacao(){
		
		$to = $_REQUEST['to'];
		$from = $_REQUEST['from'];
		$idjob = $_REQUEST['idjob'];
		
		$model = new ExcelItemDB();
		
		//array com itens da praca destino
		$arrToItens = $model->getByJobPraca($idjob, $to);
		
		//array com itens da praca origem
		$arrFromItens = $model->getByJobPraca($idjob, $from);

		if( count($arrToItens) > 0 ){
			//Deixa o array apenas com o ID_FICHA
			$arrToItens = array_map('PegarChave', $arrToItens, array_fill(0, count($arrToItens), 'ID_FICHA'));
		}
		
		if( count($arrFromItens) > 0 ){
			//Deixa o array apenas com o ID_FICHA
			$arrFromItens = array_map('PegarChave', $arrFromItens, array_fill(0, count($arrFromItens), 'ID_FICHA'));
		}
		
		//monta um array com as fichas (ID_FICHA) comuns entre a praca origem e a praca destino 
		$arraySemelhancas = array_intersect($arrFromItens, $arrToItens);
		
		//monta um array com as fichas (ID_FICHA) que só existem em uma das pracas  
		$diferencaMenos = array_diff($arrToItens, $arrFromItens);
		
		//array com as fichas que ficarão em destaque na tela. fichas da praca destino que estiverem com pagina/ordem diferente da praca origem 
		$retorno = Array();
		
		//percorre os itens da praca origem
		foreach( $arrFromItens as $idItem ){
			
			// efetua a copia da pagina/ordem do item da praca origem para a praca destino,
			// somente se o item existir na praca destino e a pagina/ordem for diferente da praca origem
			if( $model->copiarPaginacao( $idjob, $to, $from, $idItem ) ){
				
				// percorre o array das fichas comuns das pracas origem e destino
				foreach ($arraySemelhancas as $semelhanca) {
					
					//se a ficha da praca origem que foi copiada existir no array das fichas comuns 
					if ($semelhanca == $idItem) {
						
						//Coloca o item no array das fichas que ficarão em destaque da praca destino. Por estar com pagina/ordem diferente da praca origem
						$retorno[] = $idItem;
					}
				}
			}
		}
		
		//Limpa a paginacao da praca destino dos itens que não existem na praca origem
		foreach( $diferencaMenos as $idItem ){		
			$model->zeraPaginacaoByPraca($idjob, $to, $idItem);
		}
		
		$flash_destaque = Array();
		$flash_destaque[$idjob] = Array();
		$flash_destaque[$idjob][$to] = $retorno;
		
		$this->session->set_userdata('destaque_paginacao', json_encode($flash_destaque));
		$this->session->set_flashdata('aba_copiada', $to);
		
		echo(json_encode($retorno));
	}
	
	/**
	 * Salva as alteracoes da paginacao
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id
	 * @return void
	 */
	public function save($id = null){
		$this->checaPermissaoAlterar($id);
		
		//$xls = $this->excel->getByJob($id);
		//$this->excelItem->zeraPaginacao($xls['ID_EXCEL']);
		
		$this->form($id);
	}
	
	/**
	 * Envia um job para proxima etapa
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do job
	 * @return void
	 */
	public function proxima_etapa($id){
		
		$errosMudaEtapa = array ();
		
		$this->checaPermissaoAlterar($id);
		
		$this->save($id);
		
		$errosMudaEtapa = $this->job->mudaEtapa($id, Sessao::get('usuario'));
		
		if(!empty($errosMudaEtapa)){
			$this->assign('erros', $errosMudaEtapa );
			$this->form($id);
		}else{
			redirect('paginacao/index');
		}		
	}
	
	/**
	 * 
	 * @param $id
	 * @param $idpraca
	 * @return unknown_type
	 */
	public function form_pesquisa($id=0, $idpraca=0){
		$offset = post('offset',true);
		$limit  = post('limit',true);
		
		$filtros = array();
		$filtros['I.ORDEM_ITEM'] = 0;
		$filtros['I.PAGINA_ITEM'] = 0;
		
		if(post('nome',true) != '') $filtros['F.NOME_FICHA'] = post('nome',true);
		if(post('codigo',true) != '') $filtros['F.COD_BARRAS_FICHA'] = post('codigo',true);
		if(post('codigo_regional',true) != '') $filtros['FC.CODIGO'] = post('codigo_regional',true);
		
		$itens = $this->excel->getItensForJobPraca($id,$idpraca,$offset,$limit,$filtros);
				
		for( $i = 0; $i <= count($itens) -1; $i++ ){
			$itens[$i]['COR_CENARIO'] = $this->cenario->pegarCor( $itens[$i]['ID_CENARIO'] );
			$itens[$i]['SUBFICHAS'] = $this->subficha->getByFichaPrincipal( $itens[$i]['ID_FICHA']);
		}

		$this->assign('total',  $this->excel->countItensForJobPraca($id,$idpraca,$filtros));
		$this->assign('itens',  $itens);
		$this->assign('praca',  $this->praca->getById($idpraca));
		$this->assign('offset', $offset);
		$this->assign('limit',  $limit);

		$this->display('form_pesquisa');
	}
	
	/**
	 * carrega os filtros padrao
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data Dados para indicar quais filtros usar
	 * @return void
	 */
	protected function loadFilters($data = null){
		
		$user = Sessao::get('usuario');
		
		if(!empty($user['ID_PRODUTO']) ){
			$data['ID_PRODUTO'] = $user['ID_PRODUTO'];
		}
		
		if(!empty($user['ID_AGENCIA']) ){
			$data['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}
		
		if(!empty($user['ID_CLIENTE']) ){
			$data['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}
		
		if(!empty($data['ID_AGENCIA'])){
			$this->assign('clientes', $this->cliente->getByAgencia($data['ID_AGENCIA']));
		} else {
			$this->assign('clientes', array());
		}
		
		if(!empty($data['ID_CLIENTE'])){
			$this->assign('agencias', $this->agencia->getByCliente($data['ID_CLIENTE']));
		} else {
			$this->assign('agencias', array());
		}
		
		if(!empty($data['ID_PRODUTO'])){
			$filters = array(
				'ID_PRODUTO' => $data['ID_PRODUTO'],
				'STATUS_CAMPANHA' => 1
			);
			$campanha = $this->campanha->listItems($filters,0,1000);
			$this->data['campanhas'] = $campanha['data'];
			
		} else {
			$this->data['campanhas'] = array();
		}
		
		$this->assign('produtos',array());
		if( !empty($data['ID_CLIENTE']) ){
			$this->assign('produtos', getProdutosUsuario(val($data,'ID_CLIENTE'), val($data,'ID_AGENCIA')));
		}
		
	}

	
	/**
	 * Checa se o usuario logado tem permissao ou nao de alterar o job acessado
	 * 
	 * Se nao tiver permissao, redireciona para a listagem
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idjob
	 * @return void
	 */
	protected function checaPermissaoAlterar($idjob){
		
		$user = Sessao::get('usuario');
		$job = $this->job->getById($idjob);
		$produtos = getValoresChave(getProdutosUsuario($job['ID_CLIENTE'],$job['ID_AGENCIA']),'ID_PRODUTO');
		$goto = 'paginacao/index';
		
		//Verifica se o job nao esta bloqueado
		if ( $job['ID_STATUS'] == $this->status->getIdByKey('BLOQUEADO') ) {
			redirect($goto);
		}
		
		//Verifica se esta na etapa correta
		if( $job['ID_ETAPA'] != $this->etapa->getIdByKey('PAGINACAO') ){
			redirect($goto);
		}
		
		//Verifica se o cliente do usuario eh o mesmo cliente do job
		if(!empty($user['ID_CLIENTE'])){
			if($job['ID_CLIENTE'] != $user['ID_CLIENTE']){
				redirect($goto);
			}
			
			//Verifica se o produto do job esta na lista permitida para o cliente x agencia
			if(!in_array($job['ID_PRODUTO'],$produtos)){
				redirect($goto);
			}
		}
		
		//Verifica se a agencia do usuario eh a mesma agencia do job
		if(!empty($user['ID_AGENCIA'])){
			if($user['ID_AGENCIA'] != $job['ID_AGENCIA']){
				redirect($goto);
			}
			
			$clientes = getValoresChave($this->cliente->getByAgencia($user['ID_AGENCIA']),'ID_CLIENTE');
			if(!in_array($job['ID_PRODUTO'],$produtos)){
				redirect($goto);
			}
			
			if(!in_array($job['ID_CLIENTE'],$clientes)){
				redirect($goto);
			}
		}
	}
	
	public function subfichas($idExcelItem = null){
		$subfichas = array();
		$ei = $this->excelItem->getById($idExcelItem);
		$ficha = $this->ficha->getById($ei['ID_FICHA']);
		$eiSubs = $this->excelItem->getSubfichasByVersaoPraca($ei['ID_EXCEL_VERSAO'], $ei['ID_PRACA'], $ei['ID_FICHA']);

		foreach($eiSubs as $s){
			$subfichas[] = $this->ficha->getById($s['ID_FICHA']);
		}

		$this->assign('ficha',  $ficha);
		$this->assign('subfichas',  $subfichas);
		
		$this->display('subfichas');
	}
	
}
