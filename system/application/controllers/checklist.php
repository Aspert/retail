<?php

/**
 * Controller de itens em checklist
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 */
class Checklist extends MY_Controller{

    /**
     * Construtor
     *
     * @author Hugo Ferreira da Silva
     * @link http://www.247id.com.br
     * @return Checklist
     */
    public function __construct(){
        parent::__construct();

        $this->_templatesBasePath = 'ROOT/checklist/';
    }

    /**
     * Lista os jobs cadastrados
     *
     * @author Hugo Ferreira da Silva
     * @link http://www.247id.com.br
     * @param int $pg
     * @return void
     */
    public function index($pg = 0){

        if(isset($_SESSION['ID_JOB'])){
            unset($_SESSION['ID_JOB']);
        } else if(isset($_SESSION['idJob'])){
            unset($_SESSION['idJob']);
        } else if(isset($_SESSION['relatorioObjetoPendente'])){
            unset($_SESSION['relatorioObjetoPendente']);
        }

        // se enviou dados
        if(!empty($_POST)){
            // altera a sessao
            Sessao::set('busca', $_POST);
        }

        $user = Sessao::get('usuario');

        $dados = (array) Sessao::get('busca');
        $dados['STATUS_CAMPANHA'] = 1;
        $dados['PRODUTOS'] = getValoresChave(getProdutosUsuario(),'ID_PRODUTO');

        $this->setSessionData( $dados );

        if( !empty($user['ID_AGENCIA']) ){
            // se for agencia, so pode ver jobs enviados para agencia
            $dados['ID_ETAPA'] = array($this->etapa->getIdByKey('ENVIADO_AGENCIA'),
                                       $this->etapa->getIdByKey('APROVACAO'),
                                       $this->etapa->getIdByKey('FINALIZADO'));
        }

        if(empty($dados['ORDER'])){
            $dados['ORDER'] = 'DATA_INICIO';
        }

        if(empty($dados['ORDER_DIRECTION'])){
            $dados['ORDER_DIRECTION'] = 'DESC';
        }

        $verTodos = Sessao::hasPermission('checklist','ver_todos');
        $limit = empty($dados['pagina']) ? 0 : $dados['pagina'];

        $lista = $this->job->listItems($dados, $pg, $limit, $dados['ORDER'], $dados['ORDER_DIRECTION'],$verTodos);

        $this->loadFilters($dados);

        $this->data['podeExportarJob'] = Sessao::hasPermission('checklist','exportarJobExcel');
        $this->data['podeDesbloquear'] = Sessao::hasPermission('checklist','desbloquear');
        $this->data['podeExcluir'] = Sessao::hasPermission('checklist','delete');
        $this->data['podeAlterar'] = Sessao::hasPermission('checklist','aprovacao');
        $this->data['podeEnviarEtapa'] = Sessao::hasPermission('checklist','enviar_etapa');
        /*$this->data['podeVoltarEtapa'] = Sessao::hasPermission('checklist','voltar_etapa');*/
        $this->data['busca'] = $dados;
        /*$listaJob = $this->jobUsuario->viewJobUsuario($lista['data'],$verTodos);
        $listaJob['total'] = count($listaJob);*/

        $this->data['lista'] = $lista['data'];
        /*$this->data['lista'] = $listaJob;*/
        /*echo '<pre>';
        var_dump($limit);
        exit();*/
        $this->data['paginacao'] = linkpagination($lista, $limit);
        $this->display('index');
    }

    /**
     * Exibe o formulario de Checklist
     *
     * @author Hugo Ferreira da Silva
     * @link http://www.247id.com.br
     * @param int $id
     * @return void
     */
    public function form($id = null){

        $this->checaPermissaoAlterar($id);

        $pracas = $this->job->getPracas($id);
        $job = $this->job->getById($id);
        $jobs = $this->job->getJobsToCopy($id,$job['PAGINAS_JOB']);

        $etapasProcesso = $this->processoEtapa->getByProcesso($job['ID_PROCESSO']);
        $users = $this->jobUsuario->getUsers($id);
        $usuariosResponsaveis = "";
        foreach ($users as $u) {
            if ($usuariosResponsaveis != "") $usuariosResponsaveis .= ", ";
            $usuariosResponsaveis .= $u['NOME_USUARIO'];
        }
//		$etapas = $this->etapa->getAll(null,null,'ORDEM_ETAPA');

        // pegando as etapas realizadas
        $realizado = $this->job->getHistoricoEtapas($id);
        //print_rr($job);die;

        $this->assign('etapasProcesso', $etapasProcesso );
        $this->assign('usuariosResponsaveis', $usuariosResponsaveis );
        $this->assign('codigoChecklist', $this->etapa->getIdByKey('CHECKLIST'));
        $this->assign('podeVerCronograma', Sessao::hasPermission('plano_midia','visualizar_cronograma'));
        $this->assign('podeSolicitaAlteracoes', Sessao::hasPermission('checklist','solicita_alteracoes'));
        $this->assign('correcoes', Sessao::hasPermission('checklist','correcoes'));
        $this->assign('pracas', $pracas);
        $this->assign('job', $job);
        $this->assign('jobs', $jobs);
        $this->assign('historico', $this->job->getHistoricoAprovacao($id) );
        $this->assign('historicoPraca', $this->job->getHistoricoPraca($id) );
        $this->assign('etapasRealizadas', $realizado);
        $this->assign('podeEnviarEtapa', Sessao::hasPermission('checklist','enviar_etapa'));
        $this->assign('podeVoltarEtapa', Sessao::hasPermission('checklist','voltar_etapa'));
        $this->assign('podeEditarFicha', Sessao::hasPermission('ficha','form'));
        $this->assign('podeAdicionarProdutos', Sessao::hasPermission('checklist','adicionarProdutos'));
        $this->assign('podeEditarProdutos', Sessao::hasPermission('checklist','editarProdutos'));
        $this->assign('copiarPraca', Sessao::hasPermission('checklist','copiar_praca'));

        $this->display('form');
    }

    /**
     * Exclui um job
     *
     * @param int $idJob
     */
    public function delete($idJob = 0){
        if($idJob == 0){
            $idJob = isset($_POST['idJob']) ? $_POST['idJob'] : $idJob;    
        }
        if(!is_array($idJob)){
            $this->checaPermissaoAlterar($idJob);

            $job = $this->job->getById($idJob);
            $excel = $this->excel->getByJob($idJob);

            //recupera o caminho da pasta da aprovação
            $pathAP = $this->aprovacao->getPathJob($idJob);

            //remove os arquivos das aprovacoes do job e as pastas
            rrmdir($pathAP);

            //exclui os registros associados no cascade
            $this->job->delete($idJob);

            redirect('checklist/index');
        }else{
           

            foreach($idJob as $i){
                $this->checaPermissaoAlterar($i);

                $job = $this->job->getById($i);
                $excel = $this->excel->getByJob($i);

                //recupera o caminho da pasta da aprovação
                $pathAP = $this->aprovacao->getPathJob($i);

                //remove os arquivos das aprovacoes do job e as pastas
                rrmdir($pathAP);

                //exclui os registros associados no cascade
                $this->job->delete($i);
            }

            echo json_encode("Foram deletados ".count($idJob)." Jobs!");
        }
    }

    /**
     * Desbloquear um job
     *
     * @author esdras.filho
     * @param int $idJob
     */
    public function desbloquear($idJob, $delay){

        $arrUpdate = Array();

        //delay selecionado para atualizar o job
        //$delay = post('delay_bloqueio',true);
        if(!empty($delay) && is_numeric($delay)){
            //sendo valido o delay, soma a hora atual e grava no campo
            $arrUpdate['DT_DELAY_BLOQUEIO'] = date('Y-m-d H:i:s', time()+$delay);
        }else{
            //senao redireciona para a pauta
            redirect('checklist/index');
        }

        //Pegar o historico do job para voltar a situacao antiga dele
        $arr_historico = $this->jobHistorico->getHistoricoPeca($idJob, 2, 'DATA_HISTORICO DESC');
        $situacao_antiga = $arr_historico[1]['ID_STATUS'];

        //Atualizar o job para situacao anterior do seu bloqueio
        $arrUpdate['ID_STATUS'] = $situacao_antiga;
        $this->job->save($arrUpdate, $idJob);

        //Salvar historico q o job foi modificado
        $usuario = Sessao::get('usuario');
        $data = array(
            'ID_ETAPA' => $arr_historico[0]['ID_ETAPA'],
            'ID_JOB' => $idJob,
            'DATA_HISTORICO' => date('Y-m-d H:i:s'),
            'ID_USUARIO' => $usuario['ID_USUARIO'],
            'ID_PROCESSO_ETAPA' => $arr_historico[0]['ID_PROCESSO_ETAPA'],
            'ID_STATUS' => $situacao_antiga
        );
        $this->jobHistorico->save($data);

        redirect('checklist/index');
    }

    /**
     * Altera a etapa do Job
     *
     * @author Hugo Ferreira da Silva
     * @param int $idJob
     * @return void
     */
    public function enviar_etapa($idJob = 0){
        if($idJob == 0){
            $idJob = isset($_POST['idJob']) ? $_POST['idJob'] : $idJob;
        }
        $user = Sessao::get('usuario');
        if(!is_array($idJob)){
            $this->checaPermissaoAlterar($idJob);
    
            $this->job->mudaEtapa($idJob, $user);
    
            redirect('checklist/index');
        }else{
            foreach($idJob as $i){
                $this->checaPermissaoAlterar($i);
    
                $this->job->mudaEtapa($i, $user);
            }

            echo json_encode(count($idJob)." Jobs foram atualizados!");

        }
        
    }

    /**
     * Volta a etapa do Job
     *
     * @author Diego Andrade
     * @param int $idJob
     * @return void
     */
    public function voltar_etapa($idJob)
    {
        $user = Sessao::get('usuario');
        $this->checaPermissaoAlterar($idJob);
        $this->job->voltaEtapa($idJob, $user);
        redirect('checklist/index');
    }

    /**
     * Volta a etapa do Job para Pauta quando foi enviado para agência
     *
     * @author Diego Andrade
     * @param int $idJob
     * @return void
     */
    public function correcoes($idJob)
    {
        $user = Sessao::get('usuario');
        $this->checaPermissaoAlterar($idJob);
        $this->job->correcoes($idJob, $user);
        redirect('checklist/index');
    }

    /**
     * Exibe a janela de aprovacao
     *
     * @author Hugo Ferreira da Silva
     * @link http://www.247id.com.br
     * @param int $idjob Codigo do job
     * @param string $status Nome do status que sera exibido
     * @return void
     */
    public function aprovacao($idjob, $status, $popwin = 0){
        $this->checaPermissaoAlterar($idjob);

        if($status == 'MOD_APROVACAO'){
            //quando vier este status especial  redirecionamos o metodo
            //para não ter que dar permissao para mais um metodo
            $this->mod_aprovacao($idjob);
        }


        $job = $this->job->getById($idjob);
        $user = Sessao::get('usuario');

        // se enviou os dados de aprovacao
        if( !empty($_POST['ID_STATUS']) ){
            $job['ID_STATUS'] = $_POST['ID_STATUS'];

            // se esta aprovando
            if($_POST['ID_STATUS'] == $this->status->getIdByKey('ELABORACAO')){
                // manda para a agencia
                $_POST['ID_ETAPA'] = $this->etapa->getIdByKey('ENVIADO_AGENCIA');

                $etapa = $this->etapa->getById($_POST['ID_ETAPA']);

                $job['COMENTARIO'] = post('COMENTARIO_JOB_APROVACAO',true);

                $erros = $this->email->sendEmailRegra('elaboracao_job',
                    'Job enviado para agência para elaboração',
                    'ROOT/templates/aprovacao_job',
                    array(
                        'job'       => $job,
                        'etapa'     => $etapa,
                        'aprovador' => $user
                    ),
                    $job['ID_AGENCIA'],
                    $job['ID_CLIENTE']
                );
            }

            // se escolheu uma nova etapa
            if( !empty($_POST['ID_ETAPA']) ){
                $job['ID_ETAPA'] = $_POST['ID_ETAPA'];
                //$this->job->mudaEtapa($idjob, $job['ID_ETAPA'],$this->status->getIdByKey('EM_APROVACAO'), Sessao::get('usuario'));
                $this->job->mudaEtapa($idjob, Sessao::get('usuario'));
            }

            // se mandou para altearcao
            if( $_POST['CHAVE_STATUS'] == 'EM_ALTERACAO' ){

                $etapa = $this->etapa->getById($_POST['ID_ETAPA']);
                $job['COMENTARIO'] = post('COMENTARIO_JOB_APROVACAO',true);

                $erros = $this->email->sendEmailRegra('solicitacao_alteracoes',
                    'Solicitação de Alteração',
                    'ROOT/templates/solicitacao_alteracoes',
                    array(
                        'job'       => $job,
                        'etapa'     => $etapa,
                        'aprovador' => $user
                    ),
                    $job['ID_AGENCIA'],
                    $job['ID_CLIENTE']
                );

                // indica que o job sofreu alteracoes
                $job['ALTERACOES_JOB']++;
            }

            // atualiza o job
            $this->job->save($job, $job['ID_JOB']);

            // agora gravamos o historico
            $this->job->addHistorico($idjob, $job['ID_STATUS'], $job['ID_ETAPA'], $_POST['COMENTARIO_JOB_APROVACAO']);

            die('ok');
        }


        $etapas = $this->etapa->getItensForAprovacao();


        $this->assign('job', $job);
        $this->assign('status', $this->status->getByKey($status));
        $this->assign('etapas', $etapas);
        $this->assign('historico', $this->job->getHistoricoAprovacao($idjob));
        if($popwin == 0){
            $this->display('popup_aprovacao');
        }else{
            $this->display('popup_aprovacao_ap');
        }
    }

    /**
     * Alterar datas do Job
     *
     * @author esdras.filho
     * @param int $idJob
     */
    public function solicita_alteracoes( $idJob ){

        $job = $this->job->getById($idJob);
        $this->assign('job', $job);
        $this->assign('etapas', $this->processoEtapa->getByProcesso($job['ID_PROCESSO']));
        $this->display('popup_solicita_alteracoes');
    }

    /**
     * Envia da Pauta pauta para o modulo aprovacao - para que o mkt consolide os
     * comentarios antes de enviar para agencia de novo
     * @author juliano.polito
     * @param $idjob
     * @return void
     */
    public function mod_aprovacao($idjob){

        $job = $this->job->getById($idjob);
        $xls = $this->excel->getById($job['ID_EXCEL']);

        $user = Sessao::get('usuario');

        //$this->job->mudaEtapa($idjob, $this->etapa->getIdByKey('APROVACAO'),$this->status->getIdByKey('REVISAO'), Sessao::get('usuario'));
        $this->job->mudaEtapa($idjob, Sessao::get('usuario'));

        redirect('aprovacao/form_cliente/'.$xls['ID_EXCEL']);

    }

    /**
     * Finaliza um Job
     *
     * @author juliano.polito
     * @link http://www.247id.com.br
     * @param int $id
     * @return void
     */
    public function finalizar($idJob = null, $pop = 1){
        $idJob = 0;
        if(isset($idJob) != null){
            $idJob = isset($_POST['idJob']) ? $_POST['idJob'] : $idJob;
        }
        $user = Sessao::get('usuario');
        if(!is_array($idJob)){
            $this->checaPermissaoAlterar($idJob);

            if($pop == 1){
                $this->assign('idJob', $idJob);
                $this->assign('historico', $this->job->getHistoricoAprovacao($idJob));
                $this->display('popup_finalizar');
            }else{
                $job = $this->job->getById($idJob);
                
                // agora gravamos o historico
                $this->job->addHistorico($idjob, $this->status->getIdByKey('APROVADO'), $this->etapa->getIdByKey('FINALIZADO'), $_POST['COMENTARIO_JOB_APROVACAO']);
                
                //aletra a etapa para finalizado
                $this->job->setEtapa($idjob,10);
                // $this->job->mudaEtapa($idjob, $this->etapa->getIdByKey('FINALIZADO'),$this->status->getIdByKey('APROVADO'), Sessao::get('usuario'));
                
                // $this->job->mudaEtapa($idJob, Sessao::get('usuario'));

                echo "ok";
            }
        }else{
            foreach($idJob as $i){
                $job = $this->job->getById($i);
                $this->job->addHistorico($i, $this->status->getIdByKey('APROVADO'), $this->etapa->getIdByKey('FINALIZADO'), 'Finalização Automática');
                
                // $this->job->mudaEtapa($i, $this->etapa->getIdByKey('FINALIZADO'),$this->status->getIdByKey('APROVADO'), Sessao::get('usuario'));
                $this->job->setEtapa($i,10);
                // $this->job->mudaEtapa($i, Sessao::get('usuario'));
            }

            echo json_encode(count($idJob)." Jobs foram Finalizados!");
        }
    }

    /**
     * Salva as alteracoes de Checklist
     *
     * @author Hugo Ferreira da Silva
     * @link http://www.247id.com.br
     * @param int $id
     * @return void
     */
    public function save($id = null){
        $this->checaPermissaoAlterar($id);

        // se foi enviado por post
        if( !empty($_POST['item']) ){

            // coloca o job em andamento novamente
            // $this->job->setStatus($id, $this->status->getIdByKey('EM_APROVACAO'));

            // $this->paginacao->savePaginacao( $_POST['item'] );

        }

        $this->form($id);
    }

    /**
     * Salvar a alteração de datas
     *
     * @author esdras.filho
     * @param int $idJob
     */
    public function saveSolicitacao( $idJob=0 ){

        //Array com o job
        $job = array();

        //Array com o usuario
        $usuario = array();

        //Pegar o usuario logado
        $usuario = Sessao::get('usuario');

        //Pegar job
        $job = $this->job->getById($idJob);

        //Verifica se encontrou o job
        if (!empty($job) && is_array($job)) {

            //Monta array para salvar o job
            $arraySave = array();

            //array com o primeiro processo etapa
            $processoEtapa = array();

            //Verifica se foi escolhida a Etapa
            if (!empty($_POST['ID_PROCESSO_ETAPA'])) {

                //Recupera o processo etapa a ser gravado pelo ID
                $processoEtapa = $this->processoEtapa->getById($_POST['ID_PROCESSO_ETAPA']);

                //Recupera a nova etapa
                $etapa = $this->etapa->getById( $processoEtapa['ID_ETAPA'] );

                //Recupera o processo etapa anterior, ou seja antes de ser alterado
                $processoEtapaAnterior = $this->processoEtapa->getById($_POST['ID_PROCESSO_ETAPA_HIDDEN']);

                //Recupera a etapa anterior, ou seja antes de ser alterado
                $etapaAnterior = $this->etapa->getById( $processoEtapaAnterior['ID_ETAPA'] );
                //Caso a etapa saida de enviado para agencia e volte para qualquer outra os INDD's gerados para o job sao apagados
                if( $etapaAnterior['CHAVE_ETAPA'] == 'ENVIADO_AGENCIA' ){
                    if( $etapa['ORDEM_ETAPA'] < $etapaAnterior['ORDEM_ETAPA'] ){
                        $this->template->deletarTemplatesByJob( $idJob );
                    }
                }

                //verifica se encontrou
                if (!empty($processoEtapa) && is_array($processoEtapa)) {

                    //Monta o array de gravacao
                    $arraySave['ID_PROCESSO_ETAPA'] = $processoEtapa['ID_PROCESSO_ETAPA'];
                    $arraySave['ID_ETAPA'] = $processoEtapa['ID_ETAPA'];
                    $arraySave['ID_STATUS'] = $processoEtapa['ID_STATUS'];
                    $arraySave['DATA_INICIO_PROCESSO'] = format_date_to_db($_POST['DATA_INICIO_PROCESSO'], 2);
                    $arraySave['DATA_INICIO'] = format_date_to_db($_POST['DATA_INICIO'], 2);
                    $arraySave['DATA_TERMINO'] = format_date_to_db($_POST['DATA_TERMINO'], 2);

                    //Salva
                    $this->job->save($arraySave, $idJob);

                    //Agora gravamos informando que o job mudou de etapa
                    $this->jobHistorico->addHistorico($idJob, $processoEtapa['ID_ETAPA'], $usuario['ID_USUARIO'], $processoEtapa['ID_PROCESSO_ETAPA'], $processoEtapa['ID_STATUS']);

                } else {

                    echo '['.json_encode(array('erro'=>'Não foi encontrado etapas do processo do job. Por favor, verifique se não foi excluído!')).']';
                    return;

                }

            } else {

                echo '['.json_encode(array('erro'=>'Favor informar uma etapa para o job!')).']';
                return;
            }

        } else {

            echo '['.json_encode(array('erro'=>'O Job não foi encontrado para alteração. Por favor, verifique se não foi excluído!')).']';
            return;

        }

        //Deu tudo certo
        echo '['.json_encode(array('erro'=>'ok')).']';
        return;
    }

    /**
     * carrega os filtros padrao
     *
     * @author Hugo Ferreira da Silva
     * @link http://www.247id.com.br
     * @param array $data Dados para indicar quais filtros usar
     * @return void
     */
    protected function loadFilters($data = null){

        $user = Sessao::get('usuario');

        if(!empty($user['ID_PRODUTO']) ){
            $data['ID_PRODUTO'] = $user['ID_PRODUTO'];
        }

        if(!empty($user['ID_AGENCIA']) ){
            $data['ID_AGENCIA'] = $user['ID_AGENCIA'];
        }

        if(!empty($user['ID_CLIENTE']) ){
            $data['ID_CLIENTE'] = $user['ID_CLIENTE'];
        }

        $agencias = $this->agencia->listItems(array('STATUS_AGENCIA'=>1),0,10000);
        $tipos = $this->tipoPeca->getAll(0,1000,'DESC_TIPO_PECA');

        if(!empty($data['ID_AGENCIA'])){
            $this->assign('clientes', $this->cliente->getByAgencia($data['ID_AGENCIA']));
        } else {
            $this->assign('clientes', array());
        }

        if(!empty($data['ID_CLIENTE'])){
            $this->assign('agencias', $this->agencia->getByCliente($data['ID_CLIENTE']));
        } else {
            $this->assign('agencias', array());
        }

        if(!empty($data['ID_PRODUTO'])){
            $filters = array(
                'ID_PRODUTO' => $data['ID_PRODUTO'],
                'STATUS_CAMPANHA' => 1
            );
            $campanha = $this->campanha->listItems($filters,0,1000);
            $this->data['campanhas'] = $campanha['data'];

        } else {
            $this->data['campanhas'] = array();
        }


        $this->assign('produtos',array());
        if( !empty($data['ID_CLIENTE']) ){
            $this->assign('produtos', getProdutosUsuario(val($data,'ID_CLIENTE'), val($data,'ID_AGENCIA')));
        }

        $status = $this->status->getAll(0, 100, 'DESC_STATUS');
        $etapas = $this->etapa->getAll(0, 100, 'ORDEM_ETAPA');

        $this->assign('etapasParaCombo', $this->etapa->getItensForAprovacao());
        $this->assign('etapas', $etapas['data']);
        $this->assign('status', $status['data']);
    }


    /**
     * Checa se o usuario logado tem permissao ou nao de alterar o job acessado
     *
     * Se nao tiver permissao, redireciona para a listagem
     *
     * @author Hugo Ferreira da Silva
     * @param int $idjob
     * @return void
     */
    protected function checaPermissaoAlterar($idjob){

        $user = Sessao::get('usuario');
        $job = $this->job->getById($idjob);
        $produtos = getValoresChave(getProdutosUsuario($job['ID_CLIENTE'],$job['ID_AGENCIA']),'ID_PRODUTO');
        $goto = 'checklist/index';

        //Verifica se o job nao esta bloqueado
        if ( $job['ID_STATUS'] == $this->status->getIdByKey('BLOQUEADO') ) {
            redirect($goto);
        }

        //Verifica se o cliente do usuario eh o mesmo cliente do job
        if(!empty($user['ID_CLIENTE'])){
            if($job['ID_CLIENTE'] != $user['ID_CLIENTE']){
                redirect($goto);
            }

            //Verifica se o produto do job esta na lista permitida para o cliente x agencia
            if(!in_array($job['ID_PRODUTO'],$produtos)){
                redirect($goto);
            }
        }

        //Verifica se a agencia do usuario eh a mesma agencia do job
        if(!empty($user['ID_AGENCIA'])){
            if($user['ID_AGENCIA'] != $job['ID_AGENCIA']){
                redirect($goto);
            }

            $clientes = getValoresChave($this->cliente->getByAgencia($user['ID_AGENCIA']),'ID_CLIENTE');
            if(!in_array($job['ID_PRODUTO'],$produtos)){
                redirect($goto);
            }

            if(!in_array($job['ID_CLIENTE'],$clientes)){
                redirect($goto);
            }
        }
    }

    //alteração chamado 25346
    public function exportarJobExcel($id = null) {
        $item = $this->excelItem->getByIdJob($id);
        //echo $this->db->last_query();die;
        $this->data['EXPORTAR'] = $item;
        //print_rr($this->data);die;
        header_excel('Job_'.$id.' - '.date('Ymd-His'));

        $this->load->view('ROOT/checklist/exportar_job',$this->data);
    }

}
