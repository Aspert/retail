<?php

/**
 * Controller para gerenciamento de chaves de API
 * 
 * @author Gabriel Silva
 * 06 de Junho de 2018
 */
 class Api extends MY_Controller    {
 
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/api/';
	}
 
 /**
 * Lista as APIs cadastradas
 * 
 * @author Gabriel Silva
 * @param int $pagina Pagina Atual
 * @return void
 */
 
 function lista($pagina=0)	{
	if ( !empty($_POST))  {
			Sessao::set('busca', $_POST);
		}
 	
 	$dados = Sessao::get('busca');
		if(!is_array($dados)){
			$dados = array();
		}
		
 	$limit = empty($dados['pagina']) ? 0 : $dados['pagina'];
 	
 	// Filtros
 	$clientes = array();
 	$modulos = array();
 	$agencias = array();
 	
 	$rs = $this->agencia->getAll(0,77777,'DESC_AGENCIA','ASC');
 	$agencias = $rs['data'];
 	
 	if(!empty($dados['ID_AGENCIA']))	{
 		$clientes = $this->cliente->getByAgencia($dados['ID_AGENCIA']);
 	} 
 	
 	/** $rs = $this->controllers->getAll(0,77777,'DESC_CONTROLLER', 'ASC');
 	$modulos = $rs['data']; **/
 	
 	$this->assign('clientes', $clientes);
 	/** $this->assign('modulos', $modulos); **/
     $this->assign('agencias', $agencias);
 	
 	$apis = $this->api->listItems($dados, $pagina, $limit);
 	
 	$this->assign('total', $apis['total']);
 	
 	$this->data['apis'] = $apis['data'];
 	$this->data['paginacao'] = linkpagination($apis, $limit);
 	$this->data['total'] = $apis['total'];
 	$this->data['busca'] = $dados;
 	$this->data['podeAlterar'] = Sessao::hasPermission('api','salvar');
 	$this->display('index');
 	
 }
 
 }