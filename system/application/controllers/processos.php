<?php
/**
 * Controller para gerenciar os cadastros de processos
 *
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class Processos extends MY_Controller {

	/**
	 * Construtor
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Processos
	 */
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/processos/';
	}

	/**
	 * Lista os processos cadastrados
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $pagina Numero da pagina atual
	 * @return void
	 */
	public function lista($pagina = 0) {
		if ( !empty($_POST) )  {
			Sessao::set('busca',$_POST);
		}

		$busca = (array) Sessao::get('busca');
		$msg = Sessao::get('_mensagem');
		Sessao::clear('_mensagem');

		$this->loadFilters( $busca );

		$limit = val($busca,'pagina',5);
		$result = $this->processo->listItems($busca,$pagina, $limit, 'DESC_CLIENTE, DESC_TIPO_PECA, TB_PROCESSO.ID_PROCESSO','ASC');

		$this->assign('podeAlterar', Sessao::hasPermission('processos','save'));
		$this->assign('podeAlterarStatus', Sessao::hasPermission('processos','alterar_status'));
		$this->assign('busca',$busca);
		$this->assign('lista', $result['data']);
		$this->assign('paginacao', linkpagination($result, $limit));
		$this->assign('msg', $msg);

		$this->display('index');
	}

	/**
	 * Exibe o formulario de edicao
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do fabricante quando editando
	 * @return void
	 */
	public function form($id=null) {
		if($id!=null && $_SERVER['REQUEST_METHOD'] == 'GET'){
			//$this->checaPermissaoAlterar($id);
			$_POST = $this->processo->getById($id);
			$arrProcessos = $this->processoEtapa->getByProcesso($id);

			foreach( $arrProcessos as $item){
				$_POST['etapas'][]     = $item['ID_ETAPA'];
				$_POST['descricoes'][] = $item['DESC_ETAPA'];
				$_POST['qtd_dias'][]   = $item['QTD_DIAS'];
				$_POST['qtd_horas'][]  = $item['QTD_HORAS'];
				$_POST['id_processo_etapas'][]  = $item['ID_PROCESSO_ETAPA'];
				$_POST['id_status'][]  = $item['ID_STATUS'];
			}
			$this->assign('idProcesso', $id);
		} else {
			if(isset($_POST['ID_PROCESSO'])){
				$this->assign('idProcesso', $_POST['ID_PROCESSO']);
			}
			else{
				$this->assign('idProcesso', 0);
				
				if (!empty($_POST['etapas'])) {
					$arrProcessos = $_POST['etapas'];
					foreach( $arrProcessos as $item){
						$_POST['id_processo_etapas'][] = 0;
					}
				}
			}
		}
		
		$this->loadFilters( $_POST );

		$this->assign('etapas', $this->etapa->getItensControleProcessos());
		$this->assign('tipos', $this->tipo_peca->getTiposAtivos( val($_POST,'ID_CLIENTE',0) ));
		
		$arrChavesStatus = array(0 => 'EM_ALTERACAO', 1 => 'EM_APROVACAO', 2 => 'ELABORACAO', 3 => 'REVISAO', );
		$this->assign('status', $this->status->getByKeys($arrChavesStatus));
		$this->assign('em_elaboracao', $this->status->getByKeys( array(0 => 'ELABORACAO') ));

		$this->display('form');
	}

	/**
	 * Persiste os dados de um fabricante
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do fabricante
	 * @return void
	 */
	public function save($id=null) {
		$_POST['ID_PROCESSO'] = $id;
		$erros = $this->processo->validate($_POST);

		if(empty($erros)){
			if( !isset( $_POST['CONSIDERAR_DIAS_UTEIS'] ) ){ $_POST['CONSIDERAR_DIAS_UTEIS'] = 'N'; }
			if( !isset( $_POST['CONSIDERAR_FERIADO'] ) ){ $_POST['CONSIDERAR_FERIADO'] = 'N'; }			
			$this->processo->save($_POST, $id);
			redirect('processos/lista');
		}

		$_REQUEST['erros'] = $erros;
		$this->assign('erros', $erros);
		$this->form($id);
	}

	/**
	 * Altera o status do processo
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id codigo do processo
	 * @return void
	 */
	public function alterar_status($id=null) {
		$pro = $this->processo->getById($id);

		if( !empty($pro) ){

			// flag para indicar se pode mudar
			$podeAlterar = true;

			// nova flag
			$pro['STATUS_PROCESSO'] = empty($pro['STATUS_PROCESSO']) ? 1 : 0;

			// se esta ativando
			if( $pro['STATUS_PROCESSO'] == 1 ){
				// vamos pegar processos
				// com o mesmo tipo de peca
				$filters = array();
				$filters['ID_TIPO_PECA'] = $pro['ID_TIPO_PECA'];
				$filters['STATUS_PROCESSO'] = 1;

				$list = $this->processo->listItems($filters);

				// para cada resultado encontrado
				if( !empty($list['data']) ){
					foreach( $list['data'] as $item ){
						// se encontrou outro processo que nao e o mesmo
						if( $item['ID_PROCESSO'] != $pro['ID_PROCESSO'] ){
							// nao pode alterar
							Sessao::set('_mensagem', 'Você não pode ter mais de um processo ativo para o mesmo tipo de peça');
							$podeAlterar = false;
							break;
						}
					}
				}
			}

			if( $podeAlterar ){
				$this->processo->save($pro, $id);
			}
		}

		header("Location: " . $_SERVER['HTTP_REFERER']);
		exit;
	}

	/**
	 * Modal com as regras de email do processo
	 *
	 * @author Esdras Eduardo
	 * @link http://www.247id.com.br
	 * @param int $idProcessoEtapa codigo do processo de etapa
	 * @return void
	 */
	public function modal_regra_email( $idProcessoEtapa ) {
		$this->loadFiltersRegra($_POST);
		$this->assign('idProcessoEtapa', $idProcessoEtapa);
		$this->assign('grupos_selecionados', $this->regraEmailProcessoEtapaUsuario->getGruposByRegra($idProcessoEtapa));
		$this->assign('regras', $this->regraEmailProcessoEtapa->findAll($idProcessoEtapa));
		$this->display('modal_regra_email');
	}
	
	/**
	 * Carrega os filtros padroes
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	protected function loadFilters( &$data = array() ){
		$user = Sessao::get('usuario');

		$this->assign('clientes',array());
		$this->assign('tiposPeca',array());

		if( !empty($user['ID_AGENCIA']) ){
			$list = $this->cliente->getByAgencia($user['ID_AGENCIA']);
			$this->assign('clientes', $list);

			foreach( $list as $item ) {
				$data['CLIENTES'][] = $item['ID_CLIENTE'];
			}

		} else if(!empty($user['ID_CLIENTE'])) {
			$data['ID_CLIENTE'] = $user['ID_CLIENTE'];

		}

		if( !empty($data['ID_CLIENTE']) ){
			$this->assign('tiposPeca', $this->tipo_peca->getTiposAtivos($data['ID_CLIENTE']));
		}
	}

	/**
	 * Verifica se o usuario pode alterar o processo
	 *
	 * usado somente na
	 *
	 * @see system/application/libraries/MY_Controller::checaPermissaoAlterar()
	 */
	protected function checaPermissaoAlterar($data){
		$filters = array();
		$filters['ID_PROCESSO'] = sprintf('%d', $data);

		$res = $this->processo->listItems($filters);

		// se encontrou
		if( !empty($res['data']) ){
			// pega o primeiro registro
			$first = reset($res['data']);

			// se houver jobs
			if( $first['QTD_JOB'] > 0 ){
				// nao pode editar, entao redirecionamos
				redirect('processos/lista');
			}
		}
	}
	
	/**
	 * Popular combo box de filtro
	 * @author esdras.filho
	 * @param $data
	 */
	private function loadFiltersRegra($data){
		$this->assign('clientes', array());
		$this->assign('agencias', array());
		$this->assign('grupos',   array());
		
		// recupera as agencias
		$rs = $this->agencia->listItems(array('STATUS_AGENCIA'=>1),0,100000);
		$this->assign('agencias', $rs['data']);
		
		// recupera os clientes
		$rs = $this->cliente->listItems(array('STATUS_CLIENTE'=>1),0,100000);
		$this->assign('clientes', $rs['data']);
		
		
		if(!empty($data['ID_AGENCIA'])){
			$this->assign('grupos', $this->grupo->getByAgencia($data['ID_AGENCIA']));
		}
		
		if(!empty($data['ID_CLIENTE'])){
			$this->assign('grupos', $this->grupo->getByCliente($data['ID_CLIENTE']));
		}
	}
	
}


