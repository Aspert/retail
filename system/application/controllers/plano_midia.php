<?php

/**
 * Controller de plano de midia
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 */
class Plano_midia extends MY_Controller{

	/**
	 * Construtor
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Plano_midia
	 */
	public function __construct(){
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/plano_midia/';
	}

	/**
	 * Lista os jobs cadastrados
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $pg
	 * @return void
	 */
	public function index($pg = 0){
		
		if(isset($_SESSION['ID_JOB'])){
			unset($_SESSION['ID_JOB']);
		} else if(isset($_SESSION['idJob'])){
			unset($_SESSION['idJob']);
		} else if(isset($_SESSION['relatorioObjetoPendente'])){
			unset($_SESSION['relatorioObjetoPendente']);
		}
		
		// se enviou dados
		if(!empty($_POST)){
			// altera a sessao
			Sessao::set('busca', $_POST);
		}

		$user = Sessao::get('usuario');
		$dados = (array) Sessao::get('busca');
		$this->setSessionData( $dados );

		// somente os desta etapa
		$dados['ID_ETAPA'] = $this->etapa->getIdByKey('SEL_MIDIA');

		$dados['STATUS_CAMPANHA'] = 1;
		$dados['PRODUTOS'] = getValoresChave(getProdutosUsuario(),'ID_PRODUTO');

		if(empty($dados['ORDER'])){
			$dados['ORDER'] = 'DATA_INICIO';
		}
		if(empty($dados['ORDER_DIRECTION'])){
			$dados['ORDER_DIRECTION'] = 'DESC';
		}

		//Retira os bloqueados
		$dados['NOT_ID_STATUS'] = array ($this->status->getIdByKey('BLOQUEADO'));
		
		$limit = empty($dados['pagina']) ? 0 : $dados['pagina'];
		$lista = $this->job->listItems($dados, $pg, $limit, $dados['ORDER'], $dados['ORDER_DIRECTION']);

		$this->loadFilters($dados);

		$this->data['podeAlterar'] = Sessao::hasPermission('plano_midia','save');
		$this->data['acoesLote'] = Sessao::hasPermission('plano_midia','Acoes_lote');
		$this->data['podeImportar'] = Sessao::hasPermission('importacao_excel','importar');
		$this->data['busca'] = $dados;
		$this->data['lista'] = $lista['data'];
		$this->data['paginacao'] = linkpagination($lista, $limit);
		
		//print_rr($lista);die;

		$this->display('index');
	}

	/**
	 * Exibe o formulario de edicao do plano de midia
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id
	 * @return void
	 */
	public function form($id = null){
		$user = Sessao::get('usuario');

		if(!empty($id)){
			$this->checaPermissaoAlterar($id);
		}

		if(!empty($id) && $_SERVER['REQUEST_METHOD'] == 'GET'){
			$_POST = $this->job->getById($id);
			$_POST = array_merge($_POST, $this->excel->getByJob($id));
			$list = $this->job->getPracas($id);

			$_POST['DATA_INICIO_PROCESSO'] = format_date_to_form($_POST['DATA_INICIO_PROCESSO']);
			$_POST['DATA_INICIO'] = format_date_to_form($_POST['DATA_INICIO']);
			$_POST['DATA_TERMINO'] = format_date_to_form($_POST['DATA_TERMINO']);

			if( !empty($_POST['LARGURA_JOB']) ){
				$_POST['LARGURA_JOB'] = centimetragem($_POST['LARGURA_JOB']);
			}

			if( !empty($_POST['ALTURA_JOB']) ){
				$_POST['ALTURA_JOB'] = centimetragem($_POST['ALTURA_JOB']);
			}

			foreach($list as $item){
				$_POST['selecionadas'][] = $item['ID_PRACA'];
			}

		}

		if(empty($_POST['selecionadas'])){
			$_POST['selecionadas'] = array();
		}
        /*echo '<pre>';
        var_dump($_SESSION);
        exit();*/

		/*if ($id > 0) {

        }*/
        $clienteId = sprintf('%d', post('ID_CLIENTE',true));
        if (empty($_POST['ID_CLIENTE'])) {
            $clienteId = sprintf('%d', $_SESSION['usuario']['ID_CLIENTE']);
        }

        $usuarios = $this->usuario->getByCliente($clienteId);
        $this->assign('usuarios', $usuarios);
        $u = array();

        // se ja gravou, pega as pracas e categorias
		if( !empty($id) ){
			$pracas = $this->job->getPracas($id);
            // Buscando os usuários responsaveis do job
            $usuariosSelecionados = $this->jobUsuario->getUsers($id);

            foreach ($usuariosSelecionados as $usuariosSelecionado) {
                $u[] = $usuariosSelecionado['ID_USUARIO'];
            }

            // Busca todos os usuários do Cliente


			$this->assign('pracasSelecionadas', $pracas);
			$postSubcategorias = array();
			
			if( isset($_POST['qtd']) ){
				foreach( $_POST['qtd'] as $key => $val){
					foreach( $val as $ke => $va){
						foreach( $va as $k => $va){
							$postSubcategorias[$key][$ke]['quantidade'] = $va;
						}
					}
				}
			}
			
			if( isset($_POST['pagina']) ){
				foreach( $_POST['pagina'] as $key => $val){
					foreach( $val as $ke => $va){
						foreach( $va as $k => $va){
							$postSubcategorias[$key][$ke]['pagina'] = $va;
						}
					}
				}
			}

			$categorias = array();

			foreach($pracas as $item){
				$list = $this->categoria->getForPlanoMidia($_POST['ID_CLIENTE'], $id, $item['ID_PRACA'], $user['ID_USUARIO']);

				foreach($list as $cat){
					if(empty($categorias[$item['ID_PRACA']][$cat['ID_CATEGORIA']])){
						$categorias[$item['ID_PRACA']][$cat['ID_CATEGORIA']] = $cat;
					}
					
					if( isset($postSubcategorias[$item['ID_PRACA']][$cat['ID_SUBCATEGORIA']]) ){
						$cat['QTDE_EXCEL_CATEGORIA'] = $postSubcategorias[$item['ID_PRACA']][$cat['ID_SUBCATEGORIA']]['quantidade'];
						$cat['PAGINA_EXCEL_CATEGORIA'] = $postSubcategorias[$item['ID_PRACA']][$cat['ID_SUBCATEGORIA']]['pagina'];
					}
					
					$categorias[$item['ID_PRACA']][$cat['ID_CATEGORIA']]['subcategorias'][] = $cat;
				}
			}
			
			$this->assign('categoriasSelecionadas', $categorias);

			// agora vamos pegar as categorias e subcategorias
			$list = $this->categoria->getByUsuarioCliente($user['ID_USUARIO'],$_POST['ID_CLIENTE']);

			// para cada categorias
			foreach($list as $idx => $item){
				$list[ $idx ]['subcategorias'] = $this->subcategoria->getByCategoria($item['ID_CATEGORIA']);
			}

			// coloca as categorias
			$this->assign('categorias', $list);
		}

		//$this->assign('pracas', $this->praca->getByCliente());
		$this->assign('id', $id);
		$this->assign('selecionadas', $_POST['selecionadas']);

		$this->assign('abrir_multiplos', Sessao::hasPermission('plano_midia', 'abrir_multiplos') );
		$this->assign('podeVerCronograma', Sessao::hasPermission('plano_midia', 'visualizar_cronograma') );
		$this->assign('podePassarEtapa', Sessao::hasPermission('plano_midia', 'proxima_etapa') );
		$this->assign('podeSalvar', Sessao::hasPermission('plano_midia', 'save') );
		$this->assign('podeReaproveitar', Sessao::hasPermission('plano_midia', 'reaproveitar_job') );

		$this->assign('defineUsuarios', Sessao::hasPermission('plano_midia', 'usuarios_responsaveis') );
        $this->assign('usuariosSelecionados', $u);

		//print_rr($this->data);die;
		$this->loadFilters( $_POST );
		$this->display('form');
	}
	
	public function reaproveitar_job($id) {
		$user = Sessao::get('usuario');
		
		unset($_POST['TITULO_JOB']);
		unset($_POST['ID_CAMPANHA']);
		unset($_POST['ID_PRODUTO']);
		unset($_POST['DATA_INICIO_PROCESSO']);
		unset($_POST['DATA_INICIO']);
		unset($_POST['DATA_TERMINO']);
		
		if( !empty($id) ){
			$pracas = $this->job->getPracas($id);
		
			$this->assign('pracasSelecionadas', $pracas);
				
			$postSubcategorias = array();
		
			$categorias = array();
		
			foreach($pracas as $item){
				$list = $this->categoria->getForPlanoMidia($_POST['ID_CLIENTE'], $id, $item['ID_PRACA'], $user['ID_USUARIO']);
		
				foreach($list as $cat){
					if(empty($categorias[$item['ID_PRACA']][$cat['ID_CATEGORIA']])){
						$categorias[$item['ID_PRACA']][$cat['ID_CATEGORIA']] = $cat;
					}
						
					if( isset($postSubcategorias[$item['ID_PRACA']][$cat['ID_SUBCATEGORIA']]) ){
						$cat['QTDE_EXCEL_CATEGORIA'] = $postSubcategorias[$item['ID_PRACA']][$cat['ID_SUBCATEGORIA']]['quantidade'];
						$cat['PAGINA_EXCEL_CATEGORIA'] = $postSubcategorias[$item['ID_PRACA']][$cat['ID_SUBCATEGORIA']]['pagina'];
					}
						
					$categorias[$item['ID_PRACA']][$cat['ID_CATEGORIA']]['subcategorias'][] = $cat;
				}
			}
				
			$this->assign('categoriasSelecionadas', $categorias);
		
			// agora vamos pegar as categorias e subcategorias
			$list = $this->categoria->getByUsuarioCliente($user['ID_USUARIO'],$_POST['ID_CLIENTE']);
		
			// para cada categorias
			foreach($list as $idx => $item){
				$list[ $idx ]['subcategorias'] = $this->subcategoria->getByCategoria($item['ID_CATEGORIA']);
			}
		
			// coloca as categorias
			$this->assign('categorias', $list);
		}
		
		
		$this->assign('id', $id);
		
		//$this->assign('pracasSelecionadas',$_POST['pracas']);
		$this->assign('selecionadas', $_POST['pracas']);
		
		$this->assign('podeVerCronograma', Sessao::hasPermission('plano_midia', 'visualizar_cronograma') );
		$this->assign('podeSalvar', Sessao::hasPermission('plano_midia', 'save') );
		$this->assign('podePassarEtapa', Sessao::hasPermission('plano_midia', 'proxima_etapa') );
		//print_rr($this->data);die;
		$this->loadFilters( $_POST );
		$this->display('reaproveitarForm');
	}
	/**
	 * Salva as alteracoes do plano de midia
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id
	 * @return void
	 */
	public function save($id = null, $redirect = true, $reaproveitar = false, $dados = array()){
		
		$multiplos = "N";
		if(isset($_POST['ABRIR_MULTIPLOS'])){
			$multiplos = $_POST['ABRIR_MULTIPLOS'];
		}
		
		if(count($dados) > 0){
			$_POST = $dados;
		}

		$arrayJobsGerados = array();
		$arrayPracas = array();
		$pracasSelecionadas = isset($_POST['pracas']) ? $_POST['pracas'] : array();

		$user = Sessao::get('usuario');
		
		//Indicador para gravar historico, se for a primeira vez que o job estah sendo salvo
		$gravaHistorico = false;
		
		//Indicador de retorno
		$sucesso = false;
		
		//Etapas do Processo
		$processoEtapa = array();
		
		if(!empty($id)){
			$this->checaPermissaoAlterar($id);
		}

		//Recupera informacoes do usuario logado
		$user = Sessao::get('usuario');
			
		$_POST['ID_JOB'] = $id;
		$res =  $this->excel->validate($_POST);
				//print_r($res);die;

		if( empty($id) ){
			//Significa que eh um job novo
			$_POST['ID_USUARIO_AUTOR'] = $user['ID_USUARIO'];
			
			//Atribui variavel para gravar historico
			$gravaHistorico = true;
		}

		if( empty($res) ){

			$sucesso = true;
			
			// pega o processo associado
			$proc = $this->processo->getByTipoPeca($_POST['ID_TIPO_PECA']);

			// coloca o codigo do processo para salvar com os dados do job
			$_POST['ID_PROCESSO'] = $proc['ID_PROCESSO'];
			
			//Recupera o primeiro ID_PROCESSO_ETAPA de 'SEL_MIDIA' (Plano de Marketing) do Processo 
			$processoEtapa = $this->processoEtapa->getByProcessoChaveEtapa($proc['ID_PROCESSO'], 'SEL_MIDIA');
			
			//Verifica se encontrou o Plano de Marketing no Processo
			if ( !empty($processoEtapa) && is_array($processoEtapa) ) {
                $usuariosResponsaveis = post('ID_USUARIO_RESPONSAVEL',true);
                // remove os usuarios responsáveis por causa da nova mudança no sistema
                unset($_POST['ID_USUARIO_RESPONSAVEL']);
                //Coloca no POST para salvar com os dados do job
				$_POST['ID_PROCESSO_ETAPA'] = $processoEtapa['ID_PROCESSO_ETAPA'];
				
				//Codigo da Etapa
				$_POST['ID_ETAPA'] = $processoEtapa['ID_ETAPA'];
				
				//Codigo do status
				$_POST['ID_STATUS'] = $processoEtapa['ID_STATUS'];
	
				//salva o plano de midia
				if($reaproveitar == true){
					if($multiplos == "S"){
						$arrayPracas = $_POST['pracas'];
						$titulo = $_POST['TITULO_JOB'];
						foreach($arrayPracas as $p){
							$dadosPraca = $this->praca->getById($p);
							
							$_POST['pracas'] = array();
							array_push($_POST['pracas'],$p);

							$_POST['TITULO_JOB'] = $titulo." - ".$dadosPraca['DESC_PRACA'];
							array_push($arrayJobsGerados,$this->job->savePlanoMidiaReaproveitar($_POST, $id));
						}
					}else{
						$id = $this->job->savePlanoMidiaReaproveitar($_POST, $id);
					}
				}
				else{
					if($multiplos == "S"){
						//Praças Selecionadas
						$arrayPracas = $_POST['pracas'];
						$titulo = $_POST['TITULO_JOB'];
						foreach($arrayPracas as $p){
							
							$dadosPraca = $this->praca->getById($p);
							
							$_POST['pracas'] = array();
							array_push($_POST['pracas'],$p);

							$_POST['TITULO_JOB'] = $titulo." - ".$dadosPraca['DESC_PRACA'];
							
							array_push($arrayJobsGerados,$this->job->savePlanoMidia($_POST, $id));
						}

					}else{
						$id = $this->job->savePlanoMidia($_POST, $id);
					}
				}
				

                // Salva os Usuarios Responsaveis
					if($multiplos == "S"){

						foreach($arrayJobsGerados as $a){

							$pracas = $this->job->getPracas($a);
							foreach($pracas as $p){
								$usuariosPraca = $this->praca->getByUsersPraca($p['ID_PRACA']);
								$users = array();

								foreach($usuariosPraca as $p){
									array_push($users,$p['ID_USUARIO']);
								}

								$this->jobUsuario->salvaJobUsuario($a, $users);
							}
							
							if (is_array($usuariosResponsaveis)) {
								$this->jobUsuario->salvaJobUsuario($a, $usuariosResponsaveis);
							}
						}


					}else{
						$pracas = $this->job->getPracas($id);
						foreach($pracas as $p){
							$usuariosPraca = $this->praca->getByUsersPraca($p['ID_PRACA']);
							$users = array();
							foreach($usuariosPraca as $p){
								array_push($users,$p['ID_USUARIO']);
							}
							$this->jobUsuario->salvaJobUsuario($id, $users);
						}
						if (is_array($usuariosResponsaveis)) {
							$this->jobUsuario->salvaJobUsuario($id, $usuariosResponsaveis);
						}
					}
                    
                

				if ($gravaHistorico == true) {
					//Significa que eh a primeira gravacao do job e ira gravar o historico
					
					//Prepara dados do historico para gravar primeira etapa
					if($multiplos == "S"){
						foreach($arrayJobsGerados as $a){
							$data = array(
								'ID_ETAPA' => $processoEtapa['ID_ETAPA'],
								'ID_JOB' => $a,
								'DATA_HISTORICO' => date('Y-m-d H:i:s'),
								'ID_USUARIO' => $user['ID_USUARIO'],
								'ID_PROCESSO_ETAPA' => $processoEtapa['ID_PROCESSO_ETAPA'],
								'ID_STATUS' => $processoEtapa['ID_STATUS']
							);
							//Salva historico
							$this->db->insert('TB_JOB_HISTORICO', $data);
						}
					}else{
						$data = array(
							'ID_ETAPA' => $processoEtapa['ID_ETAPA'],
							'ID_JOB' => $id,
							'DATA_HISTORICO' => date('Y-m-d H:i:s'),
							'ID_USUARIO' => $user['ID_USUARIO'],
							'ID_PROCESSO_ETAPA' => $processoEtapa['ID_PROCESSO_ETAPA'],
							'ID_STATUS' => $processoEtapa['ID_STATUS']
						);
						//Salva historico
						$this->db->insert('TB_JOB_HISTORICO', $data);
					}

				}
				
				//Deixa redirecionar
				if($redirect == true){
					if($multiplos == "S"){
						redirect('plano_midia/index/');
					}else{
						redirect('plano_midia/form/' . $id);
					}
					
				}
				
			} else {
				
				$sucesso = false;
			}
				
		} else {

			$sucesso = false;
			
			// nao passou, vamos pegar as pracas
			// que estavam selecionadas
			if( !empty($_POST['pracas']) ){
				$_POST['selecionadas'] = $_POST['pracas'];
			}

			$this->assign('erros', $res);	
			$this->form($id);
		}
		
		return $sucesso;
	}
	/**
	 * Envia um job para proxima etapa
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do job
	 * @return void
	 */
	public function proxima_etapa($id){
		
		//array de erros
		$errosMudaEtapa = array();
		
		$this->checaPermissaoAlterar($id);

		//Se conseguir salvar
		if ( $this->save($id,false) == true ) { 

			$errosMudaEtapa = $this->job->mudaEtapa($id, Sessao::get('usuario'));
				
			if(!empty($errosMudaEtapa)){
				$this->assign('errosMudaEtapa', $errosMudaEtapa );
				$this->form($id);
			} else {
				redirect('plano_midia/index');
			}
			
		}
	}

	/**
	 * carrega os filtros padrao
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data Dados para indicar quais filtros usar
	 * @return void
	 */
	protected function loadFilters($data = null){

		$user = Sessao::get('usuario');

		if(!empty($user['ID_PRODUTO']) ){
			$data['ID_PRODUTO'] = $user['ID_PRODUTO'];
		}

		if(!empty($user['ID_AGENCIA']) ){
			$data['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}

		if(!empty($user['ID_CLIENTE']) ){
			$data['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}

		$tipoJob = $this->tipoJob->getAll(0,1000,'DESC_TIPO_JOB');

		if( !empty($data['ID_CLIENTE']) ){
			$agencias = $this->agencia->getByCliente( $data['ID_CLIENTE'] );
		} else {
			$agencias = array();
		}

		if(!empty($data['ID_AGENCIA'])){
			$this->data['clientes'] = $this->cliente->getByAgencia($data['ID_AGENCIA']);
		} else {
			$this->data['clientes'] = array();
		}

		if(!empty($data['ID_CLIENTE'])){
			$this->data['regioes'] = $this->regiao->getByCliente($data['ID_CLIENTE']);
		} else {
			$this->data['regioes'] = array();
		}

		if(!empty($data['ID_PRODUTO'])){
			$this->data['campanhas'] = $this->campanha->getByProduto($data['ID_PRODUTO']);
			$this->data['regioes'] = $this->regiao->getByProduto($data['ID_PRODUTO']);

		} else {
			$this->data['campanhas'] = array();
			$this->data['regioes'] = array();
		}

		if(!empty($data['ID_REGIAO'])){
			$this->data['pracas'] = $this->praca->getByRegiao($data['ID_REGIAO']);

		} else {
			$this->data['pracas'] = array();
		}

		$this->assign('produtos',array());
		if( !empty($data['ID_CLIENTE']) ){
			$this->assign('produtos', getProdutosUsuario(val($data,'ID_CLIENTE'), val($data,'ID_AGENCIA')));
		}
		$this->data['tiposJob'] = $tipoJob['data'];
		$this->data['tipos'] = $this->tipo_peca->getTiposAtivos( val($data,'ID_CLIENTE',0) );
		$this->data['agencias'] = $agencias;
	}

	/**
	 * Checa se o usuario logado tem permissao ou nao de alterar o job acessado
	 *
	 * Se nao tiver permissao, redireciona para a listagem
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idjob
	 * @return void
	 */
	protected function checaPermissaoAlterar($idjob){
		
		$user = Sessao::get('usuario');
		$job = $this->job->getById($idjob);
		$produtos = getValoresChave(getProdutosUsuario($job['ID_CLIENTE'],$job['ID_AGENCIA']),'ID_PRODUTO');
		$goto = 'plano_midia/index';

		//Verifica se o job nao esta bloqueado
		if ( $job['ID_STATUS'] == $this->status->getIdByKey('BLOQUEADO') ) {
			redirect($goto);
		}
		
		//Verifica se esta na etapa correta
		if( $job['ID_ETAPA'] != $this->etapa->getIdByKey('SEL_MIDIA') ){
			redirect($goto);
		}

		//Verifica se o cliente do usuario eh o mesmo cliente do job
		if(!empty($user['ID_CLIENTE'])){
			if($job['ID_CLIENTE'] != $user['ID_CLIENTE']){
				redirect($goto);
			}

			//Verifica se o produto do job esta na lista permitida para o cliente x agencia
			if(!in_array($job['ID_PRODUTO'],$produtos)){
				redirect($goto);
			}
		}

		//Verifica se a agencia do usuario eh a mesma agencia do job
		if(!empty($user['ID_AGENCIA'])){
			if($user['ID_AGENCIA'] != $job['ID_AGENCIA']){
				redirect($goto);
			}

			$clientes = getValoresChave($this->cliente->getByAgencia($user['ID_AGENCIA']),'ID_CLIENTE');
			if(!in_array($job['ID_PRODUTO'],$produtos)){
				redirect($goto);
			}

			if(!in_array($job['ID_CLIENTE'],$clientes)){
				redirect($goto);
			}
		}
	}
	
	public function espelho( $idCliente, $idJob, $idPracao ){
		$user = Sessao::get('usuario');
		$lista = $this->categoria->getForPlanoMidia($idCliente, $idJob, $idPracao, $user['ID_USUARIO']);
		
		$arrEspelho = Array();
		$arrCategoria = Array();
		$arrCores = Array();
		foreach( $lista as $espelho ){
			if( !empty($espelho['PAGINA_EXCEL_CATEGORIA']) && ($espelho['QTDE_EXCEL_CATEGORIA'] > 0) ){
				if(!isset($arrEspelho[$espelho['PAGINA_EXCEL_CATEGORIA']])){
					$arrEspelho[$espelho['PAGINA_EXCEL_CATEGORIA']] = Array();
				}
				$arrEspelho[$espelho['PAGINA_EXCEL_CATEGORIA']][$espelho['ID_SUBCATEGORIA']] = $espelho['QTDE_EXCEL_CATEGORIA'];
				$arrCategoria[$espelho['ID_SUBCATEGORIA']] = $espelho['DESC_CATEGORIA'] . '/' . $espelho['DESC_SUBCATEGORIA'];
				$arrCores[$espelho['ID_SUBCATEGORIA']] = $this->subcategoria->pegarCor($espelho['ID_SUBCATEGORIA'], $idCliente);
			}
		}
		
		if (!empty($arrEspelho) && is_array($arrEspelho)) { 
			$this->assign('strEspelho', json_encode($arrEspelho));
		}
		
		if (!empty($arrCategoria) && is_array($arrCategoria)) { 
			$this->assign('strCategoria', json_encode($arrCategoria));
		}
		
		if (!empty($arrCores) && is_array($arrCores)) { 
			$this->assign('strCores', json_encode($arrCores));
		}
		
		$this->display('visualizar_espelho');
	}
}
