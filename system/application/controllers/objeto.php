<?php

/**
 * Controller de objetos
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 *
 */
class Objeto extends MY_Controller {
	
	private $debug = false;

	/**
	 * Construtor
	 * @author Hugo Ferreira da Silva
	 * @return Objeto
	 */
	function __construct(){
		parent::__construct();
		$this->load->library('DBXinet');

		$this->_templatesBasePath = "ROOT/objeto/";

		// sempre vamos precisar das categorias, entao carregamos todas
		$this->data['categorias'][''] = '[Selecione]';
		$categorias = $this->categoria->getAll();

		foreach ($categorias['data'] as $key => $value){
			$this->data['categorias'][$value['ID_CATEGORIA'] . '#' . $value['DESC_CATEGORIA']] = $value['DESC_CATEGORIA'];
		}
	}

	/**
	 * Pagina principal
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	function index(){
		$this->debug('session: ' . session_id());
		$this->lista();
	}

	/**
	 * Lista os objetos cadastrados
	 * @author Hugo Ferreira da Silva
	 * @param int $pagina Pagina atual
	 * @return void
	 */
	function lista($pagina=0){
		// se esta pesquisando
		if (!empty($_POST)) {
			Sessao::set('busca', $_POST);
		}

		// pega os parametros de busca
		$data = (array) Sessao::get('busca');
		// pega o usuario logado
		$user = Sessao::get('usuario');

		// se nao escolheu uma ordem
		if(empty($data['ORDER'])){
			$data['ORDER'] = 'FILENAME';
		}

		// se nao escolheu a ordenacao dos resultados
		if(empty($data['ORDER_DIRECTION'])){
			$data['ORDER_DIRECTION'] = 'ASC';
		}

		if( !empty($user['ID_CLIENTE']) ){
			$data['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}

		if( !empty($user['ID_AGENCIA']) ){
			$data['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}

		$this->loadFilters($data);

		$limit = empty($data['pagina']) ? 10 : $data['pagina'];
		$pagina = $this->uri->segment(3);
		$pagina = empty($pagina) ? 0 : $this->uri->segment(3);

		$data['CATEGORIAS'] = getCategoriasUsuario();
		
		// efetua a consulta
		$objetos = $this->objeto->listItems($data, $pagina, $limit, $data['ORDER'], $data['ORDER_DIRECTION']);

		$this->data['busca'] = $data;
		$this->data['objetos'] = $objetos['data'];
		$this->data['objetosTotal'] = $objetos['total'];
		$this->data['paginacao'] = linkpagination($objetos,$limit);
		$this->data['podeEnviar'] = Sessao::hasPermission('objeto','upload');
		$this->data['podeEditar'] = Sessao::hasPermission('objeto','form');
		$this->data['podeEditarMultiplo'] = Sessao::hasPermission('objeto','formMultiple');
		$this->data['podeExcluir'] = Sessao::hasPermission('objeto','delete');
		$this->data['podeExcluirMultiplo'] = Sessao::hasPermission('objeto','deleteMultiple');
		$this->data['podeFicha'] = Sessao::hasPermission('objeto','fichas');
		$this->data['podeDownload'] = Sessao::hasPermission('objeto','download');

		$this->load->view('ROOT/objeto/index', $this->data);
	}


	/**
	 * Exibe o formulario de edicao
	 * @author Hugo Ferreira da Silva
	 * @param int $id Codigo do objeto
	 * @return void
	 */
	function form($id){
		$file = $this->objeto->getByFileId($id);
		$this->checaPermissaoAlterar($file['ID_OBJETO']);

		$keywords = $this->general->getKeywordsByFileID($id);

		if (!empty($keywords['CLIENTE'])) {
			$_POST['ID_CLIENTE'] = $keywords['CLIENTE'];
		} else {
			$_POST['ID_CLIENTE'] = '';
		}
		
		if (!empty($keywords['TIPO'])) {
			$_POST['ID_TIPO_OBJETO'] = $keywords['TIPO'];
		} else {
			$_POST['ID_TIPO_OBJETO'] = '';
		}

		if($_SERVER['REQUEST_METHOD'] == 'GET'){

			$this->data['codigo'] = empty($keywords['CODIGO']) ? '' : $keywords['CODIGO'];

			//foreach ($keywords as $key=> $value) {
			//	$keywords[$key] = utf8_encode($value);
			//}

			$_POST['KEYWORDS'] = empty($keywords['KEYWORDS']) ? '' : $keywords['KEYWORDS'];

			$user = Sessao::get('usuario');

			if(!empty($keywords['MARCA'])){
				$_POST['ID_MARCA'] = $keywords['MARCA'];
			}

			if(!empty($keywords['FABRICANTE'])){
				$_POST['ID_FABRICANTE'] = $keywords['FABRICANTE'];
			}

			if(!empty($keywords['CATEGORIA'])){
				$_POST['ID_CATEGORIA'] = $keywords['CATEGORIA'];
				$_POST['CATEGORIA'] = $keywords['CATEGORIA'];

				// carregamos os dados do cliente e agencia
				$cat = $this->categoria->getById($_POST['CATEGORIA']);
				$this->cliente->getById($cat['ID_CLIENTE']);

				$_POST['ID_CLIENTE'] = $cat['ID_CLIENTE'];
			}

			if(!empty($keywords['SUBCATEGORIA'])){
				$_POST['ID_SUBCATEGORIA'] = $keywords['SUBCATEGORIA'];
				$_POST['SUBCATEGORIA'] = $keywords['SUBCATEGORIA'];
			}

			$_POST['CODIGO'] = empty($keywords['CODIGO']) ? '' : $keywords['CODIGO'];
		}

		$this->data['nome'] = empty($keywords['NOME']) ? '' : utf8_encode($keywords['NOME']);

		$this->data['cliente'] = $this->cliente->getById($_POST['ID_CLIENTE']);
		$this->data['tipo'] = $this->tipo_objeto->getById($_POST['ID_TIPO_OBJETO']);

		$this->loadFilters($_POST);
		$this->data['id'] = $id;
		$previewPath = $this->xinet->getPathPorId($id);
		$this->data['previewPath'] = $previewPath['File'];

		$this->data['podeEmail'] = Sessao::hasPermission('objeto','salvarComEmail');
		$this->data['podeSalvar'] = Sessao::hasPermission('objeto','save');

		$this->display('form');

	}

	/**
	 * Permite edicao de multiplos arquivos simultaneamente
	 *
	 * @author juliano.polito
	 * @return void
	 */
	function formMultiple(){
		//recuperas os file ids selecionados
		$ids = post('FILE_ID',true);

		//recupera os registros relacionados
		$files = $this->objeto->getByFileId($ids);
		//pre($files);
		//verifica se ha mais que um cliente
		$idclientes = getValoresChave($files,'ID_CLIENTE');
		$clientesUnicos = array_unique($idclientes);

		//se houver exibe um erro
		if(count($clientesUnicos) > 1){
			$erros[] = $this->data['erros'][] = "Não é possível editar objetos de múltiplos clientes. Favor selecionar imagens de um cliente por vez.";
		}else if(count($clientesUnicos) == 0){
			redirect('objeto/lista');
		}

		if(!empty($erros)){

			$this->display('formMultiple');

		}else{

			$_POST['ID_CLIENTE'] = $clientesUnicos[0];

			$this->data['objetos'] = $files;
			$cli = $this->cliente->getById($clientesUnicos[0]);
			$this->data['cliente'] = $cli['DESC_CLIENTE'];
			$this->data['idcliente'] = $cli['ID_CLIENTE'];

			$this->loadFilters($_POST);

			$this->data['podeEmail'] = Sessao::hasPermission('objeto','saveMultipleEmail');
			$this->data['podeSalvar'] = Sessao::hasPermission('objeto','saveMultiple');


			//pre($this->data);

			$this->display('formMultiple');
		}
	}

	function delete($fileID){
		
		// pegamos as fichas relacionadas
		$fichas = $this->objeto->getFichasByFileId( $fileID );
		// se houver fichas
		if( count($fichas) > 0 ){
			// fazemos um redirect, pois nao pode remover
			// Demandas #4230, #4083
			redirect('objeto/lista?r='.rand(0,time()));
			exit;
		}

		$file = $this->xinet->getPathPorId($fileID);
		
		//Dah permissao no arquivo antes de excluir
		chmod($file['File'],0777);
		
		//parametros do indesign
		$argumentos = array(
			array('name' => 'origem', 'value' => rawurlencode($file['File']))
		);

		//print_rr($file);die;
		
		$scriptResult = unlink("$file[Path]/$file[FileName]");
		//recupera o retorno da execucao do script
		//$scriptResult = $this->ids->call('apaga_arquivo_xinet',$argumentos);

		if(empty($scriptResult['errorNumber'])){
			// pesquisa o objeto no retail
			$obj = $this->objeto->getByFileId($fileID);

			// se encontrar
			if( !empty($obj) ){
				// remove do banco
				$this->objeto->delete($obj['ID_OBJETO']);
			}
			//faz um sync do arquivo pra garantir que será excluído do banco de dados
			echo $this->xinet->sincroniza($file['File'],'&delete=true');
		}else{
			//
		}


		// redirecionamos
		sleep(1);
		redirect('objeto/lista?r='.rand(0,time()));
	}

	/**
	 * Permite excluir multiplos arquivos de uma vez
	 *
	 * @author juliano.polito
	 * @param array $fileID
	 * @return void
	 */
	function deleteMultiple(){
		
		$ids = post('FILE_ID',true);

		// pegamos as fichas relacionadas
		$fichas = $this->objeto->getFichasByFileIds( $ids );

		// se houver fichas
		if( count($fichas) > 0 ){
			// fazemos um redirect, pois nao pode remover
			// Demandas #4230, #4083
			Sessao::set('erro',"Um ou mais objetos possuem fichas associadas. Não foi possível excluir.");
			redirect('objeto/lista/');
			exit;
		}

		//recupera os arquivos/objetos pelos ids
		$files = $this->objeto->getByFileId($ids);

		//exclui cada objeto
		foreach($files as $file){
			
			$path = $file['PATH'].$file['FILENAME'];

			//Dah permissao no arquivo antes de excluir
			chmod($path,0777);
			
			$argumentos = array(
				array('name' => 'origem', 'value' => rawurlencode($path))
			);

			$scriptResult = $this->ids->call('apaga_arquivo_xinet',$argumentos);

			if(empty($scriptResult['errorNumber'])){
				$this->objeto->delete($file['ID_OBJETO']);
				$this->xinet->sincroniza($path,'&delete=true');
			}

		}

		sleep(1);
		redirect('objeto/lista');
	}


	function save($id,$email = false){

		//$marca = $this->marca->getById(post('ID_MARCA',true));
		//$categoria = $this->categoria->getById(post('ID_CATEGORIA',true));
		//$subcategoria = $this->subcategoria->getById(post('ID_SUBCATEGORIA',true));
		$keywords = post('KEYWORDS',true);

		$erros = array();

		if(post('ID_CLIENTE',true)=='') $erros[] = 'Informe o cliente';
		if(post('ID_TIPO_OBJETO',true)=='') {
			$erros[] = 'Informe o tipo';
		} else {
			$tipo = $this->tipo_objeto->getById(post('ID_TIPO_OBJETO',true));
			if($tipo['EXIGE_CATEGORIA'] == 1){
				if(post('ID_CATEGORIA',true)=='') $erros[] = 'Informe a categoria';
				if(post('ID_SUBCATEGORIA',true)=='') $erros[] = 'Informe a subcategoria';
			}
		}

		if( !empty($_POST['CODIGO']) ){
			$obj = $this->objeto->getByCodigo($_POST['CODIGO']);
			if( !empty($obj) && $obj['FILE_ID'] != $id ){
				$erros[] = 'Este código está atribuido a outro objeto';
			}
		}

		if(empty($erros)){
			$data = array();
			//verifica se o codigo existe para criar a chave
			//pois ao salvar em lote nao queremos alterar os codigos individuais dos objetos,
			//pois nao podem se repetir
			if(isset($_POST['CODIGO'])){
				$data['CODIGO'] = post('CODIGO',true);
			}
			$data['MARCA'] = post('ID_MARCA',true);
			$data['FABRICANTE'] = post('ID_FABRICANTE',true);
			$data['CATEGORIA'] = post('ID_CATEGORIA',true);
			$data['TIPO'] = post('ID_TIPO_OBJETO',true);
			$data['SUBCATEGORIA'] = post('ID_SUBCATEGORIA',true);
			$data['CLIENTE'] = post('ID_CLIENTE',true);

			$data['ID_MARCA'] = post('ID_MARCA',true);
			$data['ID_FABRICANTE'] = post('ID_FABRICANTE',true);
			$data['ID_CATEGORIA'] = post('ID_CATEGORIA',true);
			$data['ID_TIPO_OBJETO'] = post('ID_TIPO_OBJETO',true);
			$data['ID_SUBCATEGORIA'] = post('ID_SUBCATEGORIA',true);
			$data['ID_CLIENTE'] = post('ID_CLIENTE',true);

			$data['KEYWORDS'] =  utf8_decode($keywords);

			if( empty($data['ID_CATEGORIA']) ){
				unset($data['ID_CATEGORIA']);
			}

			if( empty($data['ID_SUBCATEGORIA']) ){
				unset($data['ID_SUBCATEGORIA']);
			}

			$this->general->saveFileKeywords($id,$data);

			$obj = $this->objeto->getByFileId($id);
			if( !empty($obj) ){
				foreach($data as $key => $val){
					$obj[$key] = $val;
				}

				if( $obj['ID_FABRICANTE'] == '' ) {
					unset($obj['ID_FABRICANTE']);
				}
				if( $obj['ID_MARCA'] == '' ) {
					unset($obj['ID_MARCA']);
				}

				$this->objeto->save($obj, $obj['ID_OBJETO']);
			}

			//caso receba true booleano, ou seja,
			//for passado por outra funcao interna
			if($email === true){

				//recupera dados da tb file para o objeto
				$objeto = $this->xinet->getPathPorId($id);

				//url do preview
				$preview = $this->config->item('xinet_url').$objeto['Path'];

				//recupera os nomes dos ids
				$dataArray = array('data'=>array($data));
				$this->trocaCodigosPorNomes($dataArray);

				//keywords do objeto
				$keywords = $dataArray['data'][0];

				//titulo
				$titulo = sprintf('Alteração de objeto %s %s',$keywords['CODIGO'],$objeto['FileName']);

				//envia emails de notificacao associado a regra
				$erros = $this->email->sendEmailRegra('objeto_salvar',
														$titulo,
														'ROOT/templates/objeto_salvar',
														array('objeto'=>$objeto,
																'keywords'=>$keywords,
																'preview'=>$preview),
														0,
														$data['CLIENTE']);

			}

			redirect('objeto/lista');
		}

		$this->data['erros'] = $erros;
		$this->form($id);
	}

	/**
	 * Atualiza multiplos arquivos simultaneamente
	 *
	 * @author juliano.polito
	 * @param array $ids
	 * @param $email
	 * @return void
	 */
	function saveMultiple($email = false){
		$ids = post('FILE_ID',true);

		$erros = array();

		//verifica se marca é do fabricante
		if(!empty($_POST['ID_MARCA'])){
			if(empty($_POST['ID_FABRICANTE'])){
				$erros[] = 'Uma marca foi selecionada sem fabricante. Selecione fabricante e marca.';
			}else{
				$marca = $this->marca->getById($_POST['ID_MARCA']);
				if($marca['ID_FABRICANTE'] != $_POST['ID_FABRICANTE']){
					$erros[] = 'A marca selecionada não pertence ao fabricante selecionado. Selecione novamente.';
				}
			}
		}else {
			if(!empty($_POST['ID_FABRICANTE'])){
				$erros[] = 'Um fabricante foi selecionado sem marca. Selecione fabricante e marca.';
			}
		}

		//verifica se a sub categoria é da categoria
		if(!empty($_POST['ID_SUBCATEGORIA'])){
			if(empty($_POST['ID_CATEGORIA'])){
				$erros[] = 'Uma subcategoria foi selecionada sem categoria. Selecione categoria e subcategoria.';
			}else{
				$sub = $this->subcategoria->getById($_POST['ID_SUBCATEGORIA']);
				if($sub['ID_CATEGORIA'] != $_POST['ID_CATEGORIA']){
					$erros[] = 'A subcategoria selecionada não pertence à categoria selecionada. Selecione novamente.';
				}
			}
		}else {
			if(!empty($_POST['ID_CATEGORIA'])){
				$erros[] = 'Uma categoria foi selecionada sem subcategoria. Selecione categoria e subcategoria.';
			}
		}

		if(empty($erros)){
			$files = $this->objeto->getByFileId($ids);

			$data = array();

			$data['MARCA'] = post('ID_MARCA',true);
			$data['FABRICANTE'] = post('ID_FABRICANTE',true);
			$data['CATEGORIA'] = post('ID_CATEGORIA',true);
			$data['SUBCATEGORIA'] = post('ID_SUBCATEGORIA',true);

			$data['ID_MARCA'] = post('ID_MARCA',true);
			$data['ID_FABRICANTE'] = post('ID_FABRICANTE',true);
			$data['ID_CATEGORIA'] = post('ID_CATEGORIA',true);
			$data['ID_SUBCATEGORIA'] = post('ID_SUBCATEGORIA',true);

			if(isset($_POST['chkKeywords'])){
				$data['KEYWORDS'] = utf8_decode(post('KEYWORDS',true));
			}

			if( empty($data['ID_CATEGORIA']) ){
				unset($data['ID_CATEGORIA']);
			}

			if( empty($data['ID_SUBCATEGORIA']) ){
				unset($data['ID_SUBCATEGORIA']);
			}

			if( empty($data['ID_MARCA']) ){
				unset($data['ID_MARCA']);
			}

			if( empty($data['ID_FABRICANTE']) ){
				unset($data['ID_FABRICANTE']);
			}
			if( empty($data['CATEGORIA']) ){
				unset($data['CATEGORIA']);
			}

			if( empty($data['SUBCATEGORIA']) ){
				unset($data['SUBCATEGORIA']);
			}

			if( empty($data['MARCA']) ){
				unset($data['MARCA']);
			}

			if( empty($data['FABRICANTE']) ){
				unset($data['FABRICANTE']);
			}

			if(!empty($data)){
				//cria segundo array com as keywords em utf8 para banco local
				//$data2 = $data;
				//if(isset($data2['KEYWORDS'])){
					//$data2['KEYWORDS'] = utf8_encode($data2['KEYWORDS']);
				//}

				foreach($files as $obj){
					$this->general->saveFileKeywords2($obj['FILE_ID'],$data);
					$this->objeto->save($data, $obj['ID_OBJETO']);
				}
			}

			//caso receba true booleano, ou seja,
			//for passado por outra funcao interna
			if($email === true && !empty($data)){

				//recupera dados da tb file para o objeto
				$objeto = $this->xinet->getPathPorId($obj['FILE_ID']);

				//url do preview
				$preview = $this->config->item('xinet_url').$obj['PATH'];

				//recupera os nomes dos ids
				$dataArray = array('data'=>array($data2));
				$this->trocaCodigosPorNomes($dataArray);

				//pre($dataArray);

				//keywords do objeto
				$keywords = $dataArray['data'][0];

				//titulo
				$titulo = 'Alteração de objetos';

				//envia emails de notificacao associado a regra
				$erros = $this->email->sendEmailRegra('objeto_salvar',
														$titulo,
														'ROOT/templates/objeto_salvar_multiplo',
														array('objetos'=>$files,
																'keywords'=>$dataArray['data'][0]),
														0,
														$data['CLIENTE']);

			}

			redirect('objeto/lista');
		}

		$this->data['erros'] = $erros;
		$this->formMultiple();
	}

	/**
	 * Salva multiplos objetos com envio de email
	 *
	 * @author juliano.polito
	 * @return void
	 */
	public function saveMultipleEmail(){
		$this->saveMultiple(true);
	}

	/**
	 * Salva e envia notificacao de email
	 * @author juliano.polito
	 * @param $id fileid do objeto
	 * @return void
	 */
	function salvarComEmail($id){
		$this->save($id,true);
	}


	/**
	 * Exibe as fichas de um objeto
	 * @author juliano.polito
	 * @return void
	 */
	function fichas($fileID){
		$file = $this->objeto->getByFileId($fileID);

		$this->checaPermissaoAlterar($file['ID_OBJETO']);

		$fichas = $this->objeto->getFichasByFileId($fileID);


		$this->data['fichas'] = $fichas;
		$this->data['objeto'] = $file;
		$this->data['id'] = $fileID;
		$this->data['podeEditarFicha'] = Sessao::hasPermission('ficha','form');

		$this->display('fichas');
	}

	/**
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param $lista
	 * @return unknown_type
	 */
	private function trocaCodigosPorNomes(&$lista){
	$cats = array(0);
		$subs = array(0);
		$mars = array(0);
		$tipos = array(0);

		foreach($lista['data'] as $key => $item){
			if(isset($item['CATEGORIA']))
				$cats[] = $item['CATEGORIA'];
			if(isset($item['SUBCATEGORIA']))
				$subs[] = $item['SUBCATEGORIA'];
			if(isset($item['MARCA']))
				$mars[] = $item['MARCA'];
			if(isset($item['TIPO']))
				$tipos[] = $item['TIPO'];
		}

		$cats = array_unique($cats);
		$subs = array_unique($subs);
		$mars = array_unique($mars);
		$tipos = array_unique($tipos);

		if(isset($item['CATEGORIA']))
			$cats_nomes = $this->categoria->getCategorias($cats);
		if(isset($item['SUBCATEGORIA']))
			$subs_nomes = $this->subcategoria->getSubcategorias($subs);
		if(isset($item['MARCA']))
			$mars_nomes = $this->marca->getMarcas($mars);
		if(isset($item['TIPO']))
			$tipos_nomes = $this->tipo_objeto->getTipos($tipos);

		foreach($lista['data'] as $key => $item){
			if(isset($item['CATEGORIA']))
				$lista['data'][$key]['CATEGORIA'] = !empty($cats_nomes[$item['CATEGORIA']]) ? $cats_nomes[$item['CATEGORIA']] : '';
			if(isset($item['SUBCATEGORIA']))
				$lista['data'][$key]['SUBCATEGORIA'] = !empty($subs_nomes[$item['SUBCATEGORIA']]) ? $subs_nomes[$item['SUBCATEGORIA']] : '';
			if(isset($item['MARCA']))
				$lista['data'][$key]['MARCA'] = !empty($mars_nomes[$item['MARCA']]) ? $mars_nomes[$item['MARCA']] : '';
			if(isset($item['TIPO']))
				$lista['data'][$key]['TIPO'] = !empty($tipos_nomes[$item['TIPO']]) ? $tipos_nomes[$item['TIPO']] : '';
			if(isset($item['TIPO']))
				$lista['data'][$key]['ID_TIPO'] = $item['TIPO'];
		}

	}

	/**
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param $data
	 * @return unknown_type
	 */
	private function loadFilters($data){
		$user = Sessao::get('usuario');

		$this->data['clientes'] = array();
		$this->data['categorias'] = array();
		$this->data['subcategorias'] = array();
		$this->data['marcas'] = array();
		$this->data['tipos'] = array();
		$this->data['fabricantes'] = array();

		// recupera as agencias
		$rs = $this->fabricante->getAll(0,10000,'DESC_FABRICANTE');
		$this->data['fabricantes'] = $rs['data'];

		// marcas
		if(!empty($data['ID_FABRICANTE'])){
			$this->data['marcas'] = $this->marca->getByFabricante($data['ID_FABRICANTE']);
		}

		if( empty($user['ID_AGENCIA']) ){
			$this->data['clientes'] = array();
 		} else {
			$this->data['clientes'] = $this->cliente->getByAgencia($user['ID_AGENCIA']);
 		}

		if(!empty($data['ID_CLIENTE'])){
			if( empty($user['ID_CLIENTE']) ){
				$lista = $this->categoria->getByCliente($data['ID_CLIENTE']);
			} else {
				$lista = $this->categoria->getByUsuarioCliente($user['ID_USUARIO'], $user['ID_CLIENTE']);
			}
			$this->data['categorias'] = $lista;
		}

		if(!empty($data['ID_CLIENTE'])){
			$this->data['tipos'] = $this->tipo_objeto->getByCliente($data['ID_CLIENTE']);
		}

		if(isset($data['CATEGORIA']) && $data['CATEGORIA'] != null){
			$this->data['subcategorias'] = $this->categoria->getSubcategorias($data['CATEGORIA']);
		}

		if(isset($data['ID_CATEGORIA']) && $data['ID_CATEGORIA'] != null){
			$this->data['subcategorias'] = $this->categoria->getSubcategorias($data['ID_CATEGORIA']);
		}
	}

	/**
	 * Apresenta a tela de upload
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function upload(){
		if(!empty($_FILES)){
			$data = array_shift($_FILES);
			$path = $this->tipo_objeto->getPath($_REQUEST['tipo_objeto']);

			//Despreza arquivos sem extensao
			$partes = pathinfo($_REQUEST['name']);
			if(empty($partes['extension']) || $partes['extension'] == '') {
				echo 'sem extensao';
				return;
			}

			if(is_uploaded_file($data['tmp_name'])){
				$novoNome = removeAcentos($novoNome) . "." . $arrExtensao[count($arrExtensao)-1];
				$this->sendFile($data['tmp_name'], $path, removeCaracteresEspeciaisNomeArquivo($data['name']), $_POST);			
				return;
			}
		}
		else{
			$user = Sessao::get('usuario');
		
			$filters = $_POST;
			
			if(!empty($user['ID_AGENCIA'])){
				$filters['ID_AGENCIA'] = $user['ID_AGENCIA'];
			}
		
			if(!empty($user['ID_CLIENTE'])){
				$filters['ID_CLIENTE'] = $user['ID_CLIENTE'];
				$_POST['ID_CLIENTE'] = $user['ID_CLIENTE'];
			}
			
			$this->loadFilters($filters);
			
			$this->display('upload');
		}
	}
	


	/**
	 * envia um arquivo para o indesign
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $origem
	 * @param string $path
	 * @param string $novoNome
	 * @param array $data
	 * @return void
	 */
	protected function sendFile($origem, $path, $novoNome, $data = array()){	
		//recupera a configuracao do site
		$site = $this->config->item("site");
		
		$path = utf8MAC($path);

		// cria o diretorio
		$this->xinet->createPath($path);
		$destino = $path . $novoNome;
		file_put_contents('debug.txt', $origem.' - '.$destino);
		move_uploaded_file($origem, $destino);

        $this->xinet->sincroniza($destino);

        sleep(3);

		foreach($data as $key => $item){
			$key = preg_replace('@^ID_@i', '', $key);
			$data[$key] = $item;
		}

		// dados que nao vem no form
		$data['MARCA'] = '';
		$data['FABRICANTE'] = '';
		$data['KEYWORDS'] = '';
		$data['CODIGO'] = '';
		$data['CLIENTE'] = $data['cliente'];
		$data['CATEGORIA'] = $data['categoria'];;
		$data['SUBCATEGORIA'] = $data['subcategoria'];;
		$data['ID_TIPO_OBJETO'] = $data['tipo_objeto'];
		$data['ID_CLIENTE'] = $data['cliente'];
		$data['ID_CATEGORIA'] = $data['categoria'];
		$data['ID_SUBCATEGORIA'] = $data['subcategoria'];
		$data['TIPO'] = $data['tipo_objeto'];
		unset($data['cliente']);
		unset($data['categoria']);
		unset($data['subcategoria']);
		unset($data['tipo_objeto']);
	
        $id = $this->xinet->getFilePorPath($path, $novoNome);
        $xfile = $this->xinet->getPathPorId($id);
        $this->xinet->sincroniza($xfile['File']);
        $countTentativas = 0;

        if(empty($id)){
        	while($countTentativas <= 3 || empty($id)){
        		$countTentativas++;
        		$this->xinet->sincroniza($destino);
        		sleep(3);
        		$id = $this->xinet->getFilePorPath($path, $novoNome);
        	}
        }
        
        if(!empty($id)){
        	$this->general->saveFileKeywords($id, $data);

        	// salvando a referencia na tabela de objetos
        	unset($data['MARCA'], $data['FABRICANTE'], $data['CODIGO']);
        	// procura o objeto por fileid
        	$obj = $this->objeto->getByFileId($id);

        	$this->debug('$obj '.print_r($this->objeto->getByFileId($id),true) );
        	
        	if( !empty($obj) ){
        		foreach($data as $key => $val){
        			$obj[$key] = $val;
        		}
        		$obj['DATA_UPLOAD'] = date('Y-m-d H:i:s');
        		$this->objeto->save($obj, $obj['ID_OBJETO']);
        	} else {
        		$data['PATH']        = $path;
        		$data['FILENAME']    = $novoNome;
        		$data['FILE_ID']     = $id;
        		$data['DATA_UPLOAD'] = date('Y-m-d H:i:s');
        		$this->objeto->save($data, null);
        	}
        }

		echo 'qualquercoisa';
	}

	/**
	 * Download de objetos do xinet
	 *
	 * @author juliano.polito
	 * @param $fileID
	 * @return void
	 */
	public function download($fileID){
		ob_end_clean();
		$this->load->helper('download');
		$fileData = $this->xinet->getPathPorId(strrev($fileID));

		$cliPath = '';

		//verifica se o usuario tem permissao para acessar este arquivo
		if(!empty($user['ID_AGENCIA'])){
			$cliPath = $this->agencia->getPath($user['ID_AGENCIA']);
		}

		if(!empty($user['ID_CLIENTE'])){
			$cliPath = $this->cliente->getPath($user['ID_CLIENTE']);
		}

		if(!empty($cliPath) && strpos($fileData['File'],$cliPath) === false){
			return;
		}

		/////////////////////////////////////////////////////
		// Alteracoes efetuadas para funcionar com
		// o Java de download de objetos
		/////////////////////////////////////////////////////

		// separamos o nome do arquivo
		$parts = explode('/', $fileData['File']);
		// pegamos somente o final
		$filename = end($parts);

		// cabecalhos para o Java
		// IMPORTANTE: para o java no MAC,
		// so funcionou como application/octet-stream
		// quando colocava o mime correto, dava problemas com JPG, GIF e PNG
		header("Content-Type: application/octet-stream");
		// enviamos como anexo
		header('Content-Disposition: attachment; filename="'.$filename.'"');

		// adicionamos mais alguns headers
		// porque da problema para baixar via HTTPS pelo IE
		if (strstr($_SERVER['HTTP_USER_AGENT'], "MSIE")) {
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header("Content-Transfer-Encoding: binary");
			header('Pragma: public');
		}

		// indicamos o tamanho do arquivo
		header("Content-Length: " . filesize($fileData['File']));

		// abrimos o arquivo para leitura
		$fp = fopen($fileData['File'], 'r');
		// enquanto nao chegar no final do arquivo
		while( !feof($fp) ){
			// escreve um pouco (para nao sobrecarregar o retail)
			echo fgets($fp, 1024);
		}
		// fechamos o arquivo
		fclose($fp);
	}

	/**
	 * Checa se o usuario logado tem permissao ou nao de alterar o job acessado
	 *
	 * Se nao tiver permissao, redireciona para a listagem
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idjob
	 * @return void
	 */
	protected function checaPermissaoAlterar($idobjeto){
		$user = Sessao::get('usuario');
		$objeto = $this->objeto->getById($idobjeto);

		$categorias = getCategoriasUsuario($objeto['ID_CLIENTE'],true);
		$goto = 'objeto/lista';

		if(!empty($user['ID_CLIENTE'])){
			if($objeto['ID_CLIENTE'] != $user['ID_CLIENTE']){
				redirect($goto);
			}

			if(!empty($objeto['ID_CATEGORIA']) && !in_array($objeto['ID_CATEGORIA'],$categorias)){
				redirect($goto);
			}
		}

		if(!empty($user['ID_AGENCIA'])){
			$clientes = getValoresChave($this->cliente->getByAgencia($user['ID_AGENCIA']),'ID_CLIENTE');

			if(!in_array($objeto['ID_CLIENTE'],$clientes)){
				redirect($goto);
			}
		}
		
	}
	private function debug($str) {
		$str = "---------------------------\n$str\n====\n\n";
		if($this->debug) {
			file_put_contents('system/application/controllers/uploadDebug.txt', $str, FILE_APPEND);
		}
	}
	
	public function validaArquivos($excel=false){
		if($excel){
			header_excel('arquivos_'.date('Ymd-His'));
		}
		$this->data['excel'] = $excel;
		$this->data['arquivosExistentes'] = $_SESSION['arquivosExistentes'];
		$this->display('validaArquivos');
	}
}


