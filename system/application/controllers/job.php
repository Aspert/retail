<?php
/**
 * controller para gerenciar os jobs
 * 
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class Job extends MY_Controller
{
	/**
	 * Construtor
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Grupo
	 */
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/job/';
	}
	
	/**
	 * Lista os jobs cadastrados
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $pagina Pagina atual de listagem
	 * @return void
	 */
	function lista($pagina=0) {
		
		if(!empty($_POST)){
			Sessao::set('busca', $_POST);
		}

		$dados = (array) Sessao::get('busca');
		
		$limit = empty($dados['pagina']) ? 0 : $dados['pagina'];
		$lista = $this->job->listItems($dados, $pagina, $limit);

		$this->loadFilters($dados);

		$this->data['podeAlterar'] = Sessao::hasPermission('job','save');
		$this->data['busca'] = $dados;
		$this->data['lista'] = $lista['data'];
		$this->data['paginacao'] = linkpagination($lista, $limit);
	
		$this->display('index');	
	}
	
	/**
	 * Exibe o formulario de edicao do job
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do job quando estiver editando
	 * @return void
	 */
	function form($id=null) {
		
		if( !is_null($id) && $_SERVER['REQUEST_METHOD'] == 'GET' ){
			$_POST = $this->job->getById($id);
			$_POST['DATA_INICIO'] = format_date($_POST['DATA_INICIO'],'d/m/Y');
			$_POST['DATA_TERMINO'] = format_date($_POST['DATA_TERMINO'],'d/m/Y');
		}
		
		$this->loadFilters($_POST);
		$this->display('form');
	}
	
	/**
	 * Persiste os dados informados no formulario no banco
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do job quando estiver editando
	 * @return void
	 */
	function save($id=null) {
		$erros = $this->job->validate($_POST);
		
		if( empty($erros) ) {
			$_POST['DATA_INICIO'] = format_date($_POST['DATA_INICIO']);
			$_POST['DATA_TERMINO'] = format_date($_POST['DATA_TERMINO']);
			$id = $this->job->save($_POST,$id);
			$this->xinet->createPath($this->job->getPath($id));
			redirect('job/lista');
		}
		
		$this->data['erros'] = $erros;
		$this->form($id);
	}
	
	/**
	 * carrega os filtros 
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $arrValues Array contendo valores padrao
	 * @return void
	 */
	protected function loadFilters($arrValues=array()){
		$agencias = $this->agencia->listItems(array('STATUS_AGENCIA'=>1),0,10000);
		$tipos = $this->tipoPeca->getAll(0,1000,'DESC_TIPO_PECA');
		$tipoJob = $this->tipoJob->getAll(0,1000,'DESC_TIPO_JOB');
		
		if(!empty($arrValues['ID_AGENCIA'])){
			$filters = array(
				'ID_AGENCIA' => $arrValues['ID_AGENCIA'],
				'STATUS_CLIENTE' => 1
			);
			$clientes = $this->cliente->listItems($filters,0,1000);
			$this->data['clientes'] = $clientes['data'];
		} else {
			$this->data['clientes'] = array();
		}
		
		if(!empty($arrValues['ID_CLIENTE'])){
			$filters = array(
				'ID_CLIENTE' => $arrValues['ID_CLIENTE'],
				'STATUS_PRODUTO' => 1
			);
			$campanha = $this->produto->listItems($filters,0,1000);
			$this->data['produtos'] = $campanha['data'];
		} else {
			$this->data['produtos'] = array();
		}
		
		if(!empty($arrValues['ID_PRODUTO'])){
			$filters = array(
				'ID_PRODUTO' => $arrValues['ID_PRODUTO'],
				'STATUS_CAMPANHA' => 1
			);
			$campanha = $this->campanha->listItems($filters,0,1000);
			$this->data['campanhas'] = $campanha['data'];
		} else {
			$this->data['campanhas'] = array();
		}
		
		$this->data['tiposJob'] = $tipoJob['data'];
		$this->data['tipos'] = $tipos['data'];
		$this->data['agencias'] = $agencias['data'];
	}
}
