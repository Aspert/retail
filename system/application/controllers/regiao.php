<?php
/**
 * Controller para gerenciar regiao
 * 
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class Regiao extends MY_Controller {
	
	/**
	 * Construtor
	 * 
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 */
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/regiao/';
	}
	
	/**
	 * Lista as regioes cadastradas
	 * 
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @param int $pagina Numero da pagina atual
	 * @return void
	 */
	function lista($pagina = 0) {
		if ( !empty($_POST) ) {
			Sessao::set('busca', $_POST);
		}
		
		$busca = (array) Sessao::get('busca');
		$limit = empty($busca['pagina']) ? 0 : $busca['pagina'];
		
		$user = Sessao::get('usuario');
				
		// carrega os clientes
		$filters = array(
			'STATUS_CLIENTE' => 1 
		);
		$clientes = $this->cliente->listItems($filters, 0, 1000);

		$regioes = $this->regiao->listItems($busca, $pagina, $limit);

		$this->data['regioes'] = $regioes['data'];
		$this->data['clientes'] = $clientes['data'];
		$this->data['busca'] = $busca;
		$this->data['paginacao'] = linkpagination($regioes,$limit);
		$this->data['podeAlterar'] = Sessao::hasPermission('regiao','save');
		
		$this->display('index');
	}
	
	/**
	 * Exibe o formulario de edicao de regiao
	 * 
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @param int $id Codigo da regiao quando editando
	 * @return void
	 */
	function form($id=null) {
		if( !is_null($id) && $_SERVER['REQUEST_METHOD'] == 'GET' ){
			$_POST = $this->regiao->getById($id);
		}
		
		// carrega as agencias
		//$res = $this->agencia->listItems(array('STATUS_AGENCIA'=>1),0,10000);
		
		$user = Sessao::get('usuario');

		$filters = array(
			'STATUS_CLIENTE' => 1
		);
		
		$clientes = $this->cliente->listItems($filters, 0, 1000);

		$this->data['clientes'] = $clientes['data'];

		$this->display('form');
	}
	
	/**
	 * Grava uma nova regiao ou as alteracoes de uma existente
	 * 
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @param int $id Codigo da regiao a ser alterada ou null para uma nova
	 * @return void
	 */
	function save($id=null) {
		$_POST['ID_REGIAO'] = sprintf('%d', $id);
		// faz a validacao dos dados
		$res = $this->regiao->validate($_POST);
		// se nao houve erros

		if( empty($res) ){
			//grava os dados no banco
			$this->regiao->save($_POST,$id);
			// redireciona para a pagina
			redirect('regiao/lista');
		}
		
		// indica os erros
		$_REQUEST['erros'] = $res;
		// carrega o formulario
		$this->form($id);
	}
}
