<?php
/**
 * controller para gerenciar o cadastro de transformadores
 *
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class Transformadores extends MY_Controller
{
	/**
	 * Construtor
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Transformadores
	 */
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/transformadores/';
	}

	/**
	 * Lista os transformadores cadastrados
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $pagina Pagina atual de listagem
	 * @return void
	 */
	function index($pagina=0) {

		if(!empty($_POST)){
			Sessao::set('busca', $_POST);
		}

		$dados = (array) Sessao::get('busca');

		$limit = empty($dados['pagina']) ? 0 : $dados['pagina'];
		$lista = $this->transformador->listItems($dados, $pagina, $limit);

		$this->data['busca'] = $dados;
		$this->data['lista'] = $lista['data'];
		$this->data['podeAlterar'] = Sessao::hasPermission('transformadores','save');
		$this->data['paginacao'] = linkpagination($lista, $limit);

		$this->display('index');
	}

	/**
	 * Exibe o formulario de edicao
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do transformador quando estiver editando
	 * @return void
	 */
	function form($id=null) {

		if( !is_null($id) && $_SERVER['REQUEST_METHOD'] == 'GET' ){
			$_POST = $this->transformador->getById($id);

			// pega as acoes
			$_POST['ACOES'] = $this->transformador->getActions($id);
		}

		$this->data['acoes'] = $this->transformadorAcao->getPossibleActions();
		$this->display('form');
	}

	/**
	 * Persiste os dados informados no formulario no banco
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do transformador quando estiver editando
	 * @return void
	 */
	function save($id=null) {
		$_POST['ID_TRANSFORMADOR'] = $id;
		$erros = $this->transformador->validate($_POST);

		if( empty($erros) ) {
			$id = $this->transformador->save($_POST,$id);
			redirect('transformadores/index');
		}

		$this->data['erros'] = $erros;
		$this->form($id);
	}
}
