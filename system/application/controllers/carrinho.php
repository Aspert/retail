<?php

/**
 * Controller de carrinho
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 */
class Carrinho extends MY_Controller{

	/**
	 * Construtor
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Carrinho
	 */
	public function __construct(){
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/carrinho/';
	}
	
	/**
	 * Lista os jobs cadastrados
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $pg
	 * @return void
	 */
	public function index($pg = 0){
		
		if(isset($_SESSION['ID_JOB'])){
			unset($_SESSION['ID_JOB']);
		} else if(isset($_SESSION['idJob'])){
			unset($_SESSION['idJob']);
		} else if(isset($_SESSION['relatorioObjetoPendente'])){
			unset($_SESSION['relatorioObjetoPendente']);
		}
		// se enviou dados
		if(!empty($_POST)){
			// altera a sessao
			Sessao::set('busca', $_POST);
		}

		$user = Sessao::get('usuario');
		$dados = (array) Sessao::get('busca');
		$this->setSessionData( $dados );

		// somente os desta etapa
		$dados['ID_ETAPA'] = $this->etapa->getIdByKey('CARRINHO');
		$dados['STATUS_CAMPANHA'] = 1;
		$dados['PRODUTOS'] = getValoresChave(getProdutosUsuario(),'ID_PRODUTO');

		if(empty($dados['ORDER'])){
			$dados['ORDER'] = 'DATA_INICIO';
		}
		if(empty($dados['ORDER_DIRECTION'])){
			$dados['ORDER_DIRECTION'] = 'DESC';
		}

		//Retira os bloqueados
		$dados['NOT_ID_STATUS'] = array ($this->status->getIdByKey('BLOQUEADO'));
		
		$limit = empty($dados['pagina']) ? 0 : $dados['pagina'];
		$lista = $this->job->listItems($dados, $pg, $limit, $dados['ORDER'], $dados['ORDER_DIRECTION']);

		$this->loadFilters($dados);

		$this->data['podeAlterar'] = Sessao::hasPermission('carrinho','save');
		$this->data['busca'] = $dados;
		$this->data['lista'] = $lista['data'];
		$this->data['paginacao'] = linkpagination($lista, $limit);

		$this->display('index');
	}

	/**
	 * Exibe o formulario de edicao do carrinho
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id
	 * @return void
	 */
	public function form($id = null){
		$this->checaPermissaoAlterar($id);
		$pracas = $this->job->getPracas($id);
		$job = $this->job->getById($id);

		// categorias do job com indicacoes
		$plano_midia = $this->job->getPlanoMidia($id, true);
		
		//pega total de fichas existentes no job
		$totalFichas = $this->job->getFichasUnicasSemItem($id);

		$this->loadFilters( $_POST );
		$this->assign('job', $job);
		$this->assign('podeEditarFicha', Sessao::hasPermission('ficha','form'));
		$this->assign('podePassarEtapa', Sessao::hasPermission('carrinho','proxima_etapa'));
		$this->assign('podeVoltarEtapa', Sessao::hasPermission('carrinho','voltar_etapa'));
		$this->assign('podeVisualizarPendencia', Sessao::hasPermission('ficha','pendencia'));

		$this->assign('pracas', $pracas);
		$this->assign('plano_midia', $plano_midia);
		$this->assign('categorias_usuario', getCategoriasUsuario($job['ID_CLIENTE']));
		$this->assign('total_fichas', $totalFichas);
		$this->display('form');
	}
	
	//função para criar fichas em lote
	public function criarEmLote(){
		
		//aqui tratamos o POST para uma variavel local e convertemos novamente para um array
		$serializedData = $_POST['post'];
		$post = array();
		parse_str($serializedData,$post);
		!empty($post['tipo_importacao']) ? $post['tipo_importacao'] = $post['tipo_importacao'] : $post['tipo_importacao'] = 'substituir';
		isset($post['FICHAS_NAO_ENCONTRADAS']) && !empty($post['FICHAS_NAO_ENCONTRADAS']) ? $post['FICHAS_NAO_ENCONTRADAS'] = $post['FICHAS_NAO_ENCONTRADAS'] : $post['FICHAS_NAO_ENCONTRADAS'] = 0;
		
		//obtemos o nome do arquivo salvo na sessão
		$filename = Sessao::get('__excel');
		$startTime = microtime(true);
		//aqui é aonde criamos as fichas
		$result = $this->excel->validateImport($post, $filename);

		//somente envia email com as fichas cadastradas em lote se o usuario tiver permissão
			
		// dados do job
		if(isset($post['ID_JOB'])&& !empty($post['ID_JOB']))
			$job = $this->job->getById($post['ID_JOB']);
		else
			$job = $post;
		
		//aqui deixamos somente as fichas que foram adicionadas
		$adicionadas = $result;
		if(!isset($result['erros'])){
			foreach ($result as $tipo => $res){
				unset($adicionadas[$tipo]['erros']);
				unset($adicionadas[$tipo]['avisos']);
			}
		}
		
		$dados['problemas'] = $result;
		
		//se for hermes ou compra facil chama-se produto e não ficha
		$user = Sessao::get('usuario');
		if( (strtolower($user['DESC_CLIENTE']) == "hermes") ||
		(strtolower($user['DESC_AGENCIA']) == "hermes") ||
		(strtolower($user['DESC_AGENCIA']) == "comprafacil") ||
		(strtolower($user['DESC_CLIENTE']) == "comprafacil")){
			$titulo = 'Lista de produtos cadastrados em lote';
		} else {
			$titulo = 'Lista de fichas cadastradas em lote';
		}
		
		//aqui enviamos novamente o email com as pendencias atualizadas
		$emailData = array_merge($job, array('problemas'=>$result));
		$erros = $this->email->sendEmailRegra('importacao_salvar',
				'Lista de pendencias do Job '.$emailData['TITULO_JOB'],
				'ROOT/templates/importacao_salvar',
				array('objeto'=>$emailData,
						'pendencias'=>$emailData),
				isset($emailData['ID_AGENCIA']) ? $emailData['ID_AGENCIA'] : $post['ID_AGENCIA'],
				isset($emailData['ID_CLIENTE']) ? $emailData['ID_CLIENTE'] : $post['ID_CLIENTE']
		);
			
		if(Sessao::hasPermission('ficha','emailFichasCadastradasEmLote')){
			
			
			$emailData = array_merge($job, array('problemas'=>$adicionadas));
			$erros = $this->email->sendEmailRegra('importacao_fichasCriadasEmLote',
					$titulo,
					'ROOT/templates/importacao_fichasCriadasEmLote',
					array(
							'objeto' => $emailData,
							'pendencias' => $emailData,
					),
			
					isset($emailData['ID_AGENCIA']) ? $emailData['ID_AGENCIA'] : $post['ID_AGENCIA'],
					isset($emailData['ID_CLIENTE']) ? $emailData['ID_CLIENTE'] : $post['ID_CLIENTE']
			);
		}		
		
		//se entrar aqui não criou nenhuma ficha
		if(isset($result['erros'])){
			echo 1;
		} else{
			$this->data['tempo_decorrido'] = microtime(true) - $startTime;
			$this->data['problemas'] = $result;
			$this->data['criarEmLote'] = 1;
			$this->data['comErros'] = 0;
			$this->load->view('ROOT/importacao_excel/resultado_validacao',$this->data);
		}		
	}
	
	//função para mover o arquivo para a pasta FILES
	public function moverArquivo($id){
		
		//obtemos o nome do arquivo salvo na sessão
		$filename = Sessao::get('__excel');
		// vejamos se foi enviado um arquivo
		if( !empty($_FILES['arquivo']['tmp_name']) && is_uploaded_file($_FILES['arquivo']['tmp_name']) ){
			// partes do arquivo
			$parts = pathinfo($_FILES['arquivo']['name']);
			// pegamos a extensao do arquivo
			$ext = $parts['extension'];
		
			// se nao esta na lista de itens suportados
			if( !in_array($ext,array('xlsx')) ){
				// mostra o erro
				$erros[] = 'Extensão do arquivo não suportado: ' . $ext .
				'Por favor, insira um arquivo com extensão .xlsx';
					
			} else {
					
				// se ja existir um na sessao
				if( Sessao::get('__excel') != '' && file_exists(Sessao::get('__excel')) ){
					// remove o anterior
					unlink(Sessao::get('__excel'));
				}
					
				// novo nome do excel
				$newname = md5($id . rand(0,time())) . '.' . $parts['extension'];
				// nome com a pasta
				$filename = 'files/'.$newname;
				if( file_exists($filename) ){
					unlink($filename);
				}
				// colocamos o arquivo na pasta files
				move_uploaded_file($_FILES['arquivo']['tmp_name'], $filename);
				Sessao::set('__excel', $filename);
			}
		}
	}

	/**
	 * Salva as alteracoes do plano de midia
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id
	 * @return void
	 */
	public function save($id = null, $redirect = true, $precisaValidar = true){
		
		if(isset($_FILES['arquivo']['name']) && !empty($_FILES['arquivo']['name'])){
			$_SESSION['arquivo'] = $_FILES['arquivo']['name'];
		}
		
		if(isset($_SESSION['ID_JOB'])){
			unset($_SESSION['ID_JOB']);
		} else if(isset($_SESSION['idJob'])){
			unset($_SESSION['idJob']);
		} else if(isset($_SESSION['relatorioObjetoPendente'])){
			unset($_SESSION['relatorioObjetoPendente']);
		}
		
		//Indicador de retorno
		$sucesso = false;

		$this->checaPermissaoAlterar($id);

		//forçando a entrada quando for cadastrar fichas em lote
		//Validar sera falso quando vier da tela de pendencias do carrinho
		if(($precisaValidar == true) || ((isset($_POST['CADASTRAR_EM_LOTE'])) && ($_POST['CADASTRAR_EM_LOTE'] == 1))){
			// se o usuario enviou um arquivo
			if(!empty($_FILES['arquivo']['tmp_name'])){
				$startTime = microtime(true);
	
				try {
					if( !checaExtensaoValida($_FILES['arquivo']['name'])){
						throw new Exception('A extensão do arquivo é inválida');
					}

					// SETA A PRACA BASE
					$_SESSION['PRACA_BASE'] = $this->excel->getPracaBase($id, $_FILES['arquivo']['tmp_name']);
					
					// importa os itens do excel
					$this->importarExcel($id, $_FILES['arquivo']['tmp_name']);

					//aqui tentamos forçar que o arquivo seja movido para a pasta FILES, pois em alguns casos ele não era movido
					$this->moverArquivo($id);
					
					
				} catch(Exception $e) {
	
	
					$data = array();
						
					// se o erro for de arquivo invalido para o adaptador
					if( $e->getCode() == ExcelImporterFactory::ARQUIVO_INVALIDO ){
						$this->assign('erros', array('Verifique se o arquivo é um XLSX válido para este cliente'));
	
					} else {
						$this->assign('erros', array('Verifique se o arquivo é um XLSX válido'));
	
					}
	
					$this->assign('tempo_decorrido', microtime(true) - $startTime);
					$this->assign('dados', $data);
				}
	
				$sucesso = false;
				
				// carrega a tela ao inves de redirect,
				// para poder dar um feedback
				$this->form($id);
			} else {
	
				// pega os itens do post
				$itens = empty($_POST['item']) ? array() : $_POST['item'];
	
				//pega os itens do post
				//$filhas = empty($_POST['filha']) ? array() : $_POST['filha'];
	
				$itensPaisFilhos = array();
	
				// salva o carrinho filhas
				//$this->job->saveCarrinhoFilhas($id, $filhas);

				// salva o carrinho
				$this->job->saveCarrinho($id, $itens);
	
				// altera a etapa
				$this->job->setStatus($id, $this->status->getIdByKey('EM_APROVACAO'));
				
				$sucesso = true;
	 
				if($redirect == true){
					// rediciona
					redirect('carrinho/form/' . $id);
				}
			}
			
		} else {
			$data['itens'] = Sessao::get('itens_excel');

			foreach($data['itens'] as $key=>$value){
				if(isset($_SESSION['PRACA_BASE']['ID_PRACA']) && is_numeric($_SESSION['PRACA_BASE']['ID_PRACA'])){
					$data['itens'][$key] = $this->job->ordenaPracaBase($id, $_SESSION['PRACA_BASE'], $value);
				}

				//gambi honesta pois quando os dados são salvos na sessão as novas fichas ainda não foram criadas e por isso ainda não se tem dados necessarios para salvar na Excel Item
				
				foreach ($value as $chave => $val){
//					if(!isset($val['ID_FICHA'])){
						$ficha = $this->ficha->getByClienteCodigo($_POST['ID_CLIENTE'],$val['PRODUTO']);
						$data['itens'][$key][$chave]['ID_FICHA'] = $ficha['ID_FICHA'];
						$data['itens'][$key][$chave]['ID_CATEGORIA'] = $ficha['ID_CATEGORIA'];
						$data['itens'][$key][$chave]['ID_SUBCATEGORIA'] = $ficha['ID_SUBCATEGORIA'];
						$data['itens'][$key][$chave]['DESCRICAO_ITEM'] = $ficha['NOME_FICHA'];
						$data['itens'][$key][$chave]['CODIGO_ITEM'] = $ficha['COD_BARRAS_FICHA'];
//					}
				}
			}
				
			$this->job->saveCarrinhoFromExcel($id, $data['itens']);
			Sessao::clear('itens_excel');
			redirect('carrinho/form/' . $id);
			$sucesso = true;
		}	
		return $sucesso;
		
	}
	/**
	 * Envia um job para proxima etapa sem itens
	 *
	 * @author Bruno Seiji Inoue Maia
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do job
	 * @return void
	 */
	public function proximaEtapaSemItem($id){
		$erros = $this->job->mudaEtapa($id, Sessao::get('usuario'));
		
		if(!empty($erros)){
			$this->assign('erros', $erros );
			$this->form($id);
		} else {
			redirect('carrinho/index');
		}		
	}

	/**
	 * Envia um job para proxima etapa
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do job
	 * @return void
	 */
	public function proxima_etapa($id){
		
		if(isset($_SESSION['ID_JOB'])){
			unset($_SESSION['ID_JOB']);
		} else if(isset($_SESSION['idJob'])){
			unset($_SESSION['idJob']);
		} else if(isset($_SESSION['relatorioObjetoPendente'])){
			unset($_SESSION['relatorioObjetoPendente']);
		}
		
		//array de erros
		$erros = array();
		
		//informacoes do job
		$job = array();
		
		//indicador de mudanca de etapa
		$mudaEtapa = true;
		
		//indicador de limite nao encontrado
		$limiteNaoEncontrado = false;
		
		$this->checaPermissaoAlterar($id);

		//Tenta salvar
		if ($this->save($id,false) == true) {

			//Verifica se ha itens ativos para mudar de etapa
			if( $this->job->hasItensAtivos($id) == true ){
				
				//Pega todas informacoes do job
				$job = $this->job->getById($id);
				
				//valida os itens do carrinho com o espelho
				$erros['erros_espelho'] = $this->job->validaItensEspelho($id);
				
				// se houver alguma incompatibilidade entre carrinho e espelho
				if(count($erros['erros_espelho']) > 0){

					// pega todas informacoes do tipo de peca
					$tipoPeca = $this->tipoPeca->getById($job['ID_TIPO_PECA']);
		
					//Verifica se houve retorno de algum limite minimo/maximo nao encontrado
					if ( !empty($erros['erros_espelho']['ERRO_LIMITE_NOT_FOUND']) && $erros['erros_espelho']['ERRO_LIMITE_NOT_FOUND'] == true ){
						//Deve se mostrar o array de erros. Nao muda de etapa em hipotese alguma
						$limiteNaoEncontrado = true;
						
						//Limpa o valor do array para nao mostrar na tela
						unset($erros['erros_espelho']['ERRO_LIMITE_NOT_FOUND']);
					}
					
					//Se nao puder passar a etapa
					if($tipoPeca['FLAG_BLOQUEIO_LIMITE'] == 1 || $limiteNaoEncontrado == true){
						
						//Nao deixa mudar de etapa
						$mudaEtapa = false;
					
						$this->assign('dados', $erros);
						// carrega a tela ao inves de redirect,
						// para poder dar um feedback
						$this->form($id);
						
					} else {
						
						// prepara a array de email
						$emailData = array_merge($job, array('problemas'=>$erros['erros_espelho']));
						
						//Array com o relatorio Divergencias Carrinho x Espelho
						$data = array();
						
						$data = $this->relatorio->divergenciasEspelhoJob(array('ID_JOB'=>$id));
		
						$this->assign('agencia', $this->agencia->getById($job['ID_AGENCIA']));
						$this->assign('cliente', $this->cliente->getById($job['ID_CLIENTE']));
						$this->assign('produto', $this->produto->getById($job['ID_PRODUTO']));
						$this->assign('campanha', $this->campanha->getById($job['ID_CAMPANHA']));
						$this->assign('lista', $data);
		
						$excelStr = $this->load->view('ROOT/relatorios/divergencias_espelho_job_html',$this->data, true);
						
						$excelName = 'files/'.time().'_excel_'.$id.'.xls';
						
						file_put_contents($excelName,$excelStr);
						
						// envia o email de pendencias
						$this->email->sendEmailRegra('limite_excedido',
													 'Relatório de Divergências entre Espelho e Carrinho - '.$job['TITULO_JOB'],
													 'ROOT/templates/limite_excedido',
													 array(	'objeto' => $emailData,
															'pendencias' => $emailData,
														),
														$emailData['ID_AGENCIA'],
														$emailData['ID_CLIENTE'],
														$excelName
													);
						
						@unlink($excelName);
						
					}
				}
			
				//Muda de etapa se tudo der certo
				if ($mudaEtapa == true) {
					
					$erros = $this->job->mudaEtapa($id, Sessao::get('usuario'));

					if(!empty($erros)){
						$this->assign('erros', $erros );
						$this->form($id);
					} else {
						redirect('carrinho/index');
					}
				}
					
			} else {
				$erros['erro_itens_ativos'] = 'Não há ítens ativos para este Job. O Job não mudou de Etapa.';
			} //if( $this->job->hasItensAtivos($id) == true ){
						
		} //if ($this->save($id,false) == true) {
	}

	/**
	 * Exibe o formulario de pesquisa de fichas
	 *
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function form_pesquisa($idjob=null){
		if( is_null($idjob) ){
			echo 'Job nao informado';
			exit;
		}

		$job = $this->job->getById($idjob);

		$user = Sessao::get('usuario');

		$filters = array();
		$this->setSessionData($filters);
		$filters['ID_CLIENTE'] = $job['ID_CLIENTE'];

		$this->loadFilters($filters);

		$this->assign('categorias', getCategoriasUsuario($job['ID_CLIENTE'],false));
		$this->assign('cliente', $this->cliente->getById($job['ID_CLIENTE']));

		$this->assign('busca', $filters);
		$this->display('form_pesquisa');
	}

	/**
	 * Realiza a importacao dos dados de uma planilha para o carrinho
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idJob Codigo do Job
	 * @param string $filename Nome do arquivo
	 * @return void
	 */
	protected function importarExcel($idJob, $filename){
		try {
			//salvamos o $_POST em uma variavel local para possiveis consultas futuras
			$post = $_POST;		
				
			// dados do job
			$job = $this->job->getById($idJob);

			// pegamos os dados do excel
			$xls = $this->excel->getById($job['ID_EXCEL']);

			// pegamos o tipo de importacao se eh pra substituir ou atualizar
			$tipoImportacao = $_POST['tipo_importacao'];
			// colocamos os dados do job no post
			$_POST = $job;
			
			// tempo de inicio da validacao
			$startTime = microtime( true );
			
			// pega a id do excel atual
			$idExcel = $job['ID_EXCEL'];
			
			// pega aid excel versao atual
			$idExcelVersao = $this->excelVersao->getVersaoAtual($idExcel);
			$idExcelVersao = $idExcelVersao['ID_EXCEL_VERSAO'];
			
			// inicia a validacao do carrinho
			$data = $this->excel->parseExcelCarrinho($idJob, $filename, $tipoImportacao);
			
			// atribui o valor falso a array de erros
			$comErros = 0;
			$fichasNaoEncontradas = array();
			$semPraca = 0;
			// verifica se houve algum erro q impedisse a subida da planilha e armazena na var
			foreach($data['erros'] as  $resultado){
				foreach($resultado as $key => $value){
					if($key == 'erros'){
						$comErros = 1;
					}
					if(is_array($value)){
						foreach($value as $chave => $val){
							if((substr($val['MENSAGEM'], -16) == 'nao possui ficha') && (substr(@$value[$chave+1]['MENSAGEM'], -18) != 'nao foi encontrada')){
								$fichasNaoEncontradas[] = $val['PRODUTO'];
							} else if(substr($val['MENSAGEM'], -20) == 'escolhida para o Job') {
								$semPraca = 1;
								$error = $resultado;
							}
								
						}
					}
				}
			}
			
			isset($error) ? $data['erros'] = $error : $data['erros'] = $data['erros'];
			
			$this->data['fichasNaoEncontradas'] = $fichasNaoEncontradas;
			$this->assign('comErros', $comErros);

			// Declara a variavel responsavel por armazenar as fichas relacionadas a cenarios excluidas
			$fichasCenarioExcluidas = array();
					
			// se nao houve erros
			if( empty($data['erros']) ){
				// Verificamos se alguma ficha que estava associada ao job e relacionada a algum cenario 
				// foi removida durante a subida da planilha

				// Armazenamos as fichas atuais do job
				$fichas_atuais = array();
				$fichas_atuais = $this->excelItem->getUnicosByExcelVersaoComCenarios( $idExcel, $idExcelVersao );
				
				// Armazenamos as fichas novas que veem da planilha
				$fichas_novas = array();
				foreach ( $data['itens'] as $d ){
					foreach ( $d as $i ){
						$fichas_novas[] = $i['ID_FICHA'];
					}
				}
				
				// Comparamos as fichas atuais com as novas fichas para ver se existe alguma ficha relacionada a um cenario q foi excluida
				foreach ( $fichas_atuais as $fa ) {
					$encontrou = false;
					
					foreach( $fichas_novas as $fn ) {
						if( $fa['ID_FICHA'] == $fn ) {
							$encontrou = true;
						}
					}
					
					if( $encontrou == false){
						$fichasCenarioExcluidas[] = $fa;
					}
				}
				
				// caso haja uma praca base ordena os produtos usando a mesma como referencia
				if(isset($_SESSION['PRACA_BASE']['ID_PRACA']) && is_numeric($_SESSION['PRACA_BASE']['ID_PRACA'])){
					foreach($data['itens'] as $key=>$value){
						$data['itens'][$key] = $this->job->ordenaPracaBase($idJob, $_SESSION['PRACA_BASE'], $value);
					}
				}

				// salva os itens
				$this->job->saveCarrinhoFromExcel($idJob, $data['itens'], $tipoImportacao);
			}

			// armazena as fichas removidas com cenarios relacionados para enviar pra view
			$data['fichas_cenario_excluidas'] = $fichasCenarioExcluidas;

			// tempo final
			$endTime = microtime( true );

			// tempo decorrido
			$this->assign('tempo_decorrido', $endTime - $startTime);

			// teve erros?
			if( !empty($data['erros']) ){
				$problemas['tipo_importacao'] = $tipoImportacao;
				$problemas['CADASTRAR_EM_LOTE'] = 0;
				$problemas['ID_CLIENTE'] = $_POST['ID_CLIENTE'];
				$problemas['ID_PRODUTO'] = $_POST['ID_PRODUTO'];
				$problemas['ID_USUARIO'] = $_POST['ID_USUARIO'];
				$problemas['ID_JOB'] = $_POST['ID_JOB'];
				$problemas['ID_TIPO_PECA'] = $_POST['ID_TIPO_PECA'];
				$problemas['PAGINAS_JOB'] = $_POST['PAGINAS_JOB'];
				$problemas['FICHAS_NAO_ENCONTRADAS'] = $fichasNaoEncontradas;
				
				$result = $this->excel->validateImport($problemas, $filename);
				//envia emails de notificacao associado a regra
				$emailData = array_merge($job, array('problemas'=>$result));
				
				$erros = $this->email->sendEmailRegra('carrinho_pendencias',
														'Lista de pendencias de importacao do Job '.$job['TITULO_JOB'],
														'ROOT/templates/importacao_salvar',
														array(
															'objeto' => $emailData,
															'pendencias' => $emailData,
														),

														$emailData['ID_AGENCIA'],
														$emailData['ID_CLIENTE']
														);

			}

			// coloca os dados encontrados na view,
			// para que possa ser exibido um feedback para o usuario
			$this->assign('dados', $data);

		} catch(Exception $e){
			throw $e;

		}

	}

	/**
	 * carrega os filtros padrao
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data Dados para indicar quais filtros usar
	 * @return void
	 */
	protected function loadFilters($data = null){

		$user = Sessao::get('usuario');

		$this->assign('produtos', array());
		$agencias = array();

		if(!empty($user['ID_PRODUTO']) ){
			$data['ID_PRODUTO'] = $user['ID_PRODUTO'];
		}

		if(!empty($user['ID_AGENCIA']) ){
			$data['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}

		if(!empty($user['ID_CLIENTE']) ){
			$data['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}

		$tipos    = $this->tipoPeca->getAll(0,1000,'DESC_TIPO_PECA');
		$tipoJob  = $this->tipoJob->getAll(0,1000,'DESC_TIPO_JOB');

		if(!empty($data['ID_AGENCIA'])){
			$this->data['clientes'] = $this->cliente->getByAgencia($data['ID_AGENCIA']);
			$agencias = array();

		} else {
			$this->data['clientes'] = array();
		}

		$this->assign('categorias', array());

		if(!empty($data['ID_CLIENTE'])){
			if( !empty($user['ID_CLIENTE']) ){
				$this->assign('categorias', $this->categoria->getByUsuarioCliente($user['ID_USUARIO'],$user['ID_CLIENTE']) );
			} else {
				$this->assign('categorias', $this->categoria->getByCliente($data['ID_CLIENTE']) );
			}

			$agencias = $this->agencia->getByCliente($data['ID_CLIENTE']);
		}

		if(!empty($data['ID_PRODUTO'])){
			$filters = array(
				'ID_PRODUTO' => $data['ID_PRODUTO'],
				'STATUS_CAMPANHA' => 1
			);
			$campanha = $this->campanha->listItems($filters,0,1000);
			$this->data['campanhas'] = $campanha['data'];

		} else {
			$this->data['campanhas'] = array();
		}

		$fab = $this->fabricante->getAll(0,10000,'DESC_FABRICANTE');
		$sit = $this->aprovacao_ficha->getAll(0,10000,'DESC_APROVACAO_FICHA');

		$this->assign('produtos',array());
		if( !empty($data['ID_CLIENTE']) ){
			$this->assign('produtos', getProdutosUsuario(val($data,'ID_CLIENTE'), val($data,'ID_AGENCIA')));
		}

		$this->assign('subcategorias', array() );
		$this->assign('fabricantes', $fab['data']);
		$this->assign('situacoes', $sit['data']);

		$this->data['tiposJob'] = $tipoJob['data'];
		$this->data['tipos'] = $tipos['data'];
		$this->data['agencias'] = $agencias;
	}

	/**
	 * Checa se o usuario logado tem permissao ou nao de alterar o job acessado
	 *
	 * Se nao tiver permissao, redireciona para a listagem
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idjob
	 * @return void
	 */
	protected function checaPermissaoAlterar($idjob){
		
		$user = Sessao::get('usuario');
		$job = $this->job->getById($idjob);
		$produtos = getValoresChave(getProdutosUsuario($job['ID_CLIENTE'],$job['ID_AGENCIA']),'ID_PRODUTO');
		$goto = 'carrinho/index';

		//Verifica se o job nao esta bloqueado
		if ( $job['ID_STATUS'] == $this->status->getIdByKey('BLOQUEADO') ) {
			redirect($goto);
		}
		
		//Verifica se esta na etapa correta
		if( $job['ID_ETAPA'] != $this->etapa->getIdByKey('CARRINHO') ){
			redirect($goto);
		}

		//Verifica se o cliente do usuario eh o mesmo cliente do job
		if(!empty($user['ID_CLIENTE'])){
			if($job['ID_CLIENTE'] != $user['ID_CLIENTE']){
				redirect($goto);
			}

			//Verifica se o produto do job esta na lista permitida para o cliente x agencia
			if(!in_array($job['ID_PRODUTO'],$produtos)){
				redirect($goto);
			}
		}

		//Verifica se a agencia do usuario eh a mesma agencia do job
		if(!empty($user['ID_AGENCIA'])){
			if($user['ID_AGENCIA'] != $job['ID_AGENCIA']){
				redirect($goto);
			}

			$clientes = getValoresChave($this->cliente->getByAgencia($user['ID_AGENCIA']),'ID_CLIENTE');
			if(!in_array($job['ID_PRODUTO'],$produtos)){
				redirect($goto);
			}

			if(!in_array($job['ID_CLIENTE'],$clientes)){
				redirect($goto);
			}
		}
	}
}
