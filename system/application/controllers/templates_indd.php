<?php

/**
 * Classe para controlar os templates de indesign cadastrados
 * 
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class Templates_indd extends MY_Controller {
	
	
	/**
	 * Construtor
	 *  
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Templates_indd
	 */
	function __construct(){
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/templates_indd/';
	}
	
	/**
	 * Chama a pagina de consulta
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $pagina
	 * @return void
	 */
	function lista($pagina=0){
		if(!empty($_POST)){
			Sessao::set('busca', $_POST);
		}
		
		$busca = (array) Sessao::get('busca');
		$user = Sessao::get('usuario');
		
		$this->loadFilters($busca);
		
		if(empty($busca['ORDER'])){
			$busca['ORDER'] = 'DESC_CAMPANHA';
		}

		if(empty($busca['ORDER_DIRECTION'])){
			$busca['ORDER_DIRECTION'] = 'ASC';
		}
		
		$this->setSessionData($busca);
		$busca['PRODUTOS'] = getValoresChave(getProdutosUsuario(),'ID_PRODUTO');
		
		$limit = empty($busca['pagina']) ? 0 : $busca['pagina'];
		$lista = $this->template->listItems($busca, $pagina, $limit, $busca['ORDER'],$busca['ORDER_DIRECTION']);
		
		foreach ($lista['data'] as &$item) {
			$path = $this->template->getPath($item['ID_TEMPLATE']);
			$file = $item['ID_TEMPLATE'].'.pdf';
			$filePath = utf8MAC($path).$file;
			$preview = '';
			if(is_file($filePath)){
				$preview = $item['ID_TEMPLATE'];
			};
			
			$item['PREVIEW'] = $preview;
		}
		
		$this->data['busca'] = $busca;
		
		$this->data['lista'] = $lista['data'];
		$this->data['paginacao'] = linkpagination($lista, $limit);
		$this->data['podeExcluir'] = Sessao::hasPermission('templates_indd','delete');
		$this->data['podeDownload'] = Sessao::hasPermission('templates_indd','download');
		$this->data['podeAlterar'] = Sessao::hasPermission('templates_indd','save');
		$this->data['podeEnviarArquivos'] = Sessao::hasPermission('templates_indd','enviar_arquivos');
		$this->data['podeEnviarIndd'] = Sessao::hasPermission('templates_indd','enviar_indd');
		$this->display('index');
	}
	
	/**
	 * Exibe o formulario de cadastro/edicao de templates
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do template a ser editado
	 * @return void
	 */
	function form($id=null){
		if( !is_null($id) ){
			if( !$this->checaPermissaoAlterar($this->template->getById($id)) ){
				return;
			}
		}
		
		if(!is_null($id) && $_SERVER['REQUEST_METHOD'] == 'GET'){
			$_POST = $this->template->getById($id);
			if( !$this->checaPermissaoAlterar($_POST) ){
				return;
			}
		}
		
		$this->data['PREVIEW'] = '';
		if(!empty($id)){
			$path = $this->template->getPath($id);
			$previewid = $this->template->getFileIdPreview($id);
			$this->data['PREVIEW'] = $previewid;
		}
		
		$this->loadFilters($_POST);
		// exibe o resultado
		$this->display('form');
	}
	
	/**
	 * Grava um template
	 * 
	 * <p>Caso nao informe um template, um novo sera gravado.</p>
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do template a ser gravado
	 * @return void
	 */
	function save($id=null){
		if( !is_null($id) ){
			if( !$this->checaPermissaoAlterar($this->template->getById($id)) ){
				return;
			}
		}
		// efetua a validacao dos dados
		$result = $this->template->validate($_POST);
		// se passar na validacao
		if( empty($result) ){
			if( is_null($id) ){
				$_POST['DATA_CADASTRO_TEMPLATE'] = date('Y-m-d H:i:s');
			}
			
			
			// salva os dados
			$id = $this->template->save($_POST, $id);
			$this->xinet->createPath($this->template->getPath($id));
			
			$_POST['TEMPLATE_EXISTS'] = $this->template->checaTemplateCompleto($id);
			$id = $this->template->save($_POST, $id);
			
			
			//recupera o obj template
			$objeto = $this->template->getById($id);
			
			$user = Sessao::get('usuario');
			$idagencia = empty($user['ID_AGENCIA']) ? 0 : $user['ID_AGENCIA']; 
			
			//envia emails de notificacao associado a regra
			$erros = $this->email->sendEmailRegra('template_salvar', 'Alteração de template '.$objeto['DESC_TEMPLATE'], 'ROOT/templates/template_salvar',array('objeto'=>$objeto), $idagencia, $objeto['ID_CLIENTE']);
			
			// envia para a tela de listagem
			redirect('templates_indd/lista');
		}
		
		// envia os erros
		$_REQUEST['erros'] = $result;
		$this->data['erros'] = $result;
		$this->form($id);
	}
	
	/**
	 * Exclui o template
	 * 
	 * @param int $id
	 */
	public function delete($id){
		//verifica se o usuario tem permissao de mexer no template
		$template = $this->template->getById($id);
		
		$user = Sessao::get('usuario');
		$codigosProduto = $this->produto->getCodigosByUsuario($user['ID_USUARIO']);
		
		if(!in_array($template['ID_PRODUTO'], $codigosProduto)){
			$this->showError('Você não tem permissão de excluir este template.');
			return;
		}
		
		//exclui o registro e os arquivos
		$this->template->deleteTemplate($id);
		
		// envia para a tela de listagem
		redirect('templates_indd/lista');
		
	}
	
	/**
	 * Tela para envio de arquivos de imagens e fontes
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do template
	 * @return void
	 */
	public function enviar_arquivos($id=null){
		$dados = $this->template->getById($id);
		
		if( !$this->checaPermissaoAlterar($dados) ){
			return;
		}

		if(!empty($_FILES)){
			$data = array_shift($_FILES);
			
			if(is_uploaded_file($data['tmp_name'])){
				$this->sendFile($data['tmp_name'],$data['name'],$id);
				
				//recupera o obj template
				$objeto = $dados;
				
				//recupera pendencias do template
				$pendLinks = $this->template->getBrokenLinks($id);
				
				//se nao temos mais pendencias, envia notificacao
				if(empty($pendLinks)){
					//se nao encontrar links em falta
					
					//pega os links resolvidos
					$pendLinks = $this->template->getLinks($id);
					
					$user = Sessao::get('usuario');
					$idagencia = empty($user['ID_AGENCIA']) ? 0 : $user['ID_AGENCIA'];
				
					//envia emails de notificacao associado a regra
					$erros = $this->email->sendEmailRegra('template_upload_links', 
															'Upload de arquivos pendentes do template '.$objeto['DESC_TEMPLATE'], 
															'ROOT/templates/template_upload_links',
															array('objeto'=>$objeto,
																  'pendLinks'=>$pendLinks), $idagencia, $objeto['ID_CLIENTE']);
															
					// chama o script indesign para FPO
					// $this->processaFPO($id);
				}
				
				
				echo 'ok';
				return;
			}
		}
		
		$xml = XmlUtil::array2xml($this->template->getFilesInPath($id));
		$xml = preg_replace('@(\r|\n)@','',$xml);
		
		$this->data['files'] = $xml;
		$this->data['links'] = $this->template->getBrokenLinks($id);
		$this->data['template'] = $dados;
		$this->display('enviar_arquivos');
	}
	
	/**
	 * Tela para enviar arquivo de indesign
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do template
	 * @return void
	 */
	public function enviar_indd($id=null, $acao = '', $iditem = ''){
		$dados = $this->template->getById($id);
		
		if( !$this->checaPermissaoAlterar($dados) ){
			return;
		}
		
		if( !empty($acao) ){
			switch($acao){
				case 'remover_item':
					$this->templateItem->remover($iditem);
					$this->template->atualizarPDFPreview($id);
					redirect('templates_indd/enviar_indd/'.$id);
				break;
				
				case 'salvar_ordens':
					if( !empty($_POST['pgInicio']) ){
						
						$files = array();
						
						// primeiro, vamos pegar a pasta
						$path = utf8MAC($this->template->getPath($id));
						
						foreach($_POST['pgInicio'] as $iditem => $startPage){
							$endPage = isset($_POST['pgFim'][$iditem]) ? $_POST['pgFim'][$iditem] : 0;
							
							// pega os dados do item
							$data = $this->templateItem->getById($iditem);
							
							// nome anterior
							$oldName = $this->templateItem->getFilenameByTemplateRange(
								$id, $data['PAGINA_INICIO_TEMPLATE_ITEM'], $data['PAGINA_FIM_TEMPLATE_ITEM']
							);
							
							// nome novo
							$newName = $this->templateItem->getFilenameByTemplateRange(
								$id, $startPage, $endPage
							);
							
							// nome temporario atual
							$tempName = $oldName .'.xpto';
							// renomeamos o arquivo
							rename($path.$oldName, $path.$tempName);
							// tambem renomeamos o jpg
							$jpg = str_replace('.indd','.jpg', $oldName);
							$tempJpg = str_replace('.indd','.jpg', $tempName);
							
							rename($path.$jpg, $path.$tempJpg);
							
							
							// vamos colocar os arquivos em uma fila
							$filename = array(
								'old' => $oldName,
								'new' => $newName,
								'curr' => $tempName
							);
							
							// gravamos na fila
							$files[] = $filename;
							
							$item = array(
								'ID_TEMPLATE_ITEM' => $iditem,
								'PAGINA_INICIO_TEMPLATE_ITEM' => $startPage,
								'PAGINA_FIM_TEMPLATE_ITEM' => $endPage
							);
							
							$this->templateItem->save($item, $iditem);
						}
						
						// para cada item na fila
						foreach($files as $file){
							// renomeamos para o nome correto
							rename($path.$file['curr'], $path.$file['new']);
							// renomeamos o jpg
							$jpgTemp = str_replace('.indd','.jpg',$file['curr']);
							$jpg = str_replace('.indd','.jpg',$file['new']);
							rename($path.$jpgTemp, $path.$jpg);
						}
						
						$existe = $this->template->checaTemplateCompleto($id) == true ? 1 : 0;
						$dataSave = array('TEMPLATE_EXISTS' => $existe);
						$this->template->save($dataSave,$id);
						
						if( $existe == 1 ){
							$this->template->atualizarPDFPreview($id);
						}
						
						redirect('templates_indd/enviar_indd/'.$id);
					}
				break;
				
				case 'checar_ordens':
					// lista de erros
					$erros = array();
					$alertas = array();
					
					$xml = array();
					$xml[] = '<retorno>';
					
					// se enviou alguma coisa pelo post
					if( !empty($_POST['total']) ){
						// para cada item enviado
						for($i=0; $i<$_POST['total']; $i++){
							// pega a pagina inicial
							$startPage = isset($_POST['inicio'.$i]) ? $_POST['inicio'.$i] : 0;
							// pega a pagina final
							$endPage = isset($_POST['fim'.$i]) ? $_POST['fim'.$i] : 0;
							
							// se o range escolhido eh invalido
							if( !$this->templateItem->validateRange($id, $startPage, $endPage) ){
								// anexa o erro
								$msg = 'O item #' . ($i+1) . ' intercala com outro item no servidor';
								$erros[] = $msg;
							}
							
							// se ja existe outro no mesmo range no servidor
							$res = $this->templateItem->getByTemplateRange($id,$startPage,$endPage);
							if( !empty($res) ){
								$alertas[] = 'O item com o range '.$startPage.' - '.$endPage.' será substituido no servidor';
							}
							
							if( $startPage > $dados['NUMERO_PAGINAS'] ){
								$erros[] = 'A página inicial do item #'.($i+1).' é maior que o numero de páginas do pedido de template ('.$dados['NUMERO_PAGINAS'].')';
							}
							
							if( $endPage > $dados['NUMERO_PAGINAS'] ){
								$erros[] = 'A página final do item #'.($i+1).' é maior que o numero de páginas do pedido de template ('.$dados['NUMERO_PAGINAS'].')';
							}
							
						}
						
						foreach($erros as $item){
							$xml[] = '<erros><![CDATA[' . $item . ']]></erros>';
						}
						
						foreach($alertas as $item){
							$xml[] = '<alertas><![CDATA[' . $item . ']]></alertas>';
						}
						
					}
					
					$xml[] = '</retorno>';
					
					header("Content-Type: text/xml");
					echo implode(PHP_EOL, $xml);
					
					exit;
					
				break;
			}
		}
		
		
		if(!empty($_FILES['file']) && is_uploaded_file($_FILES['file']['tmp_name'])){
			$ext = strtolower(substr($_FILES['file']['name'], strrpos($_FILES['file']['name'], '.')+1));
			
			if($ext == 'indd'){
				try {
					$iditem = $this->templateItem->salvarItem($id, $_FILES['file']['tmp_name'], $_POST['startPage'], $_POST['endPage']);
				
					//Atualiza o registro do indesign indicando que ja existe arquivo de template
					$existe = $this->template->checaTemplateCompleto($id) == true ? 1 : 0;
					$dataSave = array('TEMPLATE_EXISTS' => $existe);
					$this->template->save($dataSave,$id);
					
					if( $existe == 1 ){
						$this->template->atualizarPDFPreview($id);
					}
					
					//recupera o obj template
					$objeto = $this->template->getById($id);
					
					//recupera pendencias do template
					$pendLinks = $this->template->getBrokenLinks($id);
					
					$user = Sessao::get('usuario');
					$idagencia = empty($user['ID_AGENCIA']) ? 0 : $user['ID_AGENCIA'];
					
					//envia emails de notificacao associado a regra
					$erros = $this->email->sendEmailRegra('template_upload_indd', 
															'Upload de arquivo de template '.$objeto['DESC_TEMPLATE'], 
															'ROOT/templates/template_upload_indd',
															array('objeto'=>$objeto,
																	'pendLinks'=>$pendLinks), $idagencia, $objeto['ID_CLIENTE']);
					
					//redirect('templates_indd/enviar_arquivos/'.$id);
					die('ok');
				
				} catch(Exception $e) {
					$this->data['erros'] = $e->getMessage();
					$dataSave = array('TEMPLATE_EXISTS' => 0);
					$this->template->save($dataSave,$id);
				}
			} else {
				$this->data['erros'][] = 'O arquivo enviado deve ser do tipo INDD';
			}
		}
		
		// recupera a lista de itens de template pelo codigo do template
		$this->data['templates'] = $this->templateItem->getByTemplate($id);
		// dados do template
		$this->data['template'] = $dados;
		
		// exibe a view
		$this->display('enviar_indd');
		
	}
	
	/**
	 * Carrega os filtros para exibir na pagina
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data
	 * @return void
	 */
	private function loadFilters(array $data){
		$user = Sessao::get('usuario');
		// verificamos se tem algo na sessao
		// nestes casos independem do formulario
		if(!empty($user['ID_AGENCIA'])){
			$data['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}
		
		if(!empty($user['ID_CLIENTE'])){
			$data['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}
		
		if(!empty($user['ID_PRODUTO'])){
			$data['ID_PRODUTO'] = $user['ID_PRODUTO'];
		}
		
		//////////////////////////////////////////////////////
		// filtros
		//////////////////////////////////////////////////////
		
		if(!empty($data['ID_AGENCIA'])){
			$this->assign('clientes', $this->cliente->getByAgencia($data['ID_AGENCIA']));
		} else {
			$this->assign('clientes', array());
		}
		
		if(!empty($data['ID_CLIENTE'])){
			$this->assign('agencias', $this->agencia->getByCliente($data['ID_CLIENTE']));
		} else {
			$this->assign('agencias', array());
		}
		
		if(!empty($data['ID_PRODUTO'])){
			$cl = $this->campanha->listItems(array("ID_PRODUTO"=>$data['ID_PRODUTO'],'STATUS_CAMPANHA'=>1),0,10000);
			$this->data['campanhas'] = $cl['data'];
		} else {
			$this->data['campanhas'] = array();
		}
		
		$this->assign('produtos', array());
		if( !empty($data['ID_CLIENTE']) ){
			$this->assign('produtos', getProdutosUsuario(val($data,'ID_CLIENTE'), val($data,'ID_AGENCIA')));
		}
	}
	
	/**
	 * Checa se o usuario pode manipular o registro acessado
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data
	 * @return void
	 */
	protected function checaPermissaoAlterar($data){
		$user = Sessao::get('usuario');
		return true;
		
		if(empty($data)){
			$this->showError('Nenhum dado encontrado');
			return false;
		}
		
		if(!empty($user['ID_AGENCIA']) && $data['ID_AGENCIA'] != $user['ID_AGENCIA']){
			$this->showError('Você não pode alterar o registro acessado');
			return false;
		}
		
		if(!empty($user['ID_CLIENTE']) && $data['ID_CLIENTE'] != $user['ID_CLIENTE']){
			$this->showError('Você não pode alterar o registro acessado');
			return false;
		}
		
		if(!empty($user['ID_PRODUTO']) && $data['ID_PRODUTO'] != $user['ID_PRODUTO']){
			$this->showError('Você não pode alterar o registro acessado');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Envia um arquivo para o indesign server
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $origem
	 * @param string $destino
	 * @param int $idTemplate
	 * @return array
	 */
	protected function sendFile($origem, $novoNome,$idTemplate){
		// se o arquivo nao for indd
		if(!preg_match('@\.indd$@i', $novoNome)){
			$path = $this->template->getPath($idTemplate);
			$path = utf8MAC($path);
			$destino = $path . $novoNome;
			
			// cria o diretorio
			$this->xinet->createPath($path);
			
			move_uploaded_file($origem, $destino);
	        $this->xinet->sincroniza($destino);
	        
	        ////////////////////////////////////////////////////////
	        // agora vamos gravar as keywords
	        $tpl = $this->template->getById($idTemplate);
			$data['MARCA'] = '';
			$data['FABRICANTE'] = '';
			$data['KEYWORDS'] = '';
			$data['CODIGO'] = '';
			$data['TIPO'] = $this->tipo_objeto->getTipoAvulsa($tpl['ID_CLIENTE']);
			$data['CLIENTE'] = $tpl['ID_CLIENTE'];
			$data['AGENCIA'] = $tpl['ID_AGENCIA'];
			
	        $id = $this->xinet->getFilePorPath($path, $novoNome);
	        
	        if(!empty($id)){
	        	$this->general->saveFileKeywords($id, $data);
	        }
	        
	        $this->template->setLinkOk($idTemplate, $novoNome);
	        return true;
	        ////////////////////////////////////////////////////////
		}
	}
	
	/**
	 * Faz a troca dentro do indesign para FPO
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id codigo do template
	 * @return void
	 */
	private function processaFPO($id){
		$tpl = $this->template->getById($id);
		$path = $this->template->getPath($id);
		$fpo = str_replace(
			$this->config->item('caminho_storage'),
			$this->config->item('caminho_storage_fpo'),
			$path
		);
		
		//parametros do indesign
		$argumentos = array(
			array('name' => 'base_indd', 'value' => ($path . $tpl['ID_TEMPLATE'].'.indd')),
			array('name' => 'path_fpo', 'value' => ($fpo))
		);
		
		$res = $this->ids->call('troca_fpo',$argumentos);
		
	}
	
	/**
	 * Download de arquivos pdf/preview do template
	 * @author juliano.polito
	 * @param $id
	 * @return void
	 */
	public function download($id){
		ob_end_clean();
		$tpl = $this->template->getById($id);
		$path = $this->template->getPath($id);
		
		$user = Sessao::get('usuario');
		
		//verifica se o usuario tem permissao para acessar este arquivo
		/*
		if(!empty($user['ID_AGENCIA']) && $tpl['ID_AGENCIA'] != $user['ID_AGENCIA']){
			return;
		}
		*/
		if(!empty($user['ID_CLIENTE']) && $tpl['ID_CLIENTE'] != $user['ID_CLIENTE']){
			return;
		}
		
		
		$file = $id.'.pdf';
		
		$filePath = utf8MAC($path.$file);
		
		if(is_file($filePath)){
			header('Content-type: application/pdf');
			header('Content-Disposition: attachment; filename="'.$file.'"');
			
			// adicionamos mais alguns headers
			// porque da problema para baixar via HTTPS pelo IE
			if (strstr($_SERVER['HTTP_USER_AGENT'], "MSIE")) {
				header('Expires: 0');
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header("Content-Transfer-Encoding: binary");
				header('Pragma: public');
			}
							
			header("Content-Length: " . filesize($filePath));
			
			if(is_file($filePath)){
				$fhandle = fopen($filePath,'r');
				while(!feof($fhandle)){
					echo fread($fhandle,1024);
				}
				fclose($fhandle);
			}
		}
	}
	
}


