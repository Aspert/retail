<?php
/**
 * Controller para gerenciar tipos de peca
 * @author Hugo Silva
 *
 */
class Tipo_peca extends MY_Controller {
	/**
	 * Construtor
	 * @author Hugo Silva
	 * @return Tipo_peca
	 */
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/tipo_peca/';
	}
	
	/**
	 * Lista os tipos de peca cadastrados
	 * 
	 * @author Hugo Silva
	 * @param int $pagina Pagina inicial da listagem
	 * @return void
	 */
	public function lista($pagina=0){
		
		if (!empty($_POST)) {
			Sessao::set('busca',$_POST);			
		}
					
		$data = (array) Sessao::get('busca');
		
		//Se o usuario estiver amarrado em algum cliente e nao estiver selecionado nenhum na tela
		$user = Sessao::get('usuario');
		if (!empty($user['ID_CLIENTE']) && empty($data['ID_CLIENTE']) ) {
			$data['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}			
		
		$limit = !empty($data['pagina']) ? $data['pagina'] : 5;
		
		$lista = $this->tipo_peca->listItems($data, $pagina, $limit);
		
		$this->data['podeAlterar'] = Sessao::hasPermission('tipo_peca','save');
		
		$this->data['clientes'] = $this->cliente->getAtivos();
		$this->data['busca'] = $data;
		$this->data['lista'] = $lista['data'];
		$this->data['paginacao'] = linkpagination($lista, $limit);
		$this->display('index');		
	}
	
	/**
	 * Exibe o formulario para edicao ou insercao de tipo de peca
	 * 
	 * @author Hugo Silva
	 * @param int $id codigo do tipo de peca
	 * @return void
	 */
	public function form($id=null){
		
		//lista de categorias selecionadas
		$list = array();
		
		//idCliente a ser usado na recuperacao do combo de categorias
		$idCliente = 0;
		
		$pracas = array();
		
		//Se for uma edicao
		if(!is_null($id)) {
			
			$_POST = $this->tipo_peca->getById($id);
			
			$categorias = array();

			$list = $this->tipoPecaLimite->listItems(Array('ID_TIPO_PECA'=>$id),0,9999999999);
			
			if( $list['total'] ){
				foreach($list['data'] as $cat){
					$categorias[] = $cat;
				}
			}
			
			// pega todas as pracas relacionadas ao cliente
			$pracas = $this->praca->getByCliente($_POST['ID_CLIENTE']);

			$pracasSelecionadas = array();

			// pega as pracas faz o for para ver se ja existe pracas selecionadas
			foreach($pracas as $p){
				$tipoPecaPraca = $this->tipoPecaPraca->getByTipoPecaPraca($_POST['ID_TIPO_PECA'], $p['ID_PRACA']);

				if(count($tipoPecaPraca) > 0){
					$p['SELECIONADO'] = 1;
				}
				else{
					$p['SELECIONADO'] = 0;
				}
				
				$pracasSelecionadas[] = $p;
			}
			
			$this->assign('pracas', $pracasSelecionadas);
			
			$this->assign('categoriasSelecionadas', $categorias);
			
			//Monta combo de categorias pelo ID_CLIENTE da peca selecionada
			$idCliente = $_POST['ID_CLIENTE']; 
			
		} else { 
		
			//Recupera dados do usuario logado
			$user = Sessao::get('usuario');
			
			// pega todas as pracas relacionadas ao cliente
			$pracas = $this->praca->getByCliente($user['ID_CLIENTE']);
		
			$this->assign('pracas', $pracas);
			
			//Monta combo de categorias pelo ID_CLIENTE do usuario
			$idCliente = $user['ID_CLIENTE'];
			
		}
		
		// agora vamos pegar as categorias e subcategorias
		$list = $this->categoria->getByCliente($idCliente);

		// para cada categorias
		foreach($list as $idx => $item){
			$list[ $idx ]['subcategorias'] = $this->subcategoria->getByCategoria($item['ID_CATEGORIA']);
		}
		
		$this->data['categorias'] = $list;
		$this->data['clientes'] = $this->cliente->getAtivos();
		$this->display('form');
	}
	
	/**
	 * Grava o tipo de peca
	 * 
	 * @author Hugo Silva
	 * @param int $id Codigo do tipo de peca
	 * @return void
	 */
	public function save($id=null){
		$_POST['ID_TIPO_PECA'] = $id;
		$_POST['FLAG_BLOQUEIO_LIMITE'] = !isset($_POST['FLAG_BLOQUEIO_LIMITE'])?0:1;
		
		$erros = $this->tipo_peca->validate($_POST);
		$limites = Array();

		if(empty($erros)){
			if( isset($_POST['limiteMinimo']) ){
				foreach( $_POST['limiteMinimo'] as $id_subcatagoria => $valor ){
					$arrLimite = Array();
					$arrLimite['ID_SUBCATEGORIA'] = $id_subcatagoria;				
					$arrLimite['LIMITE_MINIMO'] = $valor;				
					$arrLimite['LIMITE_MAXIMO'] = $_POST['limiteMaximo'][$id_subcatagoria];
					$limites[] = $arrLimite;
					unset($_POST['limiteMaximo'][$id_subcatagoria]);
					unset($_POST['limiteMinimo'][$id_subcatagoria]);
				}
				unset($_POST['limiteMaximo']);
				unset($_POST['limiteMinimo']);
			}
			$this->tipoPecaLimite->deleteAll(array('ID_TIPO_PECA'=>$id));
			$this->tipoPecaPraca->deleteAll(array('ID_TIPO_PECA'=>$id));
			
			$id = $this->tipo_peca->save($_POST,$id);
			
			foreach( $limites as $limite ){
				$limite['ID_TIPO_PECA'] = $id;
				$this->tipoPecaLimite->save($limite);
			}
			
			foreach($_POST['pracas'] as $idPraca){
				if(is_numeric($idPraca)){
					$this->tipoPecaPraca->save(array('ID_TIPO_PECA'=>$id, 'ID_PRACA'=>$idPraca));
				}
			}
			
			redirect('tipo_peca/lista');
		}
		
		$this->assign('erros', $erros);
		$this->form($id);
	}
	
}