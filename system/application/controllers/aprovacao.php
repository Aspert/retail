<?php

class Aprovacao extends MY_Controller {

	public $path;
	private $debug = false;

	public function __construct(){
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/aprovacao/';
		$this->path = $this->config->item('aprovacao_path');
	}


	public function index(){
		//verifica se é um usuario de cliente antes de exibir a pauta
		$this->checaPautaCliente();
		$this->checaPautaAgencia();
	}

	/**
	 * Lista os jobs para aprovacao - visao da cliente
	 *
	 * @author juliano.polito
	 * @param int $pagina
	 * @return void
	 */
	public function listar_cliente($pagina = 0){

		//verifica se é um usuario de cliente antes de exibir a pauta
		$this->checaPautaCliente();

		if(!empty($_POST)){
			Sessao::set('busca', $_POST);
		}

		$busca = (array) Sessao::get('busca');
		//$busca['ID_ETAPA'] = array($this->etapa->getIdByKey('APROVACAO'),$this->etapa->getIdByKey('FINALIZADO'));
		$busca['ID_ETAPA'] = array($this->etapa->getIdByKey('APROVACAO'));
		$busca['PRODUTOS'] = getValoresChave(getProdutosUsuario(),'ID_PRODUTO');
		//$busca['COM_REVISAO'] = 1;

		$user = Sessao::get('usuario');

		$this->loadFilters($busca);

		// verificamos se tem algo na sessao
		// nestes casos independem do formulario
		$this->setSessionData($busca);

		if(empty($busca['ORDER'])){
			$busca['ORDER'] = 'E.ORDEM_ETAPA, DATA_INICIO';
		}

		if(empty($busca['ORDER_DIRECTION'])){
			$busca['ORDER_DIRECTION'] = 'ASC';
		}

		$busca['STATUS_CAMPANHA'] = 1;
		
		//Retira os jobs bloqueados da lista
		$busca['NOT_ID_STATUS'] = array( $this->status->getIdByKey('BLOQUEADO') );

		$limit = empty($busca['pagina']) ? 0 : $busca['pagina'];
		$lista = $this->excel->listItems($busca, $pagina, $limit, $busca['ORDER'], $busca['ORDER_DIRECTION']);

		$this->data['busca'] = $busca;
		$this->data['listaStatus'] = $this->status->getByKeys(array('EM_APROVACAO','APROVADO','EM_ALTERACAO'));
		$this->data['lista'] = $lista['data'];
		$this->data['paginacao'] = linkpagination($lista , $limit);

		$this->data['podeAprovar'] = Sessao::hasPermission('aprovacao','form_cliente');
		$this->data['verHistorico'] = Sessao::hasPermission('aprovacao','historico');

		$this->display('index_cliente');
	}

	/**
	 * Lista Jobs para aprovacao - visao agencia
	 * @author juliano.polito
	 * @param int $pagina
	 * @return void
	 */
	public function listar_agencia($pagina = 0){

		//verifica se é um usuario de cliente antes de exibir a pauta
		$this->checaPautaAgencia();

		if(!empty($_POST)){
			Sessao::set('busca', $_POST);
		}

		$busca = (array) Sessao::get('busca');
		//$busca['ID_ETAPA'] = array($this->etapa->getIdByKey('ENVIADO_AGENCIA'),$this->etapa->getIdByKey('APROVACAO'),$this->etapa->getIdByKey('FINALIZADO'));
		$busca['ID_ETAPA'] = array($this->etapa->getIdByKey('ENVIADO_AGENCIA'));
		$busca['PRODUTOS'] = getValoresChave(getProdutosUsuario(),'ID_PRODUTO');

		$user = Sessao::get('usuario');

		$this->loadFilters($busca);

		// verificamos se tem algo na sessao
		// nestes casos independem do formulario
		$this->setSessionData($busca);

		if(empty($busca['ORDER'])){
			$busca['ORDER'] = 'E.ORDEM_ETAPA';
		}

		if(empty($busca['ORDER_DIRECTION'])){
			$busca['ORDER_DIRECTION'] = 'ASC';
		}

		$busca['STATUS_CAMPANHA'] = 1;

		//Retira os jobs bloqueados da lista
		$busca['NOT_ID_STATUS'] = array( $this->status->getIdByKey('BLOQUEADO') );
		
		$limit = empty($busca['pagina']) ? 0 : $busca['pagina'];
		$lista = $this->excel->listItems($busca, $pagina, $limit, $busca['ORDER'], $busca['ORDER_DIRECTION']);

		$this->data['busca'] = $busca;
		$this->data['listaStatus'] = $this->status->getByKeys(array('EM_APROVACAO','APROVADO','EM_ALTERACAO'));
		$this->data['lista'] = $lista['data'];
		$this->data['paginacao'] = linkpagination($lista , $limit);

		$this->data['podeAprovar'] = Sessao::hasPermission('aprovacao','form_agencia') && Sessao::hasPermission('aprovacao','form_arquivo') ;
		$this->data['verHistorico'] = Sessao::hasPermission('aprovacao','historico');


		$this->display('index_agencia');
	}

	/**
	 * Exibe a tela para solicitação de aprovação da agencia
	 *
	 * @author juliano.polito
	 * @param int $idexcel
	 * @return void
	 */
	public function form_agencia($idexcel){
		//verifica se é um usuario de cliente antes de exibir a pauta
		$this->checaPautaAgencia();

		$xls = $this->excel->getById($idexcel);

		$this->checaPermissaoJob($xls['ID_JOB']);

		$user = Sessao::get('usuario');

		//Pega a ultima aprovacao nao enviada ao cliente
		$ap = $this->aprovacao->getLastByJob($xls['ID_JOB']);
		$idap = 0;
		if(empty($ap)){
			//se nao existir uma nao enviada ao cliente, criamos uma nova
			$idap = $this->aprovacao->saveNew($xls['ID_JOB'], $user['ID_USUARIO']);
		}else{
			$idap = $ap['ID_APROVACAO'];
		}
		
		//$this->assign('job', $this->job->getById($xls['ID_JOB']));
		//$this->assign('idap', $idap);
		redirect('aprovacao/form_arquivo/'.$idap);
	}

	/**
	 * Exibe a tela para vizualização de aprovação do cliente
	 *
	 * @author juliano.polito
	 * @param int $idexcel
	 * @return void
	 */
	public function form_cliente($idexcel){
		//verifica se é um usuario de cliente antes de exibir a pauta
		$this->checaPautaCliente();

		$xls = $this->excel->getById($idexcel);

		$this->checaPermissaoJob($xls['ID_JOB']);

		$aprovacoes = $this->aprovacao->getByJob($xls['ID_JOB'], true);

		foreach($aprovacoes as &$item){
			$item['arquivos'] = $this->aprovacao->getArquivosAprovacao($item['ID_APROVACAO'], false, 0);
		}

		$etapasProibidas = array(
			$this->etapa->getIdByKey('ENVIADO_AGENCIA'),
			$this->etapa->getIdByKey('FINALIZADO'),
		);

		$job = $this->job->getById($xls['ID_JOB']);
		
		$this->assign('job', $job);
		$this->assign('historico', $aprovacoes);
		$this->assign('podeComentarArquivo', Sessao::hasPermission('aprovacao','comentarArquivo') && !in_array($job['ID_ETAPA'],$etapasProibidas));
		//print_rr($aprovacoes);die;
		$this->display('visualiza_aprovacao');
	}

	/**
	 * Form de envio de arquivos para uma aprovacao
	 * @author juliano.polito
	 * @param int $idaprovacao
	 * @return void
	 */
	public function form_arquivo($idaprovacao){
		
		$ap = $this->aprovacao->getById($idaprovacao);
		
		//verifica se é um usuario de cliente e se o job esta bloqueado antes de exibir a pauta
		$this->checaPautaAgencia($ap['ID_JOB']);
		
		$job = $this->job->getById($ap['ID_JOB']);
		
		$aprovacoes = $this->aprovacao->getByJob($ap['ID_JOB'], FALSE);
		
		$idAprovacaoNaoEnviado = '';
		foreach ($aprovacoes as $key => $aprovacao){
			if($aprovacao['ENVIADO_APROVACAO'] == 0){
				$idAprovacaoNaoEnviado = $aprovacao['ID_APROVACAO'];
				unset($aprovacoes[$key]);
			}
		}
		
		//aqui armazenamos a URL para que possa ser usado no botão copiar URL
		$proto = strtolower(preg_replace('/[^a-zA-Z]/','',$_SERVER['SERVER_PROTOCOL'])); //pegando só o que for letra
		$location = $proto.'://'.$_SERVER['HTTP_HOST'].'/index.php/aprovacao/form_cliente/'.$job['ID_EXCEL'];
				
		$arquivos = $this->aprovacaoArquivo->getByAprovacao($idaprovacao);
		
		$this->assign('arquivos', $arquivos);
		$this->assign('job', $job);
		$this->assign('URL', $location);
		$this->assign('idAprovacaoNaoEnviado', $idAprovacaoNaoEnviado);
		$this->assign('ap', $ap);
		$this->assign('historico', $aprovacoes);
		$this->assign('podeEnviarArquivo', Sessao::hasPermission('aprovacao','enviar_arquivo'));
		$this->assign('podeExcluir', Sessao::hasPermission('aprovacao','excluir_arquivo'));
		$this->assign('podeDownload', Sessao::hasPermission('aprovacao','visualizarPDFAP'));
		//print_rr($arquivos);die;
		$this->display('form_arquivo');
	}

	/**
	 * Envia/upload do arquivo pdf para uma aprovacao
	 * @author juliano.polito
	 * @param int $idAprovacao
	 * @return void
	 */
	public function enviar_arquivo($idAprovacao){
		
		//$this->debug('post '.print_r($_POST,true) );
		//$this->debug('file '.print_r($_FILES,true) );
		$this->checaPautaAgencia();
		$user = Sessao::get('usuario');

		set_time_limit(0);
		if(!empty($_FILES['ARQUIVO']) && is_uploaded_file($_FILES['ARQUIVO']['tmp_name'])){

			$titulo = $_FILES['ARQUIVO']['name'];

			//Grava o registro do arquivo
			$dataArquivo = array(
					'ID_USUARIO' => $user['ID_USUARIO'],
					'TITULO_APROVACAO_ARQUIVO' => $_FILES['ARQUIVO']['name'],
					'ID_APROVACAO' => $idAprovacao,
					'DATA_CADASTRO' => date('Y-m-d H:i:s')
			);
			$idArquivo = $this->aprovacaoArquivo->save($dataArquivo);

			//Se houver comentario associado, grava tambem
			if(!empty($_POST['COMENTARIO'])){
				$dataComentario = array(
						'ID_USUARIO' => $user['ID_USUARIO'],
						'COMENTARIO' => post('COMENTARIO',true),
						'DATA_COMENTARIO' => date('Y-m-d H:i:s')
				);
				$idComentario = $this->aprovacaoComentario->save($dataComentario);
				
			}
			
			$dataComentarioArquivo = array(
					'ID_APROVACAO_ARQUIVO' => $idArquivo,
					'ID_APROVACAO_COMENTARIO' => !empty($idComentario) ? $idComentario : null,
					'IS_ARQUIVO_CLIENTE' => 0,
			);
			//salvando o relacionamento entre o comentario e o arquivo
			$this->aprovacaoComentarioArquivo->save($dataComentarioArquivo);

			//Pega o novo nome do arquivo
			$newPath = $this->aprovacaoArquivo->getPathArquivo($idArquivo);

			//move para a pasta destino
			$ok = move_uploaded_file($_FILES['ARQUIVO']['tmp_name'], $newPath);
			if(!$ok){
				//caso de erro no envio do arquivo, exclui o registro
				$this->aprovacaoArquivo->delete($idArquivo);
				$erros +=  array('Falha ao gravar arquivo, tente novamente.');
			}
		}

		echo 'enviado';
		//redirect('aprovacao/form_arquivo/'.$idAprovacao);

	}
	
	/**
	 * Adiciona um comentario a um arquivo
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idarquivo codigo do arquivo
	 * @return void
	 */
	public function comentarArquivo($idarquivo,$idAprovacao) {
	
	
		$user = Sessao::get('usuario');
	
		$pathProva = $this->aprovacaoArquivo->getPathArquivo($idarquivo);
		$pathArquivoCliente = explode('/', $pathProva);
		$pathArquivoCliente1 = array_pop($pathArquivoCliente);
		$pathArquivoCliente = implode('/', $pathArquivoCliente) . '/ARQUIVOS_CLIENTE/';
	
		if(!is_dir($pathArquivoCliente)){
			mkdir($pathArquivoCliente,0777,true);
			chmod($pathArquivoCliente, 0777);
		}
	
		$Arquivo = array();
		$Arquivo[] = $idarquivo;
		foreach ($_FILES as $file){
			if(!empty($file['tmp_name'])){
				// colocamos o(s) arquivo(s) na pasta de destino
				move_uploaded_file($file['tmp_name'], $pathArquivoCliente.$file['name']);
	
				$dataArquivo = array(
						'ID_USUARIO' => $user['ID_USUARIO'],
						'TITULO_APROVACAO_ARQUIVO' => $file['name'],
						'ID_APROVACAO' => $idAprovacao,
						'DATA_CADASTRO' => date('Y-m-d H:i:s')
				);
	
				//salvando o arquivo
				$Arquivo[] = $this->aprovacaoArquivo->save($dataArquivo);
			}
		}
	
		$dataComentario = array(
				'ID_USUARIO' => $user['ID_USUARIO'],
				'COMENTARIO' => post('COMENTARIO',true),
				'DATA_COMENTARIO' => date('Y-m-d H:i:s')
		);
		//salvando o comentario
		$Comentario = $this->aprovacaoComentario->save($dataComentario);
	
		foreach ($Arquivo as $arqui) {
			$dataComentarioArquivo = array(
					'ID_APROVACAO_ARQUIVO' => $arqui,
					'ID_APROVACAO_COMENTARIO' => $Comentario,
					'IS_ARQUIVO_CLIENTE' => 1,
			);
			//salvando o relacionamento entre o comentario e o arquivo
			$this->aprovacaoComentarioArquivo->save($dataComentarioArquivo);
		}
	
		echo 'comentado';
	}

	/**
	 * Exclui arquivo pelo id
	 * @author juliano.polito
	 * @param int $idarquivo
	 * @return void
	 */
	public function excluir_arquivo($idarquivo){
		$this->checaPautaAgencia();
		$arquivo = $this->aprovacaoArquivo->getById($idarquivo);
		$pathArquivo = $this->aprovacaoArquivo->getPathArquivo($idarquivo);
		if(is_file($pathArquivo)){
			if(unlink($pathArquivo)){
				//exclui o registro do arquivo - o cascae exclui os comentarios associados
				$this->aprovacaoArquivo->delete($idarquivo);
			}
		}
		redirect('aprovacao/form_arquivo/'.$arquivo['ID_APROVACAO']);
	}

	/**
	 * Solicita a aprovação do job ao cliente - envia para a pauta de aprovacao do cliente
	 *
	 * @author juliano.polito
	 * @param int $id Codigo do excel
	 * @return void
	 */
	public function solicitar($idjob, $idaprovacao){
		
		//verifica se o usuário é agência
		$this->checaPautaAgencia();
		//verifica se esta agência tem direito de acessar o job em questão
		$this->checaPermissaoJob($idjob);

		$arquivos = $this->aprovacaoArquivo->getByAprovacao($idaprovacao);

		if(count($arquivos) == 0){
			$erros = array('É obrigatório enviar ao menos um arquivo para aprovação');
			$this->assign('erros',$erros);
			$this->form_arquivo($idaprovacao);
		}else{
			//objetos do job
			$ap = $this->aprovacao->getById($idaprovacao);
			$job = $this->job->getById($idjob);
			$excel = $this->excel->getById($job['ID_EXCEL']);
			$user = Sessao::get('usuario');

			//gera o registro no array
			$ap['ID_USUARIO']=$user['ID_USUARIO'];
			$ap['ID_OLDSTATUS']=$job['ID_STATUS'];
			$ap['ID_STATUS']=$this->status->getIdByKey('EM_APROVACAO');
			$ap['DATA_SOLICITACAO']=date('Y-m-d H:i:s');
			$ap['ENVIADO_APROVACAO']= 1;

			//salva a aprovação
			$id = $this->aprovacao->save($ap, $ap['ID_APROVACAO']);

			//altera a etapa do job
			//$this->job->mudaEtapa($idjob, $this->etapa->getIdByKey('APROVACAO'), $this->status->getIdByKey('EM_APROVACAO'), $user);
			$this->job->mudaEtapa($idjob, $user, 1);

			$erros = $this->email->sendEmailRegra('ap_solicita_aprovacao',
												'Aprovação solicitada para JOB '.$excel['ID_JOB'],
												'ROOT/templates/ap_solicita_aprovacao',
												array('job'=>$job, 'aprovador'=>$user),
												$job['ID_AGENCIA'],
												$job['ID_CLIENTE']
												);

			redirect('aprovacao/listar_agencia');
		}

	}

	/**
	 * Aprova ou solicita alterações de um job
	 *
	 * @author juliano.polito
	 * @param int $idjob
	 * @return void
	 */
	public function aprovar($idjob){
		
		//objetos job
		$job = $this->job->getById($idjob);
		
		//Apenas clientes podem aprovar
		//Verificamos se é um cliente e se o job nao esta bloqueado
		$this->checaPautaCliente($idjob);
		
		//$excel = $this->excel->getById($job['ID_EXCEL']);
		$user = Sessao::get('usuario');

		//acao é obrigatoria. 1 Aprovado, 0 Alteracoes
		if(isset($_POST['acao']) && $_POST['acao'] !== '' && !empty($job)){

			//acao selecionada
			$acao = $_POST['acao'];

			//Valida os campos e o envio do arquivo
			$erros = $this->aprovacao->validate($_POST);
			//if($acao == 0 && empty($_POST['OBS_APROVACAO'])){
			//	$erros['OBS_APROVACAO'] = 'Comentários são obrigatórios para solicitação de alteração.';
			//}

			if(empty($erros)){
				if($acao == 1){
					//gera o registro no array
					$ap =array_merge(array(
					'ID_JOB'=>$idjob,
					'ID_USUARIO_APROVADOR'=>$user['ID_USUARIO'],
					'OBS_APROVACAO' => 'Job aprovado por ' . $user['NOME_USUARIO'],
					'ID_OLDSTATUS'=>$job['ID_STATUS'],
					'ID_STATUS'=>$this->status->getIdByKey('APROVADO'),
					'DATA_APROVACAO'=>date('Y-m-d H:i:s')
					),$_POST);

					//novo status do job
					$jobstatus = 'APROVADO';

					$erros = $this->email->sendEmailRegra('ap_job_aprovado',
														'JOB '.$job['ID_JOB'].' foi aprovado.',
														'ROOT/templates/ap_job_aprovado',
														array('job'=>$job, 'aprovador'=>$user),
														$job['ID_AGENCIA'],
														$job['ID_CLIENTE']
														);

				}else if($acao == 0){
					//gera o registro no array
					$ap =array_merge(array(
					'ID_JOB'=>$idjob,
					'ID_USUARIO_APROVADOR'=>$user['ID_USUARIO'],
					'OBS_APROVACAO' => 'Alterações solicitadas por ' . $user['NOME_USUARIO'],
					'ID_OLDSTATUS'=>$job['ID_STATUS'],
					'ID_STATUS'=>$this->status->getIdByKey('ELABORACAO'),
					'DATA_APROVACAO'=>date('Y-m-d H:i:s')
					),$_POST);

					//novo status do job
					$jobstatus = 'ELABORACAO';

					$erros = $this->email->sendEmailRegra('ap_solicita_alteracao',
														'Alteração solicitada para JOB '.$job['ID_JOB'],
														'ROOT/templates/ap_solicita_alteracao',
														array('job'=>$job, 'aprovador'=>$user),
														$job['ID_AGENCIA'],
														$job['ID_CLIENTE']
														);

				}

				$ultima = $this->aprovacao->getLastByJob($idjob,false,true);
				$id = $ultima['ID_APROVACAO'];

				//salva a aprovacao
				$this->aprovacao->save($ap, $id);

				//altera etapa e status do job
				//$this->job->mudaEtapa($idjob, $this->etapa->getIdByKey('ENVIADO_AGENCIA'), $this->status->getIdByKey($jobstatus), $user);
				$this->job->mudaEtapa($idjob, $user);

				// agora gravamos o historico de alteracao - informacao fica duplicada em dois historicos para aparecer na pauta
				$this->job->addHistorico($idjob, $this->status->getIdByKey($jobstatus),$this->etapa->getIdByKey('ENVIADO_AGENCIA'), $_POST['OBS_APROVACAO']);

				//volta para a pauta
				redirect('aprovacao/listar_cliente');

			}else{
				$_REQUEST['erros'] = $erros;
				$this->form_cliente($job['ID_EXCEL']);
			}
		}else{
			//volta para a pauta
			redirect('aprovacao/listar_cliente');
		}

	}

	/**
	 * Faz o stream da versao especifica de arquivo pdf para aprovação
	 *
	 * @author juliano.polito
	 * @param int $id Codigo do arquivo
	 * @return void
	 */
	public function visualizarPDFAP($id, $attach = 1,$IsArquivoCliente=false){
		
		$arquivo = $this->aprovacaoArquivo->getById($id);
		$ap = $this->aprovacao->getById($arquivo['ID_APROVACAO']);
		$idjob = $ap['ID_JOB'];

		//verifica se o usuário pode ver o job
		$ok = $this->checaPermissaoJob($idjob, false);

		//caso não tenha direito de baixar o job, retorna
		if(!$ok){
			return;
		}

		//Path do último arquivo disponível para aprovação
		$path = $this->aprovacaoArquivo->getPathArquivo($id,$IsArquivoCliente);
				
		//faz stream do arquivo
		streamFile($path,$arquivo['TITULO_APROVACAO_ARQUIVO'], $attach);
	}

	/**
	 * Mostra o historico de um job
	 *
	 * @author juliano.polito
	 * @link http://www.247id.com.br
	 * @param int $id
	 * @return void
	 */
	public function historico($idexcel){
		
		$xls = $this->excel->getById($idexcel);

		$this->checaPermissaoJob($xls['ID_JOB']);

		$aprovacoes = $this->aprovacao->getByJob($xls['ID_JOB'], true);

		foreach($aprovacoes as &$item){
			$item['arquivos'] = $this->aprovacao->getArquivosAprovacao($item['ID_APROVACAO'], false);
		}

		$this->assign('job', $this->job->getById($xls['ID_JOB']));
		$this->assign('historico', $aprovacoes);
		$this->assign('podeAprovar', false);
		$this->assign('podeComentarArquivo', false);
		$this->assign('podeVisualizarPDFAP', false);
		$this->assign('podeAprovarCheck', Sessao::hasPermission('checklist','aprovacao'));

		$this->display('visualiza_aprovacao');
	}



	/**
	 * Carrega os filtros para exibir na pagina
	 *
	 * @author juliano.polito
	 * @link http://www.247id.com.br
	 * @param array $data
	 * @return void
	 */
	private function loadFilters(array $data){
		
		$user = Sessao::get('usuario');
		// verificamos se tem algo na sessao
		// nestes casos independem do formulario
		if(!empty($user['ID_AGENCIA'])){
			$data['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}

		if(!empty($user['ID_CLIENTE'])){
			$data['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}

		if(!empty($user['ID_PRODUTO'])){
			$data['ID_PRODUTO'] = $user['ID_PRODUTO'];
		}

		//////////////////////////////////////////////////////
		// filtros
		//////////////////////////////////////////////////////
		$user['STATUS_AGENCIA'] = 1;
		$this->assign('produtos', array());

		if(!empty($data['ID_AGENCIA'])){
			$this->assign('clientes', $this->cliente->getByAgencia($data['ID_AGENCIA']));
		} else {
			$this->assign('clientes', array());
		}

		if(!empty($data['ID_CLIENTE'])){
			$this->assign('agencias', $this->agencia->getByCliente($data['ID_CLIENTE']));
			$this->assign('produtos', getProdutosUsuario(val($data,'ID_CLIENTE'), val($data,'ID_AGENCIA')));
		} else {
			$this->assign('agencias', array());
		}

		if(!empty($data['ID_PRODUTO'])){
			$cl = $this->campanha->listItems(array("ID_PRODUTO"=>$data['ID_PRODUTO'],'STATUS_CAMPANHA'=>1),0,10000);
			$this->data['campanhas'] = $cl['data'];
		} else {
			$this->data['campanhas'] = array();
		}


		$tipo = $this->tipoPeca->getAll(0,1000,'DESC_TIPO_PECA');
		$this->data['tipos'] = $tipo['data'];

		$tipo = $this->tipoJob->getAll(0,1000,'DESC_TIPO_JOB');
		$this->data['tiposJob'] = $tipo['data'];
	}

	/**
	 * Verifica se o usuario é do tipo cliente ou agência.
	 * Caso seja cliente, redireciona para a pauta de agencia e vice-versa.
	 * @author juliano.polito
	 * @return void
	 */
	protected function checaPautaCliente( $idjob = 0 ){
		
		$user = Sessao::get('usuario');
		$goto = 'aprovacao/listar_agencia';
		
		if ($idjob > 0) {
			//Verifica se o job nao esta bloqueado
			$job = $this->job->getById($idjob);
			if ( $job['ID_STATUS'] == $this->status->getIdByKey('BLOQUEADO') ) {
				redirect($goto);
			}
		}
		
		if(empty($user['ID_CLIENTE'])){
			redirect($goto);
		}

	}
	protected function checaPautaAgencia( $idjob = 0 ){
		
		$user = Sessao::get('usuario');
		$goto = 'aprovacao/listar_cliente';
		
		if ($idjob > 0) {
			//Verifica se o job nao esta bloqueado
			$job = $this->job->getById($idjob);
			if ( $job['ID_STATUS'] == $this->status->getIdByKey('BLOQUEADO') ) {
				redirect($goto);
			}
		}
		
		if(empty($user['ID_AGENCIA'])){
			redirect($goto);
		}

	}

	/**
	 * Checa se o usuario logado tem permissao ou nao de alterar o job acessado
	 *
	 * Se nao tiver permissao, redireciona para a listagem
	 *
	 * @author juliano.polito
	 * @param int $idjob
	 * @return void
	 */
	protected function checaPermissaoJob($idjob, $redirect = true){
		
		$user = Sessao::get('usuario');
		$job = $this->job->getById($idjob);
		$produtos = getValoresChave(getProdutosUsuario($job['ID_CLIENTE'],$job['ID_AGENCIA']),'ID_PRODUTO');

		
		if(!empty($user['ID_CLIENTE'])){
			$goto = 'gerar_indd/listar_cliente';
			//Verifica se o job nao esta bloqueado
			if ( $job['ID_STATUS'] == $this->status->getIdByKey('BLOQUEADO') ) {
				redirect($goto);
			}
		
			if($job['ID_CLIENTE'] != $user['ID_CLIENTE']){
				if($redirect){
					redirect($goto);
				}else{
					return false;
				}
			}

			if(!in_array($job['ID_PRODUTO'],$produtos)){
				if($redirect){
					redirect($goto);
				}else{
					return false;
				}
			}
		}

		if(!empty($user['ID_AGENCIA'])){
			$goto = 'gerar_indd/listar_agencia';
			//Verifica se o job nao esta bloqueado
			if ( $job['ID_STATUS'] == $this->status->getIdByKey('BLOQUEADO') ) {
				redirect($goto);
			}
			
			if($user['ID_AGENCIA'] != $job['ID_AGENCIA']){
				if($redirect){
					redirect($goto);
				}else{
					return false;
				}
			}

			$clientes = getValoresChave($this->cliente->getByAgencia($user['ID_AGENCIA']),'ID_CLIENTE');
			if(!in_array($job['ID_PRODUTO'],$produtos)){
				if($redirect){
					redirect($goto);
				}else{
					return false;
				}
			}

			if(!in_array($job['ID_CLIENTE'],$clientes)){
				if($redirect){
					redirect($goto);
				}else{
					return false;
				}
			}
		}

		return true;
	}
	
	/**
	 * Muda o job para a proxima etapa ou volta etapa do processo
     * Caso etapa = 2 volta etapa
	 * @author Diego Andrade
	 * @param int $idjob
	 * @param int $etapa
	 * @return void
	 */
	
	public function proxima_etapa ($idjob, $etapa = 0)
	{
		$user = Sessao::get('usuario');
		$job = $this->job->getById($idjob);
		$goto = 'aprovacao/listar_cliente';
		
		//Verifica se o job nao esta bloqueado
		if ( $job['ID_STATUS'] == $this->status->getIdByKey('BLOQUEADO') ) {
			redirect($goto);
		}
		if ($etapa == 2) {
            $this->job->voltaEtapa($idjob,$user);
        } else {
            $this->job->mudaEtapa($idjob, $user, 1);
        }
		redirect($goto);
	}
	
	private function debug($str) {
		$str = "---------------------------\n$str\n====\n\n";
		if($this->debug) {
			file_put_contents('system/application/controllers/uploadDebug.txt', $str, FILE_APPEND);
		}
	}
	
}