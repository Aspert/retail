<?php
/**
 * Controller para gerenciar Pracas
 * 
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class Pracas extends MY_Controller {
	
	/**
	 * Construtor
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Pracas
	 */
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/pracas/';
	}
	
	/**
	 * Lista as pracas cadastradas
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $pagina Numero da pagina atual
	 * @return void
	 */
	function lista($pagina = 0) {
		if ( !empty($_POST) ) {
			Sessao::set('busca', $_POST);
		}

		$busca = (array) Sessao::get('busca');
		
		$clientes = $this->cliente->listItems(array(), 0, 10000);
		$regioes = array();
		
		if(!empty($busca['ID_CLIENTE'])){
			$regioes = $this->regiao->getByCliente($busca['ID_CLIENTE']);
		}
		
		
		$limit = empty($busca['pagina']) ? 0 : $busca['pagina'];
		$result = $this->praca->listItems($busca, $pagina, $limit);
		
		$this->assign('regioes', $regioes);
		$this->assign('busca', $busca);
		$this->assign('clientes', $clientes['data']);
		$this->assign('lista', $result['data']);
		$this->assign('paginacao', linkpagination($result,$limit));
		$this->assign('total', $result['total']);
		
		$this->assign('podeAlterar', Sessao::hasPermission('pracas','save'));
		
		$this->display('index');
	}

	/**
	 * Exibe o formulario de edicao 
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do registro quando editando
	 * @return void
	 */
	function form($id=null) {
		$user = Sessao::get('usuario');
		
		$this->assign('regioes', array());
		
		$filters = array();
		$filters['STATUS_CLIENTE'] = 1;
		
		// se o usuario logado for cliente
		if( !empty($user['ID_CLIENTE']) ){
			$filters['ID_CLIENTE'] = $user['ID_CLIENTE'];
		} 
		
		$clientes = $this->cliente->listItems($filters, 0, 10000);
		
		if( !is_null($id) && $_SERVER['REQUEST_METHOD'] == 'GET' ){
			$_POST = $this->praca->getById($id);
			
			$regiao = $this->regiao->getById($_POST['ID_REGIAO']);
			$filters['ID_CLIENTE'] = $regiao['ID_CLIENTE'];
			
			$regioes = $this->regiao->listItems($filters, 0, 10000);
			$this->assign('regioes', $regioes['data']);
			
			$_POST['ID_CLIENTE'] = $regiao['ID_CLIENTE'];
		}
		

		$this->assign('clientes', $clientes['data']);
		
		$this->display('form');
	}
	
	/**
	 * Grava um novo registro ou atualiza um existente
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do registro a ser alterado ou null para um novo
	 * @return void
	 */
	function save($id=null) {
		$_POST['ID_PRACA'] = sprintf('%d', $id);
		// faz a validacao dos dados
		$res = $this->praca->validate($_POST);
		// se nao houve erros
		if( empty($res) ){
			//grava os dados no banco
			$this->praca->save($_POST,$id);
			// redireciona para a pagina
			redirect('pracas/lista');
		}
		
		// indica os erros
		$this->assign('erros', $res);
		// carrega o formulario
		$this->form($id);
	}
}
