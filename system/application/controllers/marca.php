<?php
/**
 * Controller para gerenciar marcas
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class Marca extends MY_Controller {
	
	/**
	 * Construtor
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return Fabricante
	 */
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/marca/';
	}
	
	/**
	 * Lista as marcas cadastradas
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $pagina Numero da pagina atual
	 * @return void
	 */
	function lista($pagina = 0) {
		if ( !empty($_POST) )  {
			Sessao::set('busca',$_POST);
		}
		
		$busca = (array) Sessao::get('busca');
		$limit = !empty($busca['pagina']) ? $busca['pagina'] : '';
		$marcas = $this->marca->listItems($busca, $pagina, $limit);
		$fabricantes = $this->fabricante->getAll(0,1000,'DESC_FABRICANTE');
		
		$this->data['podeAlterar'] = Sessao::hasPermission('marca','save');
		$this->data['busca'] = $busca;		
		$this->data['marcas'] = $marcas['data'];		
		$this->data['fabricantes'] = $fabricantes['data'];		
		$this->data['paginacao'] = linkpagination($marcas, $limit);
		
		$this->display('index');			
	}	
	
	/**
	 * Exibe o formulario de edicao
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo da marca quando editando
	 * @return void
	 */
	function form($id=null) {
		if($id!=null && $_SERVER['REQUEST_METHOD'] == 'GET'){
			$_POST = $this->marca->getById($id);
		}
		
		$fabricantes = $this->fabricante->getAll(0,1000,'DESC_FABRICANTE');
		$this->data['fabricantes'] = $fabricantes['data'];		
		
		$this->display('form');
	}
	
	/**
	 * Persiste os dados de uma marca
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo da marca 
	 * @return void
	 */
	function save($id=null) {
		$erros = $this->marca->validate($_POST);
		
		if(empty($erros)){
			$this->marca->save($_POST,$id);
			redirect('marca/lista');
		}
		
		$_REQUEST['erros'] = $erros;
		$this->data['erros'] = $erros;
		$this->form($id);
	}		
} 


