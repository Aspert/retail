<?php
class Campo_ficha extends MY_Controller {
	function Campo_ficha() {
		parent :: Controller();
	}
	
	// acessa o model para sele��o de campos 
	function getAllCampoFicha(){
		if(!empty($ID)){
			$this->campo_ficha->getAllCampoFicha($ID);
		}
	}
	
	// insere os labels e valores na tabela
	function saveCampoFicha($campo, $valor, $id, $id_campo){
		$this->campo_ficha->saveCampoFicha($campo, $valor, $id, $id_campo);
	}
	
	// remove um label e valor
	function deleteCampoFicha($id, $id_ficha){
		if(!empty($id) && !empty($id_ficha)){
			
			// pega o label da tabela campo_ficha
			$id_obj_ficha = $this->campo_ficha->getLabel($id);
			
			// remove da tabela de elemento para o layout da ficha
			$this->elemento_ficha->removeObjFicha($id_ficha, $id_obj_ficha);
			
			$this->campo_ficha->deleteCampoFicha($id);
		}
	}
}