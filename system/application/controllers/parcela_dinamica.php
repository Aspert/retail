<?php
/**
 * controller para gerenciar o cadastro de parcela dinamica
 *
 * @author Sidnei
 * @link http://www.247id.com.br
 */
class Parcela_dinamica extends MY_Controller
{
	/**
	 * Construtor
	 * @author Sidnei
	 * @link http://www.247id.com.br
	 * @return Parcela Dinamica
	 */
	function __construct() {
		parent::__construct();
		$this->_templatesBasePath = 'ROOT/parcela_dinamica/';
	}
	
	/**
	 * Lista as parcelas dinamicas cadastradas
	 * @author Sidnei
	 * @link http://www.247id.com.br
	 * @param int $pagina Pagina atual de listagem
	 * @return void
	 */
	function index($pagina = 0) {
		if ( !empty($_POST) ) {
			Sessao::set('busca', $_POST);
		}
		
		$user = Sessao::get('usuario');

		$busca = (array) Sessao::get('busca');
		$limit = 10000;
		
		$agencias = $this->agencia->listItems(array('STATUS_AGENCIA'=>1),0,10000);
		
		if(!empty($user['ID_AGENCIA'])){
			$busca['ID_AGENCIA'] = $user['ID_AGENCIA'];
		}

		if(!empty($user['ID_CLIENTE'])){
			$busca['ID_CLIENTE'] = $user['ID_CLIENTE'];
		}

		$user = Sessao::get('usuario');
		
		$this->loadFilters($busca);
	
		$parcelasDinamicas = $this->parcela_dinamica->listItems($busca, $pagina, $limit);

		$this->data['busca'] = $busca;
		$this->data['agencias'] = $agencias['data'];
		$this->data['parcelasDinamicas'] = $parcelasDinamicas['data'];
		$this->data['paginacao'] = linkpagination($parcelasDinamicas, $limit);
		$this->data['podeAlterar'] = Sessao::hasPermission('parcela_dinamica','form');
		
		$this->display('index');
	}
	
	public function form($idCliente=null){
		if(!Sessao::hasPermission('parcela_dinamica','form')){
			redirect('parcela_dinamica/index');
		}
		
		$cliente = array();
		$parcelasDinamicas = array();
		
		$user = Sessao::get('usuario');
		
		if($idCliente==null && !empty($user['ID_CLIENTE'])){
			$idCliente = $user['ID_CLIENTE'];
		}

		if( !is_null($idCliente)  ){
			$parcelasDinamicas = $this->parcela_dinamica->listByCliente($idCliente);
			$cliente = $this->cliente->getById($idCliente);
		}

		$user = Sessao::get('usuario');
		
		$this->loadFilters();

		$filters = array(
			'STATUS_CLIENTE' => 1
		);
		
		$this->data['parcelasDinamicas'] = $parcelasDinamicas;

		$this->display('form');
	}
	
	public function save($idCliente=null){
		if(!Sessao::hasPermission('parcela_dinamica','save')){
			redirect('parcela_dinamica/index');
		}

		if($_POST['ID_CLIENTE'] != '' && is_numeric($_POST['ID_CLIENTE'])){
			$this->parcela_dinamica->deleteByCliente($_POST['ID_CLIENTE']);
			if(!isset($_POST['ARREDONDA'])){
				$_POST['ARREDONDA'] = 0;
			}
			if(!isset($_POST['AJUSTA'])){
				$_POST['AJUSTA'] = 0;
			}
			for($i = 0; $i <= count($_POST['VALOR_VISTA_1']) - 1; $i++ ){
				$insert = array(
								'ID_CLIENTE' => $_POST['ID_CLIENTE'],
								'VALOR_VISTA_1' => money2float($_POST['VALOR_VISTA_1'][$i]),
								'VALOR_VISTA_2' => money2float($_POST['VALOR_VISTA_2'][$i]),
								'PARCELAS' => $_POST['PARCELAS'][$i],
								'ARREDONDA' => $_POST['ARREDONDA'],
								'AJUSTA' => $_POST['AJUSTA']
							);

				$this->parcela_dinamica->save($insert);
			}
		}	

		redirect('parcela_dinamica/index');		
	}
	
	// Carrega os filtros para exibir na pagina
	private function loadFilters($data=null){
		$user = Sessao::get('usuario');
		
		$this->data['clientes'] = array();
		
		if( !empty($user['ID_AGENCIA']) ){
			$this->data['clientes'] = $this->cliente->getByAgencia($user['ID_AGENCIA']);
		} else if( !empty($user['ID_CLIENTE']) ) {
			$item = $this->cliente->getById($user['ID_CLIENTE']);
			$this->data['clientes'] = array($item);
			$this->data['ID_CLIENTE'] = $user['ID_CLIENTE'];
		} else {
			$list = $this->cliente->listItems($data, 0, 1000000);
			$this->data['clientes'] = $list['data'];
		}
	}
}








