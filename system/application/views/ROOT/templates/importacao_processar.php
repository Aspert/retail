<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>aspert - Retail</title>
<style type="text/css">
<!--
.style2 {color: #2384C6}
a:link {
	text-decoration: none;
}
a:visited {
	text-decoration: none;
}
a:hover {
	text-decoration: underline;
}
a:active {
	text-decoration: none;
}
-->
</style>
</head>

<body style="margin:0 0 0 0">
<table width="700" border="0" align="center" cellpadding="2" cellspacing="0">
  <tr>
    <td width="120"><div align="left"><img src="<?= base_url() ?>themes/snow_white/img/01_01.gif" alt="" /></div></td>
    <td width="828"><div align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="4"><strong>aspert - Retail</strong></font></div></td>
  </tr>
  <tr>
    <td colspan="2"><img src="<?= base_url() ?>themes/snow_white/img/01_03.gif" width="100%" height="42" alt="" /></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" align="center" cellpadding="2" cellspacing="1">
      <tr>
        <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><br />
          Olá 
          <?= $__usuario['NOME_USUARIO']?>
        </span>,<br />
        Job 
        <?= $objeto['TITULO_JOB'] ?>
        </span>processado com sucesso.</font><br />
          <br /></td>
      </tr>
    </table>
      <table width="100%" border="0" align="center" cellpadding="2" cellspacing="1">
      <tr>
        <td><table width="100%" border="0" cellspacing="1" cellpadding="2">
          <tr>
            <td width="54%"><table width="100%" border="0" cellspacing="1" cellpadding="2">
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Agência: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $objeto['DESC_AGENCIA']?>
        </span></font></td>
              </tr>
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Cliente: </font></strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
                  <?= $objeto['DESC_CLIENTE']?>
                </span></font></td>
              </tr>
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Bandeira: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $objeto['DESC_PRODUTO']?>
        </span></font></td>
              </tr>
              
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Campanha: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $objeto['DESC_CAMPANHA']?>
        </span></font></td>
              </tr>
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Nome Job: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $objeto['TITULO_JOB']?>
        </span></font></td>
              </tr>
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Usu&aacute;rio Respons&aacute;vel: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $objeto['NOME_USUARIO_RESPONSAVEL']?>
        </span></font></td>
              </tr>
              <tr>
               <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Validade: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $objeto['DATA_INICIO'] .' até '.$objeto['DATA_TERMINO'] ?>
        </span></font></td>
              </tr>
			  
		<?php if(!empty($job['NOME_USUARIO'])): ?>
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Criado por: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $job['NOME_USUARIO']?>
        </span></font></td>
              </tr>
		<?php endif; ?>
              
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table></td>
            <td width="46%">  
            
              <div align="center">
              
              <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
              <? if(!empty($preview)): ?>
            	<img src="<?= $preview ?>" alt="Preview">
            <? endif; ?>
                </span></font>
                
                </div></td>
          </tr>
        </table>
          <div align="center">
            <table width="100%" border="0" cellspacing="1" cellpadding="2">
               <tr>
                  <td bgcolor="#2384C6"><div align="center">
                    <p><font face="Arial, Helvetica, sans-serif" size="2"><strong><br />
                      Foram disponibilizados os seguintes arquivos de template: </strong></font>
                      <br />
                      
	<font color="#FFFFFF" size="2" face="Verdana, Arial, Helvetica, sans-serif">				  
					  <?php
if( !empty($objeto['arquivo']) ){
	echo '<a style="color: #FFFFFF; font-weight: bold" href="'.base_url().$objeto['arquivo'].'">clique aqui para baixar o arquivo</a>';
}

echo '<br><br>' . $objeto['texto'];

?>
     </font>                 
                      <br />
                      <br />
                    </p>
                  </div></td>
                </tr>
            </table>
          </div></td>
      </tr>
     
      
    </table>
    
    <div style="color:#FFFFFF; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:30px">
   		<div align="center" class="style1"><font face="Verdana, Arial, Helvetica, sans-serif"><a href="<?= base_url().index_page() ?>" 
        	target="_blank" 
            style="text-decoration:none; color:#2384C6; font-weight:bold;" 
            title="Clique aqui">Clique aqui</a><font color="#000000"> para acessar o sistema aspert - Retail.    </font></font></div>
    </div>

	<div class="style2" style="text-decoration:none; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold" align="center">
		================= POR FAVOR NÃO RESPONDA ESTA MENSAGEM ================= <br />
		Qualquer dúvida entre em contato com o <a title="Clique aqui" href="mailto:suporte@aspert.com.br" style="text-decoration: underline;">Suporte</a> ou pelo número de telefone (11) 4349-0222
	</div>
    
    <div style="color:#FFFFFF; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:30px">
      <div align="center"><font color="#000000" face="Verdana, Arial, Helvetica, sans-serif"><strong>24\7 Inteligência Digital</strong><br />
   	        
   	        Fone (11) 4349-0222<br />
   	        </font><font face="Verdana, Arial, Helvetica, sans-serif"><a href="#" title="Clique aqui" target="_blank" class="style2" style="text-decoration:none; font-weight:bold;">http://aspert.com.br</a></font><br />
   	            <br />
        </div>
  </div>
    
    </td>
  </tr>
</table>
</body>
</html>
