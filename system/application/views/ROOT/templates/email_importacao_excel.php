<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>aspert - Retail</title>
<style type="text/css">
<!--
.style1 {
	font-size: 12px
}
.style2 {color: #2384C6}
-->
</style>
</head>

<body style="margin:0 0 0 0">
<table width="700" border="0" align="center" cellpadding="2" cellspacing="0">
  <tr>
    <td width="120"><div align="left"><img src="<?= base_url() ?>themes/snow_white/img/01_01.gif" alt="" /></div></td>
    <td width="828"><div align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="4"><strong>aspert - Retail</strong></font></div></td>
  </tr>
  <tr>
    <td colspan="2"><img src="<?= base_url() ?>themes/snow_white/img/01_03.gif" width="100%" height="42" alt="" /></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="1" cellpadding="2">
      <tr>
        <td><table width="100%" border="0" align="center" cellpadding="2" cellspacing="1">

          <tr>
            <td width="24%"><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Cliente:</font></strong></td>
            <td width="76%"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><?php echo $DESC_CLIENTE; ?></font></td>
          </tr>
          <tr>
            <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Bandeira:</font></strong></td>
            <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><?php echo $DESC_PRODUTO; ?></font></td>
          </tr>
          <tr>
            <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Campanha:</font></strong></td>
            <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><?php echo $DESC_CAMPANHA; ?></font></td>
          </tr>
          <tr>
            <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Título:</font></strong></td>
            <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><?php echo $DESC_EXCEL; ?></font></td>
          </tr>
          <tr>
            <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Período de validade:</font></strong></td>
            <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><?php echo $DATA_INICIO; ?> a <?php echo $DATA_TERMINO; ?></font></td>
          </tr>
          <tr>
            <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Observações:</font></strong></td>
            <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><?php echo nl2br($OBSERVACOES_EXCEL); ?></font></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Resultados:</font></strong></td>
            <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><?php
                        if(!empty($texto)){
							echo $texto;
                        } else if(!empty($problemas)) {
							echo '<ul>';
							foreach($problemas as $praca => $tipos){
								echo '<li>'.$praca.'</li>';
								echo '<ul>';
								foreach($tipos as $tipo => $items){
									foreach($items as  $item) {
										printf('<li>%s: %s</li>',
											ucfirst($tipo), $item);
									}
								}
								echo '</ul>';
							}
							echo '</ul>';
						}
                        ?></font></td>
          </tr>
        </table>
          </td>
      </tr>
      
    </table>
    <br />
	<div class="style2" style="text-decoration:none; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold" align="center">
		================= POR FAVOR NÃO RESPONDA ESTA MENSAGEM ================= <br />
		Qualquer dúvida entre em contato com o <a title="Clique aqui" href="mailto:suporte@aspert.com.br" style="text-decoration: underline;">Suporte</a> ou pelo número de telefone (11) 4349-0222
	</div>
	
    <div style="color:#FFFFFF; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:30px">
      <div align="center"><font color="#000000" face="Verdana, Arial, Helvetica, sans-serif"><strong>Aspert Soluções Tecnológicas</strong><br />
   	       
   	        Fone (11) 4349-0222<br />
   	        </font><font face="Verdana, Arial, Helvetica, sans-serif"><a href="#" title="Clique aqui" target="_blank" class="style2" style="text-decoration:none; font-weight:bold;">http://aspert.com.br</a></font><br />
   	            <br />
       </div>
  </div>
    
    </td>
  </tr>
</table>
</body>
</html>
