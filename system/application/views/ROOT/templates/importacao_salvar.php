<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>aspert - Retail</title>

</head>

<body style="margin:0 0 0 0">
<table width="700" border="0" align="center" cellpadding="2" cellspacing="0">
  <tr>
    <td width="120"><div align="left"><img src="<?= base_url() ?>themes/snow_white/img/01_01.gif" alt="" /></div></td>
    <td width="828"><div align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="4"><strong>aspert - Retail</strong></font></div></td>
  </tr>
  <tr>
    <td colspan="2"><img src="<?= base_url() ?>themes/snow_white/img/01_03.gif" width="100%" height="42" alt="" /></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" align="center" cellpadding="2" cellspacing="1">
      <tr>
        <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><br />
          Ol&aacute; 
          <?= $__usuario['NOME_USUARIO']?>
        </span>,<br />
        Durante a importa&ccedil;&atilde;o do job abaixo foram encontradas as seguintes pend&ecirc;ncias:</font><br />
        <br /></td>
      </tr>
    </table>
      <table width="100%" border="0" align="center" cellpadding="2" cellspacing="1">
      <tr>
        <td><table width="100%" border="0" cellspacing="1" cellpadding="2">
          <tr>
            <td width="54%"><table width="100%" border="0" cellspacing="1" cellpadding="2">
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Ag&ecirc;ncia: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $pendencias['DESC_AGENCIA']?>
        </span></font></td>
              </tr>
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Cliente: </font></strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
                  <?= $pendencias['DESC_CLIENTE']?>
                </span></font></td>
              </tr>
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Bandeira: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $pendencias['DESC_PRODUTO']?>
        </span></font></td>
              </tr>
              
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Campanha: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $pendencias['DESC_CAMPANHA']?>
        </span></font></td>
              </tr>
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Nome Job: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $objeto['TITULO_JOB']?>
        </span></font></td>
              </tr>
				<tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Usu&aacute;rio Respons&aacute;vel: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $objeto['NOME_USUARIO_RESPONSAVEL']?>
        </span></font></td>
              </tr>
              <tr>
               <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Validade: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $objeto['DATA_INICIO'] .' at&eacute '. $objeto['DATA_TERMINO']; ?>
        </span></font></td>
              </tr>
			  
		<?php if(!empty($objeto['NOME_USUARIO'])): ?>
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Criado por: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $objeto['NOME_USUARIO'] ?>
        </span></font></td>
              </tr>
		<?php endif; ?>
              
            </table></td>
            <td width="46%">  
            
              <div align="center">
              
              <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
              <? if(!empty($preview)): ?>
            	<img src="<?= $preview ?>" alt="Preview">
            <? endif; ?>
                </span></font>
                
                </div></td>
          </tr>
        </table>
          </td>
      </tr>
     
      
    </table>
    
      <table width="100%" border="0" align="center" cellpadding="2" cellspacing="1">
        <tr>
          <td height="160"><table width="100%" border="0" cellspacing="1" cellpadding="2">
            <tr>
              <td>
               </td>
            </tr>
            
          </table><? 
	$style = ' style="border-bottom: 1px solid #333333; font-family: Verdana, Arial; font-size: 11px"';
	if(!empty($pendencias['problemas'])):
	
	?>             
    
            <table width="100%" border="0" cellspacing="1" cellpadding="2">
            <?php foreach($pendencias['problemas'] as $praca => $resultado): ?>
            
            <tr>
              <td colspan="7" bgcolor="#F58320">
			  	<font color="#FFFFFF" size="3" face="Verdana, Arial, Helvetica, sans-serif">
			  	<strong><?php echo $praca; ?></strong>
				</font>
			 </td>
            </tr>
                        
            <tr>
              <td width="2%" bgcolor="#666666"<?php echo $style; ?>>&nbsp;</td>
              <td width="11%" bgcolor="#6A6A6B"><div align="left"><strong><font color="#FFFFFF" size="2" face="Verdana, Arial, Helvetica, sans-serif">Produto</font></strong></div></td>
                <td width="17%" bgcolor="#6A6A6B"><div align="left"><strong><font color="#FFFFFF" size="2" face="Verdana, Arial, Helvetica, sans-serif">C&oacute;digo Regional</font></strong></div></td>
                <td width="27%" bgcolor="#6A6A6B"><div align="left"><strong><font color="#FFFFFF" size="2" face="Verdana, Arial, Helvetica, sans-serif">Descri&ccedil;&atilde;o</font></strong></div></td>
                <td width="8%" bgcolor="#6A6A6B"><div align="left"><strong><font color="#FFFFFF" size="2" face="Verdana, Arial, Helvetica, sans-serif">P&aacute;gina</font></strong></div></td>
                <td width="8%" bgcolor="#6A6A6B"><div align="left"><strong><font color="#FFFFFF" size="2" face="Verdana, Arial, Helvetica, sans-serif">Ordem</font></strong></div></td>
                <td width="27%" bgcolor="#6A6A6B"><div align="left"><strong><font color="#FFFFFF" size="2" face="Verdana, Arial, Helvetica, sans-serif">Mensagem</font></strong></div></td>
            </tr>
            
            
            <font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">            </font>
            <?php
			foreach($resultado as $tipo => $itens):
				$cor = '';
				switch( $tipo ){
					case 'avisos': $cor = '#FFFF00'; break;
					case 'erros': $cor = '#FF0000'; break;
				}
				
				if($tipo == 'avisos' || $tipo == 'erros'):
					foreach($itens as $item):
	
			?>
			
            <tr>
              <td<?php echo $style; ?> bgcolor="<?php echo $cor; ?>">&nbsp;</td>
				<td bgcolor="#CCCCCC"<?php echo $style; ?>><?php echo $item['PRODUTO']; ?></td>
				<td bgcolor="#CCCCCC"<?php echo $style; ?>><?php echo val($item,'CODIGO_REGIONAL'); ?></td>
				<td bgcolor="#CCCCCC"<?php echo $style; ?>><?php echo htmlentities(utf8_decode($item['NOME_FICHA'])); ?></td>
				<td bgcolor="#CCCCCC"<?php echo $style; ?>><?php echo $item['PAGINA']; ?></td>
				<td bgcolor="#CCCCCC"<?php echo $style; ?>><?php echo $item['ORDEM']; ?></td>
				<td bgcolor="#CCCCCC"<?php echo $style; ?>><?php echo htmlentities(utf8_decode($item['MENSAGEM'])); ?></td>
            </tr>
            <?php
						endforeach;
					endif;
				endforeach;
			endforeach;
			?>
          </table>
          <? endif; ?>
    </td>
        </tr>
      </table>
      <br />
      <div style="color:#FFFFFF; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:30px">
   		<div align="center" class="style1"><font face="Verdana, Arial, Helvetica, sans-serif"><a href="<?= base_url().index_page() ?>" 
        	target="_blank" 
            style="text-decoration:none; color:#2384C6; font-weight:bold;" 
            title="Clique aqui">Clique aqui</a><font color="#000000"> para acessar o sistema aspert - Retail.    </font></font></div>
    </div>

	<div style="text-decoration:none; color:#2384C6; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold" align="center">
		================= POR FAVOR N&Atilde;O RESPONDA ESTA MENSAGEM ================= <br />
		Qualquer d&uacute;vida entre em contato com o <a title="Clique aqui" href="mailto:suporte@aspert.com.br" style="text-decoration: underline;">Suporte</a> ou pelo n&uacute;mero de telefone (11) 4349-0222
	</div>
    
    <div style="color:#FFFFFF; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:30px">
      <div align="center"><font color="#000000" face="Verdana, Arial, Helvetica, sans-serif"><strong>Aspert Soluções Tecnológicas</strong><br />
   	           	        Fone (11) 4349-0222<br />
   	        </font><font face="Verdana, Arial, Helvetica, sans-serif"><a href="http://aspert.com.br" title="Clique aqui" target="_blank" class="style2" style="text-decoration:none; font-weight:bold;">http://aspert.com.br</a></font><br />
   	            <br />
      </div>
  </div>
    
    </td>
  </tr>
</table>
</body>
</html>
