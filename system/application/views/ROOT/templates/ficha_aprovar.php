<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>aspert - Retail</title>
<style type="text/css">
<!--
.style2 {color: #2384C6}
a:link {
	text-decoration: none;
}
a:visited {
	text-decoration: none;
}
a:hover {
	text-decoration: underline;
}
a:active {
	text-decoration: none;
}
-->
</style>
</head>

<body style="margin:0 0 0 0">
<table width="700" border="0" align="center" cellpadding="2" cellspacing="0">
  <tr>
    <td width="120"><div align="left"><img src="<?= base_url() ?>themes/snow_white/img/01_01.gif" alt="" /></div></td>
    <td width="828"><div align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="4"><strong>aspert - Retail</strong></font></div></td>
  </tr>
  <tr>
    <td colspan="2"><img src="<?= base_url() ?>themes/snow_white/img/01_03.gif" width="100%" height="42" alt="" /></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" align="center" cellpadding="2" cellspacing="1">
      <tr>
        <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><br />
          Olá 
          <?= $__usuario['NOME_USUARIO']?>
        </span>,<br />
        A ficha abaixo teve seu status alterado para <?= $objeto['DESC_APROVACAO_FICHA']?>
        </span></font>
        </font>
          <br />
          <br /></td>
      </tr>
    </table>
      <table width="100%" border="0" align="center" cellpadding="2" cellspacing="1">
      <tr>
        <td><table width="100%" border="0" cellspacing="1" cellpadding="2">
          <tr>
            <td width="54%"><table width="100%" border="0" cellspacing="1" cellpadding="2">
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Código de Barras: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $objeto['COD_BARRAS_FICHA']?>
        </span></font></td>
              </tr>
              <?php if ( !empty($objeto['REGIOES']) ):?>
              <?php foreach ( $objeto['REGIOES'] as $regiao ):?>
              <?php foreach ( $regiao as $nomeRegiao => $codigoRegional ):?>
		              <tr>
		                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Código <?php echo $nomeRegiao;?>: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
		          <?= $codigoRegional;?>
		        </span></font></td>
		              </tr>
			  <?php endforeach;?>              
			  <?php endforeach;?>              
			  <?php endif;?>              
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Nome da Ficha: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $objeto['NOME_FICHA']?>
        </span></font></td>
              </tr>
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Temporário: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $objeto['FLAG_TEMPORARIO'] == 1 ? 'Sim' : 'Nao'?>
        </span></font></td>
              </tr>
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Status: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $objeto['DESC_APROVACAO_FICHA']?>
        </span></font></td>
              </tr>
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Data|Hora Alteração: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= date("d/m/Y H:i:s")?>
        </span></font></td>
              </tr>
              <tr>
               <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Usuário alteração: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $__logado['NOME_USUARIO']?>
        </span></font></td>
              </tr>
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Pendências de dados básicos: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= nl2br($objeto['PENDENCIAS_DADOS_BASICOS']);?>
        </span></font></td>
              </tr>
              <tr>
               <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Pendências de campos: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= nl2br($objeto['PENDENCIAS_CAMPOS']);?>
        </span></font></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table></td>
            <td width="46%">  
            
              <div align="center">
              
              <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
                <? if(!empty($main)): ?>
                <img src="<?= base_url().'img.php?img_id='.$main['FILE_ID']?>" alt="Preview">
                    <? endif; ?>
                </span></font>
                
                </div></td>
          </tr>
        </table></td>
      </tr>
     
      
    </table>
    
    <div style="color:#FFFFFF; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:30px">
   		<div align="center" class="style1"><font face="Verdana, Arial, Helvetica, sans-serif"><a href="<?= base_url().index_page() ?>" 
        	target="_blank" 
            style="text-decoration:none; color:#2384C6; font-weight:bold;" 
            title="Clique aqui">Clique aqui</a><font color="#000000"> para acessar o sistema aspert - Retail.    </font></font></div>
    </div>
    
	<div class="style2" style="text-decoration:none; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold" align="center">
		================= POR FAVOR NÃO RESPONDA ESTA MENSAGEM ================= <br />
		Qualquer dúvida entre em contato com o <a title="Clique aqui" href="mailto:suporte@aspert.com.br" style="text-decoration: underline;">Suporte</a> ou pelo número de telefone (11) 4349-0222
	</div>	
	
    <div style="color:#FFFFFF; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:30px">
      <div align="center"><font color="#000000" face="Verdana, Arial, Helvetica, sans-serif"><strong>Aspert Soluções Tecnológicas</strong><br />
   	        
   	        Fone (11) 4349-0222<br />
   	        </font><font face="Verdana, Arial, Helvetica, sans-serif"><a href="#" title="Clique aqui" target="_blank" class="style2" style="text-decoration:none; font-weight:bold;">http://aspert.com.br</a></font><br />
   	            <br />
        </div>
  </div>
    
    </td>
  </tr>
</table>
</body>
</html>
