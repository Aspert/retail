<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>aspert - Retail</title>
<style type="text/css">
<!--
.style1 {
	font-size: 12px
}
.style2 {color: #2384C6}
-->
</style>
</head>

<body style="margin:0 0 0 0">
<table width="700" border="0" align="center" cellpadding="2" cellspacing="0">
  <tr>
    <td width="120"><div align="left"><img src="<?= base_url() ?>themes/snow_white/img/01_01.gif" alt="" /></div></td>
    <td width="828"><div align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="4"><strong>aspert - Retail</strong></font></div></td>
  </tr>
  <tr>
    <td colspan="2"><img src="<?= base_url() ?>themes/snow_white/img/01_03.gif" width="100%" height="42" alt="" /></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="1" cellpadding="2">
      <tr>
        <td><p><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Ol&aacute; <span style="color:#2384C6; font-weight:bold;">
            <?= $__usuario['NOME_USUARIO']?></span>,</font></p></td>
      </tr>
      <tr>
      	<td>&nbsp;</td>
      	</tr>
      <tr>
        <td><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Em <?php echo $job['DATA_PROXIMA_ETAPA']; ?> você receberá o Job abaixo para elaboração na etapa &quot;<?php echo $job['PROXIMA_ETAPA'] ?>&quot;:</font></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">N&uacute;mero do Job:</font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $job['ID_JOB']?>
        </span></font></td>
      </tr>
      <tr>
        <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">T&iacute;tulo do Job:</font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $job['TITULO_JOB']?>
        </span></font>        </td>
      </tr>
      <tr>
        <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Usu&aacute;rio Respons&aacute;vel:</font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $job['NOME_USUARIO_RESPONSAVEL']?>
        </span></font>        </td>
      </tr>
      <tr>
        <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Data de in&iacute;cio:</font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= date('d/m/Y', strtotime($job['DATA_INICIO']))?>
        </span></font>        </td>
      </tr>
      <tr>
        <td><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Ag&ecirc;ncia</strong>:</font> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $job['DESC_AGENCIA']?>
        </span></font>        </td>
      </tr>
      <tr>
        <td><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Cliente</strong>:</font> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000"><?= $job['DESC_CLIENTE']?>
        </span></font>        </td>
      </tr>
      <tr>
        <td><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Bandeira</strong>:</font> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
			<?= $job['DESC_PRODUTO']?>
        </span></font>        </td>
      </tr>
      <tr>
        <td><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Campanha</strong>:</font> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $job['DESC_CAMPANHA']?>
        </span></font>        </td>
      </tr>
      <tr>
        <td><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>T&eacute;rmino do Job</strong>:</font> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
      	  <?= date('d/m/Y', strtotime($job['DATA_TERMINO']))?>
        </span></font>      	</td>
      </tr>
      <tr>
        <td><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Situa&ccedil;&atilde;o</strong>:</font> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $job['DESC_STATUS']?>
        </span></font>        </td>
      </tr>
	  <?php if(!empty($job['NOME_USUARIO'])): ?>
      <tr>
        <td><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Criado por</strong>:</font> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $job['NOME_USUARIO']?>
        </span></font>        </td>
      </tr>
	  <?php endif;?>
    </table></td>
  </tr>
</table>
<br />
<td colspan="3" align="center">
    <div style="color:#FFFFFF; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:30px">
   		<div align="center" class="style1"><font face="Verdana, Arial, Helvetica, sans-serif"><a href="<?= base_url().index_page() ?>" 
        	target="_blank" 
            style="text-decoration:none; color:#2384C6; font-weight:bold;" 
            title="Clique aqui">Clique aqui</a><font color="#000000"> para acessar o sistema aspert - Retail.    </font></font></div>
    </div>
	
	<div class="style2" style="text-decoration:none; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold" align="center">
		================= POR FAVOR NÃO RESPONDA ESTA MENSAGEM ================= <br />
		Qualquer dúvida entre em contato com o <a title="Clique aqui" href="mailto:suporte@aspert.com.br" style="text-decoration: underline;">Suporte</a> ou pelo número de telefone (11) 4349-0222
	</div>	
	
    <div style="color:#FFFFFF; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:30px">
      <div align="center"><font color="#000000" face="Verdana, Arial, Helvetica, sans-serif"><strong>Aspert Soluções Tecnológicas</strong><br />
   	           	        Fone (11) 4349-0222<br />
   	        </font><font face="Verdana, Arial, Helvetica, sans-serif"><a href="#" title="Clique aqui" target="_blank" class="style2" style="text-decoration:none; font-weight:bold;">http://aspert.com.br</a></font><br />
   	            <br />
        </div>
  </div>
</body>
</html>
