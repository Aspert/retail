<?php $this->load->view("ROOT/layout/header"); ?>
<div id="contentGlobal">
<?php if ( isset($erros) ) : ?>
	<ul style="color:#ff0000;font-weight:bold;">
		<?php foreach ( $erros as $e ) : ?>
			<li><?=$e?></li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>
	
<?php echo form_open('fabricante/save/'.$this->uri->segment(3));?>
<h1>Cadastro de Fabricantes</h1>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="Alignleft">
  <tr>
    <td width="14%" scope="col">Nome</td>
    <td width="86%" scope="col"><input name="DESC_FABRICANTE" type="text" value="<?php post('DESC_FABRICANTE'); ?>" />
    <?php showError('DESC_MARCA'); ?></td>
  </tr>
  <tr>
    <td>Situa&ccedil;&atilde;o</td>
    <td><select name="FLAG_ACTIVE_FABRICANTE">
      <option value="1">Ativo</option>
      <option value="0"<?php echo post('FLAG_ACTIVE_FABRICANTE',true)=='0' ? ' selected="selected"' : ''; ?>>Inativo</option>
    </select></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
    <a href="<?php echo site_url('fabricante/lista'); ?>" class="button"><span>Cancelar</span></a>
     <a href="javascript:;" onclick="$j(this).closest('form').submit()" class="button"><span>Salvar</span></a>
    </td>
  </tr>
</table>
<?php echo form_close();?>

</div>
	
<?php $this->load->view("ROOT/layout/footer") ?>