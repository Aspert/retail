<?php $this->load->view("ROOT/layout/header") ?>
<div id="contentGlobal">
<h1>Cadastro de Fabricantes</h1>
<form method="post" action="<?php echo site_url('fabricante/lista'); ?>" id="pesquisa_simples">
<table width="100%" border="0" cellspacing="1" cellpadding="2">
  <tr>
    <?php if($podeAlterar): ?>
    <td width="7%" rowspan="2" align="center">
      <a href="<?php echo site_url('fabricante/form'); ?>" title="Adicionar">
        <img src="<?php echo base_url().THEME; ?>img/file_add.png" alt="Adicionar" />
      </a>
     </td>
    <?php endif; ?>
    <td width="44%">Nome do fabricante</td>
    <td width="37%">Itens por p&aacute;gna</td>
    <td width="12%" rowspan="2">
    <a href="javascript:;" onclick="$j(this).closest('form').submit();" class="button"><span>Pesquisar</span></a>
    </td>
  </tr>
  <tr>
    <td><input type="text" name="DESC_FABRICANTE" id="DESC_FABRICANTE" value="<?php echo !empty($busca['DESC_FABRICANTE']) ? $busca['DESC_FABRICANTE'] : ''; ?>" /></td>
    <td><select name="pagina" id="pagina">
      <?php
            $pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
            for($i=5; $i<=50; $i+=5){
				printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
            }
            ?>
    </select>
    </td>
  </tr>
</table>
</form>
<br />
<?php if(!empty($fabricantes)) :?>
<div id="tableObjeto">
<table border="0" width="100%" class="tableSelection">
        <thead>
        <tr>
           <?php if($podeAlterar): ?>
           <td>Editar</td>
		   <?php endif; ?>
           <td width="570">Nome</td>
           <td width="261">Situa&ccedil;&atilde;o</td>
        </tr>
        </thead>
		<?php foreach($fabricantes as $item):?>
		<tr>
          <?php if($podeAlterar): ?>
          <td align="center" width="67"><a title="Alterar" href="<?php echo site_url('fabricante/form/' . $item['ID_FABRICANTE']); ?>"><img src="<?php echo base_url().THEME; ?>img/file_edit.png" alt="Alterar" /></a></td>
          <?php endif; ?>
          <td align="center"><?php echo $item['DESC_FABRICANTE']; ?></td>
		  <td align="center"><?php echo $item['FLAG_ACTIVE_FABRICANTE'] == 1 ? 'Ativo' : 'Inativo'; ?></td>
		</tr>
		<?php endforeach; ?>
</table>
</div>
<span class="paginacao"><?php echo $paginacao; ?></span>

<?php else:?>
	Nenhum resultado
<?php endif;?>

</div>


<?php $this->load->view("ROOT/layout/footer") ?>