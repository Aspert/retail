<?php $this->load->view("ROOT/layout/header"); ?>

<div id="contentGlobal">
  <?php if ( isset($erros) && is_array($erros) ) : ?>
  <ul style="color:#ff0000;font-weight:bold;">
    <?php foreach ( $erros as $e ) : ?>
    <li>
      <?=$e?>
    </li>
    <?php endforeach; ?>
  </ul>
  <?php endif; ?>
  <?php echo form_open('templates_indd/save/'.$this->uri->segment(3));?>
  <h1>Cadastro de Templates</h1>
  <table width="100%" class="Alignleft">
    <tr>
    	<td width="15%" scope="col">Cliente</td>
    	<td width="38%" scope="col"><?php if(!empty($_sessao['ID_CLIENTE'])): ?>
    		<?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?>
    		<br />
    		<?php else: ?>
    		<select name="ID_CLIENTE" id="ID_CLIENTE">
    			<option value=""></option>
    			<?php echo montaOptions($clientes, 'ID_CLIENTE','DESC_CLIENTE', post('ID_CLIENTE',true)); ?>
    			</select>
    		<?php endif; ?></td>
    	<td width="47%" rowspan="9" valign="top" scope="col"><div style="float:right">
    		<? if(!empty($PREVIEW)): ?>
    		<img src="<?= base_url() ?>img.php?img_id=<?= $PREVIEW; ?>&amp;img_size=big&amp;rand=<?= rand()?>"  height="230" />
    		<? endif; ?>
    		</div></td>
    	</tr>
    <tr>
    	<td>Bandeira</td>
    	<td><?php if(!empty($_sessao['ID_PRODUTO'])): ?>
    		<?php sessao_hidden('ID_PRODUTO','DESC_PRODUTO'); ?>
    		<br />
    		<?php else: ?>
    		<select name="ID_PRODUTO" id="ID_PRODUTO">
    			<option value=""></option>
    			<?php echo montaOptions($produtos, 'ID_PRODUTO','DESC_PRODUTO', post('ID_PRODUTO',true)); ?>
    			</select>
    		<?php endif; ?></td>
    	</tr>
    <tr>
      <td>Campanha</td>
      <td><select name="ID_CAMPANHA" id="ID_CAMPANHA">
          <option value=""></option>
          <?php echo montaOptions($campanhas, 'ID_CAMPANHA','DESC_CAMPANHA', post('ID_CAMPANHA',true)); ?>
        </select></td>
    </tr>
    <tr>
      <td>Nome template</td>
      <td><input name="DESC_TEMPLATE" type="text" value="<?php post('DESC_TEMPLATE'); ?>" size="50" maxlength="150" /></td>
    </tr>
    <tr>
      <td>Origem</td>
      <td><input name="ORIGEM_TEMPLATE" type="text" value="<?php post('ORIGEM_TEMPLATE'); ?>" size="50" maxlength="150" /></td>
    </tr>
    <tr>
      <td>Formato</td>
      <td><input id="LARGURA_TEMPLATE" name="LARGURA_TEMPLATE" type="text" size="6" class="inputs" value="<?php post('LARGURA_TEMPLATE') ?>" />
        x
        <input id="ALTURA_TEMPLATE" name="ALTURA_TEMPLATE" type="text" size="6" class="inputs" value="<?php post('ALTURA_TEMPLATE') ?>" /></td>
    </tr>
    <tr>
      <td>Situa&ccedil;&atilde;o</td>
      <td><select name="STATUS_TEMPLATE">
          <option value="1">ATIVO</option>
          <option value="0"<?php echo isset($_POST['STATUS_TEMPLATE']) && $_POST['STATUS_TEMPLATE'] == '0' ? ' selected="selected"' : ''; ?>>INATIVO</option>
        </select></td>
    </tr>
    <tr>
      <td>N&uacute;mero de p&aacute;ginas</td>
      <td><input name="NUMERO_PAGINAS" type="text" id="NUMERO_PAGINAS" value="<?php post('NUMERO_PAGINAS'); ?>" size="5" maxlength="4" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><a href="<?php echo site_url('templates_indd/lista'); ?>" class="button"><span>Cancelar</span></a> <a href="javascript:" onclick="$j(this).closest('form').submit()" class="button"><span>Gravar</span></a></td>
    </tr>
  </table>
</div>
<?php echo form_close();?>
<script type="text/javascript">

$('ID_CLIENTE').addEvent('change', function(){
	clearSelect($('ID_CAMPANHA'),1);
	clearSelect($('ID_PRODUTO'),1);
	montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByCliente'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
});

$('ID_PRODUTO').addEvent('change', function(){
	clearSelect($('ID_CAMPANHA'),1);
	montaOptionsAjax($('ID_CAMPANHA'),'<?php echo site_url('json/admin/getCampanhasByProduto'); ?>','id=' + this.value,'ID_CAMPANHA','DESC_CAMPANHA');
});

$j('#LARGURA_TEMPLATE,#ALTURA_TEMPLATE').maskMoney({showSymbol: false, thousands:''});

</script>
<?php $this->load->view("ROOT/layout/footer") ?>
