<?php $this->load->view("ROOT/layout/header"); ?>

<div id="contentGlobal">
  <?php if ( isset($erros) && is_array($erros) ) : ?>
  <ul style="color:#ff0000;font-weight:bold;">
    <?php foreach ( $erros as $e ) : ?>
    <li>
      <?=$e?>
    </li>
    <?php endforeach; ?>
  </ul>
  <?php endif; ?>
  <form method="post" action="<?php echo site_url('templates_indd/enviar_indd/'.$this->uri->segment(3).'/salvar_ordens'); ?>" enctype="multipart/form-data">
    <h1>Enviar arquivos INDD para "<?php echo $template['DESC_TEMPLATE']; ?>"</h1>
    <div id="flash_container"></div>
    <div>&nbsp;</div>
    <div>Tamanho maximo de cada arquivo: <?php echo ini_get('upload_max_filesize'); ?></div>
    <script type="text/javascript">
var vars = {};
vars.SessionID = '<?php echo session_id(); ?>';

vars.urlUpload = '<?php echo site_url('templates_indd/enviar_indd/' . $this->uri->segment(3)); ?>';
vars.urlVoltar = '<?php echo site_url('templates_indd/lista'); ?>';
vars.urlRedirect = '<?php echo site_url('templates_indd/enviar_arquivos/' . $this->uri->segment(3) ); ?>';
vars.urlChecagemOrdens = '<?php echo site_url('templates_indd/enviar_indd/' . $this->uri->segment(3) . '/checar_ordens'); ?>';

swfobject.embedSWF("<?php echo base_url(); ?>swf/upload_templates.swf", "flash_container", "100%", "300", "9.0.0",{},vars,{wmode:'transparent',allowscriptaccess:'always'});

function itemEnviado(name){
	
}

function btnCancelar_clickHandler(){
	location.href = '<?php echo site_url('objeto/lista'); ?>';
}

</script>
    <div class="clearBoth">&nbsp;</div>
    <h1>Arquivos j&aacute; enviados</h1>
    <?php if(!empty($templates)): ?>
    <div id="tableObjeto">
    <table width="100%" class="tableSelection" id="tabelaBusca">
        <thead>
          <tr>
            <td width="5%">Excluir</td>
            <td width="15%">ID</td>
            <td width="31%">P&aacute;gina inicial</td>
            <td width="34%">P&aacute;gina Final</td>
            <td width="15%">Data de Cadastro</td>
          </tr>
        </thead>
        <?php foreach($templates as $item): ?>
        <tr class="linha" id="item_<?php echo $item['ID_TEMPLATE_ITEM']; ?>">
          <td><a href="<?php echo site_url('templates_indd/enviar_indd/'.$template['ID_TEMPLATE'].'/remover_item/'.$item['ID_TEMPLATE_ITEM']); ?>"><img src="<?php echo base_url().THEME; ?>/img/trash.png" alt="Excluir" width="31" height="31" border="0" /></a></td>
          <td><?php echo $item['ID_TEMPLATE_ITEM'] ?></td>
          <td><input class="startPage" name="pgInicio[<?php echo $item['ID_TEMPLATE_ITEM'] ?>]" type="text" id="pgInicio" size="4" maxlength="4" value="<?php echo $item['PAGINA_INICIO_TEMPLATE_ITEM'] ?>" /></td>
          <td><input class="endPage" name="pgFim[<?php echo $item['ID_TEMPLATE_ITEM'] ?>]" type="text" id="pgFim" size="4" maxlength="4" value="<?php echo $item['PAGINA_FIM_TEMPLATE_ITEM'] ?>" /></td>
          <td><?php echo date('d/m/Y H:i', strtotime($item['DATA_CADASTRO'])) ?></td>
        </tr>
        <?php endforeach; ?>
        <tr>
          <td colspan="4">&nbsp;</td>
          <td><a href="javascript:" class="button" onclick="$j(this).closest('form').submit()"><span>Atualizar P&aacute;ginas</span></a>&nbsp;</td>
        </tr>
      </table>
    </div>
    <?php else: ?>
    Nenhum arquivo enviado
    <?php endif; ?>
  </form>
</div>

<script type="text/javascript">

function valida(){
	var $ = $j;
	
	var erros = [];
	
	$('.linha').each(function(){
		var curr = this;
		
		var a1 = parseFloat($(curr).find('.startPage').val());
		var a2 = parseFloat($(curr).find('.endPage').val());
		
		if( a1 > a2 ){
			erros.push('Item #' + curr.id + ': a pagina inicial deve ser menor ou igual a final');
		}
		
		$('.linha').each(function(){
			var test = this;
			
			if( test != curr ){
				var b1 = parseFloat($(test).find('.startPage').val());
				var b2 = parseFloat($(test).find('.endPage').val());
				
				var msg = 'O item #'+curr.id+' intercala com o item ' + test.id;
				
				if( a1 >= b1 && a2 <= b2 ){
					erros.push(msg);
				} else if(a1 <= b1 && a2 >= b1 && a2 <= b2 ){
					erros.push(msg);
				} else if(a1 >= b1 && a1 <= b2){
					erros.push(msg);
				} else if(b1 >= a1 && b2 <= a2) {
					erros.push(msg);
				}
				
			}
		});
	});
	
	if( erros.length > 0 ){
		alert( erros.join('\n') );
		return false;
	}
	
	return true;
}

$j(function($){
	$('form').submit( valida );
});
</script>

<?php $this->load->view("ROOT/layout/footer") ?>
