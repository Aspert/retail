<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
  <h1>Templates</h1>
  <div id="pesquisa_simples"> <?php echo form_open('templates_indd/lista');?>
    <table width="100%" border="0" cellspacing="1" cellpadding="2">
      <tr>
        <td width="6%" rowspan="4" align="center"><?php if($podeAlterar): ?>
          <a href="<?php echo site_url('templates_indd/form'); ?>"> <img src="<?php echo base_url(). THEME; ?>img/file_add.png" alt="Adicionar" width="31" height="31" border="0" /> </a>
          <?php endif; ?></td>
  	<?php if(!empty($_sessao['ID_AGENCIA'])): ?>
		<td width="14%">Ag&ecirc;ncia</td>
		<td width="13%">Cliente</td>
	<?php elseif(!empty($_sessao['ID_CLIENTE'])): ?>
		<td width="14%">Cliente</td>
		<td width="13%">Ag&ecirc;ncia</td>
	<?php endif; ?>
        <td width="14%">Produto</td>
        <td width="14%">Campanha</td>
        <td width="17%">Template</td>
        <td width="19%">&nbsp;</td>
        <td width="3%" rowspan="4">&nbsp;</td>
      </tr>
      <tr>
        
      <?php if(!empty($_sessao['ID_AGENCIA'])): ?>
		<td>
		  <?php sessao_hidden('ID_AGENCIA','DESC_AGENCIA'); ?>
		 </td>
		<td>
		  <select name="ID_CLIENTE" id="ID_CLIENTE">
			<option value=""></option>
			<?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', !empty($busca['ID_CLIENTE']) ? $busca['ID_CLIENTE'] : ''); ?>
			</select>
		  </td>
	  <?php elseif(!empty($_sessao['ID_CLIENTE'])): ?>
		<td><?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?></td>
		<td>
		  <select name="ID_AGENCIA" id="ID_AGENCIA">
			<option value=""></option>
			<?php echo montaOptions($agencias,'ID_AGENCIA','DESC_AGENCIA', !empty($busca['ID_AGENCIA']) ? $busca['ID_AGENCIA'] : ''); ?>
		  </select>
		</td>
	  <?php endif; ?>
		
        <td valign="middle"><?php if(!empty($_sessao['ID_PRODUTO'])): ?>
          <?php sessao_hidden('ID_PRODUTO','DESC_PRODUTO'); ?>
          <?php else: ?>
          <select name="ID_PRODUTO" id="ID_PRODUTO" class="comboPesquisa">
            <option value=""></option>
            <?php echo montaOptions($produtos,'ID_PRODUTO','DESC_PRODUTO', !empty($busca['ID_PRODUTO']) ? $busca['ID_PRODUTO'] : ''); ?>
          </select>
          <?php endif; ?></td>
        <td valign="middle"><select name="ID_CAMPANHA" id="ID_CAMPANHA" class="comboPesquisa">
            <option value=""></option>
            <?php echo montaOptions($campanhas,'ID_CAMPANHA','DESC_CAMPANHA', !empty($busca['ID_CAMPANHA']) ? $busca['ID_CAMPANHA'] : ''); ?>
          </select></td>
        <td valign="middle"><input class="comboPesquisa" type="text" name="DESC_TEMPLATE" id="DESC_TEMPLATE" value="<?php echo !empty($busca['DESC_TEMPLATE']) ? $busca['DESC_TEMPLATE'] : ''; ?>" /></td>
        <td valign="middle"><a href="javascript:;" class="button" onclick="$j(this).closest('form').submit()"><span>Pesquisar</span></a></td>
      </tr>
      <tr>
        <td>N&uacute;mero Template</td>
        <td>Status</td>
        <td>Data de inclus&atilde;o</td>
        <td>Possui INDD</td>
        <td>Itens por p&aacute;gina</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td valign="bottom"><input class="comboPesquisa" type="text" name="ID_TEMPLATE" id="ID_TEMPLATE" value="<?php echo !empty($busca['ID_TEMPLATE']) ? $busca['ID_TEMPLATE'] : ''; ?>" /></td>
        <td valign="bottom"><select name="STATUS_TEMPLATE" id="STATUS_TEMPLATE" class="comboPesquisa">
            <option value=""></option>
            <option value="1"<?php echo val($busca,'STATUS_TEMPLATE') == 1 ? ' selected="selected"' : '';?>>Ativo</option>
            <option value="0"<?php echo val($busca,'STATUS_TEMPLATE') == '0' ? ' selected="selected"' : '';?>>Inativo</option>
          </select>
        </td>
        <td valign="bottom"><input type="text" name="DATA_CADASTRO_TEMPLATE" id="DATA_CADASTRO_TEMPLATE" class="_calendar" value="<?php echo val($busca,'DATA_CADASTRO_TEMPLATE'); ?>" size="9"/></td>
        <td valign="bottom">
          <input name="TEMPLATE_EXISTS" type="radio" id="TEMPLATE_EXISTS" value="1" <?php echo (isset($busca['TEMPLATE_EXISTS']) && $busca['TEMPLATE_EXISTS'] === '1') ? ' checked="cheked"' : '';?> />
          Sim
          <input name="TEMPLATE_EXISTS" type="radio" id="TEMPLATE_EXISTS2" value="0" <?php echo (isset($busca['TEMPLATE_EXISTS']) && $busca['TEMPLATE_EXISTS'] === '0') ? ' checked="cheked"' : '';?>/>
          N&atilde;o 
        </td>
        <td valign="bottom">
          <span class="campo dir">
	          <select name="pagina" id="pagina" class="comboPesquisa">
	            <?php
					$pagina_atual = val($busca,'pagina',0);
					for($i=5; $i<=50; $i+=5){
						printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
					}
				?>
	          </select>
          </span>
        </td>
        <td valign="middle">&nbsp;</td>
        <td valign="middle">&nbsp;</td>
      </tr>
    </table>
    <?php echo form_close();?> </div>
  <?php if( (!empty($lista))&& (is_array($lista))):?>
  <div id="tableObjeto">
    <table width="100%" class="tableSelection" id="tabelaBusca">
      <thead>
        <tr>
          <?php if($podeAlterar): ?>
          <th>Editar</th>
          <?php endif; ?>
          <? if($podeExcluir): ?>
          <th width="50" align="left">Excluir</th>
          <?php endif; ?>
          <?php if($podeEnviarIndd): ?>
          <th width="46" align="center"> INDD </th>
          <?php endif; ?>
          <?php if($podeEnviarArquivos): ?>
          <th width="51" align="center">Upload</th>
          <?php endif; ?>
          <? if($podeDownload): ?>
          <th width="105" align="left">Preview PDF</th>
          <?php endif; ?>
          <th width="135" align="left" id="colCliente">Cliente</th>
          <th width="112" align="left" id="colBandeira">Bandeira</th>
          <th width="131" align="left" id="colCampanha">Campanha</th>
          <th width="50" align="left" id="colNumeroTemplate">N&uacute;mero Template</th>
          <th width="97" align="left" id="colTemplate">Template</th>
          <th width="102" align="left" id="colDataCadastro">Data cadastro</th>
          <th width="102" align="left">Completo</th>
          <th width="39" align="left">Status</th>
        </tr>
      </thead>
      <?php $cont=0; ?>
      <?php foreach($lista as $item):?>
      <tr>
        <?php if($podeAlterar): ?>
        <td width="43" align="center"><a href="<?php echo site_url('templates_indd/form/' . $item['ID_TEMPLATE']); ?>"><img src="<?php echo base_url(). THEME; ?>img/file_edit.png" alt="Editar" border="0" /></a></td>
        <?php endif; ?>
        <?php if($podeExcluir): ?>
        <td width="51" align="center"><a href="javascript:" onclick="deleteTemplate(<?php echo $item['ID_TEMPLATE']; ?>)"><img src="<?php echo base_url(). THEME; ?>img/trash.png" alt="Excluir template" border="0" /></a></td>
        <?php endif; ?>
        <?php if($podeEnviarIndd): ?>
        <td width="46" align="center"><a href="<?php echo site_url('templates_indd/enviar_indd/' . $item['ID_TEMPLATE']); ?>"><img src="<?php echo base_url(). THEME; ?>img/eraser_pencil.png" alt="Enviar INDD" border="0" /></a></td>
        <?php endif; ?>
        <?php if($podeEnviarArquivos): ?>
        <td width="51" align="center"><a href="<?php echo site_url('templates_indd/enviar_arquivos/' . $item['ID_TEMPLATE']); ?>"><img src="<?php echo base_url(). THEME; ?>img/file.png" alt="Enviar arquivos" border="0" /></a></td>
        <?php endif; ?>
        <? if($podeDownload): ?>
        <td><? if(!empty($item['PREVIEW'])): ?>
          <a href="<?= base_url(); ?><?= index_page(); ?>/templates_indd/download/<?= $item['ID_TEMPLATE'] ?>" target="_blank"><img src="<?php echo base_url(). THEME; ?>img/visualizar.png" alt="Enviar arquivos" border="0" /></a>
          <? else: ?>
          <strong>N/A</strong>
          <? endif; ?></td>
        <? endif; ?>
        <td><?=$item['DESC_CLIENTE'];?></td>
        <td><?=$item['DESC_PRODUTO'];?></td>
        <td><?=$item['DESC_CAMPANHA'];?></td>
        <td><?=$item['ID_TEMPLATE'];?></td>
        <td><?=$item['DESC_TEMPLATE'];?></td>
        <td><?=format_date_to_form($item['DATA_CADASTRO_TEMPLATE']);?></td>
        <td><?= $item['TEMPLATE_EXISTS'] == 1 ? 'Sim' : 'N&atilde;o';?></td>
        <td><?=$item['STATUS_TEMPLATE'] == 1 ? 'Ativo' : 'Inativo';?></td>
        <?php $cont++; ?>
      </tr>
      <?endforeach;?>
    </table>
  </div>
  <span class="paginacao">
  <?=(isset($paginacao))?$paginacao:null?>
  </span>
  <?else:?>
  Nenhum resultado
  <?endif;?>
</div>
<script type="text/javascript">

<?php if($podeExcluir): ?>
function deleteTemplate(id){
   if(confirm('Deseja realmente excluir o template e seus arquivos associados?')){
		location.href = '<?php echo site_url('templates_indd/delete/'); ?>/'+id;
   }
}
<?php endif; ?>



$j(function(){
	$j('._calendar').datepicker({dateFormat: 'dd/mm/yy',showOn: 'button', buttonImage: '<?= base_url().THEME ?>img/calendario.png', buttonImageOnly: true});
	
	var options = 
			{
			columns:[{col:'#colCliente',label:'DESC_CLIENTE'},
					{col:'#colBandeira',label:'DESC_PRODUTO'},
					{col:'#colCampanha',label:'DESC_CAMPANHA'},
					{col:'#colNumeroTemplate',label:'ID_TEMPLATE'},
					{col:'#colTemplate',label:'DESC_TEMPLATE'},
					{col:'#colDataCadastro',label:'DATA_CADASTRO_TEMPLATE'}
					],
			form:'#pesquisa_simples form',
			orderDirection: '<?= $busca['ORDER_DIRECTION'] ?>',
			selectedField:'<?= $busca['ORDER'] ?>'
			}
			
	$j('#tabelaBusca').sortgrid(options);
	
	$j("#btnLimpaPesquisa").click(function(){
		limpaForm('#pesquisa_simples');
		if($j("#ID_CLIENTE option").length > 0){
			clearSelect($('ID_CLIENTE'),1);
			clearSelect($('ID_PRODUTO'),1);
		}
		clearSelect($('ID_CAMPANHA'),1);
	});
	
	<?php if(!empty($_sessao['ID_CLIENTE'])): ?>
	$('ID_AGENCIA').addEvent('change', function(){
		clearSelect($('ID_CAMPANHA'), 1);
		clearSelect($('ID_PRODUTO'), 1);
		montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByAgencia'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
	});
	<?php endif; ?>
	
	<?php if(!empty($_sessao['ID_AGENCIA'])): ?>
	$('ID_CLIENTE').addEvent('change', function(){
		clearSelect($('ID_CAMPANHA'), 1);
		clearSelect($('ID_PRODUTO'), 1);
		montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByCliente'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
	});
	<?php endif; ?>
	
	$('ID_PRODUTO').addEvent('change', function(){
		clearSelect($('ID_CAMPANHA'), 1);
		montaOptionsAjax($('ID_CAMPANHA'),'<?php echo site_url('json/admin/getCampanhasByProduto'); ?>','id=' + this.value,'ID_CAMPANHA','DESC_CAMPANHA');
	});
});

$j(function(){
	$j('.ui-datepicker').hide();
});
</script>
<?php $this->load->view("ROOT/layout/footer") ?>
