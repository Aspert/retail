<?php $this->load->view("ROOT/layout/header"); ?>

<div id="contentGlobal">
  <h1>Enviar arquivos para "<?php echo $template['DESC_TEMPLATE']; ?>"</h1>
  <?php if(!empty($links)): ?>
  Tamanho m&aacute;ximo de cada arquivo: <?php echo ini_get('upload_max_filesize'); ?><br />
  <br />
  <div id="inddinfo"> Foram encontrados os seguintes links em seu arquivo INDD que ainda n&atilde;o foram enviados:
    <ul id="itens">
      <?php foreach($links as $item): ?>
      <li id="<?php echo $item['ARQUIVO_LINK']; ?>"><?php echo $item['ARQUIVO_LINK']; ?></li>
      <?php endforeach; ?>
    </ul>
  </div>
  <div style="clear:both">&nbsp;</div>
  <?php endif; ?>
  <div id="flash_container">Flash goes here</div>
</div>

<script type="text/javascript">
$j(function(){
	var vars = {urlUpload: '<?php echo base_url().index_page().'/templates_indd/enviar_arquivos/'.$this->uri->segment(3); ?>'};
	vars.files = '<?php echo str_replace("'","\\'",$files) ?>';
	vars.SessionID = '<?php echo session_id(); ?>';
	swfobject.embedSWF("<?php echo base_url(); ?>swf/upload_files.swf", "flash_container", "100%", "500", "9.0.0",{},vars,{wmode:'transparent'});
});

function itemEnviado(name){
	$j('#itens').find('li').each(function(idx, el){
		if($j(el).html() == name){
			$j(el).remove();
		}
	});
	
	if($j('#itens').find('li').length == 0){
		$j('#inddinfo').remove();
	}
}

</script>
<?php $this->load->view("ROOT/layout/footer") ?>
