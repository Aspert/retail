<?php $this->load->view("ROOT/layout/header"); ?>

<div id="contentGlobal"> 

<?php if ( isset($erros) && is_array($erros) ) : ?>
	<ul style="color:#ff0000;font-weight:bold;">
		<?php foreach ( $erros as $e ) : ?>
			<li><?=$e?></li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>


 	
	<?php echo form_open_multipart('cliente/save/'.$this->uri->segment(3));?>

<h1>Cadastro de Clientes</h1>

<table width="70%" border="0" cellspacing="0" cellpadding="0" class="Alignleft">
  <tr>
    <td width="20%">Ag&ecirc;ncias:</td>
    <td width="80%">
		<select name="ID_AGENCIA[]" multiple="multiple">
        <?php foreach($agencias as $agencia){ ?>
			<?php if(array_key_exists("SELECIONADO", $agencia)){ ?>
				<option value="<?php echo $agencia['ID_AGENCIA']; ?>" <?php if($agencia['SELECIONADO'] == "1"){echo "selected";} ?>><?php echo $agencia['DESC_AGENCIA']; ?></option>
			<?php } else{ ?>
				<option value="<?php echo $agencia['ID_AGENCIA']; ?>"><?php echo $agencia['DESC_AGENCIA']; ?></option>
			<?php } ?>
        <?php } ?>
        </select>
    </td>
  </tr>
  <tr>
    <td>Nome: <span class="obrigatorio">*</span></td>
    <td> 
		<input name="DESC_CLIENTE" type="text" id="DESC_CLIENTE" value="<?php post('DESC_CLIENTE'); ?>" size="100" />
    </td>
  </tr>
  <tr>
    <td>Feed RSS</td>
    <td><input name="FEED_CLIENTE" type="text" id="FEED_CLIENTE" value="<?php post('FEED_CLIENTE'); ?>" size="100" maxlength="255" /></td>
  </tr>
  <tr>
    <td>Logotipo: </td>
    <td><input name="logotipo" type="file" />
      <span style="text-align:left; padding:6px;"> (arquivo PNG de dimens&otilde;es 118 x 50)</span></td>
  </tr>
  <tr>
    <td>Logotipo atual: </td>
    <td>
        <?php if(!empty($logo)): ?>
          <img src="<?php echo base_url(); ?>img/cliente/<?php echo $logo; ?>" title="" alt="" />
        <?php endif; ?>
    </td>
  </tr>
  <tr>
    <td>Situa&ccedil;&atilde;o: <span class="obrigatorio">*</span></td>
    <td>
    	
        <select name="STATUS_CLIENTE">
         <option value="1">ATIVO</option>
         <option value="0"<?php echo isset($_POST['STATUS_CLIENTE']) && $_POST['STATUS_CLIENTE'] == '0' ? ' selected="selected"' : ''; ?>>INATIVO</option>
        </select>
	
    </td>
  </tr>
  <tr>
  	<td>Adaptador Importa&ccedil;&atilde;o</td>
  	<td><input name="CLASSE_IMPORTADOR" type="text" id="CLASSE_IMPORTADOR" value="<?php echo post('CLASSE_IMPORTADOR',true) == '' ? 'Default' : post('CLASSE_IMPORTADOR',true) ; ?>" size="100" /></td>
  	</tr>
  <tr>
    <td>Transformador</td>
    <td><select name="ID_TRANSFORMADOR" id="ID_TRANSFORMADOR">
		<option value=""></option>
		<?php echo montaOptions($transformadores,'ID_TRANSFORMADOR','NOME',post('ID_TRANSFORMADOR',true)); ?>
    	</select></td>
  </tr>
  <tr>
    <td>Dias Inativa&ccedil;&atilde;o Fichas:</td>
    <td> 
		<input name="QTD_DIAS_INATIVACAO_FICHA" type="text" id="QTD_DIAS_INATIVACAO_FICHA" value="<?php post('QTD_DIAS_INATIVACAO_FICHA'); ?>" size="4" maxlength="4"/>
    </td>
  </tr>
  <tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><strong>Cadastro de Fichas em Lote:</strong></td>
  </tr>
  <tr>
    <td>Categoria:</td>
    <td>
		<select name="ID_CATEGORIA" id="ID_CATEGORIA">
			<option value=""></option>
			<?php echo montaOptions($categorias,'ID_CATEGORIA','DESC_CATEGORIA',$categoriaLote); ?>
		</select>
    </td>
  </tr>
  <tr>
    <td>Subcategorias:</td>
    <td>
		<select name="SUBCATEGORIA_LOTE" id="SUBCATEGORIA_LOTE">
			<option value=""></option>
			<?php echo montaOptions($subcategorias,'ID_SUBCATEGORIA','DESC_SUBCATEGORIA',$_POST['SUBCATEGORIA_LOTE']); ?>
		</select>
    </td>
  </tr>
  <tr>
</table>

<br/><br/>

<div class="both"></div>

    <a class="button" href="<?php echo site_url('cliente/lista'); ?>"><span>Cancelar</span></a>
    <a class="button" href="javascript:;" onclick="$j(this).closest('form').submit()"><span>Salvar</span></a>
    
  <?php echo form_close();?>

</div> <!-- Final de Content Global -->

<script type="text/javascript">
$j(function(){
	$('ID_CATEGORIA').addEvent('change', function(){
		clearSelect($('SUBCATEGORIA_LOTE'),1);
		montaOptionsAjax($('SUBCATEGORIA_LOTE'),'<?php echo site_url("json/admin/getSubcategoriasByCategoria"); ?>','id='+this.value,'ID_SUBCATEGORIA','DESC_SUBCATEGORIA');
	});
});
</script>
	
<?php $this->load->view("ROOT/layout/footer") ?>