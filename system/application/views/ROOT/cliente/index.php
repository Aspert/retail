<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal"> 

<h1>Cadastro de Clientes</h1>

<div id="pesquisa_simples">
	<?php echo form_open('cliente/lista');?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="17%" align="center">
      <?php if($podeAlterar): ?>
      <a href="<?php echo site_url('cliente/form'); ?>"><img src="<?php echo base_url().THEME; ?>img/file_add.png" alt="Adicionar" /></a>
      <?php endif; ?>
      </td>
    <td width="27%" align="center">Nome do cliente 
    	<input type="text" name="DESC_CLIENTE" id="DESC_CLIENTE" value="<?php echo val($busca, 'DESC_CLIENTE'); ?>" /></td>
    <td width="32%">Itens por p&aacute;gina:      <select name="pagina" id="pagina">
      <?php
		$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
		for($i=5; $i<=50; $i+=5){
			printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
		}
		?>
      </select></td>
    <td width="24%"><a class="button" href="javascript:;" onclick="$j(this).closest('form').submit()"><span>Pesquisar</span></a></td>
  </tr>
  </table>

  <?php echo form_close();?>
</div>
<?php if(!empty($lista) && is_array($lista)):?>

<div id="tableObjeto">
<table border="0" width="100%" class="tableSelection">
<thead>
<tr>
		<?php if($podeAlterar): ?>
        <th>Editar</th>
		<?php endif; ?>
        <th width="446" align="left">Cliente</th>
        <th width="71" align="left">Feed RSS</th>
        <th width="71" align="left">Status</th>
</tr>
</thead>
		<?php $cont=0; ?>
		<?php foreach($lista as $item):?>

		<tr>        
			<?php if($podeAlterar): ?>
            <td align="center" width="66">
				<?php echo anchor('cliente/form/'. $item['ID_CLIENTE'], '<img src="'. base_url().THEME.'img/file_edit.png"/>',array('title'=>'Editar cliente'));?>            </td>
            <?php endif; ?>
			<td><?=$item['DESC_CLIENTE'];?></td>
			<td><?=$item['FEED_CLIENTE'];?></td>
			<td><?=$item['STATUS_CLIENTE'] == 1 ? 'Ativo' : 'Inativo';?></td>
			<?php $cont++; ?>
		</tr>
		<?endforeach;?>
</table>
</div> <!-- Final de Table Objeto -->

<span class="paginacao"><?=(isset($paginacao))?$paginacao:null?></span>
<?else:?>
	Nenhum resultado
<?endif;?>

</div> <!-- Final de content Global -->

<?php $this->load->view("ROOT/layout/footer") ?>