<?php $this->load->view("ROOT/layout/header") ?>
<div id="pesquisa_simples">
	<?php echo form_open('midia/lista');?>
		<?=form_hidden('tipo_pesquisa','simples');?>
			<div class="botao esq"><?=anchor('midia/form','<img src="'.base_url().'img/add.gif" border="0" />',array('title'=>'Nova Midia'))?></div>
		<div class="campo esq"> Nome: <?= form_input('DESC_MIDIA',(isset($this->session->userdata['BUSCA']['midia'])) ? $this->session->userdata['BUSCA']['midia']['DESC_MIDIA'] : '');?></div>
		<div class="campo esq">
		Itens/P&aacute;gina:

		<?$pagina_atual = (isset($this->session->userdata["BUSCA"]['midia']["pagina"])) ? $this->session->userdata['BUSCA']['midia']["pagina"] : 0;?>

		<select name="pagina" id="pagina">
		<option value="5" <?=($pagina_atual == 5) ? " selected " : ""?>>5</option>
		<option value="10" <?=($pagina_atual == 10) ? " selected " : ""?>>10</option>
		<option value="15" <?=($pagina_atual == 15) ? " selected " : ""?>>15</option>
		<option value="20" <?=($pagina_atual == 20) ? " selected " : ""?>>20</option>
		<option value="25" <?=($pagina_atual == 25) ? " selected " : ""?>>25</option>
		<option value="50" <?=($pagina_atual == 50) ? " selected " : ""?>>50</option>
		</select>
		</div>
		<div class="botao"><input type="image" src="<?=base_url();?>img/lupa.gif" class="btn" value="Submit" alt="Pesquisa" title="Pesquisa"></div>
	<?php echo form_close();?>
</div>

<?php if( (isset($midias))&& (is_array($midias))):?>
<table border="0" width="100%">
	<tr onmouseover="new Util().mouseover(this)" onmouseout="new Util().mouseout(this)">
		<th>Editar</th>
		<th>Nome</th>
		<th>Excluir</th>
	</tr>
		<?php $cont=0; ?>
		<?php foreach($midias as $midia):?>

		<tr class="alter" onmouseover="new Util().mouseover(this)" onmouseout="new Util().mouseout(this)">
			<td align="center" width="50">
				<?php echo anchor('midia/form/'. $midia['ID_MIDIA'], '<img src="'. base_url().'img/edititem.gif" border="0"/>',array('title'=>'Editar Midia'));?>
			</td>
			<td><?=$midia['DESC_MIDIA'];?></td>
			<td align="center" width="50">
				<?php echo anchor('midia/excluir/'. $midia['ID_MIDIA'], '<img src="'. base_url().'img/cancelar.gif" border="0"/>',array('title'=>'Editar Midia'));?>
			</td>
			<?php $cont++; ?>
		</tr>
		<?endforeach;?>
</table>
	<span class="paginacao"><?=(isset($paginacao))?$paginacao:null?></span>
<?else:?>
	Nenhum resultado
<?endif;?>
<?php $this->load->view("ROOT/layout/footer") ?>