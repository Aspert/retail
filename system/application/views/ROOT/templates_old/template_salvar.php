<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>24\7 Inteligência Digital - Retail</title>
<style type="text/css">
<!--
.style2 {color: #f4911e}
a:link {
	text-decoration: none;
}
a:visited {
	text-decoration: none;
}
a:hover {
	text-decoration: underline;
}
a:active {
	text-decoration: none;
}
-->
</style>
</head>

<body style="margin:0 0 0 0">
<table width="700" border="0" align="center" cellpadding="2" cellspacing="0">
  <tr>
    <td width="120"><div align="left"><img src="<?= base_url() ?>themes/snow_white/img/01_01.gif" width="119" height="77" alt="" /></div></td>
    <td width="828"><div align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="4"><strong>24\7 Intelig&ecirc;ncia Digital - Retail</strong></font></div></td>
  </tr>
  <tr>
    <td colspan="2"><img src="<?= base_url() ?>themes/snow_white/img/01_03.gif" width="100%" height="42" alt="" /></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" align="center" cellpadding="2" cellspacing="1">
      <tr>
        <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><br />
          Olá 
          <?= $__usuario['NOME_USUARIO']?>
        </span>,<br />
        O template abaixo foi salvo: </span></font>
        </font>
          <br />
          <br /></td>
      </tr>
    </table>
      <table width="100%" border="0" align="center" cellpadding="2" cellspacing="1">
      <tr>
        <td><table width="100%" border="0" cellspacing="1" cellpadding="2">
          <tr>
            <td width="54%"><table width="100%" border="0" cellspacing="1" cellpadding="2">
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Cliente: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $objeto['DESC_CLIENTE']?>
        </span></font></td>
              </tr>
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Bandeira: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $objeto['DESC_PRODUTO']?>
        </span></font></td>
              </tr>
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Campanha: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $objeto['DESC_CAMPANHA']?>
        </span></font></td>
              </tr>
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Nome template: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $objeto['DESC_TEMPLATE']?>
        </span></font></td>
              </tr>
              <tr>
                <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Data|Hora Alteração: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= date("d/m/Y H:i:s")?>
        </span></font></td>
              </tr>
              <tr>
               <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Usuário alteração: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
          <?= $__logado['NOME_USUARIO']?>
        </span></font></td>
              </tr>
              
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table></td>
            <td width="46%">  
            
              <div align="center">
              
              <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
                	<? if(!empty($preview)): ?>
            	<img src="<?= $preview ?>" alt="Preview">
            <? endif; ?>
                </span></font>
                
                </div></td>
          </tr>
        </table></td>
      </tr>
     
      
    </table>
    
    <div style="color:#FFFFFF; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:30px">
   		<div align="center" class="style1"><font face="Verdana, Arial, Helvetica, sans-serif"><a href="<?= base_url().index_page() ?>" 
        	target="_blank" 
            style="text-decoration:none; color:#f4911e; font-weight:bold;" 
            title="Clique aqui">Clique aqui</a><font color="#000000"> para acessar o sistema 24\7 Inteligência Digital- Retail.    </font></font></div>
    </div>

	<div class="style2" style="text-decoration:none; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold" align="center">
		================= POR FAVOR NÃO RESPONDA ESTA MENSAGEM ================= <br />
		Qualquer dúvida entre em contato com o <a title="Clique aqui" href="mailto:suporte@247id.com.br" style="text-decoration: underline;">Suporte</a> ou pelo número de telefone (11)4349-0222
	</div>
    
    <div style="color:#FFFFFF; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:30px">
      <div align="center"><font color="#000000" face="Verdana, Arial, Helvetica, sans-serif"><strong>24\7 Inteligência Digital</strong><br />
   	        Rua Cunha Gago, 700 - 13º andar<br />
   	        Pinheiros. CEP: 05421-000<br />
   	        São Paulo-SP<br />
   	        Fone (11) 4349-0222<br />
   	        </font><font face="Verdana, Arial, Helvetica, sans-serif"><a href="#" title="Clique aqui" target="_blank" class="style2" style="text-decoration:none; font-weight:bold;">http://www.247id.com.br</a></font><br />
   	            <br />
        <font color="#38CDFF" face="Verdana, Arial, Helvetica, sans-serif">Siga-nos no</font><font color="#000000" face="Verdana, Arial, Helvetica, sans-serif"> <a href="http://www.twitter.com/247id" target="_blank"><img src="<?= base_url() ?>themes/snow_white/img/twitter.png" width="51" height="12" border="0" /></a></font></div>
  </div>
    
    </td>
  </tr>
</table>
</body>
</html>
