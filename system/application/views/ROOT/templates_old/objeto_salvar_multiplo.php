<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>24\7 Inteligência Digital - Retail</title>
<style type="text/css">
<!--
.style2 {color: #f4911e}
a:link {
	text-decoration: none;
}
a:visited {
	text-decoration: none;
}
a:hover {
	text-decoration: underline;
}
a:active {
	text-decoration: none;
}
-->
</style>
</head>

<body style="margin:0 0 0 0">
<table width="700" border="0" align="center" cellpadding="2" cellspacing="0">
  <tr>
    <td width="120"><div align="left"><img src="<?= base_url() ?>themes/snow_white/img/01_01.gif" width="119" height="77" alt="" /></div></td>
    <td width="828"><div align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="4"><strong>24\7 Intelig&ecirc;ncia Digital - Retail</strong></font></div></td>
  </tr>
  <tr>
    <td colspan="2"><img src="<?= base_url() ?>themes/snow_white/img/01_03.gif" width="100%" height="42" alt="" /></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" align="center" cellpadding="2" cellspacing="1">
      <tr>
        <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><br />
          Olá 
          <?= $__usuario['NOME_USUARIO']?>
        </span>,<br />
        Os objetos abaixo foram salvos em lote com as seguintes informações: </span></font>
        </font>
          <br />
          <br /></td>
      </tr>
    </table>
      <table width="100%" border="0" align="center" cellpadding="2" cellspacing="1">
      <tr>
        <td><table width="100%" border="0" cellspacing="1" cellpadding="2">
          <tr>
            <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Categoria: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
              <?= empty($keywords['CATEGORIA']) ? 'N/A' : $keywords['CATEGORIA'] ?>
              </span></font></td>
          </tr>
          <tr>
            <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Subcategoria: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
              <?= empty($keywords['SUBCATEGORIA']) ? 'N/A' : $keywords['SUBCATEGORIA'] ?>
            </span></font></td>
          </tr>
          <tr>
            <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Marca: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
            <?= empty($keywords['MARCA']) ? 'N/A' :  $keywords['MARCA']; ?>
            </span></font></td>
          </tr>
          <tr>
            <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Keywords: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
            <?= empty($keywords['KEYWORDS']) ? 'N/A' :  $keywords['KEYWORDS']; ?>
            </span></font></td>
          </tr>
          <tr>
            <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Data|Hora Alteração: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
              <?= date("d/m/Y H:i:s")?>
            </span></font></td>
          </tr>
          <tr>
            <td><strong><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Usuário alteração: </font></strong> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span style="color:#000000">
              <?= $__logado['NOME_USUARIO']; ?>
              </span></font></td>
          </tr>
          </table>

          <h3>Lista de objetos</h3>
<table width="100%" height="53" border="1" cellpadding="0" cellspacing="0">
  <tr>
        <th width="43" align="center" valign="middle" style="border:1px solid #CCCCCC">&nbsp;</th>
        <th width="469" align="left" valign="middle" style="border:1px solid #CCCCCC"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
        Nome do arquivo</font>
        </th>
        <th width="120" align="left" valign="middle" style="border:1px solid #CCCCCC"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Tipo</font></th>
        </tr>
  <? foreach($objetos as $obj):?>
      <tr>
        <td width="43" align="center" valign="middle" style="border:1px solid #CCCCCC">
        	<img src="<?= base_url() ?>img.php?img_id=<?= $obj['FILE_ID']; ?>&rand=<?= rand()?>&a.jpg" width="40" border="0" />
        </td>
        <td width="469" align="left" valign="middle" style="border:1px solid #CCCCCC">
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">
		<?= $obj['FILENAME']; ?>
        </font>
        </td>
        <td width="120" align="left" valign="middle" style="border:1px solid #CCCCCC">
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">
		<?= $obj['DESC_TIPO_OBJETO']; ?>
        </font>
        </td>
        
        </tr>
  <? endforeach; ?>
</table>
<p>&nbsp;</p></td>
      </tr>
     
      
    </table>
    
    <div style="color:#FFFFFF; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:30px">
   		<div align="center" class="style1"><font face="Verdana, Arial, Helvetica, sans-serif"><a href="<?= base_url().index_page() ?>" 
        	target="_blank" 
            style="text-decoration:none; color:#f4911e; font-weight:bold;" 
            title="Clique aqui">Clique aqui</a><font color="#000000"> para acessar o sistema 24\7 Inteligência Digital - Retail.    </font></font></div>
    </div>

	<div class="style2" style="text-decoration:none; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold" align="center">
		================= POR FAVOR NÃO RESPONDA ESTA MENSAGEM ================= <br />
		Qualquer dúvida entre em contato com o <a title="Clique aqui" href="mailto:suporte@247id.com.br" style="text-decoration: underline;">Suporte</a> ou pelo número de telefone (11)4349-0222
	</div>
    
    <div style="color:#FFFFFF; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:30px">
      <div align="center"><font color="#000000" face="Verdana, Arial, Helvetica, sans-serif"><strong>24\7 Inteligência Digital</strong><br />
   	        Rua Cunha Gago, 700 - 13º andar<br />
   	        Pinheiros. CEP: 05421-000<br />
   	        São Paulo-SP<br />
   	        Fone (11) 4349-0222<br />
   	        </font><font face="Verdana, Arial, Helvetica, sans-serif"><a href="#" title="Clique aqui" target="_blank" class="style2" style="text-decoration:none; font-weight:bold;">http://www.247id.com.br</a></font><br />
   	            <br />
        <font color="#38CDFF" face="Verdana, Arial, Helvetica, sans-serif">Siga-nos no</font><font color="#000000" face="Verdana, Arial, Helvetica, sans-serif"> <a href="http://www.twitter.com/247id" target="_blank"><img src="<?= base_url() ?>themes/snow_white/img/twitter.png" width="51" height="12" border="0" /></a></font></div>
  </div>
    
    </td>
  </tr>
</table>
</body>
</html>
