<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">

<h1>Hist&oacute;rico / Importa&ccedil;&atilde;o de Excel</h1>

<?php if(!empty($historico['PRACAS'])): ?>


<div id="tableObjeto">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableSelection">
<thead>
  <tr>
    <td>Job:</td>
    <td>Data importa&ccedil;&atilde;o anterior</td>
    <td>Data importa&ccedil;&atilde;o atual</td>
    <td>Data Importa&ccedil;&atilde;o</td>
  </tr>
</thead>
  <tr>
    <td><?php echo $excel['NUMERO_JOB_EXCEL']; ?></td>
    <?php if(!empty($historico['DATA_1'])): ?>
    <td><?php echo date('d/m/Y H:i:s', strtotime($historico['DATA_1'])); ?></td>
    <td><?php echo date('d/m/Y H:i:s', strtotime($historico['DATA_2'])); ?></td>
    <?php else: ?>
    <td><?php echo date('d/m/Y H:i:s', strtotime($historico['DATA_2'])); ?></td>
    <?php endif; ?>
    <td></td>
  </tr>
</table>
</div> <!-- Final de Content Objeto -->

<div id="contentLegenda">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableLegenda">
  <thead>
    <tr>
      <td colspan="7" style="text-align:left">Legenda:</td>
      </tr>
  </thead>
  <tr>
    <td width="257">PU: Pre&ccedil;o Unit&aacute;rio</td>
    <td width="257">TU: Tipo Unit&aacute;rio</td>
    <td width="257">PC: Pre&ccedil;o Caixa</td>
    <td width="257">TC: Tipo Caixa</td>
    <td width="257">JAA: Juros ao ano</td>
    <td width="43" class="removido">&nbsp;</td>
    <td width="281">Item removido</td>
  </tr>
  <tr>
    <td>PV: Pre&ccedil;o a vista</td>
    <td>DE: Pre&ccedil;o de</td>
    <td>Por: Pre&ccedil;o Por</td>
    <td>EC: Economize</td>
    <td>TP: Total Prazo</td>
    <td class="alteracao">&nbsp;</td>
    <td>Item alterado</td>
  </tr>
  <tr>
    <td>EN: Entrada</td>
    <td>PA: Parcelas</td>
    <td>PR: Presta&ccedil;&atilde;o</td>
    <td>JAM: Juros ao m&ecirc;s</td>
    <td>&nbsp;</td>
    <td class="adicionado">&nbsp;</td>
    <td>Item adicionado</td>
  </tr>
  </table>

</div> <!-- Final de Content Legenda -->

<div style="clear: both;"></div>

<div id="tabs">
      <ul>
            <?php foreach($historico['PRACAS'] as $praca => $data): ?>
        <li><a href="#<?php echo removeAcentos($praca); ?>"><?php echo $praca; ?></a></li>
            <?php endforeach;?>
      </ul>
        
        <?php foreach($historico['PRACAS'] as $praca => $data): ?>
        <div id="<?php echo removeAcentos($praca); ?>">
       	  <div id="contentExcel" style="overflow-x: auto;">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableExcel">
				<thead>
					<tr>
						<td width="9%">P&aacute;gina</td>
						<td width="9%">Ordem</td>
						<td width="9%">Produto</td>
						<td width="9%">Descri&ccedil;&atilde;o</td>
						<td colspan="4">Atacarejo</td>
						<td colspan="4">A vista</td>
						<td colspan="6">Financiado</td>
						<td width="16%">Observa&ccedil;&otilde;es</td>
					</tr>
				</thead>
				
				<thead>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>PU</td>
						<td>TU</td>
						<td>PC</td>
						<td>TC</td>
						<td>PV</td>
						<td>De</td>
						<td>Por</td>
						<td>EC</td>
						<td>EN</td>
						<td>PA</td>
						<td>PR</td>
						<td>JAM</td>
						<td>JAA</td>
						<td>TP</td>
						<td>&nbsp;</td>
					</tr>
				</thead>
			<?php
			foreach($data['ITENS'] as $pagina => $tmp):
			foreach($tmp as $ordem => $item):
			?>
				<tr class="<?php echo $item['ADICIONADO'] == 1 ? 'adicionado' : ''; echo $item['REMOVIDO'] == 1 ? 'removido' : ''; ?>">
				<td<?php marcaAlterado($item['PAGINA_ITEM']); ?>><?php echo $item['PAGINA_ITEM']['novo']; ?></td>
				<td<?php marcaAlterado($item['ORDEM_ITEM']); ?>><?php echo $item['ORDEM_ITEM']['novo']; ?></td>
				<td<?php marcaAlterado($item['CODIGO_ITEM']); ?>><?php echo empty($item['CODIGO_ITEM']['novo']) ? '---' : $item['CODIGO_ITEM']['novo']; ?></td>
				<td<?php marcaAlterado($item['DESCRICAO_ITEM']); ?>><?php echo $item['DESCRICAO_ITEM']['novo']; ?></td>
				<td<?php marcaAlterado($item['PRECO_UNITARIO']); ?>><?php echo money($item['PRECO_UNITARIO']['novo']); ?></td>
				<td<?php marcaAlterado($item['TIPO_UNITARIO']); ?>><?php echo $item['TIPO_UNITARIO']['novo']; ?></td>
				<td<?php marcaAlterado($item['PRECO_CAIXA']); ?>><?php echo money($item['PRECO_CAIXA']['novo']); ?></td>
				<td<?php marcaAlterado($item['TIPO_CAIXA']); ?>><?php echo $item['TIPO_CAIXA']['novo']; ?></td>
				<td<?php marcaAlterado($item['PRECO_VISTA']); ?>><?php echo money($item['PRECO_VISTA']['novo']); ?></td>
				<td<?php marcaAlterado($item['PRECO_DE']); ?>><?php echo money($item['PRECO_DE']['novo']); ?></td>
				<td<?php marcaAlterado($item['PRECO_POR']); ?>><?php echo money($item['PRECO_POR']['novo']); ?></td>
				<td<?php marcaAlterado($item['ECONOMIZE']); ?>><?php echo money($item['ECONOMIZE']['novo']); ?></td>
				<td<?php marcaAlterado($item['ENTRADA']); ?>><?php echo money($item['ENTRADA']['novo']); ?></td>
				<td<?php marcaAlterado($item['PARCELAS']); ?>><?php echo $item['PARCELAS']['novo']; ?></td>
				<td<?php marcaAlterado($item['PRESTACAO']); ?>><?php echo money($item['PRESTACAO']['novo']); ?></td>
				<td<?php marcaAlterado($item['JUROS_AM']); ?>><?php echo $item['JUROS_AM']['novo']; ?></td>
				<td<?php marcaAlterado($item['JUROS_AA']); ?>><?php echo $item['JUROS_AA']['novo']; ?></td>
				<td<?php marcaAlterado($item['TOTAL_PRAZO']); ?>><?php echo $item['TOTAL_PRAZO']['novo']; ?></td>
				<td<?php marcaAlterado($item['OBSERVACOES']); ?>><?php echo htmlentities($item['OBSERVACOES']['novo']); ?></td>
				</tr>
			<?php
			endforeach;
			endforeach;
			?>
			</table>
            </div>
        </div>
        <?php endforeach;?>
    </div>
    
<?php endif; ?>

<script type="text/javascript">
var obj;

function valorAntigo(ref, vlr){
	obj.html(vlr);
	obj.show();
}

function hideValorAntigo(){
	obj.hide();
}

$j(function(){
	obj = $j(document.body).append('<div class="tooltipValores"></div>').find('.tooltipValores');
	obj.css('position','absolute').hide();
	
	$j().mousemove(function(e){
		$j(obj).css('left',e.pageX + 10)
			.css('top',e.pageY+10);
	});
	
	$j('#tabs').tabs();
});

</script>

</div> <!-- Final de Content Global -->

<?php $this->load->view("ROOT/layout/footer") ?>