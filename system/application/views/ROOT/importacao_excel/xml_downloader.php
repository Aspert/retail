<?php

if( !isset($token) ){
	$token = substr(md5(uniqid(rand(0,time()))), 0, 20);
}

echo '<?xml version="1.0" encoding="UTF-8"?>
<files>';


foreach($objetos as $item){
	$filename = $item['PATH'].'/'.$item['FILENAME'];
	
	if( file_exists($filename) ){
		$size = number_format(filesize($filename)/1024/1024, 2, '.', '');
		echo '<file>';
		echo '<name><![CDATA[' . site_url('objeto/download/' . strrev($item['FILE_ID']) . '/'.$token.'/'.$item['FILENAME']) . ']]></name>';
		echo '<size><![CDATA[' . $size . 'mb]]></size>';
		echo '</file>';
	}
}


echo '</files>';


?>