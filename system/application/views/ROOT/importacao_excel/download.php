<?php $this->load->view("ROOT/layout/header"); ?>

<div id="contentGlobal">

<h1>Download do &uacute;ltimo InDesign gerado</h1>

<p>Os arquivos ficam dispon&iacute;veis por um periodo de 24h em nossos servidores.<br />
Ap&oacute;s este per&iacute;odo, ser&aacute; necess&aacute;rio realizar uma nova importa&ccedil;&atilde;o de planilha.</p>
<p>&nbsp;</p>
<div id="tableObjeto">
    <table width="100%" class="tableSelection">
        <thead>
          <tr>
            <td width="85%" align="left">Pra&ccedil;a</td>
            <td width="15%">Download</td>
          </tr>
        </thead>
      <?php foreach($lista as $key => $item): ?>
      <tr>
        <td style="text-align: left; font-weight: bold"><?php echo $key; ?></td>
      </tr>
	      <?php foreach($item as $titem): ?>
		      <tr>
		        <td style="text-align: left">
		        	&nbsp;
		        	&nbsp;
		        	&nbsp;
		        	&nbsp;
		        	&nbsp;
		        	Pagina <?php echo $titem['inicio']; ?> a <?php echo $titem['fim']; ?> (<?php echo $titem['nome']; ?>)
		        </td>
		        <td align="center">
		        	<? if($titem['exists']): ?>
		        		<a href="<?php echo $titem['url']; ?>"><img src="<?php echo base_url().THEME; ?>img/disc.png" alt="Download" /></a>
		        	<? else: ?>
		        		<img src="<?php echo base_url().THEME; ?>img/file_delete.png" alt="Nao disponivel" title="Nao disponivel" />
		        	<? endif; ?>
		        </td>
		      </tr>
	      <?php endforeach; ?>
      <?php endforeach; ?>
    </table>
</div>
<div class="clearBoth">&nbsp;</div>

<a href="<?php echo site_url('gerar_indd/listar_agencia'); ?>" class="button"><span>Voltar</span></a>


</div> <!-- Final de Content Global -->

<?php $this->load->view("ROOT/layout/footer"); ?>
