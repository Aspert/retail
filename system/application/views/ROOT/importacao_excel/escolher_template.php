﻿<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">

<h1>Escolher template para gerar INDD</h1>

<form action="<?php echo site_url('gerar_indd/save/'.$this->uri->segment(3)); ?>" method="post">
<table width="100%" class="Alignleft">
  <tr>
    <td width="17%"><strong>Ag&ecirc;ncia</strong></td>
    <td width="45%"><?php echo $job['DESC_AGENCIA']; ?></td>
    <td width="38%" rowspan="16" valign="top">
        <img id="imgTemplate" src="" style="display:none"  height="230" />
    </td>
  </tr>
  <tr>
    <td><strong>Cliente</strong></td>
    <td><?php echo $job['DESC_CLIENTE']; ?></td>
    </tr>
  <tr>
    <td><strong>Bandeira</strong></td>
    <td><?php echo $job['DESC_PRODUTO']; ?></td>
    </tr>
  <tr>
    <td><strong>Campanha</strong></td>
    <td><?php echo $job['DESC_CAMPANHA']; ?></td>
    </tr>
  <tr>
    <td><strong>Tipo de pe&ccedil;a</strong></td>
    <td><?php echo $job['DESC_TIPO_PECA']; ?></td>
    </tr>
  <tr>
    <td><strong>Tipo de Job</strong></td>
    <td><?php echo $job['DESC_TIPO_JOB']; ?></td>
    </tr>
  <tr>
    <td><strong>N&uacute;mero do Job</strong></td>
    <td><?php echo $job['NUMERO_JOB_EXCEL']; ?></td>
    </tr>
  <tr>
    <td><strong>T&iacute;tulo</strong></td>
    <td><?php echo $job['TITULO_JOB']; ?></td>
    </tr>
  <tr>
  	<td><strong>Formato</strong></td>
  	<td><?php echo centimetragem($job['LARGURA_JOB']), ' x ', centimetragem($job['ALTURA_JOB']); ?></td>
  	</tr>
  <tr>
    <td><strong>N&uacute;mero de p&aacute;ginas</strong></td>
    <td><?php echo $job['PAGINAS_JOB']; ?></td>
    </tr>
  <tr>
    <td><strong>In&iacute;cio  da Validade</strong></td>
    <td><?php echo format_date_to_form($job['DATA_INICIO']); ?></td>
    </tr>
  <tr>
    <td><strong>Final da Validade</strong></td>
    <td><?php echo format_date_to_form($job['DATA_TERMINO']); ?></td>
    </tr>
  <tr>
    <td valign="top"><strong>Observa&ccedil;&otilde;es</strong></td>
    <td><?php echo nl2br($job['OBSERVACOES_EXCEL']); ?></td>
    </tr>
  <tr>
    <td valign="top"><strong>Situa&ccedil;&atilde;o</strong></td>
    <td><?php echo $job['DESC_STATUS']; ?></td>
    </tr>
  <tr>
    <td valign="top"><strong>Etapa atual</strong></td>
    <td><?php echo $job['DESC_ETAPA']; ?></td>
    </tr>
  <tr>
    <td valign="top"><strong>Selecione o template</strong></td>
    <td><select name="ID_TEMPLATE" id="ID_TEMPLATE">
    	<option value="">-- Selecione --</option>
      <?php echo montaOptions($templates,'ID_TEMPLATE','DESC_TEMPLATE','');?>
    </select>
	<img src="<?php echo base_url(),THEME,'img/information.png'; ?>" alt="São listados somente os templates que estão: completos, ativos, possuem o mesmo número de páginas, largura, altura e campanha do job." class="information" />
	</td>
    </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td>
    	<a href="<?php echo site_url('gerar_indd/listar_agencia'); ?>" class="button"><span>Cancelar</span></a>
		<a class="button" href="#" onclick="Ficha.HistoricoJob(<?php echo $job['ID_JOB']; ?>); return false;"><span>Ver Hist&oacute;rico de Aprova&ccedil;&atilde;o</span></a>
        <a href="javascript:" class="button" onclick="$j(this).closest('form').submit()"><span>Gerar INDD</span></a>
    </td>
    <td valign="top">&nbsp;</td>
  </tr>
</table>
</form>
</div> <!-- Final de Content Global -->
<script type="text/javascript">
$('ID_TEMPLATE').addEvent('change', function(){
	$('imgTemplate').setStyle('display','none');
	$('imgTemplate').src = "";
	if(this.value != ''){
		new Ajax('<?php echo site_url('json/admin/getTemplateData'); ?>', {
			method: 'post',
			onComplete: function(json){
				var data = eval('['+json+']');
				if(data[0] && data[0].PREVIEW_TEMPLATE != undefined){
					$('imgTemplate').setStyle('display','block');
					$('imgTemplate').src = "<?= base_url() ?>img.php?img_id="+data[0].PREVIEW_TEMPLATE+"&img_size=big&rand=<?= rand()?>";
				}
			}
		}).request('id='+this.value);
	}
});
</script>
<?php $this->load->view("ROOT/layout/footer") ?>