<div id="avisos_cenario" class="modalEstatica">
	<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabela_padrao">
		<?php foreach ( $avisos as $a) : ?>
			<tr>
				<td>
					A ficha '<strong><?php echo $a['COD_BARRAS_FICHA']; ?> - <?php echo $a['NOME_FICHA']; ?></strong>' associada ao(s) cenário(s) '<strong><?php echo $a['CENARIOS']; ?></strong>' foi retirada do Job.
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
	<br>
	<p align="center">
		<input type="button" name="btnImprimir" id="btnImprimir" value="Imprimir" onclick="printSelection($j('.modalEstatica')[0])" />
		<input type="button" name="btnFechar" id="btnFechar" value="Fechar janela" onclick="$j('#avisos_cenario').dialog('close');" />
	</p>
</div>
<script type="text/javascript">
$j(function(){
	$j('#avisos_cenario').dialog({
		height: $j(window).height() - 100,
		width: '95%',
		modal: true,
		title: 'Pendências do Job',
		autoOpen: false,
		close: function(){
			$j('#avisos_cenario').remove();
		}
	}).dialog('open');	
});

</script>