<?php if(!isset($criarEmLote)) {?>
<div id="problemas" class="modalEstatica">
<?php }
//print_rr($problemas);die;?>
	<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabela_padrao">
		<?php foreach($problemas as $praca => $resultado):?>
			<thead>
				<tr>
					<td colspan="7"><?php echo $praca; ?></td>
				</tr>
				<tr>
					<td width="1%">&nbsp;</td>
					<td width="10%">Produto</td>
					<td width="11%">Código Regional</td>
					<td width="16%">Descrição</td>
					<td width="4%">Página</td>
					<td width="4%">Ordem</td>
					<td width="54%">Mensagem</td>
				</tr>
			</thead>
<?php
			foreach($resultado as $tipo => $itens):
				//$tipos == 0 ? $tipo = 'erros' : $tipo = $tipos;
				$cor = '';
				switch( $tipo ){
					case 'avisos': $cor = '#FFFF00'; break;
					case 'erros' : $cor = '#FF0000'; $erro = 1; break;
				}
				
				if($tipo == 'avisos' || $tipo == 'erros'):

						foreach($itens as $item):
							if(isset($item['MENSAGEM']) && !empty($item['MENSAGEM'])){
	?>
							<tr>
								<td bgcolor="<?php echo $cor; ?>">&nbsp;</td>
								<td><?php echo $item['PRODUTO']; ?></td>
								<td><?php echo val($item,'CODIGO_REGIONAL'); ?></td>
								<td><?php echo $item['NOME_FICHA']; ?></td>
								<td><?php echo $item['PAGINA']; ?></td>
								<td><?php echo $item['ORDEM']; ?></td>
								<td><?php echo $item['MENSAGEM']; ?></td>
							</tr>
	<?php
							}
						endforeach;
				endif;
			endforeach;
?>
		<?php endforeach; ?>
		<tfoot>
			<tr>
				<td colspan="7"><strong>Tempo decorrido</strong>: <?php echo number_format($tempo_decorrido, 4); ?></td>
			</tr>
		</tfoot>
	</table>
	<?php foreach($problemas as $praca => $resultado):?>
		<?php foreach($resultado as $tipos => $itens):?>
			<?php if(isset($resultado['fichas_adicionadas']) && !empty($resultado['fichas_adicionadas'])): ?>
				<?php if((count($itens) > 0) && (((isset($itens[0]['OBJETO'])) && (!empty($itens[0]['OBJETO']))) || ((isset($itens['MENSAGEM'])) && (substr($itens['MENSAGEM'], -22) != 'não está cadastrada!')))): ?>
					<br/>	
								
					<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabela_padrao">
						<thead>
							<tr>
								<td colspan="5">Novos Produtos Cadastrados para a praça <?php echo $praca; ?></td>
							</tr>
							<tr>
								<td width="1%">&nbsp;</td>
								<td width="10%">Produto</td>
								<td width="20%">Descrição</td>
								<td width="39%">Mensagem</td>
								<td width="30%">Associado ao Objeto</td>
							</tr>
						</thead>
						<?php foreach($itens as $item): ?>
							<tr>
								<td bgcolor="green"></td>
								<td><?php echo $item['COD_BARRAS_FICHA']; ?></td>
								<td><?php echo $item['NOME_FICHA']; ?></td>
								<td>O produto (<?php echo $item['COD_BARRAS_FICHA']; ?>) foi cadastrado com sucesso.</td>
								<td><?php echo $item['OBJETO']; ?></td>
							</tr>
						<?php endforeach; ?>
					</table>
				<?php endif; ?>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endforeach; ?>
	


	<p align="center">
		<input type="button" name="btnImprimir" id="btnImprimir" value="Imprimir" onclick="printSelection($j('.modalEstatica')[0])" />
		<input type="button" name="btnFechar" id="btnFechar" value="Fechar janela" onclick="$j('#problemas').dialog('close');" />
		<?php if((isset($comErros) && $comErros != 1) && (!isset($erro))): ?>
			<input type="button" name="btnContinuar" id="btnContinuar" class="continuar" value="Continuar" />
		<?php endif; ?>
        <?php if((Sessao::hasPermission('ficha','criarFichasEmLote')) && (isset($fichasNaoEncontradas) && count($fichasNaoEncontradas) > 0)): ?>
        	<input type="button" id="btnCriarEmLote" onClick="$j('#cadastrarEmLote').val('1');" class="continuar" value="Cadastrar Produtos" />
		<?php endif; ?>
	</p>
<?php if(!isset($criarEmLote)) {?>
</div>
<?php }?>
<script type="text/javascript">
$j(function(){
	$j('#problemas').dialog({
		height: $j(window).height() - 100,
		width: '95%',
		modal: true,
		title: 'Pendências do Job "<?php echo addslashes(post('TITULO_JOB',true)); ?>"',
		autoOpen: false,
		close: function(){
			//window.location.href = "<?php // echo site_url('carrinho/form/') . '/' . $this->uri->segment(3); ?>";
			$j('#problemas').remove();
		}
	}).dialog('open');

	$j(".continuar").click(function(){
		$j("#btnCriarEmLote").hide();
		if($j("#btnCriarEmLote").attr("disabled") != "undefined"){
			if($j("#formImportacao").size() > 0  && $j('#cadastrarEmLote').val() != "1"){
				$j("#formImportacao").attr("action", $j("#formImportacao").attr("action") + "/1").submit();
				$j("#btnCriarEmLote").removeAttr("disabled");
			} else{
				$j("#btnCriarEmLote").attr("disabled", "disabled");
				var emLote = $j('#cadastrarEmLote').val();
				
				if(emLote == 1){  
					if($j("#formImportacao").size() > 0)
						var valorPost = $j("#formImportacao").serialize();
					else
						var valorPost = $j("#form1").serialize();
					$j.ajax({
			            type: "POST",
			            data: { post: valorPost },
			            url: '<?php echo base_url().index_page(); ?>/carrinho/criarEmLote',
			            success: function(result){
			            	$j("#btnCriarEmLote").removeAttr("disabled");
				            if(result == 1){
				            	$j('#problemas').dialog('close');
								$j('#criarEmLote').html('<div class="box_erros"><br>Não existe subcategoria cadastrada para o cliente para realizar a inserção em lote.</div><br />');
					        } else{
			            		$j('.modalEstatica').html(result);
			            		$j('#cadastrarEmLote').val('0');
						    }
			            }
			        });
				} else{
					$j("#form1").attr("action", $j("#form1").attr("action") + "/0/0").submit();
				}
			}
		}	
	});
});

function cadastroEmLote(){
	var urlImportacaoEmLote = "<?php echo base_url().index_page().'/json/ficha/cadastrarEmLote/'; ?>";
	$j.post(urlImportacaoEmLote, $j("#formLote").serialize());
}

</script>


























