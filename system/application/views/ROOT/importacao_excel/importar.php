<?php $this->load->view("ROOT/layout/header"); ?>

<div id="contentGlobal">

<h1>Jobs / Importa&ccedil;&atilde;o de Excel</h1>

<?php 
if( !empty($erros) ){
	echo '<div class="box_erros">', implode('<br />', $erros), '</div>';
}
?>

<?php
if(!empty($problemas)) {
	include(dirname(__FILE__).'/resultado_validacao.php');
}
?>

<div style="float:right; padding-right:20px; display:none">
	<? if(isset($id)): ?>
    	<img id="imgTemplate" src="<?= base_url() ?>img.php?img_id=<?= $PREVIEW_TEMPLATE; ?>&img_size=big&rand=<?= rand()?>"  height="230" />
	<? else: ?>
		<img id="imgTemplate" src="" style="display:none"  height="230" />
	<? endif; ?>
</div>


<form id="formImportacao" action="<?php echo base_url().index_page().'/importacao_excel/importar/'.sprintf('%d',$id).'/executar'; ?>" method="post" enctype="multipart/form-data">

<?php if(isset($fichasNaoEncontradas) && count($fichasNaoEncontradas) > 0): ?>
	<?php foreach($fichasNaoEncontradas as $fne): ?>
		<input type="hidden" name="FICHAS_NAO_ENCONTRADAS[]" value="<?php echo $fne; ?>">
	<?php endforeach; ?>
<?php endif; ?>
<input type="hidden" name="CADASTRAR_EM_LOTE" id="cadastrarEmLote" value="0">
<input type="hidden" name="DESC_CLIENTE" id="DESC_CLIENTE" value="<?= !empty($_sessao['DESC_CLIENTE']) ? $_sessao['DESC_CLIENTE'] : '';?>">
<input type="hidden" name="DESC_AGENCIA" id="DESC_AGENCIA" value="<?= !empty($_sessao['DESC_AGENCIA']) ? $_sessao['DESC_AGENCIA'] : '';?>">
<input type="hidden" name="DESC_PRODUTO" id="DESC_PRODUTO" value="<?= !empty($_sessao['DESC_PRODUTO']) ? $_sessao['DESC_PRODUTO'] : '';?>">
<input type="hidden" name="DESC_CAMPANHA" id="DESC_CAMPANHA" value="<?= !empty($_sessao['DESC_CAMPANHA']) ? $_sessao['DESC_CAMPANHA'] : '';?>">
<input type="hidden" name="NOME_USUARIO_RESPONSAVEL" id="NOME_USUARIO_RESPONSAVEL" value="<?= !empty($_sessao['NOME_USUARIO_RESPONSAVEL']) ? $_sessao['NOME_USUARIO_RESPONSAVEL'] : '';?>">
 <?php //print_rr($_sessao);die;?>
 <div id="pesquisa_completo" style="float:left">
 <table width="100%" border="0" cellspacing="0" cellpadding="0" id="contentPcomplete">
<?php if(!empty($_sessao['ID_AGENCIA'])) : ?>
        <tr>
          <td width="175">Ag&ecirc;ncia *</td>
          <td width="917">
            <?php sessao_hidden('ID_AGENCIA','DESC_AGENCIA'); ?>
		  </td>
        </tr>
        <tr>
          <td>Cliente *</td>
          <td>
            <select name="ID_CLIENTE" id="ID_CLIENTE">
              <option value=""></option>
              <?php echo montaOptions($clientes, 'ID_CLIENTE','DESC_CLIENTE', post('ID_CLIENTE',true)); ?>
            </select>
          </td>
        </tr>
	<?php elseif(!empty($_sessao['ID_CLIENTE'])): ?>
        <tr>
          <td>Cliente *</td>
          <td>
            <?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?></td>
        </tr>
        <tr>
          <td width="175">Ag&ecirc;ncia *</td>
          <td width="917">
            <select name="ID_AGENCIA" id="ID_AGENCIA">
              <option value=""></option>
              <?php echo montaOptions($agencias, 'ID_AGENCIA','DESC_AGENCIA', post('ID_AGENCIA',true)); ?>
            </select>
			</td>
        </tr>
	<?php endif; ?>
    <tr>
      <td>Bandeira *</td>
      <td colspan="2"><?php if(!empty($_sessao['ID_PRODUTO'])): ?>
        <?php sessao_hidden('ID_PRODUTO','DESC_PRODUTO'); ?>
        <?php else: ?>
        <select name="ID_PRODUTO" id="ID_PRODUTO">
          <option value=""></option>
          <?php echo montaOptions($produtos, 'ID_PRODUTO','DESC_PRODUTO', post('ID_PRODUTO',true)); ?>
        </select>
        <?php endif; ?>
     </td>
    </tr>
    <tr>
      <td>Campanha *</td>
      <td colspan="2">
      	<select name="ID_CAMPANHA" id="ID_CAMPANHA">
          <option value=""></option>
          <?php echo montaOptions($campanhas, 'ID_CAMPANHA','DESC_CAMPANHA', post('ID_CAMPANHA',true)); ?>
        </select>
      </td>
    </tr>
    <!-- NAO ESCOLHE MAIS INDD AQUI!
    <tr style="display: none">
      <td>Template</td>
      <td colspan="2">
        <select name="ID_TEMPLATE" id="ID_TEMPLATE">
          <option value=""></option>
          <?php echo montaOptions($templates, 'ID_TEMPLATE','DESC_TEMPLATE', post('ID_TEMPLATE',true)); ?>
          </select>
      </td>
      </tr>
	 NAO ESCOLHE MAIS INDD AQUI! -->
    <tr>
      <td>Tipo de pe&ccedil;a *</td>
      <td colspan="2">
      <select name="ID_TIPO_PECA" id="ID_TIPO_PECA">
        <option value=""></option>
        <?php echo montaOptions($tiposPeca, 'ID_TIPO_PECA','DESC_TIPO_PECA', post('ID_TIPO_PECA',true)); ?>
      </select>
      </td>
    </tr>
    <tr>
      <td>Tipo de Job *</td>
      <td colspan="2">
      <select name="ID_TIPO_JOB" id="ID_TIPO_JOB">
        <option value=""></option>
        <?php echo montaOptions($tiposJob, 'ID_TIPO_JOB','DESC_TIPO_JOB', post('ID_TIPO_JOB',true)); ?>
      </select>
      </td>
    </tr>
    <tr>
      <td>T&iacute;tulo *</td>
      <td colspan="2">
	  <span class="TEXTO_EXPLICATIVO" style="display:block;"></span>
	  <input name="TITULO_JOB" type="text" id="TITULO_JOB" size="50" maxlength="150" value="<?php post('TITULO_JOB'); ?>" />
	  
	  </td>
    </tr>
    <?php if(!$_sessao['IS_HERMES']){ ?>
    <tr>
    	<td>Formato *</td>
    	<td colspan="2"><input class="centimetragem" name="LARGURA_JOB" type="text" id="LARGURA_JOB" size="8" maxlength="7" value="<?php post('LARGURA_JOB'); ?>" />
X
	<input class="centimetragem" name="ALTURA_JOB" type="text" id="ALTURA_JOB" size="8" maxlength="7" value="<?php post('ALTURA_JOB'); ?>" /></td>
    	</tr>
    <tr>
    <?php } ?>
      <td>N&uacute;mero de P&aacute;ginas *</td>
      <td colspan="2"><input name="PAGINAS_JOB" type="text" id="PAGINAS_JOB" size="5" maxlength="5" value="<?php post('PAGINAS_JOB'); ?>" /></td>
    </tr>
    <tr>
    	<td>Data de in&iacute;cio do Processo  *</td>
    	<td colspan="2"><input name="DATA_INICIO_PROCESSO" type="text" id="DATA_INICIO_PROCESSO" size="10" maxlength="10" class="_calendar" value="<?php post('DATA_INICIO_PROCESSO'); ?>" readonly="readonly" /></td>
    	</tr>
    <tr>
      <td>In&iacute;cio  da Validade *</td>
        <td colspan="2"><input name="DATA_INICIO" type="text" id="DATA_INICIO" size="10" maxlength="10" value="<?php post('DATA_INICIO'); ?>" readonly="readonly" /></td>
    	</tr>
    <tr>
      <td>Final da Validade *</td>
      <td colspan="2">
      	<?=form_input(array('id'=>'DATA_TERMINO','name'=>'DATA_TERMINO', 'class'=>'_calendar', 'size'=>'10', 'value'=>post('DATA_TERMINO',true),'readonly'=>'readonly'));?>
      	</td>
    </tr>
    <tr>
      <td valign="top">Observa&ccedil;&otilde;es</td>
      <td colspan="2"><textarea name="OBSERVACOES_EXCEL" cols="50" rows="6" id="OBSERVACOES_EXCEL"><?php post('OBSERVACOES_EXCEL'); ?></textarea></td>
    </tr>
	<tr>
		<td>Responsável pelo Job</td>
		<td>
			<select name="ID_USUARIO_RESPONSAVEL" id="ID_USUARIO_RESPONSAVEL">
				<option value=""></option>
        	</select>
		</td>
	</tr>
    <tr>
      <td>Arquivo de Excel </td>
      <td colspan="2"><input type="file" name="file" id="file" /></td>
    </tr>
    <?php if(!empty($filename)): ?>
    <tr>
      <td>Arquivo enviado anteriormente</td>
      <td colspan="2"><?php echo str_replace('files/', '', $filename); ?> (para continuar usando este mesmo arquivo, clique em importar)</td>
    </tr>
    <?php endif; ?>
    <tr>
      <td>&nbsp;</td>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
    	<td></td>
    	<td width="917">
        	<table>
                <tr>
                	<td><a class="button" href="javascript:" onclick="location.href='<?= site_url('plano_midia/index'); ?>';"><span>Cancelar</span></a></td>
	                <td><a class="button" href="javascript:" onclick="$j(this).closest('form').submit();" ><span>Importar</span></a></td>
					<?php if(!empty($filename) && empty($comErros)): ?>
                        <td><a class="button" href="javascript:" id="btnContinuar"><span>Continuar</span></a></td>
                    <?php endif; ?>
                </tr>
            </table>
    	</td>
    </tr>
  </table>
  </div>
  <div style="clear:both"></div>
</form>

<script type="text/javascript">

var mascaras = {};
<?php
if( !empty($tiposPeca) ) {
	foreach($tiposPeca as $tipo){
		printf('mascaras[%d] = {mascara: "%s", tip: "%s"};' . PHP_EOL, $tipo['ID_TIPO_PECA'], $tipo['MASCARA'], addslashes($tipo['TEXTO_EXPLICATIVO']));
	}
}
?>

function preencheTipos(json){
	montaOptions($('ID_TIPO_PECA'), json, 'ID_TIPO_PECA', 'DESC_TIPO_PECA');
}


$j(function(){
	<?php if(empty($_sessao['ID_AGENCIA'])) { ?>
	$j('#DESC_AGENCIA').val($j.trim($j('#ID_AGENCIA').find(":selected").text()));
	<?php }?>
	<?php if(empty($_sessao['ID_CLIENTE'])) { ?>
	$j('#DESC_CLIENTE').val($j.trim($j('#ID_CLIENTE').find(":selected").text()));
	<?php }?>
	$j('#DESC_PRODUTO').val($j.trim($j('#ID_PRODUTO').find(":selected").text()));
	$j('#NOME_USUARIO_RESPONSAVEL').val($j.trim($j('#ID_USUARIO_RESPONSAVEL').find(":selected").text()));
	$j('#DESC_CAMPANHA').val($j.trim($j('#ID_CAMPANHA').find(":selected").text()));
	
	
	$j('._calendar').datepicker({dateFormat: 'dd/mm/yy',showOn: 'button', buttonImage: '<?= base_url().THEME ?>img/calendario.png', buttonImageOnly: true});

	adicionarMascaras();
	// mascaras do formato
	mascarasFormato('.centimetragem');

	//monta select de usuarios responsaveis qndo a pagina carrega
	<?php if( (!empty($_POST['ID_CLIENTE'])) && ($_POST['ID_CLIENTE'] > 0) ): ?>
		var idCliente = <?php echo $_POST['ID_CLIENTE']; ?>;
		var idUsuarioResponsavel = <?php if( isset( $_POST['ID_USUARIO_RESPONSAVEL'] ) && $_POST['ID_USUARIO_RESPONSAVEL'] > 0 ){ echo $_POST['ID_USUARIO_RESPONSAVEL']; } else{ echo '0'; } ?>;
		montaOptionsAjax($('ID_USUARIO_RESPONSAVEL'),'<?php echo site_url('json/admin/getUsuariosByCliente'); ?>','id=' + idCliente,'ID_USUARIO','NOME_USUARIO', idUsuarioResponsavel);
	<?php elseif(!empty($_sessao['ID_CLIENTE'])): ?>
		var idCliente = <?php echo $_sessao['ID_CLIENTE']; ?>;
		var idUsuarioResponsavel = <?php if( isset( $_POST['ID_USUARIO_RESPONSAVEL'] ) && $_POST['ID_USUARIO_RESPONSAVEL'] > 0 ){ echo $_POST['ID_USUARIO_RESPONSAVEL']; } else{ echo '0'; } ?>;
		montaOptionsAjax($('ID_USUARIO_RESPONSAVEL'),'<?php echo site_url('json/admin/getUsuariosByCliente'); ?>','id=' + idCliente,'ID_USUARIO','NOME_USUARIO', idUsuarioResponsavel);
	<?php endif; ?>

	<?php if(!empty($_sessao['ID_CLIENTE'])): ?>
	$('ID_AGENCIA').addEvent('change', function(){
		$j('#DESC_AGENCIA').val($j.trim($j('#ID_AGENCIA').find(":selected").text()));
		clearSelect($('ID_PRODUTO'),1);
		clearSelect($('ID_CAMPANHA'),1);
		montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByAgencia'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
	});
	<?php endif; ?>
	
	<?php if(!empty($_sessao['ID_AGENCIA'])): ?>
	$('ID_CLIENTE').addEvent('change', function(){
		$j('#DESC_CLIENTE').val($j.trim($j('#ID_CLIENTE').find(":selected").text()));
		clearSelect($('ID_CAMPANHA'),1);
		clearSelect($('ID_PRODUTO'),1);
		clearSelect($('ID_USUARIO_RESPONSAVEL'),1);
				
		carregaTiposPeca( this.value, preencheTipos );
		
		montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByCliente'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
		montaOptionsAjax($('ID_USUARIO_RESPONSAVEL'),'<?php echo site_url('json/admin/getUsuariosByCliente'); ?>','id=' + this.value,'ID_USUARIO','NOME_USUARIO');
	});
	<?php endif; ?>
	
	$('ID_PRODUTO').addEvent('change', function(){
		$j('#DESC_PRODUTO').val($j.trim($j('#ID_PRODUTO').find(":selected").text()));
		clearSelect($('ID_CAMPANHA'),1);
		montaOptionsAjax($('ID_CAMPANHA'),'<?php echo site_url('json/admin/getCampanhasByProduto'); ?>','id=' + this.value,'ID_CAMPANHA','DESC_CAMPANHA');
	});
	
	$('ID_TIPO_PECA').addEvent('change', function(){
		mudaMascara( this.value );
	});

	$('ID_USUARIO_RESPONSAVEL').addEvent('change', function(){
		$j('#NOME_USUARIO_RESPONSAVEL').val($j.trim($j('#ID_USUARIO_RESPONSAVEL').find(":selected").text()));
	});


	$('ID_CAMPANHA').addEvent('change', function(){
		$j('#DESC_CAMPANHA').val($j.trim($j('#ID_CAMPANHA').find(":selected").text()));
	});
	
	if( $('ID_TIPO_PECA').value != '' ){
		mudaMascara($('ID_TIPO_PECA').value);
	}
	
	//Inicio Calculo da DATA_INICIO

	$j('#DATA_INICIO_PROCESSO').datepicker('option',{minDate: new Date()});
	
	if($j('#DATA_INICIO_PROCESSO').val() == ""){
		if ( ($j('#ID_PRODUTO').val() != "") && ($j('#ID_TIPO_PECA').val() != "") ) {
			$j('#DATA_INICIO_PROCESSO').next().show();
		}else{
			$j('#DATA_INICIO_PROCESSO').next().hide();
		}  
	}
	
	if($j('#DATA_INICIO').val() != ""){
		var dateArray = $j('#DATA_INICIO').val().split('/');
		$j('#DATA_TERMINO').datepicker('option',{minDate: new Date(dateArray[2], dateArray[1]-1, dateArray[0])});
		$j('#DATA_TERMINO').next().show();
	}else{
		$j('#DATA_TERMINO').next().hide();
	}	
	


	// Habilita datepicker da DATA_INICIO_PROCESSO somente se o tipo de peca e a bandeira estiverem selecionados
	$j('#ID_TIPO_PECA').change(
		function(evt){
			$j('#DATA_INICIO_PROCESSO').val('');
			$j('#DATA_INICIO').val('');
			$j('#DATA_TERMINO').val('');
			if($j(this).val() == ""){
				$j('#DATA_INICIO_PROCESSO').next().hide();
				$j('#DATA_TERMINO').next().hide();
			}else{
				if ($j(this).val() != "" && $j('#ID_PRODUTO').val() != "") {
					$j('#DATA_INICIO_PROCESSO').next().show();
				}
			}
		}
	);

	$j('#ID_PRODUTO').change(
		function(evt){
			$j('#DATA_INICIO_PROCESSO').val('');
			$j('#DATA_INICIO').val('');
			$j('#DATA_TERMINO').val('');
			if($j(this).val() == ""){
				$j('#DATA_INICIO_PROCESSO').next().hide();
				$j('#DATA_TERMINO').next().hide();
			}else{
				if ($j(this).val() != "" && $j('#ID_TIPO_PECA').val() != "") {
					$j('#DATA_INICIO_PROCESSO').next().show();
				}
			}
		}
	);

	//Calcula DATA_INICIO apos escolher DATA_INICIO_PROCESSO
	$j('#DATA_INICIO_PROCESSO').datepicker('option',{
		onSelect: function(dateText, inst) { 
			$j.post("<?= site_url('json/admin/calculaDataInicio')?>", 
					{date:dateText, idTipoPeca: $j('#ID_TIPO_PECA').val(), idProduto: $j('#ID_PRODUTO').val(), compararDataAtual: '1'}, 
					function(data, textStatus, XMLHttpRequest){
						data = eval(data);
						data = data[0];
						if(data.erro == ''){
							$j('#DATA_INICIO').attr('value', data.result);
							$j('#DATA_TERMINO').next().show();

							var dateArray = data.result.split('/');
							$j('#DATA_TERMINO').datepicker('option',{minDate: new Date(dateArray[2], dateArray[1]-1, dateArray[0])});
							$j('#DATA_TERMINO').val('');
							
						}else{
							$j('#DATA_INICIO').val('');
							$j('#DATA_TERMINO').val('');
							$j('#DATA_TERMINO').next().hide();
							alert(data.erro);
							$j('#DATA_INICIO_PROCESSO').val('');
						}
					}
			);
		}
	});

	//Fim calculo DATA_INICIO
});

$j(function(){
	$j('.ui-datepicker').hide();
});

</script>

</div> <!-- Final de Content Global -->

<?php $this->load->view("ROOT/layout/footer"); ?>