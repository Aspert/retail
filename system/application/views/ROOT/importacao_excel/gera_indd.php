<?php $this->load->view("ROOT/layout/header"); ?>

<div id="contentGlobal">

<h1>Jobs / Importa&ccedil;&atilde;o de Excel</h1>

<div id="textoJson">

<p>Escolha os arquivos das pra&ccedil;as que deseja gerar os templates inicias.</p>
<p>Caso deseje realizar a atualiza&ccedil;&atilde;o dos INDD com a utiliza&ccedil;&atilde;o do Plugin, acesse o mesmo pelo InDesign.</p>
<p>&nbsp;</p>
<?php 

foreach($pracas as $praca){
	echo '<div class="itemPracaGerar">';
	printf('<input type="checkbox" name="pracas[]" class="praca" checked="checked"> <strong>%s</strong> <br />',
		$praca['DESC_PRACA'],
		$praca['ID_PRACA']
	);
	
	foreach($arquivos as $item){
		printf('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="arquivos[]" class="arquivos" value="%d-%d" checked="checked"> %s <span id="status_%d-%d"></span><br />',
			$praca['ID_PRACA'],
			$item['ID_TEMPLATE_ITEM'],
			'Da p&aacute;gina '.$item['PAGINA_INICIO_TEMPLATE_ITEM'].' at&eacute; a p&aacute;gina '.$item['PAGINA_FIM_TEMPLATE_ITEM'],
			$praca['ID_PRACA'],
			$item['ID_TEMPLATE_ITEM']
		);
	}
	echo '</div>';
}
?>
<a href="javascript:" class="button" onclick="gerar()"><span>Gerar INDD's</span></a>
</div>
<span id="finalizando"></span>

<script type="text/javascript">
var _queue = [];
var _xml = [];
var loadBar = ' <img id="imgProgress" src="<?= base_url()?>/img/AJAX_LoadingBar.gif" alt="uploading..." align="absmiddle" />';
<?php
$id = $this->uri->segment(3);
?>

function proccessQueue(){
	if(_queue.length > 0){
	
		var item = _queue.shift();
		$('status_'+item).innerHTML = ' - Rodando processo 2 de 2 ' + loadBar;
	
		$j.post('<?php echo base_url().index_page() . '/gerar_indd/gera_indd/' . $id; ?>/gerar/' + item.replace('-','/'),
			null,
			function(json){
				$('status_'+item).innerHTML = ' - Resultado do processo 2 de 2: ' + json;
				proccessQueue();
			}
		);
		
	} else {
		alert('Processo terminado');
		return;
		
		$('finalizando').innerHTML = 'Finalizando ... ' + loadBar;
		
		$j('.linkExcel').css('display','');
		var txt = $j('#textoJson').html();
		$j('.linkExcel').css('display','none');
		
		$j.post('<?php echo base_url().index_page() . '/gerar_indd/gera_indd/' . $id; ?>/final',
			'texto=' + txt,
			function(json){
				$j('.linkExcel').css('display','');
				$('finalizando').innerHTML = '';
				alert(json);
			}
		);
	}
}

function geraXML(){
	if( _xml.length > 0 ){
		var xml = _xml.shift();
		$j('#status_' + xml).html( ' - Rodando processo 1 de 2 ' + loadBar );
		
		$j.post('<?php echo base_url().index_page() . '/gerar_indd/gera_indd/' . $id; ?>/xml/' + xml.replace('-','/'), null, function(retorno){
			$j('#status_' + xml).html(' - Processo 1 de 2 finalizado');
			geraXML();
		});
	} else {
		proccessQueue();
	}
	
}

function gerar(){
	if( confirm('<?php echo utf8_encode('Deseja gerar o arquivo INDD das pra�as selecionadas com arquivo de template selecionado?'); ?>') ){
		$j('.arquivos').each(function(){
			if( this.checked ){
				_xml.push( this.value );
				_queue.push( this.value );
			}
		});
		
		geraXML();
	}
}

$j(function(){
	$j('.praca').click(function(){
		var item = this;
		$j(this).parent().find('.arquivos').each(function(){
			this.checked = item.checked;
		});
	});
});

//proccessQueue();
</script>

</div> <!-- Final de Content Global -->

<?php $this->load->view("ROOT/layout/footer"); ?>