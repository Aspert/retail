<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
	<h1>Objetos do Job
		<?= $job ?>
	</h1>
	<div>
		<applet 
			code="br.com.id247.retail.downloader.DownloadObjetos.class"
			archive="Downloader.jar, lib/AbsoluteLayout.jar"
			codebase = "<?php echo base_url(); ?>java/Downloader/dist"
			width="100%" height="300"
			alt="Download de Objetos"
			title="Download de Objetos">
			
			<param name="fileList" value="<?php echo site_url('gerar_indd/objetos/'.$this->uri->segment(3).'/0/1'); ?>" />
		
		</applet>
	</div>
</div>
<!-- Final de Content Global -->
<?php $this->load->view("ROOT/layout/footer") ?>
