<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">

<h1>Jobs prontos para Gerar INDD</h1>

<div id="pesquisa_simples">
	<?php echo form_open('gerar_indd/listar_agencia');?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  
  <?php if(!empty($_sessao['ID_AGENCIA'])): ?>
    <td width="14%" style="text-align:left" align="left">    Ag&ecirc;ncia</td>
    <td width="17%" style="text-align:left">Cliente</td>
	<?php elseif(!empty($_sessao['ID_CLIENTE'])): ?>
    <td width="14%" style="text-align:left">Cliente</td>
    <td width="17%" style="text-align:left" align="left">    Ag&ecirc;ncia</td>
  <?php endif; ?>
    <td width="18%" style="text-align:left">Bandeira</td>
    <td width="12%" style="text-align:left">Campanha</td>
    <td width="16%" style="text-align:left">Tipo de Pe&ccedil;a</td>
    <td width="23%">&nbsp;</td>
    <td width="0%" rowspan="4">&nbsp;</td>
  </tr>
  <tr>
  
  <?php if(!empty($_sessao['ID_AGENCIA'])): ?>
    <td align="left" style="text-align:left"><?php sessao_hidden('ID_AGENCIA','DESC_AGENCIA'); ?></td>
    <td style="text-align:left">
      <select name="ID_CLIENTE" id="ID_CLIENTE">
        <option value=""></option>
        <?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', !empty($busca['ID_CLIENTE']) ? $busca['ID_CLIENTE'] : ''); ?>
        </select>
	  </td>
	<?php elseif(!empty($_sessao['ID_CLIENTE'])): ?>
    <td style="text-align:left"><?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?></td>
    <td align="left" style="text-align:left">
      <select name="ID_AGENCIA" id="ID_AGENCIA">
        <option value=""></option>
        <?php echo montaOptions($agencias,'ID_AGENCIA','DESC_AGENCIA', !empty($busca['ID_AGENCIA']) ? $busca['ID_AGENCIA'] : ''); ?>
      </select>
    </td>
  <?php endif; ?>
	  
	  
    <td style="text-align:left"><?php if(!empty($_sessao['ID_PRODUTO'])): ?>
        <?php sessao_hidden('ID_PRODUTO','DESC_PRODUTO'); ?>
        <?php else: ?>
      <select name="ID_PRODUTO" id="ID_PRODUTO">
        <option value=""></option>
        <?php echo montaOptions($produtos,'ID_PRODUTO','DESC_PRODUTO', !empty($busca['ID_PRODUTO']) ? $busca['ID_PRODUTO'] : ''); ?>
    </select>
      <?php endif; ?></td>
    <td style="text-align:left"><select name="ID_CAMPANHA" id="ID_CAMPANHA">
      <option value=""></option>
      <?php echo montaOptions($campanhas,'ID_CAMPANHA','DESC_CAMPANHA', !empty($busca['ID_CAMPANHA']) ? $busca['ID_CAMPANHA'] : ''); ?>
    </select></td>
    <td style="text-align:left"><select name="ID_TIPO_PECA" id="ID_TIPO_PECA">
      <option value=""></option>
      <?php echo montaOptions($tipos,'ID_TIPO_PECA','DESC_TIPO_PECA', !empty($busca['ID_TIPO_PECA']) ? $busca['ID_TIPO_PECA'] : ''); ?>
    </select></td>
    <td>
    	<a class="button" href="javascript:" onclick="limparForm()"><span>Limpar</span></a>
    	<a class="button" href="javascript:" onclick="$j(this).closest('form').submit()"><span>Pesquisar</span></a>
    	
    	<!-- <img id="btnLimpaPesquisa" src="<?=base_url();?>img/botao_limpar.gif" class="btn" title="Limpar" alt="Limpar" style="padding-bottom: 3px;" />
      	<input class="btn" type="image" name="Pesquisar" id="Pesquisar" src="<?php echo base_url(); ?>img/lupa.gif" /> --></td>
    </tr>
  <tr>
    <td align="left" style="text-align:left">Tipo de Job</td>
    <td style="text-align:left">T&iacute;tulo do Job</td>
    <td style="text-align:left">Itens por p&aacute;gina</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
  <tr>
    <td align="left" style="text-align:left"><select name="ID_TIPO_JOB" id="ID_TIPO_JOB">
      <option value=""></option>
      <?php echo montaOptions($tiposJob,'ID_TIPO_JOB','DESC_TIPO_JOB', !empty($busca['ID_TIPO_JOB']) ? $busca['ID_TIPO_JOB'] : ''); ?>
    </select></td>
    <td style="text-align:left"><input type="text" name="DESC_EXCEL" id="DESC_EXCEL" value="<?php echo !empty($busca['DESC_EXCEL']) ? $busca['DESC_EXCEL'] : ''; ?>" /></td>
    <td style="text-align:left">
      <select name="pagina" id="pagina">
        <?php
		$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
		for($i=5; $i<=50; $i+=5){
			printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
		}
		?>
      </select>
    </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
</table>
<br />
  <?php echo form_close();?>
</div>
<?php if( (!empty($lista))&& (is_array($lista))):?>

<div id="tableObjeto">
<table border="0" width="100%" cellpadding="0" cellspacing="0" class="tableSelection tableMidia" id="tabelaBusca">
<thead>
	<tr>
        <?php if($podeGerar): ?>
		<!-- 
		<th>Gerar</th> 
		-->
  		<?php endif; ?>
        <?php if($verHistorico): ?>
		<th>Hist&oacute;rico</th>
  		<?php endif; ?>
        <?php if($podeObjeto): ?>
		<th align="center">Objetos</th>
	  <?php endif; ?>
	  <th class="agenciaJob" align="left" id="colAgencia">Ag&ecirc;ncia</th>
	  <th class="clienteJob" align="left" id="colCliente">Cliente</th>
	  <th class="bandeiraJob" align="left" id="colBandeira">Bandeira</th>
      <th class="campanhaJob" align="left" id="colCampanha">Campanha</th>
      <th class="tipoJob" align="left" id="colTipoJob">Tipo de Job</th>
      <th class="tipopecaJob" align="left" id="colTipoPeca">Tipo de Pe&ccedil;a</th>
	  <th class="codigoJob" align="left" id="colCodigoJob">N&uacute;mero Job</th>
      <th class="tituloJob" align="left" id="colJob">T&iacute;tulo do Job</th>
		<th class="inicioJob">In&iacute;cio  da Validade</th>
		<th class="terminoJob">Final da Validade</th>
	  <th class="mudancaJob">Mudan&ccedil;a Etapa</th>
	  <th class="situacao">Situa&ccedil;&atilde;o</th>
  <!-- 
  <?php if($podeBaixar): ?>
        <th align="left">Download</th>
      <?php endif; ?>
  -->
  </tr>
  </thead>
		<?php $cont=0; ?>
		<?php foreach($lista as $item):?>

		<tr>
			<?php if($podeGerar): ?>
	        <!-- 
	        <td align="center">
            	<?php if($item['CHAVE_ETAPA'] != "APROVACAO"): ?>
            	<a href="<?php echo site_url('gerar_indd/form/'. $item['ID_EXCEL']); ?>">
                	<img src="<?php echo base_url().THEME; ?>img/file_edit.png" alt="Gerar" border="0" />
                </a> 
                <?php endif; ?>   
            </td>
            -->
            <?php endif; ?>
            
			<?php if($verHistorico): ?>
	        <td align="center"><a href="<?php echo site_url('gerar_indd/historico/'. $item['ID_EXCEL']); ?>"><img src="<?php echo base_url().THEME; ?>img/visualizar_historico.png" alt="Download" border="0" /></a></td>
            <?php endif; ?>
            <?php if($podeObjeto): ?>
            <td align="center"><a href="<?php echo site_url('gerar_indd/objetos/'. $item['ID_EXCEL']); ?>"><img src="<?php echo base_url().THEME; ?>img/file_download.png" alt="Download" border="0" /></a></td>
            <?php endif; ?>
			<td><?=$item['DESC_AGENCIA'];?></td>
			<td><?=$item['DESC_CLIENTE'];?></td>
			<td><?=$item['DESC_PRODUTO'];?></td>
			<td><?=$item['DESC_CAMPANHA'];?></td>
			<td><?=$item['DESC_TIPO_JOB'];?></td>
			<td><?=$item['DESC_TIPO_PECA'];?></td>
			<td><?=$item['ID_JOB'];?></td>
			<td><?=$item['DESC_EXCEL'];?></td>
			<td><?=format_date($item['DATA_INICIO'],'d/m/Y');?></td>
			<td><?=format_date($item['DATA_TERMINO'],'d/m/Y');?>
				<br />
				<?= ($diff = strtotime($item['DATA_TERMINO']) - time()) > 0 ? segundosToHora($diff) : '&nbsp;';?></td>
			<td><?=date('d/m/Y H:i',strtotime($item['DATA_MUDANCA_ETAPA']));?></td>
            <td><?= $item['DESC_STATUS'];?></td>
            <!-- 
            <?php if($podeBaixar): ?>
			<td align="center"><a href="<?php echo site_url('importacao_excel/download/'. $item['ID_EXCEL']); ?>"><img src="<?php echo base_url().THEME; ?>img/disc.png" alt="Download"  border="0" /></a></td>
            <?php endif; ?>
            -->
		</tr>
		<?endforeach;?>
</table>
</div>

<span class="paginacao"><?=(isset($paginacao))?$paginacao:null?></span>
<?else:?>
N&Atilde;O  H&Aacute; RESULTADO PARA A PESQUISA
<?endif;?>

<script type="text/javascript">

	$j("#btnLimpaPesquisa").click(function(){
		limpaForm('#pesquisa_simples');
		if($j("#ID_CLIENTE option").length > 0){
			clearSelect($('ID_CLIENTE'),1);
			clearSelect($('ID_PRODUTO'),1);
		}
		clearSelect($('ID_CAMPANHA'),1);
	});
	
	<?php if(!empty($_sessao['ID_CLIENTE'])): ?>
	$('ID_AGENCIA').addEvent('change', function(){
		clearSelect($('ID_PRODUTO'),1);
		clearSelect($('ID_CAMPANHA'),1);
		montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByAgencia'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
	});
	<?php endif; ?>
	
	<?php if(!empty($_sessao['ID_AGENCIA'])): ?>
	$('ID_CLIENTE').addEvent('change', function(){
		clearSelect($('ID_CAMPANHA'),1);
		clearSelect($('ID_PRODUTO'),1);
		montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByCliente'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
	});
	<?php endif; ?>
	
	$('ID_PRODUTO').addEvent('change', function(){
		clearSelect($('ID_CAMPANHA'),1);
		montaOptionsAjax($('ID_CAMPANHA'),'<?php echo site_url('json/admin/getCampanhasByProduto'); ?>','id=' + this.value,'ID_CAMPANHA','DESC_CAMPANHA');
	});

function limparForm(){
	$j('input, select[name!="pagina"], textarea').val('');
};

$j(function(){
   configuraPauta('<?php echo $busca['ORDER']; ?>', '<?php echo $busca['ORDER_DIRECTION']; ?>');
});

</script>

</div> <!-- Final de Content Global -->

<?php $this->load->view("ROOT/layout/footer") ?>