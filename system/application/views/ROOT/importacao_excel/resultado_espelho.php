<div id="problemas_espelho" class="modalEstatica">
	<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabela_padrao">
		<?php foreach($problemas as $praca => $resultado): ?>
			<tr>
				<td colspan="7">
					<strong><?php echo $praca; ?></strong>
				</td>
			</tr>
			<?php foreach($resultado as $r): ?>
				<tr>
					<td colspan="7">
						<?php echo $r; ?>
					</td>
				</tr>
			<?php endforeach; ?>
		<?php endforeach; ?>
	</table>
	
	<p align="center">
		<input type="button" name="btnImprimir" id="btnImprimir" value="Imprimir" onclick="printSelection($j('.modalEstatica')[0])" />
		<input type="button" name="btnFechar" id="btnFechar" value="Fechar janela" onclick="$j('#problemas_espelho').dialog('close');" />
	</p>
</div>
<script type="text/javascript">
$j(function(){
	$j('#problemas_espelho').dialog({
		height: $j(window).height() - 100,
		width: '95%',
		modal: true,
		title: 'Pendências do Job',
		autoOpen: false,
		close: function(){
			$j('#problemas_espelho').remove();
		}
	}).dialog('open');	
});

</script>