<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">

<?php if ( isset($erros) && is_array($erros) ) : ?>
	<ul style="color:#ff0000;font-weight:bold;" class="box_erros">
		<?php foreach ( $erros as $e ) : ?>
			<li><?=$e?></li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>

<?php echo form_open('grupo/save/'.$this->uri->segment(3), array('id'=>'formGrupoSave'));?>

    <h1>Cadastro de grupos</h1>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="142" style="text-align:left; padding: 6px;">Nome do grupo: <span class="obrigatorio">*</span></td>
    <td colspan="2" style="text-align:left; padding: 6px;"><input name="DESC_GRUPO" type="text" value="<?php post('DESC_GRUPO'); ?>" /></td>
  </tr>
  <tr>
    <td style="text-align:left; padding: 6px;">Chave interna: <span class="obrigatorio">*</span></td>
    <td colspan="2" style="text-align:left; padding: 6px;"> <input name="CHAVE_GRUPO" type="text" value="<?php post('CHAVE_GRUPO'); ?>" /></td>
  </tr>
  <tr>
    <td width="142" style="text-align:left; padding: 6px;">Agência: <span class="obrigatorio">*</span></td>
    <td width="382" style="text-align:left; padding: 6px;"><select name="ID_AGENCIA" id="ID_AGENCIA">
    	<option value=""></option>
           <?php echo montaOptions($agencias,'ID_AGENCIA','DESC_AGENCIA', $_POST['ID_AGENCIA']);?>
        </select></td>
    <td width="568" rowspan="2" style="text-align:left; padding: 6px;">Um grupo pode estar associado somente a uma agência OU a um cliente.<br />
    	Selecionando uma opção, a outra é descartada.</td>
  </tr>
    <tr>
    <td width="142" style="text-align:left; padding: 6px;">Cliente: <span class="obrigatorio">*</span></td>
  <td style="text-align:left; padding: 6px;"><select name="ID_CLIENTE" id="ID_CLIENTE">
  	<option value=""></option>
  	<?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', $_POST['ID_CLIENTE']); ?>
  	</select></td>
  </tr>
  
  <tr>
    <td width="142" valign="top" style="text-align:left; padding: 6px;">Categorias</td>
  <td colspan="2" style="text-align:left; padding: 6px;">
	<select class='padrao' id='categorias' name='categorias[]' multiple style="height: 100px; width:400px">
	 	 <?php
          foreach($categorias as $item){
              printf('<option value="%d"%s>%s</option>'.PHP_EOL,
                     $item['ID_CATEGORIA'],
                     in_array($item['ID_CATEGORIA'], $_POST['categorias']) ? ' selected="selected"' : '',
                     $item['DESC_CATEGORIA']
              );
          }
          ?>
	<?php // echo montaOptions($categorias,'ID_CATEGORIA','DESC_CATEGORIA', $_POST['ID_CATEGORIA']); ?>		  
	</select>
	Segure CTRL para selecionar mais de uma categoria
   </td>
  </tr>
  <tr>
  	<td valign="top" style="text-align:left; padding: 6px;">Bandeiras</td>
  	<td colspan="2" style="text-align:left; padding: 6px;"><select class='padrao' id='produtos' name='produtos[]' multiple style="height:100px; width:400px">
  		<?php
          foreach($produtos as $item){
              printf('<option value="%d"%s>%s</option>'.PHP_EOL,
                     $item['ID_PRODUTO'],
                     in_array($item['ID_PRODUTO'], $_POST['produtos']) ? ' selected="selected"' : '',
                     $item['DESC_PRODUTO']
              );
          }
          ?>
  		<?php // echo montaOptions($categorias,'ID_CATEGORIA','DESC_CATEGORIA', $_POST['ID_CATEGORIA']); ?>
  		</select>
  		Segure CTRL para selecionar mais de uma bandeira</td>
  	</tr>
  	<tr>
	    <td width="142" style="text-align:left; padding: 6px;">Status: <span class="obrigatorio">*</span></td>
	    <td width="382" style="text-align:left; padding: 6px;">
		    <select name="STATUS_GRUPO" id="STATUS_GRUPO">
		    	<option value="1" <?php if(isset($_POST['STATUS_GRUPO']) && ($_POST['STATUS_GRUPO'] == '1')) echo 'selected="selected"'?>>Ativo</option>
				<option value="0" <?php if(isset($_POST['STATUS_GRUPO']) && ($_POST['STATUS_GRUPO'] == '0')) echo 'selected="selected"'?>>Inativo</option>
		    </select>
	    </td>
	</tr>
  	</table>
	<br />
    <a class="button" href="javascript:;" onclick="verificaUsuariosGrupo()"><span>Salvar</span></a>
    <a class="button" href="<?php echo site_url('grupo/lista'); ?>"><span>Cancelar</span></a>

<?php echo form_close(); ?>

</div> <!-- Final de Content Global -->


<script type="text/javascript">
$('ID_AGENCIA').addEvent('change', function(){
	clearSelect($('categorias'), 0);
	clearSelect($('produtos'), 0);
	clearSelect($('ID_CLIENTE'), 1);
	
	if( this.value == '' ){
		montaOptionsAjax($('ID_CLIENTE'),'<?php echo site_url('json/admin/getClientes'); ?>','','ID_CLIENTE','DESC_CLIENTE');
	} else {
		montaOptionsAjax($('ID_CLIENTE'),'<?php echo site_url('json/admin/getClientesByAgencia'); ?>','id=' + this.value,'ID_CLIENTE','DESC_CLIENTE');
	}
});

$('ID_CLIENTE').addEvent('change', function(){
	clearSelect($('categorias'), 0);
	clearSelect($('produtos'), 0);
	
	$('ID_AGENCIA').value = '';
	
	montaOptionsAjax($('categorias'),'<?php echo site_url('json/admin/getCategoriasByCliente'); ?>','id=' + this.value,'ID_CATEGORIA','DESC_CATEGORIA');
	montaOptionsAjax($('produtos'),'<?php echo site_url('json/admin/getProdutosByCliente/0'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
});

function verificaUsuariosGrupo(){

	var status = $j('#STATUS_GRUPO option:selected').val();
	<?php if (isset($_POST['ID_GRUPO'])){?>
		$j.post('<?php echo site_url('json/admin/getUsuarioByGrupo/'.$_POST['ID_GRUPO']); ?>/'+status,
				function(data){
					if(data == '1'){
						$j('#formGrupoSave').submit();
						return true;
					} else {
						alert("Não é possivel inativar grupos que tenham usuários atribuidos!");
						return false;
					}
				}
		);
	<?php } else {?>
		$j('#formGrupoSave').submit();
		return true;
	<?php }?>
}

</script>


<?php $this->load->view("ROOT/layout/footer") ?>
