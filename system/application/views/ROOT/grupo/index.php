<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">

<div id="lista">
	<h1>Cadastro de Grupos</h1>
    <form action="<?php echo site_url('grupo/lista'); ?>" method="post" id="pesquisa_simples" name="form1">
    
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
        <?php if($podeAlterar): ?>
          <td width="7%" rowspan="2" align="center"><a href="<?php echo site_url('grupo/form'); ?>"><img src="<?php echo base_url().THEME; ?>img/file_add.png" alt="Pesquisar" /></a></td>
        <?php endif; ?>
          <td width="28%" style="text-align: left;">Nome do grupo</td>
          <td width="21%" style="text-align: left;">Status</td>
          <td width="21%" style="text-align: left;">Itens por p&aacute;gna</td>
          <td width="44%" rowspan="2"><a class="button" href="javascript:;" onclick="$j(this).closest('form').submit();"><span>Pesquisar</span></a></td>
        </tr>
        <tr>
          <td style="text-align: left;"><input type="text" name="DESC_GRUPO" id="DESC_GRUPO" value="<?php echo !empty($busca['DESC_GRUPO']) ? $busca['DESC_GRUPO'] : ''; ?>" /></td>
          <td style="text-align: left;">
          	<select name="STATUS_GRUPO" id="STATUS_GRUPO">
				<option value="1" <?php if(isset($busca['STATUS_GRUPO']) && ($busca['STATUS_GRUPO'] == '1')) echo 'selected="selected"'?>>Ativo</option>
				<option value="0" <?php if(isset($busca['STATUS_GRUPO']) && ($busca['STATUS_GRUPO'] == '0')) echo 'selected="selected"'?>>Inativo</option>
				<option value="2" <?php if(!isset($busca['STATUS_GRUPO'])) echo 'selected="selected"'?>>Ambos</option>
        	</select>
          </td>
          <td style="text-align: left;">
          	<select name="pagina" id="pagina">
				<?php
	            $pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
	            for($i=5; $i<=50; $i+=5){
					printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
	            }
	            ?>
          	</select>
          </td>
        </tr>
      </table>

	</form>

      <?php if(!empty($lista)): ?>
      
      
      <div id="tableObjeto">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableSelection">
      <thead>
          <tr>
            <?php if($podeAlterar): ?>
            <th width="7%" align="center">Editar</th>
            <?php endif; ?>
            <th width="40%" style="text-align:left;">Nome do grupo</th>
            <th width="23%" style="text-align:left;">Agência</th>
            <th width="23%" style="text-align:left;">Cliente</th>
            <th width="23%" style="text-align:left;">Status</th>
          </tr>
          </thead>
          <?php foreach($lista as $item): ?>
          <tr>
            <?php if($podeAlterar): ?>
            <td align="center"><a href="<?php echo site_url('grupo/form/' . $item['ID_GRUPO'] ); ?>"><img src="<?php echo base_url().THEME; ?>img/file_edit.png" alt="Editar" border="0" /></a></td>
            <?php endif; ?>
            <td><?php echo $item['DESC_GRUPO']; ?></td>
            <td><?php echo $item['DESC_AGENCIA']; ?></td>
            <td><?php echo $item['DESC_CLIENTE']; ?></td>
            <td><?=$item['STATUS_GRUPO'] == 1 ? 'Ativo' : 'Inativo';?></td>
          </tr>
          <?php endforeach; ?>
          
        </table>
        
    </div> <!-- Final de Table Objeto -->
            
            <span class="paginacao"><?php echo $paginacao; ?></span>
        <?php else: ?>
            Nenhum resultado encontrado
        <?php endif ;?>
    
</div> <!-- Final de Lista -->

</div> <!-- Final de Content Global -->

<?php $this->load->view("ROOT/layout/footer") ?>
