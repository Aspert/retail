<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
  <h1>Cadastro de Jobs</h1>
  <?php echo form_open('job/lista', array('id' => 'pesquisa_simples'));?>
  <table width="100%" class="Alignleft">
    <tr>
      <?php if($podeAlterar): ?>
      <td width="7%" rowspan="2" align="center"><a href="<?php echo site_url('job/form'); ?>"><img src="<?php echo base_url().THEME; ?>img/file_add.png" alt="Adicionar" width="31" height="31" border="0" /></a></td>
      <?php endif; ?>
      <td width="13%">Ag&ecirc;ncia</td>
      <td width="12%">Cliente</td>
      <td width="13%">Bandeira</td>
      <td width="13%">Campanha</td>
      <td width="15%">Job</td>
      <td width="14%">Itens por p&aacute;gina</td>
      <td width="13%" rowspan="2"><a class="button" href="javascript:" title="Pesquisar" onclick="$j(this).closest('form').submit()"><span>Pesquisar</span></a></td>
    </tr>
    <tr>
      <td><select name="ID_AGENCIA" id="ID_AGENCIA" class="comboPesquisa">
          <option value=""></option>
          <?php echo montaOptions($agencias,'ID_AGENCIA','DESC_AGENCIA', !empty($busca['ID_AGENCIA']) ? $busca['ID_AGENCIA'] : ''); ?>
        </select></td>
      <td><select name="ID_CLIENTE" id="ID_CLIENTE" class="comboPesquisa">
          <option value=""></option>
          <?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', !empty($busca['ID_CLIENTE']) ? $busca['ID_CLIENTE'] : ''); ?>
        </select></td>
      <td><select name="ID_PRODUTO" id="ID_PRODUTO" class="comboPesquisa">
          <option value=""></option>
          <?php echo montaOptions($produtos,'ID_PRODUTO','DESC_PRODUTO', !empty($busca['ID_PRODUTO']) ? $busca['ID_PRODUTO'] : ''); ?>
        </select></td>
      <td><select name="ID_CAMPANHA" id="ID_CAMPANHA" class="comboPesquisa">
          <option value=""></option>
          <?php echo montaOptions($campanhas,'ID_CAMPANHA','DESC_CAMPANHA', !empty($busca['ID_CAMPANHA']) ? $busca['ID_CAMPANHA'] : ''); ?>
        </select></td>
      <td><input class="comboPesquisa" type="text" name="TITULO_JOB" id="TITULO_JOB" value="<?php echo !empty($busca['TITULO_JOB']) ? $busca['TITULO_JOB'] : '' ?>" /></td>
      <td><span class="campo esq">
        <select name="pagina" id="pagina">
          <?php
		$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
		for($i=5; $i<=50; $i+=5){
			printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
		}
		?>
        </select>
        </span></td>
    </tr>
  </table>
  <?php echo form_close();?>
  <?php if( (isset($lista))&& (is_array($lista))):?>
  <div id="tableObjeto">
    <table border="0" width="100%" class="tableSelection">
      <thead>
        <tr>
          <?php if($podeAlterar): ?>
          <td width="7%">Editar</td>
          <?php endif; ?>
          <td width="18%" align="left">Ag&ecirc;ncia</td>
          <td width="17%" align="left">Cliente</td>
          <td width="17%" align="left">Bandeira</td>
          <td width="19%" align="left">Campanha</td>
          <td width="12%" align="left">Job</td>
          <td width="10%" align="left">Situa&ccedil;&atilde;o</td>
        </tr>
      </thead>
      <?php $cont=0; ?>
      <?php foreach($lista as $item):?>
      <tr>
        <?php if($podeAlterar): ?>
        <td align="center"><a href="<?php echo site_url('job/form/'.$item['ID_JOB']); ?>"><img src="<?php echo base_url().THEME; ?>img/file_edit.png" alt="Editar" width="31" height="31" border="0" /></a></td>
        <?php endif; ?>
        <td><?=$item['DESC_AGENCIA'];?></td>
        <td><?=$item['DESC_CLIENTE'];?></td>
        <td><?=$item['DESC_PRODUTO'];?></td>
        <td><?=$item['DESC_CAMPANHA'];?></td>
        <td><?=$item['TITULO_JOB'];?></td>
        <td><?=$item['STATUS_JOB'] == 1 ? 'Ativo' : 'Inativo';?></td>
        <?php $cont++; ?>
      </tr>
      <?endforeach;?>
    </table>
  </div>
  <span class="paginacao">
  <?=(isset($paginacao))?$paginacao:null?>
  </span>
  <?else:?>
  Nenhum resultado
  <?endif;?>
</div>
<script type="text/javascript">
$('ID_AGENCIA').addEvent('change', function(){
	clearSelect($('ID_CAMPANHA'), 1);
	clearSelect($('ID_PRODUTO'), 1);
	montaOptionsAjax($('ID_CLIENTE'),'<?php echo site_url('json/admin/getClientesByAgencia'); ?>','id=' + this.value,'ID_CLIENTE','DESC_CLIENTE');
});
$('ID_CLIENTE').addEvent('change', function(){
	clearSelect($('ID_CAMPANHA'), 1);
	montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByCliente'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
});
$('ID_PRODUTO').addEvent('change', function(){
	montaOptionsAjax($('ID_CAMPANHA'),'<?php echo site_url('json/admin/getCampanhasByProduto'); ?>','id=' + this.value,'ID_CAMPANHA','DESC_CAMPANHA');
});
</script>
<?php $this->load->view("ROOT/layout/footer") ?>
