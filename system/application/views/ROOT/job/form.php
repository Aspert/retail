<?php $this->load->view("ROOT/layout/header"); ?>

<div id="contentGlobal">
  <?php if ( isset($erros) && is_array($erros) ) : ?>
  <ul style="color:#ff0000;font-weight:bold;">
    <?php foreach ( $erros as $e ) : ?>
    <li>
      <?=$e?>
    </li>
    <?php endforeach; ?>
  </ul>
  <?php endif; ?>
  <?php echo form_open_multipart('job/save/'.$this->uri->segment(3));?>
  <h1>Cadastro Job</h1>
  <table width="100%" class="Alignleft">
    <tr>
      <td width="15%">Ag&ecirc;ncia *</td>
      <td width="85%"><?php if($this->uri->segment(3) == ''): ?>
        <select name="ID_AGENCIA" id="ID_AGENCIA">
          <option value=""></option>
          <?php echo montaOptions($agencias, 'ID_AGENCIA','DESC_AGENCIA', post('ID_AGENCIA',true)); ?>
        </select>
        <?php else: ?>
        <?php post('DESC_AGENCIA'); ?>
        <input name="DESC_AGENCIA" id="DESC_AGENCIA" type="hidden" value="<?php post('DESC_AGENCIA'); ?>" />
        <input name="ID_AGENCIA" id="ID_AGENCIA" type="hidden" value="<?php post('ID_AGENCIA'); ?>" />
        <br />
        <?php endif; ?></td>
    </tr>
    <tr>
      <td>Cliente  *</td>
      <td><?php if($this->uri->segment(3) == ''): ?>
        <select name="ID_CLIENTE" id="ID_CLIENTE">
          <option value=""></option>
          <?php echo montaOptions($clientes, 'ID_CLIENTE','DESC_CLIENTE', post('ID_CLIENTE',true)); ?>
        </select>
        <?php else: ?>
        <?php post('DESC_CLIENTE'); ?>
        <input name="DESC_CLIENTE" id="DESC_CLIENTE" type="hidden" value="<?php post('DESC_CLIENTE'); ?>" />
        <input name="ID_CLIENTE" id="ID_CLIENTE" type="hidden" value="<?php post('ID_CLIENTE'); ?>" />
        <br />
        <?php endif; ?></td>
    </tr>
    <tr>
      <td>Bandeira  *</td>
      <td><?php if($this->uri->segment(3) == ''): ?>
        <select name="ID_PRODUTO" id="ID_PRODUTO">
          <option value=""></option>
          <?php echo montaOptions($produtos, 'ID_PRODUTO','DESC_PRODUTO', post('ID_PRODUTO',true)); ?>
        </select>
        <?php else: ?>
        <?php post('DESC_PRODUTO'); ?>
        <input name="DESC_PRODUTO" id="DESC_PRODUTO" type="hidden" value="<?php post('DESC_PRODUTO'); ?>" />
        <input name="ID_PRODUTO" id="ID_PRODUTO" type="hidden" value="<?php post('ID_PRODUTO'); ?>" />
        <br />
        <?php endif; ?></td>
    </tr>
    <tr>
      <td>Campanha</td>
      <td><?php if($this->uri->segment(3) == ''): ?>
        <select name="ID_CAMPANHA" id="ID_CAMPANHA">
          <option value=""></option>
          <?php echo montaOptions($campanhas, 'ID_CAMPANHA','DESC_CAMPANHA', post('ID_CAMPANHA',true)); ?>
        </select>
        <?php else: ?>
        <?php post('DESC_CAMPANHA'); ?>
        <input name="DESC_CAMPANHA" id="DESC_CAMPANHA" type="hidden" value="<?php post('DESC_CAMPANHA'); ?>" />
        <input name="ID_CAMPANHA" id="ID_CAMPANHA" type="hidden" value="<?php post('ID_CAMPANHA'); ?>" />
        <br />
        <?php endif; ?></td>
    </tr>
    <tr>
      <td>Nome  *</td>
      <td><?php if($this->uri->segment(3) == ''): ?>
        <input name="TITULO_JOB" id="TITULO_JOB" type="text" value="<?php post('TITULO_JOB'); ?>" />
        <?php else: ?>
        <?php post('TITULO_JOB'); ?>
        <input name="TITULO_JOB" id="TITULO_JOB" type="hidden" value="<?php post('TITULO_JOB'); ?>" />
        <br />
        <?php endif; ?></td>
    </tr>
    <tr>
      <td>Tipo de pe&ccedil;a  *</td>
      <td><select name="ID_TIPO_PECA" id="ID_TIPO_PECA">
          <option value=""></option>
          <?php echo montaOptions($tipos, 'ID_TIPO_PECA','DESC_TIPO_PECA', post('ID_TIPO_PECA',true)); ?>
        </select></td>
    </tr>
    <tr>
      <td>Tipo de job *</td>
      <td><select name="ID_TIPO_JOB" id="ID_TIPO_JOB">
          <option value=""></option>
          <?php echo montaOptions($tiposJob, 'ID_TIPO_JOB','DESC_TIPO_JOB', post('ID_TIPO_JOB',true)); ?>
        </select></td>
    </tr>
    <tr>
      <td>Altura </td>
      <td><input name="ALTURA_JOB" id="ALTURA_JOB" type="text" maxlength="100" value="<?php post('ALTURA_JOB'); ?>" /></td>
    </tr>
    <tr>
      <td>Largura </td>
      <td><input name="LARGURA_JOB" id="LARGURA_JOB" type="text" maxlength="100" value="<?php post('LARGURA_JOB'); ?>" /></td>
    </tr>
    <tr>
      <td>Data de in&iacute;cio</td>
      <td><input name="DATA_INICIO" id="DATA_INICIO" type="text" maxlength="100" readonly="readonly" value="<?php post('DATA_INICIO'); ?>" class="_calendar" /></td>
    </tr>
    <tr>
      <td>Data de t&eacute;rmino</td>
      <td><input name="DATA_TERMINO" id="DATA_TERMINO" type="text" readonly="readonly" maxlength="100" value="<?php post('DATA_TERMINO'); ?>" class="_calendar" /></td>
    </tr>
    <tr>
      <td>N&uacute;mero de p&aacute;ginas *</td>
      <td><input name="PAGINAS_JOB" id="PAGINAS_JOB" type="text" maxlength="100" value="<?php post('PAGINAS_JOB'); ?>" /></td>
    </tr>
    <tr>
      <td>Situa&ccedil;&atilde;o</td>
      <td><select name="STATUS_JOB">
          <option value="1">ATIVO</option>
          <option value="0"<?php echo isset($_POST['STATUS_JOB']) && $_POST['STATUS_JOB'] == '0' ? ' selected="selected"' : ''; ?>>INATIVO</option>
        </select></td>
    </tr>
    <tr>
      <td>Enviar &iacute;cone</td>
      <td><input name="file" type="file" size="40" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><a href="<?php echo site_url('job/lista'); ?>" class="button"><span>Cancelar</span></a> <a href="javascript:;" onclick="$j(this).closest('form').submit()" class="button"><span>Salvar</span></a></td>
    </tr>
  </table>
  <?php echo form_close();?> </div>
<script type="text/javascript">
$j(function(){
	$('ID_AGENCIA').addEvent('change', function(){
		clearSelect($('ID_CAMPANHA'), 1);
		clearSelect($('ID_PRODUTO'), 1);
		montaOptionsAjax($('ID_CLIENTE'),'<?php echo site_url('json/admin/getClientesByAgencia'); ?>','id=' + this.value,'ID_CLIENTE','DESC_CLIENTE');
	});
	$('ID_CLIENTE').addEvent('change', function(){
		clearSelect($('ID_CAMPANHA'), 1);
		montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByCliente'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
	});
	$('ID_PRODUTO').addEvent('change', function(){
		montaOptionsAjax($('ID_CAMPANHA'),'<?php echo site_url('json/admin/getCampanhasByProduto'); ?>','id=' + this.value,'ID_CAMPANHA','DESC_CAMPANHA');
	});
	
	$j('#ALTURA_JOB,#LARGURA_JOB').maskMoney({showSymbol: false, thousands: ''});
	$j('._calendar').datepicker({dateFormat: 'dd/mm/yy',showOn: 'button', buttonImage: '<?= base_url().THEME ?>img/calendario.png', buttonImageOnly: true});
});
</script>
<?php $this->load->view("ROOT/layout/footer") ?>
