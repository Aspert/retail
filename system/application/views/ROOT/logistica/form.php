<?php $this->load->view("ROOT/layout/header") ?>
<?php
$cor = '';
?>

<div id="contentGlobal">
	<h1>Log&iacute;stica</h1>
	<div id="tabs">
		<!-- inicio abas -->
		<ul>
			<li><a href="#dados_job">Dados do Job</a></li>
			<?php if(!empty($praca)): ?>
			<li><a href="#tab-<?php echo $praca['ID_PRACA']; ?>"><?php echo $praca['DESC_PRACA']; ?></a></li>
			<?php endif; ?>
		</ul>
		<!-- fim abas -->
		<div id="dados_job">
			<!-- inicio aba dados do job -->
			<table width="100%" class="Alignleft">
				<tr>
					<td width="17%"><strong>Ag&ecirc;ncia</strong></td>
					<td width="83%"><?php echo $job['DESC_AGENCIA']; ?></td>
				</tr>
				<tr>
					<td><strong>Cliente</strong></td>
					<td><?php echo $job['DESC_CLIENTE']; ?></td>
				</tr>
				<tr>
					<td><strong>Bandeira</strong></td>
					<td><?php echo $job['DESC_PRODUTO']; ?></td>
				</tr>
				<tr>
					<td><strong>Campanha</strong></td>
					<td><?php echo $job['DESC_CAMPANHA']; ?></td>
				</tr>
				<tr>
					<td><strong>Tipo de pe&ccedil;a</strong></td>
					<td><?php echo $job['DESC_TIPO_PECA']; ?></td>
				</tr>
				<?php if(!$_sessao['IS_HERMES']){?>
				<tr>
					<td><strong>Tipo de Job</strong></td>
					<td><?php echo $job['DESC_TIPO_JOB']; ?></td>
				</tr>
				<?php }?>
				<tr>
					<td><strong>N&uacute;mero do Job</strong></td>
					<td><?php echo $job['NUMERO_JOB_EXCEL']; ?></td>
				</tr>
				<tr>
					<td><strong>T&iacute;tulo</strong></td>
					<td><?php echo $job['TITULO_JOB']; ?></td>
				</tr>
				<tr>
					<td><strong>Usu&aacute;rio Respons&aacute;vel</strong></td>
					<td><?php echo $job['NOME_USUARIO_RESPONSAVEL']; ?></td>
				</tr>
				<tr>
					<td><strong>N&uacute;mero de p&aacute;ginas</strong></td>
					<td><?php echo $job['PAGINAS_JOB']; ?></td>
				</tr>
				<?php if(!$_sessao['IS_HERMES']){?>
				<tr>
					<td><strong>Formato</strong></td>
					<td><?php echo centimetragem($job['LARGURA_JOB']), ' x ', centimetragem($job['ALTURA_JOB']); ?></td>
				</tr>
				<?php }?>
				<tr>
					<td><strong>In&iacute;cio  da Validade</strong></td>
					<td><?php echo format_date_to_form($job['DATA_INICIO']); ?></td>
				</tr>
				<tr>
					<td><strong>Final da Validade</strong></td>
					<td><?php echo format_date_to_form($job['DATA_TERMINO']); ?></td>
				</tr>
				<tr>
					<td valign="top"><strong>Observa&ccedil;&otilde;es</strong></td>
					<td><?php echo nl2br($job['OBSERVACOES_EXCEL']); ?></td>
				</tr>
				<tr>
					<td valign="top"><strong>Situa&ccedil;&atilde;o</strong></td>
					<td><?php echo $job['DESC_STATUS']; ?></td>
				</tr>
				<tr>
					<td valign="top"><strong>Etapa atual</strong></td>
					<td><?php echo $job['DESC_ETAPA']; ?></td>
				</tr>
				<tr>
					<td valign="top"><strong>Escolha a pra&ccedil;a</strong></td>
					<td><select name="idpraca" id="idpraca">
						<option value=""></option>
						<?php
						echo montaOptions($pracas,'ID_PRACA','DESC_PRACA');
						?>
					</select></td>
				</tr>
			</table>
			<!-- Final de Table Objeto -->
		</div>
		<!-- final aba dados do job -->
		<!-- inicio conteudo -->
		<?php if(!empty($praca)): ?>
		<div id="tab-<?php echo $praca['ID_PRACA']; ?>">
			<!-- TABS PRINCIPAIS POR PRACA -->
			<form method="post" action="" title="<?php echo htmlentities($praca['DESC_PRACA'], ENT_COMPAT, 'UTF-8'); ?>">
			
				<input type="hidden" name="apagados[]" id="apagados" value="" class="apagados">
				
				<table width="100%" border="0" cellspacing="1" cellpadding="2">
					<tr>
						<td width="15%">Nome da Ficha</td>
						<td width="85%" style="text-align:left"><input type="text" name="nome" id="nome" /></td>
					</tr>
					<tr>
						<td>C&oacute;digo de Barras</td>
						<td style="text-align:left"><input type="text" name="codigo" id="codigo" />
						<input type="button" name="btnFiltrar" id="btnFiltrar" value="Filtrar" /></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td style="text-align:left">&nbsp;</td>
					</tr>
				</table>

				
				<div class="carregando_<?php echo $praca['ID_PRACA']; ?>" style="padding:20px;">Carregando...</div>
				<div id="tableObjeto">
					<table border="0" cellspacing="0" cellpadding="0" class="tableSelection" id="logistica-<?php echo $praca['ID_PRACA']; ?>" width="100%">
						<thead>
							<tr>
								<th width="10%">Ficha</th>
								<th width="10%">Subfichas</th>
								<th width="20%"><?php ghDigitalReplace($_sessao, 'Categoria'); ?></th>
								<th width="20%"><?php ghDigitalReplace($_sessao, 'Subcategoria'); ?></th>
								<th width="20%" class="grupo4">Observa&ccedil;&otilde;es</th>
								<th width="10%" class="grupo4">Dispon&iacute;vel?</th>
								<th width=10%" class="grupo4">Excluir</th>
							</tr>
						</thead>
					</table>
				</div>
				<!-- Final de Content Table Objeto -->
				<div style="border:1px solid #D3D3D3; background-color:#EDECEC; padding:10px; float:left; width:1070px; margin:10px 0 0 -1px;"> Copiar valores de
					<select name="copiar_<?php echo $praca['ID_PRACA']; ?>" id="copiar_<?php echo $praca['ID_PRACA']; ?>" class="copiar">
						<option value=""></option>
						<?php
						echo montaOptions($pracas,'ID_PRACA','DESC_PRACA', post('idpraca',true));
						?>
					</select>
				</div>
			</form>
			<div id="both" style="clear: both"></div>
		</div>
		<!-- FINAL TABS PRINCIPAIS PRACA -->
		<?php endif; ?>
		<!-- fim conteudo -->
	</div>
	<div style="margin: 10px 0 0 0; float:left;">
		<a class="button" href="javascript:;" onclick="salvar()"><span>Salvar</span></a>
		<a class="button" href="<?php echo site_url('logistica/index'); ?>"><span>Cancelar</span></a>
		<?php if($podePassarEtapa && $this->uri->segment(3) != ''): ?>
		<a class="button" href="javascript:;" onclick="proximaEtapa()"><span>Pr&oacute;xima Etapa</span></a>
		<?php endif; ?>
		<div class="both"></div>
	</div>
	<!-- Final de Content Bot�es -->
	<script type="text/javascript">



var preloaders        = {};
var url               = '<?php echo site_url('json/fluxo/get_itens_logistica'); ?>';
var urlCopia          = '<?php echo site_url('json/fluxo/get_itens_logistica_copia'); ?>';
var urlSalvarPagina   = '<?php echo site_url('json/fluxo/salvar_itens_logistica'); ?>';
var idjob             = '<?php printf('%d', $this->uri->segment(3)); ?>';
var pracaAtual        = <?php printf('%d', $this->uri->segment(4)); ?>;
var urlJob            = '<?php echo site_url('logistica/form'); ?>/' + idjob;
var urlSalvarApagados = '<?php echo site_url("json/fluxo/salvar_itens_logistica_apagados"); ?>';
var loader            = null;
var limit             = 70;
var apagadoHidden;

function teste(){
	$j('form').attr('action', '<?php echo site_url("json/fluxo/salvar_itens_logistica_apagados"); ?>').submit();
}

//REMOVE UM ITEM DA PAGINA QNDO CLICA NO BOTAO REMOVER
function deletaItem(obj, idItem){
	$j(obj).closest('tr').remove();
	$j('.apa_' + idItem).closest('tr').remove();
	
	if(apagadoHidden == undefined){
		apagadoHidden = $j('form').find('#apagados').clone();
		$j('form').find('#apagados').remove()
		apagadoHidden.removeAttr('id');
	}

	var clone = apagadoHidden.clone();
	clone.val(idItem);
	$j('form').append(clone);
}

//FUNCAO PARA DELETAR ITENS Q O USUARIO JA TENHA DELETADO, POR EXEMPLO, ELE APAGA UM ITEM NA PAGINA UM
//VAI PRA PAGINA DOIS E VOLTA PRA PAGINA UM, DAI O ELEMENTO NAO APARRECE NA LISTA, MAIS SOH EH REALMNETE
//QUANDO CLICA NO BOTAO SALVAR
function deletaItensPagina(){
	$j('form').find('.apagados').each(
			function(){
				if($j('#tr' + this.value).size() > 0){
					$j('#tr' + this.value).remove();
					$j('#apa_' + this.value).remove();
				}
			}
		);
}

function inicializar($){
	$j(".fancy").fancybox();
	$j('#idpraca').change(function(){
		if( this.value != '' ){
			location.href = '<?php echo site_url('logistica/form/'.$this->uri->segment(3)); ?>/' + this.value;
		}
	});
	$('#tabs').tabs();
	
	if( pracaAtual > 0 ){
		$('#tabs').tabs('select',1);
		configPreloader();
		doPesquisa(0);
	}
	
	$j('#btnFiltrar').click(btnFiltrarHandler);
	$j('.copiar').change(copiarItensHandler);
}

function copiarItensHandler(){
	if( this.value == '' ){
		return;
	}
	
	$j(window).scrollTop(0);
	displayStaticMessagewithparameter( 'Efetuando c&oacute;pia. Por favor aguarde.', 300, 100 );
	getDivModal().dialog('open');
	
	var data = $j('form').serialize();

	data += '&idpraca='+this.value;
	data += '&idjob='+idjob;

	$j.post(urlCopia, data, function(result){
		var json = eval('['+result+']')[0];
		var idx=1;
		var total = json.length;
		
		function copiaValores(){
			if( json.length > 0 ){
				getDivModal().html('Copiando valores #'+(idx++)+' de '+total);
				var item = json.shift();
				if(item.ID_FICHA_PAI > 0){
					$j('#disponivel-' + pracaAtual + '-' + item.ID_FICHA_PAI + '-' + item.ID_FICHA).attr('checked', item.DISPONIVEL_ITEM == 1);
					$j('#observacoes-' + pracaAtual + '-' + item.ID_FICHA_PAI + '-' + item.ID_FICHA).val(item.OBSERVACOES_ITEM);
					marcaDisponivel($j('#disponivel-' + pracaAtual + '-' + item.ID_FICHA_PAI + '-' + item.ID_FICHA));
				}
				else{
					$j('#disponivel-' + pracaAtual + '-' + item.ID_FICHA).attr('checked', item.DISPONIVEL_ITEM == 1);
					$j('#observacoes-' + pracaAtual + '-' + item.ID_FICHA).val(item.OBSERVACOES_ITEM);
					marcaDisponivel($j('#disponivel-' + pracaAtual + '-' + item.ID_FICHA));
				}
				
				setTimeout(copiaValores, 5);
			} else {
				closeMessage();
			}
		}
		
		copiaValores();
		
	});
}
 
function btnFiltrarHandler(){
	doPesquisa(0);
}

function configPreloader(){
	loader = new StackLoader(url,{});
	loader.params.idjob   = idjob;
	loader.params.idpraca = pracaAtual;
	loader.onCompleteBlock = onCompleteBlockHandler;
	loader.onComplete = onCompleteHandler;
	loader.onStart = onStartHandler;
	loader.limit = limit;
	loader.firstContainer = '#logistica-' + pracaAtual;
	loader.defaultContainer = loader.firstContainer;
}

function doPesquisa(pg){
	var data = $j('form').serialize();
	
	salvarPaginaAtual(function(){
		$j('.itemPaginacao').css('font-weight','normal');
		$j('#logistica-' + pracaAtual).find('tbody:first').remove();
		
		loader.params.nome = $j('#nome').val();
		loader.params.codigo = $j('#codigo').val();
		
		loader.load(pg*loader.limit, loader.limit);
	});
}

function onStartHandler(loader){
	$j('.carregando_'+loader.params.idpraca).show();
	return true;
}

function onCompleteBlockHandler(loader, container, html){
	container.find('tbody:last .cb_disponivel').click(marcaDisponivelHandler).each(function(){
		marcaDisponivel(this);
	});
	container.find('tbody:last .fancy').fancybox();

	return true;
}

function onCompleteHandler(loader){
	$j('.carregando_'+loader.params.idpraca).hide();
}

function marcaDisponivelHandler(){
	marcaDisponivel(this);
}

function marcaDisponivel(target){
	if( $j(target).attr('checked') == "checked" ){
		$j(target).closest('tr').css('background-color', '#FFFFFF');
		$j(target).parent().find('.valor_item').val(1);
	}
	else{
		$j(target).closest('tr').css('background-color', '#FFCC00');
		$j(target).parent().find('.valor_item').val(0);
	}
}

function salvarPaginaAtual(callback){
	$j(window).scrollTop(0);
	
	var data = $j('form').serialize();
	
	if( data != '' ){
		getDivModal().remove();
		displayStaticMessagewithparameter('Gravando dados',300,100);
		
		$j.post(urlSalvarPagina, data, function(){
			closeMessage();
			
			if( callback ){
				callback.call(null);
			}
		});
	} else {
		if( callback ){
			callback.call(null);
		}
	}
}

function salvarApagados(callback){
	$j(window).scrollTop(0);
	
	var data = $j('form').serialize();
	data += '&idjob='+idjob;
	
	if( data != '' ){
		getDivModal().remove();
		displayStaticMessagewithparameter('Gravando dados',300,100);
		
		$j.post(urlSalvarApagados, data, function(){
			closeMessage();
			
			if( callback ){
				callback.call(null);
			}
		});
	} else {
		if( callback ){
			callback.call(null);
		}
	}
}

function salvar(){
	salvarPaginaAtual(function(){
		location.href = urlJob;
	});

	salvarApagados(function(){
		location.href = urlJob;
	});
}

$j(inicializar);

<?php if($podePassarEtapa && $this->uri->segment(3) != ''): ?>
function proximaEtapa(){
	var pass = true;
	pass = confirm('Caso hajam itens indisponíveis, o job não mudará de etapa. Deseja continuar?');
	
	if(pass){
		salvarPaginaAtual(function(){
			location.href = '<?php echo site_url('logistica/proxima_etapa/'.$this->uri->segment(3)); ?>';
		});

		salvarApagados(function(){
			location.href = '<?php echo site_url('logistica/proxima_etapa/'.$this->uri->segment(3)); ?>';
		});
	}
}
<?php endif; ?>

$j(function(){
	<? if(!empty($erros)): ?>
		alert("<? echo implode('\n',$erros); ?>");
	<? endif;?>
});
	
</script>
</div>
<!-- Final de Content Global -->
<?php $this->load->view("ROOT/layout/footer") ?>
