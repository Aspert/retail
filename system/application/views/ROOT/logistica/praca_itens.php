<?php if(empty($itensPai)) exit; ?>
<tbody>
<?php
		$idCenario = 0;
		
		foreach($itensPai as $item):		
			$classPraca = 'praca_' . $praca['ID_PRACA'];
			$i = $item['ID_FICHA'];
			
			if( ($idCenario != $item['ID_CENARIO']) && ($item['ID_CENARIO'] > 0) ){
				$idCenario = $item['ID_CENARIO'];
?>
				<tr style="background-color:<?php echo $item['COR_CENARIO'] ?>;">
					<td colspan="7" style="">
						<?php echo "<strong>" . $item['NOME_CENARIO'] . "</strong> (" .$item['COD_CENARIO']. ")" ;?>
					</td>
				</tr>
<?php 
			}
?>
			<tr id="tr<?php echo $item['ID_EXCEL_ITEM']; ?>">
				<td style="<?php if( $item['ID_CENARIO'] > 0){ echo 'padding: 0px;'; } ?>">
					<?php if( $item['ID_CENARIO'] > 0): ?>
						<div style="background-color:<?php echo $item['COR_CENARIO']; ?>;height:90px;float:left;width:10px;">&nbsp;</div>
					<?php endif; ?>
					<?php if(!empty($item['FILE_ID'])): ?>
					<a href="<?php echo base_url(); ?>img.php?img_id=<?php echo $item['FILE_ID']; ?>&img_size=big&a.jpg" class="fancy"><img src="<?php echo base_url(); ?>img.php?img_id=<?php echo $item['FILE_ID']; ?>" height="50" style="<?php if( $item['ID_CENARIO'] > 0){ echo 'margin-top:5px;'; } ?>" /></a> <br />
					<?php endif; ?>
					<?php echo $item['COD_BARRAS_FICHA']; ?> <br />
					<?php echo substr($item['NOME_FICHA'],0,60); ?>
				</td>
				<td align="center">
					<a class="exibe_filhas_<?php echo $item['ID_FICHA']; ?>" style="display:none;" id="exibe_filhas_<?php echo $item['ID_FICHA']; ?>" href="javascript:new Util().vazio();" title="Exibir Filhas" onclick="$j('.modelo_ficha_pauta_filha_<?php echo $item['ID_FICHA']; ?>').toggle();toggleImgFilha($j(this).closest('td'));">
						<img src="<?= base_url().THEME ?>img/open_filhas.png"  border="0" alt="Exibir Filhas"/>
					</a>
				</td>
				<td align="center"><?php echo $item['DESC_CATEGORIA']; ?></td>
				<td align="center"><?php echo $item['DESC_SUBCATEGORIA']; ?></td>
				<td align="center" class="grupo4">
					<input class="itemPricing <?php echo $classPraca; ?>" type="text" name="item[<?php echo $item['ID_EXCEL_ITEM']; ?>][OBSERVACOES_ITEM]" id="<?php printf('observacoes-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $item['OBSERVACOES_ITEM']; ?>" size="35"/>
				</td>
				<td align="center" class="grupo4">
					<input type="checkbox" onclick="selecionaFichasFilhas('<?= $praca['ID_PRACA'] ?>', '<?= $item['ID_FICHA'] ?>', this.checked);" name="<?php printf('cb_disponivel[%d][%d]',$praca['ID_PRACA'],$item['ID_EXCEL_ITEM']); ?>" id="<?php printf('disponivel-%d-%d',$praca['ID_PRACA'] ,$i); ?>" value="1" <?php echo $item['DISPONIVEL_ITEM']==1 ? 'checked="checked"' : ''; ?> class="cb_disponivel praca_<?php echo $praca['ID_PRACA']; ?>_pai_<?php echo $item['ID_FICHA']; ?>" />
					<input type="hidden" name="<?php printf('item[%d][DISPONIVEL_ITEM]', $item['ID_EXCEL_ITEM']); ?>" id="<?php printf('item_%d_DISPONIVEL', $item['ID_EXCEL_ITEM']); ?>" value="<?php echo $item['DISPONIVEL_ITEM']; ?>" class="valor_item" />
					<input type="hidden" name="codigos[]" value="<?php echo $item['ID_FICHA']; ?>" />
				</td>
				<td>
					<a href="javascript:void(0);" class="remover"> 
						<img src="<?php echo base_url().THEME; ?>img/trash.png" alt="Remover Ficha"  title="Remover Ficha" border="0" onClick="deletaItem(this, <?php echo $item['ID_EXCEL_ITEM']; ?>, <?php echo $item['ID_FICHA']; ?>);"/>
					</a>
				</td>
			</tr>
			<?php foreach($itensFilha as $itemFilha): ?>
				<?php if($itemFilha['ID_FICHA_PAI'] == $item['ID_FICHA']): ?>
					<tr class="modelo_ficha_pauta_filha_<?php echo $item['ID_FICHA']; ?>" id="tr<?php echo $itemFilha['ID_EXCEL_ITEM']; ?>" style="display:none;">
						<input type="hidden" class="apa_<?php echo $item['ID_EXCEL_ITEM']; ?>">
						<td align="center" class="modelo_ficha_pauta_filha">
							<img src="<?= base_url().THEME ?>img/seta_filha.png" />
						</td>
						<td align="center" class="modelo_ficha_pauta_filha">
							<?php if(!empty($itemFilha['FILE_ID'])): ?>
							<a href="<?php echo base_url(); ?>img.php?img_id=<?php echo $itemFilha['FILE_ID']; ?>&img_size=big&a.jpg" class="fancy">
							<img src="<?php echo base_url(); ?>img.php?img_id=<?php echo $itemFilha['FILE_ID']; ?>" height="50" />
							</a> 
							<br />
							<?php endif; ?>
							<?php echo $itemFilha['COD_BARRAS_FICHA']; ?> 
							<!-- <br />
							<?php echo substr($itemFilha['NOME_FICHA'],0,60); ?>
							<br />-->
						</td>
						<td align="center" class="modelo_ficha_pauta_filha"><?php echo $itemFilha['DESC_CATEGORIA']; ?></td>
						<td align="center" class="modelo_ficha_pauta_filha"><?php echo $itemFilha['DESC_SUBCATEGORIA']; ?></td>
						<td align="center" class="modelo_ficha_pauta_filha"><input class="itemPricing <?php echo $classPraca; ?>" type="text" name="item[<?php echo $itemFilha['ID_EXCEL_ITEM']; ?>][OBSERVACOES_ITEM]" id="<?php printf('observacoes-%d-%d-%d', $praca['ID_PRACA'], $i, $itemFilha['ID_FICHA']); ?>" value="<?php echo $itemFilha['OBSERVACOES_ITEM']; ?>" size="35"/></td>
						<td align="center" class="modelo_ficha_pauta_filha">
							<input type="checkbox" onClick="return selecionaFilhaSemPai(<?php echo $item['ID_PRACA']; ?>, <?php echo $item['ID_FICHA']; ?>, this.checked);" name="<?php printf('cb_disponivel[%d][%d]',$praca['ID_PRACA'],$itemFilha['ID_EXCEL_ITEM']); ?>" id="<?php printf('disponivel-%d-%d-%d',$praca['ID_PRACA'] ,$i, $itemFilha['ID_FICHA']); ?>" value="1" <?php echo $itemFilha['DISPONIVEL_ITEM']==1 ? 'checked="checked"' : ''; ?> class="cb_disponivel praca_<?php echo $praca['ID_PRACA']; ?>_pai_<?php echo $item['ID_FICHA']; ?> filha_<?php echo $itemFilha['ID_FICHA']; ?>" />
							<input type="hidden" name="<?php printf('item[%d][DISPONIVEL_ITEM]', $itemFilha['ID_EXCEL_ITEM']); ?>" id="<?php printf('item_%d_DISPONIVEL', $itemFilha['ID_EXCEL_ITEM']); ?>" value="<?php echo $itemFilha['DISPONIVEL_ITEM']; ?>" class="valor_item" />
							<input type="hidden" name="codigos[]" value="<?php echo $itemFilha['ID_FICHA']; ?>" />
						</td>
						<td class="modelo_ficha_pauta_filha">
							<a href="javascript:void(0);" class="remover"> 
								<img src="<?php echo base_url().THEME; ?>img/trash.png" alt="Remover Ficha"  title="Remover Ficha" border="0" onClick="deletaItem(this, <?php echo $itemFilha['ID_EXCEL_ITEM']; ?>, <?php echo $itemFilha['ID_FICHA']; ?>);"/>
							</a>
						</td>
					</tr>

					<script type="text/javascript">
						$j('#exibe_filhas_<?php echo $item["ID_FICHA"]; ?>').show();
					</script>
				<?php endif; ?>
			<?php endforeach; ?>
	<?php endforeach; ?>
	<tr>
		<td colspan="7">
			P&aacute;ginas: | <?php for($i=0; $i<$total/$limit; $i++): ?>
			<a id="pg<?php echo $i; ?>" class="itemPaginacao" href="#" onclick="doPesquisa(<?php echo $i; ?>); return false;">
				<?php echo $i*$limit==$offset ? '<strong>' .($i+1) . '</strong>' : $i+1; ?>
			</a> |
			<?php endfor; ?>
		</td>
	</tr>
</tbody>


<script type="text/javascript">

function selecionaFichasFilhas(idPraca, idFichaPai, check){
	if(check){
		$j('.praca_' + idPraca + '_pai_' + idFichaPai).attr('checked', check);
	}
	else{
		$j('.praca_' + idPraca + '_pai_' + idFichaPai).attr('checked', check);
	}
}

function selecionaFilhaSemPai(idPraca, idFichaPai, check){
	if(check){
		$j('.praca_' + idPraca + '_pai_' + idFichaPai).attr('checked', check);
	}
	return true;
}

function removeFicha(obj, idFicha){
	$j(obj).closest('tr').remove();
	$j('.modelo_ficha_pauta_filha_' + idFicha).closest('tr').remove();
}

deletaItensPagina();
	
</script>