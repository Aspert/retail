<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Detalhes do Processo</title>
</head>
<style type="text/css">
<!-- 

#etapas td, #etapas th {
	text-align: left;
}

-->
</style>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabela_padrao">
	<tr>
		<td width="17%"><strong>Cliente</strong></td>
		<td width="83%"><?php echo $dados['DESC_CLIENTE']; ?></td>
	</tr>
	<tr>
		<td><strong>Tipo de Peça</strong></td>
		<td><?php echo $dados['DESC_TIPO_PECA']; ?></td>
	</tr>
	<tr>
		<td><strong>Situação</strong></td>
		<td><?php echo $dados['STATUS_PROCESSO'] == 1 ? 'Ativo' : 'Inativo'; ?></td>
	</tr>
</table>
<br />
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabela_padrao" id="etapas">
	<thead>
		<tr>
			<th width="40%">Descrição da etapa</th>
			<th width="60%">Quantidade de horas </th>
		</tr>
	</thead>
	<?php
	foreach($etapas as $etapa):
	?>
	<tr>
		<td><?php echo $etapa['DESC_ETAPA']; ?></td>
		<td><?php echo $etapa['QTD_HORAS']; ?></td>
	</tr>
	<?php
	endforeach;
	?>
</table>
<p>&nbsp; </p>
</body>
</html>
