<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
	<h1>Cadastro de Processos</h1>
	
	<?php
	if( !empty($msg) ){
		echo '<div class="box_erros">'.$msg.'</div>';
	}
	?>
	
	<form method="post" action="<?php echo site_url('processos/lista'); ?>" id="pesquisa_simples">
		<table width="100%" class="Alignleft">
			<tr>
				<td width="7%" rowspan="2" align="center"><?php if($podeAlterar): ?>
					<a href="<?php echo site_url('processos/form'); ?>"><img src="<?php echo base_url().THEME; ?>img/file_add.png" alt="Novo processo" title="Novo processo" width="31" height="31" border="0" /></a>
					<?php endif; ?></td>
				<td width="22%">Cliente</td>
				<td width="30%">Tipo de Pe&ccedil;a</td>
				<td width="29%">Itens / p&aacute;gina</td>
				<td width="12%" rowspan="2"><a class="button" href="javascript:" title="Pesquisar" onclick="$j(this).closest('form').submit()"><span>Pesquisar</span></a></td>
			</tr>
			<tr>
				<td>
				<?php
				if( !empty($_sessao['ID_CLIENTE']) ){
					echo sessao_hidden('ID_CLIENTE','DESC_CLIENTE');
				} else {
					echo '<select name="ID_CLIENTE" id="ID_CLIENTE">
						<option value=""></option>
						' . montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE',post('ID_CLIENTE',true)) . '
					</select>';
				}
				?>
				</td>
				<td><select name="ID_TIPO_PECA" id="ID_TIPO_PECA">
				<option value=""></option>
				<?php echo montaOptions($tiposPeca, 'ID_TIPO_PECA','DESC_TIPO_PECA',''); ?>
				</select></td>
				<td><span class="campo esq">
					<select name="pagina" id="pagina">
						<?php
						$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
						for($i=5; $i<=50; $i+=5){
							printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
						}
						?>
					</select>
					</span></td>
			</tr>
		</table>
	</form>
	<?php if( !empty($lista)) :?>
	<br />
	<div id="tableObjeto">
		<table cellspacing="1" cellpadding="2" border="0" width="100%" class="tableSelection">
			<thead>
				<tr>
					<?php if($podeAlterar): ?>
					<td width="2%"><strong>Editar</strong></td>
					<?php endif; ?>
					<td width="2%"><strong>Visualizar</strong></td>
					<td width="30%"><strong>Cliente</strong></td>
					<td width="33%"><strong>Tipo de Pe&ccedil;a</strong></td>
					<td width="17%"><strong>Data de cadastro</strong></td>
					<td width="16%"><strong>Situa&ccedil;&atilde;o</strong></td>
				</tr>
			</thead>
			<?php foreach($lista as $item): ?>
			<tr>
				<?php if($podeAlterar): ?>
				<td align="center">
					<?php
					if( $item['QTD_JOB'] == 0 || $item['STATUS_PROCESSO'] == 1){
						printf('<a href="%s">
									<img src="%simg/file_edit.png" alt="Editar" title="Editar" width="31" height="31" border="0" />
								</a>'
								, site_url('processos/form/' . $item['ID_PROCESSO'])
								, base_url() . THEME
						);
					} else {
						printf('<img class="inativo" src="%1$simg/file_edit.png" alt="%2$s" title="%2$s" width="31" height="31" border="0" />'
								, base_url() . THEME
								, 'Este processo n&atilde;o pode ser editado por haver jobs em andamento utilizando-o'
						);
					}
					
					?>
					</td>
				<?php endif; ?>
				<td align="center">
					<a href="#" onclick="visualizarProcesso(<?php echo $item['ID_PROCESSO']; ?>)">
						<img src="<?php echo base_url().THEME; ?>img/visualizar.png" align="Visualizar" title="Visualizar" />
					</a>
				</td>
				<td align="center"><?php echo $item['DESC_CLIENTE']; ?></td>
				<td align="center"><?php echo $item['DESC_TIPO_PECA']; ?></td>
				<td align="center"><?php echo format_date($item['DATA_CADASTRO'],'d/m/Y'); ?></td>
				<td align="center">
					<?php
						$label = '<img src="'.base_url().THEME.'img/accept.png" ';
						
						if( $item['STATUS_PROCESSO'] == 1 ){
							$label .= ' title="Ativo" alt="Ativo" />';
							
						} else {
							$label .= ' title="Inativo" alt="Inativo" class="inativo" />';
							
						}
							
						if( $podeAlterarStatus ){
							printf('<a href="%s">%s</a>'
								, site_url('processos/alterar_status/' . $item['ID_PROCESSO'])
								, $label
							);
						} else {
							echo $label;
						}
					?>
				</td>
			</tr>
			<?php endforeach; ?>
		</table>
	</div>
	<span class="paginacao"><?php echo $paginacao; ?></span>
	<?php else: ?>
	N&atilde;o existem informa&ccedil;&otilde;es para a pesquisa solicitada
	<?php endif; ?>
</div>

<script type="text/javascript">
$j('#ID_CLIENTE').change(function(){
	clearSelect('ID_TIPO_PECA',1);
	montaOptionsAjax($('ID_TIPO_PECA'), util.options.site_url + 'json/admin/getTipoPecaCliente','id=' + this.value,'ID_TIPO_PECA','DESC_TIPO_PECA');
});
</script>

<?php $this->load->view("ROOT/layout/footer") ?>
