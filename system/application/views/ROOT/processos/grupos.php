<div class="listagem_usuarios grupo_<?php echo $grupo['ID_GRUPO']; ?>">
	<fieldset>
		<legend>
			<?php
				echo $grupo['DESC_GRUPO'] . 
					( empty($grupo['DESC_AGENCIA']) ? '' : ' - ' . $grupo['DESC_AGENCIA']) .
					( empty($grupo['DESC_CLIENTE']) ? '' : ' - ' . $grupo['DESC_CLIENTE']);
			?>
		</legend>
		<div class="lista">
			<?php foreach($usuarios as $item): ?>
				<input id="usuario<?php echo $item['ID_USUARIO']; ?>" name="usuarios[<?php echo $id_regra; ?>][]" type="checkbox" value="<?php echo $item['ID_USUARIO']; ?>" <?php echo $item['SELECIONADO'] == 1 ? 'checked="checked"' : ''; ?> />
				<label for="usuario<?php echo $item['ID_USUARIO']; ?>"><?php echo $item['NOME_USUARIO']; ?></label><br />
			<?php endforeach; ?>
		</div>
		<div>
			<input type="button" value="Selecionar Todos" onclick="selecionarTodos(this)" />
			<input type="button" value="Selecionar nenhum" onclick="selecionarNenhum(this)" />
			<input type="button" value="Remover Grupo" onclick="removerGrupo(this)" />
		</div>
	</fieldset>
</div>