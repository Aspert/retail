﻿<?php $this->load->view("ROOT/layout/header"); ?>

<div id="contentGlobal"> <?php echo form_open('processos/save/'.$this->uri->segment(3));?>
	<h1>Cadastro de Processos</h1>
	<?php if ( !empty($erros) ) : ?>
	<ul style="color:#f00;font-weight:bold;" class="box_erros">
		<?php foreach ( $erros as $e ) : ?>
		<li>
			<?=$e?>
		</li>
		<?php endforeach; ?>
	</ul>
	<?php endif; ?>
	<table width="100%" class="Alignleft">
		<tr>
			<td width="20%">Cliente *</td>
			<td width="80%">
			<?php
			if( !empty($_sessao['ID_CLIENTE']) ){
				echo sessao_hidden('ID_CLIENTE','DESC_CLIENTE');
			} else {
				echo '<select name="ID_CLIENTE" id="ID_CLIENTE">
					<option value=""></option>
					' . montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE',post('ID_CLIENTE',true)) . '
				</select>';
			}
			?>
			</td>
		</tr>
		<tr>
			<td>Tipo de pe&ccedil;a  *</td>
			<td><select name="ID_TIPO_PECA" id="ID_TIPO_PECA">
				<option value=""></option>
					<?php echo montaOptions($tipos,'ID_TIPO_PECA','DESC_TIPO_PECA',post('ID_TIPO_PECA',true)); ?>
				</select></td>
		</tr>
		<tr>
			<td>Hora Início Elaboração *</td>
			<td>
				<input id="HORA_INICIO" name="HORA_INICIO" value="<?php post('HORA_INICIO'); ?>" size="10" />
			</td>
		</tr>
		<tr>
			<td>Hora Fim Elaboração *</td>
			<td>
				<input id="HORA_FINAL" name="HORA_FINAL" value="<?php post('HORA_FINAL'); ?>" size="10" />
			</td>
		</tr>
		<tr>
			<td>Considera Apenas dias uteis</td>
			<td>
				<input type="checkbox" name="CONSIDERAR_DIAS_UTEIS" id="CONSIDERAR_DIAS_UTEIS" value="S" <?php if(post('CONSIDERAR_DIAS_UTEIS', true) == 'S'){ echo "checked='checked'"; }; ?>>
			</td>
		</tr>
		<tr>
			<td>Considera Feriado</td>
			<td>
				<input type="checkbox" name="CONSIDERAR_FERIADO" id="CONSIDERAR_FERIADO" value="S" <?php if(post('CONSIDERAR_FERIADO', true) == 'S'){ echo "checked='checked'"; }; ?>>
			</td>
		</tr>
		<tr>
			<td>Situa&ccedil;&atilde;o *</td>
			<td><select name="STATUS_PROCESSO" id="STATUS_PROCESSO">
					<option value="1">Ativo</option>
					<option value="0"<?php echo post('STATUS_PROCESSO',true)=='0' ? ' selected="selected"' : ''; ?>>Inativo</option>
				</select></td>
		</tr>
		<tr>
			<td colspan="2">
			&nbsp;
			</td>
		</tr>
		<tr>
			<td>Etapa</td>
			<td><select name="ETAPA" id="ETAPA">
					<option value=""></option>
					<?php echo montaOptions($etapas,'ID_ETAPA','DESC_ETAPA',''); ?>
				</select>
				<a href="#" onclick="adicionarEtapaHandler(); return false;"> <img src="<?php echo base_url().THEME.'img/add.png'; ?>" alt="Adiciona uma etapa ao processo" class="information" /> </a></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">
			
				<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabela_padrao">
					<thead>
						<tr>
							<th width="5%">Ordenar</th>
							<th width="35%">Descri&ccedil;&atilde;o da etapa</th>
							<th width="30%">Quantidade de horas <img src="<?php echo base_url().THEME.'img/information.png' ?>" alt="Campo para preenchimento da quantidade de horas em que o job ficará na etapa" class="information" /> </th>
							<th width="20%">Status</th>
							<th width="5%">Excluir</th>
							<?php if($idProcesso!=0) {?>
							<th width="5%">Regra</th>
							<?php } ?>
						</tr>
					</thead>
					<tbody>
						<tr id="modelo">
							<td>
								<a href="#" class="ordenar" title="Ordenar" onclick="return false;"><img src="<?php echo base_url().THEME.'img/move_icon.png'; ?>" /></a>
							</td>
							<td>
								{DESC_ETAPA}
								<input name="etapas[]" type="hidden" id="etapas" value="{ID_ETAPA}" />
								<input name="descricoes[]" type="hidden" id="descricoes" value="{DESC_ETAPA}" />
								<?php if($idProcesso!=0) {?>
								<input name="id_processo_etapas[]" type="hidden" id="id_processo_etapas" value="{ID_PROCESSO_ETAPA}" />
								<?php } ?>
							</td>
							<td>
								<input name="qtd_horas[]" type="text" class="qtd_horas" id="qtd_horas" maxlength="5" size="10" value="{QTD_HORAS}" />
							</td>
							<td>
								<select name="id_status[]" class="cb_status">
									<?php foreach($status as $s): ?>
										<option value="<?php echo $s['ID_STATUS'] ?>"><?php echo $s['DESC_STATUS'] ?></option>
									<?php endforeach; ?>
								</select>
							</td>
							<td>
								<a href="#" title="Excluir" onclick="excluirEtapa(this, '{ID_PROCESSO_ETAPA}')"><img src="<?php echo base_url().THEME.'img/file_delete.png'; ?>" /></a>
							</td>
							<?php if($idProcesso!=0) {?>
							<td>
								<a href="#" title="Configurar Email" onclick="displayMessage('<?php echo(base_url()); ?>index.php/processos/modal_regra_email/{ID_PROCESSO_ETAPA}')"><img src="<?php echo base_url().THEME.'img/email.png'; ?>" /></a>
							</td>
							<?php } ?>
						</tr>
					</tbody>
				</table>
				
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><a href="<?php echo site_url('processos/lista'); ?>" class="button"><span>Cancelar</span></a> <a href="javascript:document.forms[0].submit();" class="button"><span>Salvar</span></a></td>
		</tr>
	</table>
	
	<?php echo form_close();?></div>
<script type="text/javascript">

var modelo = null;
var container = null;

function excluirEtapa( obj, id ){
	<?php if( $idProcesso != 0 ){ ?>
		$j.post(util.options.site_url + 'json/admin/excluirEtapa/'+id, null, function( data ){
			$j(obj).closest('tr').remove();
		});
	<?php } else { ?>
		$j(obj).closest('tr').remove();
	<?php } ?>
}

function adicionarEtapaHandler(){
	var id = $j('#ETAPA').val();
	var desc = $j('#ETAPA option:selected').text();
	var idEtapa = $j('#ETAPA option:selected').val();
	<?php if( $idProcesso != 0 ){ ?>
		$j.post(util.options.site_url + 'json/admin/addEtapa/'+<?php echo($idProcesso); ?>+'/'+idEtapa+'/'+<?php echo $em_elaboracao[0]["ID_STATUS"]; ?>, null, function( data ){
			adicionarEtapa(id, desc, '', data, '<?php echo $em_elaboracao[0]["ID_STATUS"]; ?>');
		});
	<?php } else { ?>
		adicionarEtapa(id, desc, '', 0, '<?php echo $em_elaboracao[0]["ID_STATUS"]; ?>');
	<?php } ?>
}

function adicionarEtapa(idEtapa, descEtapa, qtdHoras, idProcessoEtapa, idStatus){
	if( idEtapa == '' ){
		alert('Escolha uma etapa');
		return;
	}
	//alert(idEtapa + " - " + descEtapa + " - " + qtdHoras + " - " + idProcessoEtapa + " - " + idStatus);
	var html = $j('<div></div>').append(modelo.clone()).html();

	var obj = {
		ID_ETAPA: idEtapa,
		DESC_ETAPA: descEtapa,
		QTD_HORAS: qtdHoras,
		ID_PROCESSO_ETAPA: idProcessoEtapa
	};

	var m = null;
	while( (m = html.match(/\{(\w+)\}/)) ){
		html = html.replace(m[0], obj[ m[1] ].toString().replace(/\s/g, '&nbsp;'));
	}

	var html = $j(html);

	html.find('.cb_status').val(idStatus);
	
	html.find('.qtd_horas').mask( '99:99' );
	
	container.append( html );

}

$j(function($){
	container = $('#modelo').parent();
	
	modelo = $('#modelo').clone();
	modelo.attr('id','');

	container.sortable({
		handle: '.ordenar'
	});

	$('#modelo').remove();
			
	$('#ID_CLIENTE').change(function(){
		clearSelect($('#ID_TIPO_PECA')[0],1);
		$.post(util.options.site_url + 'json/admin/getTipoPecaCliente', 'id=' + this.value, function(json){
			montaOptions($('#ID_TIPO_PECA')[0], json, 'ID_TIPO_PECA','DESC_TIPO_PECA');
		},'json');
	});
	
	// dados que vem do PHP
	<?php
	
	if( !empty($_POST['etapas']) ){
		foreach($_POST['etapas'] as $i => $idetapa){
			printf('adicionarEtapa(%d,"%s","%s", %d, "%s");' . PHP_EOL
				, $idetapa
				, $_POST['descricoes'][$i]
				, $_POST['qtd_horas'][$i]
				, $_POST['id_processo_etapas'][$i]
				, $_POST['id_status'][$i]
			);
		}
	}
	?>
	
});

$j('#HORA_INICIO').mask( '99:99' );
$j('#HORA_FINAL').mask( '99:99' );

</script>
<?php $this->load->view("ROOT/layout/footer") ?>
