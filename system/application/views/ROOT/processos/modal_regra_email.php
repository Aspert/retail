<script type="text/javascript">
	$j(document).ready(function() {
		$j("#regras").tabs();
		<?php foreach( $regras as $regra ) { ?>
			$j('#ID_AGENCIA<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>').change(function() {
				$j('#ID_GRUPO<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>').empty();
				$j('#ID_CLIENTE<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>').value = '';
				montaOptionsAjax($('ID_GRUPO<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>'),'<?php echo site_url('json/admin/getGruposByAgencia'); ?>','id=' + this.value,'ID_GRUPO','DESC_GRUPO');
			});
	
			$j('#ID_CLIENTE<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>').change(function() {
				$j('#ID_GRUPO<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>').empty();
				$j('#ID_AGENCIA<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>').value = '';
				montaOptionsAjax($('ID_GRUPO<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>'),'<?php echo site_url('json/admin/getGruposByCliente'); ?>','id=' + this.value,'ID_GRUPO','DESC_GRUPO');
			});
			
			$j('#btnAddGrupo<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>').click(function(){
				loadGrupo( $('ID_GRUPO<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>').value, <?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?> );
				$('ID_AGENCIA<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>').value = '';
				$('ID_CLIENTE<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>').value = '';
				$j('#ID_GRUPO<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>').empty();
			});

			$j('#qtaHoraEnvio<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>').mask( '99:99' );
		<?php } ?>
	
		<?php
		
		foreach( $grupos_selecionados as $i => $grupos_selecionado ){
			foreach( $grupos_selecionado as $grupo ){
				echo 'loadGrupo('.$grupo.', ' . $i . ');' . PHP_EOL;
			}
		}
		
		?>
	});
	// carrega a listagem de usuarios por grupo
	function loadGrupo(idGrupo, idRegra ){
		if( $j('#appendUsuarios'+idRegra).find('.grupo_'+idGrupo).length > 0 ){
			alert('Este grupo ja esta carregado');
			return;
		}
		
		if( idGrupo == '' ){
			alert('Informe o grupo!');
			return;
		}
		
		// carrega a listagem de usuarios
		$j.post('<?php echo site_url('json/admin/loadGrupoProcessoRegra/' . $idProcessoEtapa); ?>/' + idGrupo + '/' + idRegra, null, function(html){
			// quando terminar, coloca na tela
			$j(html).appendTo('#appendUsuarios'+idRegra);
		});
	}

	// seleciona todos os usuarios
	function selecionarTodos(src){
		$j(src).parent().parent().find('input[type="checkbox"]').attr('checked', true);
	}

	// desmarca todos os usuarios
	function selecionarNenhum(src){
		$j(src).parent().parent().find('input[type="checkbox"]').attr('checked', false);
	}

	// remove um grupo da lista
	function removerGrupo(src){
		$j(src).parent().parent().parent().remove();
	}

	//Salvar regras
	function salvarRegras(){
		var valido = validar();
		if( valido === true ){
			$j('#formulario').ajaxSubmit(function( data ) { 
				removeDivModal();
	        });
		} else {
			alert( valido );
		}
	}

	//Validar Form
	function validar(){
		var retorno = true;
		$j('input').each(function(index) {
		   if( $j(this).attr('id') && $j(this).attr('id').substr(0, 12) == 'qtaHoraEnvio' ){
			   var valor = $j(this).val();
				 if( valor != '' ){
					var arrHoras = valor.split(':');
					if( parseInt(arrHoras[1]) > 59 ){
						if( retorno === true ){
							retorno = '';
						}
						retorno += 'Quantidade de horas esta incorreta: '  + valor + '\n';
					} else {
						var resto = arrHoras[1] % 5;
						if ( resto != 0 ) {
							if( retorno === true ){
								retorno = '';
							}
							retorno += 'Intervalo deve ser de 5 minutos: '  + valor + '\n';
						}
					}
				}
		   }
		});
		return retorno;
	}
	
</script>
<form id="formulario" action="<?php echo site_url('json/admin/saveRegrasProcesso/'.$idProcessoEtapa); ?>" method="POST">
	<div id="regras">
	    <ul>
	    	<?php foreach( $regras as $regra ) { ?>
	       		<li><a href="#regra<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>"><span><?php echo($regra['CHAVE_REGRA_EMAIL_PROCESSO_ETAPA']); ?></span></a></li>
	        <?php } ?>
	    </ul>
	   <?php foreach( $regras as $regra ) { ?>
		<div id='regra<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>'>
			<p><?php echo($regra['DESC_REGRA_EMAIL_PROCESSO_ETAPA']); ?></p>
			<br/>
			<p>Quantidade de horas antes do envio:
				<input value="<?php echo($regra['qtaHoras']); ?>" id="qtaHoraEnvio<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>" name="qtaHoraEnvio[<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>]"></input>
			</p>
			<br/>
			<h1>Adicionar usuarios para a regra</h1>
			<table width="759" border="0">
				<tr>
					<th width="137" scope="col">Agencia</th>
					<th width="129" scope="col">Cliente</th>
					<th width="130" scope="col">Grupo</th>
					<td width="345" scope="col">&nbsp;</td>
				</tr>
				<tr>
					<td><select name="ID_AGENCIA<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>" id="ID_AGENCIA<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>" class="comboPesquisa">
						<option value=""></option>
						<?php echo montaOptions($agencias,'ID_AGENCIA','DESC_AGENCIA', post('ID_AGENCIA',true)); ?>
						</select></td>
					<td><select name="ID_CLIENTE<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>" id="ID_CLIENTE<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>" class="comboPesquisa">
						<option value=""></option>
						<?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', post('ID_CLIENTE',true)); ?>
						</select></td>
					<td><select name="ID_GRUPO<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>" id="ID_GRUPO<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>" class="comboPesquisa">
						<option value=""></option>
						<?php echo montaOptions($grupos,'ID_GRUPO','DESC_GRUPO', post('ID_GRUPO',true)); ?>
						</select></td>
					<td><a class="button" href="javascript:;" title="Adicionar Grupo" id="btnAddGrupo<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>"><span>+ Adicionar grupo</span></a></td>
				</tr>
			</table>
			<br />
			<br />
			<div id="appendUsuarios<?php echo($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA']); ?>"> </div>
			<div style="clear:both">&nbsp;</div>
		</div>
	   <?php } ?>
	</div>
	<br /><br />
	<a class="button" href="javascript:;" onclick="removeDivModal();" title="Cancelar"><span>Cancelar</span></a>
	<a class="button" href="javascript:;" onclick="salvarRegras()" title="Salvar"><span>Salvar</span></a>
	<div>&nbsp; </div>
</form>
