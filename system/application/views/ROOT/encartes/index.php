<?php $this->load->view("ROOT/layout/header"); ?>

<style>
	.divEncarte 
	{
		padding-left: 7%;
		padding-right: 7%;
		
	}
	.imageContainer img
	{
		width: auto;
		height: auto;
		max-width: 140px;
		max-height: 140px;
	}
	hr {
		border: 0;
		width: 100%;
		background-color: rgba(69, 71, 71, 0.34);
		height: 1px;
	}
	.imageContainer {
	   	text-align: center;
		width: 150px;
		float: left;
		height: 140px;
		margin: 10px;
		border: 1px solid rgba(69, 71, 71, 0.34);
		padding: 10px;
	}
	#contentGlobal textarea
	{
		width: 500px;
		height: 150px;
	}
	#encarte
	{
		margin-right: 20px;
	}
	.infoArquivo
	{
		font-size: 17px;
	}
</style>
<div id="contentGlobal">
	<form action="<?php echo base_url().index_page(); ?>/encartes/uploadEncarte/" method="post" id="encartes" enctype="multipart/form-data">
	
		<table>
			<tr>
				<td width="50%">
					<span class="infoArquivo">Informações do Arquivo</span>
					<textarea id="observacao" name="observacao" rows="" cols=""></textarea>
				</td>
				<td width="25%">
					<input type="file" name="encarte" id="encarte">
				</td>
				<td width="25%">
					<a class="button" href="javascript:" id="btnImportar">
						<span>Importar arquivo</span>
					</a>
				</td>
			</tr>
		</table>
		
	</form>
	<hr>
	<div class="divEncarte">
		<?php if(!empty($encartes)) {?>
			<?php foreach($encartes as $key => $encarte) {?>
				<div class="imageContainer">
					<img onclick="exibirEncarte(<?= $encarte['ID_ENCARTE']; ?>)"  src="<?= base_url()?>img.php?img_id=<?= $encarte['FileID']; ?>&img_size=medium&img_path=<?= $encarte['path']; ?>">
				</div>
			<?php }?>
		<?php }?>
	</div>
</div>
<script type="text/javascript">

	function exibirEncarte(fileID){
		window.location = '<?php echo base_url().index_page(); ?>/encartes/form/'+fileID;
	}

	$j('#btnImportar').click(
			function(){
				$j('#encartes').submit();
			}
	);
</script>

<?php $this->load->view("ROOT/layout/footer") ?>