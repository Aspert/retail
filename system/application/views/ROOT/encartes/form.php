<?php //$this->load->view("ROOT/layout/header"); ?>
	<link rel="stylesheet" href="<?= base_url()?>css/flipbook/novo/style.css" type="text/css" />
	<link href='http://fonts.googleapis.com/css?family=Play:400,700' rel='stylesheet' type='text/css'>
	
	<!--[if IE]>
	<script src="js/html5.js" type="text/javascript"></script>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js" type="text/javascript"></script>
	<![endif]-->

	<!--[if IE]><script src="js/selectivizr.js"></script><![endif]-->
	<script src="<?= base_url()?>js/flipbook/jquery.js"></script>
	<script src="<?= base_url()?>js/flipbook/turn.js"></script>
	<script src="<?= base_url()?>js/flipbook/onload.js"></script>
	<style>
		.loader {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url('<?= base_url()?>img/flipbook/page-loader.gif') 50% 50% no-repeat rgb(249,249,249);
		}
	</style>
	<script type="text/javascript">
	
		$(window).load(function() {
			$(".loader").delay( 10000 ).fadeOut("slow");
		});
		
	</script>
	
	<!-- BEGIN PAGE -->
	<div class="loader"></div>
	<div id="page">
		
		<!-- BEGIN ABOUT -->
		<article id="about">

			<div id="end">
			<p>the end</p>
			</div>
		
		</article>
		<!-- END ABOUT -->

		<!-- BEGIN BOOK -->
		
		<div id="book">
			<!--<div id="cover">
				<ul>
					<li>
						<a href="#">
							<img src="img/examples/cover-photo1.jpg" alt="" />
						</a>
					</li>
					
					<li>
						<a href="#">
							<img src="img/examples/cover-photo2.jpg" alt="" />
						</a>
					</li>
					
					<li>
						<a href="#">
							<img src="img/examples/cover-photo3.jpg" alt="" />
						</a>
					</li>
				</ul>
				
				<img src="img/logo-on-book.png" alt="flip book" id="logo-cover" />
			</div>-->
			
			<? foreach ( $ENCARTES as $key => $encarte) {?>
			<div id="<?= $encarte['FileID']; ?>" style="background-image:url(<?= base_url()?>img.php?img_id=<?= $encarte['FileID']; ?>&img_size=normal&img_path=<?= $encarte['path']; ?>);">
				<div class="meta left">
					<!--<span class="num">2</span>
					<span class="description">Collection 2012</span>-->
				</div>
			</div>
			<?}?>

			<!--<div style="background-image:url(pages/3.jpg);">
				<div class="meta right">
					<span class="description">Collection 2012</span>
					<span class="num">3</span>
				</div>
			</div>-->
			
			
		</div><!-- END BOOK -->

		<a class="nav_arrow prev"></a>
		<a class="nav_arrow next"></a>

	</div>
	<!-- END PAGE -->


	<!-- BEGIN FOOTER -->
	<footer id="footer">
	
		<!-- <a href="#" id="logo">
			<img src="img/logo.png" alt="" />
		</a> -->
		
		<nav id="center" class="menu">
			<ul>
				<li>
					<a href="<?= base_url().index_page()?><?= $ID_ENCARTE;?>" class="download" title="Download (PDF)"></a>
				</li>
				<li>
					<a class="zoom_in" title="Zoom In"></a>
				</li>
				<li>
					<a class="zoom_out" title="Zoom Out"></a>
				</li>
				<!-- <li>
					<a class="zoom_auto" title="Zoom Auto"></a>
				</li> -->
				<!-- <li>
					<a class="zoom_original" title="Zoom Original (scale 1:1)"></a>
				</li> -->
				<li>
					<a class="show_all" title="Show All Pages"></a>
				</li>
				<li>
					<a class="home" title="Show Home Page"></a>
				</li>
				
			</ul>
		</nav>
		
		<nav id="right" class="menu">
			<ul>
				<!-- <li>
					<a class="contact" title="Send Message"></a>
				</li> -->
				<li class="goto">
					<label for="page-number">Ir para página</label>
					<input type="text" id="page-number" />
					<button type="button">IR</button>
				</li>
			</ul>
		</nav>
	
	</footer>
	<!-- END FOOTER -->



<div class="overlay" id="contact">

	<form>
		<a class="close">X</a>

		<fieldset>
			<h3>contact</h3>

			<p>
				<input type="text" value="name..." id="name" class="req" />
			</p>

			<p>
				<input type="text" value="email..." id="email" class="req" />
			</p>

			<p>
				<textarea id="message" class="req">message</textarea>
			</p>

			<p>
				<button type="submit">Send Message</button>
			</p>
		</fieldset>
		
		<fieldset class="thanks">
			<h3>Message Sent</h3>
			<p>This window will close in 5 seconds.</p>
		</fieldset>
	</form>

</div>



<!-- BEGIN ALL PAGES -->
<div class="overlay" id="all_pages">

	<section class="container">

		<div id="menu_holder">

			<ul id="slider">
				<? foreach ( $ENCARTES as $key => $encarte) {?>
					<li class="page<?= $key+1;?>">
						<img src="<?= base_url()?>img.php?img_id=<?= $encarte['FileID']; ?>&img_size=medium&img_path=<?= $encarte['path']; ?>" alt="" />
					</li>
				<?}?>
<!--				
				<li class="page2">
					<img src="pages/thumbs/2.jpg" alt="" />
				</li>
				
				<li class="page3">
					<img src="pages/thumbs/3.jpg" alt="" />
				</li>
				
				<li class="page4">
					<img src="pages/thumbs/4_5.jpg" alt="" />
				</li>
				
				<li class="page6">
					<img src="pages/thumbs/6_7.jpg" alt="" />
				</li>
				
				<li class="page8">
					<img src="pages/thumbs/8.jpg" alt="" />
				</li>
				
				<li class="page9">
					<img src="pages/thumbs/9.jpg" alt="" />
				</li>
				
				<li class="page10">
					<img src="pages/thumbs/10_11.jpg" alt="" />
				</li>
				
				<li class="page12">
					<img src="pages/thumbs/12_13.jpg" alt="" />
				</li>
				
				<li class="page14">
					<img src="pages/thumbs/14_15.jpg" alt="" />
				</li>

				<li class="page16">
					<img src="pages/thumbs/end.png" alt="" />
				</li>
-->				
			</ul>
		</div>

	</section>

</div>
<!-- END ALL PAGES -->
<?php //$this->load->view("ROOT/layout/footer") ?>