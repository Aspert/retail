<?php if(empty($itensPai)) exit; ?>

<tbody>
	<?php
		$idCenario = 0;	
	
		foreach($itensPai as $item):
			$classPraca = 'praca_' . $praca['ID_PRACA'];
			$i = $item['ID_FICHA'];
			
			if( ($idCenario != $item['ID_CENARIO']) && ($item['ID_CENARIO'] > 0) ){
				$idCenario = $item['ID_CENARIO'];
?>
				<tr style="background-color:<?php echo $item['COR_CENARIO'] ?>;">
					<td colspan="4" style="">
						<?php echo "<strong>" . $item['NOME_CENARIO'] . "</strong> (" .$item['COD_CENARIO']. ")" ;?>
					</td>
					<td class="grupo1" colspan="4"></td>
					<td class="grupo2" colspan="4"></td>
					<td class="grupo3" colspan="6"></td>
					<td class="grupo4"></td>
					<td class="grupo5" colspan="6"></td>
				</tr>
<?php 
			}
?>
			<tr>
				<td style="<?php if( $item['ID_CENARIO'] > 0){ echo 'padding: 0px;'; } ?>">
					<?php if( $item['ID_CENARIO'] > 0): ?>
						<div style="background-color:<?php echo $item['COR_CENARIO']; ?>;height:90px;float:left;width:10px;">&nbsp;</div>
					<?php endif; ?>
					<?php if(!empty($item['FILE_ID'])): ?>
					<a href="<?php echo base_url(); ?>img.php?img_id=<?php echo $item['FILE_ID']; ?>&img_size=big&a.jpg" class="fancy"><img src="<?php echo base_url(); ?>img.php?img_id=<?php echo $item['FILE_ID']; ?>" height="50" style="<?php if( $item['ID_CENARIO'] > 0){ echo 'margin-top:5px;'; } ?>" /></a>
					<br />
					<?php endif; ?>
					<?php echo $item['COD_BARRAS_FICHA']; ?> <br />
					<input type="hidden" name="codigos[]" value="<?php echo $item['ID_FICHA']; ?>" />
					<input type="hidden" name="item[<?php echo $item['ID_PRICING']; ?>][ID_EXCEL_ITEM]" value="<?php echo $item['ID_EXCEL_ITEM']; ?>" />
					<?php echo substr($item['NOME_FICHA'],0,60); ?></td>
				<td align="center">
					<a class="exibe_filhas_<?php echo $item['ID_FICHA']; ?>" style="display:none;" id="exibe_filhas_<?php echo $item['ID_FICHA']; ?>" href="javascript:new Util().vazio();" title="Exibir Filhas" onclick="$j('.modelo_ficha_pauta_filha_<?php echo $item['ID_FICHA']; ?>').toggle();toggleImgFilha($j(this).closest('td'));">
						<img src="<?= base_url().THEME ?>img/open_filhas.png"  border="0" alt="Exibir Filhas"/>
					</a>
				</td>
				<td><?php echo $item['DESC_CATEGORIA']; ?></td>
				<td><?php echo $item['DESC_SUBCATEGORIA']; ?></td>
				
				<td class="grupo1"><input id="<?php printf('PRECO_UNITARIO-%d', $item['ID_FICHA']); ?>" class="itemPricing PRECO_UNITARIO-<?php echo $item['ID_FICHA']; ?> real <?php echo $classPraca; ?>" type="text" name="item[<?php echo $item['ID_PRICING']; ?>][PRECO_UNITARIO]" id="<?php printf('precoUnitario-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo money($item['PRECO_UNITARIO']); ?>" size="9"/></td>
				<td class="grupo1"><input id="<?php printf('TIPO_UNITARIO-%d', $item['ID_FICHA']); ?>" class="itemPricing TIPO_UNITARIO-<?php echo $item['ID_FICHA']; ?> <?php echo $classPraca; ?>" type="text" name="item[<?php echo $item['ID_PRICING']; ?>][TIPO_UNITARIO]" id="<?php printf('tipoUnitario-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $item['TIPO_UNITARIO']; ?>" size="9"/></td>
				<td class="grupo1"><input id="<?php printf('PRECO_CAIXA-%d', $item['ID_FICHA']); ?>" class="itemPricing PRECO_CAIXA-<?php echo $item['ID_FICHA']; ?> real <?php echo $classPraca; ?>" type="text" name="item[<?php echo $item['ID_PRICING']; ?>][PRECO_CAIXA]" id="<?php printf('precoCaixa-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo money($item['PRECO_CAIXA']); ?>" size="9"/></td>
				<td class="grupo1"><input id="<?php printf('TIPO_CAIXA-%d', $item['ID_FICHA']); ?>" class="itemPricing TIPO_CAIXA-<?php echo $item['ID_FICHA']; ?><?php echo $classPraca; ?>" type="text" name="item[<?php echo $item['ID_PRICING']; ?>][TIPO_CAIXA]" id="<?php printf('tipoCaixa-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $item['TIPO_CAIXA']; ?>" size="9"/></td>
				
				<td class="grupo2"><input <?php if($parcelaDinamica){ ?>onBlur="parcelaDinamica(this, '<?php echo $item["ID_FICHA"]; ?>', '<?php echo $item["ID_CLIENTE"]; ?>');"<?php ;}?> id="<?php printf('PRECO_VISTA-%d', $item['ID_FICHA']); ?>" class="itemPricing PRECO_VISTA-<?php echo $item['ID_FICHA']; ?> precoVista real <?php echo $classPraca; ?>" type="text" name="item[<?php echo $item['ID_PRICING']; ?>][PRECO_VISTA]" id="<?php printf('precoVista-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo money($item['PRECO_VISTA']); ?>" size="9"/></td>
				<td class="grupo2"><input id="<?php printf('PRECO_DE-%d', $item['ID_FICHA']); ?>" class="itemPricing PRECO_DE-<?php echo $item['ID_FICHA']; ?> precoDe real <?php echo $classPraca; ?>" type="text" name="item[<?php echo $item['ID_PRICING']; ?>][PRECO_DE]" id="<?php printf('precoDe-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo money($item['PRECO_DE']); ?>" size="9"/></td>
				<td class="grupo2"><input id="<?php printf('PRECO_POR-%d', $item['ID_FICHA']); ?>" class="PRECO_POR-<?php echo $item['ID_FICHA']; ?> real itemPricing <?php echo $classPraca; ?>" type="text" name="item[<?php echo $item['ID_PRICING']; ?>][PRECO_POR]" id="<?php printf('precoPor-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo money($item['PRECO_POR']); ?>" size="9"/></td>
				<td class="grupo2"><input id="<?php printf('ECONOMIZE-%d', $item['ID_FICHA']); ?>" class="ECONOMIZE-<?php echo $item['ID_FICHA']; ?> real itemPricing <?php echo $classPraca; ?>" type="text" name="item[<?php echo $item['ID_PRICING']; ?>][ECONOMIZE]" id="<?php printf('economize-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo money($item['ECONOMIZE']); ?>" size="9"/></td>
				
				<td class="grupo3"><input id="<?php printf('ENTRADA-%d', $item['ID_FICHA']); ?>" class="itemPricing ENTRADA-<?php echo $item['ID_FICHA']; ?> real <?php echo $classPraca; ?>" type="text" name="item[<?php echo $item['ID_PRICING']; ?>][ENTRADA]" id="<?php printf('entrada-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo money($item['ENTRADA']); ?>" size="9"/></td>
				<td class="grupo3"><input id="<?php printf('PARCELAS-%d', $item['ID_FICHA']); ?>" class="itemPricing PARCELAS-<?php echo $item['ID_FICHA']; ?> <?php echo $classPraca; ?>" type="text" name="item[<?php echo $item['ID_PRICING']; ?>][PARCELAS]" id="<?php printf('parcelas-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $item['PARCELAS']; ?>" size="9"/></td>
				<td class="grupo3"><input id="<?php printf('PRESTACAO-%d', $item['ID_FICHA']); ?>" class="itemPricing PRESTACAO-<?php echo $item['ID_FICHA']; ?> real <?php echo $classPraca; ?>" type="text" name="item[<?php echo $item['ID_PRICING']; ?>][PRESTACAO]" id="<?php printf('prestacao-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo money($item['PRESTACAO']); ?>" size="9"/></td>
				<td class="grupo3"><input id="<?php printf('JUROS_AM-%d', $item['ID_FICHA']); ?>" class="itemPricing JUROS_AM-<?php echo $item['ID_FICHA']; ?> float <?php echo $classPraca; ?>" type="text" name="item[<?php echo $item['ID_PRICING']; ?>][JUROS_AM]" id="<?php printf('jurosAM-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $item['JUROS_AM']; ?>" size="9"/></td>
				<td class="grupo3"><input id="<?php printf('JUROS_AA-%d', $item['ID_FICHA']); ?>" class="itemPricing JUROS_AA-<?php echo $item['ID_FICHA']; ?> float <?php echo $classPraca; ?>" type="text" name="item[<?php echo $item['ID_PRICING']; ?>][JUROS_AA]" id="<?php printf('jurosAA-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $item['JUROS_AA']; ?>" size="9"/></td>
				<td class="grupo3"><input id="<?php printf('TOTAL_PRAZO-%d', $item['ID_FICHA']); ?>" class="itemPricing TOTAL_PRAZO-<?php echo $item['ID_FICHA']; ?> real <?php echo $classPraca; ?>" type="text" name="item[<?php echo $item['ID_PRICING']; ?>][TOTAL_PRAZO]" id="<?php printf('totalPrazo-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo money($item['TOTAL_PRAZO']); ?>" size="9"/></td>
				
				<td class="grupo5"><input id="<?php printf('TEXTO_LEGAL-%d', $item['ID_FICHA']); ?>" class="itemPricing TEXTO_LEGAL-<?php echo $item['ID_FICHA']; ?> <?php echo $classPraca; ?>" type="text" name="item[<?php echo $item['ID_PRICING']; ?>][TEXTO_LEGAL]" id="<?php printf('texto_legal-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $item['TEXTO_LEGAL']; ?>" size="9"/></td>
				<td class="grupo5"><input id="<?php printf('EXTRA1_ITEM-%d', $item['ID_FICHA']); ?>" class="itemPricing EXTRA1_ITEM-<?php echo $item['ID_FICHA']; ?> <?php echo $classPraca; ?>" type="text" name="item[<?php echo $item['ID_PRICING']; ?>][EXTRA1_ITEM]" id="<?php printf('EXTRA1_ITEM-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $item['EXTRA1_ITEM']; ?>" size="9"/></td>
				<td class="grupo5"><input id="<?php printf('EXTRA2_ITEM-%d', $item['ID_FICHA']); ?>" class="itemPricing EXTRA2_ITEM-<?php echo $item['ID_FICHA']; ?> <?php echo $classPraca; ?>" type="text" name="item[<?php echo $item['ID_PRICING']; ?>][EXTRA2_ITEM]" id="<?php printf('EXTRA2_ITEM-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $item['EXTRA2_ITEM']; ?>" size="9"/></td>
				<td class="grupo5"><input id="<?php printf('EXTRA3_ITEM-%d', $item['ID_FICHA']); ?>" class="itemPricing EXTRA3_ITEM-<?php echo $item['ID_FICHA']; ?> <?php echo $classPraca; ?>" type="text" name="item[<?php echo $item['ID_PRICING']; ?>][EXTRA3_ITEM]" id="<?php printf('EXTRA3_ITEM-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $item['EXTRA3_ITEM']; ?>" size="9"/></td>
				<td class="grupo5"><input id="<?php printf('EXTRA4_ITEM-%d', $item['ID_FICHA']); ?>" class="itemPricing EXTRA4_ITEM-<?php echo $item['ID_FICHA']; ?> <?php echo $classPraca; ?>" type="text" name="item[<?php echo $item['ID_PRICING']; ?>][EXTRA4_ITEM]" id="<?php printf('EXTRA4_ITEM-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $item['EXTRA4_ITEM']; ?>" size="9"/></td>
				<td class="grupo5"><input id="<?php printf('EXTRA5_ITEM-%d', $item['ID_FICHA']); ?>" class="itemPricing EXTRA5_ITEM-<?php echo $item['ID_FICHA']; ?> <?php echo $classPraca; ?>" type="text" name="item[<?php echo $item['ID_PRICING']; ?>][EXTRA5_ITEM]" id="<?php printf('EXTRA5_ITEM-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $item['EXTRA5_ITEM']; ?>" size="9"/></td>
				
				<td class="grupo4"><input id="<?php printf('OBSERVACOES-','%d-%d', $item['ID_FICHA']); ; ?>" class="itemPricing OBSERVACOES-<?php echo $item['ID_FICHA']; ?> <?php echo $classPraca; ?>" type="text" name="item[<?php echo $item['ID_PRICING']; ?>][OBSERVACOES]" id="<?php printf('observacoes-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $item['OBSERVACOES']; ?>" size="9"/></td>
			</tr>
		<?php foreach($itensFilha as $itemFilha): ?>
			<?php if($itemFilha['ID_FICHA_PAI'] == $item['ID_FICHA']): ?>
				<tr class="modelo_ficha_pauta_filha_<?php echo $item['ID_FICHA']; ?>" style="display:none;">
					<td align="center" class="modelo_ficha_pauta_filha">
						<img src="<?= base_url().THEME ?>img/seta_filha.png" />
					</td>
					<td class="modelo_ficha_pauta_filha">
						<?php if(!empty($itemFilha['FILE_ID'])): ?>
						<a href="<?php echo base_url(); ?>img.php?img_id=<?php echo $itemFilha['FILE_ID']; ?>&img_size=big&a.jpg" class="fancy"><img src="<?php echo base_url(); ?>img.php?img_id=<?php echo $itemFilha['FILE_ID']; ?>" height="50" /></a>
						<br />
						<?php endif; ?>
						<?php echo $itemFilha['COD_BARRAS_FICHA']; ?> <br />
						<input type="hidden" name="codigos[]" value="<?php echo $itemFilha['ID_FICHA']; ?>" />
						<input type="hidden" name="item[<?php echo $itemFilha['ID_PRICING']; ?>][ID_EXCEL_ITEM]" value="<?php echo $itemFilha['ID_EXCEL_ITEM']; ?>" />
						<?php echo substr($itemFilha['NOME_FICHA'],0,60); ?>
					</td>
					<td class="modelo_ficha_pauta_filha"><?php echo $itemFilha['DESC_CATEGORIA']; ?></td>
					<td class="modelo_ficha_pauta_filha"><?php echo $itemFilha['DESC_SUBCATEGORIA']; ?></td>

					<td class="modelo_ficha_pauta_filha grupo1"><input id="<?php printf('PRECO_UNITARIO-%d-%d', $item['ID_FICHA'], $itemFilha['ID_FICHA']); ?>" class="itemPricing PRECO_UNITARIO-<?php echo $itemFilha['ID_FICHA']; ?> real <?php echo $classPraca; ?>" type="text" name="item[<?php echo $itemFilha['ID_PRICING']; ?>][PRECO_UNITARIO]" id="<?php printf('precoUnitario-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo money($itemFilha['PRECO_UNITARIO']); ?>" size="9"/></td>
					<td class="modelo_ficha_pauta_filha grupo1"><input id="<?php printf('TIPO_UNITARIO-%d-%d', $item['ID_FICHA'], $itemFilha['ID_FICHA']); ?>" class="itemPricing TIPO_UNITARIO-<?php echo $itemFilha['ID_FICHA']; ?> <?php echo $classPraca; ?>" type="text" name="item[<?php echo $itemFilha['ID_PRICING']; ?>][TIPO_UNITARIO]" id="<?php printf('tipoUnitario-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $itemFilha['TIPO_UNITARIO']; ?>" size="9"/></td>
					<td class="modelo_ficha_pauta_filha grupo1"><input id="<?php printf('PRECO_CAIXA-%d-%d', $item['ID_FICHA'], $itemFilha['ID_FICHA']); ?>" class="itemPricing PRECO_CAIXA-<?php echo $itemFilha['ID_FICHA']; ?> real <?php echo $classPraca; ?>" type="text" name="item[<?php echo $itemFilha['ID_PRICING']; ?>][PRECO_CAIXA]" id="<?php printf('precoCaixa-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo money($itemFilha['PRECO_CAIXA']); ?>" size="9"/></td>
					<td class="modelo_ficha_pauta_filha grupo1"><input id="<?php printf('TIPO_CAIXA-%d-%d', $item['ID_FICHA'], $itemFilha['ID_FICHA']); ?>" class="itemPricing TIPO_CAIXA-<?php echo $itemFilha['ID_FICHA']; ?><?php echo $classPraca; ?>" type="text" name="item[<?php echo $itemFilha['ID_PRICING']; ?>][TIPO_CAIXA]" id="<?php printf('tipoCaixa-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $itemFilha['TIPO_CAIXA']; ?>" size="9"/></td>
					
					<td class="modelo_ficha_pauta_filha grupo2"><input <?php if($parcelaDinamica){ ?>onBlur="parcelaDinamica(this, '<?php echo $item["ID_FICHA"]; ?>-<?php echo $itemFilha["ID_FICHA"]; ?>', '<?php echo $item["ID_CLIENTE"]; ?>');"<?php ;}?> id="<?php printf('PRECO_VISTA-%d-%d', $item['ID_FICHA'], $itemFilha['ID_FICHA']); ?>" class="itemPricing PRECO_VISTA-<?php echo $itemFilha['ID_FICHA']; ?> precoVista real <?php echo $classPraca; ?>" type="text" name="item[<?php echo $itemFilha['ID_PRICING']; ?>][PRECO_VISTA]" id="<?php printf('precoVista-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo money($itemFilha['PRECO_VISTA']); ?>" size="9"/></td>
					<td class="modelo_ficha_pauta_filha grupo2"><input id="<?php printf('PRECO_DE-%d-%d', $item['ID_FICHA'], $itemFilha['ID_FICHA']); ?>" class="itemPricing PRECO_DE-<?php echo $itemFilha['ID_FICHA']; ?> precoDe real <?php echo $classPraca; ?>" type="text" name="item[<?php echo $itemFilha['ID_PRICING']; ?>][PRECO_DE]" id="<?php printf('precoDe-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo money($itemFilha['PRECO_DE']); ?>" size="9"/></td>
					<td class="modelo_ficha_pauta_filha grupo2"><input id="<?php printf('PRECO_POR-%d-%d', $item['ID_FICHA'], $itemFilha['ID_FICHA']); ?>" class="PRECO_POR-<?php echo $itemFilha['ID_FICHA']; ?> real itemPricing <?php echo $classPraca; ?>" type="text" name="item[<?php echo $itemFilha['ID_PRICING']; ?>][PRECO_POR]" id="<?php printf('precoPor-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo money($itemFilha['PRECO_POR']); ?>" size="9"/></td>
					<td class="modelo_ficha_pauta_filha grupo2"><input id="<?php printf('ECONOMIZE-%d-%d', $item['ID_FICHA'], $itemFilha['ID_FICHA']); ?>" class="ECONOMIZE-<?php echo $itemFilha['ID_FICHA']; ?> real itemPricing <?php echo $classPraca; ?>" type="text" name="item[<?php echo $itemFilha['ID_PRICING']; ?>][ECONOMIZE]" id="<?php printf('economize-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo money($itemFilha['ECONOMIZE']); ?>" size="9"/></td>
					
					<td class="modelo_ficha_pauta_filha grupo3"><input id="<?php printf('ENTRADA-%d-%d', $item['ID_FICHA'], $itemFilha['ID_FICHA']); ?>" class="itemPricing ENTRADA-<?php echo $itemFilha['ID_FICHA']; ?> real <?php echo $classPraca; ?>" type="text" name="item[<?php echo $itemFilha['ID_PRICING']; ?>][ENTRADA]" id="<?php printf('entrada-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo money($itemFilha['ENTRADA']); ?>" size="9"/></td>
					<td class="modelo_ficha_pauta_filha grupo3"><input id="<?php printf('PARCELAS-%d-%d', $item['ID_FICHA'], $itemFilha['ID_FICHA']); ?>" class="itemPricing PARCELAS-<?php echo $itemFilha['ID_FICHA']; ?> <?php echo $classPraca; ?>" type="text" name="item[<?php echo $itemFilha['ID_PRICING']; ?>][PARCELAS]" id="<?php printf('parcelas-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $itemFilha['PARCELAS']; ?>" size="9"/></td>
					<td class="modelo_ficha_pauta_filha grupo3"><input id="<?php printf('PRESTACAO-%d-%d', $item['ID_FICHA'], $itemFilha['ID_FICHA']); ?>" class="itemPricing PRESTACAO-<?php echo $itemFilha['ID_FICHA']; ?> real <?php echo $classPraca; ?>" type="text" name="item[<?php echo $itemFilha['ID_PRICING']; ?>][PRESTACAO]" id="<?php printf('prestacao-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo money($itemFilha['PRESTACAO']); ?>" size="9"/></td>
					<td class="modelo_ficha_pauta_filha grupo3"><input id="<?php printf('JUROS_AM-%d-%d', $item['ID_FICHA'], $itemFilha['ID_FICHA']); ?>" class="itemPricing JUROS_AM-<?php echo $itemFilha['ID_FICHA']; ?> float <?php echo $classPraca; ?>" type="text" name="item[<?php echo $itemFilha['ID_PRICING']; ?>][JUROS_AM]" id="<?php printf('jurosAM-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $itemFilha['JUROS_AM']; ?>" size="9"/></td>
					<td class="modelo_ficha_pauta_filha grupo3"><input id="<?php printf('JUROS_AA-%d-%d', $item['ID_FICHA'], $itemFilha['ID_FICHA']); ?>" class="itemPricing JUROS_AA-<?php echo $itemFilha['ID_FICHA']; ?> float <?php echo $classPraca; ?>" type="text" name="item[<?php echo $itemFilha['ID_PRICING']; ?>][JUROS_AA]" id="<?php printf('jurosAA-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $itemFilha['JUROS_AA']; ?>" size="9"/></td>
					<td class="modelo_ficha_pauta_filha grupo3"><input id="<?php printf('TOTAL_PRAZO-%d-%d', $item['ID_FICHA'], $itemFilha['ID_FICHA']); ?>" class="itemPricing TOTAL_PRAZO-<?php echo $itemFilha['ID_FICHA']; ?> real <?php echo $classPraca; ?>" type="text" name="item[<?php echo $itemFilha['ID_PRICING']; ?>][TOTAL_PRAZO]" id="<?php printf('totalPrazo-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo money($itemFilha['TOTAL_PRAZO']); ?>" size="9"/></td>
					
					<td class="modelo_ficha_pauta_filha grupo5"><input id="<?php printf('TEXTO_LEGAL-%d-%d', $item['ID_FICHA'], $itemFilha['ID_FICHA']); ?>" class="itemPricing TEXTO_LEGAL-<?php echo $itemFilha['ID_FICHA']; ?> <?php echo $classPraca; ?>" type="text" name="item[<?php echo $itemFilha['ID_PRICING']; ?>][TEXTO_LEGAL]" id="<?php printf('texto_legal-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $itemFilha['TEXTO_LEGAL']; ?>" size="9"/></td>
					<td class="modelo_ficha_pauta_filha grupo5"><input id="<?php printf('EXTRA1_ITEM-%d-%d', $item['ID_FICHA'], $itemFilha['ID_FICHA']); ?>" class="itemPricing EXTRA1_ITEM-<?php echo $itemFilha['ID_FICHA']; ?> <?php echo $classPraca; ?>" type="text" name="item[<?php echo $itemFilha['ID_PRICING']; ?>][EXTRA1_ITEM]" id="<?php printf('EXTRA1_ITEM-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $itemFilha['EXTRA1_ITEM']; ?>" size="9"/></td>
					<td class="modelo_ficha_pauta_filha grupo5"><input id="<?php printf('EXTRA2_ITEM-%d-%d', $item['ID_FICHA'], $itemFilha['ID_FICHA']); ?>" class="itemPricing EXTRA2_ITEM-<?php echo $itemFilha['ID_FICHA']; ?> <?php echo $classPraca; ?>" type="text" name="item[<?php echo $itemFilha['ID_PRICING']; ?>][EXTRA2_ITEM]" id="<?php printf('EXTRA2_ITEM-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $itemFilha['EXTRA2_ITEM']; ?>" size="9"/></td>
					<td class="modelo_ficha_pauta_filha grupo5"><input id="<?php printf('EXTRA3_ITEM-%d-%d', $item['ID_FICHA'], $itemFilha['ID_FICHA']); ?>" class="itemPricing EXTRA3_ITEM-<?php echo $itemFilha['ID_FICHA']; ?> <?php echo $classPraca; ?>" type="text" name="item[<?php echo $itemFilha['ID_PRICING']; ?>][EXTRA3_ITEM]" id="<?php printf('EXTRA3_ITEM-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $itemFilha['EXTRA3_ITEM']; ?>" size="9"/></td>
					<td class="modelo_ficha_pauta_filha grupo5"><input id="<?php printf('EXTRA4_ITEM-%d-%d', $item['ID_FICHA'], $itemFilha['ID_FICHA']); ?>" class="itemPricing EXTRA4_ITEM-<?php echo $itemFilha['ID_FICHA']; ?> <?php echo $classPraca; ?>" type="text" name="item[<?php echo $itemFilha['ID_PRICING']; ?>][EXTRA4_ITEM]" id="<?php printf('EXTRA4_ITEM-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $itemFilha['EXTRA4_ITEM']; ?>" size="9"/></td>
					<td class="modelo_ficha_pauta_filha grupo5"><input id="<?php printf('EXTRA5_ITEM-%d-%d', $item['ID_FICHA'], $itemFilha['ID_FICHA']); ?>" class="itemPricing EXTRA5_ITEM-<?php echo $itemFilha['ID_FICHA']; ?> <?php echo $classPraca; ?>" type="text" name="item[<?php echo $itemFilha['ID_PRICING']; ?>][EXTRA5_ITEM]" id="<?php printf('EXTRA5_ITEM-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $itemFilha['EXTRA5_ITEM']; ?>" size="9"/></td>
					
					<td class="modelo_ficha_pauta_filha grupo4"><input id="<?php printf('OBSERVACOES-%d-%d', $item['ID_FICHA'], $itemFilha['ID_FICHA']); ?>" class="itemPricing OBSERVACOES-<?php echo $itemFilha['ID_FICHA']; ?> <?php echo $classPraca; ?>" type="text" name="item[<?php echo $itemFilha['ID_PRICING']; ?>][OBSERVACOES]" id="<?php printf('observacoes-%d-%d', $praca['ID_PRACA'], $i); ?>" value="<?php echo $itemFilha['OBSERVACOES']; ?>" size="9"/></td>
				</tr>

				<script type="text/javascript">
					$j('#exibe_filhas_<?php echo $item["ID_FICHA"]; ?>').show();
				</script>

			<?php endif; ?>
		<?php endforeach; ?>
	<?php endforeach; ?>
	<tr>
		<td colspan="18"> Filtrar por p&aacute;gina: |
			<?php for($i=0; $i<$totalProdutos/$limit; $i++): ?>
			<a id="pg<?php echo $i; ?>" class="itemPaginacao" href="#" onclick="carregaPagina(this, <?php echo $i; ?>); return false;">
				<?php echo $i*$limit==$offset ? '<strong>' .($i+1) . '</strong>' : $i+1; ?>
			</a> |
			<?php endfor; ?></td>
	</tr>
</tbody>
