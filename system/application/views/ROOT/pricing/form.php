<?php $this->load->view("ROOT/layout/header") ?>
<?php
$cor = '';
$limit = 70;
?>

<div id="contentGlobal">
	<h1>Pricing</h1>
	<div id="tabs">
		<!-- inicio abas -->
		<ul>
			<li><a href="#dados_job">Dados do Job</a></li>
			<?php if(!empty($praca)): ?>
			<li><a href="#tab-<?php echo $praca['ID_PRACA']; ?>"><?php echo $praca['DESC_PRACA']; ?></a></li>
			<?php endif; ?>
		</ul>
		<!-- fim abas -->
		<div id="dados_job">
			<!-- inicio aba dados do job -->
			<table width="100%" class="Alignleft">
				<tr>
					<td width="17%"><strong>Ag&ecirc;ncia</strong></td>
					<td width="83%"><?php echo $job['DESC_AGENCIA']; ?></td>
				</tr>
				<tr>
					<td><strong>Cliente</strong></td>
					<td><?php echo $job['DESC_CLIENTE']; ?></td>
				</tr>
				<tr>
					<td><strong>Bandeira</strong></td>
					<td><?php echo $job['DESC_PRODUTO']; ?></td>
				</tr>
				<tr>
					<td><strong>Campanha</strong></td>
					<td><?php echo $job['DESC_CAMPANHA']; ?></td>
				</tr>
				<tr>
					<td><strong>Tipo de pe&ccedil;a</strong></td>
					<td><?php echo $job['DESC_TIPO_PECA']; ?></td>
				</tr>
				<?php if(!$_sessao['IS_HERMES']){?>
				<tr>
					<td><strong>Tipo de Job</strong></td>
					<td><?php echo $job['DESC_TIPO_JOB']; ?></td>
				</tr>
				<?php }?>
				<tr>
					<td><strong>N&uacute;mero do Job</strong></td>
					<td><?php echo $job['NUMERO_JOB_EXCEL']; ?></td>
				</tr>
				<tr>
					<td><strong>T&iacute;tulo</strong></td>
					<td><?php echo $job['TITULO_JOB']; ?></td>
				</tr>
				<tr>
					<td><strong>Usu&aacute;rio Respons&aacute;vel</strong></td>
					<td><?php echo $job['NOME_USUARIO_RESPONSAVEL']; ?></td>
				</tr>
				<tr>
					<td><strong>N&uacute;mero de p&aacute;ginas</strong></td>
					<td><?php echo $job['PAGINAS_JOB']; ?></td>
				</tr>
				<?php if(!$_sessao['IS_HERMES']){?>
				<tr>
					<td><strong>Formato</strong></td>
					<td><?php echo centimetragem($job['LARGURA_JOB']), ' x ', centimetragem($job['ALTURA_JOB']); ?></td>
				</tr>
				<?php }?>
				<tr>
					<td><strong>In&iacute;cio  da Validade</strong></td>
					<td><?php echo format_date_to_form($job['DATA_INICIO']); ?></td>
				</tr>
				<tr>
					<td><strong>Final da Validade</strong></td>
					<td><?php echo format_date_to_form($job['DATA_TERMINO']); ?></td>
				</tr>
				<tr>
					<td valign="top"><strong>Observa&ccedil;&otilde;es</strong></td>
					<td><?php echo nl2br($job['OBSERVACOES_EXCEL']); ?></td>
				</tr>
				<tr>
					<td valign="top"><strong>Situa&ccedil;&atilde;o</strong></td>
					<td><?php echo $job['DESC_STATUS']; ?></td>
				</tr>
				<tr>
					<td valign="top"><strong>Etapa atual</strong></td>
					<td><?php echo $job['DESC_ETAPA']; ?></td>
				</tr>
				<tr>
					<td valign="top"><strong>Selecione uma pra&ccedil;a</strong></td>
					<td><select name="idpraca" id="idpraca">
							<option value=""></option>
							<?php
						echo montaOptions($pracas,'ID_PRACA','DESC_PRACA');
						?>
						</select></td>
				</tr>
			</table>
			<!-- Final de Table Objeto -->
		</div>
		<!-- final aba dados do job -->
		<!-- inicio conteudo -->
		<?php if(!empty($praca)): ?>
		<div id="tab-<?php echo $praca['ID_PRACA']; ?>">
			<!-- TABS PRINCIPAIS POR PRACA -->
			<div>Ao mudar de p&aacute;gina, os elementos que est&atilde;o na p&aacute;gina atual ser&atilde;o gravados.</div>
			<div>&nbsp;</div>
			<div>
				Nome da ficha: <input name="nome_ficha" id="nomeFicha" type="text" />
				C&oacute;digo de Barras:  <input name="codigoFicha" id="codigoFicha" type="text" />
				<input id="btnFiltrar" name="btnFiltrar" type="button" value="Filtrar" />
			</div>
			<form method="post" action="<?php echo site_url('pricing/save/' . $this->uri->segment(3)); ?>" id="formPricing" />
			<div class="carregando-<?php echo $praca['ID_PRACA']; ?>" style="padding: 20px;">Carregando ... </div>
			
			<div id="menu">
				<ul>
					<li><a id="btnGrupo-2-<?php echo $praca['ID_PRACA']; ?>" href="javascript:" class="btnGrupos btnGrupos1 gruposPraca-<?php echo $praca['ID_PRACA']; ?>">&Agrave; Vista</a></li>
					<li><a id="btnGrupo-1-<?php echo $praca['ID_PRACA']; ?>" href="javascript:" class="btnGrupos gruposPraca-<?php echo $praca['ID_PRACA']; ?>">Atacarejo</a></li>
					<li><a id="btnGrupo-3-<?php echo $praca['ID_PRACA']; ?>" href="javascript:" class="btnGrupos gruposPraca-<?php echo $praca['ID_PRACA']; ?>">Parcelado</a></li>
					<li><a id="btnGrupo-5-<?php echo $praca['ID_PRACA']; ?>" href="javascript:" class="btnGrupos gruposPraca-<?php echo $praca['ID_PRACA']; ?>">Outros</a></li>
				</ul>
			</div>
			<div id="tableObjeto">
				<table border="0" cellspacing="0" cellpadding="0" class="tableSelection" id="pricing-<?php echo $praca['ID_PRACA']; ?>" width="100%">
					<thead>
						<tr>
							<th width="100">Ficha</th>
							<th width="100">Subfichas</th>
							<th width="100"><?php ghDigitalReplace($_sessao, 'Categoria'); ?></th>
							<th width="100"><?php ghDigitalReplace($_sessao, 'Subcategoria'); ?></th>
							
							<th class="grupo1">Pre&ccedil;o Un.</th>
							<th class="grupo1">Tipo Un.</th>
							<th class="grupo1">Pre&ccedil;o Cx.</th>
							<th class="grupo1">Tipo Cx.</th>
							
							<th class="grupo2">Pre&ccedil;o Vista</th>
							<th class="grupo2">Pre&ccedil;o De</th>
							<th class="grupo2">Pre&ccedil;o Por</th>
							<th class="grupo2">Economize</th>
							
							<th class="grupo3">Entrada</th>
							<th class="grupo3">Parcelas</th>
							<th class="grupo3">Presta&ccedil;&atilde;o</th>
							<th class="grupo3">Juros AM</th>
							<th class="grupo3">Juros AA</th>
							<th class="grupo3">Total Prazo</th>
							
							<th class="grupo5">Texto Legal</th>
							<th class="grupo5">Extra 1</th>
							<th class="grupo5">Extra 2</th>
							<th class="grupo5">Extra 3</th>
							<th class="grupo5">Extra 4</th>
							<th class="grupo5">Extra 5</th>
							
							
							<th class="grupo4">Observa&ccedil;&otilde;es</th>
						</tr>
					</thead>
				</table>
			</div>

			<!-- Final de Content Table Objeto -->
			<div style="border:1px solid #D3D3D3; background-color:#EDECEC; padding:10px; float:left; width:1070px; margin:10px 0 0 -1px;"> Copiar valores de
				<select id="copiar_<?php echo $praca['ID_PRACA']; ?>" class="copiar">
					<option value=""></option>
					<?php
					echo montaOptions($pracas,'ID_PRACA','DESC_PRACA');
					?>
				</select>
			</div>
			</form>
			<div id="both" style="clear: both"></div>
		</div>
		<!-- FINAL TABS PRINCIPAIS PRACA -->
		
		<?php endif; ?>
		<!-- fim conteudo -->
	</div>
	<div style="margin: 10px 0 0 0; float:left;"> <a class="button" href="javascript:" onclick="salvar();"><span>Salvar</span></a>
	<a class="button" href="<?php echo site_url('pricing'); ?>"><span>Cancelar</span></a>
	<?php if($podePassarEtapa && $this->uri->segment(3) != ''): ?>
		<a class="button" href="javascript:" onclick="proximaEtapa()"><span>Pr&oacute;xima Etapa</span></a>
	<?php endif; ?>
    <?php if( $podeVoltarEtapa && $this->uri->segment(3) != ''):?>
        <a class="button" href="<?php echo site_url('checklist/voltar_etapa/'.$job['ID_JOB']); ?>" ><span>Voltar Etapa</span></a>
    <?php endif;?>
    </div>
	<!-- Final de Content Botoes -->
<script type="text/javascript">

var idjob = <?php printf('%d', $this->uri->segment(3)); ?>;
var idpraca = <?php printf('%d', $this->uri->segment(4)); ?>;
var grupoAtual = null;
var url = '<?php echo site_url('json/fluxo/get_itens_pricing'); ?>';
var urlSalvarPagina = '<?php echo site_url('json/fluxo/salvar_pricing'); ?>';
var urlCopia = '<?php echo site_url('json/fluxo/get_itens_pricing_copia'); ?>';
var loader = null;
var limit = <?php echo $limit ?>;

$j(function($) {
	$('.btnGrupos').click(function(){
		var reg = this.id.match(/(\w+)-(\d+)-(\d+)/);
		$('#pricing-' + reg[3]).find('.grupo1, .grupo2, .grupo3, .grupo5').hide();
		$('#pricing-' + reg[3]).find('.grupo'+reg[2]).show();
		
		$('.gruposPraca-' + reg[3]).removeClass('ativo');
		$( this ).addClass('ativo');
		
		grupoAtual = this;
	});
	
	$('.btnGrupos1').each(function(){
		var reg = this.id.match(/(\w+)-(\d+)-(\d+)/);
		grupoAtual = this;
		$(this).click();
	});
	
	$('#tabs').tabs();
	
	if( idpraca > 0 ){
		$('#tabs').tabs('select',1);
		configPreloader( $('#tab-'+idpraca) );
		carregaPagina('#pg0',0);
	}
	
	$j('#idpraca').change(function(){
		if( this.value == '' ){
			return;
		}
		location.href = '<?php echo site_url('pricing/form/'.$this->uri->segment(3)); ?>/' + this.value;
	});
	
	$j('#btnFiltrar').click(function(){
		filtrar();
	});
	
	$j('.copiar').change(copiarDados);
});

function copiarDados(){
	if( this.value == '' ){
		return;
	}
	
	$j(window).scrollTop(0);
	displayStaticMessagewithparameter( 'Efetuando c&oacute;pia. Por favor aguarde.', 300, 100 );
	getDivModal().dialog('open');
	
	var data = $j('form').serialize();
	data += '&idjob='+idjob;
	data += '&idpraca='+this.value;
	
	$j.post(urlCopia, data, function(result){
		var json = eval('['+result+']')[0];
		var idx=1;
		var total = json.length;
		
		function copiaValores(){
			if( json.length > 0 ){
				getDivModal().html('Copiando valores #'+(idx++)+' de '+total);
				var item = json.shift();
				
				for(var str in item){
					if(item.ID_FICHA_PAI > 0){
						//alert('#' + str + '-' + item.ID_FICHA_PAI + '-' + item.ID_FICHA + ' - ' + $j('#' + str + '-' + item.ID_FICHA_PAI + '-' + item.ID_FICHA).size());
						$j('#' + str + '-' + item.ID_FICHA_PAI + '-' + item.ID_FICHA).val(item[str]);
					}
					else{
						$j('#' + str + '-' + item.ID_FICHA).val(item[str]);
					}
				}
				
				setTimeout(copiaValores, 20);
			} else {
				removeDivModal();
				closeMessage();
			}
		}
		
				
		copiaValores();
		
	});
}

function configPreloader( panel ){
	loader = new StackLoader(url, {});
	loader.params.idpraca = idpraca;
	loader.params.idjob = idjob;
	loader.limit = limit;
	loader.firstContainer = '#pricing-'+loader.params.idpraca;
	loader.defaultContainer = '#pricing-'+loader.params.idpraca;
	loader.onCompleteBlock = onCompleteBlockHandler;
	loader.onComplete = onCompleteHandler;
	loader.onStart = onStartHandler;
}

function onStartHandler(loader){
	$j('.carregando-'+loader.params.idpraca).show();
	return true;
}

function onCompleteBlockHandler(loader, container, html){
	bindElement(container.find('tbody:last'));
	return true;
}

function onCompleteHandler(loader){
	$j('.carregando-'+loader.params.idpraca).hide();
}

function filtrar(){
	salvarPaginaAtual(function(){
	
		$j('.tableSelection').find('tbody:first').remove();
		$j('.itemPaginacao').css('font-weight','normal');
		
		loader.params.nome_ficha   = $j('#nomeFicha').val();
		loader.params.codigo_ficha = $j('#codigoFicha').val();
		loader.load(0,limit);
	});
}

function carregaPagina(item, pg){
	salvarPaginaAtual(function(){
		$j('.itemPaginacao').css('font-weight','normal');
		$j(item).css('font-weight','bold');
		$j('.tableSelection').find('tbody:first').remove();
		
		loader.load(pg*loader.limit, loader.limit);
	});
}

function salvarPaginaAtual(callback){
	$j(window).scrollTop(0);
	
	var data = $j('form').serialize();
	
	if( data != '' ){
		displayStaticMessagewithparameter('Gravando dados',300,200);
		getDivModal().dialog('open');
		
		$j.post(urlSalvarPagina, data, function(){
			removeDivModal();
			closeMessage();
			
			if( callback ){
				callback.call(null);
			}
		});
	} else {
		if( callback ){
			callback.call(null);
		}
	}
}

function salvar(){
	salvarPaginaAtual(function(){
		location.href = '<?php echo site_url('pricing/form') ?>/'+idjob;
	});
}

function bindElement(element){
	element.find('.fancy').fancybox();
	element.find('.real').maskMoney({symbol: 'R$', thousands: '.', decimal: ','});
	element.find('.float').maskMoney({symbol: '', thousands: '', decimal: '.'});
	element.find('.grupo1, .grupo2, .grupo3').hide();
	
	$j(grupoAtual).click();
	
	element.find('.precoVista').blur(function(){
		$j(this).parent().parent().find('.precoPor').val( this.value );
		$j(this).parent().parent().find('.precoDe').blur();
	});
	
	element.find('.precoDe').blur(function(){
		var vlrDe = parseFloat(this.value.replace('.','').replace(',','.') );
		var vlrPor = parseFloat(
			$j(this).parent().parent().find('.precoPor').val().replace('.','').replace(',','.')
		);
		
		var vlrEconomize = vlrDe - vlrPor;
		$j(this).parent().parent().find('.economize').val( formatCurrency(vlrEconomize.toFixed(2)) );
	});
}

function parcelaDinamica(e, idFicha, idCliente){
	var val = $j.trim( $j(e).val().replace("R$ ", "") );
	$j.post(
			'<?php echo base_url().index_page(); ?>/json/fluxo/calculaParcelaDinamica', 
			{valor : val, cliente : idCliente}, 
			function(data){
				if(data != ''){
					var json = $j.parseJSON(data);
					$j('#PARCELAS-'+idFicha).val(json.PARCELAS);
					$j('#PRESTACAO-'+idFicha).val(json.PRESTACAO);
				}
				else{
					$j('#PARCELAS-'+idFicha).val('');
					$j('#PRESTACAO-'+idFicha).val('');
				}
			}
		);	
}

<?php if($podePassarEtapa && $this->uri->segment(3) != ''): ?>
function proximaEtapa(){
	salvarPaginaAtual(function(){
		location.href = '<?php echo site_url('pricing/proxima_etapa/'.$this->uri->segment(3)); ?>';
	});

}
<?php endif; ?>

<? if(!empty($erros)): ?>
alert("<? echo implode('\n',$erros); ?>");
<? endif;?>

</script>
</div>
<!-- Final de Content Global -->
<?php $this->load->view("ROOT/layout/footer") ?>
