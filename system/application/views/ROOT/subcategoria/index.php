<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">

<h1>Subcategorias</h1>

<form method="post" action="<?php echo site_url('subcategoria/lista'); ?>" id="pesquisa_simples">
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="Alignleft">
  <tr>
    <td width="8%" rowspan="2" align="center">
	<?php if($podeAlterar): ?>
    <a href="<?php echo site_url('subcategoria/form'); ?>" title="Adicionar"><img src="<?php echo base_url().THEME; ?>img/file_add.png" alt="Adicionar" /></a>
    <?php endif; ?>    </td>
    <td width="16%">Cliente</td>
    <td width="18%">Categoria</td>
    <td width="19%">Subcategoria</td>
    <td width="19%">Status</td>
    <td width="14%">Itens / p&aacute;gina</td>
    <td width="9%" rowspan="2">
	<a href="javascript:;" title="Pesquisar" onclick="$j(this).closest('form').submit()" class="button"><span>Pesquisar</span></a>
	</td>
  </tr>
  <tr>
    <td>
    <?php if(!empty($_sessao['ID_CLIENTE'])): ?>
		<?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?>
    <?php else: ?>
        <select name="ID_CLIENTE" id="ID_CLIENTE">
            <option value=""></option>
            <?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', empty($busca['ID_CLIENTE']) ? '' : $busca['ID_CLIENTE']); ?>
        </select>
    <?php endif; ?>
    </td>
    <td><select name="ID_CATEGORIA" id="ID_CATEGORIA">
      <option value=""></option>
      <?php echo montaOptions($categorias,'ID_CATEGORIA','DESC_CATEGORIA', empty($busca['ID_CATEGORIA']) ? '' : $busca['ID_CATEGORIA']); ?>
    </select></td>
    <td><input type="text" name="DESC_SUBCATEGORIA" id="DESC_SUBCATEGORIA" value="<?php echo empty($busca['DESC_SUBCATEGORIA']) ? '' : $busca['DESC_SUBCATEGORIA']?>" /></td>
     <td>
	  <select name="FLAG_ACTIVE_SUBCATEGORIA" id="FLAG_ACTIVE_SUBCATEGORIA">
        <?php echo montaOptions(array(array('DESC'=>'', 'VALUE'=>''),array('DESC'=>'ATIVO', 'VALUE'=>'1'),array('DESC'=>'INATIVO', 'VALUE'=>'0')),'VALUE','DESC', post('FLAG_ACTIVE_SUBCATEGORIA',true)); ?>
      </select>
	</td>
    <td><span class="campo esq">
      <select name="pagina" id="pagina">
        <?php
		$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
		for($i=5; $i<=50; $i+=5){
			printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
		}
		?>
      </select>
    </span></td>
    </tr>
</table>
<br />
</form>

<?php if( !empty($subcategorias)) :?>

<div id="tableObjeto">
<table cellspacing="1" cellpadding="2" border="0" width="100%" class="tableSelection">
	<thead>
	<tr>
		<?php if($podeAlterar): ?>
        <td widtd="7%">Editar</td>
		<?php endif; ?>
      <td widtd="21%">Cliente</td>
      <td widtd="22%">Categoria</td>
      <td widtd="20%">Subcategoria</td>
      <td widtd="20%">Status</td>
	</tr>
	</thead>
		<?php foreach($subcategorias as $item): ?>
		<tr>
          <?php if($podeAlterar): ?>
			<td align="center"><a title="Editar" href="<?php echo site_url('subcategoria/form/'.$item['ID_SUBCATEGORIA']); ?>"><img src="<?php echo base_url().THEME; ?>img/file_edit.png" alt="Editar" border="0" /></a></td>
          <?php endif; ?>
			<td align="center"><?php echo $item['DESC_CLIENTE']; ?></td>
			<td align="center"><?php echo $item['DESC_CATEGORIA']; ?></td>
		    <td align="center"><?php echo $item['DESC_SUBCATEGORIA']; ?></td>
		    <td align="center"><?php echo $item['FLAG_ACTIVE_SUBCATEGORIA'] == '1' ? 'ATIVO' : 'INATIVO'; ?></td>
		</tr>
        <?php endforeach; ?>
</table>
<span class="paginacao"><?php echo $paginacao; ?></span>
</div>
<?php else: ?>
	Nenhum resultado
<?php endif; ?>

</div>

<script type="text/javascript">

$('ID_CLIENTE').addEvent('change', function(){
	montaOptionsAjax($('ID_CATEGORIA'),'<?php echo site_url('json/admin/getCategoriasByCliente'); ?>','id=' + this.value,'ID_CATEGORIA','DESC_CATEGORIA');
});

</script>

<?php $this->load->view("ROOT/layout/footer") ?>