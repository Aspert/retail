<?php $this->load->view("ROOT/layout/header"); ?>
<div id="contentGlobal">
<h1>Cadastro de subcategorias</h1>
<?php echo form_open('subcategoria/save/'.$this->uri->segment(3));?>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabela_pesquisa tabela_form">
  <tr>
    <td width="16%">Cliente</td>
    <td width="84%"><?php if(!empty($_sessao['ID_CLIENTE'])): ?>
      <?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?>
      <?php else: ?>
      <select name="ID_CLIENTE" id="ID_CLIENTE">
        <option value=""></option>
        <?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', post('ID_CLIENTE',true)); ?>
      </select>
    <?php endif; ?></td>
  </tr>
  <tr>
    <td>Categoria</td>
    <td><select name="ID_CATEGORIA" id="ID_CATEGORIA">
      <option value=""></option>
      <?php echo montaOptions($categorias,'ID_CATEGORIA','DESC_CATEGORIA', post('ID_CATEGORIA',true)); ?>
    </select>
    <?php showError('ID_CATEGORIA'); ?></td>
  </tr>
  <tr>
    <td>Nome</td>
    <td><input name="DESC_SUBCATEGORIA" type="text" value="<?php post('DESC_SUBCATEGORIA')?>" />
    <?php showError('DESC_SUBCATEGORIA'); ?></td>
  </tr>
  <tr>
    <td>C&oacute;digo</td>
    <td><input name="CODIGO_SUBCATEGORIA" type="text" value="<?php post('CODIGO_SUBCATEGORIA')?>" />
    <?php showError('CODIGO_SUBCATEGORIA'); ?></td>
  </tr>
  <tr>
    <td>Status</td>
    <td>
      <select name="FLAG_ACTIVE_SUBCATEGORIA" id="FLAG_ACTIVE_SUBCATEGORIA">
        <?php echo montaOptions(array(array('DESC'=>'ATIVO', 'VALUE'=>'1'),array('DESC'=>'INATIVO', 'VALUE'=>'0')),'VALUE','DESC', post('FLAG_ACTIVE_SUBCATEGORIA',true)); ?>
      </select>
    <?php showError('FLAG_ACTIVE_SUBCATEGORIA'); ?>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
    <a href="<?php echo site_url('subcategoria/lista'); ?>" class="button"><span>Cancelar</span></a>
    <a href="javascript:;" onclick="$j(this).closest('form').submit()" class="button"><span>Gravar</span></a>
    </td>
  </tr>
</table>
<?php echo form_close();?>
</div>

<script type="text/javascript">
$('ID_CLIENTE').addEvent('change', function(){
	clearSelect($('ID_CATEGORIA'), 1);
	montaOptionsAjax($('ID_CATEGORIA'),'<?php echo site_url('json/admin/getCategoriasByCliente'); ?>','id=' + this.value,'ID_CATEGORIA','DESC_CATEGORIA');
});

    </script>

<?php $this->load->view("ROOT/layout/footer") ?>