<SCRIPT LANGUAGE="JavaScript">
<!-- 
function textCounter(field, countfield, maxlimit) {
if (field.value.length > maxlimit)
field.value = field.value.substring(0, maxlimit);
else 
countfield.value = maxlimit - field.value.length;
}
// -->
</script>
<div id="rec_senha" style="width:630px;height:580px; font: normal 11px Verdana, Lucida Grande, Trebuchet MS, Helvetica, sans-serif;">
	<div id="tabsRecuperarSenha">
		<ul>
			<li><a href="#esquecimento_dados">Esqueci meus dados de acesso</a></li>
			<li><a href="#resetar_senha">Resetar senha</a></li>
		</ul>
		<div id="esquecimento_dados">
			<form id="form1" name="form1" method="post" action="<?php echo site_url('login/recuperar_senha'); ?>" >
				<table width="100%" border="0" cellspacing="0" cellpadding="3" style="font: normal 11px Verdana, Lucida Grande, Trebuchet MS, Helvetica, sans-serif;">
					<tr>
						<td colspan="2" align="left"><p style="width:100%">Se o inconveniente estiver relacionado &agrave; perda/esquecimento de alguma informa&ccedil;&atilde;o necess&aacute;ria para acessar o sistema, preencha todos os campos abaixo e a sua solicita&ccedil;&atilde;o ser&aacute; atendida o mais r&aacute;pido poss&iacute;vel. <br />
								Se o problema com o acesso n&atilde;o estiver relacionado ao cadastro na tela inicial favor entrar em contato com o Suporte Aspert:</p>
							<p style="padding-top:5px;"><strong>Atendimento por E-mail:</strong><br />
								<a href="mailto:suporte@aspert.com.br" style="color:#2384c6">suporte@aspert.com.br</a></p>
							<p style="padding-top:5px;"> <strong>Atendimento por Telefone:</strong><br />
								+55 (11) 4349-0222<br />
								De segunda a sexta, das 09:00 as 18:00</p></td>
					</tr>
					<tr>
						<td colspan="2" align="left"><table width="100%" border="0" cellspacing="0" cellpadding="1" style="font: normal 11px Verdana, Lucida Grande, Trebuchet MS, Helvetica, sans-serif;">
								<tr>
									<td style="text-align:right">Motivo&nbsp;&nbsp;&nbsp;</td>
									<td align="left"><select name="motivo" id="motivo">
											<option value="">SELECIONE</option>
											<option value="Perdi meu cart&atilde;o">Perdi meu cart&atilde;o</option>
											<option value="Esqueci meu login">Esqueci meu login</option>
										</select></td>
								</tr>
								<tr>
									<td align="right">&nbsp;</td>
									<td align="left">&nbsp;</td>
								</tr>
								<tr>
									<td width="24%" style="text-align:right">Nome completo&nbsp;&nbsp;&nbsp;</td>
									<td width="76%" align="left"><label>
											<input name="nome_completo" type="text" id="nome_completo" size="30" />
										</label></td>
								</tr>
								<tr align="left">
									<td align="right">&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr align="left">
									<td style="text-align:right">E-mail&nbsp;&nbsp;&nbsp;</td>
									<td><input name="email" type="text" id="email" size="30" /></td>
								</tr>
								<tr align="left">
									<td align="right">&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr align="left">
									<td style="text-align:right" valign="top">Observa&ccedil;&otilde;es&nbsp;&nbsp;</td>
									<td><textarea name="observacoes" wrap=physical cols="40" rows="4" id="observacoes" onKeyUp="textCounter(this,this.form.remLen,255)" onKeyDown="textCounter(this,this.form.remLen,255);"></textarea></td>
								</tr>
								<tr>
									<td align="center">&nbsp;</td>
									<td>Restam&nbsp; <span id="restantes"><input readonly type=text name=remLen size=3 maxlength=3 value="255"></span> caracteres</td>
								</tr>
								<tr>
									<td align="center">&nbsp;</td>
									<td><a class="button" href="javascript:void(0);" onclick="recuperarSenha()"><span>Continuar</span></a> <a class="button" href="javascript:void(0);" onclick="$j('#recuperarSenha').dialog('close');"><span>Cancelar</span></a></td>
								</tr>
							</table></td>
					</tr>
				</table>
			</form>
		</div>
		
		<div id="resetar_senha">
			<form action="<?php echo site_url('usuario/recuperar_senha/resetar'); ?>" method="post" id="resetarSenha">
				<table width="100%" border="0" cellspacing="1" cellpadding="2">
					<tr>
						<td colspan="2">Utilizando este recurso, ser&aacute; gerada uma nova senha e enviaremos para o seu e-mail cadastrado. Para continuar, informe seus dados de acesso abaixo.</td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td width="28%">Login de acesso</td>
						<td width="72%"><input name="login_recuperar_senha" type="text" id="login_recuperar_senha" size="30" /></td>
					</tr>
					<tr>
						<td>E-mail cadastrado</td>
						<td><input name="email_recuperar_senha" type="text" id="email_recuperar_senha" size="30" /></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td><a href="#" onclick="resetarSenha(); return false;" class="button"><span>Criar nova senha</span></a> <a href="#" onclick="$j('#recuperarSenha').dialog('close')" class="button"><span>Cancelar</span></a></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>
