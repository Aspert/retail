<style>
	body{
		outline: 0;
	}
	.endereco{
		margin: 20px 0 20px 50px;
	}
	.telefone{
		margin: 20px 0 20px 50px;
	}
	.email{
		margin: 20px 0 20px 50px;
	}
	
	.conexao{
		margin: 20px 0 0 50px;
	}
	
	h2.tit{
		color: #2384C6;
		font-weight: bold;
		font: normal 14px Verdana, Lucida Grande, Trebuchet MS, Helvetica, sans-serif;
	}
	.right{
		float: right;
	}
	.right a img{
		text-decoration: none;
		border: none;
	}
	a:link{
		color: #000000;
		text-decoration: none;
	}
	a:hover{
		color: #F58220;
		text-decoration: none;
	}
	h1{
	  font: normal 16px Verdana, Lucida Grande, Trebuchet MS, Helvetica, sans-serif;
	  text-decoration: none;
	  font-weight: bold;
	  color: #F58220;
	  margin: 20px 0 0 48px;
	}
</style>

<h1>Contato</h1>

<div class="endereco">
	<h2 class="tit">Aspert</h2>
	<p>Visite nosso site: <a href="http://www.aspert.com.br" target="_blank">http://www.aspert.com.br</a></p>
</div>

<div class="telefone">
	<h2 class="tit">Atendimento por Telefone</h2>
	<p>+55 (11) 4349-0222</p>
	<p>De segunda a sexta, das 09:00 as 18:00</p>
</div>

<div class="email">
	<h2 class="tit">Atendimento por E-mail</h2>
	<p>suporte@aspert.com.br</p>
</div>
