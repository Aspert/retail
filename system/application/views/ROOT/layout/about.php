<style>
	body{
		outline: 0;
	}
	.endereco{
		margin: 0 0 0 50px;
	}
	.varejo{
		height: 350px;
		margin: 20px 0 0 50px;
	}
	h2.tit{
		font: 18px;
		color: #2384C6;
	}
	.right{
		float: right;
	}
	.right a img{
		text-decoration: none;
		border: none;
	}
	.hrcolor{
		color: #2384C6;
	}
	a{
		color: #2384C6;
		text-decoration: none;
	}
	
	.conexao{
		margin: 20px 0 0 0px;
	}
	
</style>
<h1>About</h1>
<div class="varejo">
	<h2 class="tit">Retail</h2>
	<p class="sep">Versão: 3.27.0</p>
	<p>Manual: <a href="<?= base_url() ?>files/help/manual_retail.pdf" target="_blank"> Clique aqui para baixar o manual</a></p>
  <p>Notas de atualizações:</p>
	<p>&nbsp; </p>
	
<h2 class="tit">Requisitos Mínimos</h2>
<table border="0">
	<tr>
		<td>Sistema Operacional: </td>
		<td>
			<div>
				<img src="<?=base_url().THEME?>img/windows-icon.jpg" title="Windows" alt="Windows">
				<img src="<?=base_url().THEME?>img/linux-icon.jpg" title="Linux" alt="Linux">
				<img src="<?=base_url().THEME?>img/mac-icon.jpg" title="Mac" alt="Mac">
			</div>					
		</td>
	</tr>
	<tr>
		<td>Navegador: </td>
		<td>
			<div>
				<img src="<?=base_url().THEME?>img/ie-icon.jpg" title="Internet Explorer" alt="Internet Explorer">
				<img src="<?=base_url().THEME?>img/firefox-icon.jpg" title="Firefox" alt="Firefox">
				<img src="<?=base_url().THEME?>img/safari-icon.jpg" title="Safari" alt="Safari">
				<img src="<?=base_url().THEME?>img/chrome-icon.jpg" title="Chrome" alt="Chrome">				
			</div>					
		</td>
	</tr>
	<tr>
		<td>Versão do Navegador: </td>
 		<td>
 			<table width="100%">
 				<tr>
 					<td width="25%" align="center">8</td>
 					<td width="25%" align="center">8</td>
 					<td width="25%" align="center">5</td>
 					<td width="25%" align="center">16</td>
 				</tr>
 			</table>
 		</td>
	</tr>
	<tr>
		<td>Java: </td>
		<td>
			<div> 6 </div>					
		</td>
	</tr>
	<tr>
		<td>Flash Player: </td>
		<td>
			<div> 10 </div>					
		</td>
	</tr>
</table>
<?php if($podeVer)	{ ?>
	<div class="conexao">
	<h2 class="tit">Teste de Conectividade</h2>
	<p><a href="<? echo site_url('conexao/index') ?>">Clique aqui para testar</a></p>
</div> <?php } ?>
</div>

<div class="endereco">
	<h2 class="tit">Aspert</h2>
	<p>Visite nosso site: <a href="http://www.aspert.com.br" target="_blank">http://www.aspert.com.br</a></p>
</div>

