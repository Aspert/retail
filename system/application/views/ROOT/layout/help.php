<style>
	body{
		outline: 0;
	}
	.endereco{
		margin: 20px 0 30px 50px;
	}
	.telefone{
		margin: 20px 0 30px 50px;
	}
	.email{
		margin: 20px 0 0 50px;
	}
	h2.tit{
		font: 18px;
		color: #2384C6;
	}
	.right{
		float: right;
	}
	.right a img{
		text-decoration: none;
		border: none;
	}
	.hrcolor{
		color: #2384C6;
	}
	a{
		color: #2384C6;
		text-decoration: none;
	}
		h1{
	  font: normal 16px Verdana, Lucida Grande, Trebuchet MS, Helvetica, sans-serif;
	  text-decoration: none;
	  font-weight: bold;
	  color: #F58220;
	  margin: 10px 0 10px 0;
	}
	ul{}
	ul li{
		margin: 10px 0 10px 0;
		text-align: justify;
	}
	
	#help_tags #modelo h1{
	  font: normal 16px Verdana, Lucida Grande, Trebuchet MS, Helvetica, sans-serif;
	  text-decoration: none;
	  font-weight: bold;
	  color: #F58220;
	  margin: 10px 0 10px 0;
	}

	
	
</style>
<h1>Help</h1>
<div id="help_tabs" style="width:97%;height:90%">
  <ul>
    <li><a href="#tab0">Cria&ccedil;&atilde;o de templates</a></li>
    <li><a href="#tab1">Tags de templates</a></li>
    <li><a href="#tab2">OPI</a></li>
    <li><a href="#tab_importacao">Importa&ccedil;&atilde;o</a></li>
    <li><a href="#tab3">Arquivos</a></li>
  </ul>
  <div id="tab0" style="min-height:250px; height:85%; overflow:auto">
    <h1>Instru&ccedil;&otilde;es</h1>
    <ul class="textos">
      <li>Ter em cada tag de texto apenas um (1) estilo de par&aacute;grafo. Apenas caixas de texto e imagem devem possuir tag.</li>
      <li>Todo template deve possuir um layer master e um layer <strong>ficha</strong>, contendo os elementos com tags.  
        Os nomes das tags do master devem ser as mesmas definidas no sistema retail (vide aba Tags de Template), caso contr&aacute;rio, as caixas n&atilde;o ser&atilde;o criadas no InDesign.</li>
      <li>N&atilde;o pode haver tags com nomes repetidos no layer master .     </li>
      <li>Os nomes dos links do template n&atilde;o podem conter caracteres especiais.</li>
      <li>A diagrama&ccedil;&atilde;o das caixas (fichas) deve ser feita no layer <strong>ficha</strong>. 
        Os nomes das tags da caixa devem ser as mesmas definidas no sistema retail , caso contr&aacute;rio o conte&uacute;do n&atilde;o ser&aacute; atualizado. 
        Depois de todo o layout feito, as fichas tem que ser agrupadas na ordem exata que ser&atilde;o preenchidas pelo sistema retail. 
        N&atilde;o pode haver tags com nomes repetidos nas caixas.    </li>
      <li>Se o diagramador excluir a imagem principal de uma ficha e executar o update, o plugin da uma mensagem que a ficha  n&atilde;o tem imagem principal e os updates desta ficha n&atilde;o ser&atilde;o executados. Faz altera&ccedil;&atilde;o, mas nenhuma inclus&atilde;o.</li>
      <li>Na planilha de excel os itens devem ser ordenados sequencialmente come&ccedil;ando pelo n&uacute;imero 1. Para cada p&aacute;gina a sequ&ecirc;ncia deve recome&ccedil;ar em 1 (ex.: p&aacute;gina 2, ordem 1,2,3,4&hellip;, p&aacute;gina 4, ordem 1,2,3,4&hellip;) 
        
        <br />
        <br />
      </li>
    </ul>
  </div>
  <div id="tab1" style="min-height:250px; height:85%; overflow:auto">
    
  <table width="80%" border="0" cellspacing="0" cellpadding="0">
  <?php if(!empty($_sessao['ID_AGENCIA'])): ?>
  <tr>
    <td width="45"><h1><strong>Ag&ecirc;ncia:</strong></h1></td>
    <td>&nbsp;</td>
    <td align="left">    	
			<h1><?php sessao_hidden('ID_AGENCIA','DESC_AGENCIA'); ?></h1>
    </td>
  </tr>
  <?php endif; ?>
  
  <tr>
    <td colspan="3">
	<?php if(empty($_sessao['ID_CLIENTE'])): ?>
        <br />
        	Selecione o cliente para visualizar as tags dispon&iacute;veis:
        <br />
    <?php endif; ?>
	</td>
    </tr>
  <tr>
  	<td><h1><strong>Cliente:</strong></h1></td>
  	<td></td>
  	<td align="left">
    	<?php if(!empty($_sessao['ID_CLIENTE'])): ?>
       		<h1><?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?></h1>
        <?php else: ?>
       
            <select name="ID_CLIENTE" id="HELP_ID_CLIENTE">
            <option value=""></option>
            <?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', !empty($busca['ID_CLIENTE']) ? $busca['ID_CLIENTE'] : ''); ?>
            </select>
        <?php endif; ?>
    </td>
  </tr>
</table>

    <div id="help_tags">
    	
<div id="modelo">
        	<h1></h1>
            <ul></ul>
        </div>
        
        <? if(count($tags) > 0):?>
			<? foreach($tags as $tagName=>$tag): ?>
            	<div>
                	<h1><strong><?= $tagName ?></strong></h1>
                    <ul>
					<? foreach($tag as $t): ?>
						<li><?= $t ?></li>
                    <? endforeach;?>
                	</ul>
                </div>
            <? endforeach;?>
    	<? endif; ?> 
    </div>

  </div>
  <div id="tab2" style="min-height:250px; height:85%; overflow:auto">
    <h1>Instru&ccedil;&otilde;es</h1>
    
    <ul>
      <li>      Para gerar o pdf para processamento de OPI sempre utilizar o preset do InDesign instalado pelo plugin do Webnative (Image Replacement).<br />
        <br />
      </li>
    </ul>
  </div>
  
  <div id="tab_importacao">
  <h1>Importa&ccedil;&atilde;o</h1>
	<ul>
		<li> Planilha deve estar na extens&atilde;o<strong> .xlsx</strong>;</li>
		<li>N&atilde;o pode conter c&eacute;lulas mescladas;</li>
		<li> Coluna c&oacute;digo de barras n&atilde;o pode estar vazia (sem preenchimento) ou com Texto;</li>
		<li>Se na planilha contiver um c&oacute;digo de barras que se repete e o mesmo n&atilde;o possui uma ficha cadastrada, o sistema informar&aacute; a aus&ecirc;ncia desta ficha todas as vezes que este item aparecer na planilha.</li>
		<li>Se a planilha contiver um c&oacute;digo de barras que se repete e este possui uma ficha cadastrada, o sistema informar&aacute; as posi&ccedil;&otilde;es em que esta ficha se repete;</li>
		<li>Todas as valida&ccedil;&otilde;es sempre s&atilde;o efetuadas pelo c&oacute;digo de barras.</li>
	</ul>
  <br />
  <h1>Convers&atilde;o Excel<br />
    <br />
  </h1>
	<table width="100%" border="0" cellspacing="1" cellpadding="2">
      <tr>
        <td width="5%"><div align="center"><img src="<?php echo base_url() . '/themes/snow_white/img/pdf_ico.png';?>" width="43" height="43" /></div></td>
        <td width="95%"><a href="<?php echo base_url().'templates/Pacote_de_compatibilidade_do_Office.pdf'; ?>" target="new">Como converter arquivos .xlsx para outras vers&otilde;es do Excel</a></td>
      </tr>
      <tr>
        <td><div align="center"><img src="<?php echo base_url() . '/themes/snow_white/img/web_ico.png';?>" width="43" height="43" /></div></td>
        <td><a href="http://www.microsoft.com/downloads/details.aspx?displaylang=pt-br&FamilyID=941b3470-3ae9-4aee-8f43-c6bb74cd1466" target="new">Download da ferramenta de convers&atilde;o de formatos anteriores.</a></td>
      </tr>
    </table>
	<ul>
		<p>&nbsp;</p>
	</ul>
	</div>
  
  <div id="tab3">
  	<h1>Arquivos</h1>
    <p><a href="<?php echo base_url().'templates/planilha_modelo.zip'; ?>">Clique aqui para baixar o modelo de planilha padrão (Excel)</a></p>
    <p>&nbsp;</p>
    <p><a href="http://retail.247id.com.br/plugin.zip">Clique aqui para baixar o plugin INDD</a></p>
    <p>&nbsp;</p>
    <p><a href="<?php echo base_url().'templates/Instrucoes_de_Instalacao_InDesign.pdf'; ?>">Clique aqui para baixar o Guia de instala&ccedil;&atilde;o do plugin INDD</a></p>
    <p>&nbsp;</p>
    <p><a href="<?php echo base_url().'templates/modelo_indd.zip'; ?>">Clique aqui para baixar o modelo de InDesign (INDD)</a></p>
    <!-- 
    <p>&nbsp;</p>
    <p><a href="<?php echo base_url().'templates/Definicao_Descricoes_Walmart.pdf'; ?>" target="new">Padr&otilde;es de descritivos Walmart</a></p>
    -->
  </div>
  
</div>
