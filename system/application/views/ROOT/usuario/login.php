<?php $this->load->view("ROOT/layout/header") ?>

<script type="text/javascript" src="<?=base_url()?>js/deployJava.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/checkPlugins.js"></script>		
<script type="text/javascript" src="<?=base_url()?>js/jquery_tooltip_home.js"></script>


<link href="<?=base_url().THEME?>css/tooltip.css" rel="stylesheet" type="text/css"/>


<div class='jqmWindow' id='modal'></div> 

<div id="contentGlobal">

<div id="contentLogin">

	<div id="pluginsDiv">
		<span class="tooltip">
			<span class="trigger_tooltip">
				<a href="#" onclick="displayMessagewithparameter('<?php echo site_url('usuario/requisitos'); ?>',500,500);"><img src="<?=base_url();?>img/plugin.gif"/></a>
			</span>
			<span id="msg" class="popup">Informações Importantes precisam de sua atenção!(Clique aqui).</span>
		</span>
	</div> <!-- Final de Tooltip -->
	<?php if(($alertActive && $alertAllowLogin) || !$alertActive): ?>
	<form style="margin-bottom:0px;" action="<?php echo site_url('usuario/do_login'); ?>" method="post" enctype="multipart/form-data" name='form'>
	<input type="hidden" name="action" value="2"/>
	
		<table width="342" border="0" cellpadding="0" cellspacing="0" class="Loginleft" style="margin:20px 0 0 36px;">
			<tr>
				<td width="163" align="left" class="textUser">Login</td>
				<td width="179" align="left" class="textPass">Senha</td>
			</tr>
			<tr>
				<td align="left"><input type="text" name="usuario" id="usuario" value="" title="Usu&aacute;rio"/></td>
				<td align="left"><input type="password" name="senha" id="senha" title="Senha"/></td>
			</tr>
			<tr>
				<td id="tdimage" align="right" class="textCard">Código do Cartão <?=$cordenada_cartao?></td>
					<?= form_hidden('cordenada_cartao',$cordenada_cartao) ?>
				<td align="left"><input type="password" name="val_cartao" id="codigo" title="C&oacute;digo do Cart&atilde;o" /></td>
			</tr>
			<tr>
				<td >&nbsp;</td>
				<td align="left" ><a class="button" href="javascript:document.form.submit();"><span>Logar no Sistema</span></a>
			    <input type="submit" name="vai" id="vai" value="Submit" style="display:none" /></td>
			</tr>
			<tr>
			<td colspan="2" align="center">
            
            <?php
			if(!empty($erros)) {
				$msgs = array();
				
				if( in_array(Sessao::LOGIN_VAZIO, $erros) ){
					$msgs[] = 'seu login';
				}
				
				if( in_array(Sessao::SENHA_VAZIA, $erros) ){
					$msgs[] = 'sua senha';
				}
				
				if( in_array(Sessao::CARTAO_VAZIO, $erros) ){
					$msgs[] = 'o código de seu cartão';
				}
				
				echo '<div style="color:red; margin-bottom:0px;">Faltou informar ' . implode(', ', $msgs) . '</div>';
			}
			?>
            
			<? if($this->uri->segment(3) == Sessao::CREDENCIAIS_INVALIDAS):?>
				<div Style="color:red; margin-bottom:0px;">Não foi possível realizar seu Login. Favor verificar dados informados</div>
			<? endif;?>
		  <? if($this->uri->segment(3) == Sessao::SESSAO_ENCERRADA):?>
				<div Style="color:red; margin-bottom:0px;" >
					Sess&atilde;o encerrada
				</div>
			<? endif;?>
			</td>
			</tr>
		</table>
	
	</form>
	<?php endif; ?>
</div> <!-- Final de Content Login -->

<div class="sombraLogin"></div>

	<?php if(($alertActive && $alertAllowLogin) || !$alertActive): ?>
	<div class="linkLogin" style="width: 414px; margin-left:340px; ">
		<div style="float: left">
			<p><a href="#" style="margin:0px;" onclick="telaAcessarSistema(); return false;" title="Não consigo acessar o sistema">Não consigo acessar o sistema</a>	</p>
		</div>
		
		<div style="float: right">
			<p><a style="margin:0px;" href="<?php echo site_url('usuario/login/test_page'); ?>">Verificar JavaScript e Cookies</a></p>
		</div>
		
		<div class="both">&nbsp;</div>
	</div>
	
	<?php endif; ?>

</div> <!-- Final de Content Global -->

<div id="recuperarSenha"></div>

<?php if(($alertActive && $alertAllowLogin) || !$alertActive): ?>
<script type="text/javascript">

var os_ok = false;
$j.each(plugins.minimo.os, function(key, val){
	if(val == plugins.usuario.os){
		os_ok = true;
	}
});

var browser_ok = false;
$j.each(plugins.minimo.browser, function(key, val){
	if(val == plugins.usuario.browser){
		browser_ok = true;
	}
});

var browserVersion_ok = false;
$j.each(plugins.minimo.browserVersion, function(key, val){
	if(val == plugins.usuario.browserVersion){
		browserVersion_ok = true;
	}
});

var java_ok = false;
if(plugins.minimo.java == plugins.usuario.java){
	java_ok = true;
}

var flash_ok = false;
if(plugins.usuario.flash >= plugins.minimo.flash){
	flash_ok = true;
}

var tudo_ok = new Array(os_ok,browser_ok,browserVersion_ok,java_ok,flash_ok);
if(tudo_ok){
	if(tudo_ok[0] == false || tudo_ok[1] == false || tudo_ok[2] == false || tudo_ok[3] == false || tudo_ok[4] == false){
		//aparece o alerta de plugin
	}else{
		$j('#pluginsDiv').html("&nbsp;");
	}
}

if(flash_ok == false){
	$j('#msg').toggleClass('popupVermelho');
	$j('#usuario').attr('disabled','true');
	$j('#senha').attr('disabled','true');
	$j('#codigo').attr('disabled','true');
	$j('#vai').attr('disabled','true');
}

function telaAcessarSistema(){
	
	$j('#recuperarSenha').html('Carregando...').dialog({
		modal: true,
		title: 'Não consigo acessar o sistema',
		width: 670,
		height: 600,
		autoOpen: false
	}).dialog('open');
	
	$j.post('<?php echo site_url('usuario/recuperar_senha') ?>', null, function(html){
		$j('#recuperarSenha').html( html ).find('#tabsRecuperarSenha').tabs();
	});
}

function resetarSenha(){
	$j.post($j('#resetarSenha').attr('action'), $j('#resetarSenha').serialize(), function(html){
		if( html == 'ok' ){
			alert('Senha alterada com sucesso.\nVerifique seu e-mail em alguns minutos.');
			$j('#recuperarSenha').dialog('close');
		} else {
			alert( html );
		}
	});
}

</script>
<?php endif; ?>


<?php $this->load->view("ROOT/layout/footer") ?>