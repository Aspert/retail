<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>24\7 inteligencia digital</title>
		
		<script type="text/javascript" src="<?= base_url(); ?>js/mootools.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>js/common.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>js/util.js"></script>	
		<script type="text/javascript" src="<?= base_url(); ?>js/modal-message.js"></script>
		
		<script type="text/javascript">
			messageObj = new DHTML_modalMessage();	// We only create one object of this class
			messageObj.setShadowOffset(5);	// Large shadow
					
			function displayMessage(url)
			{			
				messageObj.setSource(url);
				messageObj.setCssClassMessageBox(false);
				messageObj.setSize(800,500);
				messageObj.setShadowDivVisible(true);	// Enable shadow for these boxes
				messageObj.display();
			}
			
			function displayMessagewithparameter(url,width,height,oncomplete)
			{			
				messageObj.setSource(url);
				messageObj.setCssClassMessageBox(false);
				messageObj.setSize(width,height);
				messageObj.setShadowDivVisible(true);	// Enable shadow for these boxes
	
				if ( oncomplete != null ){
					messageObj.onComplete = oncomplete;
				}else{
					messageObj.onComplete = function(){};
				}
				
				messageObj.display();
			}
			
			function closeMessage(){
				messageObj.close();	
			}
			
			function recuperarSenha(){
				var er = [];
				if($('motivo').value == '') er.push('Informe o motivo');
				if($('nome_completo').value == '') er.push('Informe seu nome');
				if($('email').value == '') er.push('Informe o seu e-mail cadastrado no sistema');

				//console.log(er);debugger;
			
				if(er.length == 0){
					var data = {
						motivo: $('motivo').value,
						nome_completo: $('nome_completo').value,
						email: $('email').value,
						observacoes: $('observacoes').value
					};
					
					new Json.Remote("<?php echo site_url('usuario/recuperar_senha/json'); ?>", {
						onComplete: function(json) {
							alert('E-mail enviado com sucesso!');
							closeMessage();
						}
					}).send(data);
					
				} else {
					alert(er.join('\n'));
				}
			}
		</script>
		
		<link href="<?=base_url();?>css/modal-message.css" rel="stylesheet" type="text/css"/>
		
	</head>

	<body>
		<div id="all">
        <?$usuario = $this->session->userdata('usuario_session');?>
        <div id="logo">

            <div style="float: left;">
	            <img src="<?=base_url();?>img/logo.gif" border="0" />
	        </div>
        </div>


	<div class="area_login">
		<div id="login_img"></div>
		<div id="content_top_login"></div>
		<div id="content_middle_login">


