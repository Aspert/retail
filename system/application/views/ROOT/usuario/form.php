<?php $this->load->view("ROOT/layout/header") ?>
<style>
.lista-praca:hover    {
    background-color: #ccc;
}
</style>
<div id="contentGlobal">
<?php if ( !empty($erros) ) : ?>
	<ul style="color:#f00;font-weight:bold;" class="box_erros">
		<?php foreach ( $erros as $e ) : ?>
			<li><?=$e?></li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>
<h1>Cadastro de Usu&aacute;rios</h1>
<form action="<?php echo site_url('usuario/salvar/'.$this->uri->segment(3)); ?>" method="post" id="form1">
<table width="100%" border="0" class="tabela_pesquisa tabela_form tabela_cadastro">
  <tr>
  	<td colspan="2">Os campos agencia e cliente servem para facilitar a busca para o grupo. Um grupo pode estar relacionado somente a um cliente ou a uma ag&ecirc;ncia.<br />
  		No momento da edi&ccedil;&atilde;o de um usu&aacute;rio, estes campos estar&atilde;o em branco.</td>
  	</tr>
  <tr>
  	<td width="12%">Ag&ecirc;ncia</td>
  	<td width="88%"><select name="ID_AGENCIA" id="ID_AGENCIA" class="campoLongo">
  		<option value=""></option>
  		<?php echo montaOptions($agencias,'ID_AGENCIA','DESC_AGENCIA',post('ID_AGENCIA',true)); ?>
  		</select></td>
  	</tr>
  <tr>
    <td>Cliente</td>
    <td><select name="ID_CLIENTE" id="ID_CLIENTE" class="campoLongo">
      <option value=""></option>
      <?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE',post('ID_CLIENTE',true)); ?>
    </select></td>
  </tr>
  <tr>
  	<td colspan="2" scope="col">&nbsp;</td>
  	</tr>
  <tr>
  	<td scope="col">Grupo *</td>
  	<td scope="col"><select name="ID_GRUPO" id="ID_GRUPO" class="campoLongo">
  		<option value=""></option>
  		<?php echo montaOptions($grupos,'ID_GRUPO','DESC_GRUPO',post('ID_GRUPO',true)); ?>
  		</select></td>
  	</tr>
  <tr>
  	<td>Dom&iacute;nio *</td>
  	<td><input name="DOMINIO_USUARIO" type="text" class="campoLongo" value="<?php echo (!empty($_POST['DOMINIO_USUARIO']))?post('DOMINIO_USUARIO',true):'idigital'; ?>" /></td>
  	</tr>
  <tr>
    <td>Login *</td>
    <td><input name="LOGIN_USUARIO" type="text" class="campoLongo" value="<?php post('LOGIN_USUARIO'); ?>" /></td>
  </tr>
  <tr>
    <td>Nome *</td>
    <td><input name="NOME_USUARIO" type="text" class="campoLongo" value="<?php post('NOME_USUARIO'); ?>" /></td>
  </tr>
  <tr>
    <td>E-mail</td>
    <td><input name="EMAIL_USUARIO" type="text" class="campoLongo" value="<?php post('EMAIL_USUARIO'); ?>" /></td>
  </tr>
  <tr>
    <td>Status *</td>
    <td><input name="STATUS_USUARIO" type="radio" value="1" <?php echo post('STATUS_USUARIO',true)== 1 || empty($_POST['STATUS_USUARIO']) ? 'checked' : ''; ?> />
      Ativo
        <input name="STATUS_USUARIO" type="radio" value="0" <?php echo post('STATUS_USUARIO',true)=='0' ? 'checked' : ''; ?> />
    Inativo</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">
      <table width="100%" border="0" class="tabela_pesquisa tabela_form tabela_cadastro">
        <thead>
          <tr>
            <th><strong><center> Praça </center></strong></th>
            <th><strong><center> Seleção </center></strong></th>
          </tr>
        </thead>
        <tbody>
            <?php foreach($pracas as $p){?>
              <tr class="lista-praca">
                <td><center><?php echo $p['DESC_PRACA'] ?></center></td>
                <td><center><input type="checkbox" <?php if(in_array($p['ID_PRACA'],$pracaUsuario)){ echo "checked" ;} ?> name="lstPracas[]" value="<?php echo $p['ID_PRACA']?>"></center></td>
              </tr>
            <?php } ?>
        </tbody>
      </table>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
    <a class="button" href="<?php echo site_url('usuario/lista'); ?>" title="Pesquisa"><span>Cancelar</span></a>
    <a class="button" title="Pesquisa" onclick="$j(this).closest('form').submit()"><span>Gravar</span></a>
    
    </td>
  </tr>
</table>
</form>
</div>

<script type="text/javascript">
$('ID_AGENCIA').addEvent('change', function(){
	clearSelect($('ID_CLIENTE'), 1);
	clearSelect($('ID_GRUPO'), 1);
	montaOptionsAjax($('ID_CLIENTE'),'<?php echo site_url('json/admin/getClientesByAgencia'); ?>','id=' + this.value,'ID_CLIENTE','DESC_CLIENTE');
	montaOptionsAjax($('ID_GRUPO'),'<?php echo site_url('json/admin/getGruposByAgencia'); ?>','id=' + this.value,'ID_GRUPO','DESC_GRUPO');
	
});
$('ID_CLIENTE').addEvent('change', function(){
	clearSelect($('ID_GRUPO'), 1);
	montaOptionsAjax($('ID_GRUPO'),'<?php echo site_url('json/admin/getGruposByCliente'); ?>','id=' + this.value,'ID_GRUPO','DESC_GRUPO');
});
</script>


<?php $this->load->view("ROOT/layout/footer") ?>