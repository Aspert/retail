﻿<?php $this->load->view("ROOT/layout/header") ?>
<script type="text/javascript" src="<?=base_url()?>js/deployJava.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/checkPlugins.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/jquery_tooltip_home.js"></script>

<link href="<?=base_url().THEME?>css/tooltip.css" rel="stylesheet" type="text/css"/>

<div id="contentGlobal">

	<h1>Alterar senha</h1>
	
	<div class="box_erros">
		<p><strong>Sua senha deve conter:</strong></p>
		<p>&nbsp;</p>
		
		<ul>
			<?php
			foreach($mensagens as $item){
				echo '<li>', $item, '</li>', PHP_EOL;
			}
			?>
		</ul>
	</div>
		
	
	<form action="<?php echo site_url('usuario/trocar_senha/trocar'); ?>" method="post" id="form1">
		<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabela_padrao" style="padding-top:20px;">
			<tr>
				<td width="17%">Login de usu&aacute;rio</td>
				<td width="83%"><?php echo $_sessao['LOGIN_USUARIO']; ?></td>
			</tr>
			<tr>
				<td>Senha antiga</td>
				<td><input name="senha_antiga" type="password" id="senha_antiga" size="30" maxlength="32" /></td>
			</tr>
			<tr>
				<td>Senha nova</td>
				<td><input name="senha_nova" onKeyPress="return bloquearArroba(event.keyCode, event.which);" type="password" id="senha_nova" size="30" maxlength="32" /></td>
			</tr>
			<tr>
				<td>Confirme a senha nova</td>
				<td><input name="senha_nova_confirm" onKeyPress="return bloquearArroba(event.keyCode, event.which);" type="password" id="senha_nova_confirm" size="30" maxlength="32" /></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>
				<a href="#" onclick="$j('form').submit(); return false;" class="button"><span>Trocar Senha</span></a>
				<a href="<?php echo site_url('home'); ?>" class="button"><span>Cancelar</span></a>
				</td>
			</tr>
		</table>
	</form>
</div>

<script type="text/javascript">
$j(function($){
	$('form').submit(function(){
		var erros = [];
		
		if( $('#senha_antiga').val() == '' ) erros.push('Informe a senha antiga');
		if( $('#senha_nova').val() == '' ) erros.push('Informe a senha nova');
		if( $('#senha_nova_confirm').val() == '' ) erros.push('Informe a confirmação da senha nova');
		if( $('#senha_nova').val() != $('#senha_nova_confirm').val() ) erros.push('As senhas novas não são iguais');
		
		if( erros.length > 0 ){
			alert( erros.join('\n') );
		}
		
		return erros.length == 0;
	});
	
	<?php
	if( !empty($status) ){
		echo 'alert("'.$status.'");';
	}
	?>
	
});

//função para bloquear caracteres especiais no campo senha, pois estava dando problemas quando o usuário ia se logar no INDD
function bloquearArroba(ie, ff) {

	if (ie) {
        tecla = ie;
    } else {
        tecla = ff;
    }

	if ((tecla <= 64 && tecla >= 58) || (tecla >= 32 && tecla <= 47) || (tecla >= 91 && tecla <= 96) || (tecla >= 123 && tecla <= 126)) {
		alert("Não é permitido o uso de caracteres especiais no campo senha!");
		return false;
	}
	return true;
}
</script>

<?php $this->load->view("ROOT/layout/footer") ?>