<?php $this->load->view("ROOT/layout/header") ?>
<script type="text/javascript" src="<?=base_url()?>js/deployJava.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/checkPlugins.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/jquery_tooltip_home.js"></script>

<link href="<?=base_url().THEME?>css/tooltip.css" rel="stylesheet" type="text/css"/>

<div id="contentGlobal">

	<h1>Configura&ccedil;&atilde;o do browser: JavaScript e Cookies</h1>
	<p>Para poder utilizar o sistema, ative em seu browser o uso de JavaScript. Autorize tamb&eacute;m o uso de Cookies.<br />
		<br />
		Autoriza&ccedil;&atilde;o do uso de JavaScript: <span id="statusJS"><span style="color:red">INATIVO</span></span><br />
		Autoriza&ccedil;&atilde;o do uso de Cookies: <span id="statusCookies"><?php echo !isset($_COOKIE['PHPSESSID']) ? '<span style="color: red">INATIVO</span>' : '<span style="color: green">ATIVO</span>'; ?></span> <br />
		<br />
	Configure seu browser conforme as orienta&ccedil;&otilde;es abaixo.<br />
	<br />
	<strong>Mozilla Firefox (Windows)<br />
	<br />
	</strong>Na barra superior, escolha:<br />
	Ferramentas &gt; Op&ccedil;&otilde;es  &gt; Conte&uacute;do &gt; Permitir JavaScript<br />
	Ferramentas &gt; Op&ccedil;&otilde;es &gt; Privacidade &gt; Escolha &quot;O Firefox deve Utilizar Configura&ccedil;&otilde;es Personalizadas&quot; &gt; Sites podem definir Cookies<br />
	<br />
	<strong>Mozilla Firefox (MacOS)<br />
	<br />
	</strong>Na barra superior, escolha:<br />
	Firefox &gt; Prefer&ecirc;ncias   &gt; Conte&uacute;do &gt; Permitir JavaScript<br />
	Firefox &gt; Prefer&ecirc;ncias  &gt; Privacidade &gt; Escolha &quot;O Firefox deve Utilizar Configura&ccedil;&otilde;es Personalizadas&quot; &gt; Sites podem definir Cookies<br />
	<br />
	<strong>Safari (Windows)<br />
	<br />
	</strong>Na barra superior, escolha:<br />
	Editar &gt; Prefer&ecirc;ncias &gt; Seguran&ccedil;a &gt; Sempre aceitar Cookies<br />
	Editar &gt; Prefer&ecirc;ncias &gt; Seguran&ccedil;a &gt; Habilitar JavaScript e Java<br />
	<br />
	<strong>Safari (MacOS)<br />
	<br />
	</strong>Na barra superior, escolha:<br />
Safari &gt; Preferences &gt; Security &gt; Accept Cookies always<br />
Safari &gt; Preferences &gt; Security &gt; Enable JavaScript and Java<br />
	<br />
	<strong>Microsoft Internet Explorer (IE8)<br />
	<br />
	</strong>Na barra superior, escolha:<br />
	Ferramentas &gt; Op&ccedil;&otilde;es da Internet &gt; Seguran&ccedil;a &gt; Escolha  &quot;Internet&quot; e op&ccedil;&atilde;o &ldquo;M&eacute;dia&rdquo;<br />
	Ferramentas &gt; Op&ccedil;&otilde;es da Internet &gt; Privacidade &gt; Op&ccedil;&atilde;o &ldquo;M&eacute;dia&quot;<br />
	<br />
	<strong>Google Chorme (Windows)<br />
	<br />
	</strong>Na barra superior, escolha:<br />
	Personalizar e controlar o Google Chrome > Opções > Configurações avançadas > Privacidade > Configurações de conteúdo > JavaScript > Permitir que todos os sites executem JavaScript<br />
	Personalizar e controlar o Google Chrome > Opções > Configurações avançadas > Privacidade > Configurações de conteúdo > Cookies > Permitir a configuração de dados locais<br />
	<br />
	<strong>Google Chorme (MacOS)<br />
	<br />
	</strong>Na barra superior, escolha:<br />
	Customize and control Google Chrome > Preferences > Under the hood > Privacy > Content Setting > JavaScript > Allow all sites to run JavaScript<br />
	Customize and control Google Chrome > Preferences > Under the hood > Privacy > Content Setting > Cookies > Allow local data to be set for the current session only<br />
	<br />
	<a href="<?php echo site_url('usuario/login/test_page'); ?>">Ap&oacute;s alterar, clique aqui para validar as configura&ccedil;&otilde;es</a><br />
	<br />
	<a href="<?php echo site_url('usuario/login'); ?>">Voltar para a p&aacute;gina de login</a><br />
	<br />
	</p>
</div>

<script type="text/javascript">
$j(function(){
	$j('#statusJS').html('<span style="color: green">ATIVO</span>');
});
</script>

<?php $this->load->view("ROOT/layout/footer") ?>