<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
<h1>Usuários</h1>

<form id="pesquisa_simples" method="post" action="<?php echo site_url('usuario/lista'); ?>">
  <table width="100%" border="0">
    <tr>
      <th width="7%" scope="col">
        <a href="<?php echo site_url('usuario/form'); ?>" title="Adicionar Usuário"><img src="<?php echo base_url().THEME; ?>/img/file_add.png" width="31" height="31" alt="Adicionar" /></a>
        </th>
      <td width="39%" scope="col">Nome
        <input name="NOME_USUARIO" type="text" id="NOME_USUARIO" value="<?php echo val($busca,'NOME_USUARIO'); ?>" size="40" /></td>
      <td width="27%" scope="col">Itens por Página
        <select name="pagina" id="pagina">
          <?php
        $pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
        for($i=5; $i<=50; $i+=5){
            printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
        }
        ?>
        </select>
        <input name="tipo_pesquisa" type="hidden" id="tipo_pesquisa" value="simples" />
      </td>
      <td width="27%" align="center" scope="col">
        <a class="button" title="Pesquisa" onclick="$j(this).closest('form').submit()"><span>Pesquisa</span></a>
        <a class="button" title="Pesquisa Avan&ccedil;ada" onclick="$j(this).closest('form').hide(); $j('#pesquisa_completo').show();"><span>Pesquisa Avan&ccedil;ada</span></a>
        </td>
    </tr>
    </table>
</form>

<form id="pesquisa_completo" method="post" action="<?php echo site_url('usuario/lista'); ?>">
<h1>Pesquisa Avançada</h1>
  <table style="width:98%; padding-left: 4px;" border="0" align="center" class="tabela_pesquisa">
    <tr>
      <td width="14%" style="text-align: left" scope="col">Nome</td>
      <td width="86%" style="text-align: left" scope="col"><input name="NOME_USUARIO" type="text" id="NOME_USUARIO" value="<?php echo val($busca,'NOME_USUARIO'); ?>" size="40" /></td>
    </tr>
    <tr>
      <td style="text-align: left">Agência</td>
      <td style="text-align: left"><select name="ID_AGENCIA" id="ID_AGENCIA">
      <option value=""></option>
      <?php echo montaOptions($agencias, 'ID_AGENCIA','DESC_AGENCIA', val($busca,'ID_AGENCIA')); ?>
      </select></td>
    </tr>
    <tr>
      <td style="text-align: left">Cliente</td>
      <td style="text-align: left"><select name="ID_CLIENTE" id="ID_CLIENTE">
      <option value=""></option>
      <?php echo montaOptions($clientes, 'ID_CLIENTE','DESC_CLIENTE', val($busca,'ID_CLIENTE')); ?>
      </select></td>
    </tr>
    <tr>
      <td style="text-align: left">Grupo</td>
      <td style="text-align: left"><select name="ID_GRUPO" id="ID_GRUPO">
      <option value=""></option>
      <?php echo montaOptions($grupos, 'ID_GRUPO','DESC_GRUPO', val($busca,'ID_GRUPO')); ?>
      </select></td>
    </tr>
    <tr>
      <td style="text-align: left">Status</td>
      <td style="text-align: left">
      	<select name="STATUS_USUARIO" id="STATUS_USUARIO">
	        <option value=""></option>
	        <option value="1"<?php echo val($busca,'STATUS_USUARIO')=='1' ? ' selected="selected"': ''; ?>>Ativo</option>
	        <option value="0"<?php echo val($busca,'STATUS_USUARIO')=='0' ? ' selected="selected"': ''; ?>>Inativo</option>
      	</select>        
        <input name="tipo_pesquisa" type="hidden" id="tipo_pesquisa" value="completo" />
     </td>
    </tr>
    <tr>
     <td style="text-align: left">Itens por Página</td>
     <td style="text-align: left">
        <select name="pagina" id="pagina">
          <?php
        $pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
        for($i=5; $i<=50; $i+=5){
            printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
        }
        ?>
        </select>
        <input name="tipo_pesquisa" type="hidden" id="tipo_pesquisa" value="pesquisa_completo" />
      </td>
     </tr>
    <tr>
      <td style="text-align: left">&nbsp;</td>
      <td style="text-align: left">
      <a class="button" title="Pesquisa" onclick="$j(this).closest('form').submit()"><span>Pesquisa</span></a>
      <a class="button" title="Pesquisa Avançada" onclick="$j(this).closest('form').hide(); $j('#pesquisa_simples').show();"><span>Pesquisa Simples</span></a></td>
    </tr>
  </table>

</form>

<?php if ( (!empty($usuarios)) && is_array($usuarios) ):?>
	<div id="tableObjeto">
		<table width="100%" border="0" class="tableSelection">
			<thead>
            <tr>
            <?php if($podeAlterar): ?>
				<td>Editar</td>
            <?php endif; ?>
				<td>Nome</td>
				<td>Grupo</td>
				<td>Agência</td>
				<td>Cliente</td>
				<td>Status</td>
			</tr>
            </thead>
			<?foreach($usuarios as $us):?>
				<tr>
                <?php if($podeAlterar): ?>
					<td align="center">
                   <a href="<?php echo site_url('usuario/form/'.$us['ID_USUARIO']); ?>" title="Editar Usuário">
               	   <img src="<?php echo base_url() . THEME; ?>/img/file_edit.png" />
                   </a>
                    </td>
                <?php endif; ?>
					<td><?=$us['NOME_USUARIO']?></td>
					<td><?=$us['DESC_GRUPO']?></td>
					<td><?=$us['DESC_AGENCIA']?></td>
					<td><?=$us['DESC_CLIENTE']?></td>
					<td align="center"><?=($us['STATUS_USUARIO'] ==1 )?'Ativo':'Inativo'	?></td>
				</tr>
		<?endforeach;?>
		<tr>
	    	<td colspan="<?= 5 + (($podeAlterar) ? 1 : 0)?>" class="obj_encontrado">Total: <strong><?= $total ?></td>
	    </tr>
		</table>
        
            <br />Resultados encontrados: <?php echo $total; ?> <br />
        
		<span class="paginacao"><?=(isset($paginacao))?$paginacao:null?></span>
	</div>

<?else:?>
	Nenhum resultado
<?endif;?>

</div>

<script type="text/javascript">
$j(function(){
	var tipo = '<?php echo val($busca,'tipo_pesquisa'); ?>' == '' ? 'simples' : '<?php echo val($busca,'tipo_pesquisa'); ?>';
	if(tipo == 'simples'){
		$j('#pesquisa_completo').hide();
	} else {
		$j('#pesquisa_simples').hide();
	}
	
	$('ID_AGENCIA').addEvent('change', function(){
		clearSelect($('ID_CLIENTE'),1);
		montaOptionsAjax($('ID_CLIENTE'),'<?php echo site_url('json/admin/getClientesByAgencia'); ?>','id=' + this.value,'ID_CLIENTE','DESC_CLIENTE');
	});
	
	$('ID_CLIENTE').addEvent('change', function(){
		montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByCliente'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
	});
	
});
</script>

<?php $this->load->view("ROOT/layout/footer") ?>
