<?php $this->load->view("ROOT/layout/header"); ?>
<div id="contentGlobal">
  <form method="post" action="<?php echo site_url('permissao/salvar/'.$this->uri->segment(3)); ?>" id="form1" name="form1">
    <?php
	$editavel = $this->uri->segment(3) == '';
	?>
    <table width="100%" class="Alignleft">
      <tr>
        <td width="50%" valign="top" style="vertical-align:top;"><fieldset class="normal">
            <legend>
            <h1>Dados GACP</h1>
            </legend>
            <p>
              <label for="ID_GRUPO"><strong>Grupo:</strong></label> <?php echo $grupo['DESC_GRUPO']; ?></p>
            <p>
              <label for="ID_AGENCIA"><strong>Ag&ecirc;ncia:</strong></label> <?php echo $grupo['DESC_AGENCIA']; ?>
            </p>
            <p>
              <label for="ID_CLIENTE"><strong>Cliente:</strong></label> <?php echo $grupo['DESC_CLIENTE']; ?>
            </p>
            <p>&nbsp;</p>
          </fieldset></td>
        <td width="50%" valign="top"><fieldset class="normal">
            <legend>
            <h1>Permiss&otilde;es</h1>
            </legend>
            <?php foreach($controllers as $item): ?>
            <div id="controller_<?php echo $item['ID_CONTROLLER']; ?>">
              <div class="abaController" id="aba_<?php echo $item['ID_CONTROLLER']; ?>"><?php echo $item['DESC_CONTROLLER']; ?></div>
              <div class="functions" id="functions_<?php echo $item['ID_CONTROLLER']; ?>">
                <?php foreach($item['functions'] as $fnc): ?>
                <div class="function_item">
                  <input id="fnc_<?php echo $fnc['ID_FUNCTION']; ?>" class="fcnInput" name="functions[<?php echo $fnc['ID_FUNCTION']; ?>]" type="checkbox" value="1" <?php echo !empty($_POST['functions'][$fnc['ID_FUNCTION']]) ? 'checked="checked"' : ''; ?> />
                  <label for="fnc_<?php echo $fnc['ID_FUNCTION']; ?>"><?php echo $fnc['DESC_FUNCTION']; ?> (<?php echo $fnc['CHAVE_FUNCTION']; ?>)</label>
                </div>
                <?php endforeach; ?>
                <br />
                <input class="btnSelectAll" type="button" value="Selecionar Todos" />
                <input class="btnSelectNone" type="button" value="Remover Todos" />
              </div>
              <div class="both"></div>
            </div>
            <?php endforeach; ?>
          </fieldset></td>
      </tr>
      <tr>
        <td colspan="2" align="center" style="padding-left: 40%;">
        <a href="javascript:;" onclick="$('form1').submit();" class="button"><span>Salvar</span></a> <a href="<?php echo site_url('permissao/lista'); ?>" class="button"><span>Cancelar</span></a>
        </td>
      </tr>
    </table>
  </form>
</div>
<script type="text/javascript">
$$('.btnSelectAll').addEvent('click',function(){
	$each($(this.parentNode).getElements('input[type="checkbox"]'),function(el){
		el.checked = true;
	});
});
$$('.btnSelectNone').addEvent('click',function(){
	$each($(this.parentNode).getElements('input[type="checkbox"]'),function(el){
		el.checked = false;
	});
});


$$('.functions').setStyle('display','none');
$$('.abaController').each(function(el){
	$(el).addEvent('click', function(e){
		var aba = $(this);
		var id = aba.id.replace('aba_','');
		
		if( aba.hasClass('abaController') ){
			// fecha as outras abas
			$$('.functions').setStyle('display','none');
			$$('.abaControllerAberta').each(function(a){
				a.addClass('abaController').removeClass('abaControllerAberta');
			});
			
			// abre somente esta
			$('functions_'+id).setStyle('display','');
			aba.addClass('abaControllerAberta')
				.removeClass('abaController');
			
		} else {
			$('functions_'+id).setStyle('display','none');
			aba.addClass('abaController')
				.removeClass('abaControllerAberta');
		}
	});
});

<?php if($this->uri->segment(3) != ''): ?>
var valores = {};
$$('select').each(function(el){
	valores[ el.name ] = el.value;
	
	el.addEvent('change', function(){
		this.value = valores[ this.name ];
		alert('Você não pode alterar estes valores');
	});
});
<?php else: ?>
$('ID_AGENCIA').addEvent('change', function(){
	clearSelect($('ID_PRODUTO'), 1);
	montaOptionsAjax($('ID_CLIENTE'),'<?php echo site_url('json/admin/getClientesByAgencia'); ?>','id=' + this.value,'ID_CLIENTE','DESC_CLIENTE');
});
$('ID_CLIENTE').addEvent('change', function(){
	montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByCliente'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
});
<?php endif; ?>

</script>
<?php $this->load->view("ROOT/layout/footer") ?>
