<?php $this->load->view("ROOT/layout/header"); ?>

<div id="contentGlobal">

<h1>Permissões</h1>

<?php echo form_open('permissao/lista',array('id'=>'frm'));?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="pesquisa_simples">
  <tr>
    <td width="25%" style="text-align:left;">Ag&ecirc;ncia</td>
    <td width="23%" style="text-align:left;">Cliente</td>
    <td width="17%">Grupo</td>
    <td width="17%" style="text-align:left;">Itens por p&aacute;gina</td>
    <td width="18%" rowspan="2" style="text-align:right;">
    <a class="button" href="javascript:;" onclick="$j(this).closest('form').submit();"><span>Pesquisar</span></a></td>
  </tr>
  <tr>
  	<td style="text-align:left;">
  		<select name="ID_AGENCIA" id="ID_AGENCIA">
  			<option value=""></option>
  			<?php echo montaOptions($agencias,'ID_AGENCIA','DESC_AGENCIA', !empty($busca['ID_AGENCIA']) ? $busca['ID_AGENCIA'] : ''); ?>
  			</select>    </td>
    <td style="text-align:left;"><select name="ID_CLIENTE" id="ID_CLIENTE">
    	<option value=""></option>
    	<?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', !empty($busca['ID_CLIENTE']) ? $busca['ID_CLIENTE'] : ''); ?>
    	</select></td>
    <td>
	<select name="ID_GRUPO" id="ID_GRUPO">
    	<option value=""></option>
    	<?php echo montaOptions($lista_grupos['data'],'ID_GRUPO','DESC_GRUPO', !empty($busca['ID_GRUPO']) ? $busca['ID_GRUPO'] : ''); ?>
    	</select></td>
    <td style="text-align:left;"><span class="campo esq">
    	<select name="pagina" id="pagina" >
    		<?php
			$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
			for($i=5; $i<=50; $i+=5){
				printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
			}
			?>
    		</select>
    	</span></td>
  </tr>
</table>
<?php if(!empty($lista)): ?>
<br />

<div id="tableObjeto">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableSelection">
<thead>
  <tr>
  <?php if($podeAlterar): ?>
    <th width="6%">Editar</th>
  <?php endif; ?>
    <th width="38%">Grupo</th>
    <th width="28%">Ag&ecirc;ncia</th>
    <th width="28%">Cliente</th>
    </tr>
</thead>
  <?php foreach($lista as $item): ?>
  <tr>
   <?php if($podeAlterar): ?>
    <td align="center"><a href="<?php echo site_url('permissao/form/'.$item['ID_GRUPO']); ?>"><img src="<?php echo base_url().THEME; ?>img/file_edit.png" alt="Editar" /></a></td>
   <?php endif; ?>
    <td align="center"><?php echo $item['DESC_GRUPO']; ?></td>
    <td align="center"><?php echo $item['DESC_AGENCIA']; ?></td>
    <td align="center"><?php echo $item['DESC_CLIENTE']; ?></td>
    </tr>
  <?php endforeach; ?>
</table>
</div> <!-- Final de Table Objeto -->

<br />
<span class="paginacao"><?=(isset($paginacao))?$paginacao:null?></span>
<?php else: ?>
Nenhum resultado encontrado
<?php endif; ?>

<?php echo form_close();?>

<script type="text/javascript">
$('ID_AGENCIA').addEvent('change', function(){
	clearSelect($('ID_GRUPO'),1);
	$('ID_CLIENTE').value = '';
	montaOptionsAjax($('ID_GRUPO'),'<?php echo site_url('json/admin/getGruposByAgencia'); ?>','id=' + this.value,'ID_GRUPO','DESC_GRUPO');
});

$('ID_CLIENTE').addEvent('change', function(){
	clearSelect($('ID_GRUPO'),1);
	$('ID_AGENCIA').value = '';
	montaOptionsAjax($('ID_GRUPO'),'<?php echo site_url('json/admin/getGruposByCliente'); ?>','id=' + this.value,'ID_GRUPO','DESC_GRUPO');
});
</script>

</div> <!-- Final de Content Global -->

<?php $this->load->view("ROOT/layout/footer") ?>