<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
<h1>Plano de Marketing</h1>

<div id="pesquisa_simples">

<div id="">
	<?php echo form_open('plano_midia/index');?>
	<table width="100%" border="0" cellspacing="1" cellpadding="2">
  <tr>
    <?php if($podeAlterar): ?>
    <td width="5%" rowspan="2" style="text-align:center"><a title="Novo Plano de M&iacute;dia" href="<?php echo site_url('plano_midia/form'); ?>"><img src="<?php echo base_url().THEME ?>img/file_add.png" alt="Adicionar" /></a></td>
    <?php endif; ?>
    <?php if($podeImportar): ?>
    <td width="5%" rowspan="2" style="text-align:center"><a title="Importar Planilha" href="<?php echo site_url('importacao_excel/importar'); ?>"><img src="<?php echo base_url().THEME ?>img/file_edit.png" alt="Importar" /></a></td>
    <?php endif; ?>
	<?php if(!empty($_sessao['ID_AGENCIA']) && empty($_sessao['ID_CLIENTE'])): ?>
		<td width="15%" style="text-align:left">Ag&ecirc;ncia</td>
		<td style="text-align:left">Cliente</td>
	<?php elseif(empty($_sessao['ID_AGENCIA']) && !empty($_sessao['ID_CLIENTE'])): ?>
		<td width="15%" style="text-align:left">Cliente</td>
		<td style="text-align:left">Ag&ecirc;ncia</td>
	<?php endif; ?>
    <td width="15%" style="text-align:left">Bandeira</td>
    <td width="8%" style="text-align:left">Campanha</td>
    <td width="18%" style="text-align:left">T&iacute;tulo do Job</td>
    <td width="12%" style="text-align:left">Itens por p&aacute;gina</td>
    <td width="8%" rowspan="2" align="right"><a class="button" href="javascript:" title="Pesquisar" onclick="$j(this).closest('form').submit()"><span>Pesquisar</span></a></td>
  </tr>
  <tr>
	<?php if(!empty($_sessao['ID_AGENCIA']) && empty($_sessao['ID_CLIENTE'])): ?>
		<td style="text-align:left"><?php sessao_hidden('ID_AGENCIA','DESC_AGENCIA'); ?></td>
		<td style="text-align:left"><select name="ID_CLIENTE" id="ID_CLIENTE">
			<option value=""></option>
			<?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', !empty($busca['ID_CLIENTE']) ? $busca['ID_CLIENTE'] : ''); ?>
		</select></td>
	<?php elseif(empty($_sessao['ID_AGENCIA']) && !empty($_sessao['ID_CLIENTE'])): ?>
		<td style="text-align:left"><?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?></td>
		<td style="text-align:left"><select name="ID_AGENCIA" id="ID_AGENCIA">
			<option value=""></option>
			<?php echo montaOptions($agencias,'ID_AGENCIA','DESC_AGENCIA', !empty($busca['ID_AGENCIA']) ? $busca['ID_AGENCIA'] : ''); ?>
		</select></td>
	<?php endif; ?>
	
    <td style="text-align:left"><?php if(!empty($_sessao['ID_PRODUTO'])): ?>
      <?php sessao_hidden('ID_PRODUTO','DESC_PRODUTO'); ?>
      <?php else: ?>
      <select name="ID_PRODUTO" id="ID_PRODUTO" >
        <option value=""></option>
        <?php echo montaOptions($produtos,'ID_PRODUTO','DESC_PRODUTO', !empty($busca['ID_PRODUTO']) ? $busca['ID_PRODUTO'] : ''); ?>
      </select>
      <?php endif; ?></td>
    <td style="text-align:left"><select name="ID_CAMPANHA" id="ID_CAMPANHA" >
      <option value=""></option>
      <?php echo montaOptions($campanhas,'ID_CAMPANHA','DESC_CAMPANHA', !empty($busca['ID_CAMPANHA']) ? $busca['ID_CAMPANHA'] : ''); ?>
    </select></td>
    <td style="text-align:left"><input type="text" name="TITULO_JOB" id="TITULO_JOB" value="<?php echo !empty($busca['TITULO_JOB']) ? $busca['TITULO_JOB'] : '' ?>" /></td>
    <td style="text-align:left"><span class="campo esq">
      <select name="pagina" id="pagina">
        <?php
		$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
		for($i=5; $i<=50; $i+=5){
			printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
		}
		?>
      </select>
    </span></td>
    </tr>
</table>
</div> <!-- Final de Content Midia -->

  <?php echo form_close();?>
</div>
<?php if(count($lista) < 1) :?>
<div id="contentMidia">
<table border="0" width="100%" class="tableMidia">
<thead>
	<tr>
		<?php if($acoesLote){?>
			<th><input type="checkbox" value="" name="checkall" class="check_all" /></th>
		<?php } ?>
    <?php if($podeAlterar): ?>
			<th>Editar</th>
	  <?php endif; ?>
		<th class="">Hist&oacute;rico</th>
		<th class="numeroJob">N&uacute;mero Job</th>
		<th class="tituloJob">T&iacute;tulo Job</th>
		<th class="inicioJob">In&iacute;cio  da Validade</th>
		<th align="left" class="terminoJob">Final da Validade</th>
		<th align="left" class="agenciaJob">Ag&ecirc;ncia</th>
		<th align="left" class="clienteJob">Cliente</th>
		<th align="left" class="bandeiraJob">Bandeira</th>
		<th align="left" class="campanhaJob">Campanha</th>
		<th align="left" class="mudancaJob">Mudan&ccedil;a Etapa</th>
		<th align="left" class="situacaoJob">Situa&ccedil;&atilde;o</th>
		<!--<th align="left" class="tableSelection">&nbsp;</th> -->
	  </tr>
  </thead>
  </table>
  </div>
  
<?php elseif((isset($lista))&& (is_array($lista))):?>

<div id="contentMidia">
<table border="0" width="100%" class="tableMidia">
<thead>
	<tr>
		<?php if($acoesLote){?>
			<th><input type="checkbox" value="" name="checkall" class="check_all" /></th>
		<?php } ?>
    <?php if($podeAlterar): ?>
			<th>Editar</th>
	  <?php endif; ?>
		<th class="">Hist&oacute;rico</th>
		<th class="numeroJob">N&uacute;mero Job</th>
		<th class="tituloJob">T&iacute;tulo Job</th>
		<th class="inicioJob">In&iacute;cio  da Validade</th>
		<th align="left" class="terminoJob">Final da Validade</th>
		<th align="left" class="agenciaJob">Ag&ecirc;ncia</th>
		<th align="left" class="clienteJob">Cliente</th>
		<th align="left" class="bandeiraJob">Bandeira</th>
		<th align="left" class="campanhaJob">Campanha</th>
		<th align="left" class="mudancaJob">Mudan&ccedil;a Etapa</th>
		<th align="left" class="situacaoJob">Situa&ccedil;&atilde;o</th>
		<!--<th align="left" class="tableSelection">&nbsp;</th> -->
	  </tr>
  </thead>
		<?php $cont=0; ?>
		<?php foreach($lista as $item):?>

		<tr>
			<?php if($acoesLote){?>
				<td><input type="checkbox" class="selecao" name="selecao" value="<?php echo $item['ID_JOB'] ?>"></td>
			<?php } ?>
			<?php if($podeAlterar): ?>
            <td align="center">
				<?php echo anchor('plano_midia/form/'. $item['ID_JOB'], '<img src="'.base_url().THEME.'img/file_edit.png" border="0"/>',array('title'=>'Editar'));?>
			</td>
            <?php endif; ?>
            <td align="center"><a href="javascript:void(0);" onclick="Ficha.HistoricoJob(<?php echo $item['ID_JOB']; ?>)"><img src="<?=base_url().THEME?>img/visualizar_historico.png" border="0"/></a></td>
            <td align="center" class="alter"><?=$item['ID_JOB'];?></td>
            <td align="center" class="alter"><?=$item['TITULO_JOB'];?></td>
            <td align="center" class="alter"><?=format_date_to_form($item['DATA_INICIO']);?></td>
            <td class="alter"><?=format_date_to_form($item['DATA_TERMINO']);?>
            	<br />
            	<?= ($diff = strtotime($item['DATA_TERMINO']) - time()) > 0 ? segundosToHora($diff) : '&nbsp;';?></td>
            <td class="alter"><?=$item['DESC_AGENCIA'];?></td>
            <td class="alter"><?=$item['DESC_CLIENTE'];?></td>
            <td class="alter"><?=$item['DESC_PRODUTO'];?></td>
            <td class="alter"><?=$item['DESC_CAMPANHA'];?></td>
            <td class="alter"><?=date('d/m/Y H:i',strtotime($item['DATA_MUDANCA_ETAPA']));?>
              <br /></td>
            <td class="alter"><?=$item['DESC_STATUS'] . getRevisaoJob($item);?></td>
            <!-- <td class="alter"><?php echo sinalizarAlteracoes($item); ?></td> -->
			<?php $cont++; ?>
		</tr>
		<?endforeach;?>
		<?php if($acoesLote){?>
			<tr>
				<td align="left" colspan="14"> 
					<a class="button" onclick="ProxEtapa();" id="ProximaEtapa"><span>Próxima Etapa</span></a>
					<a class="button" onclick="Finalizar();" id="FinalizarEtapa"><span>Finalizar</span></a>
					<a class="button" onclick="Excluir();" id="ExcluirEtapa"><span>Excluir</span></a>
				</td>
			</tr>
		<?php } ?>
</table>


<span class="paginacao"><?=(isset($paginacao))?$paginacao:null?></span>
<?else:?>
	Nenhum resultado
<?endif;?>
<script type="text/javascript" src="<?= base_url(); ?>js/jquery.js"></script>
<script>
$j('.check_all').click(function(){
	$j('.selecao').attr('checked',this.checked);
});

function Excluir(){
	
	var jobs = [];
	var listaMarcados = document.getElementsByClassName("selecao");
  for (loop = 0; loop < listaMarcados.length; loop++) {
     var item = listaMarcados[loop];
		 if((item.type == "checkbox") && (item.checked)){
			 jobs.push(item.value);
		 }
  } 
	if(jobs.length > 0){
		
		var resposta = confirm("Deseja Realmente apagar "+ jobs.length+" jobs?");
		if(resposta){
			document.getElementById("ExcluirEtapa").setAttribute("onclick", "");
			$.ajax({
        type: 'POST',
        url: '<?php echo site_url('checklist/delete'); ?>',
        data: { 
          idJob : jobs,
        },
        cache: false,
        async: true,
        success: function (data) {
          alert(data);
					location.reload();
        },
        fail: function () {
      
        }
      });
		}
	}
	//Apaga
	
}

function ProxEtapa(){
	var jobs = [];
	var listaMarcados = document.getElementsByClassName("selecao");
  for (loop = 0; loop < listaMarcados.length; loop++) {
     var item = listaMarcados[loop];
		 if((item.type == "checkbox") && (item.checked)){
			 jobs.push(item.value);
		 }
  } 
	if(jobs.length > 0){
		
		var resposta = confirm("Deseja Realmente mudar "+ jobs.length+" jobs para a Próxima Etapa?");
		if(resposta){
			document.getElementById("ProximaEtapa").setAttribute("onclick", "");
			$.ajax({
        type: 'POST',
        url: '<?php echo site_url('checklist/enviar_etapa'); ?>',
        data: { 
          idJob : jobs,
        },
        cache: false,
        async: true,
        success: function (data) {
          alert(data);
					location.reload();
        },
        fail: function () {
      
        }
      });
		}
	}
	//Apaga
	
}

function Finalizar(){
	var jobs = [];
	var listaMarcados = document.getElementsByClassName("selecao");
  for (loop = 0; loop < listaMarcados.length; loop++) {
     var item = listaMarcados[loop];
		 if((item.type == "checkbox") && (item.checked)){
			 jobs.push(item.value);
		 }
  } 
	if(jobs.length > 0){
		
		var resposta = confirm("Deseja Realmente Finalizar "+ jobs.length+" jobs?");
		if(resposta){
			document.getElementById("FinalizarEtapa").setAttribute("onclick", "");
			$.ajax({
        type: 'POST',
        url: '<?php echo site_url('checklist/finalizar'); ?>',
        data: { 
          idJob : jobs,
        },
        cache: false,
        async: true,
        success: function (data) {
          alert(data);
					location.reload();
        },
        fail: function () {
      
        }
      });
		}
	}
	//Apaga
}

<?php if(!empty($_sessao['ID_CLIENTE'])): ?>
$('ID_AGENCIA').addEvent('change', function(){
	clearSelect($('ID_CAMPANHA'), 1);
	clearSelect($('ID_PRODUTO'), 1);
	montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByAgencia'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
});
<?php endif; ?>

<?php if(!empty($_sessao['ID_AGENCIA'])): ?>
$('ID_CLIENTE').addEvent('change', function(){
	clearSelect($('ID_CAMPANHA'), 1);
	clearSelect($('ID_PRODUTO'),1);
	montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByCliente'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
});
<?php endif; ?>

$('ID_PRODUTO').addEvent('change', function(){
	clearSelect($('ID_CAMPANHA'), 1);
	montaOptionsAjax($('ID_CAMPANHA'),'<?php echo site_url('json/admin/getCampanhasByProduto'); ?>','id=' + this.value,'ID_CAMPANHA','DESC_CAMPANHA');
});

$j(function(){
   configuraPauta('<?php echo $busca['ORDER']; ?>', '<?php echo $busca['ORDER_DIRECTION']; ?>');
});
</script>

</div> <!-- Final de content Midia -->

<?php $this->load->view("ROOT/layout/footer") ?>