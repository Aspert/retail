<script type="text/javascript" src="<?= base_url(); ?>js/jquery.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/jquery.pager.js"></script>

<div id="legenda"></div>
<div class="rodape_tabela" id="mostrar">
	Itens por página
	<select id="cbMostrar" onchange="criarEspelhos()">
		<option value="1">1</option>
		<option value="5">5</option>
		<option value="10">10</option>
		<option value="15">15</option>
		<option value="20">20</option>
	</select>
</div>
<div id="espelho" style="margin-top: 10px"></div>

<script type="text/javascript">
	var catColors = eval('[<?php echo($strCores); ?>]');
	var catNames = eval('[<?php echo($strCategoria); ?>]');
	catColors = catColors[0];
	catNames = catNames[0];
	
	function criarEspelhos(){
		$('#espelho').html('');
		
		var prods = eval('[<?php echo($strEspelho); ?>]');
		prods = prods[0];
		
		var amostra = $("#cbMostrar").val();
		var strEspelhos = '';
		
		var intContador = 0;
		for ( var i in prods ) {
			if( (intContador%amostra) == 0 ){
				strEspelhos += '<div>';
			}
			strEspelhos += '<div class="divPraca">';
				for( var j in prods[i] ){
					for ( var k = 0; k < prods[i][j]; k++ ){
						strEspelhos += '<div class="divProduto" style="background-color:'+catColors[j]+';" title="'+catNames[j]+'">&nbsp;</div>';
					}
				}
			strEspelhos += '<div title="Página '+(intContador+1) + '" style=" margin-bottom:0px; margin-top:247px; ">Página '+(intContador+1) + '</div></div>';
			if( ((intContador+1)%amostra) == 0 ){
				strEspelhos += '</div>';
			}
			intContador++;
		}

		$('#espelho').html(strEspelhos);
		$('#espelho').pager('div', {'prevText':'Anterior ', 'nextText':' Próximo', 'highlightClass':'med','navClass': 'rodape_tabela', 'navAppend': '<br/><div id="separa" style="clear:both"></div>', 'notClas': '#separa'});
		//$('.divPraca').css('width', (100/amostra)+'%');
	}
	
	function criarLegenda(){
		$('#legenda').html('');
		var strLegendas = '';
		for( var j in catNames ){
			strLegendas += '<div class="divProduto" style="background-color:'+catColors[j]+'"></div>'+catNames[j]+'<br/><div style="clear:both"></div>';
		}
		$('#legenda').html(strLegendas);
	}

	criarLegenda();
	criarEspelhos();

</script>