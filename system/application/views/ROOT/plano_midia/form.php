<?php $this->load->view("ROOT/layout/header"); ?>

<style type="text/css">
    .divPraca {
        width:220px;
        height:260px;
        float:left;
        border:solid #999 1px;
    }
    .divProduto {
        width:16px;
        height:16px;
        margin:3px 3px 3px 3px;
        float:left;
    }
</style>
<div id="contentGlobal">
    <h1>Plano de Marketing</h1>

    <?php if ( isset($erros) && is_array($erros) ) : ?>
        <ul style="color:#ff0000;font-weight:bold;" class="box_erros">
            <?php foreach ( $erros as $e ) : ?>
                <li><?=$e?></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
    <form id="formImportacao" action="<?php echo site_url('plano_midia/save/'. $this->uri->segment(3)); ?>" method="post" enctype="multipart/form-data">
        <div id="tabs">

            <input type="hidden" name="ID_JOB" type="text" id="ID_JOB"  value="<?php echo $id ?>" />
            <ul id="tabs_lista">
                <li><a href="#job">Dados do Job</a></li>
                <?php
                // print_rr($id);die;
                if(!empty($pracas)):
                    foreach($pracas as $item) :
                        if(in_array($item['ID_PRACA'], $selecionadas)):
                            ?>
                            <li><a href="#tab-<?php echo $item['ID_PRACA']; ?>"><?php echo $item['DESC_PRACA']; ?></a></li>
                            <?
                        endif;
                    endforeach;
                endif;

                if( $this->uri->segment(3) != '' && $podeVerCronograma ){
                    echo '<li><a href="#cronograma">Cronograma</a></li>';
                }
                ?>
            </ul>

            <div id="job">
                <div id="contentFormFicha">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableFormFicha">
                        <?php if(empty($_sessao['ID_CLIENTE']) && !empty($_sessao['ID_AGENCIA'])) : ?>
                            <tr>
                                <td width="188">Ag&ecirc;ncia *</td>
                                <td width="871">
                                    <?php sessao_hidden('ID_AGENCIA','DESC_AGENCIA'); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Cliente *</td>
                                <td>
                                    <select name="ID_CLIENTE" id="ID_CLIENTE">
                                        <option value=""></option>
                                        <?php echo montaOptions($clientes, 'ID_CLIENTE','DESC_CLIENTE', post('ID_CLIENTE',true),true); ?>
                                    </select>
                                </td>
                            </tr>
                        <?php elseif(empty($_sessao['ID_AGENCIA']) && !empty($_sessao['ID_CLIENTE'])): ?>
                            <tr>
                                <td>Cliente *</td>
                                <td>
                                    <?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?></td>
                            </tr>
                            <tr>
                                <td width="188">Ag&ecirc;ncia *</td>
                                <td width="871">
                                    <select name="ID_AGENCIA" id="ID_AGENCIA">
                                        <option value=""></option>
                                            <?php
                                                
                                             echo montaOptions($agencias, 'ID_AGENCIA','DESC_AGENCIA', post('ID_AGENCIA',true),true); 
                                             ?>
                                    </select>
                                </td>
                            </tr>
                        <?php endif; ?>

                        <tr>
                            <td>Bandeira *</td>
                            <td><?php if(!empty($_sessao['ID_PRODUTO'])): ?>
                                    <?php sessao_hidden('ID_PRODUTO','DESC_PRODUTO'); ?>
                                <?php else: ?>
                                    <select name="ID_PRODUTO" id="ID_PRODUTO">
                                        <option value=""></option>
                                        <?php echo montaOptions($produtos, 'ID_PRODUTO','DESC_PRODUTO', post('ID_PRODUTO',true),true); ?>
                                    </select>
                                <?php endif; ?></td>
                        </tr>
                        <tr>
                            <td>Regi&atilde;o *</td>
                            <td><select name="ID_REGIAO" id="ID_REGIAO">
                                    <option value=""></option>
                                    <?php echo montaOptions($regioes, 'ID_REGIAO','NOME_REGIAO', post('ID_REGIAO',true),true); ?>
                                </select></td>
                        </tr>
                        <tr>
                            <td>Campanha *</td>
                            <td><select name="ID_CAMPANHA" id="ID_CAMPANHA">
                                    <option value=""></option>
                                    <?php echo montaOptions($campanhas, 'ID_CAMPANHA','DESC_CAMPANHA', post('ID_CAMPANHA',true),true); ?>
                                </select></td>
                        </tr>

                        <tr>
                            <td>Tipo de pe&ccedil;a *</td>
                            <td><select name="ID_TIPO_PECA" id="ID_TIPO_PECA">
                                    <option value=""></option>
                                    <?php echo montaOptions($tipos, 'ID_TIPO_PECA','DESC_TIPO_PECA', post('ID_TIPO_PECA',true),true); ?>
                                </select></td>
                        </tr>
                        <tr>
                            <td>Tipo de Job *</td>
                            <td><select name="ID_TIPO_JOB" id="ID_TIPO_JOB">
                                    <option value=""></option>
                                    <?php echo montaOptions($tiposJob, 'ID_TIPO_JOB','DESC_TIPO_JOB', post('ID_TIPO_JOB',true),true); ?>
                                </select></td>
                        </tr>
                        <tr>
                            <td>T&iacute;tulo  *</td>
                            <td>
                                <span class="TEXTO_EXPLICATIVO" style="display:block;"></span>
                                <input name="TITULO_JOB" type="text" id="TITULO_JOB" size="50" maxlength="150" value="<?php post('TITULO_JOB'); ?>" />

                            </td>
                        </tr>
                        <?php if(!$_sessao['IS_HERMES']){?>
                            <tr>
                                <td>Formato *</td>
                                <td><input class="centimetragem" name="LARGURA_JOB" type="text" id="LARGURA_JOB" size="8" maxlength="7" value="<?php post('LARGURA_JOB'); ?>" />
                                    X
                                    <input class="centimetragem" name="ALTURA_JOB" type="text" id="ALTURA_JOB" size="8" maxlength="7" value="<?php post('ALTURA_JOB'); ?>" /></td>
                            </tr>
                        <?php }?>
                        <tr>
                            <td>Numero de P&aacute;ginas *</td>
                            <td><input name="PAGINAS_JOB" type="text" id="PAGINAS_JOB" size="5" maxlength="5" value="<?php post('PAGINAS_JOB'); ?>" /></td>
                        </tr>
                        <tr>
                            <td>Data de in&iacute;cio do Processo *</td>
                            <td><input name="DATA_INICIO_PROCESSO" type="text" id="DATA_INICIO_PROCESSO" size="10" maxlength="10" class="_calendar" value="<?php post('DATA_INICIO_PROCESSO'); ?>" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <td>In&iacute;cio  da Validade *</td>
                            <td><input name="DATA_INICIO" type="text" id="DATA_INICIO" size="10" maxlength="10" value="<?php post('DATA_INICIO'); ?>" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <td>Final da Validade *</td>
                            <td>
                                <?=form_input(array('id'=>'DATA_TERMINO','name'=>'DATA_TERMINO', 'class'=>'_calendar','size'=>'10', 'value'=>post('DATA_TERMINO',true),'readonly'=>'readonly'));?>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">Observa&ccedil;&otilde;es</td>
                            <td><textarea name="OBSERVACOES_EXCEL" cols="50" rows="6" id="OBSERVACOES_EXCEL"><?php post('OBSERVACOES_EXCEL'); ?></textarea></td>
                        </tr>
                        <?php if($abrir_multiplos){?>
                            <tr>
                                <td valign="top">Abrir Múltiplos</td>
                                <td><input type="checkbox" name="ABRIR_MULTIPLOS" value="S"></td>
                            </tr>
                        <?php }?>


                        <tr>
                            <td valign="top">Selecione as pra&ccedil;as *</td>
                            <td><select name="pracas[]" id="pracas" style="width:100%; height:100px;" multiple="multiple">
                                    <?php
                                    foreach($pracas as $item){
                                        printf('<option value="%d"%s>%s</option>'.PHP_EOL,
                                            $item['ID_PRACA'],
                                            in_array($item['ID_PRACA'], $selecionadas) ? ' selected="selected"' : '',
                                            $item['DESC_PRACA']
                                        );
                                    }
                                    ?>
                                </select></td>
                        </tr>
                        <tr>
                            <td valign="top">Situa&ccedil;&atilde;o</td>
                            <td><select name="STATUS_JOB">
                                    <option value="1">ATIVO</option>
                                    <option value="0"<?php echo isset($_POST['STATUS_JOB']) && $_POST['STATUS_JOB'] == '0' ? ' selected="selected"' : ''; ?>>INATIVO</option>
                                </select></td>
                        </tr>
                        <tr>
                            <?php if($defineUsuarios): ?>
                                <td>Responsáveis pelo Job</td>
                                <td>
                                    <select name="ID_USUARIO_RESPONSAVEL[]" id="ID_USUARIO_RESPONSAVEL" style="width:100%; height:100px;" multiple="multiple">
                                        <option value=""></option>
                                        <?php
                                        foreach($usuarios as $usuario){
                                            printf('<option value="%d"%s>%s</option>'.PHP_EOL,
                                                $usuario['ID_USUARIO'],
                                                in_array($usuario['ID_USUARIO'], $usuariosSelecionados) ? ' selected="selected"' : '',
                                                $usuario['NOME_USUARIO']
                                            );
                                        }
                                        ?>
                                    </select>
                                </td>
                            <?php /*else: */?>
                                <!--<td>Responsável pelo Job</td>
                                <td>
                                    <select name="ID_USUARIO_RESPONSAVEL" id="ID_USUARIO_RESPONSAVEL">
                                        <option value=""></option>
                                    </select>
                                </td>-->
                            <?php endif; ?>
                        </tr>
                    </table>

                </div> <!-- Final de Content Form Ficha -->
            </div>

            <?php if(!empty($pracas) && $this->uri->segment(3) != ''): ?>

                <?php foreach($pracas as $item): ?>

                    <?php if(in_array($item['ID_PRACA'], $selecionadas)): ?>

                        <div id="tab-<?php echo $item['ID_PRACA']; ?>" class="praca">
                            <span style="display:none" class="titulo"><?php echo $item['DESC_PRACA']; ?></span>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                Adicionar <?php ghDigitalReplace($_sessao, 'Subcategoria'); ?>
                                <select id="comboCategorias-<?php echo $item['ID_PRACA']; ?>" class="addItemEspelho">
                                    <option value=""></option>
                                    <?php foreach($categorias as $categoria): ?>
                                        <optgroup label="<?php echo $categoria['DESC_CATEGORIA']; ?>">
                                            <?php foreach($categoria['subcategorias'] as $cat): ?>
                                                <option value="<?php echo $cat['ID_SUBCATEGORIA'];?>"><?php echo $cat['DESC_SUBCATEGORIA'];?></option>
                                            <?php endforeach;?>
                                        </optgroup>
                                    <?php endforeach;?>
                                </select>

                                <a href="#" onclick="adicionarEspelhoItemHandler(this); return false;"> <img src="<?php echo base_url().THEME.'img/add.png'; ?>" alt="Adiciona uma <?php ghDigitalReplace($_sessao, 'subcategoria'); ?> ao espelho" class="information" /> </a>

                                <table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabela_padrao">
                                    <thead>
                                    <tr>
                                        <th width="30%"><?php ghDigitalReplace($_sessao, 'Categoria'); ?></th>
                                        <th width="30%"><?php ghDigitalReplace($_sessao, 'Sub Categoria'); ?></th>
                                        <th width="20%">Quantidade</th>
                                        <th width="20%">Página</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody id="itens-<?php echo $item['ID_PRACA']; ?>">

                                    </tbody>
                                </table>

                            </table>

                            <div style="border:1px solid #D3D3D3; background-color:#EDECEC; padding:10px; float:left; width:1070px; margin:10px 0 0 -1px;">
                                <div style="float: left;">
                                    Copiar valores de
                                    <select class="copiar" name="copiar_<?php echo $item['ID_PRACA']; ?>" id="copiar_<?php echo $item['ID_PRACA']; ?>">
                                        <option value=""></option>
                                        <?php
                                        echo montaOptions($pracasSelecionadas,'ID_PRACA','DESC_PRACA',true);
                                        ?>
                                    </select>
                                </div>
                                <div style="float: left;padding-left: 60px;">
                                    <a class="button" onClick="replicarEspelho('<?php echo $item['ID_PRACA']; ?>', '<?php echo $item['DESC_PRACA']; ?>')"><span>Replicar Espelho</span></a>
                                </div>
                                <div style="float: right;">
                                    Visualizar espelho:
                                    <a href="#" onclick="if(validaFormulario(null, <?php echo $item['ID_PRACA']; ?>)){mostrarEspelho(<?php echo $item['ID_PRACA']; ?>);}" style="display: inline-block;"><img style="vertical-align: middle" src="<?php echo base_url().THEME; ?>img/visualizar_espelho.png"/></a>
                                </div>
                            </div> <!-- Final de Div Copiar de -->

                            <div class="clearBoth"></div>
                        </div>

                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>

            <?php
            if( $this->uri->segment(3) != '' && $podeVerCronograma ){
                echo '<div id="cronograma">
			<p>
				Caso tenha alterado as datas no cadastro do plano de marketing, salve o 
				job para exibir a imagem atualizada.
			</p>
			<p>&nbsp;</p>
			
				<img src="', site_url('json/processo/getGraficoCronograma/'.$this->uri->segment(3).'/planejado/1090/300/0/22'), '" />
			</div>';
            }
            ?>
        </div>

        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td width="50%" align="right" valign="bottom">
                    <a class="button" href="<?php echo site_url('plano_midia'); ?>"><span>Cancelar</span></a>
                    <?php if($podeSalvar): ?>
                        <a class="button" href="javascript:" onclick="$j(this).closest('form').submit();"><span>Salvar</span></a>
                    <?php endif; ?>
                    <?php if($podePassarEtapa && $this->uri->segment(3) != '' && !empty($selecionadas)): ?>
                        <a class="button" href="javascript:" onclick="proximaEtapa()"><span>Pr&oacute;xima Etapa</span></a>
                    <?php endif; ?>
                    <?php if($podeReaproveitar && isset($id) && is_numeric($id)): ?>
                        <a class="button" href="javascript:" onclick="reaproveitarJob(<?php $id?>)"><span>Aproveitar Job</span></a>
                    <?php endif; ?>
                </td>
                <td width="6%" align="left" valign="bottom"></td>
                <td width="44%" align="left" valign="bottom">
                </td>
            </tr>
        </table>
    </form>
   
    <script type="text/javascript">
    
        function MontaPracas(idRegiao = 0){
                var idPlano = 0;
                idPlano = document.getElementById('ID_JOB').value;
                if(idPlano == ""){
                    if(idRegiao == 0){
                        idRegiao = $j('#ID_REGIAO').val();
                    }
                
                clearSelect($('pracas'),0);

                // seleciona as pracas que estiveram contidas ja no tipo de peca
                if( $j('#ID_TIPO_PECA').val() > 0 ){
                    var idTipoPeca = $j('#ID_TIPO_PECA').val();

                    new Ajax("<?php echo site_url('json/admin/getPracasByRegiaoTipoPecaSelecionada'); ?>", {
                        method: 'post',
                        onComplete: function(json){
                            var lista = eval(json);

                            $j(lista).each(function() {
                                var opt = document.createElement('option');
                                $('pracas').options.add(opt);
                                opt.text = this['DESC_PRACA'];
                                opt.value = this['ID_PRACA'];
                                if(this['SELECIONADO'] == 1){
                                    opt.selected = true;
                                }
                            });

                        }
                    }).request('id=' + idRegiao + '&idtipopeca=' + idTipoPeca);

                }
                else{
                    montaOptionsAjax($('pracas'),'<?php echo site_url('json/admin/getPracasByRegiao'); ?>','id=' + idRegiao,'ID_PRACA','DESC_PRACA',true,true);
                }
                }
            }
        // mascaras
        var mascaras = {};
        // linha modelo
        var tpl = '<tr>'
            + '<td style="text-align:left">{DESC_CATEGORIA}</td>'
            + '<td style="text-align:left">{DESC_SUBCATEGORIA}</td>'
            + '<td style="text-align:left"><input size="4" class="qtd" name="qtd[{ID_PRACA}][{ID_SUBCATEGORIA}][{PAGINA_EXCEL_CATEGORIA}]" type="text" value="{QTDE_EXCEL_CATEGORIA}" onblur="atualizaNome(this)" /></td>'
            + '<td style="text-align:left"><input size="4" class="pagina" name="pagina[{ID_PRACA}][{ID_SUBCATEGORIA}][{PAGINA_EXCEL_CATEGORIA}]" type="text" value="{PAGINA_EXCEL_CATEGORIA}" onblur="atualizaNome(this)" /></td>'
            + '<td style="text-align:left"><a href="#" onclick="$j(this).closest(\'tr\').remove(); return false;"><img src="<?php echo base_url().THEME; ?>img/file_delete.png" /></a></td>'
            + '</tr>';

        <?php
        if( !empty($tipos) ) {
            foreach($tipos as $tipo){
                printf('mascaras[%d] = {mascara: "%s", tip: "%s"};' . PHP_EOL, $tipo['ID_TIPO_PECA'], $tipo['MASCARA'], addslashes($tipo['TEXTO_EXPLICATIVO']));
            }
        }
        ?>

        <?php if( !empty($_POST['ID_JOB']) ): ?>
        function mostrarEspelho( idPraca ){
            displayMessage(util.options.site_url + 'plano_midia/espelho/<?php echo($_sessao['ID_CLIENTE']); ?>/<?php echo($_POST['ID_JOB']); ?>/'+idPraca);
        }
        <?php endif; ?>

        function preencheTipos(json){
            montaOptions($('ID_TIPO_PECA'), json, 'ID_TIPO_PECA', 'DESC_TIPO_PECA',true);
        }
        
        function PreparaCombo(){
            var idPlano = 0;
            idPlano = document.getElementById('ID_JOB').value;
            
            if(idPlano == ""){
                var idProduto = document.getElementById('ID_PRODUTO').value;
                var idRegiao = 0;
                clearSelect($('ID_CAMPANHA'),1);
                clearSelect($('ID_REGIAO'),1);
                clearSelect($('pracas'),0);
                montaOptionsAjaxLocal($('ID_REGIAO'),'<?php echo site_url('json/admin/getRegiaoByProduto'); ?>','id=' + idProduto,'ID_REGIAO','NOME_REGIAO',true,true);
                montaOptionsAjax($('ID_CAMPANHA'),'<?php echo site_url('json/admin/getCampanhasByProduto'); ?>','id=' + idProduto,'ID_CAMPANHA','DESC_CAMPANHA',true,true);
            }
        }
        window.onload = function() {
            PreparaCombo();
            
        }

        function montaOptionsAjaxLocal(el, url, data, chave, label, select,filtra = false){
            var valor = 0;
            new Ajax(url, {
                method: 'post',
                async: false,
                onComplete: function(json){
                    var lista = eval(json);
                    valor = montaOptions(el, lista, chave, label, select,filtra);
                    MontaPracas(valor);
                }

            }).request(data);

        }
       

        function replicarEspelho(de, nomePraca){
            var para = Array();
            var replicou = false;

            <?php if(isset($pracasSelecionadas)): ?>
            <?php foreach($pracasSelecionadas as $ps): ?>
            para.push( "<?php echo $ps['ID_PRACA']; ?>" );
            <?php endforeach; ?>
            <?php endif; ?>

            if( para.length <= 1){
                alert('Não foram encontradas mais praças no job.');
            }
            else{
                if(confirm('Deseja replicar o espelho desta praça selecionada para as demais praças?')){
                    for(var i = 0; i <= para.length - 1; i++){
                        if( de != para[i] ){
                            $j('#itens-' + para[i] + ' tr').remove();
                            $j('#itens-' + de + ' tr').each(function(){
                                var copia = $j(this).clone();
                                copia.find('.qtd, .pagina').each(function(){
                                    this.name = this.name.replace(/^qtd\[\d*\]/, 'qtd[' + para[i] + ']');
                                    this.name = this.name.replace(/^pagina\[\d*\]/, 'pagina[' + para[i] + ']');
                                });

                                copia.appendTo('#itens-' + para[i]);
                            });
                            var replicou = true;
                        }
                    }
                    if(replicou){
                        alert('O espelho da praça ' + nomePraca + ' foi replicado para as demais praças.')
                    }
                }
            }
        }

        $j(function(){
            
            
            //MontaPracas(idRegiao);
            adicionarMascaras();
            // mascaras do formato
            mascarasFormato('.centimetragem');

            $j('._calendar').datepicker({dateFormat: 'dd/mm/yy',showOn: 'button', buttonImage: '<?= base_url().THEME ?>img/calendario.png', buttonImageOnly: true});

            // 	adiociona eventos na select cliente e agencia q irao preencher outros select dinamicos
            <?php if(!empty($_sessao['ID_CLIENTE']) && empty($_sessao['ID_AGENCIA'])): ?>
            $('ID_AGENCIA').addEvent('change', function(){
                var idPlano = document.getElementById('ID_JOB').value;
            
                if(idPlano == ""){  
                    clearSelect($('ID_PRODUTO'),1);
                    clearSelect($('ID_CAMPANHA'),1);
                    clearSelect($('ID_REGIAO'),1);
                    clearSelect($('pracas'),0);
                    montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByAgencia'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO',true,true);
                }
            });
            <?php elseif(empty($_sessao['ID_CLIENTE']) && !empty($_sessao['ID_AGENCIA'])): ?>
            $('ID_CLIENTE').addEvent('change', function(){
                var idPlano = document.getElementById('ID_JOB').value;
            
                if(idPlano == ""){  
                    clearSelect($('ID_PRODUTO'),1);
                    clearSelect($('ID_CAMPANHA'),1);
                    clearSelect($('ID_REGIAO'),1);
                    clearSelect($('ID_TIPO_PECA'),1);
                    clearSelect($('pracas'),0);
                    clearSelect($('ID_USUARIO_RESPONSAVEL'),1);

                    carregaTiposPeca( this.value, preencheTipos );

                    montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByCliente'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO',true,true);
                }
            });
            <?php endif; ?>



            $('ID_PRODUTO').addEvent('change', function(){
                var idPlano = document.getElementById('ID_JOB').value;
            
                if(idPlano == ""){  
                    clearSelect($('ID_CAMPANHA'),1);
                    clearSelect($('ID_REGIAO'),1);
                    clearSelect($('pracas'),0);
                    montaOptionsAjax($('ID_REGIAO'),'<?php echo site_url('json/admin/getRegiaoByProduto'); ?>','id=' + this.value,'ID_REGIAO','NOME_REGIAO',true,true);
                    montaOptionsAjax($('ID_CAMPANHA'),'<?php echo site_url('json/admin/getCampanhasByProduto'); ?>','id=' + this.value,'ID_CAMPANHA','DESC_CAMPANHA',true,true);
                }
            });
            
            $('ID_REGIAO').addEvent('change', function(){
                var idPlano = document.getElementById('ID_JOB').value;
            
                if(idPlano == ""){  
                    MontaPracas();
                }
                
            });

            $('ID_REGIAO').addEvent('onfocusout', function(){
                var idPlano = document.getElementById('ID_JOB').value;
            
                if(idPlano == ""){  
                    MontaPracas();
                }
                
            });

            

            $('ID_TIPO_PECA').addEvent('change', function(){
                var idTipoPeca = this.value;
                mudaMascara( idTipoPeca );

                // seleciona as pracas que estiveram contidas ja na peca
                var idPlano = document.getElementById('ID_JOB').value;
            
                if(idPlano == ""){  
                    carregaPracasPorTipoPeca(idTipoPeca);
                }
            });

            $j('#btnCancelar').click(function(){
                location.href = '<?php echo site_url('plano_midia'); ?>';
            });

            if( $j('#ID_TIPO_PECA').val() != '' ){
                mudaMascara( $j('#ID_TIPO_PECA').val() );
            }

            <?php if(!empty($selecionadas) && !empty($id)): ?>
            $j('#tabs').tabs();
            <?php else: ?>
            $j('#tabs_lista').hide();
            <?php endif; ?>

            $j('.copiar').change(function(){
                var to = this.id.replace('copiar_','');
                var from = this.value;

                if( to == from ){
                    alert('Você não pode copiar da mesma praça!');
                    return;
                }

                if( from != '' ){
                    $j('#itens-' + to + ' tr').remove();
                    $j('#itens-' + from + ' tr').each(function(){
                        var copia = $j(this).clone();
                        copia.find('.qtd, .pagina').each(function(){
                            this.name = this.name.replace(/^qtd\[\d*\]/, 'qtd[' + to + ']');
                            this.name = this.name.replace(/^pagina\[\d*\]/, 'pagina[' + to + ']');
                        });

                        copia.appendTo('#itens-' + to);
                    });
                }
            });
            //////////////////////////////////////////////////////////////////

            <?php
            foreach($pracas as $item) {
              //  if($ID_JOB == ""){
                if( empty($categoriasSelecionadas[$item['ID_PRACA']]) ){
                    continue;
                }
                foreach($categoriasSelecionadas[$item['ID_PRACA']] as $categoria){
                    foreach($categoria['subcategorias'] as $cat){
                        if ( $cat['QTDE_EXCEL_CATEGORIA'] > 0 && $cat['PAGINA_EXCEL_CATEGORIA'] > 0){
                            //print_rr($cat);die;
                            printf('addEspelhoItem(new EspelhoItem(%d, "%s", "%s", %d, %d, %d));' . PHP_EOL
                                , $item['ID_PRACA']
                                , $cat['DESC_CATEGORIA']
                                , $cat['DESC_SUBCATEGORIA']
                                , $cat['ID_SUBCATEGORIA']
                                , $cat['QTDE_EXCEL_CATEGORIA']
                                , $cat['PAGINA_EXCEL_CATEGORIA']
                            );
                        }
                    }
                }
            //}
            }
            ?>

            //Habilita datepicker da DATA_INICIO_PROCESSO somente se o tipo de peca e a bandeira estiverem selecionados
            $j('#ID_TIPO_PECA').change(
                function(evt){
                    $j('#DATA_INICIO_PROCESSO').val('');
                    $j('#DATA_INICIO').val('');
                    $j('#DATA_TERMINO').val('');
                    if($j(this).val() == ""){
                        $j('#DATA_INICIO_PROCESSO').next().hide();
                        $j('#DATA_TERMINO').next().hide();
                    }else{
                        if ($j(this).val() != "" && $j('#ID_PRODUTO').val() != "") {
                            $j('#DATA_INICIO_PROCESSO').datepicker('option',{minDate: new Date()});
                            $j('#DATA_INICIO_PROCESSO').next().show();
                        }
                    }
                }
            );

            //Inicio Calculo da DATA_INICIO
            if($j('#DATA_INICIO_PROCESSO').val() == ""){
                if ( ($j('#ID_PRODUTO').val() != "") && ($j('#ID_TIPO_PECA').val() != "") ) {
                    $j('#DATA_INICIO_PROCESSO').datepicker('option',{minDate: new Date()});
                    $j('#DATA_INICIO_PROCESSO').next().show();
                }else{
                    //$j('#DATA_INICIO_PROCESSO').datepicker('option',{minDate: new Date()});
                    $j('#DATA_INICIO_PROCESSO').next().hide();
                }
            }


            if($j('#DATA_INICIO').val() != ""){
                var dateArray = $j('#DATA_INICIO').val().split('/');
                $j('#DATA_TERMINO').datepicker('option',{minDate: new Date(dateArray[2], dateArray[1]-1, dateArray[0])});
                $j('#DATA_TERMINO').next().show();
            }else{
                $j('#DATA_TERMINO').next().hide();
            }






            //Calcula DATA_INICIO apos escolher DATA_INICIO_PROCESSO
            $j('#DATA_INICIO_PROCESSO').datepicker('option',{
                onSelect: function(dateText, inst) {
                    $j.post(" <?= site_url('json/admin/calculaDataInicio')?>" ,
                        {date:dateText, idTipoPeca: $j('#ID_TIPO_PECA').val(), idProduto: $j('#ID_PRODUTO').val(), compararDataAtual: '1'},
                        function(data, textStatus, XMLHttpRequest){
                            data = eval(data);
                            data = data[0];
                            if(data.erro == ''){
                                $j('#DATA_INICIO').attr('value', data.result);
                                $j('#DATA_TERMINO').next().show();

                                var dateArray = data.result.split('/');
                                $j('#DATA_TERMINO').datepicker('option',{minDate: new Date(dateArray[2], dateArray[1]-1, dateArray[0])});
                                $j('#DATA_TERMINO').val('');

                                $j('#DATA_INICIO_PROCESSO').datepicker('option',{minDate: new Date()});
                            }
                            else{
                                $j('#DATA_INICIO').val('');
                                $j('#DATA_TERMINO').val('');
                                $j('#DATA_TERMINO').next().hide();
                                alert(data.erro);
                                $j('#DATA_INICIO_PROCESSO').val('');
                                $j('#DATA_INICIO_PROCESSO').datepicker('option',{minDate: new Date()});
                            }
                        }
                    );
                }
            });

            //Fim calculo DATA_INICIO

            $j('#ID_PRODUTO').change(
                function(evt){
                    $j('#DATA_INICIO_PROCESSO').val('');
                    $j('#DATA_INICIO').val('');
                    $j('#DATA_TERMINO').val('');
                    if($j(this).val() == ""){
                        $j('#DATA_INICIO_PROCESSO').next().hide();
                        $j('#DATA_TERMINO').next().hide();
                    }else{
                        if ($j(this).val() != "" && $j('#ID_TIPO_PECA').val() != "") {
                            $j('#DATA_INICIO_PROCESSO').datepicker('option',{minDate: new Date()});
                            $j('#DATA_INICIO_PROCESSO').next().show();
                        }
                    }
                }
            );
        });

        /**
         * CLASSE
         */
        function EspelhoItem(idPraca, descCategoria, descSubcategoria, idSubcategoria, qtde, pagina){
            this.ID_PRACA = idPraca;
            this.DESC_CATEGORIA = descCategoria;
            this.DESC_SUBCATEGORIA = descSubcategoria;
            this.ID_SUBCATEGORIA = idSubcategoria;
            this.QTDE_EXCEL_CATEGORIA = qtde;
            this.PAGINA_EXCEL_CATEGORIA = pagina;
        }

        function atualizaNome(ref){
            var line = $j(ref).closest('tr');
            var qtd = line.find('.qtd');
            var pagina = line.find('.pagina');

            var total = pagina.val();

            qtd.attr('name', qtd.attr('name').replace(/\[(\d*)]$/, '['+total+']'));
            pagina.attr('name', pagina.attr('name').replace(/\[(\d*)]$/, '['+total+']'));
        }

        function adicionarEspelhoItemHandler( ref ){
            var sele = $j(ref).parent().find("select");
            var opt = sele.find('option:selected');

            if( opt.val() == '' ){
                alert('Selecione uma subcategoria');
                return false;
            }

            var optg = opt.parent();
            var idPraca = sele.attr('id').split('-')[1];

            var item = new EspelhoItem(idPraca, optg.attr('label'), opt.attr('text'), opt.val(), 0, 0);
            addEspelhoItem( item );

            sele.val('');
        }

        // adiciona um item ao espelho
        // o parametro e um elemento do tipo "EspelhoItem"
        function addEspelhoItem(item){
            // faz uma copia do template
            var line = tpl;
            // objeto que guarda o resultado da expressao regular
            var m = null;

            // enquanto houverem placeholder's sem trocar
            while( m = line.match(/\{(\w+)\}/) ){
                // se o valor do placeholder encontrado existe no item do espelho e nao estiver vazio
                if( item[ m[1] ] && item[ m[1] ] != '' ){
                    // troca pelo valor do item do espelho
                    line = line.replace(new RegExp('\{' + m[1] + '\}', 'g'), item[ m[1] ]);

                } else {
                    // troca por um valor vazio
                    line = line.replace(new RegExp('\{' + m[1] + '\}', 'g'), '');
                }
            }

            // acha o container
            var container = $j('#itens-' + item.ID_PRACA);
            // adiciona a linha ao container
            $j(line).appendTo( container );
        }

        function validaFormulario( evt, intPraca ){
            return true;

            intPraca = (intPraca != undefined)?intPraca:0;
            var itens = [];
            var erros = [];
            var msg = 'Foram encontrados problemas no espelho. Verifique:\n'
                + ' - se há subcategorias iguais com páginas iguais\n'
                + ' - elementos com quantidade vazia\n'
                + ' - elementos com página vazia\n'
                + ' - se há itens com um numero de páginas maior que o informado no job';

            // PRIMEIRO, VALIDAMOS AS PAGINAS E ORDENS
            $j('.pagina').closest('tr').css('background-color','');
            $j('.pagina').each(function(obj){

                var obj = this;
                var idPraca = obj.name.match(/(\d+)/)[0];

                // se o valor da pagina for vazio ou menor igual a zero
                if( obj.value == '' || isNaN(obj.value) || parseFloat(obj.value) <= 0  ){
                    if( (idPraca == intPraca) || (intPraca == 0) ){
                        erros.push(obj);
                    }
                }

                // pega a quantidade relacionada
                var qtd = $j(obj).closest('tr').find('.qtd').val();

                // se o valor da qtde for vazio ou menor igual a zero
                if( qtd == '' || isNaN(qtd) || parseFloat(qtd) <= 0  ){
                    if( (idPraca == intPraca) || (intPraca == 0) ){
                        erros.push(obj);
                    }
                }

                $j.each(itens, function(k, item){
                    // se estiver duplicado
                    if( obj.name == item.name ){
                        if( (idPraca == intPraca) || (intPraca == 0) ){
                            erros.push(obj);
                            erros.push(item);
                        }
                        return;
                    }

                });
                itens.push( obj );

            });

            // agora vamos checar se todas as pracas possuem
            // espelho para as paginas informadas
            // no cadastro do job
            var qtdPaginas = parseFloat( $j('#PAGINAS_JOB').val() );

            // pega as pracas
            $j('.praca').each(function(){
                var div = $j(this);
                var idPraca = div.attr('id').split('-')[1];
                var nomePraca = div.find('.titulo').html();

                var found = {};
                // vamos checar pagina a pagina
                for(var i=1; i<=qtdPaginas; i++){
                    found[i] = 0;
                }

                div.find('.pagina').each(function(){
                    // pega o numero da pagina
                    var m = null;
                    if( (m = this.name.match(/\[(\d+)]$/)) ){

                        // se a pagina informada e maior que o numero de
                        // paginas informadas no job
                        if( parseFloat(m[1]) > qtdPaginas ){
                            erros.push( $j(this) );
                        } else {
                            // indica que achou para a pagina
                            // indicada
                            found[ parseFloat(m[1]) ]++;
                        }
                    }
                });

                // vamos ver quais paginas ficaram sem itens
                $j.each(found, function(pg, qtd){
                    if( qtd == 0 ) {
                        if( (idPraca == intPraca ) || ( intPraca == 0 ) ){
                            msg += '\n - A praça '+nomePraca+' está sem itens na página '+pg;
                            erros.push(1);
                        }
                    }
                });

            });

            if( erros.length > 0 ){
                $j.each(erros, function(k, obj){
                    $j(obj).closest('tr').css('background-color','#FFCC00');
                });

                alert( msg );
                return false;
            }
            else{
                return true;
            }

            //return erros.length == 0;
        }

        // aki caso o tipo de peca contenha pracas ja aprece selecionado
        function carregaPracasPorTipoPeca(idTipoPeca){
            var idPlano = 0;
            idPlano = document.getElementById('ID_JOB').value;
            if(idPlano == ""){
            new Ajax("<?php echo site_url('json/admin/getPracasByTipoPeca'); ?>", {
                method: 'post',
                onComplete: function(json){
                    var lista = eval(json);

                    $j("#pracas").val('0');
                    $j("#pracas option").each(function() {
                        var opt = $j(this);
                        $j(lista).each(function() {
                            if(this['ID_PRACA'] == opt.val()){
                                $j(opt).attr('selected', 'selected');
                            }
                        });
                    });
                }
            }).request('id=' + idTipoPeca);
            }
        }

        <?php if($podePassarEtapa && $this->uri->segment(3) != '' && !empty($selecionadas)): ?>
        function proximaEtapa(){
            if(validaFormulario()){
                $j('#formImportacao')
                    .attr('action','<?php echo site_url('plano_midia/proxima_etapa/'.$this->uri->segment(3)); ?>')
                    .submit();
            }
        }


        <?php endif; ?>

        function reaproveitarJob(){
            if(validaFormulario()){
                $j('#formImportacao')
                    .attr('action','<?php echo site_url('plano_midia/reaproveitar_job/'.$this->uri->segment(3)); ?>')
                    .submit();
            }
        }

        <? if(!empty($errosMudaEtapa)): ?>
        alert("<? echo implode('\n',$errosMudaEtapa); ?>");
        <? endif;?>


        $j(function(){
            $j('.ui-datepicker').hide();
            MontaPracas();
        });
        $j(document).ready(function () {
            MontaPracas();
        });
       
    </script>

</div> <!-- Final de Content Global-->

<?php $this->load->view("ROOT/layout/footer"); ?>
