<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
<h1>Cadastro de categorias</h1>
<form method="post" action="<?php echo site_url('categoria/lista'); ?>" id="pesquisa_simples">
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="AlignLeft" >
  <tr>
    <td width="8%" rowspan="2" align="center">
	<?php if($podeAlterar): ?>
    <a href="<?php echo site_url('categoria/form'); ?>"><img src="<?php echo base_url().THEME; ?>/img/file_add.png" alt="Adicionar" width="31" height="31" border="0" /></a>
    <?php endif; ?>    </td>
    <td width="21%">Cliente</td>
    <td width="19%">Categoria</td>
    <td width="19%">Status</td>
    <td width="15%">Itens / p&aacute;gina</td>
    <td width="19%" rowspan="2"><a class="button" title="Pesquisa" onclick="$j(this).closest('form').submit()"><span>Pesquisa</span></a></td>
  </tr>
  <tr>
    <td>
    <?php if(!empty($_sessao['ID_CLIENTE'])): ?>
		<?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?>
    <?php else: ?>
        <select name="ID_CLIENTE" id="ID_CLIENTE">
            <option value=""></option>
            <?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', empty($busca['ID_CLIENTE']) ? '' : $busca['ID_CLIENTE']); ?>
        </select>
    <?php endif; ?>
    </td>
    <td><input type="text" name="DESC_CATEGORIA" id="DESC_CATEGORIA" value="<?php echo empty($busca['DESC_CATEGORIA']) ? '' : $busca['DESC_CATEGORIA']?>" /></td>
    <td>
	  <select name="FLAG_ACTIVE_CATEGORIA" id="FLAG_ACTIVE_CATEGORIA">
        <?php echo montaOptions(array(array('DESC'=>'', 'VALUE'=>''),array('DESC'=>'ATIVO', 'VALUE'=>'1'),array('DESC'=>'INATIVO', 'VALUE'=>'0')),'VALUE','DESC', post('FLAG_ACTIVE_CATEGORIA',true)); ?>
      </select>
	</td>
    <td><span class="campo esq">
      <select name="pagina" id="pagina">
        <?php
		$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
		for($i=5; $i<=50; $i+=5){
			printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
		}
		?>
      </select>
    </span></td>
    </tr>
</table>
</form>

<?php if( !empty($categorias)) :?>
<br />
    <div id="tableObjeto">
    <table cellspacing="1" cellpadding="2" border="0" width="99%" class="tableSelection">
            <thead>
                <tr>
                    <?php if($podeAlterar): ?>
                    <th width="9%">Editar</th>
                    <?php endif; ?>
                  <th width="32%">Cliente</th>
                  <th width="27%">Categoria</th>
                  <th width="27%">Status</th>
                </tr>
            </thead>
            <?php foreach($categorias as $item): ?>
            <tr>
              <?php if($podeAlterar): ?>
                <td align="center"><a href="<?php echo site_url('categoria/form/'.$item['ID_CATEGORIA']); ?>">
                <img src="<?php echo base_url().THEME; ?>/img/file_edit.png" alt="Adicionar" border="0" />
                </a></td>
              <?php endif; ?>
                <td align="center"><?php echo $item['DESC_CLIENTE']; ?></td>
                <td align="center"><?php echo $item['DESC_CATEGORIA']; ?></td>
                <td align="center"><?php echo $item['FLAG_ACTIVE_CATEGORIA'] == '1' ? 'ATIVO' : 'INATIVO'; ?></td>
            </tr>
            <?php endforeach; ?>
    </table>
    </div>
    
    <div class="paginacao">
	    <?php echo $paginacao; ?>
    </div>
</div>

<?php else: ?>
	Nenhum resultado
<?php endif; ?>

<script type="text/javascript">
$('ID_AGENCIA').addEvent('change', function(){
	clearSelect($('ID_CLIENTE'),1);
	montaOptionsAjax($('ID_CLIENTE'),'<?php echo site_url('json/admin/getClientesByAgencia'); ?>','id=' + this.value,'ID_CLIENTE','DESC_CLIENTE');
});

</script>

<?php $this->load->view("ROOT/layout/footer") ?>