<?php $this->load->view("ROOT/layout/header"); ?>

<div id="contentGlobal">
<h1>Cadastro de Categoria</h1>
<form method="post" action="<?php echo site_url('categoria/save/'.$this->uri->segment(3)); ?>" id="form1">
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabela_pesquisa tabela_form">
  <tr>
    <td>Cliente</td>
    <td><?php if(!empty($_sessao['ID_CLIENTE'])): ?>
      <?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?>
      <?php else: ?>
      <select name="ID_CLIENTE" id="ID_CLIENTE">
        <option value=""></option>
        <?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', post('ID_CLIENTE',true)); ?>
      </select>
    <?php endif; ?></td>
  </tr>
  <tr>
    <td>Nome</td>
    <td><input name="DESC_CATEGORIA" type="text" value="<?php post('DESC_CATEGORIA'); ?>" />
    <?php showError('DESC_CATEGORIA'); ?></td>
  </tr>
  <tr>
    <td>C&oacute;digo</td>
    <td><input name="CODIGO_CATEGORIA" type="text" value="<?php post('CODIGO_CATEGORIA'); ?>" />
    <?php showError('CODIGO_CATEGORIA'); ?></td>
  </tr>
  <tr>
    <td>Status</td>
    <td>
      <select name="FLAG_ACTIVE_CATEGORIA" id="FLAG_ACTIVE_CATEGORIA">
        <?php echo montaOptions(array(array('DESC'=>'ATIVO', 'VALUE'=>'1'),array('DESC'=>'INATIVO', 'VALUE'=>'0')),'VALUE','DESC', post('FLAG_ACTIVE_CATEGORIA',true)); ?>
      </select>
    <?php showError('FLAG_ACTIVE_CATEGORIA'); ?>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><a class="button" href="<?php echo site_url('categoria/lista'); ?>" title="Pesquisa"><span>Cancelar</span></a>
     <a class="button" title="Pesquisa" onclick="$j(this).closest('form').submit()"><span>Gravar</span></a></td>
  </tr>
</table>
</form>
</div>

<script type="text/javascript">
</script>

<?php $this->load->view("ROOT/layout/footer") ?>