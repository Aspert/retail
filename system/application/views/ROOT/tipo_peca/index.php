﻿<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
	<h1>Tipos de Pe&ccedil;a</h1>
	<form method="post" action="<?php echo site_url('tipo_peca/lista'); ?>" id="pesquisa_simples">
		<table width="100%" class="Alignleft">
			<tr>
				<td width="7%" rowspan="2" align="center"><?php if($podeAlterar): ?>
					<a href="<?php echo site_url('tipo_peca/form'); ?>"><img src="<?php echo base_url().THEME; ?>img/file_add.png" title="Novo tipo de peça" alt="Novo tipo de peça" width="31" height="31" border="0" /></a>
					<?php endif; ?></td>
				<td width="22%">Clientes</td>
				<td width="30%">Tipo de Pe&ccedil;a</td>
				<td width="29%">Itens / p&aacute;gina</td>
				<td width="12%" rowspan="2"><a class="button" href="javascript:" title="Pesquisar" onclick="$j(this).closest('form').submit()"><span>Pesquisar</span></a></td>
			</tr>
			<tr>
				<td><select name="ID_CLIENTE" id="ID_CLIENTE">
				<option value=""></option>
				<?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE',post('ID_CLIENTE',true)); ?>
				</select></td>
				<td><input type="text" name="DESC_TIPO_PECA" id="DESC_TIPO_PECA" value="<?php echo empty($busca['DESC_TIPO_PECA']) ? '' : $busca['DESC_TIPO_PECA']?>" /></td>
				<td><span class="campo esq">
					<select name="pagina" id="pagina">
						<?php
						$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
						for($i=5; $i<=50; $i+=5){
							printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
						}
						?>
					</select>
					</span></td>
			</tr>
		</table>
	</form>
	<?php if( !empty($lista)) :?>
	<br />
	<div id="tableObjeto">
		<table cellspacing="1" cellpadding="2" border="0" width="100%" class="tableSelection">
			<thead>
				<tr>
					<?php if($podeAlterar): ?>
					<td width="4%"><strong>Editar</strong></td>
					<?php endif; ?>
					<td width="30%"><strong>Clientes</strong></td>
					<td width="37%"><strong>Tipo de Pe&ccedil;a</strong></td>
					<td width="29%"><strong>Situa&ccedil;&atilde;o</strong></td>
				</tr>
			</thead>
			<?php foreach($lista as $item): ?>
			<tr>
				<?php if($podeAlterar): ?>
				<td align="center"><a href="<?php echo site_url('tipo_peca/form/'.$item['ID_TIPO_PECA']); ?>"><img src="<?php echo base_url().THEME; ?>img/file_edit.png" alt="Editar" title="Editar" width="31" height="31" border="0" /></a></td>
				<?php endif; ?>
				<td align="center"><?php echo $item['DESC_CLIENTE']; ?></td>
				<td align="center"><?php echo $item['DESC_TIPO_PECA']; ?></td>
				<td align="center"><?php echo $item['FLAG_ATIVO_TIPO_PECA'] == 1 ? 'Ativo' : 'Inativo'; ?></td>
			</tr>
			<?php endforeach; ?>
		</table>
	</div>
	<span class="paginacao"><?php echo $paginacao; ?></span>
	<?php else: ?>
	Não existem informações para a pesquisa solicitada.
	<?php endif; ?>
</div>
<?php $this->load->view("ROOT/layout/footer") ?>
