<?php $this->load->view("ROOT/layout/header"); ?>

<div id="contentGlobal"> <?php echo form_open('tipo_peca/save/'.$this->uri->segment(3));?>
	<h1>Cadastro de Tipos de Pe&ccedil;a</h1>
	<?php if ( !empty($erros) ) : ?>
	<ul style="color:#f00;font-weight:bold;" class="box_erros">
		<?php foreach ( $erros as $e ) : ?>
		<li>
			<?=$e?>
		</li>
		<?php endforeach; ?>
	</ul>
	<?php endif; ?>
	<table width="100%" class="Alignleft">
		<tr>
			<td>Cliente *</td>
			<td width="80%">
			<?php
			if( !empty($_sessao['ID_CLIENTE']) ){
				echo sessao_hidden('ID_CLIENTE','DESC_CLIENTE');
			} else {
				echo '<select name="ID_CLIENTE" id="ID_CLIENTE">
					<option value=""></option>
					' . montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE',post('ID_CLIENTE',true)) . '
				</select>';
			}
			?>
			</tr>
		</tr>
		<tr>
			<td width="16%">Tipo de pe&ccedil;a  *</td>
			<td width="84%"><input name="DESC_TIPO_PECA" type="text" value="<?php post('DESC_TIPO_PECA'); ?>" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Utilize:<br />
				# - para d&iacute;gitos (de 0 a 9)<br />
				* - para letras e d&iacute;gitos</td>
		</tr>
		<tr>
			<td>M&aacute;scara</td>
			<td><input name="MASCARA" type="text" id="MASCARA" size="50" maxlength="200" value="<?php post('MASCARA'); ?>" /></td>
		</tr>
		<tr>
			<td>Testar m&aacute;scara</td>
			<td><input name="TESTE" type="text" id="TESTE" size="50" maxlength="200" /></td>
		</tr>
		<tr>
			<td>Texto explicativo</td>
			<td><input name="TEXTO_EXPLICATIVO" type="text" id="TEXTO_EXPLICATIVO" size="50" maxlength="200" value="<?php post('TEXTO_EXPLICATIVO'); ?>" /></td>
		</tr>
		<tr>
			<td>Situa&ccedil;&atilde;o *</td>
			<td><select name="FLAG_ATIVO_TIPO_PECA" id="FLAG_ATIVO_TIPO_PECA">
				<option value="1">Ativo</option>
				<option value="0"<?php echo post('FLAG_ATIVO_TIPO_PECA',true)=='0' ? ' selected="selected"' : ''; ?>>Inativo</option>
			</select></td>
		</tr>
        <tr>
          <td valign="top">Selecione as pra&ccedil;as *</td>
          <td>
          	<select name="pracas[]" id="pracas" style="width:100%; height:100px;" multiple="multiple">
          		<option value=""></option>
          		<?php foreach($pracas as $p): ?>
          			<option value="<?php echo $p['ID_PRACA']; ?>" <?php if( isset($p['SELECIONADO']) && $p['SELECIONADO'] == 1 ){ echo 'selected="selected"'; } ?>><?php echo $p['DESC_PRACA']; ?></option>
          		<?php endforeach; ?>
          	</select>
          </td>
        </tr>
		<tr>
			<td>
				Adicionar Subcategoria
			</td>
			<td>
				<select id="comboCategorias" class="addItemEspelho">
					<option value=""></option>
					<?php foreach($categorias as $categoria): ?>
						<optgroup label="<?php echo $categoria['DESC_CATEGORIA']; ?>">
						<?php foreach($categoria['subcategorias'] as $cat): ?>
							<option value="<?php echo $cat['ID_SUBCATEGORIA'];?>"><?php echo $cat['DESC_SUBCATEGORIA'];?></option>
						<?php endforeach;?>
						</optgroup>
					<?php endforeach;?>
				</select>
				<a href="#" onclick="adicionarEspelhoItemHandler(this); return false;"> <img src="<?php echo base_url().THEME.'img/add.png'; ?>" alt="Adiciona uma subcategoria" class="information" /> </a>
				<a href="#" onclick="adicionarAllEspelhoItemHandler(); return false;"> <img src="<?php echo base_url().THEME.'img/addAll.png'; ?>" alt="Adiciona todas as subcategorias" class="information" id="all"/> </a>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabela_padrao">
					<thead>
						<tr>
							<th width="30%">Categoria</th>
							<th width="30%">Sub Categoria</th>
							<th width="20%">Limite a Menos</th>
							<th width="20%">Limite a Mais</th>
							<th>Opções</th>
						</tr>
					</thead>
					<tbody id="itens">
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">Bloquear?<input type="checkbox" name="FLAG_BLOQUEIO_LIMITE" <?php echo post('FLAG_BLOQUEIO_LIMITE',true)=='1' ? 'checked': ''; ?>></input></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><a href="<?php echo site_url('tipo_peca/lista'); ?>" class="button"><span>Cancelar</span></a> <a href="javascript:;" onclick="$j(this).closest('form').submit()" class="button"><span>Salvar</span></a></td>
		</tr>
	</table>
	<?php echo form_close();?> </div>
	
<script type="text/javascript">
$j(function(){
	adicionarMascaras();
	
	$j('#MASCARA').blur(function(){
		$j('#TESTE').unmask();
		$j('#TESTE').mask( this.value );
	});

	if( $j('#MASCARA').val() != '' ){
		$j('#TESTE').mask( $j('#MASCARA').val() );
	}
	
	<?php
		if( !empty($categoriasSelecionadas) ){
			foreach($categoriasSelecionadas as $categoriasSelecionada){
				printf('addEspelhoItem(new EspelhoItem("%s", "%s", %d, %d, %d));' . PHP_EOL,
					$categoriasSelecionada['DESC_CATEGORIA'],
					$categoriasSelecionada['DESC_SUBCATEGORIA'],
					$categoriasSelecionada['ID_SUBCATEGORIA'],
					$categoriasSelecionada['LIMITE_MINIMO'],
					$categoriasSelecionada['LIMITE_MAXIMO']
				);
			}
		}
	?>

	// adiciona a validacao
	$j('form').submit( validaFormulario );
});
//linha modelo
var tpl = '<tr>'
			+ '<td style="text-align:left">{DESC_CATEGORIA}</td>'
			+ '<td style="text-align:left">{DESC_SUBCATEGORIA}</td>'
			+ '<td style="text-align:left"><input size="4" class="limiteMinimo" name="limiteMinimo[{ID_SUBCATEGORIA}]" type="text" value="{LIMITE_MINIMO}" /></td>'
			+ '<td style="text-align:left"><input size="4" class="limiteMaximo" name="limiteMaximo[{ID_SUBCATEGORIA}]" type="text" value="{LIMITE_MAXIMO}" /></td>'
			+ '<td style="text-align:left"><a href="#" onclick="$j(this).closest(\'tr\').remove(); return false;"><img src="<?php echo base_url().THEME; ?>img/file_delete.png" alt="Excluir" title="Excluir"/></a></td>'
		+ '</tr>';
		
//Espelho de itens
function EspelhoItem(descCategoria, descSubcategoria, idSubcategoria, limiteMinimo, limiteMaximo){
	this.DESC_CATEGORIA = descCategoria;
	this.DESC_SUBCATEGORIA = descSubcategoria;
	this.ID_SUBCATEGORIA = idSubcategoria;
	this.LIMITE_MINIMO = limiteMinimo;
	this.LIMITE_MAXIMO = limiteMaximo;
}

function adicionarEspelhoItemHandler( ref ){
	var sele = $j(ref).parent().find("select");
	var opt = sele.find('option:selected');
	
	if( opt.val() == '' ){
		alert('Selecione uma subcategoria');
		return false;
	}
	var optg = opt.parent();
    var item = new EspelhoItem(optg.attr('label'), opt.text(), opt.val(), 0, 0);
	addEspelhoItem( item );
	
	sele.val('');
}

function adicionarAllEspelhoItemHandler() {
    $j("#comboCategorias option").each(function() {
        if ($j(this) != "undefined") {
			var parent = $j(this).parent().attr('label');
			var text = $j(this).text();
			var value = $j(this).val();
			if (text != "" && value != "") {
				var item = new EspelhoItem(parent, text, value, 0, 0);
				addEspelhoItem(item);
			}
		}
    });
}

// adiciona um item ao espelho
// o parametro e um elemento do tipo "EspelhoItem"
function addEspelhoItem(item){
	// faz uma copia do template
	var line = tpl;
	// objeto que guarda o resultado da expressao regular
	var m = null;
	// enquanto houverem placeholder's sem trocar
	while( m = line.match(/\{(\w+)\}/) ){
		// se o valor do placeholder encontrado existe no item do espelho e nao estiver vazio
		if( item[ m[1] ] && item[ m[1] ] != '' ){
			// troca pelo valor do item do espelho
			line = line.replace(new RegExp('\{' + m[1] + '\}', 'g'), item[ m[1] ]);
			
		} else {
			// troca por um valor vazio
			line = line.replace(new RegExp('\{' + m[1] + '\}', 'g'), '0');
		}
	}
	// acha o container
	var container = $j('#itens');
	
	// adiciona a linha ao container
	$j(line).appendTo( container );
}
function validaFormulario(){
	var itens = [];
	var erros = [];
	var msg = 'Foram encontrados problemas nas categorias/subcategorias incluídas. Verifique:\n'
			+ ' - se há subcategorias iguais\n'
			+ ' - elementos somente numéricos';

	// PRIMEIRO, VALIDAMOS AS PAGINAS E ORDENS
	$j('.limiteMinimo').closest('tr').css('background-color','');
	$j('.limiteMinimo').each(function(obj){
		var obj = this;
		
		var limiteMinimo = obj.value;
		var limiteMaximo = $j(obj).closest('tr').find('.limiteMaximo').val();
		
		if( (limiteMinimo == '' && limiteMaximo == '') || ( isNaN(limiteMinimo) || isNaN(limiteMaximo) ) ){
			erros.push(obj);
		//} else if( parseFloat(limiteMaximo) <= parseFloat(limiteMinimo) ){
		//	erros.push(obj);
		}
		
		$j.each(itens, function(k, item){
			if( obj.name == item.name ){
				erros.push(obj);
				erros.push(item);
				return;        
			}
		});
		itens.push( obj );
	});
	
	if( erros.length > 0 ){
		$j.each(erros, function(k, obj){
			$j(obj).closest('tr').css('background-color','#FFCC00');
		});
		alert( msg );
	}
	
	return erros.length == 0;
}
	$('ID_CLIENTE').addEvent('change', function(){
		clearSelect($('comboCategorias'),0);
		clearSelect($('pracas'),1);
		clearSelectGroup($('comboCategorias'),0);

		$j.each($j('#itens tr'), function(k, item) {
			$j(item).remove();
		});
		
		if (this.value != '' || this.value != null ) {
			montaOptionsGroupAjax($('comboCategorias'),'<?php echo site_url('json/admin/getCatSubByCliente'); ?>','id=' + this.value,'ID_SUBCATEGORIA','DESC_SUBCATEGORIA', 'subcategorias', 'DESC_CATEGORIA', true);
		}

		montaOptionsAjax($('pracas'),'<?php echo site_url('json/admin/getPracasByCliente'); ?>','id=' + this.value,'ID_PRACA','DESC_PRACA');
	});
	
</script>
	
<?php $this->load->view("ROOT/layout/footer") ?>
