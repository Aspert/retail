<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
<h1>Administrador</h1>

<div id="contentListaAdmin">

	<ul>
		<?php echo Sessao::hasPermission('usuario','lista') ? '<li>' . anchor('usuario/lista','Usu&#225;rios Dispon&iacute;veis') . '</li>' . PHP_EOL : '';?>
		<?php echo Sessao::hasPermission('categoria','lista') ? '<li>' . anchor('categoria/lista','Categorias Dispon&iacute;veis') . '</li>' . PHP_EOL : '';?>
		<?php echo Sessao::hasPermission('grupo','lista') ? '<li>' . anchor('grupo/lista','Grupos de Usu&#225;rios Dispon&iacute;veis') . '</li>' . PHP_EOL : '';?>
		<?php echo Sessao::hasPermission('subcategoria','lista') ? '<li>' . anchor('subcategoria/lista','SubCategorias Dispon&iacute;veis') . '</li>' . PHP_EOL : '';?>
		<?php echo Sessao::hasPermission('permissao','lista') ? '<li>' . anchor('permissao/lista','Permiss&otilde;es') . '</li>' . PHP_EOL : '';?>
		<?php echo Sessao::hasPermission('fabricante','lista') ? '<li>' . anchor('fabricante/lista','Fabricantes Dispon&#237;veis') . '</li>' . PHP_EOL : '';?>
		<?php echo Sessao::hasPermission('agencia','lista') ?  '<li>' . anchor('agencia/lista','Manter Ag&ecirc;ncias') . '</li>' . PHP_EOL : '';?>
		<?php echo Sessao::hasPermission('marca','lista') ? '<li>' . anchor('marca/lista','Marcas Dispon&iacute;veis') . '</li>' . PHP_EOL : '';?>
		<?php echo Sessao::hasPermission('cliente','lista') ? '<li>' . anchor('cliente/lista','Manter Clientes') . '</li>' . PHP_EOL : '';?>
		<?php echo Sessao::hasPermission('campo','lista') ? '<li>' . anchor('campo/lista','Campos para fichas') . '</li>' . PHP_EOL : '';?>
		<?php echo Sessao::hasPermission('transformadores','index') ? '<li>' . anchor('transformadores/index','Transformadores') . '</li>' . PHP_EOL : '';?>
		<?php echo Sessao::hasPermission('parcela_dinamica','index') ? '<li>' . anchor('parcela_dinamica/index','Cálculo de Parcela Dinâmica ') . '</li>' . PHP_EOL : '';?>
	</ul>
	<ul>
		<?php echo Sessao::hasPermission('produto','lista') ? '<li>' . anchor('produto/lista','Manter Bandeiras') . '</li>' . PHP_EOL : '';?>
		<?php echo Sessao::hasPermission('tipo_objeto','lista') ? '<li>' . anchor('tipo_objeto/lista','Tipos de Objeto') . '</li>' . PHP_EOL : '';?>
		<?php echo Sessao::hasPermission('cria_ficha','form') ? '<li>' . anchor('cria_ficha/form','Criar Fichas para Objetos') . '</li>' . PHP_EOL : '';?>
		<?php echo Sessao::hasPermission('job','lista') ? '<li>' . anchor('job/lista','Manter JOBs') . '</li>' . PHP_EOL : '';?>
		<?php echo Sessao::hasPermission('controllers','lista') ? '<li>' . anchor('controllers/lista','Manter Controllers') . '</li>' . PHP_EOL : '';?>
		<?php echo Sessao::hasPermission('functions','lista') ? '<li>' . anchor('functions/lista','Manter Fun&ccedil;&otilde;es') . '</li>' . PHP_EOL : '';?>
		<?php echo Sessao::hasPermission('item_menu','lista') ? '<li>' . anchor('item_menu/lista','Manter Itens do Menu') . '</li>' . PHP_EOL : '';?>
		<?php echo Sessao::hasPermission('regra_email','lista') ? '<li>' . anchor('regra_email/lista','Manter Regras de E-mail') . '</li>' . PHP_EOL : '';?>
		<?php echo Sessao::hasPermission('pracas','lista') ? '<li>' . anchor('pracas/lista','Manter Pra&ccedil;as') . '</li>' . PHP_EOL : '';?>	
		<?php echo Sessao::hasPermission('regiao','lista') ? '<li>' . anchor('regiao/lista','Manter Regi&otilde;es') . '</li>' . PHP_EOL : '';?>	
		<?php echo Sessao::hasPermission('tipo_peca','lista') ? '<li>' . anchor('tipo_peca/lista','Manter Tipos de Pe&ccedil;a') . '</li>' . PHP_EOL : '';?>
		<?php echo Sessao::hasPermission('processos','lista') ? '<li>' . anchor('processos/lista','Cadastro de Processos') . '</li>' . PHP_EOL : '';?>	
		<?php echo Sessao::hasPermission('calendario','lista') ? '<li>' . anchor('calendario/lista','Cadastro de Calendário') . '</li>' . PHP_EOL : '';?>
		<?php echo Sessao::hasPermission('api','lista') ? '<li>' . anchor('api/lista','Controle de API') . '</li>' . PHP_EOL : '';?>
	</ul>

</div> <!-- Final de Content Lista Admin -->

</div> <!-- Final de Content Global -->

<?php $this->load->view("ROOT/layout/footer") ?>
