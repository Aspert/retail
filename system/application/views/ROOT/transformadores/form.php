<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
	<?php if ( isset($erros) && is_array($erros) ) : ?>
	<ul style="color:#ff0000;font-weight:bold;">
		<?php foreach ( $erros as $e ) : ?>
		<li>
			<?=$e?>
		</li>
		<?php endforeach; ?>
	</ul>
	<?php endif; ?>
	<h1>Cadastro de Transformadores</h1>
	<?php echo form_open('transformadores/save/'.$this->uri->segment(3));?>
	<table width="100%" class="Alignleft">
		<tr>
			<td width="17%">Nome do transformador *</td>
			<td width="83%"><input name="NOME" type="text" id="NOME" value="<?php post('NOME'); ?>" /></td>
		</tr>
		<tr>
			<td valign="top">Texto explicativo</td>
			<td><textarea name="DESCRICAO" id="DESCRICAO" cols="45" rows="5"><?php post('DESCRICAO'); ?></textarea></td>
		</tr>
		<tr>
			<td valign="top">&nbsp;</td>
			<td><a href="javascript:" class="button" onclick="addAction('','',[])"><span>Adicionar A&ccedil;&atilde;o</span></a>&nbsp;</td>
		</tr>
	</table>
	<table width="100%" class="tabela_padrao">
		<thead>
			<tr>
				<th width="27%">Campo</th>
				<th width="20%">A&ccedil;&atilde;o</th>
				<th width="30%">&nbsp;</th>
				<th width="18%">Param&ecirc;tros</th>
				<th width="5%">&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<tr id="modelo">
				<td style="text-align:left">
				<select name="ACOES[{IDX}][CAMPO]" id="ACOES[{IDX}][CAMPO]" class="campo">
					<option value="ORDEM">ORDEM</option>
					<option value="PAGINA">PAGINA</option>
					<option value="SUBCATEGORIA">SUBCATEGORIA</option>
					<option value="CODIGO_BARRAS">CODIGO_BARRAS</option>
					<option value="DESCRICAO">DESCRICAO</option>
					<option value="TEXTO_LEGAL">TEXTO_LEGAL</option>
					<option value="PRECO_UN">PRECO_UN</option>
					<option value="TIPO_UN">TIPO_UN</option>
					<option value="PRECO_CX">PRECO_CX</option>
					<option value="TIPO_CX">TIPO_CX</option>
					<option value="PRECO_VISTA">PRECO_VISTA</option>
					<option value="PRECO_DE">PRECO_DE</option>
					<option value="PRECO_POR">PRECO_POR</option>
					<option value="ECONOMIZE">ECONOMIZE</option>
					<option value="ENTRADA">ENTRADA</option>
					<option value="PARCELAS">PARCELAS</option>
					<option value="PRESTACAO">PRESTACAO</option>
					<option value="JUROS_AM">JUROS_AM</option>
					<option value="JUROS_AA">JUROS_AA</option>
					<option value="TOTAL_PRAZO">TOTAL_PRAZO</option>
					<option value="OBSERVACOES">OBSERVACOES</option>
					<option value="EXTRA1">EXTRA1</option>
					<option value="EXTRA2">EXTRA2</option>
					<option value="EXTRA3">EXTRA3</option>
					<option value="EXTRA4">EXTRA4</option>
					<option value="EXTRA5">EXTRA5</option>
				</select></td>
				<td style="text-align:left">
					<select name="ACOES[{IDX}][ACAO]" class="acao" onchange="setAction(this, this.value)">
						<option value=""></option>
						<?php
						foreach($acoes as $item){
							printf('<option value="%s" title="%s">%s</option>'
								, $item['CLASSE']
								, addslashes($item['DESCRICAO'])
								, $item['NOME']
							);
						}
						?>
					</select>
				</td>
				<td style="text-align:left" class="descricao">&nbsp;</td>
				<td style="text-align:left" class="parametros">
					<div>
						<input type="text" name="ACOES[{IDX}][PARAMETROS][]" class="param" />
					</div>
				</td>
				<td>
					<a href="#" onclick="idx--; $j(this).closest('tr').remove(); return false;">
						<img src="<?php echo base_url().THEME; ?>img/trash.png" alt="Remover" />
					</a>
				</td>
			</tr>
		</tbody>
	</table>
	
	<div style="padding-top: 20px;">
		<a href="<?php echo site_url('transformadores/index'); ?>" class="button"><span>Cancelar</span></a> <a href="javascript:;" onclick="$j(this).closest('form').submit()" class="button"><span>Salvar</span></a>
	</div>
	
	<div class="both">&nbsp;</div>
	<?php echo form_close(); ?></div>
	
<script type="text/javascript">

var idx = 0;
var possibleActions = {};
var modelo = null;
var container = null;
var modeloParam = null;

var addedActions = [];

<?php
foreach($acoes as $item){
	printf('possibleActions["%s"] = {nome: "%s", descricao: "%s", qtd: %d};' . PHP_EOL
		, $item['CLASSE']
		, addslashes($item['NOME'])
		, addslashes( preg_replace("@[\r\n]@","",$item['DESCRICAO']) )
		, $item['QTD_PARAMETROS']
	);
}

if( !empty($_POST['ACOES']) ){
	foreach($_POST['ACOES'] as $acao){
		echo 'addedActions.push(["', $acao['CAMPO'], '", "', $acao['ACAO'], '", [';
																				
		while( !empty($acao['PARAMETROS']) ){
			$val = addslashes(array_shift($acao['PARAMETROS']));
			
			echo '"' , $val, '"';
			if( !empty($acao['PARAMETROS']) ){
				echo ', ';
			}
		}
		
		echo ']]);', PHP_EOL;
	}
}
?>

function addAction(field, action, params){
	var html = $j('<div></div>').append( modelo.clone() ).html();

	html = html.substitute( {IDX: idx.toString()} );

	var line = $j(html);
	line.find('.campo').val( field );
	line.attr('id', idx);
	
	if( action != '' ){
		setAction( line.find('.acao'), action );
		
		var i=0;
		line.find('.param').each(function(){
			if( params[i] ){
				this.value = params[i];
			}
			i++;
		});
	}
	
	line.appendTo( container );
	
	idx++;
}

function setAction( ref, action ){
	var line = $j(ref).closest('tr');
	line.find('.parametros').html('');
	
	$j(ref).val( action );
	
	if( action != '' ){
		var act = possibleActions[ action ];
		var id = line.attr('id');
		var html = $j('<div></div>').append( modeloParam.clone() ).html().substitute( {IDX: id} );
		
		line.find('.descricao').html( act.descricao );
		
		for(var i=0; i<act.qtd; i++){
			line.find('.parametros').append( html );
		}
	}
}

$j(function(){
	modelo = $j('#modelo').clone();
	modeloParam = modelo.find('.parametros div').clone();
	container = $j('#modelo').parent();
	
	modelo.find('.parametros').html('');
	modelo.attr('id','');
	
	$j('#modelo').remove();
	
	while( addedActions.length > 0 ){
		var act = addedActions.shift();
		addAction(act[0], act[1], act[2]);
	}
	
});

</script>
	
	
<?php $this->load->view("ROOT/layout/footer") ?>
