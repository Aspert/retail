<?php $this->load->view("ROOT/layout/header"); ?>

<div id="contentGlobal">
  <?php echo form_open('regra_email/save/'.$this->uri->segment(3));?>
  <h1>Regra de E-mail</h1>
  <table width="100%" class="Alignleft">
    <tr>
      <td width="21%">Nome da regra: <span class="obrigatorio">*</span></td>
      <td width="79%"><input name="DESC_REGRA_EMAIL" type="hidden" value="<?php post('DESC_REGRA_EMAIL'); ?>" size="40"  />
        <?php post('DESC_REGRA_EMAIL'); ?>
        <?php showError('DESC_REGRA_EMAIL'); ?></td>
    </tr>
    <tr>
      <td>Chave: <span class="obrigatorio">*</span></td>
      <td><input name="CHAVE_REGRA_EMAIL" type="hidden" value="<?php post('CHAVE_REGRA_EMAIL'); ?>" size="40"  />
        <?php post('CHAVE_REGRA_EMAIL'); ?>
        <?php showError('CHAVE_REGRA_EMAIL'); ?></td>
    </tr>
    <tr>
      <td>Status: <span class="obrigatorio">*</span></td>
      <td><input name="STATUS_REGRA_EMAIL" type="radio" value="1" <?php echo post('STATUS_REGRA_EMAIL',true)== 1 || empty($_POST['STATUS_REGRA_EMAIL']) ? 'checked' : ''; ?> />
        Ativo
        <input name="STATUS_REGRA_EMAIL" type="radio" value="0" <?php echo post('STATUS_REGRA_EMAIL',true)=='0' ? 'checked' : ''; ?> />
        Inativo
        <?php showError('STATUS_REGRA_EMAIL'); ?></td>
    </tr>
    <tr>
      <td>Enviar para usu&aacute;rio logado? </td>
      <td><input name="FLAG_ENVIA_LOGADO" type="radio" value="1" <?php echo post('FLAG_ENVIA_LOGADO',true)== 1 || empty($_POST['FLAG_ENVIA_LOGADO']) ? 'checked' : ''; ?> />
        Sim
        <input name="FLAG_ENVIA_LOGADO" type="radio" value="0" <?php echo post('FLAG_ENVIA_LOGADO',true)=='0' ? 'checked' : ''; ?> />
        N&atilde;o
        <?php showError('FLAG_ENVIA_LOGADO'); ?></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  <br />
  <h1>Adicionar usuarios para a regra</h1>
  <table width="759" border="0">
    <tr>
      <th width="137" scope="col">Agencia</th>
      <th width="129" scope="col">Cliente</th>
      <th width="130" scope="col">Grupo</th>
      <td width="345" scope="col">&nbsp;</td>
    </tr>
    <tr>
      <td><select name="ID_AGENCIA" id="ID_AGENCIA" class="comboPesquisa">
      	<option value=""></option>
      	<?php echo montaOptions($agencias,'ID_AGENCIA','DESC_AGENCIA', post('ID_AGENCIA',true)); ?>
      	</select></td>
      <td><select name="ID_CLIENTE" id="ID_CLIENTE" class="comboPesquisa">
	  <option value=""></option>
	    <?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', post('ID_CLIENTE',true)); ?>
      	</select></td>
      <td><select name="ID_GRUPO" id="ID_GRUPO" class="comboPesquisa">
      	<option value=""></option>
      	<?php echo montaOptions($grupos,'ID_GRUPO','DESC_GRUPO', post('ID_GRUPO',true)); ?>
      	</select></td>
      <td><a class="button" href="javascript:;" title="Adicionar Grupo" id="btnAddGrupo"><span>+ Adicionar grupo</span></a></td>
    </tr>
  </table>
  <br />
  <br />
  <div id="appendUsuarios"> </div>
  <div style="clear:both">&nbsp;</div>
  <br /><br />
  <a class="button" href="<?php echo site_url('regra_email/lista'); ?>" title="Cancelar"><span>Cancelar</span></a>
  <a class="button" href="javascript:;" onclick="$j(this).closest('form').submit()" title="Salvar"><span>Salvar</span></a>
  <div>&nbsp; </div>
  <?php echo form_close();?> </div>
<script type="text/javascript">

var idregra = '<?php printf('%d', $this->uri->segment(3)); ?>';

$('ID_AGENCIA').addEvent('change', function(){
	$j('#ID_GRUPO').empty();
	$('ID_CLIENTE').value = '';
	montaOptionsAjax($('ID_GRUPO'),'<?php echo site_url('json/admin/getGruposByAgencia'); ?>','id=' + this.value,'ID_GRUPO','DESC_GRUPO');
});

$('ID_CLIENTE').addEvent('change', function(){
	$j('#ID_GRUPO').empty();
	$('ID_AGENCIA').value = '';
	montaOptionsAjax($('ID_GRUPO'),'<?php echo site_url('json/admin/getGruposByCliente'); ?>','id=' + this.value,'ID_GRUPO','DESC_GRUPO');
});

$j('#btnAddGrupo').click(function(){
	loadGrupo( $('ID_GRUPO').value );
	$('ID_AGENCIA').value = '';
	$('ID_CLIENTE').value = '';
	$j('#ID_GRUPO').empty();
});

// carrega a listagem de usuarios por grupo
function loadGrupo(idgrupo){
	if( $j('.grupo_'+idgrupo).length > 0 ){
		alert('Este grupo ja esta carregado');
		return;
	}
	
	if( idgrupo == '' ){
		alert('Informe o grupo!');
		return;
	}
	
	// carrega a listagem de usuarios
	$j.post('<?php echo site_url('json/admin/loadGrupoRegra'); ?>/' + idgrupo + '/' + idregra, null, function(html){
		// quando terminar, coloca na tela
		$j(html).appendTo('#appendUsuarios');
	});
}

// seleciona todos os usuarios
function selecionarTodos(src){
	$j(src).parent().parent().find('input[type="checkbox"]').attr('checked', true);
}

// desmarca todos os usuarios
function selecionarNenhum(src){
	$j(src).parent().parent().find('input[type="checkbox"]').attr('checked', false);
}

// remove um grupo da lista
function removerGrupo(src){
	$j(src).parent().parent().parent().remove();
}

<?php

foreach($grupos_selecionados as $grupo){
	echo 'loadGrupo('.$grupo.');' . PHP_EOL;
}
?>



</script>
<?php $this->load->view("ROOT/layout/footer") ?>
