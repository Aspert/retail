<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
  <h1>Regras de E-mail</h1>
  <form method="post" action="<?php echo site_url('regra_email/lista'); ?>" id="pesquisa_simples">
    <table width="100%" class="Alignleft">
      <tr>
        <td width="8%" rowspan="2" align="center">&nbsp;</td>
        <td width="19%">Regra email</td>
        <td width="19%">Chave</td>
        <td width="19%"></td>
        <td width="15%">Itens / p&aacute;gina</td>
        <td width="20%" rowspan="2"><a class="button" href="javascript:" title="Pesquisar" onclick="$j(this).closest('form').submit()"><span>Pesquisar</span></a></td>
      </tr>
      <tr>
        <td><input type="text" name="DESC_REGRA_EMAIL" id="DESC_REGRA_EMAIL" value="<?php echo empty($busca['DESC_REGRA_EMAIL']) ? '' : $busca['DESC_REGRA_EMAIL']?>" /></td>
        <td><input type="text" name="CHAVE_REGRA_EMAIL" id="CHAVE_REGRA_EMAIL" value="<?php echo empty($busca['CHAVE_REGRA_EMAIL']) ? '' : $busca['CHAVE_REGRA_EMAIL']?>" /></td>
        <td></td>
        <td><span class="campo esq">
          <select name="pagina" id="pagina">
            <?php
		$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
		for($i=5; $i<=50; $i+=5){
			printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i); 
		}
		?>
          </select>
          </span></td>
      </tr>
    </table>
  </form>
  <?php if( !empty($regras)) :?>
  <br />
  <div id="tableObjeto">
    <table width="100%" class="tableSelection">
      <thead>
        <tr>
          <?php if($podeAlterar): ?>
          <td width="7%">Editar</td>
          <?php endif; ?>
          <?php if($podeExcluir): ?>
          <td width="9%">Excluir</td>
          <?php endif; ?>
          <td width="21%">Regra</td>
          <td width="21%">Chave</td>
          <td width="18%">Envia p/ usu&aacute;rio logado</td>
          <td width="33%">Ativo</td>
        </tr>
      </thead>
      <?php foreach($regras as $item): ?>
      <tr>
        <?php if($podeAlterar): ?>
        <td align="center"><a href="<?php echo site_url('regra_email/form/'.$item['ID_REGRA_EMAIL']); ?>"><img src="<?php echo base_url().THEME; ?>img/file_edit.png" alt="Adicionar" width="31" height="31" border="0" /></a></td>
        <?php endif; ?>
        <?php if($podeExcluir): ?>
        <td align="center"><a href="" onclick="remover(<?php echo $item['ID_REGRA_EMAIL']; ?>)"><img src="<?php echo base_url(); ?>img/excluir.gif" alt="Adicionar" width="46" height="35" border="0" /></a></td>
        <?php endif; ?>
        <td align="center"><?php echo $item['DESC_REGRA_EMAIL']; ?></td>
        <td align="center"><?php echo $item['CHAVE_REGRA_EMAIL']; ?></td>
        <td align="center"><?php echo $item['FLAG_ENVIA_LOGADO'] == 1?"Sim":"Nao"; ?></td>
        <td align="center"><?php echo $item['STATUS_REGRA_EMAIL'] == 1?"Ativo":"Inativo"; ?></td>
      </tr>
      <?php endforeach; ?>
    </table>
  </div>
  <span class="paginacao"><?php echo $paginacao; ?></span>
  <?php else: ?>
  Nenhum resultado
  <?php endif; ?>
</div>
<script type="text/javascript">
function remover(id){
	if(confirm('Deseja realmente remover este item?')){
		location.href = '<?php echo base_url().index_page().'/regra_email/delete/' ?>'+id;
	}
}
</script>
<?php $this->load->view("ROOT/layout/footer") ?>
