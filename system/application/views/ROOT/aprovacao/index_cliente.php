<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
  <h1> Aprova&ccedil;&atilde;o</h1>
  <div id="contador" style="padding-bottom:20px;"></div>
  <div id="pesquisa_simples"> <?php echo form_open('aprovacao/listar_cliente');?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="17%" style="text-align:left" align="left"> Ag&ecirc;ncia</td>
        <td width="18%" style="text-align:left">Bandeira</td>
        <td width="12%" style="text-align:left">Campanha</td>
        <td align="left" style="text-align:left">Situa&ccedil;&atilde;o</td>
        <td style="text-align:left">Itens por p&aacute;gina</td>
        <td width="23%">&nbsp;</td>
        <td width="0%" rowspan="4">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" style="text-align:left"><select name="ID_AGENCIA" id="ID_AGENCIA">
          <option value=""></option>
          <?php echo montaOptions($agencias,'ID_AGENCIA','DESC_AGENCIA', !empty($busca['ID_AGENCIA']) ? $busca['ID_AGENCIA'] : ''); ?>
          </select>
          <?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE',false); ?></td>
        <td style="text-align:left"><?php if(!empty($_sessao['ID_PRODUTO'])): ?>
          <?php sessao_hidden('ID_PRODUTO','DESC_PRODUTO'); ?>
          <?php else: ?>
          <select name="ID_PRODUTO" id="ID_PRODUTO">
            <option value=""></option>
            <?php echo montaOptions($produtos,'ID_PRODUTO','DESC_PRODUTO', !empty($busca['ID_PRODUTO']) ? $busca['ID_PRODUTO'] : ''); ?>
          </select>
          <?php endif; ?></td>
        <td style="text-align:left"><select name="ID_CAMPANHA" id="ID_CAMPANHA">
          <option value=""></option>
          <?php echo montaOptions($campanhas,'ID_CAMPANHA','DESC_CAMPANHA', !empty($busca['ID_CAMPANHA']) ? $busca['ID_CAMPANHA'] : ''); ?>
        </select></td>
        <td align="left" style="text-align:left"><select name="STATUS_JOB" id="STATUS_JOB">
          <option value=""></option>
          <?php echo montaOptions($listaStatus,'ID_STATUS','DESC_STATUS', !empty($busca['STATUS_JOB']) ? $busca['STATUS_JOB'] : ''); ?>
        </select></td>
        <td style="text-align:left"><select name="pagina" id="pagina">
          <?php
		$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
		for($i=5; $i<=50; $i+=5){
			printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
		}
		?>
        </select></td>
        <td>
        	<a class="button" href="javascript:" onclick="limparForm()">
        		<span>Limpar</span>
            </a>
        	<a class="button" href="javascript:" onclick="$j(this).closest('form').submit()">
            	<span>Pesquisar</span>
            </a>
        </td>
      </tr>
      <tr>
        <td style="text-align:left">N&uacute;mero Job</td>
        <td style="text-align:left">T&iacute;tulo do Job</td>
        <td style="text-align:left">&nbsp;</td>
        <td align="left" style="text-align:left">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td style="text-align:left"><input type="text" name="ID_JOB" id="ID_JOB" value="<?php echo !empty($busca['ID_JOB']) ? $busca['ID_JOB'] : ''; ?>" /></td>
        <td style="text-align:left"><input type="text" name="DESC_EXCEL" id="DESC_EXCEL" value="<?php echo !empty($busca['DESC_EXCEL']) ? $busca['DESC_EXCEL'] : ''; ?>" /></td>
        <td style="text-align:left">&nbsp;</td>
        <td align="left" style="text-align:left">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
    <br />
  <?php echo form_close();?> </div>
  <?php if( (!empty($lista))&& (is_array($lista))):?>
  <div id="tableObjeto">
    <table border="0" width="100%" cellpadding="0" cellspacing="0" class="tableSelection tableMidia" id="tabelaBusca">
      <thead>
        <tr>
          <?php if($podeAprovar || $verHistorico): ?>
          <th align="center">Aprovar / Histórico</th>
          <?php endif; ?>
          <th class="codigoJob" align="left">N&uacute;mero Job</th>
          <th class="tituloJob" align="left">T&iacute;tulo do Job</th>

          <th class="inicioJob">In&iacute;cio  da Validade</th>

          <th class="clienteJob" align="left">Ag&ecirc;ncia</th>
          <th class="bandeiraJob" align="left">Bandeira</th>
          <th class="campanhaJob" align="left">Campanha</th>
          <th class="terminoJob">Final da Validade</th>
          <th class="etapaJob">Situa&ccedil;&atilde;o</th>
        </tr>
      </thead>
      <?php $cont=0; ?>
      <?php foreach($lista as $item):?>
      <tr>
        <?php if(($podeAprovar || $verHistorico)): ?>
        	<td align="center">
                <a href="<?php echo site_url('aprovacao/form_cliente/'. $item['ID_EXCEL']); ?>">
                    <img src="<?php echo base_url().THEME; ?>img/file_edit.png" alt="Gerar" border="0" />
                </a>
            </td>
        <?php endif; ?>
        <td><?=$item['ID_JOB'];?></td>
        <td><?=$item['TITULO_JOB'];?></td>
        <td><?=format_date($item['DATA_INICIO'],'d/m/Y');?></td>
        <td><?=$item['DESC_AGENCIA'];?></td>
        <td><?=$item['DESC_PRODUTO'];?></td>
        <td><?=$item['DESC_CAMPANHA'];?></td>
        <td><?=format_date($item['DATA_TERMINO'],'d/m/Y');?>
          <br />
          <?= ($diff = strtotime($item['DATA_TERMINO']) - time()) > 0 ? segundosToHora($diff) : '&nbsp;';?></td>
        <td><?=$item['DESC_ETAPA'];?> &gt; <?=$item['DESC_STATUS'] . getRevisaoJob($item);?></td>
        
      </tr>
      <?endforeach;?>
    </table>
  </div>
  <span class="paginacao">
  <?=(isset($paginacao))?$paginacao:null?>
  </span>
  <?else:?>
  N&Atilde;O  H&Aacute; RESULTADO PARA A PESQUISA
  <?endif;?>
  <script type="text/javascript">
	var tempo = 30;
	function atualizar(){
		if( tempo > 0 ){
			$j('#contador').html('Pr&oacute;xima atualiza&ccedil;&atilde;o em ' + tempo + ' ' + (tempo > 1 ? 'segundos' : 'segundo'));
			tempo--;
			ctime = setTimeout('atualizar()', 1000);
		} else {
			if( !getDivModal().dialog('isOpen') ){
				location.href = '<?php echo $_SERVER['REQUEST_URI']; ?>';
			} else {
				ctime = setTimeout('atualizar()', 1000);
			}
		}
		
	}
	
	var ctime = atualizar();
	
	$j("#btnLimpaPesquisa").click(function(){
		limpaForm('#pesquisa_simples');
		if($j("#ID_CLIENTE option").length > 0){
			clearSelect($('ID_CLIENTE'),1);
			clearSelect($('ID_PRODUTO'),1);
		}
		clearSelect($('ID_CAMPANHA'),1);
	});
	
	$('ID_AGENCIA').addEvent('change', function(){
		clearSelect($('ID_PRODUTO'),1);
		clearSelect($('ID_CAMPANHA'),1);
		montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByAgencia'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
	});

	$('ID_PRODUTO').addEvent('change', function(){
		clearSelect($('ID_CAMPANHA'),1);
		montaOptionsAjax($('ID_CAMPANHA'),'<?php echo site_url('json/admin/getCampanhasByProduto'); ?>','id=' + this.value,'ID_CAMPANHA','DESC_CAMPANHA');
	});

function limparForm(){
	$j('input, select[name!="pagina"], textarea').val('');
};

$j(function(){
   configuraPauta('<?php echo $busca['ORDER']; ?>', '<?php echo $busca['ORDER_DIRECTION']; ?>');
});

</script>
</div>
<!-- Final de Content Global -->
<?php $this->load->view("ROOT/layout/footer") ?>
