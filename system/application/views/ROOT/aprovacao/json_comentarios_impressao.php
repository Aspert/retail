<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Impressão de Comentários</title>
<link href="<?=base_url().THEME;?>css/default.css" rel="stylesheet" type="text/css"/>

<style type="text/css">
.tabela_cadastro td {
	padding: 5px;
	vertical-align: top;
}
</style>

</head>

<body style="background-image: none; padding: 10px;">
<div id="contentGlobal">
<p><strong>Imprimir comentários </strong></p>
<p>&nbsp;</p>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabela_cadastro">
	<tr>
		<td width="17%"><strong>Agência</strong></td>
		<td width="83%"><?php echo $job['DESC_AGENCIA'] ?></td>
	</tr>
	<tr>
		<td><strong>Cliente</strong></td>
		<td><?php echo $job['DESC_CLIENTE'] ?></td>
	</tr>
	<tr>
		<td><strong>Job</strong></td>
		<td><?php echo $job['TITULO_JOB'] ?></td>
	</tr>
	<tr>
		<td><strong>Data da prova</strong></td>
		<td><?php echo date('d/m/Y', strtotime($prova['DATA_SOLICITACAO'])); ?></td>
	</tr>
</table>
<br />
<?php foreach($arquivos as $file): ?>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabela_cadastro" style="border: 1px solid gray;">
	<tr>
		<td colspan="2" valign="top" style="border-bottom: 2px solid #666666;"><strong>Comentários do arquivo <?php echo $file['TITULO_APROVACAO_ARQUIVO']; ?></strong></td>
	</tr>
		<?php foreach($file['comentarios'] as $item): ?>
		<tr>
			<td width="23%" valign="top" style="border-bottom: 1px solid #CCCCCC;">
			<?php
			echo '<strong>' . $item['NOME_USUARIO'] . '</strong><br>'
				. date('d/m/Y H:i', strtotime($item['DATA_COMENTARIO'])) . '<br>'
				. (empty($item['DESC_AGENCIA']) ? $item['DESC_CLIENTE'] : $item['DESC_AGENCIA']);
				
			?>
			</td>
			<td width="77%" valign="top" style="border-bottom: 1px solid #CCCCCC;"><?php echo nl2br($item['COMENTARIO']); ?></td>
		</tr>
		<?php endforeach; ?>
</table>
 &nbsp;
 
<?php endforeach; ?>

<div align="center">
	<a href="#" onclick="window.print(); return false;">
		<img src="<?php echo base_url().THEME.'img/printer.png'; ?>" title="Imprimir" />
		Imprimir
	</a>
</div>

</div>
</body>
</html>