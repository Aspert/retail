﻿<?php $this->load->view("ROOT/layout/header") ?>


<div id="contentGlobal">

<?php if ( isset($erros) ) : ?>
	<ul style="color:#ff0000;font-weight:bold;">
		<?php foreach ( $erros as $e ) : ?>
			<li><?=$e?></li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>

<h1>Solicitar Aprova&ccedil;&atilde;o - Job <?php echo $job['ID_JOB']; ?></h1>
<h2><?php echo $job['TITULO_JOB']; ?></h2><br />


<form action="<?php echo site_url('aprovacao/solicitar/'.$job['ID_JOB']); ?>" method="post" enctype="multipart/form-data">
<table width="100%" class="Alignleft">
  <tr>
    <td width="16%"><strong>Cliente</strong></td>
    <td width="84%"><?php echo $job['DESC_CLIENTE']; ?></td>
  </tr>
  <tr>
    <td><strong>Bandeira</strong></td>
    <td><?php echo $job['DESC_PRODUTO']; ?></td>
    </tr>
  <tr>
    <td><strong>Campanha</strong></td>
    <td><?php echo $job['DESC_CAMPANHA']; ?></td>
    </tr>
  <tr>
    <td valign="top"><strong>Observa&ccedil;&otilde;es</strong></td>
    <td><textarea name="OBS_APROVACAO" id="OBS_APROVACAO" cols="55" rows="8"></textarea></td>
    </tr>
  <tr>
    <td><strong>Arquivo para aprova&ccedil;&atilde;o</strong></td>
	<td>

		<div id="swfContainer"></div>
		
		<script type="text/javascript">
		var vars = {};
		vars.sessionID = '<?php echo session_id(); ?>';
		vars.uploadURL = $j('form').attr('action');
		vars.callbackURL = '<?php echo site_url('aprovacao/index') ?>';
		vars.fileFieldName = 'ARQUIVO';
		vars.fileTypes = '*.pdf';
		vars.cancelURL = vars.callbackURL;
		vars.LABEL_ENVIAR = 'Solicitar Aprovação';
		vars.LABEL_CANCELAR = 'Cancelar';

		swfobject.embedSWF("<?php echo base_url(); ?>/flex/upload_single/bin-debug/upload.swf", "swfContainer", "500", "100", "9.0.0",{},vars,{allowscriptaccess:'always'});
		
		var obj = swfobject.getObjectById('swfContainer');
		if( !obj ){
			obj.setFileTypes('*.pdf');
		}
		
		function getFormData(){
			return $j('form').serialize();
		}
		
	</script>
	</td>
    </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td>
		<!--
			<a href="<?php echo site_url('aprovacao/listar_agencia'); ?>" class="button"><span>Cancelar</span></a>
			<a class="button submit" href="javascript:;" onclick="$j(this).closest('form').submit()"><span>Solicitar Aprova&ccedil;&atilde;o</span></a>
		-->
	</td>
    
  </tr>
</table>
</form>
</div> <!-- Final de Content Global -->
<script type="text/javascript">

</script>
<?php $this->load->view("ROOT/layout/footer") ?>