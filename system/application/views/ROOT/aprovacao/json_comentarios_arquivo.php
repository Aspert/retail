<?php foreach($comentarios as $idx => $item): ?>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="<?php echo $idx%2 == 0 ? 'linha_par' : 'linha_impar'; ?>">
	<tr>
		<td width="40%" valign="top" idArquivo="<?= $item['ID_APROVACAO_ARQUIVO']; ?>" class="comentariosArquivos">
			<?php
			echo '<strong>' . $item['NOME_USUARIO'] . '</strong><br>'
				. date('d/m/Y H:i', strtotime($item['DATA_COMENTARIO'])) . '<br>'
				. (empty($item['DESC_AGENCIA']) ? $item['DESC_CLIENTE'] : $item['DESC_AGENCIA']) . '<br>';
			if( isset($item['ComentariosCliente']) && !empty($item['ComentariosCliente'])){
				echo '<strong>Anexos:</strong><br>';
			}
			if( isset($item['ComentariosCliente']) && !empty($item['ComentariosCliente'])){
				foreach($item['ComentariosCliente'] as $comentarioCliente){
					printf('<div style="margin: 5px;"><a href="%s"><img src="%s" title="Baixar Arquivo" alt="Baixar Arquivo" /></a>'
						, site_url('aprovacao/visualizarPDFAP/' . $comentarioCliente['ID_APROVACAO_ARQUIVO'] . '/1/1' )
						, base_url() . THEME . 'img/disc_small.png'
					);
					echo '<a class="comentariosAprovacaoArquivos">' . $comentarioCliente['TITULO_APROVACAO_ARQUIVO'] . '</a></div>';
				}
			}	
			?>
		</td>
		<td width="79%" valign="top" idComentario="<?= $item['ID_APROVACAO_COMENTARIO']; ?>" class="comentariosArquivos">
			<?php echo quebraLinha(limitarQtoCaracteresPorLinha($item['COMENTARIO'],100)); ?>
		</td>
	</tr>
</table>
<?php endforeach; ?>

<?php if(empty($comentarios)): ?>
Nenhum comentário para o arquivo
<br />&nbsp;
<?php endif; ?>

<?php if($podeComentarArquivo): ?>
<form class="formComentario">
	Informe o seu comentário no espaço abaixo
	<textarea style="width: 98%; height: 150px;" name="COMENTARIO"></textarea>
	<br />
</form>
<?php endif; ?>