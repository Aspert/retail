<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
  <h1> Aprova&ccedil;&atilde;o</h1>
  <div id="contador" style="padding-bottom:20px;"></div>
  <div id="pesquisa_simples"> <?php echo form_open('aprovacao/listar_agencia');?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="17%" style="text-align:left" align="left"> Cliente </td>
        <td width="18%" style="text-align:left">Bandeira</td>
        <td width="12%" style="text-align:left">Campanha</td>
        <td align="left" style="text-align:left">Situa&ccedil;&atilde;o</td>
        <td style="text-align:left">Itens por p&aacute;gina</td>
        <td width="23%">&nbsp;</td>
        <td width="0%" rowspan="4">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" style="text-align:left">
          <select name="ID_CLIENTE" id="ID_CLIENTE">
          <option value=""></option>
          <?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', !empty($busca['ID_CLIENTE']) ? $busca['ID_CLIENTE'] : ''); ?>
          </select>
          <?php sessao_hidden('ID_AGENCIA','DESC_AGENCIA',false); ?></td>
        <td style="text-align:left"><?php if(!empty($_sessao['ID_PRODUTO'])): ?>
          <?php sessao_hidden('ID_PRODUTO','DESC_PRODUTO'); ?>
          <?php else: ?>
          <select name="ID_PRODUTO" id="ID_PRODUTO">
            <option value=""></option>
            <?php echo montaOptions($produtos,'ID_PRODUTO','DESC_PRODUTO', !empty($busca['ID_PRODUTO']) ? $busca['ID_PRODUTO'] : ''); ?>
          </select>
          <?php endif; ?></td>
        <td style="text-align:left"><select name="ID_CAMPANHA" id="ID_CAMPANHA">
          <option value=""></option>
          <?php echo montaOptions($campanhas,'ID_CAMPANHA','DESC_CAMPANHA', !empty($busca['ID_CAMPANHA']) ? $busca['ID_CAMPANHA'] : ''); ?>
        </select></td>
        <td align="left" style="text-align:left"><select name="STATUS_JOB" id="STATUS_JOB">
          <option value=""></option>
          <?php echo montaOptions($listaStatus,'ID_STATUS','DESC_STATUS', !empty($busca['STATUS_JOB']) ? $busca['STATUS_JOB'] : ''); ?>
        </select></td>
        <td style="text-align:left"><select name="pagina" id="pagina">
          <?php
		$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
		for($i=5; $i<=50; $i+=5){
			printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
		}
		?>
        </select></td>
        <td><a class="button" href="javascript:" onclick="limparForm()"><span>Limpar</span></a> <a class="button" href="javascript:" onclick="$j(this).closest('form').submit()"><span>Pesquisar</span></a>
          <!-- <img id="btnLimpaPesquisa" src="<?=base_url();?>img/botao_limpar.gif" class="btn" title="Limpar" alt="Limpar" style="padding-bottom: 3px;" />
      	<input class="btn" type="image" name="Pesquisar" id="Pesquisar" src="<?php echo base_url(); ?>img/lupa.gif" /> --></td>
      </tr>
      <tr>
        <td style="text-align:left">N&uacute;mero Job</td>
        <td style="text-align:left">T&iacute;tulo do Job</td>
        <td style="text-align:left">&nbsp;</td>
        <td align="left" style="text-align:left">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td style="text-align:left"><input type="text" name="ID_JOB" id="ID_JOB" value="<?php echo !empty($busca['ID_JOB']) ? $busca['ID_JOB'] : ''; ?>" /></td>
        <td style="text-align:left"><input type="text" name="DESC_EXCEL" id="DESC_EXCEL" value="<?php echo !empty($busca['DESC_EXCEL']) ? $busca['DESC_EXCEL'] : ''; ?>" /></td>
        <td style="text-align:left">&nbsp;</td>
        <td align="left" style="text-align:left">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
    <br />
  <?php echo form_close();?> </div>
  <?php if( (!empty($lista))&& (is_array($lista))):?>
  <div id="tableObjeto">
    <table border="0" width="100%" cellpadding="0" cellspacing="0" class="tableSelection tableMidia" id="tabelaBusca">
      <thead>
        <tr>
          <?php if($podeAprovar): ?>
          <th>Solicitar Aprova&ccedil;&atilde;o</th>
          <?php endif; ?>
          <?php if($verHistorico): ?>
          <th>Hist&oacute;rico</th>
          <?php endif; ?>
          <th class="codigoJob" align="left">N&uacute;mero Job</th>
          <th class="tituloJob" align="left">T&iacute;tulo do Job</th>
          <th class="inicioJob">Data In&iacute;cio</th>
          <th class="clienteJob" align="left">Cliente</th>
          <th class="bandeiraJob" align="left">Bandeira</th>
          <th class="campanhaJob" align="left">Campanha</th>
          <th class="terminoJob">T&eacute;rmino Job</th>
          <th class="etapaJob">Situa&ccedil;&atilde;o</th>
        </tr>
      </thead>
      <?php $cont=0; ?>
      <?php foreach($lista as $item):?>
      <tr>
        <?php if($podeAprovar): ?>
        <td align="center">
        	<? if($item['CHAVE_ETAPA'] == 'ENVIADO_AGENCIA'): ?>
                <a href="<?php echo site_url('aprovacao/form_agencia/'. $item['ID_EXCEL']); ?>">
                    <img src="<?php echo base_url().THEME; ?>img/file_edit.png" alt="Gerar" border="0" />
                </a>
            <? endif; ?>
        </td>
        <?php endif; ?>
        <?php if($verHistorico): ?>
        <td align="center"><a href="<?php echo site_url('aprovacao/historico/'. $item['ID_EXCEL']); ?>"><img src="<?php echo base_url().THEME; ?>img/visualizar_historico.png" alt="Download" border="0" /></a></td>
        <?php endif; ?>
        <td><?=$item['ID_JOB'];?></td>
        <td><?=$item['TITULO_JOB'];?></td>
        <td><?=format_date($item['DATA_INICIO'],'d/m/Y');?></td>
        <td><?=$item['DESC_CLIENTE'];?></td>
        <td><?=$item['DESC_PRODUTO'];?></td>
        <td><?=$item['DESC_CAMPANHA'];?></td>
        <td><?=format_date($item['DATA_TERMINO'],'d/m/Y');?>
          <br />
          <? if(empty($item['DATA_FINALIZADO'])): ?>
          	<?= ($diff = strtotime($item['DATA_TERMINO']) - time()) > 0 ? segundosToHora($diff) : '&nbsp;';?>
          <? else:?>
          	<?= ($diff = strtotime($item['DATA_FINALIZADO']) - time()) > 0 ? segundosToHora($diff) : '&nbsp;';?>
          <? endif; ?>	
        
        </td>
        <td><?=$item['DESC_ETAPA'];?>
&gt;        <?=$item['DESC_STATUS'] . getRevisaoJob($item) ; ?></td>
        
      </tr>
      <?endforeach;?>
    </table>
  </div>
  <span class="paginacao">
  <?=(isset($paginacao))?$paginacao:null?>
  </span>
  <?else:?>
  N&Atilde;O  H&Aacute; RESULTADO PARA A PESQUISA
  <?endif;?>
  <script type="text/javascript">
	var tempo = 30;
	function atualizar(){
		if( tempo > 0 ){
			$j('#contador').html('Pr&oacute;xima atualiza&ccedil;&atilde;o em ' + tempo + ' ' + (tempo > 1 ? 'segundos' : 'segundo'));
			tempo--;
			ctime = setTimeout('atualizar()', 1000);
		} else {
			if( !getDivModal().dialog('isOpen') ){
				location.href = '<?php echo $_SERVER['REQUEST_URI']; ?>';
			} else {
				ctime = setTimeout('atualizar()', 1000);
			}
		}
		
	}
	
	var ctime = atualizar();
	
	$j("#btnLimpaPesquisa").click(function(){
		limpaForm('#pesquisa_simples');
		if($j("#ID_CLIENTE option").length > 0){
			clearSelect($('ID_CLIENTE'),1);
			clearSelect($('ID_PRODUTO'),1);
		}
		clearSelect($('ID_CAMPANHA'),1);
	});
	
	$('ID_CLIENTE').addEvent('change', function(){
		clearSelect($('ID_CAMPANHA'), 1);
		clearSelect($('ID_PRODUTO'), 1);
		montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByCliente'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
	});

	$('ID_PRODUTO').addEvent('change', function(){
		clearSelect($('ID_CAMPANHA'),1);
		montaOptionsAjax($('ID_CAMPANHA'),'<?php echo site_url('json/admin/getCampanhasByProduto'); ?>','id=' + this.value,'ID_CAMPANHA','DESC_CAMPANHA');
	});

function limparForm(){
	$j('input, select[name!="pagina"], textarea').val('');
};

$j(function(){
   configuraPauta('<?php echo $busca['ORDER']; ?>', '<?php echo $busca['ORDER_DIRECTION']; ?>');
});

</script>
</div>
<!-- Final de Content Global -->
<?php $this->load->view("ROOT/layout/footer") ?>
