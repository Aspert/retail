<?php $this->load->view("ROOT/layout/header") ?>


<div id="contentGlobal">

<h1> Histórico - Job [<?php echo $job['ID_JOB']; ?>] <?php echo $job['TITULO_JOB']; ?></h1>
<form id="formAp" action="<?php echo site_url('aprovacao/aprovar/'.$job['ID_JOB']); ?>" method="post" enctype="multipart/form-data">
<table width="100%" class="Alignleft">
    <tr>
      <td width="16%"><strong>Cliente</strong></td>
      <td width="84%"><?php echo $job['DESC_CLIENTE']; ?></td>
      </tr>
    <tr>
      <td><strong>Bandeira</strong></td>
      <td><?php echo $job['DESC_PRODUTO']; ?></td>
      </tr>
    <tr>
      <td><strong>Campanha</strong></td>
      <td><?php echo $job['DESC_CAMPANHA']; ?></td>
      </tr>
    <tr>
      <td height="602" colspan="2" valign="top">
        <div style="float:left;width:550px; height:600px; overflow:hidden; background-color:#CCC">
          <? if(!empty($historico)): ?>
          <object data='<?= site_url('aprovacao/visualizarPDF/'.$job['ID_JOB'].'/0'); ?>#view=Fit&toolbar=1&statusbar=0&messages=1&navpanes=0&.pdf' 
            type='application/pdf' 
            width='550' 
            height='600'>
            	<embed src="<?= site_url('aprovacao/visualizarPDF/'.$job['ID_JOB'].'/0'); ?>#view=Fit&toolbar=1&statusbar=0&messages=1&navpanes=0&.pdf" href="<?= site_url('aprovacao/visualizarPDF/'.$job['ID_JOB'].'/0'); ?>" type="application/pdf" width="550" height="600"></embed>
            </object>
          <? else: ?>
          	<br />
			<br />
			<br />

          	Não há histórico de aprovação para este job.
          
          <? endif; ?>
          </div>
        <div style="width:550px;height:600px; float:right; border:1px #CCC solid;margin-right:0px;">
          <div style="width:540px; height:30px; background-color:#CCC; padding-left:10px; font-size:18px;color:#FFF; padding-top:10px; margin-bottom:3px">Histórico</div>

          <? if(!empty($historico)): ?>
          <div style="height:92%; overflow:auto">
            <? $h = array_shift($historico); ?>
            <div class="ap_box_fundo">
                  <div class="ap_box_header">
                      <div class="ap_box_status">
                        <div class="ap_texto_status"><?= $h['DESC_STATUS']; ?></div>
                        <div class="ap_status_em_aprovacao" style="background-color:#<?= $h['COR_STATUS']; ?>"></div>
                      </div>
                      <p class="ap_style_box_fundo"> 
                        <span class="ap_destaque_texto">Nome:</span> <?= $h['NOME_USUARIO']; ?> (<?= !empty($h['U_DESC_AGENCIA']) ? $h['U_DESC_AGENCIA'] : $h['U_DESC_CLIENTE']; ?>) <br />
                        <span class="ap_destaque_texto">Data:</span> <?= format_date_to_form($h['DATA_APROVACAO'],'d/m/Y'); ?> | 
                        <span class="ap_destaque_texto">Horário:</span> <?= format_date_to_form($h['DATA_APROVACAO'],'H:i:s'); ?>
                      </p>
                  </div>
                  <div class="ap_box_frente"> <span class="ap_style_box_frente"> <span class="ap_titulo_mensagem">Mensagem:</span><br />
                    <span class="ap_mensagem"><?= nl2br($h['OBS_APROVACAO'])?></span> </span> 
                  </div>
            </div>
            
            <? foreach($historico as $h):?>
                <div class="ap_box_fundo">
                  <div class="ap_box_header">
                      <div class="ap_box_status">
                        <div class="ap_texto_status"><?= $h['DESC_STATUS']; ?></div>
                        <div class="ap_status_em_aprovacao" style="background-color:#<?= $h['COR_STATUS']; ?>"></div>
                      </div>
                      <p class="ap_style_box_fundo"> 
                        <span class="ap_destaque_texto">Nome:</span> <?= $h['NOME_USUARIO']; ?> (<?= !empty($h['U_DESC_AGENCIA']) ? $h['U_DESC_AGENCIA'] : $h['U_DESC_CLIENTE']; ?>) <br />
                        <span class="ap_destaque_texto">Data:</span> <?= format_date_to_form($h['DATA_APROVACAO'],'d/m/Y'); ?> | 
                        <span class="ap_destaque_texto">Horário:</span> <?= format_date_to_form($h['DATA_APROVACAO'],'H:i:s'); ?>
                      </p>
                  </div>
                  <div class="ap_box_frente"> <span class="ap_style_box_frente"> <span class="ap_titulo_mensagem">Mensagem:</span><br />
                    <span class="ap_mensagem"><?= nl2br($h['OBS_APROVACAO'])?></span> </span> 
                  </div>
                  <? if($h['U_AGENCIA'] && $podeVisualizarPDFAP): ?>
                      <div class="ap_box_download">
                        <div class="ap_texto_download">Clique aqui para fazer o download do arquivo!</div>
                        <a href="<? echo site_url('aprovacao/visualizarPDFAP/'. $h['ID_APROVACAO']).'/1'; ?>">
                        <div class="ap_baixar_pdf"></div>
                        </a> 
                      </div>
				  <? endif; ?>
                </div>

            <? endforeach; ?>
            </div>
          <? else:?>
          Não há histórico de aprovação para este job.
          <? endif; ?>
          
          </div>
        </td>
      </tr>
    <tr>
      <td colspan="2" valign="top">
        <a href="<?php echo site_url('aprovacao/listar_agencia'); ?>" class="button"><span>Voltar</span></a></td>
    </tr>
  </table>
</form>
</div> <!-- Final de Content Global -->
<script type="text/javascript">

	
</script>
<?php $this->load->view("ROOT/layout/footer") ?>