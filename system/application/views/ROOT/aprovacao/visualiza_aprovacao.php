<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">

<h1> Aprova&ccedil;&atilde;o - Job [<?php echo $job['ID_JOB']; ?>] <?php echo $job['TITULO_JOB']; ?></h1>
<h2>&nbsp;</h2>
<table width="100%" class="Alignleft">
    <tr>
      <td width="16%"><strong>Ag&ecirc;ncia</strong></td>
      <td width="84%"><?php echo $job['DESC_AGENCIA']; ?></td>
      </tr>
    <tr>
      <td><strong>Bandeira</strong></td>
      <td><?php echo $job['DESC_PRODUTO']; ?></td>
      </tr>
    <tr>
      <td><strong>Campanha</strong></td>
      <td><?php echo $job['DESC_CAMPANHA']; ?></td>
      </tr>
    <tr>
      <td colspan="2" valign="top">
	  	<div id="tabs">
			<?php
			$total = count($historico);
			
			echo '<ul>';
			
			foreach($historico as $item){
				echo '<li><a href="#aprovacao-'.$item['ID_APROVACAO'].'">Prova #' . ($total--). ' - ' . date('d/m/Y',strtotime($item['DATA_SOLICITACAO'])) . '</a></li>';
			}
			
			echo '</ul>';
			
			foreach($historico as $item) :
			?>
			<div id="aprovacao-<?php echo $item['ID_APROVACAO']; ?>">
				
				<!-- mostra o nome do arquivo -->
				<div>
					<?php if(!empty($item['arquivos'])){?>
					<table width="100%" class="Alignleft">
						<tr>
							<td style="width: 4%;">
								<a id="visualizarPDFAP-<?php echo $item['ID_APROVACAO']; ?>" href="<?= site_url('aprovacao/visualizarPDFAP') . '/' . $item['arquivos'][0]['ID_APROVACAO_ARQUIVO'] . '/0/arquivo.pdf'?>" href="javascript:">
									<img border="0" src="http://retail.247id.com.br/themes/snow_white/img/disc.png">
								</a>
							</td>
							<td>
								<h1 class="both mostraNomeArquivo">&nbsp;</h1>
							</td>
						</tr>
					</table>
					<?php } else {?>
					Nenhum arquivo foi enviado para a prova
					<?php }?>
				</div>
				
				<!-- aqui vai o PDF -->
				<!-- <div class="containerPDF">&nbsp;</div> -->
				
				<!-- espaco entre o pdf e a paginacao -->
				<div>&nbsp;<br />&nbsp;</div>
				
				<?php if(!empty($item['arquivos'])){?>
				<!-- titulo de arquivos da revisao -->
				<div class="both"><strong>Arquivos desta revisão:</strong></div>
				<?php }?>
				
				<!-- espaco entre o titulo e as paginas -->
				<div>&nbsp;</div>
				
				<?php
				foreach($item['arquivos'] as $idx => $file){
					printf('<a href="#" alt="%s" onclick="abreArquivo(%d, this,'.$item['ID_APROVACAO'].'); return false;" class="aprovacaoArquivoItem"> %02d </a>'
						, $file['TITULO_APROVACAO_ARQUIVO']
						, $file['ID_APROVACAO_ARQUIVO']
						, $idx + 1
					);
				}
				?>
				<div class="both">&nbsp;</div>
				
				<div style="float: right;">
					<?php if(!empty($item['arquivos'])){?>
					<a href="#" onclick="imprimirComentarios('arquivo', -1); return false;" class="button"><span>Imprimir comentários deste arquivo</span></a>
					<a href="#" onclick="imprimirComentarios('prova',<?php echo $file['ID_APROVACAO']; ?>); return false;" class="button"><span>Imprimir comentários desta prova</span></a>
					<?php }?>
				</div>
				
				<div class="both">&nbsp;</div>
				
				<?php if(!empty($item['arquivos'])){?>
				<h1>Comentários deste arquivo</h1>
				<div class="comentarios"></div>
				
				<div class="both">&nbsp;</div>
				<form id="formComentario-<?php echo $item['ID_APROVACAO']; ?>">
					<div id="upload"></div>
					<div id="addArquivos-<?php echo $item['ID_APROVACAO']; ?>">
						<div>Anexar Arquivos</div>
					    <div id="newFile-<?php echo $item['ID_APROVACAO']; ?>">
							<div id="arquivo-<?php echo $item['ID_APROVACAO']; ?>-1" class="arquivo">
								<input type="file"  name="file-<?php echo $item['ID_APROVACAO']; ?>-1"/>
					      		<a id="novoArquivo-<?php echo $item['ID_APROVACAO']; ?>-1" onclick="adicionarArquivo(this);"><img border="0" src="<?php echo base_url().THEME; ?>img/add.png"></a>
					      		<br /><br />
					      		<a href="#" class="button" onclick="enviarComentario(); return false;"><span>Enviar Comentário</span></a>
					      		<br /><br />
							</div>
					    </div>
					</div>
				</form>
				<?php }?>
			</div>
			<?php endforeach; ?>
		</div>
        
        </td>
      </tr>
    <tr>
      <td colspan="2" valign="top">
	  <form id="formAp" action="<?php echo site_url('aprovacao/aprovar/'.$job['ID_JOB']); ?>" method="post" enctype="multipart/form-data">
        <a href="<?php echo site_url('aprovacao/listar_cliente'); ?>" class="button"><span>Cancelar</span></a>
        <a href="<?php echo site_url('aprovacao/proxima_etapa/'.$job['ID_JOB'].'/1'); ?>" class="button"><span>Próxima Etapa</span></a>
        <a href="<?php echo site_url('aprovacao/proxima_etapa/'.$job['ID_JOB'].'/2'); ?>" class="button"><span>Voltar Etapa</span></a>
		<input type="hidden" name="acao" id="acao" value="" />
  	</form>
        </td>
      </tr>
  </table>
  

</div> <!-- Final de Content Global -->

<div class="popNomeArquivo"></div>

<!-- Inicio modelo novo arquivo -->
<div id="modeloNovoArquivo" class="arquivo">
	<input type="file" name="file"/>
    <a id="novoArquivo" onclick="adicionarArquivo(this);"><img border="0" src="<?php echo base_url().THEME; ?>img/add.png"></a>
</div>
<!-- Fim modelo novo arquivo -->

<script type="text/javascript">
	var arquivoAtual = null;
	var idAprovacao = null;
	var linkAtual    = null;
	var panelAtual   = null;
	var podeComentar = <?php printf('%d', $podeComentarArquivo); ?>;
	var modeloNovoArquivo = null;
	
	modeloNovoArquivo = $j('#modeloNovoArquivo').clone();
	$j('#modeloNovoArquivo').remove();

	//mermão uma chatisse daquelas para adicionar mais de 1 arquivo de forma dinâmica
	function adicionarArquivo(teste){
		var array = $j(teste).attr('id').split('-');
		var cont = $j("div[id^='arquivo-"+array[1]+"']").length + 1;
		
		$j(modeloNovoArquivo.clone()).prependTo($j('#newFile-'+array[1]));
		$j('#modeloNovoArquivo').find('#novoArquivo').attr('id','novoArquivo-'+array[1]+'-'+cont);
		$j('#modeloNovoArquivo').find('input').attr('name','file-'+array[1]+'-'+cont);
		$j('#modeloNovoArquivo').attr('id','arquivo-'+array[1]+'-'+cont);
	}
	
	function imprimirComentarios(tipo, id){
		if( tipo == 'arquivo' ){
			id = arquivoAtual;
		}
		window.open('<?php echo site_url('json/aprovacao/imprimir_comentarios'); ?>/'+tipo+'/'+id,'','width=800,height=600,scrollbars=yes,resizeable=yes');
	}
	
	var tipoEscolhido = null;
	var  windowAllegria = null;
	function aprovacao(tipo){
		tipoEscolhido = tipo;
		windowAllegria = window.open('<?php echo site_url('checklist/aprovacao/'.$job['ID_JOB']); ?>/' + tipo + '/1','allegriawin','left=20,top=20,width=700,height=550,resizable=no');
	}

	function finalizarAprovacao(comentario, etapa){
		
		var erros = [];
		if(comentario == '') erros.push('Informe algum comentario');
		if(etapa == '') erros.push('Informe a etapa');
		
		if( erros.length > 0 ){
			alert( erros.join('\n') );
			windowAllegria.focus();
			
		} else {
			var turl = '<?php echo site_url('checklist/aprovacao/'.$job['ID_JOB']); ?>/' + tipoEscolhido;
			//alert(windowAllegria.jQuery('#formAprovacao').serialize());
			$j.post(turl,
				windowAllegria.jQuery('#formAprovacao').serialize(),
				function(html){
					alert("Alteração solicitada!");
					location.href = '<?php echo site_url('checklist/index'); ?>';
					windowAllegria.close();
				}
			);
		}
	}
	
	//devolve para a agencia com solicitação de alteração
	function solicitarAlteracao(){
		$j('#acao').val(0);
		$j('#formAp').submit();
	}
	
	//aprova o job e devolve para a agencia
	function aprovar(){
		$j('#acao').val(1);
		if(confirm('Deseja aprovar o job?')){
			$j('#formAp').submit();
		}
	}
	
	function showNomeArquivo(){
		var offset = $j(this).offset();
		
		$j('.popNomeArquivo').html($j(this).attr('alt'))
			.css('left', offset.left - $j(this).parent().offset().left)
			.css('top', offset.top - $j(this).height() - 10)
			.show();
		
	}
	
	function hideNomeArquivo(){
		$j('.popNomeArquivo').hide();
	}
	
	function comentarArquivoAtual(){
		$j('#popComentario').dialog({
			title: 'Comentar arquivo',
			width: 500,
			modal: false,
			autoOpen: false,
			bgiframe: true
		}).dialog('open');
		
	}
	
	function abreArquivo(idarquivo, anc, IdAprovacao){
		arquivoAtual = idarquivo;
		linkAtual = anc;
		idAprovacao = IdAprovacao;
		
		$j("a[id^='visualizarPDFAP-']").attr('href','<?= site_url('aprovacao/visualizarPDFAP')?>/'+idarquivo+'/0/arquivo.pdf');
		
		$j('.aprovacaoArquivoItem').removeClass('aprovacaoArquivoItemAtivo');
		$j(anc).addClass('aprovacaoArquivoItemAtivo');
		
		// mostra o nome do arquivo
		$j('.mostraNomeArquivo').html( 'Baixar PDF: ' + $j(anc).attr('alt'));
		
		// muda a visualizacao do arquivo
		visualizaArquivo( idarquivo, $j(anc).closest('div') );
		
		// container de comentarios
		var containerComentarios = $j(anc).closest('div').find('.comentarios');
		
		// tira os comentarios
		containerComentarios.html('Carregando');
		
		$j.post('<?php echo site_url('json/aprovacao/get_comentarios_arquivo'); ?>/' + idarquivo + '/' + podeComentar, null , function(html){
			// atualiza os comentarios
			containerComentarios.html( html );
		});

		
	}
	
	function visualizaArquivo(idarquivo, container){
		var src = '<?= site_url('aprovacao/visualizarPDFAP'); ?>/' + idarquivo + '/0/arquivo.pdf';

		$j(container)
			.find('.containerPDF')
			.html('<object data="'+src+'" '+
					'type="application/pdf" '+
					'width="100%" '+
					'height="600"> '+
					'<embed src="'+src+'" type="application/pdf" width="100%" height="600"></embed>' +
					'</object>');
	}
	
	function enviarComentario(){
		if( panelAtual.find('.formComentario textarea').val() == '' ){
			alert('Informe o comentário');
			return;
		}

		$j("form[id^='formComentario-"+idAprovacao+"']").append($j('.formComentario textarea'));
		fileUpload($j("form[id^='formComentario-"+idAprovacao+"']"),'<?php echo site_url('aprovacao/comentarArquivo/'); ?>/'+ arquivoAtual+'/'+idAprovacao,'upload');
		
	    abreArquivo(arquivoAtual, linkAtual,idAprovacao);
	    $j("form[id^='formComentario-"+idAprovacao+"'] textarea").remove();
	    $j('#upload').hide();
	    $j("input[name^='file-']").val('');
	}

	function fileUpload(form, action_url, div_id) {
	    // Create the iframe...
	    var iframe = document.createElement("iframe");
	    iframe.setAttribute("id", "upload_iframe");
	    iframe.setAttribute("name", "upload_iframe");
	    iframe.setAttribute("width", "0");
	    iframe.setAttribute("height", "0");
	    iframe.setAttribute("border", "0");
	    iframe.setAttribute("style", "width: 0; height: 0; border: none;");
	 
	    // Add to document...
	    form[0].parentNode.appendChild(iframe);
	    window.frames['upload_iframe'].name = "upload_iframe";
	 
	    iframeId = document.getElementById("upload_iframe");
	 
	    // Add event...
	    var eventHandler = function () {
	 
	            if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
	            else iframeId.removeEventListener("load", eventHandler, false);
	 
	            // Message from server...
	            if (iframeId.contentDocument) {
	                content = iframeId.contentDocument.body.innerHTML;
	            } else if (iframeId.contentWindow) {
	                content = iframeId.contentWindow.document.body.innerHTML;
	            } else if (iframeId.document) {
	                content = iframeId.document.body.innerHTML;
	            }
	 
	            document.getElementById(div_id).innerHTML = content;
	 
	            // Del the iframe...
	            setTimeout('iframeId.parentNode.removeChild(iframeId)', 250);
	        }
	 
	    if (iframeId.addEventListener) iframeId.addEventListener("load", eventHandler, true);
	    if (iframeId.attachEvent) iframeId.attachEvent("onload", eventHandler);
	 
	    // Set properties of form...
	    form[0].setAttribute("target", "upload_iframe");
	    form[0].setAttribute("action", action_url);
	    form[0].setAttribute("method", "post");
	    form[0].setAttribute("enctype", "multipart/form-data");
	    form[0].setAttribute("encoding", "multipart/form-data");
	 
	    // Submit the form...
	    form[0].submit();
	 
	    document.getElementById(div_id).innerHTML = "Uploading...";
	}
	
	$j(function(){
		$j('#tabs').tabs({
			show: function(evt, ui){
				$j(ui.panel).find('.aprovacaoArquivoItem:first').click();
				panelAtual = $j(ui.panel);
			}
		});
		
		$j('.popNomeArquivo').hide();
		$j('#popComentario').hide();
		$j('.aprovacaoArquivoItem')
			.mouseover(showNomeArquivo)
			.mouseout(hideNomeArquivo)
			
		// abre o primeiro arquivo
		$j('.aprovacaoArquivoItem:first').click();
	});
	
	
</script>
<?php $this->load->view("ROOT/layout/footer") ?>