﻿<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
  <?php if ( isset($erros) ) : ?>
  <ul style="color:#ff0000;font-weight:bold;">
    <?php foreach ( $erros as $e ) : ?>
    <li>
      <?=$e?>
    </li>
    <?php endforeach; ?>
  </ul>
  <?php endif; ?>
  <h1>Solicitar Aprova&ccedil;&atilde;o - Job <?php echo $job['ID_JOB']; ?></h1>
  <h2><?php echo $job['TITULO_JOB']; ?></h2>
  <br />
  <div style="width:55%; float:left">
    <div style="width:100%">
      <div> <strong>Cliente</strong> <?php echo $job['DESC_CLIENTE']; ?> </div>
      <div> <strong>Bandeira</strong> <?php echo $job['DESC_PRODUTO']; ?> </div>
      <div> <strong>Campanha</strong> <?php echo $job['DESC_CAMPANHA']; ?> </div>
      <br /><br />
      <div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
	      <?php 
	      $total = count($historico);
	      	
	      echo '<ul class="">';
	      //$count = 1;
	      echo '<li class="aprovacao ui-corner-top ui-state-default" id="aprovacao-'.$idAprovacaoNaoEnviado.'"><a>Nova Prova</a></li>';
	      foreach($historico as $item){
					echo '<li class="aprovacao ui-corner-top ui-state-default"  id="aprovacao-'.$item['ID_APROVACAO'].'"><a>Prova #' . ($total--). ' - ' . date('d/m/Y',strtotime($item['DATA_SOLICITACAO'])) . '</a></li>';
		  }
	      	
	      echo '</ul>';
	      foreach($historico as $item) :
	      ?>
	      
	      <div id="aprovacao-<?php echo $item['ID_APROVACAO']; ?>" >
	      
	       </div>
    <?php endforeach; ?>
	</div>
      
      <div class="enviarArquivo"><strong>Enviar Arquivo</strong></div>
    </div>
    <? if($podeEnviarArquivo): ?>
    <form id="formArquivo" action="<?php echo site_url('aprovacao/enviar_arquivo/'.$ap['ID_APROVACAO']); ?>" method="post" enctype="multipart/form-data">
      <div style="background-color:#FFFFFF; padding:10px 10px 10px 10px" class="enviarArquivo">
        <!--<input type="file" name="ARQUIVO" id="ARQUIVO" width="80" size="80" />
          <br />-->
        Comentários<br />
        <textarea name="COMENTARIO" id="COMENTARIO" cols="85" rows="8"></textarea>
        <br />
        <br />
        <!--<a class="button submit" href="javascript:;" onclick="$j(this).closest('form').submit()"><span>Enviar Arquivo</span></a> <br />
          <br />-->
        <div id="swfContainer" style="clear:both; margin-left:-20px"></div>
      </div>
      <div style="display: none;background-color:#FFFFFF; padding:10px 10px 10px 10px;height: 200px;" id="arquivosEnviados">
	      	<!-- mostra o nome do arquivo -->
			<div>
				<?php if(!empty($arquivos)){?>
				<table width="100%" class="Alignleft">
					<tr>
						<td style="width: 4%;">
							<a id="visualizarPDFAP-<?php echo $arquivos[0]['ID_APROVACAO']; ?>" href="<?= site_url('aprovacao/visualizarPDFAP') . '/' . $arquivos[0]['ID_APROVACAO_ARQUIVO'] . '/0/arquivo.pdf'?>" href="javascript:">
								<img border="0" src="http://retailh.247id.com.br/themes/snow_white/img/disc.png">
							</a>
						</td>
						<td>
							<h1 style="margin: 0;" class="both mostraNomeArquivo"><?= $arquivos[0]['TITULO_APROVACAO_ARQUIVO']; ?></h1>
						</td>
					</tr>
				</table>
				<?php } else {?>
				Nenhum arquivo foi enviado para a prova
				<?php }?>
			</div>
			
			<!-- aqui vai o PDF -->
			<!-- <div class="containerPDF">&nbsp;</div> -->
			
			<!-- espaco entre o pdf e a paginacao -->
			<div>&nbsp;<br />&nbsp;</div>
			
			<!-- titulo de arquivos da revisao -->
			<div class="both">
				<?php if(!empty($arquivos)) {?>
					<strong>Arquivos desta revisão:</strong>
				<?php }?>
			</div>
			
			<!-- espaco entre o titulo e as paginas -->
			<div>&nbsp;</div>
	      	<?php
	      	if($this->uri->segment(3) != $idAprovacaoNaoEnviado){
				foreach($arquivos as $idx => $file){
					printf('<a href="#" alt="%s" onclick="abreArquivo(%d, this,'.$item['ID_APROVACAO'].'); return false;" class="aprovacaoArquivoItem"> %02d </a>'
						, $file['TITULO_APROVACAO_ARQUIVO']
						, $file['ID_APROVACAO_ARQUIVO']
						, $idx + 1
					);
				}
			}
			?>
		
      </div>
      
      <?php if(!empty($arquivos)) {?>
      	<h1>Comentários desta prova</h1>
      <?php }?>
      <div style="width: 1100px;" class="comentarios">&nbsp;</div>
    </form>
    <div>
    	<br>
    	<br>
    </div>
    <script type="text/javascript">
		var vars = {};
		vars.sessionID = '<?php echo session_id(); ?>';
		vars.uploadURL = $j('#formArquivo').attr('action');
		vars.callbackURL = '<?php echo site_url('aprovacao/form_arquivo/'.$ap['ID_APROVACAO']) ?>';
		vars.fileFieldName = 'ARQUIVO';
		vars.fileTypes = '*.pdf';
		vars.cancelURL = vars.callbackURL;
		vars.LABEL_ENVIAR = 'Enviar Arquivo';
		vars.LABEL_CANCELAR = '';

		swfobject.embedSWF("<?php echo base_url(); ?>/flex/upload_single/bin-debug/upload.swf", "swfContainer", "100%", "100", "9.0.0",{},vars,{allowscriptaccess:'always',wmode:'opaque'});
		
		var obj = swfobject.getObjectById('swfContainer');
		if( !obj && obj != null){
			console.log(obj);
			obj.setFileTypes('*.pdf');
		}
		
		function getFormData(){
			return $j('form').serialize();
		}
		
	</script>
    <? endif; ?>
   
    <form action="<?php echo site_url('aprovacao/solicitar/'.$job['ID_JOB'].'/'.$ap['ID_APROVACAO']); ?>" method="post" enctype="multipart/form-data">
      <a href="<?php echo site_url('aprovacao/listar_agencia'); ?>" class="button"><span>Cancelar</span></a>
      <a class="button submit" href="javascript:;" onclick="$j(this).closest('form').submit()"><span>Solicitar Aprova&ccedil;&atilde;o</span></a>
    	<?php if(!empty($arquivos)){?>
    		<a id="copiarUrl" class="button" data-clipboard-text="<?= $URL?>" ><span>Copiar URL</span></a>
    	<?php }?>
    </form>

	<div style="clear:both"></div>

  </div>
  <div style="width:450px;height:300px; float:right;">
  <div><strong>Arquivos enviados</strong></div>
  	<div style="height:300px;border:1px #CCCCCC dashed; overflow:auto">
  	
    <div style="width:430px; padding:5px 5px 5px 5px; ">
      <? foreach($arquivos as $arquivo): ?>
      <div style="clear:both" ></div>
      <div style="width:98%; height:15px; padding-top:2px;padding-bottom:2px; color:#666666" class="trAlternada">
        <? if($podeDownload): ?>
        <a href="<?= site_url('aprovacao/visualizarPDFAP/'.$arquivo['ID_APROVACAO_ARQUIVO']); ?>" style="color:#666666">
        <?= $arquivo['TITULO_APROVACAO_ARQUIVO']?>
        </a>
        <? else: ?>
        <?= $arquivo['TITULO_APROVACAO_ARQUIVO']?>
        <? endif; ?>
        &nbsp;&nbsp;&nbsp;
        <? if($podeExcluir): ?>
        <img src="<?php echo base_url().THEME; ?>img/close-256x256.png" onclick="excluir(<?= $arquivo['ID_APROVACAO_ARQUIVO']; ?>)" style="float:right;margin-right:10px;cursor:pointer"/>
        <? endif; ?>
      </div>
      <? endforeach; ?>
    </div>
  </div></div>
</div>
<br />
<br />
<!-- Final de Content Global -->
<script type="text/javascript">
var arquivoAtual = null;
var idAprovacao = null;
var linkAtual    = null;
<? if($podeExcluir): ?>
function excluir(id){
	if(confirm('Deseja realmente excluir este arquivo?')){
		location.href = '<?= site_url('aprovacao/excluir_arquivo/') ?>/'+id;
	}	
}
<? endif; ?>

$j('div.trAlternada:even').addClass("linha_par_aq");
$j('div.trAlternada:odd').addClass("linha_impar_aq");

$j(function(){
	$j('#tabs').tabs({
		show: function(evt, ui){
			$j(ui.panel).find('.aprovacaoArquivoItem:first').click();
			panelAtual = $j(ui.panel);
		}
	});

	$j('#aprovacao-'+<?= $this->uri->segment(3)?>).attr('class','aprovacao ui-state-default ui-corner-top ui-tabs-selected ui-state-active');

	//aqui fazemos uma senhora gambi para que seja copiado a url para o ClipBoard
	var client = new ZeroClipboard($j("#copiarUrl"));
	client.on( 'copy', function ( event ) {
		event.client.setData('text/plain', $j("#copiarUrl"));
		alert($j("#copiarUrl").attr('data-clipboard-text')+"\n\n"+"Foi copiado com sucesso!");
	});

	
	var novaAP = '<?= $idAprovacaoNaoEnviado;?>';
	if($j('#aprovacao-'+<?= $this->uri->segment(3)?>).attr('id').substring(10) != novaAP){
		$j('.enviarArquivo').hide();
		$j('#arquivosEnviados').show();
	}


	// abre o primeiro arquivo
	$j('.aprovacaoArquivoItem:first').click();


	// container de comentarios
	var containerComentarios = $j('div').find('.comentarios');
	
	// tira os comentarios
	containerComentarios.html('Carregando');

	$j.post('<?php echo site_url('json/aprovacao/get_comentarios_aprovacao'); ?>/' + <?= $this->uri->segment(3)?> + '/' + 0, null , function(html){
		// atualiza os comentarios
		containerComentarios.html( html );
	});
});

function abreArquivo(idarquivo, anc, IdAprovacao){
	arquivoAtual = idarquivo;
	linkAtual = anc;
	idAprovacao = IdAprovacao;
	
	$j("a[id^='visualizarPDFAP-']").attr('href','<?= site_url('aprovacao/visualizarPDFAP')?>/'+idarquivo+'/0/arquivo.pdf');
	
	$j('.aprovacaoArquivoItem').removeClass('aprovacaoArquivoItemAtivo');
	$j(anc).addClass('aprovacaoArquivoItemAtivo');
	
	// mostra o nome do arquivo
	$j('.mostraNomeArquivo').html( 'Baixar PDF: ' + $j(anc).attr('alt'));
	
	// muda a visualizacao do arquivo
	visualizaArquivo( idarquivo, $j(anc).closest('div') );
	
	// container de comentarios
	var containerComentarios = $j('div').find('.comentarios');
	
	// tira os comentarios
	containerComentarios.html('Carregando');

	$j.post('<?php echo site_url('json/aprovacao/get_comentarios_arquivo'); ?>/' + idarquivo + '/' + 0, null , function(html){
		// atualiza os comentarios
		containerComentarios.html( html );
	});
	
}

function visualizaArquivo(idarquivo, container){
	var src = '<?= site_url('aprovacao/visualizarPDFAP'); ?>/' + idarquivo + '/0/arquivo.pdf';

	$j(container)
		.find('.containerPDF')
		.html('<object data="'+src+'" '+
				'type="application/pdf" '+
				'width="100%" '+
				'height="600"> '+
				'<embed src="'+src+'" type="application/pdf" width="100%" height="600"></embed>' +
				'</object>');
}

$j(".aprovacao").click(function(){
	//retiramos o click de todas as abas
	$j("li[id^='aprovacao-']").attr('class','aprovacao ui-corner-top ui-state-default');
	//setamos o click apenas na aba selecionada
	$j(this).attr('class','aprovacao ui-state-default ui-corner-top ui-tabs-selected ui-state-active');

	//aqui redirecionamos para a aba selecionada
	window.location.replace('<?php echo site_url('aprovacao/form_arquivo/'); ?>/' + $j(this).attr('id').substring(10));	
	
	var novaAP = '<?= $idAprovacaoNaoEnviado;?>';
	if($j(this).attr('id').substring(10) != novaAP){
		$j('#enviarArquivo').hide();
	}
});
</script>

<?php $this->load->view("ROOT/layout/footer") ?>
