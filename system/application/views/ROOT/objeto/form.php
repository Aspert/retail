<?php $this->load->view("ROOT/layout/header"); ?>

<h1>Edi&ccedil;&atilde;o de <?php ghDigitalReplace($_sessao, 'Objetos'); ?></h1>

<div id="contentGlobal">

<div id="contentEditobj">

<?php if ( isset($erros) && is_array($erros) ) : ?>
	<ul style="color:#ff0000;font-weight:bold;">
		<?php foreach ( $erros as $e ) : ?>
			<li><?=$e?></li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>
	
	<?php echo form_open('objeto/save/'.$this->uri->segment(3), array('id'=>'form'));?>

<input name="CODIGO" id="CODIGO" type="hidden" value="<?php post('CODIGO'); ?>" />

<table width="80%" border="0" cellspacing="0" cellpadding="0" id="tableEditobj">
  <tr>
    <td>&nbsp;</td>
    <td width="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td width="20">&nbsp;</td>
    <td rowspan="12">
       	<a href="<?= base_url() ?>img.php?img_id=<?= $id; ?>&rand=<?= rand()?>&img_size=big&a.jpg" class="jqzoom">
			<img src="<?= base_url() ?>img.php?img_id=<?= $id; ?>&rand=<?= rand()?>&a.jpg" width="60" border="0">
		</a>
	</td>
  </tr>
  <tr>
    <td><strong><?php ghDigitalReplace($_sessao, 'Cliente'); ?>:</strong></td>
    <td>&nbsp;</td>
    <td>
    	            <?php
			echo val($cliente,'DESC_CLIENTE');
			printf('<input type="hidden" name="ID_CLIENTE" id="ID_CLIENTE" value="%d" />', val($cliente,'ID_CLIENTE'));
            ?>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong>Tipo:</strong></td>
    <td>&nbsp;</td>
    <td>
    				<?php
			echo val($tipo,'DESC_TIPO_OBJETO');
			printf('<input type="hidden" name="ID_TIPO_OBJETO" id="ID_TIPO_OBJETO" value="%d" />', val($tipo,'ID_TIPO_OBJETO'));
            ?>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong>Nome do arquivo:</strong></td>
    <td>&nbsp;</td>
    <td><?= str_replace(' ','&nbsp;',$nome);?></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><?php ghDigitalReplace($_sessao, 'Categoria'); ?>:</strong></td>
    <td>&nbsp;</td>
    <td>    
    	<select name="ID_CATEGORIA" id="ID_CATEGORIA">
        <option value=""></option>
        <?php echo montaOptions($categorias,'ID_CATEGORIA','DESC_CATEGORIA', post('ID_CATEGORIA',true)); ?>
        </select>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><?php ghDigitalReplace($_sessao, 'SubCategoria'); ?></strong></td>
    <td>&nbsp;</td>
    <td>
    	     <select name="ID_SUBCATEGORIA" id="ID_SUBCATEGORIA">
            <option value=""></option>
            <?php echo montaOptions($subcategorias,'ID_SUBCATEGORIA','DESC_SUBCATEGORIA', post('ID_SUBCATEGORIA',true)); ?>
            </select>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong>Fabricante:</strong></td>
    <td>&nbsp;</td>
    <td>
    				<select id="ID_FABRICANTE" name="ID_FABRICANTE">
				<option value=""></option>
				<?= montaOptions($fabricantes, 'ID_FABRICANTE','DESC_FABRICANTE', post('ID_FABRICANTE',true)); ?>
			</select>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong>Marca:</strong></td>
    <td>&nbsp;</td>
    <td>
    				<select id="ID_MARCA" name="ID_MARCA">
				<option value=""></option>
				<?= montaOptions($marcas, 'ID_MARCA','DESC_MARCA', post('ID_MARCA',true)); ?>
			</select>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong>Keywords:</strong></td>
    <td>&nbsp;</td>
    <td>
    	<?= form_textarea(array('name'=>'KEYWORDS','value'=>post('KEYWORDS',true), 'class' => 'textareaEdit')) ?>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
    	   	<? if($podeSalvar):?>
		<a class="button" href="javascript:" onclick="$j(this).closest('form').submit();"><span>Salvar</span></a>
	<? endif; ?>
	<? if($podeEmail):?>
        <a class="button" href="javascript:new Util().vazio()" onclick="$j('#form').attr('action','<?= base_url().index_page() ?>/objeto/salvarComEmail/<?= $this->uri->segment(3) ?>').submit()"><span>Salvar com envio de email</span></a>
    <? endif; ?>
    <a class="button" href="<?php echo site_url('objeto/lista'); ?>"><span>Cancelar</span></a>
    </td>
    <td>&nbsp;</td>
  </tr>
</table>
</div> <!-- Final de Edi��o de Objetos -->


<?php echo form_close();?>
	
	
<script type="text/javascript">

$j(".jqzoom").fancybox(); 

$('ID_CLIENTE').addEvent('change', function(){
	clearSelect($('ID_SUBCATEGORIA'),1);
	montaOptionsAjax($('ID_CATEGORIA'),'<?php echo site_url('json/admin/getCategoriasByCliente'); ?>','id=' + this.value,'ID_CATEGORIA','DESC_CATEGORIA');
	montaOptionsAjax($('ID_TIPO_OBJETO'),'<?php echo site_url('json/admin/getTiposObjetosByCliente'); ?>','id=' + this.value,'ID_TIPO_OBJETO','DESC_TIPO_OBJETO');
});

$('ID_CATEGORIA').addEvent('change', function(){
	clearSelect($('ID_SUBCATEGORIA'),1);
	montaOptionsAjax($('ID_SUBCATEGORIA'),'<?php echo site_url('json/admin/getSubcategoriasByCategoria'); ?>','id=' + this.value,'ID_SUBCATEGORIA','DESC_SUBCATEGORIA');
});

$('ID_FABRICANTE').addEvent('change', function(){
	clearSelect($('ID_MARCA'),1);			   
	montaOptionsAjax($('ID_MARCA'),'<?php echo site_url('json/admin/getMarcasByFabricante'); ?>','id=' + this.value,'ID_MARCA','DESC_MARCA');
});

</script>

</div>
	
<?php $this->load->view("ROOT/layout/footer") ?>

