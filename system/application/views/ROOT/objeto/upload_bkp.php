<?php $this->load->view("ROOT/layout/header"); ?>

<h1>Enviar objetos</h1>

<div id="contentGlobal">

<p>
As seguintes extensões não são permitidas: <?php echo implode(', ', getExtensoesProibidas()); ?>
</p>
<p>
Arquivos sem extensão também não são permitidos.
</p>

<div id="flash_container">Flash goes here</div>
<script type="text/javascript">
var vars = {urlUpload: '<?php echo base_url().index_page().'/objeto/upload'; ?>'};
vars.SessionID = '<?php echo session_id(); ?>';
vars.urlData = '<?php echo base_url().index_page().'/json/admin/dataUploaderObjetos'; ?>';
vars.urlVerifica = '<?php echo base_url().index_page().'/json/admin/verificaArquivosExistentes'; ?>';
vars.supportedFiles = '*.*';
vars.deniedExtensions = '<?php echo implode(',', getExtensoesProibidas()); ?>';
swfobject.embedSWF("<?php echo base_url(); ?>swf/upload_objetos.swf", "flash_container", "975", "500", "9.0.0",{},vars,{wmode:'transparent',allowscriptaccess:'always'});

function itemEnviado(name){

}

function btnCancelar_clickHandler(){
	location.href = '<?php echo site_url('objeto/lista'); ?>';
}

</script>
</div>

<?php $this->load->view("ROOT/layout/footer") ?>