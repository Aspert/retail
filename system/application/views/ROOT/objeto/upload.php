<?php $this->load->view("ROOT/layout/header"); ?>

<style type="text/css">
	.progressBackgroundFilter
	{
		display: none;
		background-color: #000;
		bottom: 0px;
		filter: alpha(opacity=50);
		left: 0px;
		margin: 0;
		opacity: 0.5;
		overflow: hidden;
		padding: 0;
		position: fixed;
		right: 0px;
		top: 0px;
		z-index: 1000;
	}
	
	.processMessage
	{
		display: none;
		background-color: #fff;
		color: #000000;
		text-align: center;
		left: 43%;
		padding: 10px;
		position: absolute;
		top: 230%;
		width: 140px;
		z-index: 1001;
		border: 1px solid #000000;
	} 
</style>

<link rel="stylesheet" href="<?=base_url()?>js/js_uploader/jquery.plupload.queue/css/jquery.plupload.queue.css" type="text/css" media="screen" />

<script type="text/javascript" src="<?=base_url()?>js/js_uploader/plupload.full.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/js_uploader/jquery.plupload.queue/jquery.plupload.queue.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/js_uploader/i18n/pt_BR.js"></script>

<script type="text/javascript" src="<?=base_url()?>js/ZeroClipboard.js"></script>

<h1>Enviar objetos</h1>

<div class="progressBackgroundFilter" id="progressBackgroundFilter">
</div>
<div class="processMessage" id="processMessage">
	Validando Arquivos<br>
	<img src="<?php echo base_url().THEME; ?>img/ajax-loader.gif" border="0" />
</div>

<div id="contentGlobal">
	<form id="form_objeto" method="post" action="<?php echo site_url('objeto/upload/'.$this->uri->segment(3)); ?>" name="form_objeto">
	
	<table width="100%" border="0" class="tableFormObjeto" style="border-spacing:8px">
		<tr width="1">
			<td colspan="3" style="text-align: left;">
				As seguintes extensões não são permitidas: <?php echo implode(', ', getExtensoesProibidas()); ?><br/>
				Arquivos sem extensão também não são permitidos.
			</td>
		</tr>
		<tr>
			<td style="text-align: right; width: 10%;"><strong>Cliente:</strong></td>
			<td style="text-align: left; width: 90%;">
				<?php if(!empty($_sessao['ID_CLIENTE'])): ?>
					<?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?>
				<?php else: ?>
					<select name="ID_CLIENTE" id="ID_CLIENTE" style="width: 300px;">
						<option value=""></option>
						<?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', POST('ID_CLIENTE',TRUE)); ?>
          			</select>
				<?php endif; ?>
			</td>
		</tr>
		<tr>
			<td style="text-align: right;"><strong>Tipo:</strong></td>
			<td style="text-align: left;">
				<select name="ID_TIPO_OBJETO" id="ID_TIPO_OBJETO" style="width: 300px;">
					<option value=""></option>
					<?php echo montaOptions($tipos,'ID_TIPO_OBJETO','DESC_TIPO_OBJETO'); ?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="text-align: right;"><strong>Categoria:</strong></td>
			<td style="text-align: left;">
				<select name="ID_CATEGORIA" id="ID_CATEGORIA" style="width: 300px;">
					<option value=""></option>
					<?php echo montaOptions($categorias,'ID_CATEGORIA','DESC_CATEGORIA'); ?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="text-align: right;"><strong>Sub-categoria:</strong></td>
			<td style="text-align: left;">
				<select name="ID_SUBCATEGORIA" id="ID_SUBCATEGORIA" style="width: 300px;">
					<option value=""></option>
					<?php echo montaOptions(array(),'ID_SUBCATEGORIA','DESC_SUBCATEGORIA'); ?>
				</select>
			</td>
		</tr>
	</table>
	
	</form>
</div>

<form method="post" action="dump.php">
	<div id="uploader">
		<p>Your browser doesnt have Flash, Silverlight or HTML5 support.</p>
	</div>
</form>

<script type="text/javascript">
var objPlupload = null;

$j(function() {
	$('ID_CLIENTE').addEvent('change', function(){
		clearSelect($('ID_SUBCATEGORIA'),1);
		clearSelect($('ID_CATEGORIA'),1);
		montaOptionsAjax($('ID_CATEGORIA'),'<?php echo site_url('json/admin/getCategoriasByCliente'); ?>','id=' + this.value,'ID_CATEGORIA','DESC_CATEGORIA');
		montaOptionsAjax($('ID_TIPO_OBJETO'),'<?php echo site_url('json/admin/getTiposObjetosByCliente'); ?>','id=' + this.value,'ID_TIPO_OBJETO','DESC_TIPO_OBJETO');
		$j("#uploader_browse").hide();
	});

	$('ID_TIPO_OBJETO').addEvent('change', function(){
		if($j('#ID_TIPO_OBJETO').val() == "" || $j('#ID_CATEGORIA').val() == "" || $j('#ID_SUBCATEGORIA').val() == ""){
			$j("#uploader_browse").hide();
		}
		else{
			$j("#uploader_browse").show();
		}
	});

	$('ID_CATEGORIA').addEvent('change', function(){
		clearSelect($('ID_SUBCATEGORIA'),1);
		montaOptionsAjax($('ID_SUBCATEGORIA'),'<?php echo site_url('json/admin/getSubcategoriasByCategoria'); ?>','id=' + this.value,'ID_SUBCATEGORIA','DESC_SUBCATEGORIA');
		$j("#uploader_browse").hide();
	});

	$('ID_SUBCATEGORIA').addEvent('change', function(){
		if($j('#ID_TIPO_OBJETO').val() == "" || $j('#ID_CATEGORIA').val() == "" || $j('#ID_SUBCATEGORIA').val() == ""){
			$j("#uploader_browse").hide();
		}
		else{
			$j("#uploader_browse").show();
		}
	});

	var errosUpload = new Array();
	
	// Setup html5 version
	objPlupload = $j("#uploader").pluploadQueue({
		// General settings
		runtimes : 'html5,flash,silverlight,html4',
		url : '<?php echo base_url().index_page(); ?>/objeto/upload',
		//chunk_size: '1mb',
		max_file_size : '1gb',
		rename : false,
		dragdrop: true,
		multiple_queues: true,
		multipart_params: {
			cliente: '',
			tipo_objeto: '',
			categoria: '',
			subcategoria: '',
		},
		
		filters : {
			// Maximum file size
			max_file_size : '1gb',
			// Specify what files to browse for
			mime_types: [
				{title : "Image files", extensions : "jpg,gif,png,tiff,psd,eps,jpeg,tif"},
				{title : "Zip files", extensions : ""}
			]
		},

		init : {      
			BeforeUpload: function(up) {
		        if($j('#ID_CLIENTE').val() != "" && $j('#ID_TIPO_OBJETO').val() && $j('#ID_CATEGORIA').val() && $j('#ID_SUBCATEGORIA').val()){
	                this.settings.multipart_params.cliente = $j('#ID_CLIENTE').val();
	                this.settings.multipart_params.tipo_objeto = $j('#ID_TIPO_OBJETO').val();
	                this.settings.multipart_params.categoria = $j('#ID_CATEGORIA').val();
	                this.settings.multipart_params.subcategoria = $j('#ID_SUBCATEGORIA').val();
		        }
		        else{
					alert("Selecione o cliente, tipo, categoria e subcategoria");
					up.stop();
				}
	        },

            Browse: function(up, files) {
            	cancelarEnvio();
            },

			FilesAdded: function(up, files) {		
	            // Called when files are added to queue
	            var arrArquivos = new Array();

	            $j("#progressBackgroundFilter").show();
	            $j("#processMessage").show();

                plupload.each(files, function(file) {
                	arrArquivos.push(getFileName(file));
                });

            	$j.post(
            		'<?php echo base_url().index_page(); ?>/json/admin/verificaArquivosExistentes', 
            		{	
                		tipo : $j('#ID_TIPO_OBJETO').val(), 
                		cliente : $j('#ID_CLIENTE').val(),
                		files : arrArquivos
                	}, 
            		function(data){
                		var arrArquivos = $j.parseJSON(data);
                		if(arrArquivos.files != "" || arrArquivos.fichas != "" || arrArquivos.files_duplicados != ""){
							displayMessagewithparameter('<?php echo site_url("objeto/validaArquivos/");?>', 420, 400);
                		}
                		else{
							//up.start();
                    	}
            		}
            	)
            	.complete(
                    	function(data) {
            	            $j("#progressBackgroundFilter").hide();
            	            $j("#processMessage").hide();
                        }
                );
	        },

	        Error: function(up, files){
	        	errosUpload.push(files.file.name);
			},

	        UploadComplete: function(up, files){
		        if(errosUpload.length > 0){
					var html = "<div id='contentGlobal'><table cellpading='0' cellspacing='0' width='100%' style='height:100%;'><tr><td><br><strong>Não foi possível subir os arquivos abaixo para o sistema, por gentileza tente realizar upload novamente.</strong><br><br>";
					for(var i = 0; i <= errosUpload.length - 1; i++){
						html = html + errosUpload[i] + "<br>";
					}
					html = html + "</td></tr><tr><td colspan='2'>&nbsp;</td></tr><tr><td style='text-align: center;'><input type='button' value='Ok' onClick='fecharModal();' style='width: 90px;'></td></tr></table></div>";
		        	displayStaticMessagewithparameter(html, 400, 400);
				}
	        }
		}
	});
	$j("#uploader_browse").hide();
});

function fecharModal(){
	$j('#divModal').dialog('close');
}

function cancelarEnvio(){
	$j('#divModal').dialog('close');
	
	var uploader = $j("#uploader").pluploadQueue();

    for(var i = uploader.files.length - 1; i >= 0; i--){
		uploader.removeFile(uploader.files[i]);
	}
}

function enviarNovos(arrRepetidos){
	$j('#divModal').dialog('close');
	
	var uploader = $j("#uploader").pluploadQueue();

	var arrRemover = new Array();

    for(var i = uploader.files.length - 1; i >= 0; i--){
    	//alert(uploader.files.length);
		for(var h = arrRepetidos.length - 1; h >= 0; h--){
			if(uploader.files[i].name == arrRepetidos[h]){
				//uploader.removeFile(uploader.files[i]);
				arrRemover.push(uploader.files[i])
			}
		}
	}

    for(var i = arrRemover.length - 1; i >= 0; i--){
    	uploader.removeFile(arrRemover[i]);
    }

    if(uploader.files.length > 0){
    	//uploader.start();
    }
}

function enviarTodas(){
	$j('#divModal').dialog('close');
	
	var uploader = $j("#uploader").pluploadQueue();
	
    if(uploader.files.length > 0){
    	//uploader.start();
    }
}

function getFileName() {
    var str = "";
    var retorno = "";

    plupload.each(arguments, function(arg) {
        var row = "";

        if (typeof(arg) != "string") {
            plupload.each(arg, function(value, key) {
                // Convert items in File objects to human readable form
                if (arg instanceof plupload.File) {
                    // Convert status to human readable
                    switch (value) {
                        case plupload.QUEUED:
                            value = 'QUEUED';
                            break;

                        case plupload.UPLOADING:
                            value = 'UPLOADING';
                            break;

                        case plupload.FAILED:
                            value = 'FAILED';
                            break;

                        case plupload.DONE:
                            value = 'DONE';
                            break;
                    }
                }

                if (typeof(value) != "function") {
                    if(key == 'name'){
						retorno = value;
                    }
                }
            });
        } else {
        }
    });

    return retorno;
}
</script>

<?php $this->load->view("ROOT/layout/footer") ?>