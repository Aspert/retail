<?php $this->load->view("ROOT/layout/header"); ?>

<h1>Fichas do objeto <?= $objeto['FILENAME'] ?></h1>

<div id="contentGlobal">

<div id="contentFicha">

<table width="100%" border="0"  cellpadding="0"  cellspacing="0" id="contentImagem">
	<tr>
		<th width="225">Nome da ficha</th>
		<th width="225">Tipo de ficha</th>
		<th width="106">C&oacute;digo de Barras</th>
		<th width="154"><?php ghDigitalReplace($_sessao, 'Categoria'); ?></th>
		<th width="173"><?php ghDigitalReplace($_sessao, 'Subcategoria'); ?></th>
		<th width="89">Status</th>
		<th width="116">Data atualizacao</th>
		<th>Imagem</th>
	</tr>
	<!-- seta var para a imagem da ficha -->
	<? $img = false; ?>
	<?php foreach ($fichas as $ficha) : ?>
	<tr>
    	<td>
        	<? if($podeEditarFicha): ?>
				<a href="<?= base_url().index_page()?>/ficha/form/<?= $ficha['ID_FICHA']; ?>"><?= $ficha['NOME_FICHA'] ?></a>
        	<? else: ?>
            	<?= $ficha['NOME_FICHA'] ?>
            <? endif; ?>
        </td>
		<td><?= $ficha['TIPO_FICHA'] ?></td>
		<td><?= $ficha['COD_BARRAS_FICHA'] ?></td>
		<td><?= $ficha['DESC_CATEGORIA'] ?></td>
		<td><?= $ficha['DESC_SUBCATEGORIA'] ?></td>
		<td><?= $ficha['DESC_APROVACAO_FICHA'] ?></td>
		<td><?= format_date_to_form($ficha['DATA_ALTERACAO']) ?></td>
		
		<!-- adiciona a imagem da ficha -->
		<? if(!$img){?>
			<? $img = true; ?>
			<td rowspan="<?=count($fichas)?>" class="align">
		    <a href="<?= base_url() ?>img.php?img_id=<?= $id; ?>&rand=<?= rand()?>&img_size=big&a.jpg" class="jqzoom">
       		 <img src="<?= base_url() ?>img.php?img_id=<?= $id; ?>&rand=<?= rand()?>&a.jpg" width="50" border="0">
    		</a>		
			</td>
		<? } ?>
	</tr>
	<?php endforeach; ?>
</table>

<div class="both"></div>
    <a class="button" href="<?php echo site_url('objeto/lista'); ?>" title="Voltar"><span>Voltar</span></a>	
<div>&nbsp; </div>

</div>
        

	
	
<script type="text/javascript">

$j(".jqzoom").fancybox(); 

</script>

</div>

<?php $this->load->view("ROOT/layout/footer") ?>