<div id="contentGlobal">
	<table cellpading="0" cellspacing="0" width="100%" style="height:100%;">
		<?php if(count($arquivosExistentes['files']) > 0){ ?>
			<tr>
				<td colspan="4">
					<br>
					<strong>Os seguintes arquivos já existem:</strong>
					<br><br>
					<?php foreach($arquivosExistentes['files'] as $f){ ?>
						<?php echo removeCaracteresEspeciaisNomeArquivo($f); ?><br>
					<?php } ?>
				</td>
			</tr>
		<?php } ?>
		<?php if(count($arquivosExistentes['fichas']) > 0){ ?>
			<tr>
				<td colspan="4">	
					<br><br>
					<strong>E estão associados as seguintes fichas:</strong>
					<br><br>
					<?php foreach($arquivosExistentes['fichas'] as $f){ ?>
						<?php echo removeCaracteresEspeciaisNomeArquivo($f)."&nbsp"; ?><br>
					<?php } ?>
				</td>
			</tr>
		<?php } ?>	
		<?php if(count($arquivosExistentes['files_duplicados']) > 0){ ?>
			<tr>
				<td colspan="4">	
					<strong>Os seguintes arquivos já existem em outras pastas/tipo:</strong>
					<br><br>
					<?php foreach($arquivosExistentes['files_duplicados'] as $f){ ?>
						<?php echo $f['tipo'] . ' - ' . removeCaracteresEspeciaisNomeArquivo($f['file']); ?><br>
					<?php } ?>
				</td>
			</tr>
		<?php } ?>
		<?php if($excel){ echo "</table>"; die; } ?>
		<tr>
			<td colspan="4">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td>
				<input type="button" value="Cancelar Envio" onClick="cancelarEnvio();">
			</td>
			<td>
				<input type="button" value="Enviar Novos" onClick="novos();">
			</td>
			<td>
				<input type="button" value="Enviar Todos" onClick="enviarTodas();">
			</td>
			<td>
				<input type="button" value="Relatório" onClick="document.location.href = '<?php echo site_url("objeto/validaArquivos/true");?>';">
			</td>
		</tr>
	</table>
</div>
	
<script type="text/javascript">
	function novos(){
		var arquivosRepetidos = new Array();
		<?php foreach($arquivosExistentes['files'] as $f){ ?>
			arquivosRepetidos.push("<?php echo $f; ?>");
		<?php } ?>
		<?php foreach($arquivosExistentes['files_duplicados'] as $f){ ?>
			arquivosRepetidos.push("<?php echo $f['file']; ?>");
		<?php } ?>

		enviarNovos(arquivosRepetidos);
	}

</script>