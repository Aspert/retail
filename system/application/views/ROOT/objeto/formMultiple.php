<?php $this->load->view("ROOT/layout/header"); ?>

<h1>Edi&ccedil;&atilde;o de Objetos</h1>

<div id="contentGlobal">

<div id="contentEditobj">

<?php if ( isset($erros) && is_array($erros) ) : ?>
	<div class="box_erros">
      <ul style="color:#ff0000;font-weight:bold;">
        <?php foreach ( $erros as $e ) : ?>
        <li><?=$e?></li>
        <?php endforeach; ?>
      </ul>
    </div>
    <p>&nbsp;</p>
    <a class="button" href="<?= site_url('/objeto/lista') ?>" ><span>Voltar</span></a>

<? else: ?>
    
<?php echo form_open('objeto/saveMultiple/'.$this->uri->segment(3), array('id'=>'form'));?>

<table width="80%" border="0" cellspacing="0" cellpadding="0" id="tableEditobj">
  <tr>
    <td width="103">&nbsp;</td>
    <td width="20">&nbsp;</td>
    <td width="663">&nbsp;</td>
    <td width="24" rowspan="9">&nbsp;</td>
  </tr>
  <tr>
    <td><strong>Cliente:</strong></td>
    <td>&nbsp;</td>
    <td>
      <?php
			echo $cliente;
			printf('<input type="hidden" name="ID_CLIENTE" id="ID_CLIENTE" value="%d" />', post('ID_CLIENTE',true));
            ?>
      </td>
  </tr>
  <tr>
    <td><strong>Categoria:</strong></td>
    <td>&nbsp;</td>
    <td>    
      <select name="ID_CATEGORIA" id="ID_CATEGORIA">
        <option value=""></option>
        <?php echo montaOptions($categorias,'ID_CATEGORIA','DESC_CATEGORIA', post('ID_CATEGORIA',true)); ?>
        </select>
      </td>
  </tr>
  <tr>
    <td><strong>SubCategoria:</strong></td>
    <td>&nbsp;</td>
    <td>
    	     <select name="ID_SUBCATEGORIA" id="ID_SUBCATEGORIA">
            <option value=""></option>
            <?php echo montaOptions($subcategorias,'ID_SUBCATEGORIA','DESC_SUBCATEGORIA', post('ID_SUBCATEGORIA',true)); ?>
            </select>
    </td>
    </tr>
  <tr>
    <td><strong>Fabricante:</strong></td>
    <td>&nbsp;</td>
    <td>
    				<select id="ID_FABRICANTE" name="ID_FABRICANTE">
				<option value=""></option>
				<?= montaOptions($fabricantes, 'ID_FABRICANTE','DESC_FABRICANTE', post('ID_FABRICANTE',true)); ?>
			</select>
    </td>
    </tr>
  <tr>
    <td><strong>Marca:</strong></td>
    <td>&nbsp;</td>
    <td>
    				<select id="ID_MARCA" name="ID_MARCA">
				<option value=""></option>
				<?= montaOptions($marcas, 'ID_MARCA','DESC_MARCA', post('ID_MARCA',true)); ?>
			</select>
    </td>
    </tr>
  <tr>
    <td><strong>Keywords:</strong></td>
    <td>&nbsp;</td>
    <td>Atualizar keywords?
      <input name="chkKeywords" type="checkbox" id="chkKeywords" value="1" /></td>
    </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><?= form_textarea(array('name'=>'KEYWORDS','id'=>'KEYWORDS','value'=>post('KEYWORDS',true), 'class' => 'textareaEdit')) ?></td>
    </tr>
</table>
<h1>Lista de objetos</h1>
<table width="640" height="53" border="1" cellpadding="0" cellspacing="0" style="padding:3px 3px 3px 3px">
  <tr>
        <th width="47" align="center" valign="middle" style="border:1px solid #CCCCCC; font-weight:bold">&nbsp;</th>
        <th width="587" align="left" valign="middle" style="border:1px solid #CCCCCC; font-weight:bold">Nome do arquivo</th>
        <th width="587" align="left" valign="middle" style="border:1px solid #CCCCCC; font-weight:bold">Tipo</th>
        <th width="587" align="left" valign="middle" style="border:1px solid #CCCCCC; font-weight:bold">Categoria</th>
        <th width="587" align="left" valign="middle" style="border:1px solid #CCCCCC; font-weight:bold">Subcategoria</th>
   </tr>
  <? foreach($objetos as $obj):?>
      <tr>
        <td width="47" align="center" valign="middle" style="border:1px solid #CCCCCC;padding:3px 3px 3px 3px">
        <a href="<?= base_url() ?>img.php?img_id=<?= $obj['FILE_ID']; ?>&rand=<?= rand()?>&img_size=big&a.jpg" class="jqzoom"><img src="<?= base_url() ?>img.php?img_id=<?= $obj['FILE_ID']; ?>&rand=<?= rand()?>&a.jpg" width="40" border="0" /></a></td>
        <td width="587" align="left" valign="middle" style="border:1px solid #CCCCCC"><?= str_replace(' ','&nbsp;',$obj['FILENAME']); ?>
        <input type="hidden" name="FILE_ID[]" value="<?= $obj['FILE_ID']; ?>" />       
        </td>
        <td width="587" align="left" valign="middle" style="border:1px solid #CCCCCC"><?= $obj['DESC_TIPO_OBJETO']; ?></td>
        <td width="587" align="left" valign="middle" style="border:1px solid #CCCCCC"><?= $obj['DESC_CATEGORIA']; ?></td>
        <td width="587" align="left" valign="middle" style="border:1px solid #CCCCCC"><?= $obj['DESC_SUBCATEGORIA']; ?></td>
      </tr>
  <? endforeach; ?>
</table>
<p>&nbsp;</p>
<p>
  <? if($podeSalvar):?>
  <a class="button" href="javascript:" onclick="$j(this).closest('form').submit();"><span>Salvar</span></a>
  <? endif; ?>
  <? if($podeEmail):?>
  <a class="button" href="javascript:new Util().vazio()" onclick="$j('#form').attr('action','<?= base_url().index_page() ?>/objeto/saveMultipleEmail').submit()"><span>Salvar com envio de email</span></a>
  <? endif; ?>
  <a class="button" href="<?php echo site_url('objeto/lista'); ?>"><span>Cancelar</span></a> </p>
<p>&nbsp;</p>
</div> <!-- Final de Edi��o de Objetos -->


<?php echo form_close();?>

	
<script type="text/javascript">

$j(".jqzoom").fancybox(); 


$j('#KEYWORDS').attr('disabled','disabled');
$j('#chkKeywords').click(
	function(){
		if($j('#chkKeywords:checked').size() == 1){
			$j('#KEYWORDS').removeAttr('disabled');
		}else{
			$j('#KEYWORDS').attr('disabled','disabled');
		}
	});


$('ID_CATEGORIA').addEvent('change', function(){
	clearSelect($('ID_SUBCATEGORIA'),0);
	montaOptionsAjax($('ID_SUBCATEGORIA'),'<?php echo site_url('json/admin/getSubcategoriasByCategoria'); ?>','id=' + this.value,'ID_SUBCATEGORIA','DESC_SUBCATEGORIA');
});

$('ID_FABRICANTE').addEvent('change', function(){
	clearSelect($('ID_MARCA'),0);
	montaOptionsAjax($('ID_MARCA'),'<?php echo site_url('json/admin/getMarcasByFabricante'); ?>','id=' + this.value,'ID_MARCA','DESC_MARCA');
});

</script>

<?php endif; ?>

</div>
	
<?php $this->load->view("ROOT/layout/footer") ?>

