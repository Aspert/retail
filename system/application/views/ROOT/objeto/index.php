﻿<?php $this->load->view("ROOT/layout/header") ?>
<h1><?php echo ghDigitalReplace($_sessao, 'Objetos'); ?></h1>

<div id="contentGlobal">

<? 
$erro = Sessao::get('erro');
if(!empty($erro)): ?>
	<div class="box_erros">
    	<?= Sessao::get('erro'); ?>
		<?= Sessao::clear('erro'); ?>
    </div><p>&nbsp;</p>
<? endif; ?>

<!-- Inicio de Pesquisa Completa -->

<div id="pesquisa_completo" style="display:<?=(isset($busca['tipo_pesquisa'])&& $busca['tipo_pesquisa'] == 'completa') ? '':'none'?>">
<?=form_open('objeto/lista');?>
	<?=form_hidden('tipo_pesquisa','completa');?>
		
<table border="0" cellspacing="0" cellpadding="0" id="contentPcomplete">
  <tr>
    <td>Nome do arquivo:</td>
    <td>&nbsp;</td>
    <td><?=form_input('FILENAME', (isset($busca['FILENAME'])) ? $busca['FILENAME'] : null);?></td>
  </tr>
  <tr>
    <td>Itens/P&aacute;gina:</td>
    <td>&nbsp;</td>
    <td><select name="pagina" id="pagina" class="pesquisa_select">
    <?php
		$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
		for($i=10; $i<=100; $i+=10){
			printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
		}
	?></select></td>
  </tr>
  <tr>
    <td>Fabricante:</td>
    <td>&nbsp;</td>
    <td>
    	<select name="ID_FABRICANTE" id="ID_FABRICANTE">
        <option value=""></option>
        <?php echo montaOptions($fabricantes,'ID_FABRICANTE','DESC_FABRICANTE', !empty($busca['ID_FABRICANTE']) ? $busca['ID_FABRICANTE'] : ''); ?>
        </select>
    </td>
  </tr>
  <tr>
    <td>Marca:</td>
    <td>&nbsp;</td>
    <td>
    	<select name="ID_MARCA" id="ID_MARCA">
        <option value=""></option>
        <?php echo montaOptions($marcas,'ID_MARCA','DESC_MARCA', !empty($busca['ID_MARCA']) ? $busca['ID_MARCA'] : ''); ?>
        </select>
    </td>
  </tr>
  <tr>
    <td>Per&iacute;odo de upload:</td>
    <td>&nbsp;</td>
    <td valign="">
    	<?=form_input(array('id'=>'PERIODO_INICIAL','name'=>'PERIODO_INICIAL', 'class'=>'_calendar','size'=>'10', 'value'=>(isset($busca['PERIODO_INICIAL'])) ? $busca['PERIODO_INICIAL'] : null,'readonly'=>'readonly'));?>
		<?=form_input(array('id'=>'PERIODO_FINAL','name'=>'PERIODO_FINAL', 'class'=>'_calendar', 'size'=>'10', 'value'=>(isset($busca['PERIODO_FINAL'])) ? $busca['PERIODO_FINAL'] : null,'readonly'=>'readonly'));?>
    </td>
  </tr>
  <tr>
    <td>Origem:</td>
    <td>&nbsp;</td>
    <td><?=form_input(array('name'=>'ORIGEM', 'value'=>(isset($busca['ORIGEM'])) ? $busca['ORIGEM'] : null));?></td>
  </tr>
    <tr>
    	<td><?php ghDigitalReplace($_sessao, 'Cliente'); ?>:</td>
    	<td>&nbsp;</td>
    	<td>		
    		<?php if(!empty($_sessao['ID_CLIENTE'])): ?>
    		<?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?><br /><br />
    		<?php else: ?>
    		<select name="ID_CLIENTE" id="ID_CLIENTE">
    			<option value=""></option>
    			<?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', !empty($busca['ID_CLIENTE']) ? $busca['ID_CLIENTE'] : ''); ?>
    			</select><br />
    		<?php endif; ?>
    		</td>
    	</tr>
    <tr>
    <td>Tipo:</td>
    <td>&nbsp;</td>
    <td>
    	<select name="ID_TIPO_OBJETO" id="ID_TIPO_OBJETO">
        <option value=""></option>
        <?php echo montaOptions($tipos,'ID_TIPO_OBJETO','DESC_TIPO_OBJETO', !empty($busca['ID_TIPO_OBJETO']) ? $busca['ID_TIPO_OBJETO'] : ''); ?>
        </select>
    </td>
  </tr>
  <tr>
    <td><?php ghDigitalReplace($_sessao, 'Categoria'); ?>:</td>
    <td>&nbsp;</td>
    <td>
    	<select name="ID_CATEGORIA" id="ID_CATEGORIA" <? echo (!empty($busca['NO_CATEGORIA'])) ? 'disabled="disabled""' : '' ?> >
        <option value=""></option>
        <?php echo montaOptions($categorias,'ID_CATEGORIA','DESC_CATEGORIA', !empty($busca['ID_CATEGORIA']) ? $busca['ID_CATEGORIA'] : ''); ?>
        </select> 
		<input type="checkbox" name="NO_CATEGORIA" id="chkSemCategoria" <? echo (!empty($busca['NO_CATEGORIA'])) ? 'checked="checked"' : '' ?> />
  Buscar itens sem categoria
    </td>
  </tr>
  <tr>
    <td><?php ghDigitalReplace($_sessao, 'SubCategoria'); ?>:</td>
    <td>&nbsp;</td>
    <td>
    	<select name="ID_SUBCATEGORIA" id="ID_SUBCATEGORIA" <? echo (!empty($busca['NO_CATEGORIA'])) ? 'disabled="disabled""' : '' ?>>
        <option value=""></option>
        <?php echo montaOptions($subcategorias,'ID_SUBCATEGORIA','DESC_SUBCATEGORIA', !empty($busca['ID_SUBCATEGORIA']) ? $busca['ID_SUBCATEGORIA'] : ''); ?>
        </select>
    </td>
  </tr>
  <tr>
    <td>Key Words:</td>
    <td>&nbsp;</td>
    <td><?= form_textarea(array('name'=>'KEYWORDS','cols'=>'40', 'rows'=>'5', 'value'=>(isset($busca['KEYWORDS'])) ? $busca['KEYWORDS'] : null));?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>   	    	
    	<a class="button" href="javascript:" id="btnLimpaPesquisa"><span>Limpar Campos</span></a>
        <a class="button" href="javascript:" onclick="$j(this).closest('form').submit();"><span>Pesquisar</span></a>   
        <a class="button" href="javascript:" onClick="new Util().pesquisaSimples();$j.fn.sortgrid.options.form = '#pesquisa_simples form';"><span>Cancelar Pesquisa</span></a>
    </td>
  </tr>
</table>
<?=form_close();?>
</div>

<!-- Final de Pesquisa Completa -->

<div id="pesquisa_simples" <?=(isset($busca['tipo_pesquisa'])&& $busca['tipo_pesquisa'] == 'completa') ? 'style="display:none"':''?>>
	
	<?=form_open('objeto/lista');?>
	<?=form_hidden('tipo_pesquisa','simples');?>
	<?php $codigo = (isset($busca['NOME'])) ? $busca['NOME'] : null;?>
	
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tablePesquisa">
  <tr>
    <td width="300">
   	<?php if ( $podeEnviar ) : ?>
		<?=anchor('objeto/upload','<img src="'.base_url().THEME.'img/file_add.png" border="0" />',array('title'=>'Enviar objetos'))?>
	<?php endif; ?>
    </td>
    <td></td>
    <td width="150" nowrap="nowrap">
    <?
    $data = array(
              'name'        => 'FILENAME',
              'value'       => val($busca, 'FILENAME'),
              'class'   => 'pesquisa_input',
            );
    ?>
    Nome do arquivo: <?= form_input($data);?>
    </td>
    <td width="10"></td>
    <td width="10"></td>
    <td width="10"></td>
    <td width="600">Itens/P&aacute;gina:     <select name="pagina" id="pagina" class="pesquisa_select">
    <?php
		$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
		for($i=10; $i<=100; $i+=10){
			printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
		}
	?></select></td>
    <td width="10"></td>
    <td width="700">

    </td>
    <td width="10"></td>
    <td><a class="button" title="Pesquisa" onclick="$j(this).closest('form').submit()"><span>Pesquisar</span></a></td>
    <td width="10"></td>
    <td width="600"><a class="button" title="Pesquisa Avan&ccedil;ada" onclick="new Util().pesquisaCompleta();$j.fn.sortgrid.options.form = '#pesquisa_completo form';"><span>Pesquisa Avan&ccedil;ada</span></a></td>
  </tr>
</table>
<?=form_close();?>
	
</div> <!-- Final de Bloco Pesquisa -->

<div id="tableObjeto">

<?php if(!empty($objetos)&& is_array($objetos)):?>
<form action="" method="post" id="formMultiplo">
<table width="100%" id="tabelaBusca" border="0" cellspacing="0" cellpadding="0" class="tableSelection">
<thead>
	<tr>
		<? if($podeEditarMultiplo): ?>
        	<th scope="col">&nbsp;</th>
        <? endif; ?>
		<? if($podeEditar): ?>
        	<th scope="col">Editar</th>
        <? endif; ?>
		<? if($podeDownload): ?>
        	<th scope="col">Download</th>
       	<? endif; ?>
        <? if($podeExcluir): ?>
        	<th scope="col">Excluir</th>
       	<? endif; ?>
        <? if($podeFicha): ?>
        	<th scope="col"><?= ghDigitalReplace($_sessao, 'Fichas'); ?></th>
        <? endif; ?>	
		<th scope="col"><?php echo ghDigitalReplace($_sessao, 'Objetos'); ?></th>
		<th scope="col"><?= ghDigitalReplace($_sessao, 'Cliente'); ?></th>
		<th scope="col" id="col1">Nome do Arquivo</th>
		<th scope="col">Marca</th>
		<th scope="col" id="col3">Data de Upload</th>
		<th scope="col"><?php echo ghDigitalReplace($_sessao, 'Categoria'); ?></th>
		<th scope="col"><?php echo ghDigitalReplace($_sessao, 'Sub-categoria'); ?></th>
		<th scope="col">Tipo</th>
	</tr>
</thead>	
<tfoot>
</tfoot>
<tbody>

	<?php $cont=0; ?>
	<?php foreach ($objetos as $o) : ?>

	<tr>
		<? if($podeEditarMultiplo || $podeExcluirMultiplo): ?>
        	<td><input type="checkbox" name="FILE ID[]" value="<?= $o['FILE_ID'] ?>" class="chkfileid" />
        	</td>
        <? endif; ?>
		<? if($podeEditar): ?>
			<td><?php echo anchor('objeto/form/'. $o['FILE_ID'], '<img src="'. base_url().THEME.'img/file_edit.png" />',array('title'=>'Editar objeto'));?></td>
		<? endif;?>
		<? if($podeDownload): ?>
			<td><?php echo anchor('objeto/download/'. strrev($o['FILE_ID']), '<img src="'. base_url().THEME.'img/disc.png" />',array('title'=>'Download objeto','target'=>'_blank'));?></td>
		<? endif;?>
		<? if($podeExcluir): ?>
        	<td>
            	<a href="javascript:void(0)">
                	<img src="<?= base_url().THEME ?>img/trash.png" border="0" title="Excluir Objeto" onclick="remover(<?= $o['FILE_ID'] ?>, '<?= $o['ID_TIPO_OBJETO'] ?>', '<?= urlencode($o['FILENAME']) ?>', <?php echo $o['HAS_FICHA'] ? 1 : 0; ?>)"/>
                </a>          
            </td>
		<? endif; ?>
		<? if($podeFicha): ?>
        	<td>
            	
                	<? if($o['HAS_FICHA']): ?>
                    	<a href="<?= base_url().index_page()?>/objeto/fichas/<?= $o['FILE_ID']; ?>">
                			<img src="<?= base_url().THEME ?>img/file.png" border="0" title="Exibir fichas"/>
                        </a>
					<? else: ?>
                        <a href="javascript:void(0);">
                            <img src="<?= base_url().THEME ?>img/file_delete.png" border="0" title="Nao ha fichas"/>
                        </a>
                    <? endif; ?>
               
            </td>
        <? endif; ?>		
        <td>
			<a href="<?= base_url() ?>img.php?img_id=<?= $o['FILE_ID']; ?>&rand=<?= rand()?>&img_size=big&a.jpg" class="jqzoom">
				<img src="<?= base_url() ?>img.php?img_id=<?= $o['FILE_ID']; ?>&rand=<?= rand()?>" title="<?=$o['FILENAME']?>" width="50" border="0">			</a>		</td>
		<td><?=$o['DESC_CLIENTE']?></td>
		<td style="text-align: left"><?= str_replace(' ','&nbsp;',$o['FILENAME']); ?> <span id="ver<?php echo $o['FILE_ID']; ?>"></span></td>
		<td><?=$o['DESC_MARCA']?></td>
		<td><?=format_date_to_form($o['DATA_UPLOAD'])?></td>
		<td><?=$o['DESC_CATEGORIA']?></td>
		<td><?=$o['DESC_SUBCATEGORIA']?></td>
		<td><?=$o['DESC_TIPO_OBJETO']?></td>

	</tr>
	
	<?php $cont++; ?>
	<?php endforeach; ?>	
	<? if($podeEditarMultiplo || $podeExcluirMultiplo): ?>
	<tr>
		<td colspan="13" style="text-align: left">
		
			<a href="javascript:void(0)" onclick="seleciona(1)">Todos</a> |
			<a href="javascript:void(0)" onclick="seleciona(0)">Nenhum</a> |
			
			Aplicar aos selecionados: 
			<select id="acao_lote">
					<option value="0">Selecione</option>
				<? if($podeEditarMultiplo): ?>
					<option value="editar">Editar</option>
				<? endif;?>
				<? if($podeExcluirMultiplo): ?>
					<option value="excluir">Excluir</option>
				<? endif;?>
			</select>
		</td>
	</tr>
	<? endif; ?>
	<tr>
		<td <? if($podeEditarMultiplo || $podeExcluirMultiplo): ?>colspan="13"<? else: ?> colspan="12" <? endif; ?>class="obj_encontrado">Objetos encontrados: <strong><?= $objetosTotal ?></strong></td>
	</tr>
</tbody>		
</table>
</form>
</div>

<div id="contentPaginacao">
<p><span class="paginacao">
  <?=(isset($paginacao))?$paginacao:null?>
</span>
</div>

  <?php else: ?>
  	<div id="contentSemresultado">
  		<p>Nenhum resultado</p>
  	</div>
  <?php endif; ?>
  
<script type="text/javascript">
<? if($podeEditarMultiplo || $podeExcluirMultiplo): ?>

//acao do SELECT editar/excluir
$j('#acao_lote').change(
	function(){
		var val = $j('#acao_lote').val();
		if(val == "editar"){
			$j('#formMultiplo').attr('action','<?= site_url('objeto/formMultiple') ?>').submit();
		}else if(val == "excluir"){
			var objs = $j(".chkfileid:checked").size();
			if(confirm("Tem certeza que deseja excluir "+objs+" arquivo(s)")){
				$j('#formMultiplo').attr('action','<?= site_url('objeto/deleteMultiple') ?>').submit();
			}else{
				$j('#acao_lote').val(0);
			}
		}
	}
  );


//seleciona/deseleciona os chkboxes dos objetos
function seleciona(sel){
   if(sel == 1){
		$j(".chkfileid").attr('checked','checked');
   }else{
		$j(".chkfileid").removeAttr('checked');
   }
}

<? endif; ?>

function remover(id, tipo, nome, temFichas){
	
	var msg = '';
	if( temFichas ){
		msg = 'Este objeto não pode ser removido por estar vinculado a fichas.\n'+
			'Para listar as fichas, clique no ícone da coluna "Fichas"';
			
		alert(msg);
	} else {
		msg = 'Deseja realmente remover o objeto selecionado?\n' +
			'Esta ação não poderá ser desfeita';
			
		if( confirm(msg) ){
			location.href = util.options.site_url + 'objeto/delete/' + id;
		}
	}
	
}

$j(function(){
	$j('._calendar').datepicker({dateFormat: 'dd/mm/yy',showOn: 'button', buttonImage: '<?= base_url().THEME ?>img/calendario.png', buttonImageOnly: true});
	
	//{ 'zoomSpeedIn': 300, 'zoomSpeedOut': 300, 'overlayShow': false }
	$j(".jqzoom").fancybox();
	
	var options = 
			{
			columns:[{col:'#col1',label:'FILENAME'},{col:'#col3',label:'DATA_UPLOAD'}],
			form:'#pesquisa_<?= (isset($busca['tipo_pesquisa']) && $busca['tipo_pesquisa'] != 'simples' ? 'completo' : 'simples' ) ?> form',
			field:'ORDER',
			directionField:'ORDER_DIRECTION',
			orderDirection: '<?= $busca['ORDER_DIRECTION'] ?>',
			selectedField:'<?= $busca['ORDER'] ?>'
			}
	$j('#tabelaBusca').sortgrid(options);
	
	$j("#chkSemCategoria").click(function(){
		if($j("#chkSemCategoria:checked").length > 0){
			$j("#CATEGORIA option:first-child").attr("selected","selected");
			clearSelect($('SUBCATEGORIA'),1);
			$j("#CATEGORIA").attr('disabled',true);
			$j("#SUBCATEGORIA").attr('disabled',true);
		}else{
			$j("#CATEGORIA").removeAttr('disabled');
			$j("#SUBCATEGORIA").removeAttr('disabled');
		}
	});
	
	$j("#btnLimpaPesquisa").click(function(){
		limpaForm('#pesquisa_completo');
		clearSelect($('CATEGORIA'),1);
		clearSelect($('SUBCATEGORIA'),1);
		clearSelect($('ID_TIPO_OBJETO'),1);
		clearSelect($('ID_CLIENTE'),1);
		clearSelect($('ID_MARCA'),1);
	});
	
	$('ID_CLIENTE').addEvent('change', function(){
		clearSelect($('ID_SUBCATEGORIA'),1);
		clearSelect($('ID_CATEGORIA'),1);
		montaOptionsAjax($('ID_CATEGORIA'),'<?php echo site_url('json/admin/getCategoriasByCliente'); ?>','id=' + this.value,'ID_CATEGORIA','DESC_CATEGORIA');
		montaOptionsAjax($('ID_TIPO_OBJETO'),'<?php echo site_url('json/admin/getTiposObjetosByCliente'); ?>','id=' + this.value,'ID_TIPO_OBJETO','DESC_TIPO_OBJETO');
	});
	
	$('ID_CATEGORIA').addEvent('change', function(){
		clearSelect($('ID_SUBCATEGORIA'),1);
		montaOptionsAjax($('ID_SUBCATEGORIA'),'<?php echo site_url('json/admin/getSubcategoriasByCategoria'); ?>','id=' + this.value,'ID_SUBCATEGORIA','DESC_SUBCATEGORIA');
	});
	
	$('ID_FABRICANTE').addEvent('change', function(){
		clearSelect($('ID_MARCA'),1);										   
		montaOptionsAjax($('ID_MARCA'),'<?php echo site_url('json/admin/getMarcasByFabricante'); ?>','id=' + this.value,'ID_MARCA','DESC_MARCA');
	});
});

$j(function(){
	$j('.ui-datepicker').hide();
});
    </script>
  
 
</p> 

</div> <!-- Final de Content Global -->

<?php $this->load->view("ROOT/layout/footer") ?>