<?php $this->load->view("ROOT/layout/header") ?>
<div id="contentGlobal">
<h1>Manter controllers</h1>
  <form action="<?php echo site_url('controllers/lista'); ?>" method="post" id="pesquisa_simples" name="form1">
    <table width="100%" class="Alignleft">
      <tr>
        <?php if($podeAlterar): ?>
        <td width="10%" rowspan="2" align="center"><a href="<?php echo site_url('controllers/form'); ?>"><img src="<?php echo base_url().THEME; ?>img/file_add.png" alt="Pesquisar" width="31" height="31" border="0" /></a></td>
        <?php endif; ?>
        <td width="36%">Nome da controller</td>
        <td width="34%">Itens por p&aacute;gna</td>
        <td width="20%" rowspan="2"><a class="button" href="javascript:" title="Pesquisar" onclick="$j(this).closest('form').submit()"><span>Pesquisar</span></a></td>
      </tr>
      <tr>
        <td><input type="text" name="DESC_CONTROLLER" id="DESC_CONTROLLER" value="<?php echo !empty($busca['DESC_CONTROLLER']) ? $busca['DESC_CONTROLLER'] : ''; ?>" /></td>
        <td><select name="pagina" id="pagina">
            <?php
            $pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
            for($i=5; $i<=50; $i+=5){
				printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
            }
            ?>
          </select></td>
      </tr>
    </table>
  </form>
  <?php if(!empty($lista)): ?>
  <div id="tableObjeto">
    <table width="100%" class="tableSelection">
      <thead>
        <tr>
          <?php if($podeAlterar): ?>
          <td width="8%">Editar</td>
          <?php endif; ?>
          <td width="92%">Descri&ccedil;&atilde;o da controller</td>
        </tr>
      </thead>
      <?php foreach($lista as $item): ?>
      <tr>
        <?php if($podeAlterar): ?>
        <td align="center"><a href="<?php echo site_url('controllers/form/' . $item['ID_CONTROLLER'] ); ?>"><img src="<?php echo base_url().THEME; ?>img/file_edit.png" alt="Editar" width="31" height="31" border="0" /></a></td>
        <?php endif; ?>
        <td><?php echo $item['DESC_CONTROLLER']; ?></td>
      </tr>
      <?php endforeach; ?>
    </table>
  </div>
  <span class="paginacao"><?php echo $paginacao; ?></span>
  <?php else: ?>
  Nenhum resultado encontrado
  <?php endif ;?>
</div>
<?php $this->load->view("ROOT/layout/footer") ?>
