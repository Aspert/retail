<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
  <?php if ( isset($erros) && is_array($erros) ) : ?>
  <ul style="color:#ff0000;font-weight:bold;">
    <?php foreach ( $erros as $e ) : ?>
    <li>
      <?=$e?>
    </li>
    <?php endforeach; ?>
  </ul>
  <?php endif; ?>
  <?php echo form_open('controllers/save/'.$this->uri->segment(3));?>
  <h1>Cadastro de controllers</h1>
  <table width="100%" class="Alignleft">
    <tr>
      <td width="16%">Descri&ccedil;&atilde;o da controller</td>
      <td width="84%"><input name="DESC_CONTROLLER" type="text" value="<?php post('DESC_CONTROLLER'); ?>" /></td>
    </tr>
    <tr>
      <td>Chave interna</td>
      <td><input name="CHAVE_CONTROLLER" type="text" value="<?php post('CHAVE_CONTROLLER'); ?>" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><a href="<?php echo site_url('controllers/lista'); ?>" class="button"><span>Cancelar</span></a> <a href="javascript:;" onclick="$j(this).closest('form').submit()" class="button"><span>Salvar</span></a></td>
    </tr>
  </table>
  <?php echo form_close(); ?> </div>
<?php $this->load->view("ROOT/layout/footer") ?>
