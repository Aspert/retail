<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">

<h1>Ag&ecirc;ncias</h1>

<div id="pesquisa_simples">
	<?php echo form_open('agencia/lista');?>
		<?=form_hidden('tipo_pesquisa','simples');?>
        
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tablePesquisa">
  <tr>
    <td width="100">
    	<?php if($podeAlterar): ?>
			<?=anchor('agencia/form','<img src="'.base_url().THEME.'img/file_add.png"/>',array('title'=>'Novo agencia'))?>
        <?php endif; ?>
    </td>
    <td width="300" nowrap="nowrap">Nome: <?= form_input('DESC_AGENCIA', isset($busca['DESC_AGENCIA']) ? $busca['DESC_AGENCIA'] : '');?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>Status
      	<select name="STATUS_AGENCIA" id="STATUS_AGENCIA">
	        <option value="1"<?php if((isset($busca['STATUS_AGENCIA']) && ($busca['STATUS_AGENCIA'] == '1')) || (!isset($busca['STATUS_AGENCIA']))) echo 'selected="selected"'?>>Ativo</option>
        <option value="0"<?php if(isset($busca['STATUS_AGENCIA']) && ($busca['STATUS_AGENCIA'] == '0')) echo 'selected="selected"'?>>Inativo</option>
        <option value="2"<?php if(isset($busca['STATUS_AGENCIA']) && ($busca['STATUS_AGENCIA'] == '2')) echo 'selected="selected"'?>>Ambos</option>
      	</select>        
    </td>
    <td>Itens/P&aacute;gina:
		<select name="pagina" id="pagina">
        <?php
		$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
		for($i=5; $i<=50; $i+=5){
			printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
		}
		?>
		</select>
    </td>
    <td>&nbsp;</td>
    <td><a class="button" href="javascript:;" onclick="$j(this).closest('form').submit()"><span>Pesquisar</span></a></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
        
<div>
			
           </div>

		<div> </div>
		<div>

		</div>
		<div class="botao"></div>
	<?php echo form_close();?>
</div>
<?php if( (!empty($lista))&& (is_array($lista))):?>

<div id="tableObjeto"> 
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableSelection">

<thead>
<tr>
		<?php if($podeAlterar): ?>
        <th>Editar</th>
        <?php endif; ?>
		<th width="756" align="left">Nome</th>
		<th width="57" align="left">Feed RSS</th>
      <th width="57" align="left">Status</th>
</tr>
</thead>
		<?php $cont=0; ?>
		<?php foreach($lista as $item):?>

		<tr>
            <?php if($podeAlterar): ?>
			<td align="center" width="85">
				<?php echo anchor('agencia/form/'. $item['ID_AGENCIA'], '<img src="'. base_url().THEME.'img/file_edit.png" />',array('title'=>'Editar agencia'));?>			</td>
			<?php endif; ?>
            <td><?=$item['DESC_AGENCIA'];?></td>
            <td><?=$item['FEED_AGENCIA'];?></td>
			<td><?=$item['STATUS_AGENCIA'] == 1 ? 'Ativo' : 'Inativo';?></td>
			<?php $cont++; ?>
		</tr>
		<?endforeach;?>
</table>

</div> <!-- Final de Table Objeto -->

<span class="paginacao"><?=(isset($paginacao))?$paginacao:null?></span>
<?else:?>
	Nenhum resultado
<?endif;?>

</div> <!-- Final de Content Global  -->

<?php $this->load->view("ROOT/layout/footer") ?>