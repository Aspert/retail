<?php $this->load->view("ROOT/layout/header"); ?>

<div id="contentGlobal">

<?php if ( isset($erros) && is_array($erros) ) : ?>
	<ul style="color:#ff0000;font-weight:bold;">
		<?php foreach ( $erros as $e ) : ?>
			<li><?=$e?></li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>

<h1>Cadastro de Ag&ecirc;ncia</h1>

	<?php echo form_open_multipart('agencia/save/'.$this->uri->segment(3));?>
    
    	<table width="70%" border="0" cellspacing="0" cellpadding="0" style="margin:20px 0 20px 0;">
          <tr>
            <td width="200" style="text-align:left; padding:6px;">Nome:</td>
            <td style="text-align:left; padding:6px;"><input name="DESC_AGENCIA" type="text" id="DESC_AGENCIA" value="<?php post('DESC_AGENCIA'); ?>" size="100" /></td>
          </tr>
          <tr>
            <td style="text-align:left; padding:6px;">Feed RSS</td>
            <td style="text-align:left; padding:6px;"><input name="FEED_AGENCIA" type="text" id="FEED_AGENCIA" value="<?php post('FEED_AGENCIA'); ?>" size="100" maxlength="255" /></td>
          </tr>
          <tr>
            <td style="text-align:left; padding:6px;">Logotipo:</td>
            <td style="text-align:left; padding:6px;"><input name="logotipo" type="file" /> 
            (arquivo PNG de dimens&otilde;es 118 x 50)<br /></td>
          </tr>
          <tr>
            <td style="text-align:left; padding:6px;">Logotipo atual:</td>
            <td style="text-align:left; padding:6px;"><?php if(!empty($logo)): ?><img src="<?php echo base_url(); ?>img/agencia/<?php echo $logo; ?>" title="" alt="" /><?php endif; ?></td>
          </tr>
          <tr>
            <td style="text-align:left; padding:6px;">Situa&ccedil;&atilde;o: </td>
            <td style="text-align:left; padding:6px;">        
            <select name="STATUS_AGENCIA">
             <option value="1">ATIVO</option>
             <option value="0"<?php echo isset($_POST['STATUS_AGENCIA']) && $_POST['STATUS_AGENCIA'] == '0' ? ' selected="selected"' : ''; ?>>INATIVO</option>
            </select>
       		 </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>
        

<div class="clearBoth"></div>

    <a class="button" href="<?php echo site_url('agencia/lista'); ?>"><span>Cancelar</span></a>
    <a class="button" href="javascript:" onclick="$j(this).closest('form').submit()"><span>Salvar</span></a>	
		<div>&nbsp; </div>
		
	<?php echo form_close();?>
	
</div> <!-- Final de Content Global -->

<?php $this->load->view("ROOT/layout/footer") ?>