<h1>Informações</h1>

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableSelection">
	<thead>
		<tr bgcolor="#F1F1F1">
			<td width="40%"><strong>Nome</strong></td>
			<td width="40%"><strong>Código de Barras</strong></td>
			<td width="20%"><strong>Tipo</strong></td>
		</tr>
	</thead>
	<tr>
		<td><?php echo $ficha['NOME_FICHA']; ?></td>
		<td><?php echo $ficha['COD_BARRAS_FICHA']; ?></td>
		<td>Principal</td>
	</tr>
	<?php foreach($subfichas as $sub): ?>
		<tr>
			<td><?php echo $sub['NOME_FICHA']; ?></td>
			<td><?php echo $sub['COD_BARRAS_FICHA']; ?></td>
			<td>Subficha</td>
		</tr>
	<?php endforeach; ?>
</table>