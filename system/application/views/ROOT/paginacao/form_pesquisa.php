<h1>Fichas disponiveis para "<?php echo $praca['DESC_PRACA']; ?>"</h1>
<p>Clique nas imagens para adicionar os objetos à página selecionada</p>
<form id="formPesquisa">
<div id="tableObjeto">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableSelection">
    <thead>
      <tr>
      	<td align="right">Nome da ficha&nbsp;</td>
      	<td colspan="5" style="text-align:left"><input type="text" name="nome" id="nome" value="<?php post('nome');?>" /></td>
      	</tr>
      <tr>
      	<td align="right">Código regional&nbsp;</td>
      	<td colspan="5" style="text-align:left"><input type="text" name="codigo_regional" id="codigo_regional" value="<?php post('codigo_regional');?>" /></td>
      	</tr>
      <tr>
      <tr>
      	<td align="right">Código de barras&nbsp;</td>
      	<td colspan="5" style="text-align:left"><input type="text" name="codigo" id="codigo" value="<?php post('codigo');?>" />
      		<input type="button" name="btnPesquisar" id="btnPesquisar" value="Pesquisar" onclick="doPesquisa(0);" /></td>
      	</tr>
      <tr>
      	<td>&nbsp;</td>
      	<td colspan="5">&nbsp;</td>
      	</tr>
      <tr>
        <td>Produto</td>
        <td>Nome Produto</td>
        <td>Código de Barras</td>
        <td>Status</td>
        <td>Temporário</td>
        <td>Data Criação</td>
      </tr>
    </thead>
<?php
  $idx = 0;
  
  $idCenario = 0;
  
	foreach($itens as $ficha): 
		if( ($idCenario != $ficha['ID_CENARIO']) && ($ficha['ID_CENARIO'] > 0) ){
			$idCenario = $ficha['ID_CENARIO'];
?>
			<tr style="background-color:<?php echo $ficha['COR_CENARIO'] ?>;">
				<td colspan="6" style="">
					<?php echo "<strong>" . $ficha['NOME_CENARIO'] . "</strong> (" .$ficha['COD_CENARIO']. ")" ;?>
				</td>
			</tr>
<?php 
		}
?>
		<tr id="produto-<?php echo $ficha['ID_EXCEL_ITEM']; ?>" class="produto_linha" onclick="$j(this).closest('tr').hide();$j('#subficha-<?php echo $ficha['ID_EXCEL_ITEM']; ?>').hide();<?php printf("addFicha(%d,'%s','%s','%s',%s,%s,%d,'%s','%s','%s','%s','%s','%s'); updateOrdem();",$ficha['ID_EXCEL_ITEM'], str_replace(array('"',"'"), array('',''), $ficha['NOME_FICHA']),$ficha['DESC_CATEGORIA'],$ficha['DESC_SUBCATEGORIA'], 'pracaAtual', "\$j('#combo_praca-'+pracaAtual).val()", $ficha['FILE_ID'],$ficha['COD_FICHA'],money( $ficha['PRECO_VISTA'] ),'',$ficha['NOME_CENARIO'],$ficha['COD_CENARIO'],$ficha['COR_CENARIO']); ?>">
			<td  class="alter" style="cursor:pointer">
				<?php if(!empty($ficha['FILE_ID'])): ?>
          			<img src="<?= base_url() ?>img.php?img_id=<?= $ficha['FILE_ID'] ?>&amp;rand=<?= rand()?>&amp;a.jpg" title="<?=$ficha['NOME_FICHA']?>" width="50" border="0" />
	        	<?php endif; ?>
			</td>
			<td style="cursor:pointer" class="alter">
				<?=$ficha['NOME_FICHA'];?>
				<br />
 				<?=$ficha['DESC_CATEGORIA'];?> - <?=$ficha['DESC_SUBCATEGORIA'];?>
	  		</td>
      		<td style="cursor:pointer" class="alter"><?=$ficha['COD_BARRAS_FICHA'];?></td>
      		<td style="cursor:pointer" class="alter"><?=ucfirst(!empty($ficha['DESC_APROVACAO_FICHA']) ? $ficha['DESC_APROVACAO_FICHA'] : '');?></td>
      		<td style="cursor:pointer" class="alter"><?= $ficha['FLAG_TEMPORARIO'] == 1 ? 'Sim' : 'Não';?></td>
      		<td style="cursor:pointer" class="alter"><?=format_date_to_form($ficha['DT_INSERT_FICHA']);?></td>
	    </tr>
    	<?php if(count($ficha['SUBFICHAS']) > 0): ?>
    		<tr id="subficha-<?php echo $ficha['ID_EXCEL_ITEM']; ?>" class="" onclick="$j(this).closest('tr').hide();$j('#produto-<?php echo $ficha['ID_EXCEL_ITEM']; ?>').hide();<?php printf("addFicha(%d,'%s','%s','%s',%s,%s,%d,'%s','%s','%s','%s','%s','%s'); updateOrdem();",$ficha['ID_EXCEL_ITEM'], str_replace(array('"',"'"), array('',''), $ficha['NOME_FICHA']),$ficha['DESC_CATEGORIA'],$ficha['DESC_SUBCATEGORIA'], 'pracaAtual', "\$j('#combo_praca-'+pracaAtual).val()", $ficha['FILE_ID'],$ficha['COD_FICHA'],money( $ficha['PRECO_VISTA'] ),'',$ficha['NOME_CENARIO'],$ficha['COD_CENARIO'],$ficha['COR_CENARIO']); ?>">
    			<td style="cursor:pointer">Subfichas</td>
    			<td style="cursor:pointer" colspan="6" style="text-align:left;">
				    <?php foreach ($ficha['SUBFICHAS'] as $subficha): ?>
		    			<b>Código de barras:</b> <?php echo $subficha['COD_BARRAS_FICHA'];?>
		    			<?php if(isset($subficha['LABEL_CAMPO_FICHA'])):?> 
		    				| <b>Campo Principal:</b> <?php echo $subficha['LABEL_CAMPO_FICHA'];?> - <?php echo $subficha['VALOR_CAMPO_FICHA'];?>
		    			<?php endif;?>
		    			<br>
				    <?php endforeach; ?>
				</td>
		    </tr>
		<?php endif; ?>
<?php
	  $idx++;
	  endforeach; 
?>
  </table>
</div> <!-- Final de Form Pesquisa -->

<div class="paginacao">
Ir para a pagina: 
<select onchange="doPesquisa(this.value)">
<?php
	for($i=0; $i<$total/$limit; $i++){
		echo '<option value="'.($i).'"';
		if( $i*$limit == $offset ){
			echo ' selected="selected"';
		}
		echo '>' . ($i+1) . '</option>' . PHP_EOL;
	}
?>
</select>
</div>

</form>