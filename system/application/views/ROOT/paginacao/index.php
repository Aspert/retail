<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">

<h1>Pagina&ccedil;&atilde;o</h1>

<div id="pesquisa_simples">
	<?php echo form_open('paginacao/index');?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin: 0 0 0 20px">
  <tr>
  	<?php if(!empty($_sessao['ID_AGENCIA'])): ?>
		<td width="15%" style="text-align:left">Ag&ecirc;ncia</td>
		<td style="text-align:left">Cliente</td>
	<?php elseif(!empty($_sessao['ID_CLIENTE'])): ?>
		<td width="15%" style="text-align:left">Cliente</td>
		<td style="text-align:left">Ag&ecirc;ncia</td>
	<?php endif; ?>
    <td style="text-align:left">Bandeira</td>
    <td style="text-align:left">Campanha</td>
    <td style="text-align:left">T&iacute;tulo do Job</td>
    <td style="text-align:left">Itens por p&aacute;gina</td>
    <td width="10%" rowspan="2" style="text-align:center"><a class="button" href="javascript:" onclick="$j(this).closest('form').submit()"><span>Pesquisar</span></a></td>
  </tr>
  <tr>
	
	<?php if(!empty($_sessao['ID_AGENCIA'])): ?>
		<td style="text-align:left">
		  <?php sessao_hidden('ID_AGENCIA','DESC_AGENCIA'); ?>
		 </td>
		<td style="text-align:left">
		  <select name="ID_CLIENTE" id="ID_CLIENTE">
			<option value=""></option>
			<?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', !empty($busca['ID_CLIENTE']) ? $busca['ID_CLIENTE'] : ''); ?>
			</select>
		  </td>
	  <?php elseif(!empty($_sessao['ID_CLIENTE'])): ?>
		<td style="text-align:left"><?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?></td>
		<td style="text-align:left">
		  <select name="ID_AGENCIA" id="ID_AGENCIA">
			<option value=""></option>
			<?php echo montaOptions($agencias,'ID_AGENCIA','DESC_AGENCIA', !empty($busca['ID_AGENCIA']) ? $busca['ID_AGENCIA'] : ''); ?>
		  </select>
		</td>
	  <?php endif; ?>
	  
    <td style="text-align:left;"><?php if(!empty($_sessao['ID_PRODUTO'])): ?>
      <?php sessao_hidden('ID_PRODUTO','DESC_PRODUTO'); ?>
      <?php else: ?>
      <select name="ID_PRODUTO" id="ID_PRODUTO">
        <option value=""></option>
        <?php echo montaOptions($produtos,'ID_PRODUTO','DESC_PRODUTO', !empty($busca['ID_PRODUTO']) ? $busca['ID_PRODUTO'] : ''); ?>
      </select>
      <?php endif; ?></td>
    <td style="text-align:left;"><select name="ID_CAMPANHA" id="ID_CAMPANHA">
      <option value=""></option>
      <?php echo montaOptions($campanhas,'ID_CAMPANHA','DESC_CAMPANHA', !empty($busca['ID_CAMPANHA']) ? $busca['ID_CAMPANHA'] : ''); ?>
    </select></td>
    <td style="text-align:left;"><input type="text" name="TITULO_JOB" id="TITULO_JOB" value="<?php echo !empty($busca['TITULO_JOB']) ? $busca['TITULO_JOB'] : '' ?>" /></td>
    <td style="text-align:left;">
      <select name="pagina" id="pagina">
        <?php
		$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
		for($i=5; $i<=50; $i+=5){
			printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
		}
		?>
      </select>
    </td>
    </tr>
</table>

  <?php echo form_close();?>
</div>
<?php if( (isset($lista))&& (is_array($lista))):?> 
<div id="tableObjeto">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableSelection tableMidia">
<thead>
	<tr>
        <?php if($podeAlterar): ?>
		<th>Editar</th>
    <?php endif; ?>
		<th>Hist&oacute;rico</th>
		<th class="numeroJob">N&uacute;mero Job</th>
		<th class="tituloJob">T&iacute;tulo Job</th>
		<th class="inicioJob">In&iacute;cio  da Validade</th>
		<th class="terminoJob" align="left">Final da Validade</th>
		<th class="agenciaJob" align="left">Ag&ecirc;ncia</th>
		<th class="clienteJob" align="left">Cliente</th>
		<th class="bandeiraJob" align="left">Bandeira</th>
		<th class="campanhaJob" align="left">Campanha</th>
		<th class="mudancaJob" align="left">Mudan&ccedil;a Etapa</th>
		<th class="situacaoJob" align="left">Situa&ccedil;&atilde;o</th>
        <th align="left">&nbsp;</th>
  	</tr>
  </thead>
		<?php $cont=0; ?>
		<?php foreach($lista as $item):?>

		<tr class="alter" >
			<?php if($podeAlterar): ?>
            <td align="center">
				<?php echo anchor('paginacao/form/'. $item['ID_JOB'], '<img src="'. base_url().THEME.'img/file_edit.png" />',array('title'=>'Editar produto'));?>
            </td>
            <?php endif; ?>
            <td align="center"><a href="javascript:void(0);" onclick="Ficha.HistoricoJob(<?php echo $item['ID_JOB']; ?>)"><img src="<?=base_url().THEME?>img/visualizar_historico.png" border="0"/></a></td>
            <td align="center"><?=$item['ID_JOB'];?></td>
            <td align="center"><?=$item['TITULO_JOB'];?></td>
            <td align="center"><?=format_date_to_form($item['DATA_INICIO']);?></td>
		  <td><?=format_date_to_form($item['DATA_TERMINO']);?>
				<br />
			  <?= ($diff = strtotime($item['DATA_TERMINO']) - time()) > 0 ? segundosToHora($diff) : '&nbsp;';?></td>
			<td><?=$item['DESC_AGENCIA'];?></td>
            <td><?=$item['DESC_CLIENTE'];?></td>
			<td><?=$item['DESC_PRODUTO'];?></td>
			<td><?=$item['DESC_CAMPANHA'];?></td>
       		<td class="alter"><?=date('d/m/Y H:i',strtotime($item['DATA_MUDANCA_ETAPA']));?></td>
			<td><?=$item['DESC_STATUS'] . getRevisaoJob($item);?></td>
			<td><?php echo sinalizarAlteracoes($item); ?></td>
			<?php $cont++; ?>
		</tr>
		<?endforeach;?>
</table>
</div>
<span class="paginacao"><?=(isset($paginacao))?$paginacao:null?></span>
<?else:?>
	Nenhum resultado
<?endif;?>

<script type="text/javascript">
<?php if(!empty($_sessao['ID_CLIENTE'])): ?>
$('ID_AGENCIA').addEvent('change', function(){
	clearSelect($('ID_CAMPANHA'), 1);
	clearSelect($('ID_PRODUTO'), 1);
	montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByAgencia'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
});
<?php endif; ?>

<?php if(!empty($_sessao['ID_AGENCIA'])): ?>
$('ID_CLIENTE').addEvent('change', function(){
	clearSelect($('ID_CAMPANHA'), 1);
	clearSelect($('ID_PRODUTO'), 1);
	montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByCliente'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
});
<?php endif; ?>

$('ID_PRODUTO').addEvent('change', function(){
	montaOptionsAjax($('ID_CAMPANHA'),'<?php echo site_url('json/admin/getCampanhasByProduto'); ?>','id=' + this.value,'ID_CAMPANHA','DESC_CAMPANHA');
});

$j(function(){
   configuraPauta('<?php echo $busca['ORDER']; ?>', '<?php echo $busca['ORDER_DIRECTION']; ?>');
});

</script>

</div> <!-- Final de Content Global -->

<?php $this->load->view("ROOT/layout/footer") ?>