<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">

<h1>Pagina&ccedil;&atilde;o</h1>
<ul>
	<li>Ao alterar a página, os elementos serão gravados;</li>
	<li>Ao remover um item, ele perde sua página/ordem. Para inseri-lo novamente, utilize o botão &quot;Adicionar Fichas&quot;;</li>
	<li>O plano de marketing serve como referência e a validação será efetuada no momento de troca de etapa.</li>
</ul>
<br />

<?php
$cor = '';
?>

<form method="post" action="<?php echo site_url('paginacao/save/' . $this->uri->segment(3)); ?>" id="form1">
<div id="tabs">

	<!-- inicio abas -->
	<ul>
      <li><a href="#dados_job">Dados do Job</a></li>
     <?php foreach($pracas as $item): ?>
      <li><a href="#tab-<?php echo $item['ID_PRACA']; ?>"><?php echo $item['DESC_PRACA']; ?></a></li>
     <?php endforeach; ?>
    </ul>
    <!-- fim abas -->
	
	<!-- inicio aba com os dados do job -->
    <div id="dados_job">
      <table width="100%" class="Alignleft">
        <tr>
          <td width="17%"><strong>Ag&ecirc;ncia</strong></td>
          <td width="83%"><?php echo $job['DESC_AGENCIA']; ?></td>
        </tr>
        <tr>
          <td><strong>Cliente</strong></td>
          <td><?php echo $job['DESC_CLIENTE']; ?></td>
        </tr>
        <tr>
          <td><strong>Bandeira</strong></td>
          <td><?php echo $job['DESC_PRODUTO']; ?></td>
        </tr>
        <tr>
          <td><strong>Campanha</strong></td>
          <td><?php echo $job['DESC_CAMPANHA']; ?></td>
        </tr>
        <tr>
          <td><strong>Tipo de pe&ccedil;a</strong></td>
          <td><?php echo $job['DESC_TIPO_PECA']; ?></td>
        </tr>
        <?php if(!$_sessao['IS_HERMES']){?>
        <tr>
          <td><strong>Tipo de Job</strong></td>
          <td><?php echo $job['DESC_TIPO_JOB']; ?></td>
        </tr>
        <?php }?>
        <tr>
          <td><strong>N&uacute;mero do Job</strong></td>
          <td><?php echo $job['NUMERO_JOB_EXCEL']; ?></td>
        </tr>
        <tr>
          <td><strong>T&iacute;tulo</strong></td>
          <td><?php echo $job['TITULO_JOB']; ?></td>
        </tr>
        <tr>
		<tr>
			<td><strong>Usu&aacute;rio Respons&aacute;vel</strong></td>
			<td><?php echo $job['NOME_USUARIO_RESPONSAVEL']; ?></td>
		</tr>
          <td><strong>N&uacute;mero de p&aacute;ginas</strong></td>
          <td><?php echo $job['PAGINAS_JOB']; ?></td>
        </tr>
        <?php if(!$_sessao['IS_HERMES']){?>
		<tr>
			<td><strong>Formato</strong></td>
			<td><?php echo centimetragem($job['LARGURA_JOB']), ' x ', centimetragem($job['ALTURA_JOB']); ?></td>
		</tr>
		<?php }?>
        <tr>
          <td><strong>In&iacute;cio  da Validade</strong></td>
          <td><?php echo format_date_to_form($job['DATA_INICIO']); ?></td>
        </tr>
        <tr>
          <td><strong>Final da Validade</strong></td>
          <td><?php echo format_date_to_form($job['DATA_TERMINO']); ?></td>
        </tr>
        <tr>
          <td valign="top"><strong>Observa&ccedil;&otilde;es</strong></td>
          <td><?php echo nl2br($job['OBSERVACOES_EXCEL']); ?></td>
        </tr>
        <tr>
          <td valign="top"><strong>Situa&ccedil;&atilde;o</strong></td>
          <td><?php echo $job['DESC_STATUS']; ?></td>
        </tr>
        <tr>
          <td valign="top"><strong>Etapa atual</strong></td>
          <td><?php echo $job['DESC_ETAPA']; ?></td>
        </tr>
      </table>
    </div>
    <!-- fim aba com os dados do job -->
    
    <!-- inicio conteudo -->
     <?php foreach($pracas as $praca): ?>
     <div id="tab-<?php echo $praca['ID_PRACA']; ?>">
       <!-- inicio da tabela com os valores -->
       
       <!-- inicio combo pagina e botao de ficha -->
       <div class="paginacao_toolbox">
           <select class="sel_pagina paginacao_combo" id="combo_praca-<?php echo $praca['ID_PRACA']; ?>" style="float:left; margin-right: 20px; font-size: 16px;">
            <?php 
            for($i=1; $i <= $job['PAGINAS_JOB']; $i++){
                printf('<option value="%d">Pagina %d</option>', $i, $i);
            }
            ?>
           </select>
    
          <a href="javascript:" onclick="abrePesquisa('<?php echo $praca['ID_PRACA']; ?>');" class="button" style="margin-top: 4px;"><span>Adicionar Fichas</span></a>
		  <a href="#" onclick="exibirPlanoMidia(<?php echo $praca['ID_PRACA']; ?>); return false;" class="button" style="margin-top: 4px;"><span>Exibir Espelho</span></a>
           <div class="clearBoth"></div>
       </div>
       <!-- fim combo pagina e botao de ficha -->
       
        <div class="sortable" style="padding-top: 10px;">
          <ul class="paginacao_container">
          </ul>
        </div>
		
		<div class="carregando">Carregando... </div>
		<?php if( count($pracas) > 1 ){ ?>
			<div style="border:1px solid #D3D3D3; background-color:#EDECEC; padding:10px; float:left; width:1070px; margin:10px 0 0 -1px;">
				<div style="float: left;">
					Copiar valores de 
					<select class="copiar" name="copiar_<?php echo $praca['ID_PRACA']; ?>" id="copiar_<?php echo $praca['ID_PRACA']; ?>">
					<option value=""></option>
						<?php echo montaOptions($pracas,'ID_PRACA', 'DESC_PRACA'); ?>
					</select>
				</div>
			</div>
		<?php } ?>
        <div class="clearBoth">&nbsp;</div>
	</div>
    
     <?php endforeach; ?>
    <!-- fim conteudo -->
    
</div>

<!-- inicio botoes -->
<table cellpadding="0" cellspacing="0" border="0" width="100%">
  <tr>
    <td width="50%" align="right" valign="bottom">
    	<a class="button" href="javascript:" onclick="btnSalvarHandler()"><span>Salvar</span></a>
        <a class="button" href="<?php echo site_url('paginacao'); ?>"><span>Cancelar</span></a>
        <?php if($podePassarEtapa && $this->uri->segment(3) != ''): ?>
      		<a class="button" href="javascript:" onclick="proximaEtapa()"><span>Próxima Etapa</span></a>
     	<?php endif; ?>
        <?php if( $podeVoltarEtapa && $this->uri->segment(3) != ''):?>
            <a class="button" href="<?php echo site_url('checklist/voltar_etapa/'.$job['ID_JOB']); ?>" ><span>Voltar Etapa</span></a>
        <?php endif;?>
    </td>
    <td width="6%" align="left" valign="bottom"></td>
    <td width="44%" align="left" valign="bottom"></td>
  </tr>
</table>
<!-- fim botoes -->
/form>

<!-- inicio modelo -->
<ul id="ulmodelo">
    <li class="paginacao_ficha{DESTAQUE} ficha_{idficha}">
<!--      	<div class="paginacao_gap_esquerda">&nbsp;</div>-->
        <div class="paginacao_gap_esquerda" style="border:1px solid black;cursor:default;" title="Cenário: {nome_cenario} ({cod_cenario})">
        </div>
       <div class="paginacao_centro">
            <div class="paginacao_foto">
              <a class="fancy" href="<?php echo base_url().'img.php?img_id={fileID}&img_size=big&a.jpg'; ?>">
                <img src="<?php echo base_url().'img.php?img_id={fileID}' ?>" height="50" />
              </a>
              <input type="hidden" name="codigo_item" value="{idficha}" class="codigo" />
			  <input type="hidden" name="item[{idficha}][ORDEM_ITEM]" value="" class="ordem" />
              <input type="hidden" name="item[{idficha}][PAGINA_ITEM]" value="{pagina}" />
            </div>
            <div class="paginacao_descricao">
                <h2>{titulo}</h2>
                <h3>{codigo}</h3>
                <h3>{categoria}</h3>
                <h3>{subcategoria}</h3>
            </div>
        </div>
        <div class="paginacao_gap_direita">
			<a href="javascript:" class="remover" onclick="removerFicha(this);"><img src="<?php echo base_url().THEME; ?>img/close-256x256.png" width="12" height="12" alt="Remover" /></a>
        </div>
        <div class="paginacao_gap_direita subficha" style="height:5px;">
       	</div>
        <div class="paginacao_gap_direita">
			<a href="javascript:" class="" onclick='getDivModal().remove();displayMessagewithparameter("<?php echo base_url().index_page()."/paginacao/subfichas"; ?>/{idficha}", 600,400);'><img src="<?php echo base_url().THEME; ?>img/add.png" width="12" height="12" alt="Subfichas" /></a>
        </div>
		<div class="paginacao_preco_item">R$ {preco}</div>
        <div class="paginacao_ordem">
        	<strong>Ordem:</strong> <span class="ordem"></span>
        </div>
    </li>
</ul>
<!-- fim modelo -->

<!-- plano de marketing -->
<?php foreach($plano_midia as $praca): ?>
<div title="Plano de Marketing - <?php echo $praca['DESC_PRACA']; ?>" style="display:none" id="plano_midia_<?php echo $praca['ID_PRACA']; ?>">
	<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tableSelection plano" title="Plano de Marketing - <?php echo $praca['DESC_PRACA']; ?>">
		<?php foreach($praca['categorias'] as $categoria): ?>
			<tbody id="<?php echo $categoria['ID_CATEGORIA']; ?>" class="categoria" title="<?php echo $categoria['DESC_CATEGORIA']; ?>">
			<tr>
				<td style="text-align:left"><strong><?php echo $categoria['DESC_CATEGORIA']; ?></strong></td>
				<td><strong>Pg.</strong></td>
				<td><strong>Qtde.</strong></td>
			</tr>
			<?php foreach($categoria['subcategorias'] as $item): ?>
			<tr class="item" id="sub-<?php printf('%d-%d', $item['ID_PRACA'], $item['ID_SUBCATEGORIA']); ?>" title="<?php echo $item['DESC_CATEGORIA'] . ' - ' . $item['DESC_SUBCATEGORIA']; ?>">
				<td style="padding-left: 20px; text-align:left"><?php echo $item['DESC_SUBCATEGORIA']; ?></td>
				<td class="qtde" style="text-align:left" width="16%"><?php echo $item['PAGINA_EXCEL_CATEGORIA']; ?></td>
				<td class="qtde" style="text-align:left" width="16%"><?php echo $item['QTDE_EXCEL_CATEGORIA']; ?></td>
			</tr>
			<?php endforeach;?>
			</tbody>
		<?php endforeach;?>
	</table>
</div>
<?php endforeach; ?>
<!-- fim : plano de marketing -->

<script type="text/javascript">
var idjob                = <?php printf('%d', $this->uri->segment(3)); ?>;
var modelo               = null;
var sortOptions          = null;
var pracaAtual           = 0;
var paginas              = <?php printf('%d', $job['PAGINAS_JOB']); ?>;
var urlCarregarObjetos   = base_url + 'index.php/json/fluxo/get_itens_paginacao';
var urlRemoverItemPagina = base_url + 'index.php/json/fluxo/remove_item_paginacao';
var urlSalvarPagina      = base_url + 'index.php/json/fluxo/salvar_paginacao';
var urlChecarPaginacao   = base_url + 'index.php/json/fluxo/checar_paginacao';
var urlPesquisa          = base_url + 'index.php/paginacao/form_pesquisa/'+idjob;
var urlJob               = base_url + 'index.php/paginacao/form/'+idjob;
var urlCopiarPaginacao   = base_url + 'index.php/paginacao/copiar_paginacao';
var panel                = null;
var limit                = 50;

/*********************************************************************************/
/********************** ACOES DOS ELEMENTOS VISUAIS DAS ABAS *********************/
/*********************************************************************************/
// funcao para inicializar os elementos
function inicializar(){
	// recupera o html do html
	modelo = $j('#ulmodelo').html();
	// remove o modelo do DOM
	$j('#ulmodelo').remove();
	// cria as abas
	$j('#tabs').tabs({select: tabSelectHandler});

	//Ir para aba
	<?php if ( !empty($abaCopiada) ){ ?>
		$j('#tabs').tabs('select', '#tab-<?php echo($abaCopiada); ?>');
	<?php } ?>
	
	// coloca o handler quando mudar a pagina
	$j('.sel_pagina').change(mudaPaginaHandler);

	$j('.copiar').change(function(){
		var to = this.id.replace('copiar_','');
		var from = this.value;

		if( to == from ){
			alert('Você não pode copiar da mesma praça!');
			return;
		}
		
		if( from != '' ){
			// envia os dados para carregar os elementos, e quando terminar de carregar...
			$j.post(urlCopiarPaginacao, {to:to, from:from, idjob:idjob}, function( retorno ){
				refresh();
			}, 'json');
		}

	});
}

// handler para quando mudar a aba
function tabSelectHandler(evt, ui){
	
	// se nao for a aba inicial
	if( ui.panel.id != 'dados_job' ){
		// pega o codigo da praca
		pracaAtual = parseFloat(ui.panel.id.split('-')[1]);
		// pega o painel
		panel      = $j(ui.panel);
		// carrega os elementos da aba
		carregaObjetosPagina(pracaAtual, $j(ui.panel).find('.sel_pagina').val()); 
	}
}

// handler para quando mudar a pagina
function mudaPaginaHandler(){
	// carrega os elementos da pagina atual
	carregaObjetosPagina(pracaAtual, this.value); 
}

// funcao para carregar os elementos da pagina
function carregaObjetosPagina(idpraca, pagina){
	// atualiza a ordem
	updateOrdem();
	// salva os elementos atuais
	// e quando terminar de salvar, executa a funcao
	// para carregar os elementos da pagina selecionada
	salvar(function(){
		
		// se panel nao estiver definido
		if( panel == null ){
			// sai da funcao
			return;
		}
					
		// variaveis para carregamento
		var data = {};
		data.idjob   = idjob;
		data.idpraca = pracaAtual;
		data.pagina  = panel.find('.sel_pagina').val();

		// mostra o elemento de carregando
		$j('.carregando').show();
		// envia os dados para carregar os elementos, e quando terminar de carregar...
		$j.post(urlCarregarObjetos, data, function(json){
			// ... para cada elemento carregado
			$j.each(json, function(){
				// adiciona uma ficha para paginacao
				addFicha(this.ID_EXCEL_ITEM, this.NOME_FICHA, this.DESC_CATEGORIA, this.DESC_SUBCATEGORIA, this.DESC_PRACA, this.PAGINA_ITEM, this.FILE_ID, this.COD_BARRAS_FICHA, this.PRECO_VISTA, this.DESTAQUE, this.NOME_CENARIO, this.COD_CENARIO, this.COR_CENARIO);
			});
			
			// esconde o elemento de carregando
			$j('.carregando').hide();
			// atualiza a ordem
			updateOrdem();
			
		},'json');
	});
}

// metodo para remover os elementos visuais da tela
function limpaObjetos(){
	// limpa o container de paginacao
	$j('.paginacao_container').html('');
}


// metodo para remover uma ficha
function removerFicha(el){
	// variaveis
	var data = {};
	// pega o codigo da ficha
	data.iditem = $j(el).parent().parent().find('.codigo').val();
	// envia os dados para o endereco para tirar a ordem desta ficha
	$j.post(urlRemoverItemPagina, data);
	// remove o elemento visualmente
	$j(el).closest('li').remove();
	// atualiza a ordem
	updateOrdem();
}

// metodo para adicionar uma ficha
function addFicha(id, nome, categoria, subcategoria, praca, pg, fileid, codigo, preco, destaque, nomeCenario, codCenario, corCenario){
	// copia o modelo 
	var html = modelo;
	// limite de caracteres
	var limiteChars = 14;
	// se nao tem categoria
	if( categoria == null ) categoria = '';
	// se nao tem subcategoria
	if( subcategoria == null ) subcategoria = '';
	// se nao tem destaque
	if( destaque == null ) destaque = '';

	// troca os nomes
	html = html.replace(/%7B/g, '{').replace(/%7D/g,'}');
	html = html.replace(/\{titulo\}/g, '<span title="'+nome+'">' + nome.substr(0, limiteChars) + '...</span>');
	html = html.replace(/\{codigo\}/g, '<span title="'+codigo+'">' + codigo + '</span>');
	html = html.replace(/\{categoria\}/g, '<span title="'+categoria+'">' + categoria.substr(0, limiteChars) + '...</span>');
	html = html.replace(/\{subcategoria\}/g, '<span title="'+subcategoria+'">' + subcategoria.substr(0, limiteChars) + '...</span>');
	html = html.replace(/\{fileID\}/g, fileid);
	html = html.replace(/\{idficha\}/g, id);
	html = html.replace(/\{pagina\}/g, pg);
	html = html.replace(/\{preco\}/g, preco);
	html = html.replace(/\{nome_cenario\}/g, nomeCenario);
	html = html.replace(/\{cod_cenario\}/g, codCenario);
	html = html.replace(/\{DESTAQUE\}/g, destaque);

	// pega o container de elementos
	var ul = $j(panel).find('ul');

	// transforma o html em um elemnto jquery
	html = $j(html);

	// aki procuramos a divi de cenario e caso a ficha for relacionada a um senario a pintamos ou apagamos
	if( nomeCenario != null && nomeCenario != ''){
		$j(html).find('.paginacao_gap_esquerda').css('background-color', corCenario);
	}
	else{
		$j(html).find('.paginacao_gap_esquerda').css('border-color', '#ffffff');
		$j(html).find('.paginacao_gap_esquerda').css('cursor', 'move');
		$j(html).find('.paginacao_gap_esquerda').attr('title', '');
	}

	// adiciona a ficha recem criada
	ul.append( html );
		
	ul.sortable({
		update: updateOrdem,
		items: 'li'
	});
	
}

// atualiz a ordem dos elementos na tela
function updateOrdem(){
	// para cada elemento que pode ser ordenado
	$j('.sortable').each(function(){
		// a primeira posicao sempre eh 1
		var idx=1;
		// para cada elemento ficha encontrado
		$j(this).find('li').each(function(){
			// atualiza o input 
			$j(this).find('input.ordem').val( idx );
			// atualiza a ordem visual
			$j(this).find('span.ordem').html( idx );
			// aumenta o valor da ordem para o proximo elemento
			idx++;
		});
	});
	
	// coloca o handler para poder abrir a imagem maior quando clicar
	$j('.fancy').fancybox();
	
}

// abre a tela de pesquisa
function abrePesquisa(praca){
	// remove a modal atual
	getDivModal().remove();
	// abre a modal
	displayStaticMessagewithparameter('', 800, 600);
	// chama a pesquisa
	doPesquisa(0);
}

// efetua uma pesquisa no formulario de busca de fichas
function doPesquisa(pg){
	// pega os elementos do formulario serializados
	var data   = $j('#formPesquisa').serialize();
	// indica o offset de elementos
	var offset = pg * limit;
	// adiciona o offset e limit nos dados
	data += '&offset='+offset+'&limit='+limit;
	// efetua a consulta ...
	$j.post(urlPesquisa + '/' + pracaAtual, data, function(html){
		// ... atualiza o conteudo da pagina com o resultado
		getDivModal().html( html );
	});
}

// handler para quando clicar no botao de salvar
function btnSalvarHandler(){
	// salva os elementos
	salvar(function(){
		// atualiza a pagina
		location.href = urlJob;
	});
}

// funcao para salvar os elementos na tela
function salvar(callback){
	// se o painel atual esta definido e existem elementos para salvar
	if( panel != null && $j(panel).find('li').length > 0 ){
		// remove o modal de mensagem
		getDivModal().remove();
		// exibe a mensagem na tela de que esta salvando
		displayStaticMessagewithparameter('Salvando dados',200,80);
		// serializa os dados
		var data = $j('#form1').serialize();
		// grava os elementos
		$j.post(urlSalvarPagina, data, function(){
			// remove os elementos visuais
			limpaObjetos();
			// fecha a mensagem
			closeMessage();
			// chama a funcao de salvar novamente
			salvar(callback);
		});
		
		return;
	}
	
	// se chegou aqui, ou eh porque nao tem elementos para salvar
	// ou o painel nao esta definido
	// se definiu um callback
	if( callback ){
		// chama o callback
		callback.call(null);
	}
}

// exibe o plano de midia de uma determinada praca
function exibirPlanoMidia(idpraca){
	// configura o dialog para o plano de midia e abre
	$j('#plano_midia_'+idpraca).dialog({
		autoOpen: false,
		width: 300,
		modal: false
	}).dialog('open');
}

<?php if($podePassarEtapa && $this->uri->segment(3) != ''): ?>
// envia o job para proxima etapa
// antes de enviar, checa se a paginacao esta correta
function proximaEtapa(){
	salvar(checaPaginacao);
}

// checa se esta tudo correto na paginacao
// para enviar o job para a proxima etapa
function checaPaginacao(){
	// dados que vao ser enviados por post
	var data = {};
	data.id = idjob;
	
	// envia os dados ...
	$j.post(urlChecarPaginacao, data, function(html){
		// ... se o retorno for "ok"
		if( html == 'ok' ){
			// passa para a proxima etapa
			confirmaPassarEtapa();
		// se nao for ok
		} else {
			// remove a div anterior
			getDivModal().remove();
			// abre a modal de confirmacao
			getDivModal().dialog({
				width: 700,
				modal: true,
				title: 'Resultado da validação da paginação'
			}).dialog('open')
				// substitui o html da modal
				.html( html );

			// acao para quando clicar para confirmar para passar a etaoa
			$j('#btnConfirmar').click(confirmaPassarEtapa);
			// acao para quando cancelar e voltar para a paginacao
			$j('#btnCancelar').click(continuarPaginacao);
			// cria as abas na modal
			$j('#tabs_resultado').tabs();
			// recarrega os elementos da pagina atual
			carregaObjetosPagina(null,null);
		}
	});
}

// fecha a tela de apontamento de problemas da paginacao
function continuarPaginacao(){
	getDivModal().dialog('close');
}

// confirmacao pelo usuario que pode enviar 
// o job para a proxima etapa
function confirmaPassarEtapa(){
	$j('#form1')
		.attr('action','<?php echo site_url('paginacao/proxima_etapa/'.$this->uri->segment(3)); ?>')
		.submit();
}

<?php endif; ?>

$j(inicializar);

<? if(!empty($erros)): ?>
	alert("<? echo implode('\n',$erros); ?>");
<? endif;?>

</script>

</div> <!-- Final de Content Global -->

<?php $this->load->view("ROOT/layout/footer") ?>