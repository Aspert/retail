<p style="padding-bottom:20px;">
 Seguem abaixo os resultados da validação dos elementos paginados.<br />
 Uma ou mais categorias não tiveram suas informações preenchidas de acordo com o espelho job.
</p>
<div id="tabs_resultado">
	<ul>
		<?php foreach($resultado as $praca): ?>
			<li><a href="#praca-res-<?php echo $praca['ID_PRACA']; ?>"><?php echo $praca['DESC_PRACA']; ?></a></li>
		<?php endforeach; ?>
	</ul>
	
	<?php foreach($resultado as $praca): ?>
		<div id="praca-res-<?php echo $praca['ID_PRACA']; ?>">
			<div id="tableObjeto">
				<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tableSelection">
					<thead>
						<tr>
							<th width="27%">Categoria</th>
							<th width="21%">Subcategoria</th>
							<th width="14%">Página</th>
							<th width="18%">Qtde. Esperada</th>
							<th width="20%">Qtde. Informada</th>
						</tr>
					</thead>
					<?php foreach($praca['ITENS'] as $item): ?>
					<tr>
						<td><?php echo $item['DESC_CATEGORIA']; ?></td>
						<td><?php echo $item['DESC_SUBCATEGORIA']; ?></td>
						<td><?php echo $item['PAGINA_EXCEL_CATEGORIA']; ?></td>
						<td><?php echo $item['QTDE_EXCEL_CATEGORIA']; ?></td>
						<td><?php echo $item['TOTAL']; ?></td>
					</tr>
					<?php endforeach; ?>
				</table>
				<div id="clear:both">&nbsp;</div>
			</div>
			<div id="clear:both">&nbsp;</div>
		</div>
	<?php endforeach; ?>
</div>

<div>
	<a href="#" id="btnConfirmar" class="button" onclick="return false;"><span>Passar para próxima etapa mesmo assim</span></a>
	<a href="#" id="btnCancelar" class="button" onclick="return false;"><span>Continuar Paginação</span></a>
</div>