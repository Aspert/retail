<?php $this->load->view("ROOT/layout/header"); ?>
<div id="contentGlobal">

<?php echo form_open('campo/save/'.$this->uri->segment(3));?>
   
<h1>Campos para <?php ghDigitalReplace($_sessao, 'Fichas'); ?></h1>
		
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="Alignleft">
  <tr>
  	<td width="14%">Cliente</td>
  	<td width="86%"><?php if(!empty($_sessao['ID_CLIENTE'])): ?>
  		<?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?>
  		<br />
  		<?php else: ?>
  		<select name="ID_CLIENTE" id="ID_CLIENTE">
  			<option value=""></option>
  			<?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', post('ID_CLIENTE',true)); ?>
  			</select>
  		<?php endif; ?>
  		<?php showError('ID_CLIENTE'); ?></td>
  	</tr>
  <tr>
    <td>Nome</td>
    <td><input name="NOME_CAMPO" id="NOME_CAMPO" type="text"  value="<?php post('NOME_CAMPO'); ?>" />
    <?php showError('NOME_CAMPO'); ?></td>
  </tr>
  <tr>
    <td>Descri&ccedil;&atilde;o para Help</td>
    <td><input name="DESCRICAO" type="text" id="DESCRICAO" value="<?php post('DESCRICAO'); ?>" size="45" /></td>
  </tr>
  <tr>
    <td>Status</td>
    <td>
    	<select name="STATUS_CAMPO" id="STATUS_CAMPO">
    		<option value="1"  <?php if( post('STATUS_CAMPO', true) == '1'){ echo 'selected="selected"'; } ?>>Ativo</option>
    		<option value="0"  <?php if( post('STATUS_CAMPO', true) == '0'){ echo 'selected="selected"'; } ?>>Inativo</option>    		
    	</select>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><a href="<?php echo site_url('campo/lista'); ?>" class="button"><span>Cancelar</span></a>
     <a href="javascript:;" onclick="$j(this).closest('form').submit()" class="button"><span>Salvar</span></a></td>
  </tr>
</table>

        
	<?php echo form_close();?>
</div>
<script type="text/javascript">
///$j('ID_AGENCIA').addEvent('change', function(){
//	clearSelect($j('ID_CLIENTE'),1);
//	montaOptionsAjax($j('ID_CLIENTE'),'<?php echo site_url('json/admin/getClientesByAgencia'); ?>','id=' + this.value,'ID_CLIENTE','DESC_CLIENTE');
//});

$j(function(){

	$j('#NOME_CAMPO').keypress(function(){
		var v = $j('#NOME_CAMPO').val();
		v = v.replace(" ",""); 
		v = v.replace(/( )/ig,"");
		$j('#NOME_CAMPO').val(v);
				
	});

	$j('#NOME_CAMPO').keyup(function(){

		var v = $j('#NOME_CAMPO').val();
		v = v.replace(" ",""); 
		v = v.replace(/( )/ig,"");
		$j('#NOME_CAMPO').val(v);
				
	});	
	
});


</script>

<?php $this->load->view("ROOT/layout/footer") ?>