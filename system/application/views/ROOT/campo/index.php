<?php $this->load->view("ROOT/layout/header") ?>
<div id="contentGlobal">
<h1>Campos para fichas</h1>
<form method="post" action="<?php echo site_url('campo/lista'); ?>" id="pesquisa_simples">
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="Alignleft">
  <tr>
    <td width="10%" rowspan="2" align="center">
    	<?php if($podeAlterar): ?>
    	<a href="<?php echo site_url('campo/form'); ?>"><img src="<?php echo base_url().THEME; ?>img/file_add.png" alt="Adicionar" /></a>
    	<?php endif; ?>    </td>
    <td width="20%">Cliente</td>
    <td width="25%">Campo</td>
    <td width="15%">Status</td>
    <td width="15%">Itens / p&aacute;gina</td>
    <td width="15%" rowspan="2">
    <a href="javascript:;" onclick="$j('#pesquisa_simples').submit()" class="button"><span>Pesquisar</span></a>
    </td>
  </tr>
  <tr>
    <td>
    	<?php if(!empty($_sessao['ID_CLIENTE'])): ?>
    	<?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?><?php else: ?>
    	<select name="ID_CLIENTE" id="ID_CLIENTE">
    		<option value=""></option>
    		<?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', !empty($busca['ID_CLIENTE']) ? $busca['ID_CLIENTE'] : ''); ?>
    		</select>
    	<?php endif; ?>
    	</td>
    <td><input type="text" name="NOME_CAMPO" id="NOME_CAMPO" value="<?php echo empty($busca['NOME_CAMPO']) ? '' : $busca['NOME_CAMPO']?>" /></td>
    <td>
    	<select name="STATUS_CAMPO" id="STATUS_CAMPO">
			<option value=""  <?php if( isset($busca['STATUS_CAMPO']) && $busca['STATUS_CAMPO'] == ''){ echo 'selected="selected"'; } ?>>Ambos</option>
    		<option value="1"  <?php if( isset($busca['STATUS_CAMPO']) && $busca['STATUS_CAMPO'] == '1'){ echo 'selected="selected"'; } ?>>Ativo</option>
    		<option value="0"  <?php if( isset($busca['STATUS_CAMPO']) && $busca['STATUS_CAMPO'] == '0'){ echo 'selected="selected"'; } ?>>Inativo</option>    		
    	</select>
    </td>
    <td><span class="campo esq">
      <select name="pagina" id="pagina">
        <?php
		$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
		for($i=5; $i<=50; $i+=5){
			printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
		}
		?>
      </select>
    </span></td>
    </tr>
</table>

</form>

<?php if( !empty($campos)) :?>
<br />
<div id="tableObjeto"><table cellspacing="1" cellpadding="2" border="0" width="99%" class="tableSelection">
        <thead>
            <tr>
                <?php if($podeAlterar): ?>
                <td width="5%">Editar</td>
                <?php endif; ?>
                <td width="35%">Cliente</td>
              	<td width="30%">Campo</td>
              	<td width="30%">Status</td>
          </tr>
        </thead>
		<?php foreach($campos as $item): ?>
		<tr>
          <?php if($podeAlterar): ?>
			<td align="center"><a href="<?php echo site_url('campo/form/'.$item['ID_CAMPO']); ?>"><img src="<?php echo base_url().THEME; ?>img/file_edit.png" alt="Adicionar" width="31" height="31" border="0" /></a></td>
          <?php endif; ?>
	      <td align="center"><?php echo $item['DESC_CLIENTE']; ?></td>
			<td align="center"><?php echo $item['NOME_CAMPO']; ?></td>
			<td align="center"><?php if( $item['STATUS_CAMPO'] == 0){ echo 'Inativo'; } else{ echo 'Ativo'; } ?></td>
		</tr>
        <?php endforeach; ?>
</table></div>
<span class="paginacao"><?php echo $paginacao; ?></span>
<?php else: ?>
	Nenhum resultado
<?php endif; ?>
</div>
<script type="text/javascript">
$('ID_AGENCIA').addEvent('change', function(){
	clearSelect($('ID_CLIENTE'),1);
	montaOptionsAjax($('ID_CLIENTE'),'<?php echo site_url('json/admin/getClientesByAgencia'); ?>','id=' + this.value,'ID_CLIENTE','DESC_CLIENTE');
});

</script>

<?php $this->load->view("ROOT/layout/footer") ?>