<script type="text/javascript">

var os_ok = false;
$j.each(plugins.minimo.os, function(key, val){
	if(val == plugins.usuario.os){
		os_ok = true;
	}
});
if( os_ok == false ){
	$j("#OS1").attr("src",'<?=base_url().THEME?>img/windows-icon-bw.jpg').attr("title",'Windows');
	$j("#OS2").attr("src",'<?=base_url().THEME?>img/linux-icon-bw.jpg').attr("title",'Linux');
	$j("#OS3").attr("src",'<?=base_url().THEME?>img/mac-icon-bw.jpg').attr("title",'Mac');
} else {
	var icons = {Linux:"<?=base_url().THEME?>img/linux-icon.jpg", Mac:"<?=base_url().THEME?>img/mac-icon.jpg", Windows:"<?=base_url().THEME?>img/windows-icon.jpg"};
	var sys = plugins.usuario.os;
	if( sys == 'Linux'){
		$j("#OS1").attr("src",'<?=base_url().THEME?>img/windows-icon-bw.jpg').attr("title",'Windows');
		$j("#OS2").attr("src",icons['Linux']).attr("title",sys);
		$j("#OS3").attr("src",'<?=base_url().THEME?>img/mac-icon-bw.jpg').attr("title",'Mac');
	}
	if( sys == 'Windows'){
		$j("#OS1").attr("src",icons['Windows']).attr("title",sys);
		$j("#OS2").attr("src",'<?=base_url().THEME?>img/linux-icon-bw.jpg').attr("title",'Linux');
		$j("#OS3").attr("src",'<?=base_url().THEME?>img/mac-icon-bw.jpg').attr("title",'Mac');
	}
	if( sys == 'Mac'){
		$j("#OS1").attr("src",'<?=base_url().THEME?>img/windows-icon-bw.jpg').attr("title",'Windows');
		$j("#OS2").attr("src",'<?=base_url().THEME?>img/linux-icon-bw.jpg').attr("title",'Linux');
		$j("#OS3").attr("src",icons['Mac']).attr("title",sys);
	}
}

var browser_ok = false;
$j.each(plugins.minimo.browser, function(key, val){
	if(val == plugins.usuario.browser){
		browser_ok = true;
	}
});
if( browser_ok == false ){
	$j("#browser1").attr("src",'<?=base_url().THEME?>img/ie-icon-bw.jpg').attr("title",'Internet Explorer');
	$j("#browser2").attr("src",'<?=base_url().THEME?>img/firefox-icon-bw.jpg').attr("title",'Firefox' );
	$j("#browser3").attr("src",'<?=base_url().THEME?>img/safari-icon-bw.jpg').attr("title",'Safari');
	$j("#browser4").attr("src",'<?=base_url().THEME?>img/chrome-icon-bw.jpg').attr("title",'Chrome');
} else {
	var icons = {Safari:"<?=base_url().THEME?>img/safari-icon.jpg", Internet_Explorer:"<?=base_url().THEME?>img/ie-icon.jpg", Firefox:"<?=base_url().THEME?>img/firefox-icon.jpg", Chrome:"<?=base_url().THEME?>img/chrome-icon.jpg"};
	var sys = plugins.usuario.browser;
	if(sys == 'Safari'){
		$j("#browser1").attr("src",'<?=base_url().THEME?>img/ie-icon-bw.jpg').attr("title",'Internet Explorer');
		$j("#browser2").attr("src",'<?=base_url().THEME?>img/firefox-icon-bw.jpg').attr("title",'Firefox' );
		$j("#browser3").attr("src",icons['Safari']).attr("title",sys);
		$j("#browser4").attr("src",'<?=base_url().THEME?>img/chrome-icon-bw.jpg').attr("title",'Chrome' );
	}
	if(sys == 'Internet_Explorer'){
		$j("#browser1").attr("src",icons['Internet_Explorer']).attr("title",sys.replace("_"," ") );
		$j("#browser2").attr("src",'<?=base_url().THEME?>img/firefox-icon-bw.jpg').attr("title",'Firefox');
		$j("#browser3").attr("src",'<?=base_url().THEME?>img/safari-icon-bw.jpg').attr("title",'Safari');
		$j("#browser4").attr("src",'<?=base_url().THEME?>img/chrome-icon-bw.jpg').attr("title",'Chrome' );
	}
	if(sys == 'Firefox'){
		$j("#browser1").attr("src",'<?=base_url().THEME?>img/ie-icon-bw.jpg').attr("title",'Internet Explorer');
		$j("#browser2").attr("src",icons['Firefox']).attr("title",sys);
		$j("#browser3").attr("src",'<?=base_url().THEME?>img/safari-icon-bw.jpg').attr("title",'Safari');
		$j("#browser4").attr("src",'<?=base_url().THEME?>img/chrome-icon-bw.jpg').attr("title",'Chrome');
	}	
	if(sys == 'Chrome'){
		$j("#browser1").attr("src",'<?=base_url().THEME?>img/ie-icon-bw.jpg').attr("title",'Internet Explorer');
		$j("#browser2").attr("src",'<?=base_url().THEME?>img/firefox-icon-bw.jpg').attr("title",'Firefox');
		$j("#browser3").attr("src",'<?=base_url().THEME?>img/safari-icon-bw.jpg').attr("title",'Safari');
		$j("#browser4").attr("src",icons['Chrome']).attr("title",sys);
	}
}

var browserVersion_ok = false;
$j.each(plugins.minimo.browserVersion, function(key, val){
	if(val == plugins.usuario.browserVersion && browser_ok == true){
		browserVersion_ok = true;
	}
});
if( browserVersion_ok == false && browser_ok == true){
	$j('#browserVersion').html( 'Versão não suportada.' );
} else if(browserVersion_ok == true && browser_ok == true){
	$j('#browserVersion').html( plugins.usuario.browser + ' ' + plugins.usuario.browserVersion );
}else{
	$j('#ver').hide();
}

var java_ok = false;
if(plugins.minimo.java == plugins.usuario.java){
	java_ok = true;
}
if( java_ok == false){
	$j('#java').html( 'Versão inferior a 6.' );
}else{
	$j('#java').html( plugins.usuario.java );
}

var flash_ok = false;
if(plugins.usuario.flash >= plugins.minimo.flash){
	flash_ok = true;
}
if( flash_ok == false){
	$j('#flash').html( 'Versão inferior a 10.' );
}else{
	$j('#flash').html( plugins.usuario.flash );
}

//ALERTAS

if(os_ok == false){
	$j('#ID_SO').attr('src','<?=base_url().THEME?>img/alerta-amarelo.jpg').attr('title','Sistema Operacional não homologado. Algumas funcionalidades podem não funcionar corretamente.');
}else{
	$j('#ID_SO').attr('src','<?=base_url().THEME?>img/valido.jpg').attr('title','Sistema Operacional suportado.');
}

if(browser_ok == false){
	$j('#ID_NAVEGADOR').attr('src','<?=base_url().THEME?>img/alerta-amarelo.jpg').attr('title','Navegador não homologado. Algumas funcionalidades podem não funcionar corretamente.');
}else{
	$j('#ID_NAVEGADOR').attr('src','<?=base_url().THEME?>img/valido.jpg').attr('title','Navegador suportado.');
}

if(browserVersion_ok == false && browser_ok == true){
	$j('#ID_VER_NAVEGADOR').attr('src','<?=base_url().THEME?>img/alerta-amarelo.jpg').attr('title','Algumas funcionalidades do sistema podem não funcionar corretamente. Atualize seu navegador.');
}else{
	$j('#ID_VER_NAVEGADOR').attr('src','<?=base_url().THEME?>img/valido.jpg').attr('title','Versão do navegador suportada.');
}

if(java_ok == false){
	$j('#ID_JAVA').attr('src','<?=base_url().THEME?>img/alerta-amarelo.jpg').attr('title','Algumas funcionalidades podem estar desabilitadas.');
	$j('#jvm').attr('src','<?=base_url().THEME?>img/java-icon.jpg');    
}else{
	$j('#ID_JAVA').attr('src','<?=base_url().THEME?>img/valido.jpg').attr('title','Versão do Java suportada.');
	$j('#javalink').hide();
}

if(flash_ok == false){
	$j('#ID_FLASH').attr('src','<?=base_url().THEME?>img/alerta-vermelho.jpg').attr('title','É necessário obter a ultima versão do Flash Player para utilizar o sistema.');
	$j('#flashplayer').attr('src','<?=base_url().THEME?>img/flash-icon.gif');    
}else{
	$j('#ID_FLASH').attr('src','<?=base_url().THEME?>img/valido.jpg').attr('title','Versão do Flash Player suportada.');
	$j('#flashplayerlink').hide();
}

</script>

<h1>Pré-Requisitos Mínimos</h1>

<form>
	<table align="center">
		<tr>
			<td></td>
			<td></td>
			<td></td>
		</tr>
	</table><br />
		<table align="center">
		<tr>
			<td></td>
			<td>Veja abaixo se o seu computador possui os requisitos mínimos para acessar este sistema. <br /> 
				 Posicione o cursor sobre os icones para obter mais informações.</td>
			<td></td>
		</tr>	
	</table>
	<br />
	<table align="center">
		<tr>
			<td><img id="ID_SO"></td>
			<td><b>Sistema Operacional:</b> </td>
			<td>
				<div>
					<img id="OS1" >
					<img id="OS2" >
					<img id="OS3" >				
				</div>
			</td>
		</tr>
<br/>		
		<tr>
			<td><img id="ID_NAVEGADOR"></td>
			<td><b>Navegador:</b> </td>
			<td>
				<div>
					<img id="browser1" >
					<img id="browser2" >
					<img id="browser3" >
					<img id="browser4" >
				</div>
			</td>
		</tr>
<br/>				
		<tr id="ver">
			<td><img id="ID_VER_NAVEGADOR"></td>
			<td><b>Versão do Navegador:</b> </td>
			<td><div id="browserVersion"></div></td>
		</tr>				
<br/>				
		<tr>
			<td><img id="ID_JAVA"></td>
			<td><b>Java:</b> </td>
			<td><div id="java"></div></td>
		</tr>				
<br/>				
		<tr>
			<td><img id="ID_FLASH"></td>
			<td><b>Flash Player:</b> </td>
			<td><div id="flash"></div></td>
		</tr>				
	</table>

<br/><br/>

	<table align="center">
		<tr>
			<td><img src="<?=base_url().THEME?>/img/alerta-amarelo.jpg"></td>
			<td>Não Impeditivo (Acesso ao sistema sem algumas funcionalidades).</td>
			<td></td>
		</tr>
		<tr>
			<td><img src="<?=base_url().THEME?>/img/alerta-vermelho.jpg"></td>
			<td>Impeditivo (Sem acesso ao sistema).</td>
			<td></td>			
		</tr>
		<tr>
			<td><img src="<?=base_url().THEME?>/img/valido.jpg"></td>
			<td>Válido (Todos os requisitos estão de acordo).</td>
			<td></td>			
		</tr>
	</table>
	
	<table align="center">
		<tr>
			<td></td>
			<td>
				<div>
					<a id="javalink" href="http://www.java.com/en/" style="font-size: 13px"><img id="jvm" >Clique aqui para atualizar o Java.</a>
					<a id="flashplayerlink" href="http://get.adobe.com/flashplayer/" style="font-size: 13px"><img id="flashplayer">Clique aqui para atualizar o Flash Player.</a>
				</div>
			</td>
			<td></td>
		</tr>
	</table>
	
</form>