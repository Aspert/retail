<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
  <h1>Cadastro de Itens do Menu</h1>
  <form action="<?php echo site_url('item_menu/lista'); ?>" method="post" id="pesquisa_simples" name="form1">
    <table width="100%" class="Alignleft">
      <tr>
        <?php if($podeAlterar): ?>
        <td width="8%" rowspan="2" align="center"><a href="<?php echo site_url('item_menu/form'); ?>"><img src="<?php echo base_url().THEME; ?>img/file_add.png" alt="Pesquisar" width="31" height="31" border="0" /></a></td>
        <?php endif; ?>
        <td width="18%">Controller</td>
        <td width="20%">Function</td>
        <td width="19%">Nome do item</td>
        <td width="17%">Itens por p&aacute;gna</td>
        <td width="18%" rowspan="2"><a class="button" href="javascript:" title="Pesquisar" onclick="$j(this).closest('form').submit()"><span>Pesquisar</span></a></td>
      </tr>
      <tr>
        <td><select name="ID_CONTROLLER" id="ID_CONTROLLER">
            <option value=""></option>
            <?php echo montaOptions($controllers,'ID_CONTROLLER','DESC_CONTROLLER', !empty($busca['ID_CONTROLLER']) ? $busca['ID_CONTROLLER'] : ''); ?>
          </select></td>
        <td><select name="ID_FUNCTION" id="ID_FUNCTION">
            <option value=""></option>
            <?php echo montaOptions($functions,'ID_FUNCTION','DESC_FUNCTION', !empty($busca['ID_FUNCTION']) ? $busca['ID_FUNCTION'] : ''); ?>
          </select></td>
        <td><input type="text" name="DESC_ITEM_MENU" id="DESC_ITEM_MENU" value="<?php echo !empty($busca['DESC_ITEM_MENU']) ? $busca['DESC_ITEM_MENU'] : ''; ?>" /></td>
        <td><select name="pagina" id="pagina">
            <?php
            $pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
            for($i=5; $i<=50; $i+=5){
				printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
            }
            ?>
          </select></td>
      </tr>
    </table>
  </form>
  <?php if(!empty($lista)): ?>
  <div id="tableObjeto">
    <table width="100%" class="tableSelection">
      <thead>
        <tr>
          <?php if($podeAlterar): ?>
          <td width="7%" align="center">Editar</td>
          <?php endif; ?>
          <?php if($podeRemover): ?>
          <td width="7%" align="center">Remover</td>
          <?php endif; ?>
          <td width="20%">Controller</td>
          <td width="39%">Function</td>
          <td width="18%">Menu</td>
          <?php if($podeAlterar): ?>
          <td width="9%">Ordenar</td>
          <?php endif; ?>
        </tr>
      </thead>
      <tbody id="tabela">
        <?php foreach($lista as $item): ?>
        <tr id="<?php echo $item['ID_ITEM_MENU']; ?>">
          <?php if($podeAlterar): ?>
          <td align="center"><a href="<?php echo site_url('item_menu/form/' . $item['ID_ITEM_MENU'] ); ?>"><img src="<?php echo base_url().THEME; ?>img/file_edit.png" alt="Editar" width="31" height="31" border="0" /></a></td>
          <?php endif; ?>
          <?php if($podeRemover): ?>
          <td align="center"><a href="#" onclick="remover(<?php echo $item['ID_ITEM_MENU']; ?>)"><img src="<?php echo base_url().THEME; ?>img/file_delete.png" alt="Remover" width="31" height="31" border="0" /></a></td>
          <?php endif; ?>
          <td><?php echo $item['DESC_CONTROLLER']; ?></td>
          <td><?php echo $item['DESC_FUNCTION']; ?></td>
          <td><?php echo $item['DESC_ITEM_MENU']; ?></td>
          <?php if($podeAlterar): ?>
          <td align="center"><a href="javascript:;"><img class="handler" src="<?php echo base_url().THEME; ?>img/move_icon.png" alt="Ordenar" width="31" height="31" border="0" /></a></td>
          <?php endif; ?>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
  <span class="paginacao"><?php echo $paginacao; ?></span>
  <?php else: ?>
  Nenhum resultado encontrado
  <?php endif ;?>
</div>
<script type="text/javascript">
function remover(id){
	if(confirm('Deseja realmente remover este item?')){
		location.href = '<?php echo base_url().index_page().'/item_menu/remover/' ?>'+id;
	}
}
$('ID_CONTROLLER').addEvent('change', function(){
	montaOptionsAjax($('ID_FUNCTION'),'<?php echo site_url('json/admin/getFunctionsByController'); ?>','id=' + this.value,'ID_FUNCTION','DESC_FUNCTION');
});

var sortInstance = new Sortables('tabela',{
	handles: '.handler',
	onComplete: function(el){
		var i = 1;
		var data = [];
		
		$each($('tabela').getChildren(), function(row){
			data.push('lista[' + (i++) + ']='+$(row).id);
		})
		
		new Ajax('<?php echo site_url('json/admin/updateItemMenuOrder'); ?>',{
			method: 'post'
		}).request(data.join('&'));
	}
});

</script>
<?php $this->load->view("ROOT/layout/footer") ?>
