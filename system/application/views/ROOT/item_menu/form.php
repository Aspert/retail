<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
  <?php if ( isset($erros) && is_array($erros) ) : ?>
  <ul style="color:#ff0000;font-weight:bold;">
    <?php foreach ( $erros as $e ) : ?>
    <li>
      <?=$e?>
    </li>
    <?php endforeach; ?>
  </ul>
  <?php endif; ?>
  <?php echo form_open_multipart('item_menu/save/'.$this->uri->segment(3));?>
  <h1>Cadastro de itens no menu</h1>
  <table width="100%" class="Alignleft">
    <tr>
      <td width="17%">Vincular a controller</td>
      <td width="83%"><select name="ID_CONTROLLER" id="ID_CONTROLLER">
          <option value=""></option>
          <?php echo montaOptions($controllers,'ID_CONTROLLER','DESC_CONTROLLER',post('ID_CONTROLLER',true)); ?>
        </select></td>
    </tr>
    <tr>
      <td>Function</td>
      <td><select name="ID_FUNCTION" id="ID_FUNCTION">
          <option value=""></option>
          <?php echo montaOptions($functions,'ID_FUNCTION','DESC_FUNCTION',post('ID_FUNCTION',true)); ?>
        </select></td>
    </tr>
    <tr>
      <td>Nome do item</td>
      <td><input name="DESC_ITEM_MENU" type="text" value="<?php post('DESC_ITEM_MENU'); ?>" /></td>
    </tr>
    <tr>
      <td>Ordem do item</td>
      <td><input name="ORDEM_ITEM_MENU" type="text" value="<?php post('ORDEM_ITEM_MENU'); ?>" /></td>
    </tr>
    <tr>
      <td>Enviar icone</td>
      <td><input name="file" type="file" size="40" /></td>
    </tr>
    <?php if(!empty($icone)): ?>
    <tr>
      <td>&Igrave;cone atual</td>
      <td><img src="<?php echo $icone ?>" align="Icone" /></td>
    </tr>
    <?php endif; ?>
    <tr>
      <td>&nbsp;</td>
      <td><a href="<?php echo site_url('item_menu/lista'); ?>" class="button"><span>Cancelar</span></a> <a href="javascript:;" onclick="$j(this).closest('form').submit()" class="button"><span>Salvar</span></a></td>
    </tr>
  </table>
  <?php echo form_close(); ?> </div>
<script type="text/javascript">

$('ID_CONTROLLER').addEvent('change', function(){
	montaOptionsAjax($('ID_FUNCTION'),'<?php echo site_url('json/admin/getFunctionsByController'); ?>','id=' + this.value,'ID_FUNCTION','DESC_FUNCTION');
});
</script>
<?php $this->load->view("ROOT/layout/footer") ?>
