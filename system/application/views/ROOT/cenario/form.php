<?php $this->load->view("ROOT/layout/header"); ?>

<h1>Cadastro de Cenário</h1>

<?php $idCenario = sprintf('%d', $this->uri->segment(3)); ?>

<div id="contentGlobal">
	<form id="form_cenario" method="post" action="<?php echo site_url('cenario/salvar/'.$this->uri->segment(3)); ?>" name="form_ficha">
	<div id="tabs">
		<ul>
			<li><a href="#contentFormCenario">Dados B&aacute;sicos</a></li>
			<li><a href="#contentObjetos">Objeto</a></li>
			<li><a href="#contentFichas">Fichas</a></li>
		</ul>
	
		<div id="contentFormCenario">
	
			<?php if ( isset($erros) && is_array($erros) ) : ?>
				<ul style="color:#ff0000;font-weight:bold;" class="box_erros">
					<?php foreach ( $erros as $e ) : ?>
						<li><?=$e?></li>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>
		
			<h1>Dados b&aacute;sicos para Cadastro de Cenários</h1>

			<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tableFormCenario">
				<tr>
					<td width="20%">Nome da cenário<span class="obrigatorio"> *</span></td>
					<td width="80%">
		            	<? if( isset($_POST['NOME_CENARIO']) && !empty($_POST['NOME_CENARIO']) && !empty($_POST['ID_CENARIO']) ): ?>
		            		<?php post('NOME_CENARIO', false) ?>
		            	<? else : ?>
							<input type="text" name="NOME_CENARIO" id="NOME_CENARIO" size="70" value="<?php post('NOME_CENARIO', false) ?>" />
		            	<? endif; ?>
					</td>
				</tr>
				<? if( isset($_POST['COD_CENARIO']) && !empty($_POST['COD_CENARIO']) && !empty($_POST['ID_CENARIO']) ): ?>
					<tr>
			           	<td>
			           		Código do cenário<span class="obrigatorio"> *</span>
			           	</td>
			           	<td>
			           		<?php echo $_POST['COD_CENARIO']; ?>
						</td>
					</tr>
				<? endif; ?>
				<tr>
					<td>
						<?php ghDigitalReplace($_sessao, 'Cliente'); ?><span class="obrigatorio"> *</span>
					</td>
					<td>
		            	<? if( isset($_POST['DESC_CLIENTE']) && !empty($_POST['DESC_CLIENTE']) && !empty($_POST['ID_CENARIO']) ): ?>
		            		<?php post('DESC_CLIENTE', false) ?>
		            	<? else : ?>
							<?php if( !empty($_sessao['ID_CLIENTE']) ): ?>
			          			<?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?>
			          		<?php else: ?>
				          		<select name="ID_CLIENTE" id="ID_CLIENTE" >
				          			<option value=""></option>
				          			<?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', POST('ID_CLIENTE',TRUE)); ?>
			          			</select>
			          		<?php endif; ?>
		            	<? endif; ?>
					</td>
				</tr>
				<? if( isset($_POST['DT_CRIACAO_CENARIO']) && !empty($_POST['DT_CRIACAO_CENARIO']) && !empty($_POST['ID_CENARIO']) ): ?>
					<tr>
						<td>
							Data de criação
						</td>
						<td>
		            		<?php post('DT_CRIACAO_CENARIO', false) ?>
						</td>
					</tr>
				<? endif; ?>
				<tr>
					<td>
						Data de inicio
					</td>
					<td>
						<input type="text" class="_calendar" name="DT_INICIO_CENARIO" id="DT_INICIO_CENARIO" value="<?= post('DT_INICIO_CENARIO'); ?>" size="10" maxlength="10" readaonly="readonly" />
					</td>
				</tr>
				<tr>
					<td>
						Data de validade
					</td>
					<td>
						<input type="text" class="_calendar" name="DT_VALIDA_CENARIO" id="DT_VALIDA_CENARIO" value="<?= post('DT_VALIDA_CENARIO'); ?>" size="10" maxlength="10" readaonly="readonly" />
					</td>
				</tr>
				<tr>
					<td>Observações</td>
					<td><textarea name="OBS_CENARIO" cols="40" rows="4" id="OBS_CENARIO"><?= post('OBS_CENARIO',true); ?></textarea></td>
				</tr>
				<tr>
					<td>Status</td>
					<td>
						<input name="STATUS_CENARIO" type="radio" id="STATUS_CENARIO" value="1" <?php checked('STATUS_CENARIO',1); ?>/>Ativo
						<input name="STATUS_CENARIO" type="radio" id="STATUS_CENARIO" value="0" <?php checked('STATUS_CENARIO','0'); ?>/>Inativo
					</td>
				</tr>
			</table>
		</div>
		<!-- Final de form cenario -->
		
		<div id="contentObjetos">
			<h1 class="tit_lista">Lista de objeto</h1>
		
			<div class="alignAdicionaObjeto">
				<a class="button" id="btnFormPesquisa" href="javascript:new Util().vazio()" title="Adicionar objeto"><span>Adicionar Objeto</span></a>
			</div> 
			<!-- Final de Alinhamento Botão Adicionar Objeto -->
			
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="ListaSelecionados">
				<thead>
					<tr>
						<th>Produto</th>
						<th>Nome do arquivo</th>
						<th>Marca</th>
						<th>Data Cria&ccedil;&atilde;o</th>
						<th>Tipo</th>
						<th><?php ghDigitalReplace($_sessao, 'Categoria'); ?></th>
						<th><?php ghDigitalReplace($_sessao, 'Sub-Categoria'); ?></th>
						<th>Keywords</th>
						<th>Excluir</th>
					</tr>
				</thead>

				<tbody id="lista_objetos">
				<?php
					if (!empty($_POST['objetos'])) {
						$objetos = &$_POST['objetos'];

						foreach($objetos as $item) {
							$cont = $item['FILE_ID'];
							echo '' .
							'<tr class="alter" id="objeto_padrao">' .
								'<td align="center">' .
									'<a href="'.base_url().'img.php?img_id='.$item['FILE_ID'].'&rand='.rand().'&img_size=big&a.jpg" class="jqzoom">'.
										'<img src="'.base_url().'img.php?img_id='.$item['FILE_ID'].'&rand='.(rand()).'&a.jpg" width="50" border="0" class="imgThumb">'.
									'</a>'.
									'<input class="inputIdObjeto" type="hidden" name="ID_OBJETO" value="' . val($item, 'ID_OBJETO') . '" />';
								
								echo '</td>' .
								'<td>' . val($item,'FILENAME') . '</td>' .
								'<td>' . val($item, 'DESC_MARCA') . '</td>' .
								'<td>' . date('d/m/Y H:i:s', strtotime(val($item, 'DATA_UPLOAD'))) . '</td>' .
								'<td>' . val($item, 'DESC_TIPO_OBJETO') . '</td>' .
								'<td>' . val($item, 'DESC_CATEGORIA'). '</td>' .
								'<td>' . val($item, 'DESC_SUBCATEGORIA') . '</td>' .
								'<td>' . val($item, 'KEYWORDS') . '</td>';
										
								echo '<td><a href="#" onclick="Cenario.removerObjeto(this); return false;"><img src="' . base_url().THEME.'img/trash.png" border="0" /></a></td>';
				
							echo '</tr>';
							$cont++;
						}
					}
					?>
					<!-- inicio modelo -->
					<tr id="linha_modelo">
						<td align="center">
							<a href="" class="jqzoom">
								<img class="imgThumb" height="60" border="0" id="id0" name="id0" src=""/>
							</a>
							<input class="inputIdObjeto" type="hidden" value="" name="ID_OBJETO"/>
						</td>
						<td class="file"></td>
						<td class="codigo" style="display:none"></td>
						<td class="marca"></td>
						<td class="modified_date"></td>
						<td class="tipo"></td>
						<td class="categoria"></td>
						<td class="subcategoria"></td>
						<td class="keywords"></td>
						<td>
							<a onclick="Cenario.removerObjeto(this);" href="#">
								<img border="0" src="<?php echo base_url().THEME; ?>img/trash.png"/>
							</a>
						</td>
					</tr>
					<!-- fim modelo -->
				</tbody>
				
			</table>
		</div>
		<!-- final de objeto -->
		
		<div id="contentFichas">
			<div style="float:right"> <a class="button" href="javascript:" onclick="pesquisar();"><span>Pesquisar Fichas</span></a> </div>
			<div class="clearBoth">&nbsp;</div>
			
			<table id="tabela_fichas" width="100%" border="0" cellpadding="0" cellspacing="0" class="ListaSelecionados">
				<thead>
					<tr>
						<th>Produto</th>
						<th>Campos</th>
						<th>Código de barras</th>
						<th>Nome do produto</th>
						<th><?php ghDigitalReplace($_sessao, 'Categoria'); ?></th>
						<th><?php ghDigitalReplace($_sessao, 'Subcategoria'); ?></th>
						<th>Data de criação</th>
						<th>Excluir</th>
					</tr>
				</thead>

				<tbody id="lista_fichas">
<?php
					if (!empty($fichas)) {
						foreach($fichas as $item) {
?>
							<tr>
								<td>
									<a href="<?= base_url() ?>img.php?img_id=<?php echo $item['FILE_ID']; ?>&rand=<?= rand()?>&img_size=big&a.jpg" class="jqzoom"> 
										<img src="<?= base_url() ?>img.php?img_id=<?php echo $item['FILE_ID']; ?>&rand=<?= rand()?>&a.jpg" title="<?php echo $item['NOME_FICHA']; ?>" width="50" border="0"> 
									</a>
								</td>
								<td>
									<?php if( count( $item['CAMPOS'] ) > 0 ):?>
										<a href="javascript:new Util().vazio();" title="Exibir Campos" onclick="$j('.campos<?php echo $item['ID_FICHA']; ?>').toggle();toggleImgFilha($j(this).closest('td'));">
											<img src="<?= base_url().THEME ?>img/open_filhas.png"  border="0"/>
										</a>
									<?php endif; ?>
								</td>
								<td>
									<input type="hidden" name="FICHAS[]" value="<?php echo $item['ID_FICHA']; ?>">
									<?php echo $item['COD_BARRAS_FICHA']; ?>
								</td>
								<td>
									<?php echo $item['NOME_FICHA']; ?>
								</td>
								<td>
									<?php echo $item['DESC_CATEGORIA']; ?>
								</td>
								<td>
									<?php echo $item['DESC_SUBCATEGORIA']; ?>
								</td>
								<td>
									<?php echo format_date_to_form($item['DT_INSERT_FICHA'], 'd/m/Y H:i'); ?>
								</td>
								<td>
									<a onclick="$j(this).closest('tr').remove();$j('.campos<?php echo $item['ID_FICHA']; ?>').remove();" href="#">
										<img border="0" src="<?php echo base_url().THEME; ?>img/trash.png"/>
									</a>
								</td>
							</tr>
<?php
							if ( count( $item['CAMPOS'] ) > 0 ){
?>
								<tr style="background-color:#EDECEC;" class="cc campos<?php echo $item['ID_FICHA']; ?>">
									<td colspan="3">
										Campo
									</td>
									<td colspan="5">
										Conteúdo
									</td>
								</tr>
<?php
							}
							
							foreach ( $item['CAMPOS'] as $key => $value ){
?>
								<tr class="cc campos<?php echo $item['ID_FICHA']; ?>">
									<td colspan="3">
										<?php echo $key; ?>
									</td>
									<td colspan="5">
										<?php echo $value; ?>
									</td>
								</tr>
<?php
							}
						}
					}
?>
					<tr id="linha_modelo_ficha">
						<td>
							<a href="<?= base_url() ?>img.php?img_id={FILE_ID}&rand=<?= rand()?>&img_size=big&a.jpg" class="jqzoom"> 
								<img src="<?= base_url() ?>img.php?img_id={FILE_ID}&rand=<?= rand()?>&a.jpg" title="{NOME_FICHA}" width="50" border="0"> 
							</a>
						</td>
						<td>
							<a class="abrecampos" href="javascript:new Util().vazio();" title="Exibir Campos" onclick="$j('.campos{ID_FICHA}').toggle();toggleImgFilha($j(this).closest('td'));">
								<img src="<?= base_url().THEME ?>img/open_filhas.png"  border="0"/>
							</a>
						</td>
						<td>
							<input type="hidden" name="FICHAS[]" value="{ID_FICHA}">
							{COD_BARRAS_FICHA}
						</td>
						<td>{NOME_FICHA}</td>
						<td>{DESC_CATEGORIA}</td>
						<td>{DESC_SUBCATEGORIA}</td>
						<td>{DT_INSERT_FICHA}</td>
						<td>
							<a onclick="$j(this).closest('tr').remove();" href="#">
								<img border="0" src="<?php echo base_url().THEME; ?>img/trash.png"/>
							</a>
						</td>
					</tr>
						
				</tbody>
			</table>

		</div>
		
	</div>
	<!-- final tabs -->
	
	<div id="contentBotoes">
			<?php
			if( !empty($fichasComboAssociadas) && post('TIPO_FICHA',true) == 'PRODUTO' ){
				echo '<p>
					<img src="', base_url(), THEME, 'img/alerta-amarelo.jpg" />
					Esta ficha possui fichas-combo associadas, portanto você não pode alterar a aprovação.
					</p><p>&nbsp;</p>';
			}
			?>
	
			<?php if(Sessao::hasPermission('ficha','modal_aprovacao')  && post('ID_FICHA',true) != '' && empty($fichasComboAssociadas)  ): ?>
				<a class="button" href="javascript:" onclick="displayMessage('<?=site_url('ficha/modal_aprovacao/'. $this->uri->segment(3))?>');" ><span>Aprovação</span></a>
			<?php endif;?>
	
			<? if(!empty($podeSalvar)):?>
				<a class="button" href="javascript:new Util().vazio()" onclick="$j('#form_cenario').submit()"><span>Salvar</span></a>
	        <? endif; ?>
	
	        <? if(!empty($podeEmail)):?>
	            <a class="button" href="javascript:new Util().vazio()" onclick="$j('#form_ficha').attr('action','<?= base_url().index_page() ?>/ficha/salvarComEmail/<?= $this->uri->segment(3) ?>').submit()"><span>Salvar com envio de email</span></a>
	        <? endif; ?>
	
			<a class="button" href="javascript:new Util().vazio()" onClick="location.href='<?= base_url().index_page() ?>/cenario/listar/';" alt="Cancelar" title="Cancelar" ><span>Cancelar</span></a>
	</div> 
	<!-- Final de botões -->
	</form>
</div>
<!-- final contentGlobal -->

<script type="text/javascript">
	$j('._calendar').datepicker({dateFormat: 'dd/mm/yy',showOn: 'button', buttonImage: '<?= base_url().THEME ?>img/calendario.png', buttonImageOnly: true});

	// pesquisa objetos
	$j("#btnFormPesquisa").click(function(e) {
		displayMessagewithparameter('<?php echo base_url().index_page().'/cenario/form_pesquisa/'; ?>', 800, 600, function(){
			$j('#PERIODO_INICIAL,#PERIODO_FINAL').datepicker({dateFormat: 'dd/mm/yy',showOn: 'button', buttonImage: '<?= base_url().THEME ?>img/calendario.png', buttonImageOnly: true});
		});
	});

	// pesquisa fichas
	function pesquisar(){
		displayMessagewithparameter('<?php echo base_url().index_page().'/cenario/form_pesquisa_fichas/'; ?>', 800, 600, function(){
			$j('#PERIODO_INICIAL,#PERIODO_FINAL').datepicker({dateFormat: 'dd/mm/yy',showOn: 'button', buttonImage: '<?= base_url().THEME ?>img/calendario.png', buttonImageOnly: true});
		});
	}

	// pesquisa fichas
	function doPesquisa(pg){
		$j('#resultado').html('Pesquisando...');
		$j.post('<?php echo site_url('json/cenario/search_fichas'); ?>/' + pg,
			   
			$j('#formPesquisa').serialize(),
			
			function(html){
				$j('#resultado').html( html );
				$j('.jqzoom').fancybox();
			}
		);
	}

	// adiciona fichas selecionadas
	function addLinhasSelecionadas(){
		$j('.selecao').each(function(){
			if( this.checked ){
				var obj = eval('[' + $j(this).parent().find('textarea').val() + ']')[0];
				new Ficha(obj).create();
			}
		});
		
		closeMessage();
	}

	// Ficha
	function Ficha(item){
		this.item = item;
	};

	Ficha.prototype = {
		create: function(){
			// pega o html da linha
			var novoModelo = Ficha.modelo.clone();
			var idFicha = this.item['ID_FICHA'];
			novoModelo.attr('id', 'ficha' + this.item['ID_FICHA']);
			var linha = $j('<div></div>').append(novoModelo).html().replace(/%7B/g,'{').replace(/%7D/g,'}');

			// pega os place holders
			var reg = linha.match(/\{(\w+)\}/g);

			// para cada resultado encontrado
			for(var i in reg){
				// se for um indice numerico
				if( !isNaN(i) ){
					// pega a chave
					var key = reg[i].replace('{','').replace('}','');
					// substitui pelos valores do objeto
					linha = linha.replace(reg[i], this.item[ key ]);
				}
			}

			// transforma os valores substituidos em elemento jquery
			var el = $j(linha);
			
			// coloca a linha na tabela
			$j('#tabela_fichas').append( el );

			// campos da ficha
			var booCabecalho = false;
			var booTemCampos = false;

			$j.each(this.item.CAMPOS, function(key, value){
				if ( booCabecalho == false ) {
					var campos = $j("<tr style='background-color:#EDECEC;display:none;' class='cc campos" + idFicha + "'><td colspan='3'>Campo</td><td colspan='5'>Conteúdo</td></tr>");
					$j('#tabela_fichas').append( campos );					
					booCabecalho = true;
				}
				var campos = $j("<tr style='display:none;' class='cc campos" + idFicha + "'><td colspan='3'>" + key + "</td><td colspan='5'>" + value + "</td></tr>");
				$j('#tabela_fichas').append( campos );
				booTemCampos = true;
			});

			if( booTemCampos == false ){
				el.find('.abrecampos').hide();
			}
			
			// fancybox
			$j('.fancybox').fancybox();			
		}
	}
	
	$j(function(){
		$j('#tabs').tabs();

		$j(".jqzoom").fancybox();

		$j(".cc").hide();

		Cenario.modelo = $('linha_modelo');
		$('linha_modelo').remove();

		Ficha.modelo = $j('#linha_modelo_ficha');
		$('linha_modelo_ficha').remove();
		
	});
</script>

<?php $this->load->view("ROOT/layout/footer") ?>