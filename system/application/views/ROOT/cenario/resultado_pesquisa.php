<?php if(!empty($objetos)): ?>

<div id="contentGlobal">

<div id="tableObjeto">

<table width="99%" border="0" cellpadding="0" cellspacing="0" class="tableSelection" style="margin-left: 2px;">			
<thead>
  	<tr>
        <td>Selecionar</td>
        <td>Imagem</td>
        <td>Nome do arquivo</td>
        <td>Marca</td>
        <td><?php ghDigitalReplace($_sessao, 'Subcategoria'); ?></td>		
    </tr>
</thead>
  <?php
  $idx = 0;
  
  foreach($objetos as $item): ?>
  <tr>
    <td><input type="checkbox" class="selecaoObjeto" name="lista" id="lista" value='<?php echo json_encode($item) ?>' onClick="checaApenasUmObjeto(this);" /></td>
    
    <td>
        <a href="<?= base_url() ?>img.php?img_id=<?= $item['FILE_ID'] ?>&rand=<?= rand()?>&img_size=big&a.jpg" class="jqzoom">
        	<img src="<?= base_url() ?>img.php?img_id=<?= $item['FILE_ID'] ?>&rand=<?= rand()?>&a.jpg" width="50" border="0">
        </a>
    </td>
	<td><?php echo val($item,'FILENAME'); ?></td>
    <td><?php echo val($item,'DESC_MARCA'); ?></td>
    <td><?php echo val($item,'DESC_SUBCATEGORIA'); ?></td>
  </tr>
  <?php
  $idx++;
  endforeach; 
  ?>
  <tr>
    <td colspan="6">
      <a class="button" href="javascript:;" onclick="Cenario.adicionarNovosObjetos()"><span>Confirmar</span></a>
    </td>
  </tr>
  <tr>
  <td colspan="6">Objetos encontrados: <strong><?php echo $total; ?></strong><br /><br />
	<?php 
	
	$pagina = $pagina < 0 ? 0 : $pagina;
	
	$numLinks = 5;
	$inicial = ($pagina/$limit) - $numLinks;
	if( $inicial < 1 ){
		$inicial = 1;
	}
	$final = ($pagina/$limit) + $numLinks;
	if( $final > $totalPagina ){
		$final = $totalPagina;
	}
	
	$list = array();
    for($i=$inicial; $i<=$final; $i++){
		if( $pagina == ($i-1)*$limit){
			$list[] = '<strong class="linkAtivoPaginacao">'.$i.'</strong>';
		} else {
			$list[] = sprintf('<a href="javascript:void(0)" onclick="Cenario.pesquisaPorPaginacao(%d)"> %s </a>',
				($i-1)*$limit,
				$i
			);
		}
    }
	
	if( $pagina > 0 ){
		echo '<a href="javascript:void(0)" onclick="Cenario.pesquisaPorPaginacao(0)">Primeira</a> ';
		echo '<a class="anterior" href="javascript:void(0)" onclick="Cenario.pesquisaPorPaginacao('.($pagina-$limit).')">Anterior</a> ';
	}
	
	echo ' | ' . implode(' | ', $list) . ' | ';
	
	if( $pagina/$limit < $totalPagina-1 ){
		echo '<a class="proximo" href="javascript:void(0)" onclick="Cenario.pesquisaPorPaginacao('.($pagina == 0 ? 5 : $pagina+$limit).')">Próxima</a> ';
		echo '<a href="javascript:void(0)" onclick="Cenario.pesquisaPorPaginacao('.(($totalPagina-1)*$limit).')">Última</a> ';
	}
    ?>  </td>
  </tr>
</table>
</div> <!-- Final de Table Objeto -->

<?php else: ?>
	Nenhum resultado encontrado
<?php endif; ?>

</div> <!-- Final de content Global -->

