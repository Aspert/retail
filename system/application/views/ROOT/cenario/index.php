<?php $this->load->view("ROOT/layout/header") ?>

<h1>Cenários</h1>
<div id="contentGlobal">
<div id="pesquisa_simples" <?=(isset($busca['tipo_pesquisa'])&& $busca['tipo_pesquisa'] == 'completa') ? 'style="display:none"':''?>> 
<?php echo form_open('cenario/listar');?>
	<?=form_hidden('tipo_pesquisa','simples');?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tablePesquisa">
		<tr>
			<td width="92"><?php if ( $podeEditar ) : ?>
				<?=anchor('cenario/form','<img src="'.base_url().THEME.'img/file_add.png" border="0" />',array('title'=>'Novo Cenário'))?>
				<?php endif; ?></td>
			<td width="24"></td>
			<td width="140" nowrap="nowrap">
			<div style="width: 500px;">
				C&oacute;digo do Cenário:
				<?= form_input('COD_CENARIO',(isset($busca['COD_CENARIO'])) ? $busca['COD_CENARIO'] : '');?>
				<?$pagina_atual = (isset($busca["pagina"])) ? $busca["pagina"] : 0;?>
			</div>
			</td>
			<td width="4"></td>
			<td width="4"></td>
			<td width="4"></td>
			<td width="261"> Itens/P&aacute;gina:
				<select name="pagina" id="pagina" >
					<?php
						$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
						for($i=10; $i<=100; $i+=10){
							printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
						}
					?>
				</select></td>
			<td width="4"></td>
			<td width="244"></td>
			<td width="4"></td>
			<td width="53"><a class="button" title="Pesquisa" onclick="$j(this).closest('form').submit()"><span>Pesquisa</span></a></td>
			<td width="4"></td>
			<td width="254"><a class="button" title="Pesquisa Avan&ccedil;ada" onclick="new Util().pesquisaCompleta();$j.fn.sortgrid.options.form = '#pesquisa_completo form';"><span>Pesquisa Avan&ccedil;ada</span></a></td>
		</tr>
	</table>
</div>
<?php echo form_close();?>


<div id="pesquisa_completo" style="display:<?=(isset($busca['tipo_pesquisa'])&& $busca['tipo_pesquisa'] == 'completa') ? '':'none'?>"> 
<?php echo form_open('cenario/listar', array('id' => 'form_pesquisa_avancada'));?>
	<?=form_hidden('tipo_pesquisa','completa');?>
	<h1 class="align_right">Consulta de Cenários</h1>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="contentPcomplete">
		<tr>
			<td width="250">C&oacute;digo do cenário:</td>
			<td width="20">&nbsp;</td>
			<td><?= form_input('COD_CENARIO',(isset($busca['COD_CENARIO'])) ? $busca['COD_CENARIO'] : '');?></td>
		</tr>
		<tr>
			<td valign="top" style="vertical-align:top">C&oacute;digo de barras das fichas do cenário:</td>
			<td>&nbsp;</td>
			<td>
				<div id="containerCodigos">
					<div id="modeloCodigo" class="campoCodigoBarras">
						<input id="modeloCodigo" name="CODIGO_BARRAS_FICHA[]" type="text" size="40" />
						<a href="#" onclick="$j(this).closest('div').remove(); return false;"> <img src="<?php echo base_url(), THEME, 'img/alerta-vermelho.jpg'; ?>" align="absbottom" title="Remover Codigo" /> </a>
					</div>
				</div>
				<p><a href="#" onclick="adicionarCampoCodigo(''); return false;" class="button"><span>Adicionar Código</span></a></p>
			</td>
		</tr>
		<tr>
			<td colspan="3"></td>
		</tr>
		<tr>
			<td>Cliente:</td>
			<td>&nbsp;</td>
			<td>
				<?php if(!empty($_sessao['ID_CLIENTE'])): ?>
					<?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?>
					<br />
					<br />
				<?php else: ?>
					<select name="ID_CLIENTE" id="ID_CLIENTE">
						<?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', !empty($busca['ID_CLIENTE']) ? $busca['ID_CLIENTE'] : ''); ?>
					</select>
					<br />
				<?php endif; ?>
			</td>
		</tr>
		<tr>
			<td>Data de criação:</td>
			<td>&nbsp;</td>
			<td>
				<input type="text" name="DT_CRIACAO_CENARIO" id="DT_CRIACAO_CENARIO" size="10" maxlength="10" class="_calendar" value="<?php post('DT_CRIACAO_CENARIO'); ?>" readonly="readonly" />
			</td>
		</tr>
		<tr>
			<td>Status:</td>
			<td>&nbsp;</td>
			<td><?=form_radio(array('name'=>'STATUS_CENARIO','value'=> '1','checked'=> (isset($busca['STATUS_CENARIO']) && $busca['STATUS_CENARIO']==1) ? TRUE: FALSE ));?>
				Ativo
				<?=form_radio(array('name'=>'STATUS_CENARIO','value'=> '0','checked'=> (isset($busca['STATUS_CENARIO']) && $busca['STATUS_CENARIO']==0) ? TRUE: FALSE ));?>
				Inativo<br/>
			</td>		
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>
				<a class="button" href="javascript:" id="btnLimpaPesquisa" onclick="limpaForm('#form_pesquisa_avancada');"><span>Limpar Campos</span></a> 
				<a class="button" href="javascript:" onclick="$j(this).closest('form').submit();"><span>Pesquisar</span></a>
				<a class="button" href="javascript:" onClick="new Util().pesquisaSimples();$j.fn.sortgrid.options.form = '#pesquisa_simples form';"><span>Cancelar Pesquisa</span></a>
			</td>
		</tr>
	</table>
	<?php echo form_close();?> 
</div>
<?php if( (!empty($campos))&& (is_array($campos))):?>

<p>&nbsp;</p>

<div id="tableObjeto">
<?php 
	// Numero de colunas da tabela depende das permissoes, utilizado nos colspan
	$cols = 4 + ($podeEditar?1:0);
?>
	<table id="tabelaBusca" border="0" cellspacing="0" cellpadding="0" width="100%" class="tableSelection">
		<thead>
			<tr>
				<?php if($podeEditar): ?>
				<th width="5%">Editar</th>
				<?php endif; ?>
				<th width="30%" id="colNome">Nome do cenário</th>
				<th width="25%" id="colCodigo">Código do cenário</th>
				<th width="25%" id="colDataCriacao">Data de criação</th>
				<th width="20%" id="colStatus">Status</th>				
			</tr>
		</thead>
		<?php foreach($campos as $cenario):?>
		<tr>
			<?php if ($podeEditar): ?>
				<td>
					<?php echo anchor('cenario/form/'. $cenario['ID_CENARIO'], '<img src="'. base_url().THEME.'img/file_edit.png" />',array('title'=>'Editar Ficha'));?>
				</td>
			<?php endif; ?>
			<td><?=$cenario['NOME_CENARIO']?></td>
			<td><?=$cenario['COD_CENARIO']?></td>
			<td><?=format_date_to_form($cenario['DT_CRIACAO_CENARIO'], 'd-m-Y H:i:s');?></td>
			<td><? if( $cenario['STATUS_CENARIO'] == '1' ){ echo 'Ativo'; }else{ echo 'Inativo'; } ?></td>
		</tr>
		<?endforeach;?>
		<tr>
			<td colspan="<?= $cols; ?>" class="obj_encontrado">
			Cenários encontrados: <strong><?= $total ?></strong>
			</td>
		</tr>
	</table>
</div>

<span class="paginacao">
<?=(isset($paginacao))?$paginacao:null?>
</span>
<?php else: ?>
Não há resultados para esta pesquisa
<?php endif; ?>

<script type="text/javascript">
	$j('._calendar').datepicker({dateFormat: 'dd/mm/yy',showOn: 'button', buttonImage: '<?= base_url().THEME ?>img/calendario.png', buttonImageOnly: true});

	function adicionarCampoCodigo(valor){
		var ipt = modelo.clone();
		ipt.find('input').val(valor);
		ipt.appendTo('#containerCodigos');
	}

	modelo = $j('#modeloCodigo').clone();
	modelo.attr('id','');

	$j('#modeloCodigo').remove();

	$j(function(){
		var options = 
				{
				columns:[{col:'#colNome',label:'NOME_CENARIO'},
						{col:'#colCodigo',label:'COD_CENARIO'},
						{col:'#colDataCriacao',label:'DT_CRIACAO_CENARIO'},
						{col:'#colStatus',label:'STATUS_CENARIO'},
						],
				form:'#pesquisa_<?= (isset($busca['tipo_pesquisa']) && $busca['tipo_pesquisa'] != 'simples' ? 'completo' : 'simples' ) ?> form',
				orderDirection: '<?= $busca['ORDER_DIRECTION'] ?>',
				selectedField:'<?= $busca['ORDER'] ?>'
				}
			
		$j('#tabelaBusca').sortgrid(options);
	});
	
<?php
	if(!empty($busca['CODIGO_BARRAS_FICHA'])){
		foreach($busca['CODIGO_BARRAS_FICHA'] as $codigo){
			if(!empty($codigo)){
				echo 'adicionarCampoCodigo("', addslashes($codigo), '");'.PHP_EOL;
			}
		}
	}
?>

$j(function(){
	$j('.ui-datepicker').hide();
});
	
	adicionarCampoCodigo('');	
</script>

<br/>
<br/>
<br/>
<?php $this->load->view("ROOT/layout/footer") ?>
