<html>
<head>
<style type="text/css">
table{
	font-family:verdana;
	font-size:11px;
	padding: 0px;
	spacing:0px;
}

h1{
	font-family:verdana;
	font-size:20px;
	text-align:center;
}
</style>
</head>
<body>

<h1><?php echo $job['ID_JOB'] . ' - ' . $job['TITULO_JOB']; ?></h1>


<?php foreach($itens as $ficha): ?>
	<table width="100%" cellspacing="3" style="border:1px solid gray;background-color:#f8f8f8;">
		<tr>
			<td>
				<strong><?php echo $ficha['NOME_FICHA']; ?> (<?php echo $ficha['COD_BARRAS_FICHA']; ?>)</strong>
			</td>
		</tr>
		<tr>
			<td>
				Página: <strong><?php echo $ficha['PAGINA_ITEM']; ?></strong>
				&nbsp;&nbsp;&nbsp;
				Ordem: <strong><?php echo $ficha['ORDEM_ITEM']; ?></strong>
			</td>
		</tr>
		
		<tr heigth="12px">
			<td></td>
		</tr>
		
		<?php if(count($ficha['CAMPOS'])): ?>
			<tr style="background-color:#e8e8e8;">
				<td style="border-bottom:1px solid #c9c9c9;">
					<strong>Campos:</strong>
				</td>
			</tr>
			<tr>
				<td>
				
					<table width="100%" cellspacing="0">
						<?php foreach($ficha['CAMPOS'] as $campo): ?>
							<tr>
								<td width="20%" style="border-bottom:1px solid #c9c9c9;">
									<?php echo $campo['LABEL_CAMPO_FICHA'] ;?>:
								</td>
								<td width="80%" style="border-bottom:1px solid #c9c9c9;"><?php echo $campo['VALOR_CAMPO_FICHA'] ;?></td>
							</tr>
						<?php endforeach; ?>
					</table>		
								
				</td>
			</tr>
		<?php endif; ?>
		
		<?php if(count($ficha['SUBFICHAS'])): ?>
			<tr>
				<td>&nbsp;</td>
			</tr>
			
			<?php foreach($ficha['SUBFICHAS'] as $subficha): ?>
				<tr style="background-color:#e8e8e8;">
					<td style="border-bottom:1px solid #c9c9c9;">
						<strong>Subficha (<?php echo $subficha['COD_BARRAS_FICHA']; ?>)</strong>
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellspacing="0">
							<?php foreach($subficha['CAMPOS'] as $campo): ?>
								<tr>
									<td width="20%" style="border-bottom:1px solid #c9c9c9;"><?php echo $campo['LABEL_CAMPO_FICHA'] ;?>:</td>
									<td width="80%" style="border-bottom:1px solid #c9c9c9;"><?php echo $campo['VALOR_CAMPO_FICHA'] ;?></td>
								</tr>
							<?php endforeach; ?>
						</table>		
								
					</td>
				</tr>
			<?php endforeach; ?>
		<?php endif; ?>
	</table>
	
	<br>
<?php endforeach; ?>

</body>

</html>