<?php $this->load->view("ROOT/layout/header"); ?>

<h1><?php ghDigitalReplace($_sessao, 'Fichas'); ?></h1>
<?php
$idficha = sprintf('%d', $this->uri->segment(3));
?>

<div id="carregando">Carregando...</div>

<div id="contentGlobal">
<form id="form_ficha" method="post" action="<?php echo site_url('ficha/salvar/'.$this->uri->segment(3)); ?>" name="form_ficha">
<input type="hidden" name="COD_CIP_FICHA" id="COD_CIP_FICHA">
<div id="tabs">
	<ul>
		<li><a href="#contentFormFicha" id="contDadosBasicos">Dados B&aacute;sicos</a></li>
		<li><a href="#contentCampos" id="contCampos">Campos</a></li>
		<li><a href="#contentObjetos" id="contObjetos" ><?php ghDigitalReplace($_sessao, 'Objetos'); ?></a></li>
		<!-- <li><a href="#contentFichasPai" class="TIPO_PRODUTO triggers">Fichas Pai</a></li> -->
		<!-- <li><a href="#contentFichasFilha" class="TIPO_PRODUTO triggers">Fichas Filha</a></li> -->
		<li><a href="#contentCombo" id="contCombo" class="TIPO_PRODUTO triggers">Combo</a></li>
		<li><a href="#contentFichaCombo" id="contFichaCombo" class="TIPO_COMBO triggers"><?php ghDigitalReplace($_sessao, 'Fichas'); ?> (Combo)</a></li>
		<li><a href="#contentSubFichas" id="contSubfichas" class="TIPO_PRODUTO triggers"><?php ghDigitalReplace($_sessao, 'SubFichas'); ?></a></li>
	</ul>



	<div id="contentFormFicha">

	<?php if ( isset($erros) && is_array($erros) ) : ?>
		<ul style="color:#ff0000;font-weight:bold;" class="box_erros">
			<?php foreach ( $erros as $e ) : ?>
				<li><?=$e?></li>
			<?php endforeach; ?>
		</ul>
	<?php endif; ?>

			<h1>Dados b&aacute;sicos para Cadastro de <?php ghDigitalReplace($_sessao, 'Fichas'); ?></h1>

			<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tableFormFicha">
	          <tr>
	            <td width="192"><?php ghDigitalReplace($_sessao, 'Tipo de Ficha'); ?></td>
	            <td width="460">
					<?php
					if( $this->uri->segment(3) == '' ) {
						echo '<select name="TIPO_FICHA" id="TIPO_FICHA">
							<option value="PRODUTO"', selected('TIPO_FICHA','PRODUTO'), ' >Produto</option>
							<option value="COMBO"', selected('TIPO_FICHA','COMBO'), ' >Combo</option>
							<option value="SUBFICHA"', selected('TIPO_FICHA','SUBFICHA'), ' >';
						ghDigitalReplace($_sessao, 'SubFichas');
						echo'</option>
						</select>';
					} else {
						echo ghDigitalReplace($_sessao, ucfirst(strtolower(post('TIPO_FICHA',true)))),
							'<input type="hidden" name="TIPO_FICHA" id="TIPO_FICHA" value="', post('TIPO_FICHA',true), '" />';
					}
					?>
				</td>
	            <td width="424" rowspan="16" style="text-align: right; vertical-align:top" class="alvoPrincipal">
				<?php
				if(!empty($_POST['objetos'])){
					foreach($_POST['objetos'] as $item){
						if($item['PRINCIPAL'] == 1){
							echo '<img src="'.base_url().'img.php?img_id='.$item['FILE_ID'].'" />';
						}
					}
				}
				?>
                </td>
	          </tr>
	          <tr class="hidesubficha">
	            <td><?php ghDigitalReplace($_sessao, 'Nome da ficha'); ?><span class="obrigatorio" id='ASTERISCO_NOME_PRODUTO'>
	            <?php 
	            if(isset($_POST['DESC_CLIENTE'])){
	            	if(strtoupper($_POST['DESC_CLIENTE']) != 'HERMES'){ 
	            		echo '*';
	            	} 
	            }
	            else if(!$isClienteHermes){
	            	echo '*'; 
	            } 
	            ?>	            
	            </span></td>
	            <td><input name="NOME_FICHA" type="text" id="NOME_FICHA" size="100" value="<?= trim(htmlspecialchars(post('NOME_FICHA',true))); ?>" /></td>
              </tr>
			  <tbody id="codigos_regionais">
			  <?php foreach($regioes as $item): ?>
	          <tr class="hidesubficha">
	            <td>Código <?php echo $item['NOME_REGIAO']; ?></td>
	            <td><input name="<?php printf('codigos[%d]', $item['ID_REGIAO']); ?>" type="text" id="<?php printf('codigos_%d', $item['ID_REGIAO']); ?>" size="40" value="<?php echo ref($_POST, 'codigos', $item['ID_REGIAO']); ?>" /></td>
              </tr>
			  <?php endforeach; ?>
			  </tbody>
	          <tr>
	            <td><?php ghDigitalReplace($_sessao, 'Código de Barras'); ?><span class="obrigatorio"> *</span></td>
	            <td>
					<div class="codigo_barras_produto">
						<input name="COD_BARRAS_FICHA" type="text" id="COD_BARRAS_FICHA" size="40" value="<?php post('COD_BARRAS_FICHA'); ?>" />&nbsp;&nbsp;
						<?php if($podeImportar){ ?>
							<?php if($isClienteCompraFacil){ ?>
								<img class="hidesubficha" border="0" src="<?php echo base_url().THEME; ?>img/magnify.png" style="cursor:pointer;" onclick="importaCompraFacil();"/>
							<?php } ?>
							<?php if($isClienteHermes){ ?>
								<img class="hidesubficha" border="0" src="<?php echo base_url().THEME; ?>img/magnify.png" style="cursor:pointer;" onclick="importaHermes();"/>
							<?php } ?>
						<?php } ?>
					</div>
					<div class="codigo_barras_combo">
						<?php echo post('COD_BARRAS_FICHA',true) == '' ? 'NOVO' : post('COD_BARRAS_FICHA',true); ?>
						<input name="COD_BARRAS_FICHA_COMBO" type="hidden" value="<?php post('COD_BARRAS_FICHA'); ?>" />
					</div>
              </tr>
	          <tr>
	          	<td><?php ghDigitalReplace($_sessao, 'Cliente'); ?><span class="obrigatorio">*</span></td>
	          	<td>
	          		<?php if(!empty($_sessao['ID_CLIENTE'])): ?>
	          		<?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?>
	          		<?php else: ?>
	          		<select name="ID_CLIENTE" id="ID_CLIENTE" >
	          			<option value=""></option>
	          			<?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', POST('ID_CLIENTE',TRUE)); ?>
          			</select>
	          		<?php endif; ?>
	          	</td>
          	</tr>
	          <tr <?php if($isClienteCompraFacil==1){echo"style='display:none;'";}?>>
	          	<td><?php ghDigitalReplace($_sessao, 'Categoria'); ?><span class="obrigatorio">*</span></td>
	          	<td><select name="ID_CATEGORIA" id="ID_CATEGORIA">
	          		<option value=""></option>
	          		<?php echo montaOptions($categorias,'ID_CATEGORIA','DESC_CATEGORIA',POST('ID_CATEGORIA',TRUE)); ?>
	          		</select></td>
          	</tr>
	          <tr>
	            <td><?php ghDigitalReplace($_sessao, 'SubCategoria'); ?><span class="obrigatorio">*</span></td>
	            <td><select name="ID_SUBCATEGORIA" id="ID_SUBCATEGORIA">
	              <option value=""></option>
	              <?php echo montaOptions($subcategorias,'ID_SUBCATEGORIA','DESC_SUBCATEGORIA',POST('ID_SUBCATEGORIA',TRUE)); ?>
	            </select></td>
              </tr>
	          <tr class="" <?php if($isHermes==1){ echo"style='display:none;'"; }else{ echo"class='hidesubficha'"; }?>>
	            <td>Fabricante</td>
	            <td><select name="ID_FABRICANTE" id="ID_FABRICANTE">
	              <option value=""></option>
	              <?php echo montaOptions($fabricantes,'ID_FABRICANTE','DESC_FABRICANTE',POST('ID_FABRICANTE',TRUE)); ?>
	            </select></td>
              </tr>
	          <tr class="hidesubficha">
	            <td>Marca</td>
	            <td>
                <select name="ID_MARCA" id="ID_MARCA">
	              <option value=""></option>
	              <?php echo montaOptions($marcas,'ID_MARCA','DESC_MARCA',POST('ID_MARCA',TRUE)); ?>
	            </select>
                </td>
              </tr>
	          <tr class="hidesubficha">
	            <td>Modelo</td>
	            <td><input name="MODELO_FICHA" type="text" id="MODELO_FICHA" size="40" value="<?= htmlspecialchars(post('MODELO_FICHA',true)); ?>" /></td>
              </tr>
	          <tr class="hidesubficha">
	          	<td>Data de inicio da ficha</td>
	          	<td><input class="data" name="DATA_INICIO" type="text" id="DATA_INICIO" value="<?= post('DATA_INICIO'); ?>" size="10" maxlength="10" /></td>
          	</tr>
	          <tr class="hidesubficha">
	          	<td>Data de validade da ficha</td>
	          	<td><input class="data" name="DATA_VALIDADE" type="text" id="DATA_VALIDADE" value="<?= post('DATA_VALIDADE'); ?>" size="10" maxlength="10" /></td>
          	</tr>
	          <tr class="hidesubficha">
	            <td>Observações</td>
	            <td><textarea name="OBS_FICHA" cols="40" rows="4" id="OBS_FICHA"><?= post('OBS_FICHA',true); ?></textarea></td>
              </tr>
	          <tr class="hidesubficha">
	          	<td>Pendências</td>
	          	<td><textarea name="PENDENCIAS_DADOS_BASICOS" cols="40" rows="4" id="PENDENCIAS_DADOS_BASICOS"><?= post('PENDENCIAS_DADOS_BASICOS',true); ?></textarea></td>
          	</tr>
	          <tr class="hidesubficha">
	            <td>Temporário</td>
	            <td><input name="FLAG_TEMPORARIO" type="radio" id="FLAG_TEMPORARIO" value="1" <?php checked('FLAG_TEMPORARIO',1); ?><?= disabled(!$podeTemporario)?> />
	            Sim
	              <input name="FLAG_TEMPORARIO" type="radio" id="FLAG_TEMPORARIO" value="0" <?php checked('FLAG_TEMPORARIO','0'); ?><?= disabled(!$podeTemporario)?> />
	            Não</td>
              </tr>
	          <tr>
	            <td>Status</td>
	            <td>
                  <input name="FLAG_ACTIVE_FICHA" type="radio" id="FLAG_ACTIVE_FICHA" value="1" <?php checked('FLAG_ACTIVE_FICHA',1); ?><?= disabled(!$podeStatus)?>/>Ativo
                  <input name="FLAG_ACTIVE_FICHA" type="radio" id="FLAG_ACTIVE_FICHA" value="0" <?php checked('FLAG_ACTIVE_FICHA','0'); ?><?= disabled(!$podeStatus)?>/>Inativo
	            </td>
              </tr>
	    	</table>
	    	<input name="ID_APROVACAO_FICHA" type="hidden" id="ID_APROVACAO_FICHA" value="<?= post('ID_APROVACAO_FICHA'); ?>" />
	</div> <!-- Final de form ficha -->

	<div id="contentCampos">

		<h1>Campos para Cadastro de <?php ghDigitalReplace($_sessao, 'Fichas'); ?></h1>
		<div id="contentListaFicha">
			<table border=1 width="100%" border="0" cellpadding="0" cellspacing="0" class="tableLista">
				<tr>
					<td width="20%" style="background-color: #EDECEC;">Campo</td>
					<td width="35%" style="background-color: #EDECEC;">Conteúdo</td>
					<td width="15%" style="background-color: #EDECEC;" class="hidesubficha"><?php ghDigitalReplace($_sessao, 'SubFichas'); ?></td>
					<? if($podeIncluirCampo): ?>
						<td width="15%" style="background-color: #EDECEC;">Adiciona Campos</td>
					<? endif; ?>
					<td width="15%" rowspan="2" style="text-align: center; vertical-align:top" class="alvoPrincipal">
					<?php
						if(!empty($_POST['objetos'])){
							foreach($_POST['objetos'] as $item){
								if($item['PRINCIPAL'] == 1){
									echo '<img src="'.base_url().'img.php?img_id='.$item['FILE_ID'].'" />';
								}
							}
						}
					?>
					</td>
				</tr>
				<tr>
					<td>
						<select id="Campo" name="x">
							<option value="">[Selecione]</option>
				            <?php echo montaOptions($campos, 'NOME_CAMPO','NOME_CAMPO'); ?>
			        	</select>
		        	</td>
					<td>
						<textarea id='Valor' rows='2' cols='25' name='y' value='' wrap="physical"></textarea>
					</td>
					<td class="hidesubficha">
						<input type="checkbox" name="subficha" id="Subficha">
					</td>
					<? if($podeIncluirCampo): ?>
						<td>
							<a class="button" href="javascript:" onClick="insRow('tbl','<?= base_url() ?>', $j('#TIPO_FICHA').val());" accesskey="a" title="Adicionar Campo"><span>Adicionar Campo</span></a>
						</td>
					<? endif; ?>
				</tr>
			</table>
	</div> <!-- Final de content lista ficha -->

		<div id="contentListagemFicha">
		<input type='hidden' value="<?php post('ID_FICHA'); ?>" name="ID_FICHA" id="ID_FICHA">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" id="tbl" class="ListagemFicha">
				<tr style = "background-color: #EDECEC;">
				  <td width="20%" style ="background-color: #EDECEC;">Campo</td>
				  <td width="35%" style="color: #000000;">Conteúdo</td>
				  <td width="15%" style="color: #000000;"><div id="labelSubficha1"><?php ghDigitalReplace($_sessao, 'SubFichas'); ?></div></td>
				  <td width='15%' style="color: #000000;"><? if($podeExcluirCampo): ?>Excluir<? endif; ?></td>
				  <td width='15%' style="color: #000000;"></td>
		 	  </tr>
			<tr id="modelo_pv">
			    <td>
			    	<span class="lbl_campo"></span>
			    </td>
				<td>
					<textarea rows="2" cols="25" class="textarea_valor" name="valor[]" value="" ></textarea>
					<input type="hidden" name="campo[]" value="" class="campo" />
					<!-- <textarea style="display:none" name="valor[]" value="" class="valor" ></textarea> -->
				</td>
				<td>
					<input type="radio" name="Principalcampo" class="rad_subficha" value="">
					<input type="checkbox" class="chk_subficha" onClick="checSubficha(this);">
					<input type="hidden" name="subficha[]" class="subficha" value="0"/>
				</td>
				<td align="center" style="clear: both;"><? if($podeExcluirCampo): ?><a class="button" href="javascript:" onClick='removeCampo(this);' value='Delete'><span>Excluir</span></a><? endif; ?></td>
				<td></td>
			</tr>
			<?php if(!empty($_POST['campo'])): ?>
				<?php foreach($_POST['campo'] as $idx => $valor): ?>
				<tr>
				    <td>
				    	<?php echo $_POST['campo'][$idx]; ?>
				    </td>
					<td>
						<?php //echo str_replace("\n",'<br />',$_POST['valor'][$idx]); ?>
						<input type="hidden" class="campo" name="campo[]" value="<?php echo $_POST['campo'][$idx]; ?>" />
				        <textarea style="" rows="2" cols="25" name="valor[]" value="<?php echo $_POST['valor'][$idx]; ?>" ><?php echo $_POST['valor'][$idx]; ?></textarea>
				    </td>
					<td>
						<?php if($_POST['TIPO_FICHA'] == "SUBFICHA"){ ?>
							<input type="radio" name="Principalcampo" class="rad_subficha" value="<?php echo $_POST['campo'][$idx]; ?>" <?php if(isset($_POST['Principalcampo']) && $_POST['Principalcampo'] == $_POST['campo'][$idx]){ echo "checked"; }?>>
						<?php } else{ ?>
							<input type="checkbox" class="chk_subficha" onClick="checSubficha(this);" <?php echo ($_POST['subficha'][$idx] == "1") ? "checked" : "" ; ?>>
						<?php } ?>
						<input type="hidden" name="subficha[]" class="subficha" value="<?php echo $_POST['subficha'][$idx]; ?>"/>
					</td>
		            <td align="center"><? if($podeExcluirCampo): ?><a class="button" href="javascript:" onClick='removeCampo(this);' value='Delete'><span>Excluir</span></a><? endif; ?></td>
		            <td></td>
				</tr>
				<?php endforeach;?>
			<?php endif; ?>
			</table>
		</div> <!-- Final de content listagem ficha -->


		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td width="14%">Pendências</td>
				<td width="86%" style="text-align:left"><textarea name="PENDENCIAS_CAMPOS" cols="40" rows="4" id="PENDENCIAS_CAMPOS"><?= post('PENDENCIAS_CAMPOS',true); ?></textarea></td>
			</tr>
		</table>

	</div><!-- final contentCampos -->

	<div id="contentObjetos">
	<h1 class="tit_lista">Lista de <?php ghDigitalReplace($_sessao, 'objetos'); ?> selecionados</h1>

	<div class="alignAdicionaObjeto">
	<?php if($podeIncluirObjeto): ?>
		<a id="btnAtualizarObjetos" href="#" class="button" onclick="Ficha.Combo.atualizarObjetos(); return false;"><span>Atualizar <?php ghDigitalReplace($_sessao, 'Objetos'); ?></span></a>
		<a class="button" id="btnFormPesquisa" href="javascript:new Util().vazio()" title="Adicionar objeto"><span>Adicionar <?php ghDigitalReplace($_sessao, 'Objetos'); ?></span></a>
	<?php endif;?>
	</div> <!-- Final de Alinhamento Botão Adicionar Objeto -->

	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="ListaSelecionados">
	  <thead>
	  <tr>
	    <th>Produto</th>
	    <th>Nome </th>
	    <th>Marca</th>
	    <th>Data Cria&ccedil;&atilde;o</th>
	    <th>Origem</th>
	    <th>Tipo</th>
	    <th><?php ghDigitalReplace($_sessao, 'Categoria'); ?></th>
	    <th><?php ghDigitalReplace($_sessao, 'Sub-Categoria'); ?></th>
	    <th>Keywords</th>
	    <th>Principal</th>
		<? if($podeExcluirObjeto): ?>
	    <th>Excluir</th>
        <? endif; ?>
	  </tr>
	  </thead>

	  <tbody id="lista_objetos">
	  <?php

	  if (!empty($_POST['objetos'])) {
				$objetos = &$_POST['objetos'];

				foreach($objetos as $item) {
					if((!isset($item['ID_FICHA'])) || ( isset($item['ID_FICHA']) && $item['ID_FICHA'] == $_POST['ID_FICHA'])){
					$cont = $item['FILE_ID'];
					echo '' .
					'<tr class="alter">' .
						'<td align="center">' .
							'<a href="'.base_url().'img.php?img_id='.$item['FILE_ID'].'&rand='.rand().'&img_size=big&a.jpg." class="jqzoom">'.
							'<img src="'.base_url().'img.php?img_id='.$item['FILE_ID'].'" width="40" border="0" class="imgThumb">'.
							'</a>'.
							'<input class="inputIdObjeto" type="hidden" name="objetos['.$cont.'][ID_OBJETO]" value="' . val($item, 'ID_OBJETO') . '" />' .
							'<input type="hidden" name="objetos['.$cont.'][FILENAME]" value="' . val($item, 'FILENAME') . '" />' .
							'<input type="hidden" name="objetos['.$cont.'][PATH]" value="' . val($item, 'PATH'). '" />' .
							'<input type="hidden" name="objetos['.$cont.'][FILE_ID]" value="' . val($item, 'FILE_ID'). '" />' .
							'<input type="hidden" name="objetos['.$cont.'][ID_TIPO_OBJETO]" value="' . val($item, 'DESC_TIPO_OBJETO') . '" />' .
							// DADOS GERAIS
							'<input type="hidden" name="objetos['.$cont.'][CODIGO]" value="' . val($item, 'CODIGO') . '" />' .
							'<input type="hidden" name="objetos['.$cont.'][ID_MARCA]" value="' . val($item, 'DESC_MARCA') . '" />' .
							'<input type="hidden" name="objetos['.$cont.'][DATA_UPLOAD]" value="' . val($item, 'DATA_UPLOAD') . '" />' .
							'<input type="hidden" name="objetos['.$cont.'][ORIGEM]" value="' . val($item, 'ORIGEM') . '" />' .
							'<input type="hidden" name="objetos['.$cont.'][ID_TIPO_OBJETO]" value="' . val($item, 'DESC_TIPO_OBJETO') . '" />' .
							'<input type="hidden" name="objetos['.$cont.'][ID_CATEGORIA]" value="' . val($item, 'DESC_CATEGORIA') . '" />' .
							'<input type="hidden" name="objetos['.$cont.'][ID_SUBCATEGORIA]" value="' . val($item, 'DESC_UBCATEGORIA') . '" />' .
							'<input type="hidden" name="objetos['.$cont.'][KEYWORDS]" value="' . val($item, 'KEYWORDS') . '" />' .
							'<input type="hidden" name="objetos['.$cont.'][ID_FICHA_PRODUTO]" value="' . val($item, 'ID_FICHA_PRODUTO') . '" />';

						if( !empty($item['fichas']) ){
							foreach($item['fichas'] as $idficha) {
								echo '<input class="codigos_objetos" type="hidden" name="objetos[', $cont, '][fichas][]" value="', $idficha, '" />';
							}
						}

						echo '</td>' .
						'<td style="text-align: left">' . str_replace(' ', '&nbsp;', val($item,'FILENAME')) . '</td>' .
						'<td>' . val($item, 'DESC_MARCA') . '</td>' .
						'<td>' . date('d/m/Y H:i:s', strtotime(val($item, 'DATA_UPLOAD'))) . '</td>' .
						'<td>' . val($item, 'ORIGEM') . '</td>' .
						'<td>' . val($item, 'DESC_TIPO_OBJETO') . '</td>' .
						'<td>' . val($item, 'DESC_CATEGORIA'). '</td>' .
						'<td>' . val($item, 'DESC_SUBCATEGORIA') . '</td>' .
						'<td>' . val($item, 'KEYWORDS') . '</td>';

						if($podeAlterarPrincipal){
							echo '<td><input type="radio" class="objFicha" value="' . val($item, 'FILE_ID') . '" name="PRINCIPAL"';
							checked('PRINCIPAL', val($item, 'FILE_ID'));
							echo ' /></td>';
						}else{
							echo '<td><input type="hidden" class="objFicha" value="' . val($item, 'FILE_ID') . '" name="PRINCIPAL"';
							echo ' />'.((!empty($item['PRINCIPAL']) && $item['PRINCIPAL'] == $_POST['PRINCIPAL']) ? 'Sim' : 'Não').'</td>';
						}
					 if( $podeExcluirObjeto ){
						echo '<td><a href="#" onclick="Ficha.removerObjeto(this); return false;"><img src="' . base_url().THEME.'img/trash.png" border="0" /></a></td>';
					 }

					 echo '</tr>';
					 $cont++;
				}
			}
		}
	  ?>
	  <!-- inicio modelo -->
	    <tr id="linha_modelo">
	        <td align="center">
	        	<a href="" class="jqzoom">
					<img class='imgThumb' height="60" border="0" id="id0" name="id0" src=""/>
				</a>
	            <input class="inputIdObjeto2" type="hidden" value="" name="objetos[{holder}][ID_OBJETO]"/>
				<input class="inputFile" type="hidden" value="" name="objetos[{holder}][FILENAME]"/>
	            <input class="inputPath" type="hidden" value="" name="objetos[{holder}][PATH]"/>
	            <input class="inputFileid" type="hidden" value="" name="objetos[{holder}][FILE_ID]"/>
	            <input class="inputCodigo" type="hidden" name="objetos[{holder}][CODIGO]" value="" />
	            <input class="inputMarca" type="hidden" name="objetos[{holder}][DESC_MARCA]" value="" />
	            <input class="inputDataCriacao" type="hidden" name="objetos[{holder}][DATA_UPLOAD]" value="" />
	            <input class="inputOrigem" type="hidden" name="objetos[{holder}][ORIGEM]" value="" />
	            <input class="inputTipo" type="hidden" name="objetos[{holder}][DESC_TIPO_OBJETO]" value="" />
	            <input class="inputCategoria" type="hidden" name="objetos[{holder}][DESC_CATEGORIA]" value="" />
	            <input class="inputSubcategoria" type="hidden" name="objetos[{holder}][DESC_SUBCATEGORIA]" value="" />
	            <input class="inputKeywords" type="hidden" name="objetos[{holder}][KEYWORDS]" value="" />
	            <input class="inputTipoObjeto" type="hidden" value="" name="objetos[{holder}][DESC_TIPO_OBJETO]"/>
	        </td>
	        <td class="file"></td>
	        <td class="codigo" style="display:none"></td>
	        <td class="marca"></td>
	        <td class="modified_date"></td>
	        <td class="origem"></td>
	        <td class="tipo"></td>
	        <td class="categoria"></td>
	        <td class="subcategoria"></td>
	        <td class="keywords"></td>
	        <td>
			<? if($podeAlterarPrincipal): ?>
	            <input class="objFicha" type="radio" name="PRINCIPAL" value="1"/>
	        <? else: ?>
	            Não<input class="objFicha" type="hidden" name="PRINCIPAL" value="1"/>
			<? endif; ?>
	        </td>
			<? if($podeExcluirObjeto): ?>
	        <td>
	            <a onclick="Ficha.removerObjeto(this); return false;" href="#">
	                <img border="0" src="<?php echo base_url().THEME; ?>img/trash.png"/>
	            </a>
	        </td>
			<? endif; ?>
	    </tr>
		<!-- fim modelo -->
		
		<!-- inicio modelo -->
	    <tr id="modelo_AdicionarObjetoSubFicha">
	        <td style="width: 60px; padding: 0px; text-align: center;" class="infoObjeto">
	        	<a href="" class="jqzoom">
					<img class='imgThumb' height="40" border="0" id="id0" name="id0" src=""/>
				</a>
	            <input class="inputIdObjeto2" type="hidden" value="" name="objetos[{holder}][ID_OBJETO]"/>
	            <input class="inputIdSubFicha" type="hidden" value="" name="objetos[{holder}][ID_SUBFICHA]"/>
	            <input class="inputPrincipal" type="hidden" value="" name="objetos[{holder}][PRINCIPAL]"/>
				<input class="inputFile" type="hidden" value="" name="objetos[{holder}][FILENAME]"/>
	            <input class="inputPath" type="hidden" value="" name="objetos[{holder}][PATH]"/>
	            <input class="inputFileid" type="hidden" value="" name="objetos[{holder}][FILE_ID]"/>
	            <input class="inputCodigo" type="hidden" name="objetos[{holder}][CODIGO]" value="" />
	            <input class="inputMarca" type="hidden" name="objetos[{holder}][DESC_MARCA]" value="" />
	            <input class="inputDataCriacao" type="hidden" name="objetos[{holder}][DATA_UPLOAD]" value="" />
	            <input class="inputOrigem" type="hidden" name="objetos[{holder}][ORIGEM]" value="" />
	            <input class="inputTipo" type="hidden" name="objetos[{holder}][DESC_TIPO_OBJETO]" value="" />
	            <input class="inputCategoria" type="hidden" name="objetos[{holder}][DESC_CATEGORIA]" value="" />
	            <input class="inputSubcategoria" type="hidden" name="objetos[{holder}][DESC_SUBCATEGORIA]" value="" />
	            <input class="inputKeywords" type="hidden" name="objetos[{holder}][KEYWORDS]" value="" />
	            <input class="inputTipoObjeto" type="hidden" value="" name="objetos[{holder}][DESC_TIPO_OBJETO]"/>
	        </td>
	        <td class="file" style="width: 185px; padding: 0px;text-align: left;"></td>
	        <td class="codigo" style="display:none"></td>
	        <td class="tipo" style="width: 55px; padding: 0px;"></td>
	        <td class="categoria" style="width: 60px; padding: 0px;"></td>
	        <td class="subcategoria" style="width: 184px; padding: 0px;"></td>
	        <td style="width: 48px; text-align: center; padding: 0px;">
			<? if($podeAlterarPrincipal): ?>
	            <input class="objSubFicha" type="radio" fileID="" name="SUBFICHA_PRINCIPAL" value="1"/>
	        <? else: ?>
	            Não<input class="objSubFicha" type="hidden" fileID="" name="SUBFICHA_PRINCIPAL" value="1"/>
			<? endif; ?>
	        </td>
			<? if($podeExcluirObjeto): ?>
	        <td style="width: 38px; text-align: center; padding: 0px;">
	            <a onclick="Ficha.removerObjeto(this); return false;" href="#">
	                <img border="0" src="<?php echo base_url().THEME; ?>img/trash.png"/>
	            </a>
	        </td>
			<? endif; ?>
	    </tr>
		<!-- fim modelo -->
	
		</tbody>
	</table>
	</div>
	

	<div id="contentCombo">
		<p><a style="float: right;" href="#" onclick="Ficha.Combo.buscarFichas(); return false;" class="button"><span>Pesquisar Fichas</span></a></p>
		<p style="clear:both">&nbsp;</p>

		<h1>Fichas que compõem este combo</h1>

		<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabela_padrao">
			<thead>
				<tr>
					<td width="17%">Código de Barras</td>
					<td width="23%">Nome</td>
					<td width="20%">Categoria</td>
					<td width="19%">Sub Categoria</td>
					<td width="17%">Data de Criação</td>
					<td width="4%">&nbsp;</td>
				</tr>
			</thead>
			<tbody id="containerFichasCombo">
				<tr id="modelo_ficha_combo">
					<td>
						<input class="fichaCombo" name="fichasCombo[{ROW_ID}][ID_FICHA]" type="hidden" value="{ID_FICHA}" />
						<input name="fichasCombo[{ROW_ID}][NOME_FICHA]" type="hidden" value="{NOME_FICHA}" />
						<input name="fichasCombo[{ROW_ID}][COD_BARRAS_FICHA]" type="hidden" value="{COD_BARRAS_FICHA}" />
						<input name="fichasCombo[{ROW_ID}][DESC_CATEGORIA]" type="hidden" value="{DESC_CATEGORIA}" />
						<input name="fichasCombo[{ROW_ID}][DESC_SUBCATEGORIA]" type="hidden" value="{DESC_SUBCATEGORIA}" />

						{COD_BARRAS_FICHA}
					</td>
					<td>{NOME_FICHA}</td>
					<td>{DESC_CATEGORIA}</td>
					<td>{DESC_SUBCATEGORIA}</td>
					<td>{DT_INSERT_FICHA}</td>
					<td>
						<a onclick="Ficha.Combo.removerFicha('{ID_FICHA}'); return false;" href="#">
							<img border="0" src="<?php echo base_url().THEME; ?>img/trash.png"/>
						</a>
					</td>
				</tr>
			</tbody>
		</table>

	</div>
	
	<div id="contentFichaCombo">
		<?php if( !empty($fichasComboAssociadas) ): ?>
			<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabela_padrao">
				<thead>
					<tr>
						<td width="12%">Produto</td>
						<td width="28%">Nome Ficha</td>
						<td width="20%">Código de Barras</td>
						<td width="11%">Situação</td>
						<td width="10%">Temporário</td>
						<td width="17%">Data de Criação</td>
						<td width="2%">
							<input type="checkbox" onclick="$j('.atualizarFichas').attr('checked',this.checked)" title="Selecionar Todos" />
						</td>
					</tr>
				</thead>
				<tbody>
					<?php foreach($fichasComboAssociadas as $item): ?>
					<tr>
						<td style="text-align:center">
							<?php
							if( !empty($item['FILE_ID'])) {
								echo '<img height="50" src="', base_url(), 'img.php?img_id=', $item['FILE_ID'], '" />';
							}
							?>
						</td>
						<td><?php echo $item['NOME_FICHA']; ?></td>
						<td><?php echo $item['COD_BARRAS_FICHA']; ?></td>
						<td><?php echo $item['DESC_APROVACAO_FICHA']; ?></td>
						<td><?php echo $item['FLAG_TEMPORARIO'] == 0 ? 'Não' : 'Sim'; ?></td>
						<td><?php echo date('d/m/Y H:i', strtotime($item['DT_INSERT_FICHA'])); ?></td>
						<td><input type="checkbox" name="atualizarFichas" class="atualizarFichas" value="<?php echo $item['ID_FICHA']; ?>"  /></td>
					</tr>
					<?php endforeach; ?>
					<tr>
						<td colspan="7">
							<hr />
							<img src="<?php echo base_url().THEME ?>img/alerta-amarelo.jpg" width="22" height="18" alt="Atenção" />
							Para atualizar os objetos nas fichas-combo associadas, os objetos da ficha atual serão salvos primeiro.</td>
					</tr>
					<tr>
						<td colspan="7"><a href="#" onclick="Ficha.Combo.atualizarAssociacoes($j(this).parent()); return false;" class="button"><span>Atualizar Objetos</span></a></td>
					</tr>
				</tbody>
			</table>

		<?php else: ?>
			Nenhuma ficha-combo associada.
		<?php endif; ?>
	</div>
	
	<div id="contentSubFichas">		
		<h1 class="tit_lista"><?php ghDigitalReplace($_sessao, 'SubFichas'); ?></h1>

		<div class="alignAdicionaSubFicha">
			<?php if($podeIncluirSubficha == 1): ?>
				<a class="button" id="btnFormPesquisaSubFicha" href="javascript:new Util().vazio()" title="<?php ghDigitalReplace($_sessao, 'Pesquisar Subficha'); ?>"><span><?php ghDigitalReplace($_sessao, 'Pesquisar Subficha'); ?></span></a>
				<a class="button" id="btnFormCadastraSubFicha" href="javascript:new Util().vazio()" title="<?php ghDigitalReplace($_sessao, 'Adicionar Subficha'); ?>"><span><?php ghDigitalReplace($_sessao, 'Adicionar Subficha'); ?></span></a>
			<?php endif; ?>
		</div> <!-- Final de Alinhamento Botão Adicionar Objeto -->

		<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabela_padrao">
			<thead>
				<tr>
					<td width="7%"><?php ghDigitalReplace($_sessao, 'Objeto'); ?></td>
					<td width="30%"><?php ghDigitalReplace($_sessao, 'Código de Barras'); ?></td>
					<td width="30%">Criado por</td>
					<td width="20%">Data de Criação</td>
					<td width="10%"></td>
					<td width="10%"></td>
				</tr>
			</thead>
			<tbody id="container_subficha">
				<?php foreach($subfichas as $sf): //print_rr($subfichas);die; ?>
					<tr id="trSubficha<?php echo $sf['ID_FICHA2']; ?>">
						<td class="lms_objeto">
							<a id="imgHREF_<?php echo $sf['ID_FICHA2']; ?>" href="<?= base_url();?>img.php?img_id=<?= $sf['FILE_ID'];?>&rand=<?= rand()?>&img_size=big&a.jpg" class="jqzoom">
							<img id="imgSRC_<?php echo $sf['ID_FICHA2']; ?>" src="<?= base_url().'img.php?img_id='.$sf['FILE_ID']?>" width="50" border="0" class='imgThumb'>
							</a>
						</td>
						<td class="lms_cod_barras"><?php echo $sf['COD_BARRAS_FICHA']; ?></td>
						<td class="lms_nome_usuario"><?php echo $sf['NOME_USUARIO']; ?></td>
						<td class="lms_dt_insert_ficha"><?php echo format_date_to_form($sf['DT_INSERT_FICHA']); ?></td>
						<td>
							<a href="#" onclick="editarSubficha('<?php echo $sf['ID_FICHA1'];?>', '<?php echo $sf['ID_FICHA2'];?>')">
								<img src="<?php echo base_url() . THEME; ?>img/file_edit.png" />
							</a>
						</td>
						<td>
							<a href="#" onclick="Ficha.Subficha.desvincular(this, <?php echo $sf['ID_FICHA1'];?>, <?php echo $sf['ID_FICHA2'];?>); return false">
								<img src="<?php echo base_url() . THEME; ?>img/trash.png" />
							</a>
						</td>
					</tr>
				<?php endforeach; ?>
				<tr id="linha_modelo_subficha">
					<td class="lms_objeto">
						<a id="imgHREF_{ID_FICHA2}" href="<?= base_url();?>img.php?img_id={FILE_ID}&rand=<?= rand()?>&img_size=big&a.jpg" class="jqzoom">
						<img id="imgSRC_{ID_FICHA2}" src="<?= base_url().'img.php?img_id={FILE_ID}'?>" width="50" border="0" class='imgThumb'>
						</a>
					</td>
					<td class="lms_cod_barras">{COD_BARRAS_FICHA}</td>
					<td class="lms_nome_usuario">{NOME_USUARIO}</td>
					<td class="lms_dt_insert_ficha">{DT_INSERT_FICHA}</td>
					<td>
						<a href="#" onclick="editarSubficha('{ID_FICHA1}', '{ID_FICHA2}')">
							<img src="<?php echo base_url() . THEME; ?>img/file_edit.png" />
						</a>
					</td>
					<td>
						<a href="#" onclick="Ficha.Subficha.desvincular(this, {ID_FICHA1}, {ID_FICHA2}); return false">
							<img src="<?php echo base_url() . THEME; ?>img/trash.png" />
						</a>
					</td>
				</tr>
			</tbody>
		</table>
	</div>

</div><!-- final tabs -->

<div id="contentBotoes">
	<?php
	if( !empty($fichasComboAssociadas) && post('TIPO_FICHA',true) == 'PRODUTO' ){
		echo '<p>
			<img src="', base_url(), THEME, 'img/alerta-amarelo.jpg" />
			Esta ficha possui fichas-combo associadas, portanto você não pode alterar a aprovação.
			</p><p>&nbsp;</p>';
	}
	?>

	<?php if(Sessao::hasPermission('ficha','modal_aprovacao')  && post('ID_FICHA',true) != '' && empty($fichasComboAssociadas)  ): ?>
		<a class="button" href="javascript:" onclick="displayMessagewithparameter('<?=site_url('ficha/modal_aprovacao/'. $this->uri->segment(3))?>',800,600);" ><span>Aprovação</span></a>
	<?php endif;?>

	<? if(!empty($podeSalvar)):?>
		<a class="button" href="javascript:new Util().vazio()" onclick="salvar(false);"><span>Salvar</span></a>
        <? endif; ?>

        <? if(!empty($podeEmail)):?>
            <a class="button" href="javascript:new Util().vazio()" onclick="salvar(true);"><span>Salvar com envio de email</span></a>
        <? endif; ?>
        <?php if(isset($_SESSION['ID_JOB']) && !empty($_SESSION['ID_JOB'])){?>
        	<a class="button" href="javascript:new Util().vazio()" onClick="location.href='<?= base_url().index_page() ?>/carrinho/form/<?= $_SESSION['ID_JOB']?>';" alt="Cancelar" title="Cancelar" ><span>Cancelar</span></a>
        <?php } else if(isset($_SESSION['idJob']) && !empty($_SESSION['idJob'])){ ?>
				<a class="button" href="javascript:new Util().vazio()" onClick="location.href='<?= base_url().index_page() ?>/checklist/form/<?= $_SESSION['idJob']?>';" alt="Cancelar" title="Cancelar" ><span>Cancelar</span></a>
		<?php } else if(isset($_SESSION['relatorioObjetoPendente']) && !empty($_SESSION['relatorioObjetoPendente'])){ ?>
				<a class="button" href="javascript:new Util().vazio()" onClick="location.href='<?= base_url().index_page() ?>/relatorios/relatorio_objetos_pendentes/tela';" alt="Cancelar" title="Cancelar" ><span>Cancelar</span></a>
		<?php 	}else{ ?>
			<a class="button" href="javascript:new Util().vazio()" onClick="location.href='<?= base_url().index_page() ?>/ficha/lista/';" alt="Cancelar" title="Cancelar" ><span>Cancelar</span></a>
        <?php }?>
</div> <!-- Final de botões -->
</form>

</div><!-- final contentGlobal -->


<script type="text/javascript">
var idficha = '<?php echo $idficha; ?>';
var fichas = eval('[<?php echo str_replace(array('\\',"'"),array('\\\\',"\\'"),json_encode($fichasCombo)); ?>]')[0];
var fichaPai = eval('[<?php echo str_replace(array('\\',"'"),array('\\\\',"\\'"),json_encode($fichaPai)); ?>]')[0];
var fichaFilha = eval('[<?php echo str_replace(array('\\',"'"),array('\\\\',"\\'"),json_encode($fichaFilha)); ?>]')[0];
var modelo_pv = null;
var modelo_barra = null;
var linha_modelo_subficha = null;
var modelo_subficha = null;
var modelo_AdicionarObjetoSubFicha = null;

var codCenarios = Array();
<?php foreach ( $cenarios as $c ): ?>
	codCenarios.push( '<?php echo $c['COD_CENARIO']; ?>' );
<?php endforeach; ?>

$j(function(){
	$j('#tabs').tabs();
	atribuiCalendarios('.data');
	
	$j('.jqzoom').fancybox();
	
	modelo_pv = $j('#modelo_pv').clone();
	$j('#modelo_pv').remove();

	Ficha.modelo = $('linha_modelo');
	$('linha_modelo').remove();

	modelo_subficha = $j('#linha_modelo_subficha').clone();
	$j('#linha_modelo_subficha').remove();
	
	modelo_AdicionarObjetoSubFicha = $('modelo_AdicionarObjetoSubFicha');
	$('modelo_AdicionarObjetoSubFicha').remove();

	$j("#btnFormPesquisa").click(function(e) {
		displayMessagewithparameter('<?php echo base_url().index_page().'/ficha/form_pesquisa/'; ?>', 800, 600, function(){
			$j('#PERIODO_INICIAL,#PERIODO_FINAL').datepicker({dateFormat: 'dd/mm/yy',showOn: 'button', buttonImage: '<?= base_url().THEME ?>img/calendario.png', buttonImageOnly: true});
		});
	});

	$j("#btnFormCadastraSubFicha").click(function(e) {
		<?php if(isset($_POST['ID_FICHA']) && is_numeric($_POST['ID_FICHA']) ):?>
			displayMessagewithparameter('<?php echo base_url().index_page().'/ficha/form_subficha/'.$_POST['ID_FICHA']; ?>', 1000, 600);
		<?php else: ?>
			alert('Para cadastrar ' + '<?php ghDigitalReplace($_sessao, "subficha"); ?>' + ' é necessário antes cadastrar a ' + '<?php ghDigitalReplace($_sessao, "ficha"); ?>' + ' em questão.');
		<?php endif;?>
	});

	$j("#btnFormPesquisaSubFicha").click(function(e) {
		<?php if(isset($_POST['ID_FICHA']) && is_numeric($_POST['ID_FICHA'])):?>
			displayMessagewithparameter('<?php echo base_url().index_page().'/json/ficha/busca_subfichas'; ?>', 800, 600);
		<?php else:?>
			alert('Para pesquisar ' + '<?php ghDigitalReplace($_sessao, "subficha"); ?>' + ' é necessário antes cadastrar a ' + '<?php ghDigitalReplace($_sessao, "ficha"); ?>' + ' em questão.');
		<?php endif;?>
	});

	$j("#form_ficha").submit(function() {
	  var chk = $j(".objFicha:checked").length;
	  var objs = $j(".objFicha").length;

      if ( chk > 0 || objs == 0) {
        return true;
      }
      alert("Selecione um objeto principal.");
      return false;
    });

	$('ID_CATEGORIA').addEvent('change', function(){
		clearSelect($('ID_SUBCATEGORIA'),1);
		montaOptionsAjax($('ID_SUBCATEGORIA'),'<?php echo site_url("json/admin/getSubcategoriasByCategoria"); ?>','id='+this.value,'ID_SUBCATEGORIA','DESC_SUBCATEGORIA');
	});

	$('ID_FABRICANTE').addEvent('change', function(){
		clearSelect($('ID_MARCA'),1);
		montaOptionsAjax($('ID_MARCA'),'<?php echo site_url("json/admin/getMarcasByFabricante"); ?>','id='+this.value,'ID_MARCA','DESC_MARCA');
	});

	$('ID_CLIENTE').addEvent('change', function(){
		clearSelect($('ID_CATEGORIA'),1);
		clearSelect($('ID_SUBCATEGORIA'),1);

		var idCliente = $('ID_CLIENTE').value;

		if($j('#ID_CLIENTE option:selected').text() != 'HERMES'){
			$j('#ASTERISCO_NOME_PRODUTO').show();
		}
		else{
			$j('#ASTERISCO_NOME_PRODUTO').hide();
		}
		
		montaOptionsAjax($('Campo'),'<?php echo site_url('json/admin/getCamposByCliente'); ?>','idCliente='+idCliente,'NOME_CAMPO','NOME_CAMPO');
		montaOptionsAjax($('ID_CATEGORIA'),'<?php echo site_url('json/admin/getCategoriasByCliente'); ?>','id=' + this.value,'ID_CATEGORIA','DESC_CATEGORIA');

		carregaCampos();
	});

	$j('#TIPO_FICHA').change(trocaTipoFicha);

	$j(fichas).each(function(){
		Ficha.Combo.adicionarFicha( this );
	});

	$j(fichaPai).each(function(){
		Ficha.Pai.adicionarFicha( this );
	});

	$j(fichaFilha).each(function(){
		Ficha.Filha.adicionarFicha( this );
	});

	trocaTipoFicha();
	updateRadioPrincipal();
});

function carregaCampos(){
	var idcliente = $j('#ID_CLIENTE').val();
	var data = 'idcliente='+idcliente+'&idficha=<?php echo $idficha; ?>';

	$j('#codigos_regionais tr').remove();

	$j.post('<?php echo site_url('json/ficha/carrega_codigos'); ?>', data, function(html){
		$j(html).appendTo('#codigos_regionais');
	});
}

function updateRadioPrincipal(){
	$j('.objFicha').click(function(){
		var img = $j(this).parent().parent().find('.imgThumb');
		$j('.alvoPrincipal').html('').append( img.clone() );
	});
}

function trocaTipoFicha(){
	//$j('.triggers').hide();
	$j('.codigo_barras_produto, .codigo_barras_combo').hide();
	$j('.codigo_barras_' + $j('#TIPO_FICHA').val().toLowerCase()).show();

	if( $j('#TIPO_FICHA').val() == 'SUBFICHA' ){
		//$j('#contObjetos').hide();
		$j('#contCombo').hide();
		$j('#contFichaCombo').hide();
		$j('#contSubfichas').hide();
		$j('.codigo_barras_produto').show();
		$j('.hidesubficha').hide();
		$j('#labelSubficha1').html('Principal');
		$j('.chk_subficha').hide();
		$j('.rad_subficha').show();
	}
	else if( $j('#TIPO_FICHA').val() == 'COMBO' ){
		$j('#contObjetos').show();
		$j('#contCombo').show();
		$j('#contFichaCombo').hide();
		$j('#contSubfichas').hide();
		$j('#btnAtualizarObjetos').show();
		$j('.codigo_barras_combo').find('input').attr('name', 'COD_BARRAS_FICHA');
		$j('.codigo_barras_combo').show();
		$j('.hidesubficha').show();
		$j('#labelSubficha1').html('<?php ghDigitalReplace($_sessao, 'SubFichas'); ?>');
		$j('.chk_subficha').show();
		$j('.rad_subficha').hide();
	}
	else if( $j('#TIPO_FICHA').val() == 'PRODUTO' ){
		$j('#contObjetos').show();
		$j('#contCombo').hide();
		$j('#contFichaCombo').show();
		$j('#contSubfichas').show();
		$j('#btnAtualizarObjetos').hide();
		$j('.codigo_barras_produto').show();
		$j('.codigo_barras_combo').find('input').attr('name', 'COD_BARRAS_FICHA_COMBO');
		$j('.hidesubficha').show();
		$j('#labelSubficha1').html('<?php ghDigitalReplace($_sessao, 'SubFichas'); ?>');
		$j('.chk_subficha').show();
		$j('.rad_subficha').hide();
	}

	//$j('#COD_BARRAS_FICHA').attr('readonly', $j('#TIPO_FICHA').val() == 'COMBO');
}


function adicionarCodigoCombo(valor){
	if(modelo_barra == null){
		modelo_barra = $j('#mod_cod_barras').clone();
		modelo_barra.css('display','');
		$j('#mod_cod_barras').remove();
	}
	var ipt = modelo_barra.clone();
	ipt.removeAttr('id');
	ipt.find('input').val(valor);
	$j('#insertCodBarra').after(ipt);
}

function removeCampo(obj) {
	if( codCenarios.length > 0 ){
		var msg = 'Esta ficha está associada a um Cenário:\n';
		for( var i = 0; i <= codCenarios.length -1; i++ ){
			msg = msg + codCenarios[i] + '\n';
		}
		msg = msg + '\nConfirma a exclusão do campo?\n\n';
		if( confirm(msg) ){
			removeLinha(obj);
		}
	}
	else{
		removeLinha(obj);
	}
}

function salvar(comEnvioEmail){
	var statusAtual = '<?php if( isset($_POST['FLAG_ACTIVE_FICHA']) ){ echo $_POST['FLAG_ACTIVE_FICHA']; } else{ echo ''; } ?>';
	var statusNovo = $j('#FLAG_ACTIVE_FICHA:checked').val();

	if(comEnvioEmail){
		$j('#form_ficha').attr('action','<?= base_url().index_page() ?>/ficha/salvarComEmail/<?= $this->uri->segment(3) ?>').submit();
	}
	
	if( codCenarios.length > 0 ){
		if( (statusAtual == '1') && (statusNovo == '0') ){
			var msg = 'Esta ficha está sendo utilizada por um Cenário, confirma a inativação/alteração?';
			if( confirm(msg) ){
				$j('#form_ficha').submit();
			}
		}
		else{
			$j('#form_ficha').submit();
		}
	}
	else{
		$j('#form_ficha').submit();
	}
}

function salvarHistorico(){
	var situacaoAtual = '<?php if( isset($_POST['ID_APROVACAO_FICHA']) ){ echo $_POST['ID_APROVACAO_FICHA']; } else{ echo ''; } ?>';
	var situacaoNova = $j('#ID_APROVACAO_FICHA:checked').val();

	if( (situacaoAtual == '1') && (situacaoNova != '1') ){
		var msg = 'Esta ficha está sendo utilizada por um Cenário, confirma a inativação/alteração?';
		if( confirm(msg) ){
			$j('#outro').submit();
		}
	}
	else{
		$j('#outro').submit();
	}
}

function checSubficha(objCheckbox){
	if($j(objCheckbox).attr("checked") == "checked"){
		$j(objCheckbox).parent().find(".subficha").val("1");
	}
	else{
		$j(objCheckbox).parent().find(".subficha").val("0");
	}
}

function adcionarSubficha(data){
	var container = $j('#container_subficha');
	var linha_modelo_subficha = $j(modelo_subficha.clone());

	//alteração foi pois estava dando erro de javascript quando o objeto já existia na tela
	if(data != null || data != undefined){
		linha_modelo_subficha.attr('id','trSubficha'+data.ID_FICHA);

		var el = $j('<div></div>');
		$j(linha_modelo_subficha).appendTo(el);
		
		html = el.html();
	
		html = html.replace(new RegExp('\{'+'ID_FICHA1'+'\}','g'), idficha);
		html = html.replace(new RegExp('\{'+'ID_FICHA2'+'\}','g'), data.ID_FICHA);
	
		for(var name in data){
			html = html.replace(new RegExp('\{'+name+'\}','g'), data[ name ]);
		}
		
		$j(container).append(html);
	}
}

function atualizarSubficha(data){
	$j('#trSubficha'+data.ID_FICHA).find(".lms_cod_barras").html(data.COD_BARRAS_FICHA);
	$j('#trSubficha'+data.ID_FICHA).find(".lms_nome_usuario").html(data.NOME_USUARIO);
	$j('#trSubficha'+data.ID_FICHA).find(".lms_dt_insert_ficha").html(data.DT_INSERT_FICHA);
}

function editarSubficha(idFicha1, idFicha2){
	displayMessagewithparameter("<?php echo base_url().index_page().'/ficha/form_subficha/'; ?>" + idFicha1 + "/" + idFicha2 , 1000, 600);
}

function pesquisarSubficha(){
	displayMessagewithparameter("<?php echo base_url().index_page().'/json/subficha/busca_fichas_filha'; ?>" + idFicha1 + "/" + idFicha2 , 800, 600);
}

function vincularSubficha(idSubficha){
	$j.post(
			'<?php echo base_url().index_page(); ?>/json/subficha/vincular', 
			{idFicha1 : idficha, idFicha2 : idSubficha}, 
			function(data){
				var json = $j.parseJSON(data);
				adcionarSubficha(json);
			}
		);
}

function importaCompraFacil(){
	if($j('#COD_BARRAS_FICHA').val() == ""){
		alert('Digite uma referência');
		$j('#COD_BARRAS_FICHA').focus();
	}
	else{
		$j('#carregando').show();
		var referencia = $j('#COD_BARRAS_FICHA').val();
		$j.post(
				'<?php echo base_url().index_page(); ?>/json/ficha/importaCompraFacil/' + referencia, 
				{}, 
				function(data){
					var json = $j.parseJSON(data);
					if(json.msgErro == undefined){
						if(json.COD_CIP_FICHA != ""){
							verificaCipExistente(json.COD_CIP_FICHA);
						}
						$j('#COD_CIP_FICHA').val(json.COD_CIP_FICHA);
						$j('#NOME_FICHA').val(json.NOME_FICHA);
						
						if(!json.MODELO_FICHA){
							$j('#MODELO_FICHA').val("");
						}
						else{
							$j('#MODELO_FICHA').val(json.MODELO_FICHA);
						}
						
						selecionaMarca(json.DESC_MARCA);
						selecionaSubcategoria(json.DESC_SUBCATEGORIA);
						removeTodosCampos();
						if(json.CAMPO_VOLTAGEM != ""){
							insRowImportacao('tbl','voltagem', json.CAMPO_VOLTAGEM);
						}
						if(json.CAMPO_GARANTIA != ""){
							insRowImportacao('tbl','garantia', json.CAMPO_GARANTIA);
						}
						if(json.CAMPO_DIMENSOES != ""){
							insRowImportacao('tbl','dimensoes', json.CAMPO_DIMENSOES);
						}
						if(json.CAMPO_DESCRICAO != ""){
							insRowImportacao('tbl','descricao', json.CAMPO_DESCRICAO);
						}
						if(json.CAMPO_COR != ""){
							insRowImportacao('tbl','cor', json.CAMPO_COR);
						}
					}
					else{
						alert(json.msgErro);
						$j('#COD_BARRAS_FICHA').select();
					}
				}
			);
		$j('#carregando').hide();
	}
}

function importaHermes(){
	if($j('#COD_BARRAS_FICHA').val() == ""){
		alert('Digite uma referência');
		$j('#COD_BARRAS_FICHA').focus();
	}
	else{
		$j('#carregando').show();
		$j('#ID_CATEGORIA').prop('selectedIndex', 0);
		clearSelect($('ID_SUBCATEGORIA'),1);
		var referencia = $j('#COD_BARRAS_FICHA').val();
		$j.post(
				'<?php echo base_url().index_page(); ?>/json/ficha/importaHermes/' + referencia, 
				{}, 
				function(data){
					var json = $j.parseJSON(data);
					if(json.msgErro == undefined){
						$j('#NOME_FICHA').val(json.NOME_FICHA);

						selecionaCategoria(json.DESC_CATEGORIA);
						selecionaSubcategoria(json.DESC_SUBCATEGORIA);
						removeTodosCampos();
						if(json.CAMPO_DIMENSOES != ""){
							insRowImportacao('tbl','dimensoes', json.CAMPO_DIMENSOES);
						}
						if(json.CAMPO_DESCRICAO != ""){
							insRowImportacao('tbl','descricao', json.CAMPO_DESCRICAO);
						}
						if(json.CAMPO_ETIQUETA != ""){
							insRowImportacao('tbl','etiqueta', json.CAMPO_ETIQUETA);
						}
						if(json.CAMPO_COMPOSICAO != ""){
							insRowImportacao('tbl','composicao', json.CAMPO_COMPOSICAO);
						}
						if(json.CAMPO_CHAMADA != ""){
							insRowImportacao('tbl','chamada', json.CAMPO_CHAMADA);
						}
					}
					else{
						alert(json.msgErro);
						$j('#COD_BARRAS_FICHA').select();
					}
				}
			);
		$j('#carregando').hide();
	}
}

function verificaCipExistente(cip){
	$j.post(
			'<?php echo base_url().index_page(); ?>/json/ficha/verificaCipExistente/' + cip, 
			{}, 
			function(data){
				if(data != 'false'){
					var j = $j.parseJSON(data);
					alert('Já existe(m) produto(s) relacionado(s) a esta referência. \nO sistema irá relaciona-lo automaticamente ao cadastrar.');
					$j('#TIPO_FICHA').find('option[value="SUBFICHA"]').attr('selected',true);
					trocaTipoFicha();
					$j("#TIPO_FICHA option").not(":selected").attr("disabled", "disabled");
				}
				else{
					return false;
				}
			}
		);
}

function selecionaMarca(marca){
	var fabricante = $j('#ID_FABRICANTE').val();
	$j.post(
			'<?php echo base_url().index_page(); ?>/json/marca/getByDescFabricante/' + marca + '/' + fabricante, 
			{}, 
			function(data){
				if(data != 'false'){
					var j = $j.parseJSON(data);
					$j('#ID_MARCA').find('option[value="' +j.ID_MARCA+ '"]').attr('selected',true);
				}
				else{
					return false;
				}
			}
		);
}

function selecionaCategoria(categoria){
	if(categoria != ''){
		$j("#ID_CATEGORIA").find("option:contains('" +categoria+ "')").each(function(){
			if( $j(this).text() == categoria ) {
				$j(this).attr("selected","selected");
				var idCategoria = $j(this).val();
				clearSelect($('ID_SUBCATEGORIA'),1);
				montaOptionsAjax($('ID_SUBCATEGORIA'),'<?php echo site_url("json/admin/getSubcategoriasByCategoria"); ?>','id='+idCategoria,'ID_SUBCATEGORIA','DESC_SUBCATEGORIA');
			}
		});
	}
	else{
		return false;
	}
}

function selecionaSubcategoria(subcategoria){
	var categoria = $j('#ID_CATEGORIA').val();
	if(categoria != ''){
		$j.post(
				'<?php echo base_url().index_page(); ?>/json/categoria/getSubcategoriaByDescCategoria/', 
				{descricao : subcategoria, idCategoria : categoria}, 
				function(data){
					if(data != 'false'){
						var j = $j.parseJSON(data);
						var idCategoria = $j('#ID_CATEGORIA').val();
						clearSelect($('ID_SUBCATEGORIA'),1);
						montaOptionsAjax($('ID_SUBCATEGORIA'),'<?php echo site_url("json/admin/getSubcategoriasByCategoria"); ?>','id='+idCategoria,'ID_SUBCATEGORIA','DESC_SUBCATEGORIA',j.ID_SUBCATEGORIA);
					}
					else{
						return false;
					}
				}
			);
	}
}

function insRowImportacao(id,campo,valor){
	var campoValue = campo;
	var valorValue = valor;

	var existeInserido = false;
	$j('.campo').each(function(){
		if($j(this).val() == campoValue) {
			existeInserido = true;
		}
	});

	var existeCampo = false;
	$j("#Campo").find("option:contains('" +campo+ "')").each(function(){
		if( $j(this).text().toLowerCase() == campo.toLowerCase() ) {
			existeCampo = true;
		}
	});

	if(!existeInserido && existeCampo){
		if(campoValue != '' && valorValue != ''){
			var item = $j(modelo_pv.clone());
			item.find('.campo').val(campoValue);
			item.find('.valor').val(valorValue);
			
			item.find('.lbl_campo').html(campoValue.replace('\n', '<br />'));
			item.find('.textarea_valor').html(valorValue.replace(/\n/g, '<br />'));
			item.find('.rad_subficha').attr('value', campoValue);
					
			$j('#'+id).append(item);
	
			$j('.chk_subficha').show();
			$j('.rad_subficha').hide();
			
			if($j('#TIPO_FICHA').val() == 'SUBFICHA'){
				$j('.chk_subficha').hide();
				$j('.rad_subficha').show();
			}
			else{
				$j('.chk_subficha').show();
				$j('.rad_subficha').hide();
			}		
		}
	}

}

function removeTodosCampos(){
	$j('.campo').each(function(){
		removeLinha($j(this).closest('tr'));
	});
}

function excluir(){
	if( confirm("Tem certeza que deseja excluir?") ){
		$j('#form_ficha').attr('action','<?= base_url().index_page() ?>/ficha/excluir/<?= $this->uri->segment(3) ?>').submit();
	}	
}

</script>



<?php $this->load->view("ROOT/layout/footer") ?>




















