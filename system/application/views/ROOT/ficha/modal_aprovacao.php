<h1>Aprova&ccedil;&atilde;o de Fichas</h1>
<form method="post" action="<?php echo site_url('ficha/saveHistory/'.$ficha['ID_FICHA']); ?>" id="outro" name="outro">
  <div>
    <table width="100%" border="0" cellspacing="1" cellpadding="2">
      <tr>
        <td width="28%" valign="top" scope="col" style="vertical-align:top">Hist&oacute;rico</td>
        <td width="72%" scope="col">
      	  <div style="height: 250px; overflow:auto; padding-left:2px; padding-top: 2px;" id="tableObjeto">
        	<table width="100%" border="0" class="tableSelection">
            <thead>
                <tr>
                  <td>Data </td>
                  <td>Usu&aacute;rio</td>
                  <td>Coment&aacute;rio</td>
                </tr>
            </thead>
            <?php	if($historico): ?>
            <?php foreach($historico as $item): ?>
            <tr>
              <td width="24%"><?=format_date_to_form($item['DT_INSERT_HISTORICO']);?></td>
              <td width="26%"><?=$item['NOME_USUARIO'];?></td>
              <td width="50%"><?=$item['COMENT_HISTORICO_FICHA'];?></td>
            </tr>
            <?php endforeach;?>
            <?php endif;?>
          </table>
          </div>
        </td>
      </tr>
      <tr>
        <td valign="top" style="vertical-align:top">Observa&ccedil;&otilde;es</td>
        <td><textarea name="COMENT_HISTORICO_FICHA" rows="5" cols="40">-</textarea></td>
      </tr>
      <tr>
        <td>Situação</td>
        <td><?php if($status_aprovacao):?>
          <?php foreach($status_aprovacao['data'] as $item):?>
          <input style="border:0px;" type="radio" <?=(isset($ficha['ID_APROVACAO_FICHA'])&&$item['ID_APROVACAO_FICHA']==$ficha['ID_APROVACAO_FICHA'])?'checked="checked"':NULL;?> value="<?=$item['ID_APROVACAO_FICHA']?>" name="ID_APROVACAO_FICHA" id="ID_APROVACAO_FICHA" />
          <?=ucfirst($item['DESC_APROVACAO_FICHA'])?>
          &nbsp;&nbsp;
          <?php endforeach;?>
          <?php endif;?></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>
        	<a href="javascript:" onclick="closeMessage();" class="button"><span>Cancelar</span></a> 
        	<a href="javascript:" onclick="salvarHistorico();" class="button"><span>Gravar</span></a></td>
      </tr>
    </table>
  </div>
</form>
