<?php if(!empty($lista)): ?>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabela_padrao">
	<thead>
		<tr>
			<td width="2%"><input title="Selecionar Todas" type="checkbox" name="chall" id="chall" class="chall" onclick="$j('.item').attr('checked', this.checked);" /></td>
			<td width="8%">Produto</td>
			<td width="20%">Nome da Ficha</td>
			<td width="20%">Código de Barras</td>
			<td width="20%"><?php ghDigitalReplace($_sessao, 'Categoria'); ?></td>
			<td width="20%"><?php ghDigitalReplace($_sessao, 'Sub Categoria'); ?></td>
			<td width="10%">Data de Criação</td>
		</tr>
	</thead>
	<tbody>
		<?php foreach($lista as $i => $item): ?>
		<tr>
			<td>
				<input type="checkbox" name="item" id="item" class="item" value="<?php echo $item['ID_FICHA']; ?>" />
				<textarea style="display:none"><?php echo json_encode($item); ?></textarea>
			</td>
			<td style="text-align:center">
				<?php
				if( !empty($item['FILE_ID'])) {
					echo '<img height="50" src="', base_url(), 'img.php?img_id=', $item['FILE_ID'], '" />';
				}
				?>
			</td>
			<td><?php echo $item['NOME_FICHA']; ?></td>
			<td><?php echo $item['COD_BARRAS_FICHA']; ?></td>
			<td><?php echo $item['DESC_CATEGORIA']; ?></td>
			<td><?php echo $item['DESC_SUBCATEGORIA']; ?></td>
			<td><?php echo date('d/m/Y H:i', strtotime($item['DT_INSERT_FICHA'])); ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="7"><a href="#" onclick="Ficha.Filha.adicionar(); return false;" class="button"><span>Confirmar</span></a>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="7">Páginas: <?php paginacao($total, $limit, post('pagina',true), 5, "javascript:Ficha.Filha.efetuarPesquisa(%d)"); ?></td>
		</tr>
		<tr>
			<td colspan="7">Resultados encontrados: <?php echo $total; ?></td>
		</tr>
	</tfoot>
</table>
<?php else: ?>
Não há resultados para esta pesquisa
<?php endif; ?>