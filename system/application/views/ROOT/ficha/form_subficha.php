<div id="contentGlobal">
<div style="overflow-y:auto;overflow-x:hidden;" >
	<form id="form_subficha" method="post" action="<?php echo site_url('ficha/salvar/'); ?>" name="form_subficha">
	
	<h1><?php ghDigitalReplace($_sessao, 'SubFichas'); ?></h1> 
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="1"><?php ghDigitalReplace($_sessao, 'Código de Barras'); ?>:</td>
			<td colspan="2"><?=form_input(array('name' => 'COD_BARRAS_FICHA'), (isset($subficha['COD_BARRAS_FICHA'])) ? $subficha['COD_BARRAS_FICHA'] : null, 'id="CodBarras1" style="width:300px;"');?></td>
			<td colspan="1">
				<a class="button" id="btnFormPesquisaSubFicha" onclick="adicionarObjeto();" title="Adicionar objeto"><span>Adicionar <?php ghDigitalReplace($_sessao, 'Objetos'); ?></span></a>
			</td>
		</tr>
		<tr id="objetosSubFicha">
			<td colspan="4">
				<table width="100%" border="1" cellspacing="0" cellpadding="0" class="lista_objetosSubFicha ListaSelecionados" style="display:none; margin-bottom: -9px; background-color: #e5e4e2;">
					  <tr>
						    <td style="width: 60px; padding: 0px;">Produto</td>
						    <td style="width: 185px; padding: 0px;">Nome </td>
						    <td style="width: 55px; padding: 0px;">Tipo</td>
						    <td style="width: 60px; padding: 0px;"><?php ghDigitalReplace($_sessao, 'Categoria'); ?></td>
						    <td style="width: 184px; padding: 0px;"><?php ghDigitalReplace($_sessao, 'Sub-Categoria'); ?></td>
						    <td style="width: 48px; padding: 0px;">Principal</td>
							<? if($podeExcluirObjeto): ?>
						    <td style="width: 38px; padding: 0px;">Excluir</td>
					        <? endif; ?>
					  </tr>
				 </table>
			 </td>
		 </tr>
		 <tr>
			<td colspan="4">
				<table width="100%" border="1" cellspacing="0" cellpadding="0" id="lista_objetosSubFicha" class="lista_objetosSubFicha ListaSelecionados" style="display:none">
	  <?php
	  if (!empty($_POST['objetos'])) {
				$objetos = &$_POST['objetos'];
				foreach($objetos as $item) {
					$cont = $item['FILE_ID'];
					echo '' .
					'<tr class="alter">' .
						'<td style="width: 60px; padding: 0px; text-align: center;" class="infoObjeto">' .
							'<a href="'.base_url().'img.php?img_id='.$item['FILE_ID'].'&rand='.rand().'&img_size=big&a.jpg" class="jqzoom">'.
							'<img src="'.base_url().'img.php?img_id='.$item['FILE_ID'].'" width="50" border="0" class="imgThumb">'.
							'</a>'.
							'<input class="inputIdObjeto2" type="hidden" name="objetos['.$cont.'][ID_OBJETO]" value="' . val($item, 'ID_OBJETO') . '" />' .
							'<input class="inputIdSubFicha" type="hidden" name="objetos['.$cont.'][ID_SUBFICHA]" value="' . val($item, 'ID_FICHA') . '" />' .
							'<input class="inputPrincipal" type="hidden" name="objetos['.$cont.'][PRINCIPAL]" value="' . val($item, 'PRINCIPAL') . '" />' .
							'<input type="hidden" name="objetos['.$cont.'][FILENAME]" value="' . val($item, 'FILENAME') . '" />' .
							'<input type="hidden" name="objetos['.$cont.'][PATH]" value="' . val($item, 'PATH'). '" />' .
							'<input type="hidden" name="objetos['.$cont.'][FILE_ID]" value="' . val($item, 'FILE_ID'). '" />' .
							'<input type="hidden" name="objetos['.$cont.'][ID_TIPO_OBJETO]" value="' . val($item, 'DESC_TIPO_OBJETO') . '" />' .
							// DADOS GERAIS
							'<input type="hidden" name="objetos['.$cont.'][CODIGO]" value="' . val($item, 'CODIGO') . '" />' .
							'<input type="hidden" name="objetos['.$cont.'][ID_MARCA]" value="' . val($item, 'DESC_MARCA') . '" />' .
							'<input type="hidden" name="objetos['.$cont.'][DATA_UPLOAD]" value="' . val($item, 'DATA_UPLOAD') . '" />' .
							'<input type="hidden" name="objetos['.$cont.'][ORIGEM]" value="' . val($item, 'ORIGEM') . '" />' .
							'<input type="hidden" name="objetos['.$cont.'][ID_TIPO_OBJETO]" value="' . val($item, 'DESC_TIPO_OBJETO') . '" />' .
							'<input type="hidden" name="objetos['.$cont.'][ID_CATEGORIA]" value="' . val($item, 'DESC_CATEGORIA') . '" />' .
							'<input type="hidden" name="objetos['.$cont.'][ID_SUBCATEGORIA]" value="' . val($item, 'DESC_UBCATEGORIA') . '" />' .
							'<input type="hidden" name="objetos['.$cont.'][KEYWORDS]" value="' . val($item, 'KEYWORDS') . '" />' .
							'<input type="hidden" name="objetos['.$cont.'][ID_FICHA_PRODUTO]" value="' . val($item, 'ID_FICHA_PRODUTO') . '" />';

						if( !empty($item['fichas']) ){
							foreach($item['fichas'] as $idficha) {
								echo '<input class="codigos_objetos" type="hidden" name="objetos[', $cont, '][fichas][]" value="', $idficha, '" />';
							}
						}

						echo '</td>' .
						'<td style="width: 185px; padding: 0px;text-align: left;">' . str_replace(' ', '&nbsp;', val($item,'FILENAME')) . '</td>' .
						'<td style="width: 55px; padding: 0px;">' . val($item, 'DESC_TIPO_OBJETO') . '</td>' .
						'<td style="width: 60px; padding: 0px;">' . val($item, 'DESC_CATEGORIA'). '</td>' .
						'<td style="width: 184px; padding: 0px;">' . val($item, 'DESC_SUBCATEGORIA') . '</td>';

						if($podeAlterarPrincipal){
							echo '<td style="width: 48px; text-align: center; padding: 0px;"><input type="radio" class="objSubFicha" fileID="' . val($item, 'FILE_ID') . '" id="' . val($item, 'ID_OBJETO') . '" value="' . val($item, 'PRINCIPAL') . '" name="SUBFICHA_PRINCIPAL"';
							checked('SUBFICHA_PRINCIPAL', val($item, 'PRINCIPAL'));
							echo ' /></td>';
						}else{
							echo '<td style="width: 48px; text-align: center; padding: 0px;"><input type="hidden" class="objSubFicha" fileID="' . val($item, 'FILE_ID') . '" id="' . val($item, 'ID_OBJETO') . '" value="' . val($item, 'PRINCIPAL') . '" name="SUBFICHA_PRINCIPAL"';
							echo ' />'.((!empty($item['PRINCIPAL']) && $item['PRINCIPAL'] == $_POST['PRINCIPAL']) ? 'Sim' : 'Não').'</td>';
						}
					 if( $podeExcluirObjeto ){
						echo '<td style="width: 38px; text-align: center; padding: 0px;"><a href="#" onclick="Ficha.removerObjeto(this); return false;"><img src="' . base_url().THEME.'img/trash.png" border="0" /></a></td>';
					 }

					 echo '</tr>';
					 $cont++;
			}
		}
	  ?>
	  		</table>
			</td>
		</tr>
		<tr>
			<td colspan="1">
				<select id="Campo1">
					<option value="">[Selecione]</option>
					<?php echo montaOptions($campos, 'ID_CAMPO','NOME_CAMPO'); ?>
				</select>
			</td>
			<td colspan="2">
				<textarea id="Valor1" rows="1" style="width:300px;"></textarea>
			</td>
			<td colspan="1">
				<? if($podeIncluirCampo): ?>
					<a class="button" href="javascript:" onClick="insRowSubficha('tbl_subficha','<?= base_url() ?>')" accesskey="a" title="Adicionar Campo"><span>Adicionar Campo</span></a>
				<? endif; ?>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="1">
				Campos Relacionados:
			</td>
			<td colspan="2">
				<select name="relacionado[]" id="relacionado1" style="width:314px;height:100px;" multiple>
					<?php foreach($relacionados as $rel): ?>
						<option value="<?php echo $rel['ID_CAMPO_FICHA']?>" <?php if(is_numeric($rel['ID_FICHA2'])){echo "selected";}?>><?php echo $rel['VALOR_CAMPO_FICHA'] ?></option>
					<?php endforeach; ?>
				</select>
			</td>
			<td colspan="1"></td>
		</tr>
		<tr>
			<td colspan="4">
				<div id="contentListagemFicha">
				<input type='hidden' value="<?php post('ID_FICHA'); ?>" name="ID_FICHA" id="ID_FICHA">
				<input type='hidden' value="<?php if(isset($subficha['ID_FICHA'])){echo $subficha['ID_FICHA'];} ?>" name="ID_SUBFICHA" id="ID_SUBFICHA">
				
				<table border="0" cellpadding="0" cellspacing="0" width="100%" id="tbl_subficha" class="ListagemFicha">
					<tr style = "background-color: #EDECEC;">
						<td width="30%">
							Campo
						</td>
						<td width="30%">
							Conte&uacute;do
						</td>
						<td width="20%">
							Principal
						</td>
						<td width="20%">
							<? if($podeExcluirCampo): ?>Excluir<? endif; ?>
						</td>
				 	</tr>
					<tr id="modelo_pv_subficha">
						<td>
					    	<span class="lbl_subficha_campo"></span>
							<input type="hidden" class="campo1" name="campo[]" value=""/>
							<input type="hidden" class="idcampo1" name="idcampo[]" value=""/>
					    </td>
						<td>
							<span class="lbl_subficha_valor"></span>
							<textarea style="display:none" name="valor[]" value="" class="valor1" ></textarea>
						</td>
						<td>
					    	<input type="radio" name="principal" class="principal1" value="">
					    </td>
						<td>
							<? if($podeExcluirCampo): ?>
								<a class="button" href="javascript:" onClick='removeCampo(this);' value='Delete'><span>Excluir</span></a>
							<? endif; ?>
						</td>
					</tr>

					<?php if(!empty($_POST['campo'])): ?>
						<?php foreach($_POST['campo'] as $idx => $valor): ?>
							<tr>
							    <td>
							    	<?php echo $_POST['campo'][$idx]; ?>
									<input type="hidden" class="campo1" name="campo[]" value="<?php echo $_POST['campo'][$idx]; ?>" />
									<input type="hidden" class="idcampo1" name="idcampo[]" value="<?php echo $_POST['id_campo'][$idx]; ?>"/>
							    </td>
								<td>
									<?php echo str_replace("\n",'<br />',$_POST['valor'][$idx]); ?>
									<textarea style="display:none" name="valor[]" value="<?php echo $_POST['valor'][$idx]; ?>" ><?php echo $_POST['valor'][$idx]; ?></textarea>
							    </td>
								<td>
							    	<input type="radio" name="principal" class="principal1" value="<?php echo $_POST['id_campo'][$idx]; ?>" <?php if($_POST['principal'][$idx] == 1){ echo "checked"; } ?>>
							    </td>
					            <td style="text-align:center;">
					            	<? if($podeExcluirCampo): ?>
					            		<a class="button" href="javascript:" onClick='removeCampo(this);' value='Delete'><span>Excluir</span></a>
					            	<? endif; ?>
					            </td>
					 	  	</tr>
						<?php endforeach;?>
					<?php endif; ?>

					</table>
				</div> <!-- Final de content listagem ficha -->
				
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<a class="button" href="javascript:;" onclick="$j('#form_subficha').submit();"><span>Salvar</span></a>
				<a class="button" href="javascript:;" onclick="closeMessage();"><span>Cancelar</span></a>
			</td>
		</tr>
	</table>

	<div class="clearBoth"></div>
	
	</form>
</div>

</div> <!-- Final de content Global -->

<script type="text/javascript">

	// INSERE CAMPO NA SUBFICHA
	function insRowSubficha(id,url){
		var idCampoValue = $j('#Campo1').val();
		var campoValue = $j('#Campo1 :selected').text();
		var valorValue = $j('#Valor1').val();
	
		var existe = false;
		
		$j('.campo1').each(function(){
			if($j(this).val() == campoValue) {
				alert("Não é permitido inserir campos repetidos.");
				existe = true;
			}
		});
	
		if(existe){
			return false;
		}
	
		if(campoValue != '' && valorValue != ''){
			var item = $j(modelo_pv_subficha.clone());
			item.find('.campo1').val(campoValue);
			item.find('.valor1').val(valorValue);
			item.find('.idcampo1').val(idCampoValue);
	
			item.find('.lbl_subficha_campo').html(campoValue.replace('\n', '<br />'));
			item.find('.lbl_subficha_valor').html(valorValue.replace(/\n/g, '<br />'));
			item.find('.principal1').val(idCampoValue);
	
			$j('#'+id).append(item);
	
			$j('#Campo1').val('');
			$j('#Valor1').val('');
		}
	
	}

	function adicionarObjeto(){
		displayMessagewithparameter('<?php echo base_url().index_page().'/ficha/form_pesquisa/'; ?>', 800, 600, function(){
			$j('#PERIODO_INICIAL,#PERIODO_FINAL').datepicker({dateFormat: 'dd/mm/yy',showOn: 'button', buttonImage: '<?= base_url().THEME ?>img/calendario.png', buttonImageOnly: true});
		},2);

	}
	
	var modelo_pv_subficha = null;
	
 	 $j(function(){

 		$j('.objSubFicha').each( function(){
			if($j(this).val() == 1){
				$j(this).attr('checked','checked');
			}
 		}); 		 
 	 	 
 		var idFicha1 = '<?php post('ID_FICHA'); ?>';
		modelo_pv_subficha = $j('#modelo_pv_subficha').clone();
		$j('#modelo_pv_subficha').remove();

		<?php if(isset($_POST['objetos']) && !empty($_POST['objetos'])){?>
			$j('.lista_objetosSubFicha').css('display','');
		<?php }?>

	});
	$j("#btnFormPesquisaSubFicha").on('click', function(e) {
	});

	//removemos todos os attr que estão marcados como checked e deixamos somente do que foi selecionado por ultimo
	$j('input[name=SUBFICHA_PRINCIPAL]').on('change',function(e) {
		$j('input[name=SUBFICHA_PRINCIPAL]').val('0');
		$j('input[name=SUBFICHA_PRINCIPAL]').removeAttr('checked');
		$j(this).val('1');
		$j(this).attr('checked','checked');
	});

	// SALVA A SUBFICHA
	$j("#form_subficha").submit(function() {
		if($j("#CodBarras1").val() != ""){
			//quantos estão checados
			var chk = $j(".objSubFicha:checked").length;
			//quantos objetos possuem na tela
			var objs = $j(".objSubFicha").length;

			//verificamos se existe algum objeto selecionado
		    if (objs != 0 && chk != 1) {
			    alert("Selecione um objeto principal.");
			    return false;
		    }
			$j.post(
				"<?php echo site_url('json/subficha/salvar/'); ?>", 
				$j("#form_subficha").serialize(),
				function(data){
					try{
						
						var json = $j.parseJSON(data);
						if($j('.lista_objetosSubFicha').length > 0) {
						    //pegamos o id o objeto marcado como principal
							var idPrincipal = $j('input[name=SUBFICHA_PRINCIPAL]:checked').attr('id');
							var objetoSubFicha = new Array();
							//criamos um objeto jquery com as informações necessarias para salvar no banco
							$j('.infoObjeto').each( function(){
								var principal = 0;
								if(idPrincipal == $j(this).children().next().first().val()) {
									principal = 1;
								}
						        var subFicha = {
						        		idObjeto: $j(this).children().next().first().val(),
						        		idSubFicha: json['ID_FICHA'],
						        		principal: principal
						        };
						        objetoSubFicha.push(subFicha);
							});
			
							//caso todos os objetos tenham sido apagados passamos apenas o idSubficha para apagar todos objetos vinculados à subficha
							if(objetoSubFicha == ''){
								var subFicha = {
						        		idSubFicha:json['ID_FICHA']
						        };
						        objetoSubFicha.push(subFicha);
							}
							//ajax onde salvamos os objetos nas subfichas
							$j.ajax({
				                type: "POST",
				                url: '<?php echo base_url().index_page(); ?>/json/subficha/salvarObjetoSubficha/',
				                data: { post: objetoSubFicha }
				            });
						}
						
						var fileID = $j('input[name=SUBFICHA_PRINCIPAL]:checked').attr('fileID');
						if($j('#ID_SUBFICHA').val() == ""){
							adcionarSubficha(json);
						}
						else{
							atualizarSubficha(json);
						}
						//aqui atualizamos o src e o href com o preview da nova imagem principal sem dar refresh na página
						$j('#imgHREF_'+json['ID_FICHA']).fancybox();
						$j('#imgSRC_'+json['ID_FICHA']).attr('src','<?= base_url();?>img.php?img_id=' + fileID );
						$j('#imgHREF_'+json['ID_FICHA']).attr('href','<?= base_url();?>img.php?img_id=' + fileID + '&rand=<?= rand()?>&img_size=big&a.jpg');
						closeMessage();
					}
					catch(e){
						alert(data);
					}
				}
			);
			
			return false;
		}
		else{
			alert("Digite um Código de barras para a subficha.");
			$j("#CodBarras1").focus();
			return false;
		}
	});
</script>

