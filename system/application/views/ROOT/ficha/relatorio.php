<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>24\7 Inteligencia Digital - Retail</title>

		<script type="text/javascript" src="<?= base_url(); ?>js/date.format.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>js/jquery.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>js/jquery.bgiframe.min.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>js/jquery-fancybox.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>js/jquery-easing.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.mask.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.sortgrid.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery-ui.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.tools.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/ui.datepicker-pt-BR.js"></script>
		
		<script type="text/javascript">
			var base_url = '<?= base_url() ?>';
		
			$j = jQuery.noConflict();
			$j(function(){
				hideErrors();
				<?php
				$etapa = Sessao::get('nova_etapa');
				if(!empty($etapa)){
					echo 'alert("O job foi enviado para ', $etapa['DESC_ETAPA'], '");';
					Sessao::clear('nova_etapa');
				}
				?>
			});			
			
			function hideErrors(){
				$j('.phpError').hide();
				$j('<a href="javascript:void" class="linkErro" >...</a>').insertBefore('.phpError');
				$j('.linkErro').click(function(){
					$j(this).next('.phpError').toggle();
				});
			}

			<?php if(isset($alertActive) && $alertActive == true): ?>
				$j(function(){
					alert('<?= $alertMsg ?>');
				});
			<?php endif; ?>
			
		</script>
		
		<script type="text/javascript" src="<?= base_url(); ?>js/mootools.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>js/common.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>js/util.js"></script>	
		<script type="text/javascript" src="<?= base_url(); ?>js/swfobject.js"></script>	
		
		<?php foreach ( array_diff(scandir('js/classes'), array('.', '..', 'CVS','.svn')) as $file) : ?>
			<script type="text/javascript" src="<?=base_url()?>js/classes/<?=$file?>"></script>
		<?php endforeach; ?>
		
		<script type="text/javascript" src="<?= base_url(); ?>js/calendar.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>js/modal-message.js"></script>
		
        <link href="<?=base_url().THEME;?>css/style.css" rel="stylesheet" type="text/css"/>
        <link href="<?=base_url().THEME;?>css/jquery_ui/jquery.css" rel="stylesheet" type="text/css"/>		
		<link href="<?=base_url().THEME;?>css/modal-message.css" rel="stylesheet" type="text/css"/>
		<link href="<?=base_url().THEME;?>css/calendar.css" rel="stylesheet" type="text/css"/>
		<link href="<?=base_url().THEME;?>css/jquery-fancybox.css" rel="stylesheet" type="text/css"/>
        <link href="<?=base_url().THEME;?>css/jquery.sortgrid.css" rel="stylesheet" type="text/css"/>		
        <link href="<?=base_url().THEME;?>css/default.css" rel="stylesheet" type="text/css"/>
	 </head>	 
<body style="background-image:none">

<div id="conteudoGeral">
<div id="contentGlobal">
<div style="background-color:#CCCCCC; text-align:center"> RELATÓRIO DE FICHAS</div>

<? if(!empty($busca)): ?>

<br />
<h1>Filtros</h1>
<br />

<table width="457" border="1" class="tableSelection">
	<? foreach($busca as $key => $value): ?>
  <tr>
    <th width="126" scope="row"><?= htmlentities(utf8_decode($key)); ?></th>
    <td width="315">
    	<? if (!is_array($value)): ?>
			<?= htmlentities($value) ?>
        <? else:?>
        	<?= htmlentities(utf8_decode(implode(',',$value))); ?>
        <? endif; ?>
    </td>
  </tr>
  <? endforeach; ?>
  
</table>

<? endif; ?>
<p>&nbsp;</p>
<table width="100%" border="1" class="tableSelection">
  <thead>
  <tr>
    <th width="75" scope="col">Preview</th>
    <th width="210" scope="col">Nome</th>
    <th width="124" scope="col">Códigos</th>
    <th width="134" scope="col"><?php ghDigitalReplace($_sessao, 'Categoria'); ?></th>
    <th width="152" scope="col"><?php ghDigitalReplace($_sessao, 'SubCategoria'); ?></th>
    <th width="152" scope="col">Campos</th>
    <th width="264" scope="col">Descrição</th>
    <th width="71" scope="col">Situação</th>
    <th width="63" scope="col">Status</th>
  </tr>
  </thead>
  <tbody>
  <? foreach($fichas as $ficha): ?>
      <tr>
        <td><? if(isset($ficha['OBJETOS'])): ?>
				<? if(count($ficha['OBJETOS']) > 0):?>
					<? $fileid = $ficha['OBJETOS'][0]; ?>
                    Principal <br /><br />
                    <img src="<?= base_url() ?>img.php?img_id=<?= $fileid ?>&a.jpg" title="" width="70" border="0">
                    <br /><br />
                <? endif; ?>
                <? if(count($ficha['OBJETOS']) > 1):?>
                	<hr />
                    outras imagens <br /><br />
                <? endif; ?>
                <? for($i = 1; $i < count($ficha['OBJETOS']);$i++): ?>
					<? $fileid = $ficha['OBJETOS'][$i]; ?>
                    <img src="<?= base_url() ?>img.php?img_id=<?= $fileid ?>&a.jpg" title="" width="50" border="0">
                    <br />
                <? endfor; ?>
			<? endif; ?>
        </td>
        <td><?= $ficha['NOME_FICHA'] ?></td>
        <td style="text-align:left">
        <ul>
		<li><strong>Barras : </strong> <?= $ficha['COD_BARRAS_FICHA'] ?></li>
        
        <? foreach($ficha['CODIGOS_REGIONAIS'] as $campo => $valor): ?>
        <li><strong><?= htmlentities(utf8_decode($campo)) ?></strong> : <?= htmlentities(utf8_decode($valor)) ?></li>
        <? endforeach; ?>
        </ul>
        </td>
        <td><?= $ficha['DESC_CATEGORIA'] ?></td>
        <td><?= $ficha['DESC_SUBCATEGORIA'] ?></td>
        <td style="text-align:left"><? if(isset($ficha['CAMPOS'])): ?>
        		<ul>
				<? foreach($ficha['CAMPOS'] as $campo => $valor): ?>
                    <li><strong><?= htmlentities(utf8_decode($campo)) ?></strong> : <?= (($valor)) ?></li>
                <? endforeach; ?>
                </ul>
			<? endif; ?>
        </td>
        <td><?= $ficha['OBS_FICHA'] ?></td>
        <td><?= $ficha['DESC_APROVACAO_FICHA'] ?></td>
        <td><?= $ficha['FLAG_ACTIVE_FICHA'] == 1 ? 'Ativo' : 'Inativo' ?></td>
      </tr>
  <? endforeach; ?>
  </tbody>
  <tfoot><tr>
        <td colspan="9">TOTAL: <?= count($fichas); ?></td>
      </tr></tfoot>
</table>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>







<?php $this->load->view("ROOT/layout/footer") ?>
