<form id="busca_fichas">
	<table class="tabela_padrao" width="100%">
		<tr>
			<td colspan="2">
				<img src="<?php echo base_url().THEME ?>img/alerta-amarelo.jpg" width="22" height="18" alt="Atenção" />
				Somente serão pesquisadas fichas aprovadas, ativas e não temporárias.
			</td>
		</tr>
		<tr>
			<td width="19%">Nome da Ficha</td>
			<td width="81%"><input name="NOME_FICHA" type="text" id="NOME_FICHA2" size="40"></td>
		</tr>
		<tr>
			<td>Código de Barras</td>
			<td><input name="COD_BARRAS_FICHA" type="text" id="COD_BARRAS_FICHA2" size="40"></td>
		</tr>
		<tr>
			<td>Código Regional</td>
			<td><input name="CODIGO_REGIONAL" type="text" id="CODIGO_REGIONAL2" size="40"></td>
		</tr>
		<tr>
			<td><?php ghDigitalReplace($_sessao, 'Categoria'); ?></td>
			<td><select name="ID_CATEGORIA" id="ID_CATEGORIA2" onchange="carregaSubcategorias();">
			<option value=""></option>
			<?php echo montaOptions($categorias,'ID_CATEGORIA','DESC_CATEGORIA',''); ?>
			</select></td>
		</tr>
		<tr>
			<td><?php ghDigitalReplace($_sessao, 'Subcategoria'); ?></td>
			<td><select name="ID_SUBCATEGORIA" id="ID_SUBCATEGORIA2">
			<option value=""></option>
			</select></td>
		</tr>
		<tr>
			<td>Fabricante</td>
			<td><select name="ID_FABRICANTE" id="ID_FABRICANTE2" onchange="carregaMarcas();">
				<option value=""></option>
				<?php echo montaOptions($fabricantes,'ID_FABRICANTE','DESC_FABRICANTE',''); ?>
			</select></td>
		</tr>
		<tr>
			<td>Marca</td>
			<td>
                <select name="ID_MARCA" id="ID_MARCA2">
	              <option value=""></option>
	              <?php echo montaOptions($marcas,'ID_MARCA','DESC_MARCA',POST('ID_MARCA',TRUE)); ?>
	            </select>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><a href="#" onclick="Ficha.Filha.efetuarPesquisa(0); return false;" class="button"><span>Buscar</span></a>&nbsp;</td>
		</tr>
	</table>
	
	<div id="resultadoBusca">Pesquisa ainda n&atilde;o efetuada.</div>
</form>
<script type="text/javascript">
function carregaSubcategorias(){
	var cid = $j('#ID_CATEGORIA2').val();
	var tar = $('ID_SUBCATEGORIA2');
	clearSelect(tar, 1);
	
	if( cid != '' ){
		$j.post(util.options.site_url+'json/admin/getSubcategoriasByCategoria','id='+cid,function(json){
			montaOptions(tar, json, 'ID_SUBCATEGORIA','DESC_SUBCATEGORIA');
		},'json');
	}
}

function carregaMarcas(){
	var cid = $j('#ID_FABRICANTE2').val();
	var tar = $('ID_MARCA2');
	clearSelect(tar, 1);
	
	if( cid != '' ){
		$j.post(util.options.site_url+'json/fabricante/getMarcas','id='+cid,function(json){
			montaOptions(tar, json, 'ID_MARCA','DESC_MARCA');
		},'json');
	}
}
</script>