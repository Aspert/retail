<?php if(!empty($lista)): ?>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabela_padrao">
	<thead>
		<tr>
			<td width="30%">Código de Barras</td>
			<td width="20%"><?php ghDigitalReplace($_sessao, 'Categoria'); ?></td>
			<td width="20%"><?php ghDigitalReplace($_sessao, 'Sub Categoria'); ?></td>
			<td width="20%">Data de Criação</td>
			<td width="10%"></td>
		</tr>
	</thead>
	<tbody>
		<?php foreach($lista as $i => $item): ?>
		<tr>
			<td><?php echo $item['COD_BARRAS_FICHA']; ?></td>
			<td><?php echo $item['DESC_CATEGORIA']; ?></td>
			<td><?php echo $item['DESC_SUBCATEGORIA']; ?></td>
			<td><?php echo date('d/m/Y H:i', strtotime($item['DT_INSERT_FICHA'])); ?></td>
			<td><a class="button" href="javascript:" onClick="vincularSubficha(<?php echo $item['ID_FICHA']; ?>)" title="Adicionar"><span>Adicionar</span></a></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="7">Páginas: <?php paginacao($total, $limit, post('pagina',true), 5, "javascript:Ficha.Filha.efetuarPesquisa(%d)"); ?></td>
		</tr>
		<tr>
			<td colspan="7">Resultados encontrados: <?php echo $total; ?></td>
		</tr>
	</tfoot>
</table>
<?php else: ?>
Não há resultados para esta pesquisa
<?php endif; ?>