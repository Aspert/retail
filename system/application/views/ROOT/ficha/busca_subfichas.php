<form id="busca_fichas">
	<table class="tabela_padrao" width="100%">
		<tr>
			<td colspan="2">
				<img src="<?php echo base_url().THEME ?>img/alerta-amarelo.jpg" width="22" height="18" alt="Atenção" />
				Somente serão pesquisadas subfichas aprovadas, ativas e não temporárias.
			</td>
		</tr>
		<tr>
			<td><?php ghDigitalReplace($_sessao, 'Código de Barras'); ?></td>
			<td><input name="COD_BARRAS_FICHA" type="text" id="COD_BARRAS_FICHA2" size="40"></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><a href="#" onclick="Ficha.Subficha.efetuarPesquisa(0); return false;" class="button"><span>Buscar</span></a>&nbsp;</td>
		</tr>
	</table>
	
	<div id="resultadoBusca">Pesquisa ainda n&atilde;o efetuada.</div>
</form>