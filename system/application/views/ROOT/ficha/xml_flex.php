<?php
function createXml($arr1, $arr2, $arr3, $id_ficha){
	$xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';//.chr(13);
	$xml .= '<dados ID_FICHA=\''.$id_ficha.'\'>';
	
	if(is_array($arr1)){
		$xml .= '<textos>';
		foreach($arr1 as $key=>$arr_texto){
			$xml .= '<texto label=\''.htmlspecialchars($arr_texto['LABEL_CAMPO_FICHA']).'\' valor=\''.htmlspecialchars($arr_texto['VALOR_CAMPO_FICHA']).'\'/>';//.chr(13);
		}
		$xml .= '</textos>';
	}
	
	if(is_array($arr2)){
		$xml .= '<imagens>';
		foreach($arr2 as $key2=>$arr_image){
			$xml .= '<objeto ID_OBJ_FICHA=\''.$arr_image['xinet'][0]['FileID'].'\' FILE_OBJ_FICHA=\''.$arr_image['OBJ_FICHA']['FILE_OBJ_FICHA'].'\' PATH_OBJ_FICHA=\''.$arr_image['OBJ_FICHA']['PATH_OBJ_FICHA'].'\' DESC_TIPO_OBJETO=\''.$arr_image['OBJ_FICHA']['DESC_TIPO_OBJETO'].'\'/>';
		}
		$xml .= '</imagens>';
	}
	
	if(is_array($arr3)){
		$xml .= '<layout tipo_layout="'.$arr3[0]['ID_TIPO_LAYOUT_FICHA'].'">';
		foreach($arr3 as $arr_layout){
			
			if($arr_layout['TIPO_ELEM_FICHA'] == 'image'){
				$xml .= '<objeto id="'.$arr_layout['ID_OBJ_FICHA'].'" type="'.$arr_layout['TIPO_ELEM_FICHA'].'" index="'.$arr_layout['INDEX_ELEM_FICHA'].'" x="'.$arr_layout['X_ELEM_FICHA'].'" y="'.$arr_layout['Y_ELEM_FICHA'].'" width="'.$arr_layout['W_ELEM_FICHA'].'"  height="'.$arr_layout['H_ELEM_FICHA'].'" rotation="'.$arr_layout['R_ELEM_FICHA'].'" />';
			}else{
				$xml .= '<texto id="'.$arr_layout['ID_OBJ_FICHA'].'" type="'.$arr_layout['TIPO_ELEM_FICHA'].'" index="'.$arr_layout['INDEX_ELEM_FICHA'].'" color="'.$arr_layout['COLOR_ELEM_FICHA'].'"  x="'.$arr_layout['X_ELEM_FICHA'].'" y="'.$arr_layout['Y_ELEM_FICHA'].'" width="'.$arr_layout['W_ELEM_FICHA'].'"  height="'.$arr_layout['H_ELEM_FICHA'].'" rotation="'.$arr_layout['R_ELEM_FICHA'].'" fontFamily="'.$arr_layout['FONT_FAMILY_ELEM_FICHA'].'" fontSize="'.$arr_layout['FONT_SIZE_ELEM_FICHA'].'" fontWeight="'.$arr_layout['FONT_WEIGHT_ELEM_FICHA'].'" fontStyle="'.$arr_layout['FONT_STYLE_ELEM_FICHA'].'"/>';
			}
		}
		$xml .= '</layout>';
	}else if(is_int($arr3)){
		$xml .= '<layout tipo_layout="'.$arr3.'"></layout>';
	}
	
	$xml .= '</dados>';
	echo $xml;
}

#print '<pre>';print_r($ficha['ID_FICHA']); exit;

header('content-type:text/xml');
createXml($campo_ficha, $objetos, $layout, $ficha['ID_FICHA']);
?>