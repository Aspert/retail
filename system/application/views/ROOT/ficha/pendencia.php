
<form id="form_ficha" method="post" action="<?php echo site_url('ficha/salvar/'.$ID_FICHA); ?>" name="form_ficha">
<h1><?php ghDigitalReplace($_sessao, 'Pendências da Ficha'); ?></h1>
<table width="300"   >
    <tr>
    	<td width="86%" style="text-align:left">
    		<textarea  maxlength="800" rows="6" cols="53" id="PENDENCIAS_DADOS_BASICOS" name="PENDENCIAS_DADOS_BASICOS"><?= $PENDENCIA ?></textarea>
    	</td>
    </tr>
    <tr style="text-align:top">
    	<td style="text-align:top">
    		<a class="button" onclick="salvar()" href="javascript:close()">
			<span>Salvar</span>
			</a>
    	</td>
    </tr>	
</table>
</form>
<script>

function salvar(){
	var id_fichas = "<?php echo $ID_FICHA?>";
	var pendencias = $j('#PENDENCIAS_DADOS_BASICOS').val();
	
	$j.post(
			'<?php echo base_url().index_page(); ?>/json/ficha/updatePendencia', 
			{idFicha : id_fichas, pendencia : pendencias},
			function(data){
				if (data == 1) {
					alert ("Pendência salva com sucesso");
				} else {
					alert ("Não foi inserida nenhuma informação");
				}
					// fechando a janela atual ( popup )  
				$j('#divModal').dialog("close");
				$j.post('<?php echo site_url('json/ficha/getFichaByIdComRetorno/');?>/'+id_fichas,
						function(data) {
							if (data == 1) {
								$j('#pendencias_'+id_fichas).removeClass('apagado').addClass('acesso');
							} else {
								$j('#pendencias_'+id_fichas).removeClass('acesso').addClass('apagado');
							}
					});
			}
			
	);
}

</script>