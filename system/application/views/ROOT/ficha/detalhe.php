<?php if(post('TIPO_FICHA', TRUE) == 'SUBFICHA'): ?>
	<h1>Detalhes de <?php ghDigitalReplace($_sessao, 'Subficha'); ?></h1>
<?php else: ?>
	<h1>Detalhes de <?php ghDigitalReplace($_sessao, 'ficha'); ?></h1>
<?php endif;?>


<table width="750" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="147">Tipo de ficha</td>
        <td width="642">
			<?php echo post('TIPO_FICHA', TRUE); ?>
        </td>
    </tr>
    <tr>
        <td>Nome <?php ghDigitalReplace($_sessao, 'ficha'); ?><span class="obrigatorio"><?php if(strtoupper(post('DESC_CLIENTE',true)) != 'HERMES'){ echo '*'; } ?></span></td>
        <td><?php post('NOME_FICHA'); ?></td>
    </tr>
	<?php
		if(!empty($_POST['codigos'])): 
			foreach($_POST['codigos'] as $item):
	?>
    <tr>
        <td>Código <?php echo $item['NOME_REGIAO']; ?></td>
        <td><?php echo $item['CODIGO']; ?></td>
    </tr>
	<?php
			endforeach;
		endif;
	?>
    <tr>
        <td><?php ghDigitalReplace($_sessao, 'Código de Barras'); ?><span class="obrigatorio"></span></td>
        <td><?php post('COD_BARRAS_FICHA'); ?></td>
    </tr>
    <tr>
        <td><?php ghDigitalReplace($_sessao, 'Cliente'); ?><span class="obrigatorio">*</span></td>
        <td><?php post('DESC_CLIENTE'); ?></td>
    </tr>
    <tr>
        <td><?php ghDigitalReplace($_sessao, 'Categoria'); ?><span class="obrigatorio">*</span></td>
        <td><?php post('DESC_CATEGORIA'); ?></td>
    </tr>
    <tr>
        <td><?php ghDigitalReplace($_sessao, 'SubCategoria'); ?><span class="obrigatorio">*</span></td>
        <td><?php post('DESC_SUBCATEGORIA'); ?></td>
    </tr>
    <tr>
        <td>Fabricante</td>
        <td><?php post('DESC_FABRICANTE'); ?></td>
    </tr>
    <tr>
        <td>Marca</td>
        <td><?php post('DESC_MARCA'); ?></td>
    </tr>
    <tr>
        <td>Modelo</td>
        <td><?php post('MODELO_FICHA'); ?></td>
    </tr>
    <tr>
        <td>Data de Início</td>
        <td><?php echo post('DATA_INICIO',TRUE) != '' ? format_date(post('DATA_INICIO',TRUE),'d/m/Y') : ''; ?></td>
    </tr>
    <tr>
        <td>Data de Validade</td>
        <td><?php echo post('DATA_VALIDADE',TRUE) != '' ? format_date(post('DATA_VALIDADE',TRUE),'d/m/Y') : ''; ?></td>
    </tr>
    <tr>
        <td>Status aprovação:</td>
        <td><?= empty($_POST['aprovacao']['DESC_APROVACAO_FICHA']) ? '' : $_POST['aprovacao']['DESC_APROVACAO_FICHA']; ?></td>
    </tr>
    <tr>
        <td>Usuário aprovação:</td>
        <td><?= empty($_POST['aprovacao']['NOME_USUARIO']) ? '' : $_POST['aprovacao']['NOME_USUARIO']; ?></td>
    </tr>
    <tr>
        <td>Temporário</td>
        <td><?= $_POST['FLAG_TEMPORARIO'] == 1 ? 'Sim' : 'N&atilde;o'; ?></td>
    </tr>
    <tr>
        <td>Status</td>
        <td><?= $_POST['FLAG_ACTIVE_FICHA'] == 1 ? 'Ativo' : 'Inativo'; ?></td>
    </tr>
    <tr>
    	<td colspan="2">&nbsp;</td>
    </tr>
</table>

<? if (!empty($_POST['campos'])): ?>

<h1>Lista de campos</h1>

<div id="tableObjeto">
<table border="0" cellpadding="0" cellspacing="0" class="tableSelection" width="750">
<thead>
  <tr>
    <td width="40%">Campo</td>
    <td width="40%">Conte&uacute;do</td>
	<?php if(post('TIPO_FICHA', TRUE) == 'SUBFICHA'): ?>
		<td width="20%">Principal</td>
	<?php endif;?>
  </tr>
</thead>
<? foreach($_POST['campos'] as $c): ?>
  <tr>
    <td><? echo $c['LABEL_CAMPO_FICHA']?></td>
    <td><? echo quebraLinha($c['VALOR_CAMPO_FICHA'])?></td>
	<?php if(post('TIPO_FICHA', TRUE) == 'SUBFICHA'): ?>
		<td><? if($c['PRINCIPAL'] == 1){ echo "Sim"; }else{ echo "Não"; }?></td>
	<?php endif;?>
  </tr>
<? endforeach; ?>
</table>
</div>

<? endif;?>

<? if (!empty($_POST['objetos'])): ?>
<h1>&nbsp;</h1>
<h1>Lista de objetos</h1>

<div id="tableObjeto">
<table border="0" cellpadding="0" cellspacing="0" class="tableSelection" width="750">
<thead>
  <tr>
    <td>Produto</td>
    <td>Nome </td>
    <td>Marca</td>
    <td>Data Cria&ccedil;&atilde;o</td>
    <td>Origem</td>
    <td>Tipo</td>
    <td><?php ghDigitalReplace($_sessao, 'Categoria'); ?></td>
    <td><?php ghDigitalReplace($_sessao, 'Sub-Categoria'); ?></td>
    <td>Keywords</td>
  </tr>
</thead>
  <?php
  
  if (!empty($_POST['objetos'])) {
			$cont=0;
			$objetos = &$_POST['objetos'];
			
			foreach($objetos as $item) {
			
				echo '' .
				'<tr>' .
					'<td>' .
						'<a href="'.base_url().'img.php?img_id='.$item['FILE_ID'].'&rand='.rand().'&img_size=big&a.jpg" class="jqzoom2">'.
						'<img src="'.base_url().'img.php?img_id='.$item['FILE_ID'].'&rand='.(rand()).'&a.jpg" width="50" border="0">'.
						'</a>'.
					'</td>' .
					'<td>' . val($item,'FILENAME') . '</td>' .
					'<td>' . val($item, 'DESC_MARCA') . '</td>' .
					'<td>' . date('d/m/Y H:i:s', strtotime(val($item, 'DATA_UPLOAD'))) . '</td>' .
					'<td>' . val($item, 'ORIGEM') . '</td>' .
					'<td>' . val($item, 'DESC_TIPO_OBJETO') . '</td>' .
					'<td>' . val($item, 'DESC_CATEGORIA'). '</td>' .
					'<td>' . val($item, 'DESC_SUBCATEGORIA') . '</td>' .
					'<td>' . val($item, 'KEYWORDS') . '</td>';
				'</tr>';
				 $cont++;
			}
		}
  ?>
	
</table>
</div> <!-- Final de Table Objeto -->

<? endif; ?>
