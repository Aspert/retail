<?php $this->load->view("ROOT/layout/header") ?>

<h1><?php ghDigitalReplace($_sessao, 'Fichas'); ?></h1>
<div id="contentGlobal">
<div id="pesquisa_simples" <?=(isset($busca['tipo_pesquisa'])&& $busca['tipo_pesquisa'] == 'completa') ? 'style="display:none"':''?>> <?php echo form_open('ficha/lista');?>
	<?=form_hidden('tipo_pesquisa','simples');?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tablePesquisa">
		<tr>
			<td width="92"><?php if ( $podeEditar ) : ?>
				<?=anchor('ficha/form','<img src="'.base_url().THEME.'img/file_add.png" border="0" />',array('title'=>'Nova Ficha'))?>
				<?php endif; ?></td>
			<td width="24"></td>
			<td width="140" nowrap="nowrap">
			<div style="width: 500px;">
				<?php ghDigitalReplace($_sessao, 'Código de Barras'); ?>
				<?= form_input('COD_BARRAS_FICHA',(isset($busca['COD_BARRAS_FICHA'])) ? $busca['COD_BARRAS_FICHA'] : '');?>
				<?$pagina_atual = (isset($busca["pagina"])) ? $busca["pagina"] : 0;?>
				<img style="cursor:pointer;" title="Informe o código de barras completo ou partes deste código para a busca de fichas" src="<?php echo base_url().THEME.'img/information.png' ?>">

			</div>
			</td>
			<td width="4"></td>
			<td width="4"></td>
			<td width="4"></td>
			<td width="261"> Itens/P&aacute;gina:
				<select name="pagina" id="pagina" >
					<?php
						$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
						for($i=10; $i<=100; $i+=10){
							printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
						}
					?>
				</select></td>
			<td width="4"></td>
			<td width="244"></td>
			<td width="4"></td>
			<td width="53"><a class="button" title="Pesquisa" onclick="$j(this).closest('form').submit()"><span>Pesquisa</span></a></td>
			<td width="4"></td>
			<td width="254"><a class="button" title="Pesquisa Avan&ccedil;ada" onclick="new Util().pesquisaCompleta();$j.fn.sortgrid.options.form = '#pesquisa_completo form';"><span>Pesquisa Avan&ccedil;ada</span></a></td>
		</tr>
	</table>
	<div class="radiosbox">
		<?=form_checkbox(array('name'=>'ID_APROVACAO_FICHA[0]','value'=> '1','checked'=> (isset($busca['ID_APROVACAO_FICHA'][0]) && $busca['ID_APROVACAO_FICHA'][0]==1) ? TRUE: FALSE ));?>
		Aprovado
		<?=form_checkbox(array('name'=>'ID_APROVACAO_FICHA[1]','value'=> '3','checked'=> (isset($busca['ID_APROVACAO_FICHA'][1]) && $busca['ID_APROVACAO_FICHA'][1]==3) ? TRUE: FALSE ));?>
		Em aprovação
		<?=form_checkbox(array('name'=>'ID_APROVACAO_FICHA[2]','value'=> '2','checked'=> (isset($busca['ID_APROVACAO_FICHA'][2]) && $busca['ID_APROVACAO_FICHA'][2]==2) ? TRUE: FALSE ));?>
		Reprovado <?php echo form_close();?> </div>
</div>
<div id="pesquisa_completo" style="display:<?=(isset($busca['tipo_pesquisa'])&& $busca['tipo_pesquisa'] == 'completa') ? '':'none'?>"> <?php echo form_open('ficha/lista');?>
	<?=form_hidden('tipo_pesquisa','completa');?>
	<h1 class="align_right"><?php ghDigitalReplace($_sessao, 'Consulta de Fichas'); ?></h1>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="contentPcomplete">
		<tr>
			<td width="200"><?php ghDigitalReplace($_sessao, 'Nome da ficha'); ?>:</td>
			<td width="20">&nbsp;</td>
			<td><?=form_input('NOME_FICHA',(isset($busca['NOME_FICHA']) ) ? $busca['NOME_FICHA']: null );?></td>
		</tr>
		<tr>
			<td width="200">C&oacute;digo Regional:</td>
			<td>&nbsp;</td>
			<td><?= form_input('CODIGO_REGIONAL', (isset($busca['CODIGO_REGIONAL']) ) ? $busca['CODIGO_REGIONAL']: null );?></td>
		</tr>
		<tr>
			<td>Itens/P&aacute;gina:</td>
			<td>&nbsp;</td>
			<td><select name="pagina" id="pagina" class="pesquisa_select">
					<?php
				$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
				for($i=10; $i<=100; $i+=10){
					printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
				}
			?>
				</select></td>
		</tr>
		<tr>
			<td valign="top" style="vertical-align:top"><?php ghDigitalReplace($_sessao, 'Código de Barras'); ?>:</td>
			<td>&nbsp;</td>
			<td><div id="containerCodigos">
					<div id="modeloCodigo" class="campoCodigoBarras">
						<input id="modeloCodigo" name="CODIGOS_BARRA[]" type="text" size="40" />
						<img style="cursor:pointer;" title="Informe o código de barras completo para a busca de fichas" src="<?php echo base_url().THEME.'img/information.png' ?>">
						<a href="#" onclick="$j(this).closest('div').remove(); return false;"> <img src="<?php echo base_url(), THEME, 'img/alerta-vermelho.jpg'; ?>" align="absbottom" title="Remover Codigo" /> </a>
					</div>
						
					</div>
				<p><a href="#" onclick="adicionarCampoCodigo(''); return false;" class="button"><span>Adicionar Código</span></a></p></td>
		</tr>
		<tr>
			<td><?php ghDigitalReplace($_sessao, 'Cliente'); ?>:</td>
			<td>&nbsp;</td>
			<td><?php if(!empty($_sessao['ID_CLIENTE'])): ?>
				<?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?>
				<br />
				<br />
				<?php else: ?>
				<select name="ID_CLIENTE" id="ID_CLIENTE">
					<option value=""></option>
					<?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', !empty($busca['ID_CLIENTE']) ? $busca['ID_CLIENTE'] : ''); ?>
				</select>
				<br />
				<?php endif; ?></td>
		</tr>
		<tr>
			<td><?php ghDigitalReplace($_sessao, 'Categoria'); ?>:</td>
			<td>&nbsp;</td>
			<td><select name="ID_CATEGORIA" id="ID_CATEGORIA">
					<option value=""></option>
					<?php echo montaOptions($categorias,'ID_CATEGORIA','DESC_CATEGORIA', !empty($busca['ID_CATEGORIA']) ? $busca['ID_CATEGORIA'] : ''); ?>
				</select></td>
		</tr>
		<tr>
			<td><?php ghDigitalReplace($_sessao, 'SubCategoria'); ?>: </td>
			<td>&nbsp;</td>
			<td><select name="ID_SUBCATEGORIA" id="ID_SUBCATEGORIA">
					<option value=""></option>
					<?php echo montaOptions($subcategorias,'ID_SUBCATEGORIA','DESC_SUBCATEGORIA', !empty($busca['ID_SUBCATEGORIA']) ? $busca['ID_SUBCATEGORIA'] : ''); ?>
				</select></td>
		</tr>
		<tr>
			<td>Fabricante: </td>
			<td>&nbsp;</td>
			<td><?= form_dropdown('ID_FABRICANTE',$fabricantes, ( isset($busca['ID_FABRICANTE'])) ? $busca['ID_FABRICANTE'] : '' , "");?></td>
		</tr>
		<tr>
			<td>Marca:</td>
			<td>&nbsp;</td>
			<td><?= form_dropdown('ID_MARCA',$marcas, ( isset($busca['ID_MARCA'])) ? $busca['ID_MARCA'] : '' , "");?></td>
		</tr>
		<tr>
			<td><?php ghDigitalReplace($_sessao, 'Tipo de Ficha'); ?>:</td>
			<td>&nbsp;</td>
			<td><select name="TIPO_FICHA" id="TIPO_FICHA">
					<option value="">TODOS</option>
					<option value="PRODUTO" <?php echo val($busca,'TIPO_FICHA') == 'PRODUTO' ? ' selected="selected"' : ''; ?>>Produto</option>
					<option value="COMBO" <?php echo val($busca,'TIPO_FICHA') == 'COMBO' ? ' selected="selected"' : ''; ?>>Combo</option>
					<option value="SUBFICHA" <?php echo val($busca,'TIPO_FICHA') == 'SUBFICHA' ? ' selected="selected"' : ''; ?>><?php ghDigitalReplace($_sessao, 'SUBFICHA'); ?></option>
				</select></td>
		</tr>
		<tr>
			<td>Data de criação:</td>
			<td>&nbsp;</td>
			<td>
				<input type="text" name="DATA_INSERT_INICIO" id="DATA_INSERT_INICIO" size="10" maxlength="10" class="_calendar" value="<?php if( isset($busca['DATA_INSERT_INICIO']) ){ echo $busca['DATA_INSERT_INICIO']; } ?>" readonly="readonly" />
				&nbsp;&nbsp;até&nbsp;&nbsp;
				<input type="text" name="DATA_INSERT_FIM" id="DATA_INSERT_FIM" size="10" maxlength="10" class="_calendar" value="<?php if( isset($busca['DATA_INSERT_FIM']) ){ echo $busca['DATA_INSERT_FIM']; } ?>" readonly="readonly" />
			</td>
		</tr>
		<tr>
			<td>Status:</td>
			<td>&nbsp;</td>
			<td><?=form_radio(array('name'=>'FLAG_ACTIVE_FICHA','value'=> '1','checked'=> (isset($busca['FLAG_ACTIVE_FICHA']) && $busca['FLAG_ACTIVE_FICHA']==1) ? TRUE: FALSE ));?>
				Ativo
				<?=form_radio(array('name'=>'FLAG_ACTIVE_FICHA','value'=> '0','checked'=> (isset($busca['FLAG_ACTIVE_FICHA']) && $busca['FLAG_ACTIVE_FICHA']==0) ? TRUE: FALSE ));?>
				Inativo<br/></td>
		</tr>
		<tr>
			<td>Situação: </td>
			<td>&nbsp;</td>
			<td><?=form_checkbox(array('name'=>'ID_APROVACAO_FICHA[0]','value'=> '1','checked'=> (isset($busca['ID_APROVACAO_FICHA'][0]) && $busca['ID_APROVACAO_FICHA'][0]==1) ? TRUE: FALSE ));?>
				Aprovado
				<?=form_checkbox(array('name'=>'ID_APROVACAO_FICHA[1]','value'=> '3','checked'=> (isset($busca['ID_APROVACAO_FICHA'][1]) && $busca['ID_APROVACAO_FICHA'][1]==3) ? TRUE: FALSE ));?>
				Em aprovação
				<?=form_checkbox(array('name'=>'ID_APROVACAO_FICHA[2]','value'=> '2','checked'=> (isset($busca['ID_APROVACAO_FICHA'][2]) && $busca['ID_APROVACAO_FICHA'][2]==2) ? TRUE: FALSE ));?>
				Reprovado </td>
		</tr>
		<tr>
			<td>Somente com pendências:</td>
			<td>&nbsp;</td>
			<td><input name="PENDENTES" type="checkbox" id="PENDENTES" value="1"<?php echo val($busca,'PENDENTES') == 1 ? ' checked="checked"' : ''; ?> /></td>
		</tr>
		<tr>
			<td>Região:</td>
			<td>&nbsp;</td>
			<td><span id="regioes">
				<?php 
				if( empty($regioes) ){
					echo 'Escolha um cliente';
				} else {
					foreach($regioes as $regiao){
						printf('<input type="checkbox" name="REGIOES[]" value="%d" %s /> %s '
							, $regiao['ID_REGIAO']
							, !empty($busca['REGIOES']) && in_array($regiao['ID_REGIAO'], $busca['REGIOES']) ? 'checked="checked"' : ''
							, $regiao['NOME_REGIAO']
						);
					}
				}
				?>
				</span></td>
		</tr>
		<tr>
			<td>Temporário:</td>
			<td>&nbsp;</td>
			<td><input type="radio" name="FLAG_TEMPORARIO" id="FLAG_TEMPORARIO" value="1" <?php echo val($busca,'FLAG_TEMPORARIO') == 1 ? 'checked="checked"' : ''; ?> />
				Sim
				<input type="radio" name="FLAG_TEMPORARIO" id="FLAG_TEMPORARIO" value="0" <?php echo val($busca,'FLAG_TEMPORARIO') == '0' ? 'checked="checked"' : ''; ?> />
				Não </td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><a class="button" href="javascript:" id="btnLimpaPesquisa"><span>Limpar Campos</span></a> <a class="button" href="javascript:" onclick="$j(this).closest('form').submit();"><span>Pesquisar</span></a> <a class="button" href="javascript:" onClick="new Util().pesquisaSimples();$j.fn.sortgrid.options.form = '#pesquisa_simples form';"><span>Cancelar Pesquisa</span></a>
				<? if($podeRelatorio): ?>
				<a class="button" href="javascript:" onClick="$j(this).closest('form').attr('target','_blank').attr('action','<?= site_url('ficha/relatorio') ?>').submit();$j(this).closest('form').attr('target','_self').attr('action','<?= site_url('ficha/lista') ?>')"><span>Relatório</span></a>
				<? endif; ?></td>
		</tr>
	</table>
	<?php echo form_close();?> </div>
<?php if( (!empty($campos))&& (is_array($campos))):?>
<table width="100%" border="0" cellspacing="1" cellpadding="2" style="clear:both;" class="tabela_padrao">
	<tfoot>
		<tr>
			<td colspan="2" style="text-align:left"><strong>Legenda</strong></td>
		</tr>
		<tr>
			<td width="2%" class="tipo_combo" style="border: 1px solid #CCCCCC">&nbsp;</td>
			<td width="98%" style="text-align:left">Ficha Combo</td>
		</tr>
		<tr>
			<td class="tipo_produto" style="border: 1px solid #CCCCCC">&nbsp;</td>
			<td style="text-align:left">Ficha Produto</td>
		</tr>
	</tfoot>
</table>
<p>&nbsp;</p>
<div id="tableObjeto">
	<?php 
//numero de colunas da tabela depende das permissoes
//utilizado nos colspan
$cols = 12 + ($podeEditar?1:0) + ($verHistorico?1:0);

?>
	<table id="tabelaBusca" border="0" cellspacing="0" cellpadding="0" width="100%" class="tableSelection">
		<thead>
			<tr>
				<?php if($podeEditar): ?>
				<th width="5%">Editar</th>
				<?php endif; ?>
				<?php if($podeExcluir): ?>
				<th width="5%">Excluir</th>
				<?php endif; ?>
				<th width="5%">Detalhe</th>
				<?php if($verHistorico): ?>
				<th width="5%">Histórico</th>
				<?php endif; ?>
				<th width="5%"><?php ghDigitalReplace($_sessao, 'SubFichas'); ?></th>
				<th width="10%">Produto</th>
				<th id="colNome"><?php ghDigitalReplace($_sessao, 'Nome da ficha'); ?></th>
				<th id="colCod"><?php ghDigitalReplace($_sessao, 'Código de Barras'); ?></th>
				<th id="colStatus">Situação</th>
				<!--<th>Segmento</th>-->
				<th id="colTemp">Temporário?</th>
				<th id="colData">Status</th>
				<th style="width: 5px"></th>
				<th id="colTipo"><?php ghDigitalReplace($_sessao, 'Tipo de Ficha'); ?></th>
				<th id="colDataAlteracao">Última Atualização</th>
			</tr>
		</thead>
		<?php foreach($campos as $ficha):?>
		<tr>
			<?php if ($podeEditar): ?>
			<td><?php echo anchor('ficha/form/'. $ficha['ID_FICHA'], '<img src="'. base_url().THEME.'img/file_edit.png" />',array('title'=>'Editar Ficha'));?></td>
			<?php endif; ?>
			<?php if ($podeExcluir): ?>
			<td><img src="<?php echo base_url().THEME; ?>img/trash.png" onClick="excluirFicha(<?php echo $ficha['ID_FICHA']; ?>);" style="cursor:pointer;"></td>
			<?php endif; ?>			
			<td><a href="javascript:new Util().vazio()" onclick="displayMessagewithparameter('<?php echo site_url('ficha/detalhe/'.$ficha['ID_FICHA']); ?>',800,650, fancyboxDetalhes)"><img src="<?= base_url().THEME ?>img/visualizar.png" border="0"/></a></td>
			<?php if($verHistorico): ?>
			<td><a href="<?php echo site_url('ficha/historico/'.$ficha['ID_FICHA']); ?>"><img src="<?= base_url().THEME ?>img/visualizar_historico.png" border="0"/></a></td>
			<?php endif; ?>
			<td><?php if(!empty($ficha['ID_FICHA1'])): ?>
				<a href="javascript:new Util().vazio();" title="Exibir Subfichas" onclick="Ficha.Subficha.Pauta.efetuarPesquisa('<?php echo $ficha['ID_FICHA']; ?>', $j(this).closest('tr'));toggleImgFilha($j(this).closest('td'));"><img src="<?= base_url().THEME ?>img/open_filhas.png"  border="0" alt="Exibir Subfichas"/></a>
				<?php endif; ?></td>
			<td><?php if(!empty($ficha['FILE_ID'])): ?>
				<a href="<?= base_url() ?>img.php?img_id=<?= $ficha['FILE_ID'] ?>&rand=<?= rand()?>&img_size=big&a.jpg" class="jqzoom"> <img src="<?= base_url().'img.php?img_id='.$ficha['FILE_ID'] ?>" title="<?=$ficha['NOME_FICHA']?>" width="50" border="0"> </a>
				<?php endif; ?></td>
			<td><?=substr($ficha['NOME_FICHA'],0,60);?></td>
			<td><?=$ficha['COD_BARRAS_FICHA'];?></td>
			<td><?=ucfirst($ficha['DESC_APROVACAO_FICHA']);?></td>
			<!-- <td width="50" align='center'><?// $ficha['DESC_SEGMENTO'];?></td> -->
			<td><?= $ficha['FLAG_TEMPORARIO'] == 1? 'Sim' : 'Não';?>
				<?php alerta_ficha($ficha); ?></td>
			<td><?php
				echo  $ficha['FLAG_ACTIVE_FICHA'] ? 'Ativa' : 'Inativa';
				
				// se esta inativa e tem data de validade
				if( empty($ficha['FLAG_ACTIVE_FICHA']) && !empty($ficha['DATA_VALIDADE']) ){
					// se a data de validade for menor que a data atual
					if( strtotime($ficha['DATA_VALIDADE']) < time() ){
						// exibe a informacao
						echo ' <img src="', base_url(), THEME, 'img/information.png" class="information"'
							, ' alt="Inativada em '
							, format_date($ficha['DATA_VALIDADE'],'d/m/Y')
							, '" />';
					}
				}
				
				?></td>
			<td class="tipo_<?php echo strtolower($ficha['TIPO_FICHA']); ?>">&nbsp;</td>
			<td><?=ucfirst(strtolower(ghDigitalReplace($_sessao, $ficha['TIPO_FICHA'])));?></td>
			<td><?=format_date_to_form($ficha['DATA_ALTERACAO'], 'd-m-Y H:i:s');?></td>
		</tr>
		<?endforeach;?>
		<tr id="modelo_ficha_pauta_subficha" class="modelo_ficha_pauta_subficha">
			<td class="modelo_ficha_pauta_filha"><?php echo anchor('ficha/form/{ID_FICHA}', '<img src="'. base_url().THEME.'img/file_edit.png" />',array('title'=>'Editar Ficha'));?></td>
			<?php if($podeExcluir): ?>
			<td class="modelo_ficha_pauta_filha"><img src="<?php echo base_url().THEME; ?>img/trash.png" onClick="excluirFicha({ID_FICHA});" style="cursor:pointer;"></td>
			<?php endif; ?>
			<td class="modelo_ficha_pauta_filha"><a href="javascript:new Util().vazio()" onclick="displayMessagewithparameter('<?php echo site_url('ficha/detalhe/{ID_FICHA}'); ?>',800,650, fancyboxDetalhes)"><img src="<?= base_url().THEME ?>img/visualizar.png" border="0"/></a></td>
			<?php if($verHistorico): ?>
			<td class="modelo_ficha_pauta_filha"><a href="<?php echo site_url('ficha/historico/{ID_FICHA}'); ?>"><img src="<?= base_url().THEME ?>img/visualizar_historico.png" border="0"/></a></td>
			<?php endif; ?>
			<td class="modelo_ficha_pauta_filha">
				<img src="<?= base_url().THEME ?>img/seta_filha.png" />
			</td>
			<td class="modelo_ficha_pauta_filha" id="objetoSubfichaPauta_{ID_FICHA}">
				<a href="<?= base_url() ?>img.php?img_id={FILE_ID}&rand=<?= rand()?>&img_size=big&a.jpg" class="jqzoom"> 
					<img src="<?= base_url().'img.php?img_id={FILE_ID}' ?>" title="{COD_BARRAS_FICHA}" width="50" border="0"> 
				</a>
			</td>
			<td class="modelo_ficha_pauta_filha" id="setaSubficha_{ID_FICHA}" style="display:none">
				
			</td>		
			<td colspan="2" class="modelo_ficha_pauta_filha" style="text-align:center;padding-left:25px;">
				<b><?php ghDigitalReplace($_sessao, 'Código de Barras'); ?>:</b>&nbsp;&nbsp; {COD_BARRAS_FICHA}<br>
				<b>Campo Principal: </b>&nbsp;&nbsp; {LABEL_CAMPO_FICHA} - {VALOR_CAMPO_FICHA} 
			</td>
			<td class="modelo_ficha_pauta_filha"></td>
			<td class="modelo_ficha_pauta_filha"></td>
			<td class="modelo_ficha_pauta_filha"></td>
			<td class="tipo_{TIPO_FICHA}">&nbsp;</td>
			<td class="modelo_ficha_pauta_filha">
				<b><?php if($isHermes){ echo 'SUBPRODUTO'; }else{ echo 'SUBFICHA'; }?></b>
			</td>
			<td class="modelo_ficha_pauta_filha"></td>
		</tr>
		<?php if(!empty($combos)):?>
		<tr>
			<td colspan="<?= $cols; ?>" style="text-align: left; color: #FFFFFF; background-color: #999999; font-weight: bold">Fichas combo relacionadas</td>
		</tr>
		<?php foreach($combos as $ficha):?>
		<tr>
			<?php if ($podeEditar): ?>
			<td><?php echo anchor('ficha/form/'. $ficha['ID_FICHA'], '<img src="'. base_url().THEME.'img/file_edit.png" />',array('title'=>'Editar Ficha'));?></td>
			<?php endif; ?>
			<?php if($podeExcluir): ?>
			<td><img src="<?php echo base_url().THEME; ?>img/trash.png" onClick="excluirFicha(<?php echo $ficha['ID_FICHA']; ?>);" style="cursor:pointer;"></td>
			<?php endif; ?>
			<td><a href="javascript:new Util().vazio()" onclick="displayMessagewithparameter('<?php echo site_url('ficha/detalhe/'.$ficha['ID_FICHA']); ?>',800,650, fancyboxDetalhes)"><img src="<?= base_url().THEME ?>img/visualizar.png" border="0"/></a></td>
			<?php if($verHistorico): ?>
			<td><a href="<?php echo site_url('ficha/historico/'.$ficha['ID_FICHA']); ?>"><img src="<?= base_url().THEME ?>img/visualizar_historico.png" border="0"/></a></td>
			<?php endif; ?>
			<td>&nbsp;</td>
			<td><?php if(!empty($ficha['FILE_ID'])): ?>
				<a href="<?= base_url() ?>img.php?img_id=<?= $ficha['FILE_ID'] ?>&rand=<?= rand()?>&img_size=big&a.jpg" class="jqzoom"> <img src="<?= base_url().'img.php?img_id='.$ficha['FILE_ID'] ?>" title="<?=$ficha['NOME_FICHA']?>" width="50" border="0"> </a>
				<?php endif; ?></td>
			<td><?=substr($ficha['NOME_FICHA'],0,60);?></td>
			<td><?=$ficha['COD_BARRAS_FICHA'];?></td>
			<td><?=ucfirst($ficha['DESC_APROVACAO_FICHA']);?></td>
			<!-- <td width="50" align='center'><?// $ficha['DESC_SEGMENTO'];?></td> -->
			<td><?= $ficha['FLAG_TEMPORARIO'] == 1? 'Sim' : 'Não';?>
				<?php alerta_ficha($ficha); ?></td>
			<td><?php
				echo  $ficha['FLAG_ACTIVE_FICHA'] ? 'Ativa' : 'Inativa';
				
				// se esta inativa e tem data de validade
				if( empty($ficha['FLAG_ACTIVE_FICHA']) && !empty($ficha['DATA_VALIDADE']) ){
					// se a data de validade for menor que a data atual
					if( strtotime($ficha['DATA_VALIDADE']) < time() ){
						// exibe a informacao
						echo ' <img src="', base_url(), THEME, 'img/information.png" class="information"'
							, ' alt="Inativada em '
							, format_date($ficha['DATA_VALIDADE'],'d/m/Y')
							, '" />';
					}
				}
				
				?></td>
			<td class="tipo_<?php echo strtolower($ficha['TIPO_FICHA']); ?>">&nbsp;</td>
			<td><?=ucfirst(strtolower($ficha['TIPO_FICHA']));?></td>
			<td><?=format_date_to_form($ficha['DATA_ALTERACAO'], 'd-m-Y H:i:s');?></td>
		</tr>
		<?endforeach;?>
		<?php endif; ?>
		<tr>
			<td colspan="<?= $cols; ?>" class="obj_encontrado">Fichas encontradas: <strong>
				<?= $total ?>
				</strong></td>
		</tr>
	</table>
</div>
<span class="paginacao">
<?=(isset($paginacao))?$paginacao:null?>
</span>
<?php else: ?>
Nenhum resultado
<?php endif; ?>
<div id="pendencias">
	<ul>
		<li><a href="#pendenciasDadosBasicos">Dados B&aacute;sicos</a></li>
		<li><a href="#pendenciasCampos">Campos</a></li>
	</ul>
	<div id="pendenciasDadosBasicos"> </div>
	<div id="pendenciasCampos"> </div>
</div>
<script type="text/javascript">

var modelo = null;

$j(function(){
	var options = 
			{
			columns:[{col:'#colNome',label:'NOME_FICHA'},
					{col:'#colCod',label:'COD_FICHA'},
					{col:'#colStatus',label:'DESC_APROVACAO_FICHA'},
					{col:'#colTemp',label:'FLAG_TEMPORARIO'},
					{col:'#colData',label:'DT_INSERT_FICHA'},
					{col:'#colTipo',label:'TIPO_FICHA'},
					{col:'#colDataAlteracao',label:'DATA_ALTERACAO'}
					],
			form:'#pesquisa_<?= (isset($busca['tipo_pesquisa']) && $busca['tipo_pesquisa'] != 'simples' ? 'completo' : 'simples' ) ?> form',
			orderDirection: '<?= $busca['ORDER_DIRECTION'] ?>',
			selectedField:'<?= $busca['ORDER'] ?>'
			}
		
	$j('#tabelaBusca').sortgrid(options);

	$j('#IMG_FICHA_FILHA').click(function(){
		//$j(this).closest('tr').after(Ficha.Filha.Pauta.modelo.clone());
	});

	$j('._calendar').datepicker({dateFormat: 'dd/mm/yy',showOn: 'button', buttonImage: '<?= base_url().THEME ?>img/calendario.png', buttonImageOnly: true});	
});

//{ 'zoomSpeedIn': 300, 'zoomSpeedOut': 300, 'overlayShow': false }

$j(".jqzoom").fancybox(); 

$j("#btnLimpaPesquisa").click(function(){
	limpaForm('#pesquisa_completo');
	clearSelect($('ID_CATEGORIA'),1);
	clearSelect($('ID_SUBCATEGORIA'),1);
	if( $('ID_CLIENTE').tagName.toLowerCase() == 'select'){
		$('ID_CLIENTE').value = '';
	}
});

function openFlex(idFicha){
	displayMessagewithparameter("<?php echo base_url().index_page()."/ficha/ficha_flex"; ?>/" + idFicha, 990,650);
}

$('ID_CLIENTE').addEvent('change', function(){
	clearSelect($('ID_SUBCATEGORIA'),1);
	montaOptionsAjax($('ID_CATEGORIA'),'<?php echo site_url('json/admin/getCategoriasByCliente'); ?>','id=' + this.value,'ID_CATEGORIA','DESC_CATEGORIA');
	
	carregaRegioes( this.value );
});

$('ID_CATEGORIA').addEvent('change', function(){
	montaOptionsAjax($('ID_SUBCATEGORIA'),'<?php echo site_url('json/admin/getSubcategoriasByCategoria'); ?>','id=' + this.value,'ID_SUBCATEGORIA','DESC_SUBCATEGORIA');
});

function fancyboxDetalhes(){
	$j(".jqzoom2").fancybox(); 
	$j('#fancy_overlay').css('z-index', 1000000);
	$j('#fancy_wrap').css('z-index', 1000001);
	$j('#fancy_outer').css('z-index', 1000002);
	
}

function exibePendencias(titulo, dadosBasicos, campos){
	if( dadosBasicos == '' && campos == '' ){
		alert('Os campos de pendências não foram preenchidos');
		return false;
	}
	
	$j('#pendenciasDadosBasicos').html(dadosBasicos);
	$j('#pendenciasCampos').html(campos);
	$j('#pendencias').dialog({
		width: 600,
		height: $j(window).height() - 100,
		title: 'Pend&ecirc;ncias da Ficha ' + titulo,
		autoOpen: false,
		modal: true,
		close: removeDivModal
	}).dialog('open');
	
	return false;
}

function adicionarCampoCodigo(valor){
	var ipt = modelo.clone();
	ipt.find('input').val(valor);
	ipt.appendTo('#containerCodigos');
}

function carregaRegioes(idcliente) {
	$j.post('<?php echo site_url('json/admin/getRegiaoByCliente'); ?>', 'id=' + idcliente, function(json){
		$j('#regioes').html('');
		
		var model = '<input type="checkbox" name="REGIOES[]" value="{0}" /> {1} ';
		$j.each(json, function(k,item){
			$j('#regioes').append( model.substitute([item.ID_REGIAO, item.NOME_REGIAO]) );
		});
		
	}, 'json');
}

function excluirFicha(idFicha){
	var pagina = "<?= $this->uri->segment(3)?>";
	if(confirm("Tem certeza que deseja excluir?")){
		document.location.href = "<?= base_url().index_page() ?>/ficha/excluir/" + idFicha + "/" + pagina;
	}
}

$j(function(){
	$j('#pendencias').tabs().hide();
	
	modelo = $j('#modeloCodigo').clone();
	modelo.attr('id','');
	
	$j('#modeloCodigo').remove();
	
	<?php
	if(!empty($busca['CODIGOS_BARRA'])){
		foreach($busca['CODIGOS_BARRA'] as $codigo){
			if(!empty($codigo)){
				echo 'adicionarCampoCodigo("', addslashes($codigo), '");'.PHP_EOL;
			}
		}
	}
	?>
	
	adicionarCampoCodigo('');
});

$j(function(){
	$j('.ui-datepicker').hide();
});

</script>
<br/>
<br/>
<br/>
<?php $this->load->view("ROOT/layout/footer") ?>
