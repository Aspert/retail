<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
<?php
//$alteracoes = array();
if(!empty($alteracoes)):
?>

<?php if($ficha_atual['TIPO_FICHA'] == 'SUBFICHA'): ?>
	<h1>Histórico <?php ghDigitalReplace($_sessao, 'Subficha'); ?> - <?= $ficha_atual['COD_BARRAS_FICHA'] ?></h1>
<?php else: ?>
	<h1>Histórico <?php ghDigitalReplace($_sessao, 'ficha'); ?> - <?= $ficha_atual['NOME_FICHA'] ?></h1>
<?php endif;?>
<div id="contentAlteracao">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="30%" style="text-align:left">Ultima alteração feita por: <strong><?php echo $alteracoes['NOME_USUARIO'];; ?></strong></td>
  </tr>
  <tr>
    <td style="text-align:left">Data da Ultima alteração: <strong><?php echo format_date_to_form($alteracoes['DATA_FICHA_HISTORICO'],'d/m/Y H:i'); ?></strong></td>
  </tr>
  <?php if ( !empty($alteracoes['DESC_AGENCIA']) ): ?>
	  <tr>
	    <td style="text-align:left">Agência: <strong><?php echo $alteracoes['DESC_AGENCIA']; ?></strong></td>
	  </tr>
  <?php endif;?>
  <?php if ( !empty($alteracoes['DESC_CLIENTE']) ): ?>
	  <tr>
	    <td style="text-align:left">Cliente: <strong><?php echo $alteracoes['DESC_CLIENTE']; ?></strong></td>
	  </tr>
  <?php endif;?>
  </table>
</div> 


<h1>Alterações <?php ghDigitalReplace($_sessao, 'ficha'); ?></h1>
<div id="contentAlteracoesFicha">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableFicha">
 <thead>
  <tr>
    <td><strong>Campo</strong></td>
    <td><strong>Valor anterior</strong></td>
    <td><strong>Valor atual</strong></td>
  </tr>
  </thead>
   <?php
  foreach($alteracoes['alteracoes'] as $key => $item):
  	if(in_array($key, array('campos','campos_novos','objetos','objetos_novos'))) {
		continue;
	}
	?>   
  <tr>
    <td><?=$key?></td>
    <td><?=$item['antigo']?></td>
    <td><?=$item['novo']?></td>
    </tr>
	<?php endforeach; ?>
</table>
	
</div> 
<!-- Final de Content Alterações Ficha -->


<br />
<h1>Lista de Campos</h1>
<div id="contentListaCampos">
	 <table width="100%" border="0" cellspacing="1" cellpadding="2">
     <!--
  <tr>
    <th width="50%">Valores anteriores</th>
    <th width="50%">Valores novos</th>
  </tr>-->
  <tr>
    <td width="50%" valign="top" style="vertical-align:top">
    
    <table width="100%" border="0" align="left" cellpadding="2" cellspacing="1" class="tableLista1">
      	<thead>
        <tr>
        	<td colspan="3" style="text-align:left; font-weight:bold;">Valores anteriores</td>
        </tr>
        </thead>
      <tr>
      	<td width="33%" height="30" style="text-align:left; padding:10px"><strong>Código</strong></td>
        <td width="33%" height="30" style="text-align:left; padding:10px"><strong>Nome</strong></td>
        <td width="33%" style="text-align:left; padding:10px"><strong>Valor</strong></td>
      </tr>
      <?php
	  if(!empty($alteracoes['campos'])):
	  	foreach($alteracoes['campos'] as $campo):
	  ?>
      <tr>
      	<td style="text-align:left; padding:10px"><?php echo $ficha_atual['COD_BARRAS_FICHA']; ?></td>
        <td style="text-align:left; padding:10px"><?php echo $campo['LABEL_CAMPO_FICHA']; ?></td>
        <td style="text-align:left; padding:10px"><?php echo $campo['VALOR_CAMPO_FICHA']; ?></td>
      </tr>
      <?php 
	  	endforeach;
	  endif;
	  ?>
    </table></td>
    <td width="50%" valign="top" style="vertical-align:top">
    <table width="100%" border="0" align="right" cellpadding="2" cellspacing="1" class="tableLista2">
    	<thead>
        <tr>
        	<td colspan="3"  style="text-align:left;font-weight:bold;">Valores novos</td>
        </tr>
        </thead>
      <tr>
      	<td width="33%" height="30" style="text-align:left; padding:10px"><strong>Código</strong></td>
        <td width="33%" height="30" style="text-align:left; padding:10px"><strong>Nome</strong></td>
        <td width="33%" style="text-align:left; padding:10px"><strong>Valor</strong></td>
      </tr>
      <?php
	  if(!empty($alteracoes['alteracoes']['campos_novos'])):
	  	foreach($alteracoes['alteracoes']['campos_novos'] as $campo):
	  ?>
      <tr>
      	<td style="text-align:left; padding:10px"><?php echo $ficha_atual['COD_BARRAS_FICHA']; ?></td>
        <td style="text-align:left; padding:10px"><?php echo $campo['LABEL_CAMPO_FICHA']; ?></td>
        <td style="text-align:left; padding:10px"><?php echo $campo['VALOR_CAMPO_FICHA']; ?></td>
      </tr>
      <?php 
	  	endforeach;
	  endif;
	  ?>
    </table></td>
  </tr>
  <?php
  foreach($alteracoes['alteracoes'] as $key => $item):
  	if(in_array($key, array('campos','campos_novos','objetos','objetos_novos'))) {
		continue;
	}
	?>
  <?php endforeach; ?>
</table>
</div> 
<!-- Final de content Lista Campos -->

<?php if($ficha_atual['TIPO_FICHA'] != 'SUBFICHA'): ?>
	<br />
	<div id="contentListaAnteriores">
	<div id="tableObjeto">
	<h1>Lista de Objetos - Objetos Anteriores</h1>
	
	<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tableSelection">
	<thead>
	  <tr>
	    <th>Produto</th>
	    <th>Nome </th>
	    <th>Marca</th>
	    <th>Data Cria&ccedil;&atilde;o</th>
	    <th>Origem</th>
	    <th>Tipo</th>
	    <th><?php ghDigitalReplace($_sessao, 'Categoria'); ?></th>
	    <th><?php ghDigitalReplace($_sessao, 'Sub-Categoria'); ?></th>
	    <th>Keywords</th>
	  </tr>
	  </thead>
	  <?php
		if(!empty($alteracoes['objetos'])) :
			foreach($alteracoes['objetos'] as $item):
	  ?>
	  <tr>
	    <td align="center"><img height="40" src="<?php echo base_url(); ?>img.php?img_id=<?php echo $item['FILE_ID']; ?>" /></td>
	    <td><?php echo $item['FILENAME']; ?></td>
	    <td><?php echo $item['DESC_MARCA']; ?></td>
	    <td><?php echo $item['DATA_UPLOAD']; ?></td>
	    <td><?php echo $item['ORIGEM']; ?></td>
	    <td><?php echo $item['DESC_TIPO_OBJETO']; ?></td>
	    <td><?php echo $item['DESC_CATEGORIA']; ?></td>
	    <td><?php echo $item['DESC_SUBCATEGORIA']; ?></td>
	    <td><?php echo $item['KEYWORDS']; ?></td>
	  </tr>
	  <?php
			endforeach;
		endif;
	  ?>
	
	  </table>
	 </div> <!-- Final Lista de Objetos - Objetos Anteriores -->
	 </div> <!-- Final de Content Objetos Anteriores -->
	 
	<div id="both" style="clear: both;"></div>
	
	<div id="contentListaNovos">  
	<div id="tableObjeto">
	<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tableSelection">
	
	<h1>Lista de Objetos - Objetos Novos</h1>
	<thead>
	  <tr>
	    <th>Produto</th>
	    <th>Nome </th>
	    <th>Marca</th>
	    <th>Data Cria&ccedil;&atilde;o</th>
	    <th>Origem</th>
	    <th>Tipo</th>
	    <th><?php ghDigitalReplace($_sessao, 'Categoria'); ?></th>
	    <th><?php ghDigitalReplace($_sessao, 'Sub-Categoria'); ?></th>
	    <th>Keywords</th>
	  </tr>
	  </thead>
	  <?php
	  foreach($alteracoes['alteracoes']['objetos_novos'] as $item):
	  ?>
	  <tr>
	    <td align="center"><img height="40" src="<?php echo base_url(); ?>img.php?img_id=<?php echo $item['FILE_ID']; ?>" /></td>
	    <td><?php echo $item['FILENAME']; ?></td>
	    <td><?php echo $item['DESC_MARCA']; ?></td>
	    <td><?php echo $item['DATA_UPLOAD']; ?></td>
	    <td><?php echo $item['ORIGEM']; ?></td>
	    <td><?php echo $item['DESC_TIPO_OBJETO']; ?></td>
	    <td><?php echo $item['DESC_CATEGORIA']; ?></td>
	    <td><?php echo $item['DESC_SUBCATEGORIA']; ?></td>
	    <td><?php echo $item['KEYWORDS']; ?></td>
	  </tr>
	  <?php
	  endforeach;
	  ?>
	  <td colspan="10">
	
	  </td>
	  </table>
	</div> <!-- Final de Table Objeto -->
	
	</div> <!-- Final de Content Objetos Novos -->
<?php endif; ?>

<?php else: ?>
	<?php if($ficha_atual['TIPO_FICHA'] == 'SUBFICHA'): ?>
		<h1>Histórico <?php ghDigitalReplace($_sessao, 'Subficha'); ?> - <?= $ficha_atual['COD_BARRAS_FICHA'] ?></h1>
	<?php else: ?>
		<h1>Histórico <?php ghDigitalReplace($_sessao, 'ficha'); ?> - <?= $ficha_atual['NOME_FICHA'] ?></h1>
	<?php endif;?>
	Não sofreu alterações
<?php endif; ?>
<div>&nbsp;</div>
<a href="<?php echo site_url('ficha/lista'); ?>" class="button"><span>Voltar</span></a>

</div> <!-- Final de Content Global -->


	
<?php $this->load->view("ROOT/layout/footer") ?>