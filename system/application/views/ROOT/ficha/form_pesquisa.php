<div id="contentGlobal">

<div style="overflow-y:auto;overflow-x:hidden;" >
<?=form_open('',array("id"=>"frmPesquisa","onsubmit"=>"return false;"));?>
	
	<h1>Pesquisa de <?php ghDigitalReplace($_sessao, 'Objetos'); ?></h1>
    
    
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Nome do arquivo:</td>
    <td><?=form_input('NOME', (isset($busca['NOME'])) ? $busca['NOME'] : null);?></td>
  </tr>
  <tr>
    <td>Fabricante:</td>
    <td>
    	<select name="ID_FABRICANTE" id="ID_FABRICANTE2"
		onchange="montaOptionsAjax($('ID_MARCA2'),'<?php echo site_url('json/admin/getMarcasByFabricante'); ?>','id=' + this.value,'ID_MARCA','DESC_MARCA');"
		>
        <option value=""></option>
        <?php echo montaOptions($fabricantes,'ID_FABRICANTE','DESC_FABRICANTE', !empty($busca['ID_FABRICANTE']) ? $busca['ID_FABRICANTE'] : ''); ?>
        </select>
    </td>
  </tr>
  <tr>
    <td>Marca:</td>
    <td>
    	<select name="ID_MARCA" id="ID_MARCA2">
        <option value=""></option>
        <?php echo montaOptions($marcas,'ID_MARCA','DESC_MARCA', !empty($busca['ID_MARCA']) ? $busca['ID_MARCA'] : ''); ?>
        </select>
    </td>
  </tr>
  <tr>
    <td>Periodo upload:</td>
    <td>
 
        
     <?=form_input(array('id'=>'PERIODO_INICIAL','name'=>'PERIODO_INICIAL', 'class'=>'_calendar','size'=>'10', 'value'=>post('PERIODO_INICIAL',true),'readonly'=>'readonly'));?> at&eacute; <?=form_input(array('id'=>'PERIODO_FINAL','name'=>'PERIODO_FINAL', 'class'=>'_calendar','size'=>'10', 'value'=>post('PERIODO_FINAL',true),'readonly'=>'readonly'));?>
    
    </td>
  </tr>
  <tr>
    <td>Origem:</td>
    <td><?=form_input(array('name'=>'ORIGEM', 'value'=>(isset($busca['ORIGEM'])) ? $busca['ORIGEM'] : null));?></td>
  </tr>
  <tr>
  	<td>Cliente:</td>
  	<td>
  		<?php if(!empty($_sessao['ID_CLIENTE'])): ?>
  		<?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?>
  		<?php else: ?>
  		<select name="ID_CLIENTE" id="ID_CLIENTE2"
        onchange="clearSelect($('SUBCATEGORIA'),1);
				montaOptionsAjax($('CATEGORIA'),'<?php echo site_url('json/admin/getCategoriasByCliente'); ?>','id=' + this.value,'ID_CATEGORIA','DESC_CATEGORIA');
				montaOptionsAjax($('ID_TIPO_OBJETO2'),'<?php echo site_url('json/admin/getTiposObjetosByCliente'); ?>','id=' + this.value,'ID_TIPO_OBJETO','DESC_TIPO_OBJETO');"
        >
  			<option value=""></option>
  			<?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', !empty($busca['ID_CLIENTE']) ? $busca['ID_CLIENTE'] : ''); ?>
  			</select>
  		<?php endif; ?>
  		</td>
  	</tr>
  <tr>
    <td>Tipo:</td>
    <td>
    	<select name="ID_TIPO_OBJETO" id="ID_TIPO_OBJETO2">
        <option value=""></option>
        <?php echo montaOptions($tipos,'ID_TIPO_OBJETO','DESC_TIPO_OBJETO', !empty($busca['ID_TIPO_OBJETO']) ? $busca['ID_TIPO_OBJETO'] : ''); ?>
        </select>
    </td>
  </tr>
  <tr>
    <td><?php ghDigitalReplace($_sessao, 'Categoria'); ?>:</td>
    <td>
    	<select name="CATEGORIA" id="CATEGORIA"
		onchange="montaOptionsAjax($('SUBCATEGORIA'),'<?php echo site_url('json/admin/getSubcategoriasByCategoria'); ?>','id=' + this.value,'ID_SUBCATEGORIA','DESC_SUBCATEGORIA');">
        <option value=""></option>
        <?php echo montaOptions($categorias,'ID_CATEGORIA','DESC_CATEGORIA', !empty($busca['CATEGORIA']) ? $busca['CATEGORIA'] : ''); ?>
        </select><br />
    </td>
  </tr>
  <tr>
    <td><?php ghDigitalReplace($_sessao, 'SubCategoria'); ?>:</td>
    <td>
    	<select name="SUBCATEGORIA" id="SUBCATEGORIA">
        <option value=""></option>
        <?php echo montaOptions($subcategorias,'ID_SUBCATEGORIA','DESC_SUBCATEGORIA', !empty($busca['SUBCATEGORIA']) ? $busca['SUBCATEGORIA'] : ''); ?>
        </select><br />
    </td>
  </tr>
  <tr>
    <td>Keywords:</td>
    <td><?= form_textarea(array('name'=>'KEYWORDS','cols'=>'40', 'rows'=>'2', 'value'=>(isset($busca['KEYWORDS'])) ? $busca['KEYWORDS'] : null));?></td>
  </tr>
</table>

<div style="margin: 20px 0 0 180px;">
	<a class="button" href="javascript:;" onclick="Ficha.pesquisar($('frmPesquisa').toQueryString(),0)"><span>Pesquisar</span></a>
	<a class="button" href="javascript:;" onclick="closeMessage();"><span>Cancelar</span></a>
	<input name="pagina" type="hidden" id="pagina" value="0">
</div> <!-- Final de Botões -->

<div class="clearBoth"></div>

<br /><br />

    <h1>Resultados da pesquisa</h1>
  	<div id="resultados" class="resultadosPesquisa">
  		Pesquisa ainda não efetuada
  	</div>
<?=form_close();?>
</div>

</div> <!-- Final de content Global -->

