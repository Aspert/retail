<?php if(!empty($objetos)): ?>
<div id="contentGlobal">

<div id="tableObjeto">

<table width="99%" border="0" cellpadding="0" cellspacing="0" class="tableSelection" style="margin-left: 2px;">			
<thead>
  	<tr>
        <td>Seleção</td>
        <td>Imagem</td>
        <td>Marca</td>
        <td><?php ghDigitalReplace($_sessao, 'Categoria'); ?></td>
        <td><?php ghDigitalReplace($_sessao, 'SubCategoria'); ?></td>		
        <td>Arquivo</td>
    </tr>
</thead>
  <?php
  $idx = 0;
  
  foreach($objetos as $item)://print_rr($item);die; ?>
  <tr>
    <td>
    	<input type="checkbox" class="selecaoObjeto" idObjeto="<?php echo $item['ID_OBJETO']?>" name="lista" id="lista"/>
    	<textarea class="selecaoObjeto_t" style="display: none;"><?php echo json_encode($item) ?></textarea>
    </td>
    
    <td>
        <a href="<?= base_url() ?>img.php?img_id=<?= $item['FILE_ID'] ?>&rand=<?= rand()?>&img_size=big&a.jpg" class="jqzoom">
        	<img src="<?= base_url() ?>img.php?img_id=<?= $item['FILE_ID'] ?>&rand=<?= rand()?>&a.jpg" width="50" border="0">
        </a>
    </td>
    <td><?php echo val($item,'DESC_MARCA'); ?></td>
    <td><?php echo val($item,'DESC_CATEGORIA'); ?></td>
    <td><?php echo val($item,'DESC_SUBCATEGORIA'); ?></td>
    <td style="text-align: left"><?php echo str_replace(' ','&nbsp;',$item['FILENAME']); ?></td>
  </tr>
  <?php
  $idx++;
  endforeach; 
  ?>
  <tr>
    <td colspan="6">
      <a class="button" href="javascript:;" onclick="Ficha.adicionarNovosObjetos()"><span>Confirmar</span></a>
    </td>
  </tr>
  <tr>
  <td colspan="6">Objetos encontrados: <strong><?php echo $total; ?></strong><br /><br />
	<?php 
	
	$pagina = $pagina < 0 ? 0 : $pagina;
	
	$numLinks = 5;
	$inicial = ($pagina/$limit) - $numLinks;
	if( $inicial < 1 ){
		$inicial = 1;
	}
	$final = ($pagina/$limit) + $numLinks;
	if( $final > $totalPagina ){
		$final = $totalPagina;
	}
	
	$list = array();
    for($i=$inicial; $i<=$final; $i++){
		if( $pagina == ($i-1)*$limit){
			$list[] = '<strong class="linkAtivoPaginacao">'.$i.'</strong>';
		} else {
			$list[] = sprintf('<a href="javascript:void(0)" onclick="Ficha.pesquisaPorPaginacao(%d)"> %s </a>',
				($i-1)*$limit,
				$i
			);
		}
    }
	
	if( $pagina > 0 ){
		echo '<a href="javascript:void(0)" onclick="Ficha.pesquisaPorPaginacao(0)">Primeira</a> ';
		echo '<a class="anterior" href="javascript:void(0)" onclick="Ficha.pesquisaPorPaginacao('.($pagina-$limit).')">Anterior</a> ';
	}
	
	echo ' | ' . implode(' | ', $list) . ' | ';
	
	if( $pagina/$limit < $totalPagina-1 ){
		echo '<a class="proximo" href="javascript:void(0)" onclick="Ficha.pesquisaPorPaginacao('.($pagina == 0 ? 5 : $pagina+$limit).')">Próxima</a> ';
		echo '<a href="javascript:void(0)" onclick="Ficha.pesquisaPorPaginacao('.(($totalPagina-1)*$limit).')">Última</a> ';
	}
    ?>  </td>
  </tr>
</table>
</div> <!-- Final de Table Objeto -->

<?php else: ?>
	Nenhum resultado encontrado
<?php endif; ?>



</div> <!-- Final de content Global -->