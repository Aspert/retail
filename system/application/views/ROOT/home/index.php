<?php $this->load->view("ROOT/layout/header") ?>

<?php
if( $dias_expirar_senha <= 5 ){
	echo '<div class="box_erros">Faltam '.$dias_expirar_senha.' dias para sua senha expirar.</div><br />';
}
?>

<h1>Home</h1>
<div id="contentGlobal">
    <div class="calendario" style="float:left">
        <?= $this->calendar->generate(date('Y'), date('m')) ?>
    </div> 
    
    <div class="feeds" style="float:right">
    	<div id="title">Feeds</div>
        <!-- root element for scrollable -->
        <div class="scrollable vertical">
            <!-- root element for the items -->
            <div class="items">
            <?php if(isset($rss) && is_array($rss)): ?>
	                <? foreach($rss['rss']['channel'] as $key => $feed): ?>
	                    <div>
	                    <? if(strpos($key,'item') === 0): ?>
	                        <h3><a href="<?= rawurldecode($feed['link']); ?>" target="_blank"><?= $feed['title']; ?></a></h3>
	                        <div class="date"><?= $feed['pubDate']; ?></div>
	                        <div class="description"><?if( isset($feed['description']) ){ echo $feed['description']; }; ?></div>
	                    <? endif; ?>
	                    </div>
	                <? endforeach; ?>
	         <? endif; ?>
            </div>
        </div>

        <div id="actions" class="paginacao" style="float:right">
            <span class="anterior"><a href="javascript:" class="prevPage">Anterior</a></span>
            <span class="proximo"><a href="javascript:" class="nextPage">Pr&oacute;ximo</a></span>
        </div>
        <div style="clear:right"></div>

	</div>

<script type="text/javascript">
$j(".feeds div.scrollable").scrollable({ 
	vertical:true,  
	size: 8 
}).mousewheel();

$j(function(){
	$j('.ui-datepicker').hide();
});
</script>

<?php $this->load->view("ROOT/layout/footer"); ?>