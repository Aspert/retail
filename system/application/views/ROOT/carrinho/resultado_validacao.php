<div id="problemas" class="modalEstatica">
	<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabela_padrao">
		<?php foreach($problemas as $praca => $resultado): ?>
		<thead>
			<tr>
				<td colspan="7"><?php echo $praca; ?></td>
			</tr>
			<tr>
				<td width="1%">&nbsp;</td>
				<td width="10%">Produto</td>
				<td width="11%">Código Regional</td>
				<td width="16%">Descrição</td>
				<td width="4%">Página</td>
				<td width="4%">Ordem</td>
				<td width="54%">Mensagem</td>
			</tr>
		</thead>
		<?php
		$comErros = false;
		foreach($resultado as $tipo => $itens):
			$cor = '';
			switch( $tipo ){
				case 'avisos': $cor = '#FFFF00'; break;
				case 'erros': 
					$comErros = true;
					$cor = '#FF0000'; 
					break;
			}
			
			foreach($itens as $item):

		?>
		<tr>
			<td bgcolor="<?php echo $cor; ?>">&nbsp;</td>
			<td><?php echo $item['PRODUTO']; ?></td>
			<td><?php echo val($item,'CODIGO_REGIONAL'); ?></td>
			<td><?php echo $item['NOME_FICHA']; ?></td>
			<td><?php echo $item['PAGINA']; ?></td>
			<td><?php echo $item['ORDEM']; ?></td>
			<td><?php echo $item['MENSAGEM']; ?></td>
		</tr>
		<?php
				endforeach;
			endforeach;
		endforeach;
		?>
		<tfoot>
			<tr>
				<td colspan="7"><strong>Tempo decorrido</strong>: <?php echo number_format($tempo_decorrido, 4); ?></td>
			</tr>
		</tfoot>
	</table>
	
	<p align="center">
		<input type="button" name="btnImprimir" id="btnImprimir" value="Imprimir" onclick="printSelection($j('.modalEstatica')[0])" />
		<input type="button" name="btnFechar" id="btnFechar" value="Fechar janela" onclick="$j('#problemas').dialog('close');" />
		<?php if(empty($comErros)): ?>
			<input type="button" name="btnContinuar" id="btnContinuar" class="continuar" value="Continuar" />
		<?php endif; ?>
	</p>
	
</div>
<script type="text/javascript">
$j(function(){
	$j('#problemas').dialog({
		height: $j(window).height() - 100,
		width: '95%',
		modal: true,
		title: 'Pendências do Job "<?php echo addslashes(post('TITULO_JOB',true)); ?>"',
		autoOpen: false,
		close: function(){
			$j('#problemas').remove();
		}
	}).dialog('open');

	$j(".continuar").click(function(){
	   continuaSalvarCarrinho();
	});
	
});

</script>