<h1>Buscar Ficha</h1>

<div id="contentFormPesquisa">

<form id="formPesquisa" method="post">
  <table width="97%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td colspan="2"></td>
    </tr>
    <tr>
    	<td colspan="2"><img src="<?php echo base_url().THEME ?>img/alerta-amarelo.jpg" width="22" height="18" alt="Atenção" /> Somente serão pesquisadas fichas aprovadas, ativas e não temporárias. </td>
    	</tr>
    <tr>
      <td width="21%">Nome da ficha</td>
      <td width="79%"><input name="NOME_FICHA" type="text" id="NOME_FICHA" value="<?php echo val($busca,'NOME_FICHA'); ?>" />
      	<input type="hidden" name="ID_JOB" id="ID_JOB" value="<?php echo $this->uri->segment(3); ?>" />
      	<input name="ID_CLIENTE" type="hidden" value="<?php echo $cliente['ID_CLIENTE']; ?>" /></td>
    </tr>
    <tr>
      <td>Código Regional</td>
      <td><input name="CODIGO_REGIONAL" type="text" id="CODIGO_REGIONAL" value="<?php echo val($busca,'CODIGO_REGIONAL'); ?>" /></td>
    </tr>
    <tr>
      <td style="vertical-align:top">Código de barras</td>
      <td>
	  	<div id="containerCodigos">
			<div id="modeloCodigo">
			  	<input name="CODIGOS_BARRA[]" type="text" />
				<a href="#" onclick="$j(this).closest('div').remove(); return false;">
					<img src="<?php echo base_url(), THEME, 'img/alerta-vermelho.jpg'; ?>" align="absbottom" title="Remover Codigo" />
				</a>
			</div>
		</div>
		
		<p><a href="#" onclick="adicionarCampoCodigo(''); return false;" class="button"><span>Adicionar Código</span></a></p>
	 </td>
    </tr>
    <tr>
    	<td>Tipo de Ficha</td>
    	<td><select name="TIPO_FICHA" id="TIPO_FICHA">
    		<option value="">TODOS</option>
    		<option value="PRODUTO">Produto</option>
    		<option value="COMBO">Combo</option>
    		</select></td>
    	</tr>
    <tr>
      <td><?php ghDigitalReplace($_sessao, 'Categoria'); ?></td>
      <td>
        <select name="ID_CATEGORIA" id="ID_CATEGORIA">
          <option value=""></option>
          <?php echo montaOptions($categorias,'ID_CATEGORIA','DESC_CATEGORIA', !empty($busca['ID_CATEGORIA']) ? $busca['ID_CATEGORIA'] : ''); ?>
        </select>
      </td>
    </tr>
    <tr>
      <td><?php ghDigitalReplace($_sessao, 'Subcategoria'); ?></td>
      <td>
        <select name="ID_SUBCATEGORIA" id="ID_SUBCATEGORIA">
          <option value=""></option>
          <?php echo montaOptions($subcategorias,'ID_SUBCATEGORIA','DESC_SUBCATEGORIA', !empty($busca['ID_SUBCATEGORIA']) ? $busca['ID_SUBCATEGORIA'] : ''); ?>
        </select>
      </td>
    </tr>
    <tr>
      <td>Fabricante</td>
      <td>
        <select name="ID_FABRICANTE" id="ID_FABRICANTE2" onchange="carregaMarcas();">
          <option value=""></option>
          <?php echo montaOptions($fabricantes,'ID_FABRICANTE','DESC_FABRICANTE', !empty($busca['ID_FABRICANTE']) ? $busca['ID_FABRICANTE'] : ''); ?>
        </select>
      </td>
    </tr>
    <tr>
      <td>Marca</td>
      <td>
      <select name="ID_MARCA" id="ID_MARCA2">
        <option value=""></option>
      </select>
      </td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
    	<td>
    		<a class="button" href="javascript:" onclick="doPesquisa(0)"><span>Buscar</span></a>
    		<a class="button" href="javascript:" onclick="closeMessage();"><span>Cancelar</span></a></td>
    	</tr>

  </table>
</form>

</div> <!-- Final de Form Pesquisa -->

<div id="tableObjeto">
	<div id="resultado"></div>
</div> <!-- Final de Resultado -->

<script type="text/javascript">
function adicionarCampoCodigo(valor){
	var ipt = modeloLinha.clone();
	ipt.find('input').val(valor);
	ipt.appendTo('#containerCodigos');
}

var modeloLinha = null;
$j(function(){
	modeloLinha = $j('#modeloCodigo').clone();
	modeloLinha.attr('id','');
	$j('#modeloCodigo').remove();
	
	adicionarCampoCodigo('');
});

function carregaMarcas(){
	var cid = $j('#ID_FABRICANTE2').val();
	var tar = $('ID_MARCA2');
	clearSelect(tar, 1);
	
	if( cid != '' ){
		$j.post(util.options.site_url+'json/fabricante/getMarcas','id='+cid,function(json){
			montaOptions(tar, json, 'ID_MARCA','DESC_MARCA');
		},'json');
	}
}

</script>
