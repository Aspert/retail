<?php if(!empty($lista)): ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableSelection">			
    <thead>
        <tr>
            <td><input type="checkbox" onclick="$j('.selecao').attr('checked',this.checked); " /></td>
            <td>Produto</td>
            <td>Filhas</td>
            <td>Nome Produto</td>
            <td>Código de Barras</td>
            <td>Status</td>		
            <td>Temporário</td>
            <td>Data Criação</td>
            <td>&nbsp;</td>
            <td>Tipo de Ficha</td>
        </tr>
    </thead>

  <?php
  $idx = 0;
  foreach($lista['data'] as $ficha): ?>
  <tr>
	<td>
		<?php if(sizeof($ficha['subfichas']) > 0): ?>
			<input type="checkbox" class="selecao" name="lista" id="lista<?= $ficha['ID_FICHA'] ?>" onclick="selecionaFichasFilhas('<?= $ficha['ID_FICHA'] ?>', this.checked); "/>
		<?php else: ?>
			<?php $ficha['FICHA_FILHA'] = '0'; ?>
			<input type="checkbox" class="selecao" name="lista" id="lista"/>
		<?php endif; ?>
		<textarea style="display:none;"><?php echo json_encode($ficha); ?></textarea>	
	</td>
	<td class="alter"><?php if(!empty($ficha['FILE_ID'])): ?>
	  <a href="<?= base_url() ?>img.php?img_id=<?= $ficha['FILE_ID'] ?>&amp;rand=<?= rand()?>&amp;img_size=big&amp;a.jpg" class="jqzoom">
	  	<img src="<?= base_url() ?>img.php?img_id=<?= $ficha['FILE_ID'] ?>&amp;rand=<?= rand()?>&amp;a.jpg" title="<?=$ficha['NOME_FICHA']?>" width="50" border="0" />
	  </a>
	  <?php endif; ?>
    </td>    
    <td>
		<?php if(sizeof($ficha['subfichas']) > 0): ?>
			<a href="javascript:new Util().vazio();" title="Exibir Filhas" onclick="$j('#id<?php echo $ficha['ID_FICHA'] ?>').toggle();toggleImgFilha($j(this).closest('td'));">
				<img src="<?= base_url().THEME ?>img/open_filhas.png"  border="0" alt="Exibir Filhas"/>
			</a>
		<?php endif; ?>
	</td>
	<td class="alter"><?=substr($ficha['NOME_FICHA'],0,60);?><br />
	<?=$ficha['DESC_CATEGORIA'];?> / <?=$ficha['DESC_SUBCATEGORIA'];?>
	</td>
	<td class="alter"><?=$ficha['COD_BARRAS_FICHA'];?></td>
	<td class="alter"><?=ucfirst($ficha['DESC_APROVACAO_FICHA']);?></td>
	<td class="alter"><?= $ficha['FLAG_TEMPORARIO'] == 1? 'Sim' : 'Não';?></td>
	<td class="alter"><?=format_date_to_form($ficha['DT_INSERT_FICHA']);?></td>
	<td class="alter tipo_<?php echo strtolower($ficha['TIPO_FICHA']); ?>" style="width:5px"></td>
	<td class="alter"><?=$ficha['TIPO_FICHA'];?></td>
  </tr>
<?php if(sizeof($ficha['subfichas']) > 0): ?>
	<tbody id="id<?php echo $ficha['ID_FICHA']; ?>" style="display:none;">
		<?php foreach($ficha['subfichas'] as $subficha): ?>
			<?php //$fichaFilha['ID_FICHA_PAI'] = $ficha['ID_FICHA']; ?> 
			<tr>
				<td class="modelo_ficha_pauta_filha">
					<input type="checkbox" class="selecao <?php echo $ficha['ID_FICHA']; ?>" name="lista" id="selecao" onClick="return selecionaFilhaSemPai(<?php echo $ficha['ID_FICHA']; ?>, this.checked);"/>
					<textarea style="display:none"><?php echo json_encode($subficha); ?></textarea>
				</td>
				<td class="modelo_ficha_pauta_filha"><img src="<?= base_url().THEME ?>img/seta_filha.png" /></td>
				<td class="modelo_ficha_pauta_filha"></td>
				<td class="modelo_ficha_pauta_filha" colspan="3" style="text-align:left;padding-left:25px;">
					<b>Código de Barras:</b>&nbsp;&nbsp;&nbsp; <?=$subficha['COD_BARRAS_FICHA'];?><br>
					<b>Campo Principal:</b>&nbsp;&nbsp;&nbsp;&nbsp; <?=$subficha['LABEL_CAMPO_FICHA'];?> - <?=$subficha['VALOR_CAMPO_FICHA'];?> 
				</td>
				<td class="modelo_ficha_pauta_filha"></td>
				<td class="modelo_ficha_pauta_filha"></td>
				<td class="alter tipo_<?php echo strtolower($subficha['TIPO_FICHA']); ?>" style="width:5px"></td>
				<td class="modelo_ficha_pauta_filha"><?=$subficha['TIPO_FICHA'];?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
<?php endif; ?>
  <?php
  $idx++;
  endforeach; 
  ?>
  <? if(!empty($fichasCombo)): ?>
  <tr>
	<td colspan="10" style="text-align: left; color: #FFFFFF; background-color: #999999; font-weight: bold">Fichas combo relacionadas</td>
  </tr>
  
  <?php
  foreach($fichasCombo['data'] as $ficha): ?>
  <tr>
	<td>
		<input type="checkbox" class="selecao" name="lista" id="lista" />
		<textarea style="display:none"><?php echo json_encode($ficha); ?></textarea>
	</td>
	<td class="alter"><?php if(!empty($ficha['FILE_ID'])): ?>
	  <a href="<?= base_url() ?>img.php?img_id=<?= $ficha['FILE_ID'] ?>&amp;rand=<?= rand()?>&amp;img_size=big&amp;a.jpg" class="jqzoom"> <img src="<?= base_url() ?>img.php?img_id=<?= $ficha['FILE_ID'] ?>&amp;rand=<?= rand()?>&amp;a.jpg" title="<?=$ficha['NOME_FICHA']?>" width="50" border="0" /> </a>
	  <?php endif; ?>
    </td>
    <td>&nbsp;</td>
	<td class="alter"><?=substr($ficha['NOME_FICHA'],0,60);?><br />
	<?=$ficha['DESC_CATEGORIA'];?> / <?=$ficha['DESC_SUBCATEGORIA'];?>
	</td>
	<td class="alter"><?=$ficha['COD_BARRAS_FICHA'];?></td>
	<td class="alter"><?=ucfirst($ficha['DESC_APROVACAO_FICHA']);?></td>
	<td class="alter"><?= $ficha['FLAG_TEMPORARIO'] == 1? 'Sim' : 'Não';?></td>
	<td class="alter"><?=format_date_to_form($ficha['DT_INSERT_FICHA']);?></td>
	<td class="alter tipo_<?php echo strtolower($ficha['TIPO_FICHA']); ?>" style="width:5px"></td>
	<td class="alter"><?=$ficha['TIPO_FICHA'];?></td>
  </tr>
  <?php
  endforeach; 
  ?>
  <? endif; ?>
  <tr>
	<td colspan="10">
	  <a class="button" href="javascript:" onclick="addLinhasSelecionadas()"><span>Confirmar</span></a>
	</td>
  </tr>
  <tr>
  <td colspan="10" style="text-align:left;">Páginas: 
	<?php 
	$list = array();
	$maxPages = 5;
	$inicio = $pagina - $maxPages < 0 ? 0 : $pagina - $maxPages;
	$fim = $pagina + $maxPages > $lista['nPaginas'] ? $lista['nPaginas'] : $pagina + $maxPages;
	
	if( $pagina > 0 ){
		echo '<a href="#" onclick="doPesquisa(0); return false;"> Primeira </a> |
			<a href="#" onclick="doPesquisa(',($pagina-1),'); return false;"> Anterior </a> - ';
	}
	
	for($i=$inicio; $i<$fim; $i++){
		if( $i == $pagina ){
			$list[] = '<span class="linkAtual">' . ($i+1) . '</span>';
		} else {
			$list[] = sprintf('<a href="javascript:void(0)" class="linkBranco" onclick="doPesquisa(%d)"> %s </a>',
				$i,
				$i + 1
			);
		}
	}
	
	echo implode(' | ', $list);
	
	if( $pagina < $lista['nPaginas']-1 ){
		echo ' - <a href="#" onclick="doPesquisa(',($pagina+1),'); return false;"> Próxima </a> |
			<a href="#" onclick="doPesquisa(', ($lista['nPaginas']-1), '); return false;"> Última </a>';
	}
	
	?>  
    </td>
  </tr>
  <tr>
 	<td colspan="10" style="text-align:left;">Fichas encontradas: <strong><?php echo $lista['total']; ?></strong></td>
  	</tr>

</table>

<?php else: ?>
Nenhum resultado encontrado
<?php endif; ?>


<script type="text/javascript">

function selecionaFichasFilhas(idFichaPai, check){
	if(check){
		$j('.' + idFichaPai).attr('checked', check);
	}
	else{
		$j('.' + idFichaPai).attr('checked', check);
	}
}

function selecionaFilhaSemPai(idFichaPai, check){
	if(check){
		document.getElementById('lista'+idFichaPai).checked = true;

	}
	return true;
}
	
</script>