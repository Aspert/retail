<?php $this->load->view("ROOT/layout/header");
	$_SESSION['ID_JOB'] =  $this->uri->segment(3);?>

<style type="text/css">

.apagado{

opacity:0.5;
filter: alpha(opacity=50);

}

.acesso{


}

</style>

<div id="contentGlobal" >
	<h1 >Carrinho</h1>
	<div id='criarEmLote'></div>
	<form action="<?php echo site_url('carrinho/save/'.$this->uri->segment(3)); ?>" method="post" enctype="multipart/form-data" id="form1">
		<input type='hidden' name='tipo_importacao' id='TIPO_IMPORTACAO' value=''>
		<input type="hidden" name="CADASTRAR_EM_LOTE" id="cadastrarEmLote" value="0">
		<input type="hidden" name="ID_CLIENTE" id="ID_CLIENTE" value="<?= $job['ID_CLIENTE']?>">
		<input type="hidden" name="ID_PRODUTO" id="ID_PRODUTO" value="<?= $job['ID_PRODUTO']?>">
		<input type="hidden" name="ID_USUARIO" id="ID_USUARIO" value="<?= $job['ID_USUARIO']?>">
		<input type="hidden" name="ID_JOB" id="ID_JOB" value="<?= $job['ID_JOB']?>">
		<input type="hidden" name="ID_TIPO_PECA" id="ID_TIPO_PECA" value="<?= $job['ID_TIPO_PECA']?>">
		<input type="hidden" name="PAGINAS_JOB" id="PAGINAS_JOB" value="<?= $job['PAGINAS_JOB']?>">
		<?php if(isset($fichasNaoEncontradas) && count($fichasNaoEncontradas) > 0): ?>
			<?php foreach($fichasNaoEncontradas as $fne): ?>
				<input type="hidden" name="FICHAS_NAO_ENCONTRADAS[]" id="FICHAS_NAO_ENCONTRADAS" value="<?php echo $fne; ?>">
			<?php endforeach; ?>
		<?php endif; ?>
		<!-- inicio conteudo -->
<?php
		if(!empty($dados['erros'])){
			if(isset($dados['erros']['erros'])){
				unset($dados['fichas_cenario_excluidas']);
				unset($dados['itens']);
				$problemas = $dados;
			} else {
				$problemas = $dados['erros'];
			}
			require_once dirname(dirname(__FILE__)) . '/importacao_excel/resultado_validacao.php';
		}

		if( isset($dados['erros_espelho']) && !empty($dados['erros_espelho']) ){
			$problemas = $dados['erros_espelho'];
			require_once dirname(dirname(__FILE__)) . '/importacao_excel/resultado_espelho.php';
		}
		
		if( isset($dados['fichas_cenario_excluidas']) && !empty($dados['fichas_cenario_excluidas']) ){
			$avisos = $dados['fichas_cenario_excluidas'];
			require_once dirname(dirname(__FILE__)) . '/importacao_excel/resultado_cenario.php';
		}
			
		if( !empty($erros) ){
			echo '<div class="box_erros">', implode('<br>', $erros), '</div><br />';
		}
?>

		<!-- inicio abas -->
		<div id="tabs">
			<ul>
				<li><a href="#dados_job">Dados do job</a></li>
				<li><a href="#carrinho">Itens do Carrinho</a></li>
				<li><a href="#planilha">Importar Planilha</a></li>
			</ul>
			
			<div id="dados_job">
				<!-- inicio aba dados do job -->
				<table width="100%" class="Alignleft">
					<tr>
						<td width="17%"><strong>Ag&ecirc;ncia</strong></td>
						<td width="83%"><?php echo $job['DESC_AGENCIA']; ?></td>
					</tr>
					<tr>
						<td><strong>Cliente</strong></td>
						<td><?php echo $job['DESC_CLIENTE']; ?></td>
					</tr>
					<tr>
						<td><strong>Bandeira</strong></td>
						<td><?php echo $job['DESC_PRODUTO']; ?></td>
					</tr>
					<tr>
						<td><strong>Campanha</strong></td>
						<td><?php echo $job['DESC_CAMPANHA']; ?></td>
					</tr>
					<tr>
						<td><strong>Tipo de pe&ccedil;a</strong></td>
						<td><?php echo $job['DESC_TIPO_PECA']; ?></td>
					</tr>
					<?php if(!$_sessao['IS_HERMES']){?>
					<tr>
						<td><strong>Tipo de Job</strong></td>
						<td><?php echo $job['DESC_TIPO_JOB']; ?></td>
					</tr>
					<?php }?>
					<tr>
						<td><strong>N&uacute;mero do Job</strong></td>
						<td><?php echo $job['NUMERO_JOB_EXCEL']; ?></td>
					</tr>
					<tr>
						<td><strong>T&iacute;tulo</strong></td>
						<td><?php echo $job['TITULO_JOB']; ?></td>
					</tr>
					<tr>
						<td><strong>Usu&aacute;rio Respons&aacute;vel</strong></td>
						<td><?php echo $job['NOME_USUARIO_RESPONSAVEL']; ?></td>
					</tr>
					<tr>
						<td><strong>N&uacute;mero de p&aacute;ginas</strong></td>
						<td><?php echo $job['PAGINAS_JOB']; ?></td>
					</tr>
					<?php if(!$_sessao['IS_HERMES']){?>
					<tr>
						<td><strong>Formato</strong></td>
						<td><?php echo centimetragem($job['LARGURA_JOB']), ' x ', centimetragem($job['ALTURA_JOB']); ?></td>
					</tr>
					<?php }?>
					<tr>
						<td><strong>In&iacute;cio  da Validade</strong></td>
						<td><?php echo format_date_to_form($job['DATA_INICIO']); ?></td>
					</tr>
					<tr>
						<td><strong>Final da Validade</strong></td>
						<td><?php echo format_date_to_form($job['DATA_TERMINO']); ?></td>
					</tr>
					<tr>
						<td valign="top"><strong>Observa&ccedil;&otilde;es</strong></td>
						<td><?php echo nl2br($job['OBSERVACOES_EXCEL']); ?></td>
					</tr>
					<tr>
						<td valign="top"><strong>Situa&ccedil;&atilde;o</strong></td>
						<td><?php echo $job['DESC_STATUS']; ?></td>
					</tr>
					<tr>
						<td valign="top"><strong>Etapa atual</strong></td>
						<td><?php echo $job['DESC_ETAPA']; ?></td>
					</tr>
				</table>
				<!-- Final de Table Objeto -->
			</div>
			<!-- final aba dados do job -->
			
			<div id="carrinho">
				<!-- inicio botao adicionar itens -->
				<div style="padding-bottom: 20px;">
					Todos os itens indicados no plano de marketing devem estar completos, ou seja, todos os itens devem ficar com valor zero no plano de marketing indicado para a pra&ccedil;a.<br />
					Para ver os itens do plano de marketing da pra&ccedil;a, clique no &iacute;cone 
					<img border="0" src="<?php echo base_url().THEME ?>img/magnify.png" width="16" height="16" title="Exibir Plano de Marketing" alt="Exibir Plano de Marketing" />
					ao lado do nome da pra&ccedil;a desejada.
				</div>
				
				<div style="float:right"> <a class="button" href="javascript:" onclick="pesquisar();"><span><?php ghDigitalReplace($_sessao, 'Buscar Fichas'); ?></span></a> </div>
				<div class="clearBoth">&nbsp;</div>
				
				<!-- fim botao adicionar itens -->
				<div id="tableObjeto">
					
						<table cellpadding="0" cellspacing="0" width="100%" border="0" class="tableSelection">
							<!-- inicio cabecalho -->
							<thead>
								<tr>
									<?php if ($podeVisualizarPendencia) {?>
										<th>Detalhes</th>
									<?php }?>
									<th>Imagem</th>
									<?php if($podeEditarFicha): ?>
									<th width="5%">Editar</th>
									<?php endif; ?>
									<th><?php ghDigitalReplace($_sessao, 'SubFichas'); ?></th>
									<th><?php ghDigitalReplace($_sessao, 'Fichas'); ?></th>
									<?php foreach($pracas as $praca): ?>
									<th>
										<?php echo $praca['DESC_PRACA']; ?>
										<a href="#" onclick="exibirPlanoMidia(<?php echo $praca['ID_PRACA']; ?>); return false;">
											<img border="0" src="<?php echo base_url().THEME ?>img/magnify.png" width="16" height="16" title="Exibir Plano de Marketing" alt="Exibir Plano de Marketing" />
										</a>
									</th>
									<?php endforeach; ?>
									<?php if ($podeVisualizarPendencia) {?>
										<th>Pendências</th>
									<?php }?>
									<th>Excluir</th>
								</tr>
							</thead>
							<!-- fim cabecalho -->
							<!-- inicio listagem itens -->
							<tbody id="tabela_itens">
								<!-- inicio linha modelo normal -->
								<tr id="modelo" class="linhaItem">
									<?php if ($podeVisualizarPendencia) {?>
										<td><a href="javascript:new Util().vazio()" onclick="displayMessagewithparameter('<?php echo site_url('ficha/detalhe/{ID_FICHA}');?>',800,650, fancyboxDetalhes)"><img src="<?php echo base_url().THEME; ?>img/visualizar.png" alt="Visualizar" id="visualizar" name="visualizar" title="Visualizar" border="0" /></a></td>
									<?php }?>
									<td>
										<a href="<?php echo base_url(); ?>img.php?img_size=big&img_id={FILE_ID}&a.jpg" class="fancybox">
											<img src="<?= base_url().'img.php?img_id={FILE_ID}' ?>" height="50" />
										</a>
									</td>
									<?php if ($podeEditarFicha): ?>
									<td><?php echo anchor('ficha/form/{ID_FICHA}', '<img src="'. base_url().THEME.'img/file_edit.png" />',array('title'=>'Editar Ficha'));?></td>
									<?php endif; ?>
									<td>
										<a class="exibe_filhas_{ID_FICHA}" id="exibe_filhas_{ID_FICHA}" href="javascript:new Util().vazio();" title="Exibir Filhas" onclick="$j('.modelo_ficha_pauta_filha_{ID_FICHA}').toggle();toggleImgFilha($j(this).closest('td'));" style="display:none;">
											<img src="<?= base_url().THEME ?>img/open_filhas.png"  border="0" alt="Exibir Filhas"/>
										</a>
									</td>
									<td><strong>{NOME_FICHA}</strong><br />
									{DESC_CATEGORIA} - {DESC_SUBCATEGORIA}<br />
										({COD_BARRAS_FICHA}) </td>
									<?php foreach($pracas as $praca): ?>
									<td>
										<input class="cb praca_<?php echo $praca['ID_PRACA']; ?> produto_{ID_FICHA}" type="checkbox" name="item[<?php echo $praca['ID_PRACA']; ?>][{ID_FICHA}]" id="produtoPai{ID_FICHA}-{ID_CATEGORIA}-{ID_SUBCATEGORIA}" value="1" onchange="selecionaFilhas(this, '<?php echo $praca['ID_PRACA']; ?>', '{ID_FICHA}');"/>
										<br><br>
										<select class="sl cenario_praca_<?php echo $praca['ID_PRACA']; ?> produto_{ID_FICHA}" id="cenario_praca_<?php echo $praca['ID_PRACA']; ?>_item_{ID_FICHA}" name="item[<?php echo $praca['ID_PRACA']; ?>][{ID_FICHA}][CENARIO]">
											<option value="0"></option>
										</select>
										<textarea class="info" style="display:none"></textarea>
										<div class="indisponivel_<?php echo $praca['ID_PRACA']; ?>" style="display:none"> <img src="<?php echo base_url().THEME; ?>img/error.png" title="Item indisponivel" /> </div>
									</td>
									<?php endforeach; ?>
									<?php if ($podeVisualizarPendencia) {?>
										<td><a href="javascript:new Util().vazio()" onclick="displayMessagewithparameter('<?php echo site_url("ficha/pendencia/{ID_FICHA}");?>',400,250, fancyboxDetalhes);"><img class='apagado' src="<?php echo base_url().THEME; ?>img/alerta-amarelo.jpg" alt="Pendências" id="pendencias_{ID_FICHA}" name="pendencias" title="Pendências" border="0" /></a></td>
									<?php }?>
									<td><a href="javascript:void(0);" class="remover"> <img src="<?php echo base_url().THEME; ?>img/trash.png" alt="Remover Ficha"  title="Remover Ficha" border="0" /> </a></td>
								</tr>
								<!-- fim linha modelo normal -->

								<!-- inicio linha modelo filha -->
								<tr id="modelo_filha" class="linhaItem">
									<td>
										<a href="<?php echo base_url(); ?>img.php?img_size=big&img_id={FILE_ID}&a.jpg" class="fancybox">
											<img src="<?= base_url().'img.php?img_id={FILE_ID}' ?>" height="50" />
										</a>
									</td>
									<td>
										<a class="exibe_filhas_{ID_FICHA}" id="exibe_filhas_{ID_FICHA}" href="javascript:new Util().vazio();" title="Exibir Filhas" onclick="$j('.modelo_ficha_pauta_filha_{ID_FICHA}').toggle();toggleImgFilha($j(this).closest('td'));" style="display:none;">
											<img src="<?= base_url().THEME ?>img/open_filhas.png"  border="0" alt="Exibir Filhas"/>
										</a>
									</td>
									<td><strong>{NOME_FICHA}</strong><br />
									{DESC_CATEGORIA} - {DESC_SUBCATEGORIA}<br />
										({COD_BARRAS_FICHA}) </td>
									<?php foreach($pracas as $praca): ?>
									<td>
										<input class="cb praca_<?php echo $praca['ID_PRACA']; ?> produto_{ID_FICHA_PAI}" type="checkbox" name="item[filhas][{ID_FICHA1}][<?php echo $praca['ID_PRACA']; ?>][{ID_FICHA}]" id="produtoFilha{ID_FICHA_PAI}-{ID_CATEGORIA}-{ID_SUBCATEGORIA}" value="1" onclick="return false;selecionaPai(this, '<?php echo $praca['ID_PRACA']; ?>', '{ID_FICHA_PAI}');"/>
										<textarea class="info" style="display:none"></textarea>
										<div class="indisponivel_<?php echo $praca['ID_PRACA']; ?>" style="display:none"> <img src="<?php echo base_url().THEME; ?>img/error.png" title="Item indisponivel" /></div>
									</td>									
									<?php endforeach; ?>
									<td><a href="javascript:void(0);" class="remover"> <img src="<?php echo base_url().THEME; ?>img/trash.png" alt="Remover Ficha"  title="Remover Ficha" border="0" /> </a></td>
								</tr>
								<!-- fim linha modelo filha -->
								
								<!-- inicio linha modelo subficha -->
								<tr id="modelo_subficha" class="linhaItem">
									<?php if ($podeVisualizarPendencia) {?>
										<td><a href="javascript:new Util().vazio()" onclick="displayMessagewithparameter('<?php echo site_url('ficha/detalhe/{ID_FICHA}');?>',800,650, fancyboxDetalhes)"><img src="<?php echo base_url().THEME; ?>img/visualizar.png" alt="Visualizar" id="visualizar" name="visualizar" title="Visualizar" border="0" /></a></td>
									<?php }?>
									<td></td>
									<?php if ($podeEditarFicha): ?>
									<td><?php echo anchor('ficha/form/{ID_FICHA}', '<img src="'. base_url().THEME.'img/file_edit.png" />',array('title'=>'Editar Ficha'));?></td>
									<?php endif; ?>
									<td id='objetoSubFicha_{ID_FICHA}' >
										<a href='<?php echo base_url(); ?>img.php?img_size=big&img_id={FILE_ID}&a.jpg' class="fancybox">
											<img src="<?= base_url().'img.php?img_id={FILE_ID}' ?>" height="50" />
										</a>
									</td>
									<td id='setaFicha_{ID_FICHA}' style="display:none">
										<img src="<?= base_url().THEME ?>img/seta_filha.png" />
									</td>
									<td><strong>{COD_BARRAS_FICHA}</strong><br />
									{LABEL_CAMPO_FICHA} - {VALOR_CAMPO_FICHA}
									<?php foreach($pracas as $praca): ?>
									<td>
										<input class="cb praca_<?php echo $praca['ID_PRACA']; ?> produto_{ID_FICHA1}" type="checkbox" name="item[subfichas][{ID_FICHA1}][<?php echo $praca['ID_PRACA']; ?>][{ID_FICHA}]" id="produtoFilha{ID_FICHA1}-{ID_CATEGORIA}-{ID_SUBCATEGORIA}" value="1" onclick="selecionaPai(this, '<?php echo $praca['ID_PRACA']; ?>', '{ID_FICHA1}');"/>
										<textarea class="info" style="display:none"></textarea>
										<div class="indisponivel_<?php echo $praca['ID_PRACA']; ?>" style="display:none"> <img src="<?php echo base_url().THEME; ?>img/error.png" title="Item indisponivel" /></div>
									</td>									
									<?php endforeach; ?>
									<?php if ($podeVisualizarPendencia) {?>
										<td><a href="javascript:new Util().vazio()" onclick="displayMessagewithparameter('<?php echo site_url("ficha/pendencia/{ID_FICHA}");?>',400,250, fancyboxDetalhes);"><img class='apagado' src="<?php echo base_url().THEME; ?>img/alerta-amarelo.jpg" alt="Pendências" id="pendencias_{ID_FICHA}" name="pendencias" title="Pendências" border="0" /></a></td>
									<?php }?>
									<td><a href="javascript:void(0);" class="remover"> <img src="<?php echo base_url().THEME; ?>img/trash.png" alt="Remover Ficha"  title="Remover Ficha" border="0" /> </a></td>
								</tr>
								<!-- fim linha modelo subficha -->
								
							</tbody>
							<!-- fim listagem itens -->
						</table>
					
					<div style="clear:both">&nbsp;</div>
				</div>
				<!-- final de Table Objeto -->
				<div class="carregando" style="padding-top:20px;">Carregando...</div>
				<div class="clearBoth">&nbsp;</div>
				<!-- fim conteudo -->
				<div style="float:right"> <a class="button" href="javascript:" onclick="pesquisar();"><span><?php ghDigitalReplace($_sessao, 'Buscar Fichas'); ?></span></a> </div>
				<div style="clear:both; padding-top: 30px;"> O &iacute;cone <img src="<?php echo base_url().THEME; ?>img/error.png" title="Item indisponivel" /> indica que o produto foi marcado como indispon&iacute;vel pela logística na pra&ccedil;a indicada. </div>
			</div>
			<!-- fim aba carrinho -->
			<div id="planilha">
				<!-- inicio aba importacao -->
				<p>Caso voc&ecirc; tenha um arquivo de Excel contendo os valores para preencher o carrinho, utilize os campos abaixo para realizar a importa&ccedil;&atilde;o.</p>
				<p><strong>Aten&ccedil;&atilde;o</strong>: os dados do arquivo devem seguir o modelo de Excel.</p>
				<p>&nbsp;</p>
				<p>Importar arquivo:
					<input type="file" name="arquivo" id="arquivo" />
					<br />
					<br />
					<a id="btnImportar" href="javascript:" class="button"><span>Importar arquivo</span></a> </p>
				<div class="clearBoth">&nbsp;</div>
			</div>
			<!-- fim aba importacao -->
			
		</div>
		<!-- fim tabs -->
		<div id="both" style="clear: both;"></div>
		<div style="margin:10px 0 0 0;">
			<!-- inicio botoes -->
			<a class="button btns" href="javascript:" onclick="salvarCarrinho()" id="btn_salvar" style="display: none;"><span>Salvar</span></a> <a class="button" href="<?php echo site_url('carrinho/index'); ?>"><span>Cancelar</span></a>
			<?php if($podePassarEtapa && $this->uri->segment(3) != ''): ?>
			<a class="button btns" href="javascript:" onclick="proximaEtapa()" id="prox_etapa" style="display: none;"><span>Pr&oacute;xima Etapa</span></a>
			<?php endif; ?>
            <?php if( $podeVoltarEtapa && $this->uri->segment(3) != ''):?>
                <a class="button" href="<?php echo site_url('checklist/voltar_etapa/'.$job['ID_JOB']); ?>" ><span>Voltar Etapa</span></a>
            <?php endif;?>
			<a class="button" href="javascript:" onclick="proximaEtapaSemItem()" id="prox_etapa_sem_item"><span>Pr&oacute;xima Etapa</span></a>
		</div>
		<!-- fim botoes -->
		<div id="both" style="clear: both;"></div>
	</form>
	
<!-- inicio : criando o plano de marketing para cada praca -->

<?php foreach($plano_midia as $praca): ?>
<div title="Plano de Marketing - <?php echo $praca['DESC_PRACA']; ?>" style="display:none" id="plano_midia_<?php echo $praca['ID_PRACA']; ?>">
	<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tableSelection plano" title="Plano de Marketing - <?php echo $praca['DESC_PRACA']; ?>">
		<?php foreach($praca['categorias'] as $categoria): ?>
			<tbody id="<?php echo $categoria['ID_CATEGORIA']; ?>" class="categoria" title="<?php echo $categoria['DESC_CATEGORIA']; ?>">
			<tr>
				<td colspan="2" style="text-align:left"><strong><?php echo $categoria['DESC_CATEGORIA']; ?></strong></td>
			</tr>
			<?php foreach($categoria['subcategorias'] as $item): ?>
			<tr class="item" id="sub-<?php printf('%d-%d', $item['ID_PRACA'], $item['ID_SUBCATEGORIA']); ?>" title="<?php echo $item['DESC_CATEGORIA'] . ' - ' . $item['DESC_SUBCATEGORIA']; ?>">
				<td style="padding-left: 20px; text-align:left"><?php echo $item['DESC_SUBCATEGORIA']; ?></td>
				<td class="qtde" style="text-align:left" width="16%"><?php echo $item['QTDE_EXCEL_CATEGORIA']; ?></td>
			</tr>
			<?php endforeach;?>
			</tbody>
		<?php endforeach;?>
	</table>
</div>
<?php endforeach; ?>
<!-- fim : criando o plano de marketing para cada praca -->

<div id="dialog-confirm" title="Carrinho" style="display:none;">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Já existem produtos para este carrinho. Deseja substituí-los?</p>
</div>

<script type="text/javascript">
var modelo = null;
var modelo_filha = null;
var modelo_subficha = null;
var categorias_usuario = [<?php echo implode(', ', $categorias_usuario); ?>];
var itensTabela = new Array;

$j(function($){

	$j('.jqzoom').fancybox();
	$j('#fancy_overlay').css('z-index',100005);
	$j('#fancy_outer').css('z-index',100006);
			
	modelo = $j('#modelo').clone();
	modelo.attr('id','');
	$j('#modelo').remove();

	modelo_filha = $j('#modelo_filha').clone();
	modelo_filha.attr('id','');
	$j('#modelo_filha').remove();

	modelo_subficha = $j('#modelo_subficha').clone();
	modelo_subficha.attr('id','');
	$j('#modelo_subficha').remove();

	$j('#tabs').tabs();

	$j('a[href=#carrinho]').one('click',function(){
		
		// esconde os botoes principais ate terminar o carregamento
		$j('.btns').hide();
		
		// criamos um loader para carregar aos poucos
		var loader = new StackLoader('<?php echo site_url('json/fluxo/get_itens_carrinho'); ?>',{});
		loader.params.idjob = <?php printf('%d', $this->uri->segment(3)); ?>;
		loader.limit = 50;
		
		loader.onCompleteBlock = function(_loader, container, result){
			var lista = eval('['+result+']')[0];
	
			if( lista.length == 0 ){
				return false;
			}
			
			$j.each(lista, function(){
				if(this.ID_FICHA_PAI == 0 || !this.ID_FICHA_PAI){
					new Ficha(this, this.PRACAS, this.INDISPONIVEL, this.CENARIOS).create();
				}
			});
	
			$j.each(lista, function(){
				if(this.ID_FICHA_PAI > 0){
					new Subficha(this, this.PRACAS, this.INDISPONIVEL).create();
				}
			});
			
			return true;
		}
		loader.onComplete = function(_loader){
			$j('.carregando').hide();
			$j('#prox_etapa_sem_item').hide();
			$j('.btns').show();
			$j('#btn_salvar').show();
			$j('#prox_etapa').show();
		}
		loader.start();
		//loader.load(loader.offset,loader.limit);
	});

	$j('#btnImportar').click(
			function(){
				importarArquivo($j(this));
			}
		);
	
});

//para adicionar aos poucos o itens da tabela
function timeOut(i){

	setTimeout(function(){
		 $j('#tabela_itens').append( itensTabela[i] );
    }, 1000);

}

function importarArquivo(obj){
	var totalFichas = <?php echo $total_fichas; ?>;
	if(totalFichas > 0){
		$j( "#dialog-confirm" ).clone().dialog({
			resizable: false,
			height:140,
			modal: true,
			buttons: {
				"Cancelar": function() {
					$j( this ).dialog( 'close' );
				},
				/*
				"Não": function() {
					$j('#TIPO_IMPORTACAO').attr('value', 'adicionar');
					$j( this ).dialog( 'close' );
					obj.closest('form').submit();
				},
				*/
				"Sim": function() {
					$j('#TIPO_IMPORTACAO').attr('value', 'substituir');
					$j( this ).dialog( 'close' );
					obj.closest('form').submit();
				}
			}
		});
	} else {
		$j('#TIPO_IMPORTACAO').attr('value', 'substituir');
		obj.closest('form').submit();
	}
}

/*****************************************************
******************************************************
******************************************************
     Funcoes usadas no modal de busca de fichas
******************************************************
******************************************************
*****************************************************/

function addLinhasSelecionadas(){
	$j('.selecao').each(function(){
		if( this.checked ){
			var obj = eval('['+$j(this).parent().find('textarea').val()+']')[0];
			if(obj.ID_FICHA == obj.ID_FICHA1 || obj.ID_FICHA1 == null) {
				// FICHA PRINCIPAL
				new Ficha(obj, [], [], obj.CENARIOS).create();
			}
			else{
				// SUBFICHA
				new Subficha(obj, [], []).create();
				//$j('#exibe_filhas_'+obj.ID_FICHA1).show();
				//$j('#ficha'+obj.ID_FICHA1).after('<tr><td colspan=5>joia</td></tr>');
			}


			/*
			if(obj.ID_FICHA_PAI == "" || !obj.ID_FICHA_PAI){
				new Ficha(obj, [], [], obj.CENARIOS).create();
			}
			else{
				new FichaFilha(obj, [], []).create();
			}
			*/
		}
	});
	
	closeMessage();
}

function pesquisar(){
	displayMessagewithparameter('<?php echo base_url().index_page().'/carrinho/form_pesquisa/'.$this->uri->segment(3); ?>',800,600,assignModalHandlers);
}

function assignModalHandlers(){
	$('ID_CATEGORIA').addEvent('change', function(){
		clearSelect($('ID_SUBCATEGORIA'),1);
		montaOptionsAjax($('ID_SUBCATEGORIA'),'<?php echo site_url('json/admin/getSubcategoriasByCategoria'); ?>','id=' + this.value,'ID_SUBCATEGORIA','DESC_SUBCATEGORIA');
	});
}

function doPesquisa(pg){
	$j('#resultado').html('Pesquisando...');
	$j.post('<?php echo site_url('json/ficha/search'); ?>/' + pg,
		   
		$j('#formPesquisa').serialize(),
		
		function(html){
			$j('#resultado').html( html );
			$j('.jqzoom').fancybox();
		}
	);
}

function fancyboxDetalhes(){
	$j(".jqzoom2").fancybox(); 
	$j('#fancy_overlay').css('z-index', 1000000);
	$j('#fancy_wrap').css('z-index', 1000001);
	$j('#fancy_outer').css('z-index', 1000002);
	
}

// linha unica
function Ficha(item, pracas, pracasIndisponiveis, cenarios){
	this.item = item;
	this.pracas = pracas;
	this.pracasIndisponiveis = pracasIndisponiveis;
	this.cenarios = cenarios;
};

//add Subficha
function Subficha(item, pracas, pracasIndisponiveis){
	this.item = item;
	this.pracas = pracas;
	this.pracasIndisponiveis = pracasIndisponiveis;
}

//add ficha ficha
function FichaFilha(item, pracas, pracasIndisponiveis){
	this.item = item;
	this.pracas = pracas;
	this.pracasIndisponiveis = pracasIndisponiveis;
}

Ficha.prototype = {
		create: function(){
			// armazena o bjeto numa variavel
			var thisObj = this;

			// armazena as pracas existentes para o job estando ativas ou nao
			var todasPracasExistentes = Array();
			<?php foreach($pracas as $praca): ?>
				todasPracasExistentes.push('<?php echo $praca['ID_PRACA']; ?>')
			<?php endforeach; ?>
			
			// pega o html da linha
			var novoModelo = modelo.clone();
			novoModelo.attr('id', 'ficha' + this.item['ID_FICHA']);
			var linha = $j('<div></div>').append(novoModelo).html().replace(/%7B/g,'{').replace(/%7D/g,'}');

			// pega os place holders
			var reg = linha.match(/\{(\w+)\}/g);
			
			// para cada resultado encontrado
			for(var i in reg){
				// se for um indice numerico
				if( !isNaN(i) ){
					// pega a chave
					var key = reg[i].replace('{','').replace('}','');
					// substitui pelos valores do objeto
					linha = linha.replace(reg[i], this.item[ key ]);
				}
			}
			
			// transforma os valores substituidos em elemento jquery
			var el = $j(linha);
		
			// Preenchemos todas as checkbox de pracas que estiverem marcadas
			if(this.pracas.length > 0){
				$j.each(this.pracas, function(key, item){
					// coloca a checkbox da praca marcada
					el.find('.praca_' + item).attr('checked',true);
				});
			}
			else{
				el.find('input').attr('checked', true);
			}

			// Preencvhemos todas as combobox de cenario, e a ativamos ou desatimavamos conforme seu estado
			if( todasPracasExistentes.length > 0 ){
				$j.each( todasPracasExistentes, function(){
					// pega o ID do Cenario do excel item
					var idCenario = thisObj.item['CENARIO_PRACA_' + this];

					$j.each( el.find('.cenario_praca_' + this), function(){
						var comboCenario = this;

						$j(comboCenario).change(function() {
							if( $j(this).val() > 0 ){
								new Ajax("<?php echo site_url('json/cenario/getCor'); ?>", {
									method: 'post',
									onComplete: function(retorno){
										$j(comboCenario).closest('td').css('background-color', retorno);
									}
								}).request('idCenario=' + $j(this).val());
							}
							else{
								$j(comboCenario).closest('td').css('background-color', 'transparent');
							}
						});

						if( thisObj.cenarios.length > 0 ){
							montaOptions(comboCenario, thisObj.cenarios, 'ID_CENARIO', 'NOME_CENARIO', idCenario);
	
							if( $j(comboCenario).closest('td').find('input:checkbox').size() == 1 ){
								$j.each($j(comboCenario).closest('td').find('input:checkbox'), function(){
									if(this.checked){
										comboCenario.disabled = false;
									}
									else{
										comboCenario.disabled = true;
									}
								});
							}
						}
						else{
							comboCenario.disabled = true;
							$j(comboCenario).hide();
							$j(comboCenario).closest('td').find('br').hide();
						}

						if( idCenario > 0 ){
							new Ajax("<?php echo site_url('json/cenario/getCor'); ?>", {
								method: 'post',
								onComplete: function(retorno){
									$j(comboCenario).closest('td').css('background-color', retorno);
								}
							}).request('idCenario='+idCenario);
						}
					});
				});
			}

			// para cada praca que tem item marcando disponibilidade
			$j.each(this.pracasIndisponiveis, function(key, item){
				// se estiver marcado que nao esta disponivel
				el.find('.indisponivel_' + item)
					.show()
					.parent()
					.attr('bgColor','#FFFF99');
			});

			// se nao tem permissao de acessar
			if( $j.inArray( parseFloat(this.item.ID_CATEGORIA), categorias_usuario) == -1 ){
				el.find('td').css('opacity', .3);
				el.find('.cb').each(function(){
					this.onclick = function(){
						this.checked = !this.checked;
					};
				});
				el.find('.sl').each(function(){
					this.disabled = true;
				});
			} else {
				var idFicha = this.item.ID_FICHA;

				el.find('.cb').each(function(){
					this.onclick = function(){
						var chk = this;

						$j(this).closest('td').find('.sl').each(function(){
							if(chk.checked){
								this.disabled = false;
								$j(this).val('0');
							}
							else{
								$j(this).val('0');
								this.disabled = true;
								$j(this).closest('td').css('background-color', 'transparent');
							}
						});
					};
				});

				if(this.item['PENDENCIAS_DADOS_BASICOS'] != '' || this.item['PENDENCIAS_DADOS_BASICOS'] == undefined){
					el.find('#pendencias_' + this.item['ID_FICHA']).attr('class', 'acesso');
					
				}
				
				// quando clicar no botao de remover
				el.find('.remover').click(function(){			
					// para cada checkbox encontrada
					$j(this).closest('tr').find('.cb').each(function(){
						// se estiver selecionado
						if( this.checked ){
							// separa os valores
							var p = this.id.split('-');
							// pega a celula onde tem o valor
							var td = $j('#sub-'+p[1]+'-'+p[3]).find('.qtde');
							// atualiza o valor
							td.html( parseFloat(td.html()) + 1 );
						}
					});

					//remove as filhas
					$j('#tableObjeto').find('.modelo_ficha_pauta_filha_' + idFicha).remove();

					// remove a linha
					$j(this).closest('tr').remove();
				});
				
				// handler para quando marcar/desmarcar um item de uma praca
				el.find('.cb').click(atualizar_qtde_handler);
			}


			//adiciona todos os itens a um ARRAY para uma tentativa de melhorar o desempenho
			//itensTabela.push(el);
			// coloca a linha na tabela
			$j('#tabela_itens').append( el );

			// fancybox
			$j('.fancybox').fancybox();
			
			// atualiza as quantidades
			atualiza_quantidades(this);
		}
	}

Subficha.prototype = {
	create: function(){
		this.item['ID_FICHA1'] = this.item['ID_FICHA_PAI'];
		
		var novoModelo = modelo_subficha.clone();
		novoModelo.attr('class', 'modelo_ficha_pauta_filha_' + this.item['ID_FICHA1']);
		novoModelo.find('td').attr('class', 'modelo_ficha_pauta_filha');
		novoModelo.attr('id', 'ficha_filha_' + this.item['ID_FICHA1']);

		
				
		// html da linha
		var linha = $j('<div></div>').append(novoModelo).html().replace(/%7B/g,'{').replace(/%7D/g,'}');

		// place holders
		var reg = linha.match(/\{(\w+)\}/g);

		// para cada resultado encontrado
		for(var i in reg){
			// se for um indice numerico
			if( !isNaN(i) ){
				// pega a chave
				var key = reg[i].replace('{','').replace('}','');
				// substitui pelos valores do objeto
				if(this.item[ key ] == null){
					linha = linha.replace(reg[i], '');
				}
				else{
					linha = linha.replace(reg[i], this.item[ key ]);
				}
			}
		}
		//console.log(this.item);

		// transforma os valores substituidos em elemento jquery
		var el = $j(linha);

		//deleta o icone de exibir fichas filhas da ficha filha
		el.find('.exibe_filhas_' + this.item.ID_FICHA).remove();

		//deixa o botao de exibir filhas visivel na ficha pai
		$j('.exibe_filhas_' + this.item.ID_FICHA1).show();

		var idFichaPai = this.item.ID_FICHA1;
		var idFicha = this.item.ID_FICHA;

		// para cada praca desta ficha
		if(this.pracas.length > 0){
			$j.each(this.pracas, function(key, item){
				// coloca a checkbox da praca marada
				el.find('.praca_' + item).attr('checked', true);
				el.find('.praca_' + item).css('opacity', .6);
			});
		}
		else{
			el.find('input').attr('checked', true);
		}

		if(this.item['PENDENCIAS_DADOS_BASICOS'] != '' || this.item['PENDENCIAS_DADOS_BASICOS'] == undefined){
			el.find('#pendencias_' + this.item['ID_FICHA']).attr('class', 'acesso');
			
		}
		// para cada praca que tem item marcando disponibilidade
		$j.each(this.pracasIndisponiveis, function(key, item){
			// se estiver marcado que nao esta disponivel
			el.find('.indisponivel_' + item)
				.show()
				.parent()
				.attr('bgColor','#FFFF99');
		});

		// se nao tem permissao de acessar
		if( $j.inArray( parseFloat(this.item.ID_CATEGORIA), categorias_usuario) == -1 ){
			el.find('.cb').each(function(){
				this.onclick = function(){
					this.checked = !this.checked;
				};
			});
		} else {
			el.find('.remover').css('opacity', .6);
		}
		//remove os campos de formulario (os checkbox no caso)
		//el.find('input').remove();

		
		//el.append('<input type="hidden" name="filhas[' + this.item.ID_FICHA_PAI + '][' + this.item.ID_FICHA + ']" value="1"></input>');
		$j('#ficha' + this.item['ID_FICHA1']).after(el);

		
		el.hide();
		

		// fancybox
		$j('.fancybox').fancybox();

		//alteração feita para que seja exibido o objeto caso exista algum finculado à subficha
		if(this.item['FILE_ID'] == null){
			$j('#objetoSubFicha_'+this.item['ID_FICHA']).hide();
			$j('#setaFicha_'+this.item['ID_FICHA']).show();
		} else {
			$j('#objetoSubFicha_'+this.item['ID_FICHA']).show();
			$j('#setaFicha_'+this.item['ID_FICHA']).hide();
		}
	}
}

FichaFilha.prototype = {
	create: function(){
		var novoModelo = modelo_filha.clone();
		novoModelo.attr('class', 'modelo_ficha_pauta_filha_' + this.item['ID_FICHA_PAI']);
		novoModelo.find('td').attr('class', 'modelo_ficha_pauta_filha');
		novoModelo.attr('id', 'ficha_filha_' + this.item['ID_FICHA_PAI']);

		// html da linha
		var linha = $j('<div></div>').append(novoModelo).html().replace(/%7B/g,'{').replace(/%7D/g,'}');

		// place holders
		var reg = linha.match(/\{(\w+)\}/g);

		// para cada resultado encontrado
		for(var i in reg){
			// se for um indice numerico
			if( !isNaN(i) ){
				// pega a chave
				var key = reg[i].replace('{','').replace('}','');
				// substitui pelos valores do objeto
				linha = linha.replace(reg[i], this.item[ key ]);
			}
		}


		// transforma os valores substituidos em elemento jquery
		var el = $j(linha);

		//deleta o icone de exibir fichas filhas da ficha filha
		el.find('.exibe_filhas_' + this.item.ID_FICHA).remove();

		//deixa o botao de exibir filhas visivel na ficha pai
		$j('.exibe_filhas_' + this.item.ID_FICHA_PAI).show();

		var idFichaPai = this.item.ID_FICHA_PAI;
		var idFicha = this.item.ID_FICHA;

		// para cada praca desta ficha
		if(this.pracas.length > 0){
			$j.each(this.pracas, function(key, item){
				// coloca a checkbox da praca marada
				el.find('.praca_' + item).attr('checked', true);
				el.find('.praca_' + item).css('opacity', .6);
			});
		}
		else{
			el.find('input').attr('checked', true);
		}

		// para cada praca que tem item marcando disponibilidade
		$j.each(this.pracasIndisponiveis, function(key, item){
			// se estiver marcado que nao esta disponivel
			el.find('.indisponivel_' + item)
				.show()
				.parent()
				.attr('bgColor','#FFFF99');
		});

		// se nao tem permissao de acessar
		if( $j.inArray( parseFloat(this.item.ID_CATEGORIA), categorias_usuario) == -1 ){
			el.find('.cb').each(function(){
				this.onclick = function(){
					this.checked = !this.checked;
				};
			});
		} else {
			el.find('.remover').css('opacity', .6);
		}

		//remove os campos de formulario (os checkbox no caso)
		//el.find('input').remove();
		
		if(this.item['PENDENCIAS_DADOS_BASICOS'] != '' || this.item['PENDENCIAS_DADOS_BASICOS'] == undefined){
			el.find('#pendencias_' + this.item['ID_FICHA']).attr('class', '');
			
		}
		
		//el.append('<input type="hidden" name="filhas[' + this.item.ID_FICHA_PAI + '][' + this.item.ID_FICHA + ']" value="1"></input>');

		$j('#ficha' + this.item['ID_FICHA_PAI']).after(el);

		el.hide();

		// fancybox
		$j('.fancybox').fancybox();
	}
}

/**
 * funcao para atualizar o quadro de quantidades nas categorias
 * @param recebe um objeto de ficha como parametro
 */
function atualiza_quantidades(ficha){
	$j.each(ficha.pracas, function(){
		var td = $j('#sub-'+this+'-'+ficha.item.ID_SUBCATEGORIA).find('.qtde');
		td.html( parseFloat(td.html())-1 );
	});
}

// funcao para atualizar as quantidades quando clicar no
// checkbox de um produto
function atualizar_qtde_handler(evt){
	var p =this.id.split('-');

	var td = $j('#sub-'+p[1]+'-'+p[3]).find('.qtde');
	td.html( parseFloat(td.html())- (this.checked ? 1 : -1) );
}

// valida os itens antes de enviar para salvar
function validaItens(){
	// retorno do metodo
	var retorno = true;
	// mensagem de alerta
	var msg = '';
	
	// primeiro, vamos verificar se um item ficou sem praca
	$j('.linhaItem').each(function(){
		// por padrao nao passa
		var pass = false;
		// para cada checkbox da linha
		$j(this).find('.cb').each(function(){
			// se estiver checada
			if(this.checked){
				// entao pode passar
				pass = true;
			}
		});
		
		// se nenhum elemento da linha esta marcada
		if(!pass){
			// mensagem de alerta
			msg = '<?php echo ('Um ou mais itens ficaram sem praça'); ?>';
			// retorno falso
			retorno = false;
		}
	});
	
	// se o retorno for verdadeiro
	if( retorno == true ){
		// agora, vamos ver se uma praca ficou sem itens
		var pracas = [];
		// nomes encontrados
		var nomes = {};
		// para cada praca do PHP, colocamos no array de pracas e nomes
		<?php foreach($pracas as $praca): ?>
		pracas.push(<?php echo $praca['ID_PRACA']; ?>);
		nomes[<?php echo $praca['ID_PRACA']; ?>] = '<?php echo str_replace("'",'',$praca['DESC_PRACA']); ?>';
		<?php endforeach; ?>
		
		// para cada praca
		$j.each(pracas, function(){
			// padrao de retorno e false
			var pass = false;
			// para cada checkox desta praca
			$j('.praca_' + this).each(function(){
				// se pelo menos uma estiver marcada
				if(this.checked){
					// pode passar
					pass = true;
				}
			});
			
			// se nao puder passar
			if( !pass ) {
				// indica a mensagem
				msg += <?php echo ("'\\nA praça ' + nomes[this] + ' está sem itens'"); ?>;
				// o retorno da funcao e false
				retorno = false;
			}
		});
	}
	
	// se ainda assim o retorno eh true
	if( retorno == true ){
		/*
		// vamos checar o plano de marketing
		// nele, todos os itens devem estar com o valor zero
		// para cada plano de midida
		$j('.plano').each(function(){
			// mensagem temporaria
			var tmp = [];
			// pegamos o objeto
			var plano = $j(this);
			// para cada item
			plano.find('.item').each(function(){
				// se o valor nao estiver vazio
				if( parseFloat($j(this).find('.qtde').html()) != 0 ){
					// coloca a mensagem
					tmp.push('- '+$j(this).attr('title') + ' não está completo');
				}
			})
			
			// se encontrou problemas
			if( tmp.length > 0 ){
				// indica a mensagem
				msg += plano.attr('title') + '\n' + tmp.join('\n') + '\n';
				// indica que nao pode continuar
				retorno = false;
			}
		});
		*/
		
		// se deu problema
		if( !retorno ){
			retorno = confirm('Houve os seguintes problemas:\n\n'+msg+'\nDeseja continuar?');
			msg = '';
		}
	}
	
	if(!retorno && msg != ''){
		alert( msg );
	}
	
	return retorno;
}

// salva os dados do carrinho
function salvarCarrinho(){
	$j('#form1')
		.attr('action','<?php echo site_url('carrinho/save/'.$this->uri->segment(3)); ?>')
		.submit();

}

// exibe o plano de midia de uma determinada praca
function exibirPlanoMidia(idpraca){
	$j('#plano_midia_'+idpraca).dialog({
		autoOpen: false,
		width: 300,
		modal: false
	}).dialog('open');
}

// seleciona as filhas, ou 'deseleciona'
function selecionaFilhas(obj, idPraca, idFicha){
	$j('[id^=produtoFilha'+idFicha+'].praca_'+idPraca).each(function(){
		this.checked = obj.checked;
	});
}

//seleciona as filhas, ou 'deseleciona'
function selecionaPai(obj, idPraca, idFicha){
	$j('[id^=produtoPai'+idFicha+'].praca_'+idPraca).each(function(){
		if(obj.checked){
			if(!this.checked){
				this.checked = true;
			}
		}
	});
}

<?php if($podePassarEtapa && $this->uri->segment(3) != ''): ?>
function proximaEtapa(){
	if(!validaItens()){
		return;
	}
	$j('#form1')
		.attr('action','<?php echo site_url('carrinho/proxima_etapa/'.$this->uri->segment(3)); ?>')
		.submit();
}
<?php endif; ?>

function proximaEtapaSemItem(){
	$j('#form1')
		.attr('action','<?php echo site_url('carrinho/proximaEtapaSemItem/'.$this->uri->segment(3)); ?>')
		.submit();
}

</script>
</div>
<!-- Final de Content Global -->

<?php $this->load->view("ROOT/layout/footer") ?>