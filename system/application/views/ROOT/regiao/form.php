<?php $this->load->view("ROOT/layout/header"); ?>

<div id="contentGlobal">
<h1>Cadastro de regi&atilde;o</h1>
<form method="post" action="<?php echo site_url('regiao/save/'.$this->uri->segment(3)); ?>" id="form1">
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabela_pesquisa tabela_form">
  <tr>
    <td width="13%" align="left">Cliente</td>
    <td width="87%" align="left"><?php if(!empty($_sessao['ID_CLIENTE'])): ?>
      <?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?>
      <?php else: ?>
      <select name="ID_CLIENTE" id="ID_CLIENTE">
        <option value=""></option>
        <?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', post('ID_CLIENTE',true)); ?>
      </select>
    <?php endif; ?></td>
  </tr>
  <tr>
    <td align="left">Nome</td>
    <td align="left"><input name="NOME_REGIAO" id="NOME_REGIAO" type="text" value="<?php post('NOME_REGIAO'); ?>" />
    <?php showError('NOME_REGIAO'); ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><a class="button" href="<?php echo site_url('regiao/lista'); ?>" title="Pesquisa"><span>Cancelar</span></a>
     <a class="button" title="Pesquisa" onclick="$j(this).closest('form').submit()"><span>Gravar</span></a></td>
  </tr>
</table>
</form>
</div>

<script type="text/javascript">
</script>

<?php //$this->load->view("ROOT/layout/footer") ?>