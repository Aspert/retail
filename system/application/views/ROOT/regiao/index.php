<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
<h1>Cadastro de regi&otilde;es</h1>
<form method="post" action="<?php echo site_url('regiao/lista'); ?>" id="pesquisa_simples">
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tableSelection">
  <tr>
    <td width="10%" rowspan="2" align="center">
	<?php if($podeAlterar): ?>
    <a href="<?php echo site_url('regiao/form'); ?>"><img src="<?php echo base_url().THEME; ?>/img/file_add.png" alt="Adicionar" width="31" height="31" border="0" /></a>
    <?php endif; ?></td>
    <td width="25%">Cliente</td>
	<td width="25%">Regiao</td>
    <td width="20%">Itens / p&aacute;gina</td>
    <td width="20%" rowspan="2"><a class="button" title="Pesquisa" onclick="$j(this).closest('form').submit()"><span>Pesquisa</span></a></td>
  </tr>
  <tr>
    <td>
		<?php if(!empty($_sessao['ID_CLIENTE'])): ?>
			<?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?>
		<?php else: ?>
        <select name="ID_CLIENTE" id="ID_CLIENTE">
            <option value=""></option>
            <?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', empty($busca['ID_CLIENTE']) ? '' : $busca['ID_CLIENTE']); ?>
        </select>
    <?php endif; ?>
    </td>
    <td><input type="text" name="NOME_REGIAO" id="NOME_REGIAO" value="<?php echo empty($busca['NOME_REGIAO']) ? '' : $busca['NOME_REGIAO']?>" /></td>
    <td>
      <select name="pagina" id="pagina">
        <?php
		$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
		for($i=5; $i<=50; $i+=5){
			printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
		}
		?>
      </select>
    </td>
    </tr>
</table>
</form>

<?php if( !empty($regioes)) :?>
<br />
    <div id="tableObjeto">
    <table cellspacing="1" cellpadding="2" border="0" width="99%" class="tableSelection">
            <thead>
                <tr>
                    <?php if($podeAlterar): ?>
                    <th width="9%">Editar</th>
                    <?php endif; ?>
                  <th width="32%">Cliente</th>
                  <th width="27%">Regiao</th>
                </tr>
            </thead>

            <?php foreach($regioes as $item): ?>
            <tr>
              <?php if($podeAlterar): ?>
                <td align="center"><a href="<?php echo site_url('regiao/form/'.$item['ID_REGIAO']); ?>">
                <img src="<?php echo base_url().THEME; ?>/img/file_edit.png" alt="Editar" border="0" />
                </a></td>
              <?php endif; ?>
                <td align="center"><?php echo $item['DESC_CLIENTE']; ?></td>
                <td align="center"><?php echo $item['NOME_REGIAO']; ?></td>
            </tr>
            <?php endforeach; ?>
    </table>
    </div>
    
    <div class="paginacao">
	    <?php echo $paginacao; ?>
    </div>
</div>

<?php else: ?>
	Nenhum resultado
<?php endif; ?>

<script type="text/javascript">
</script>

<?php $this->load->view("ROOT/layout/footer") ?>