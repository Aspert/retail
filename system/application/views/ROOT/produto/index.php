<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
  <h1>Bandeiras</h1>
  <div id="pesquisa_simples"> <?php echo form_open('produto/lista');?>
    <table width="100%" border="0" cellspacing="1" cellpadding="2" class="Alignleft">
      <tr>
        <?php if($podeAlterar): ?>
        <td width="10%" rowspan="2" align="center"><a href="<?php echo site_url('produto/form'); ?>"><img src="<?php echo base_url().THEME; ?>img/file_add.png" alt="Adicionar" width="31" height="31" border="0" /></a></td>
        <?php endif; ?>
        <td width="17%">Cliente</td>
        <td width="23%">Bandeira</td>
        <td width="22%">Itens por p&aacute;gina</td>
        <td width="12%" rowspan="2"><a href="javascript:;" onclick="$j(this).closest('form').submit()" class="button"><span>Pesquisar</span></a></td>
      </tr>
      <tr>
        <td><select name="ID_CLIENTE" id="ID_CLIENTE">
            <option value=""></option>
            <?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', !empty($busca['ID_CLIENTE']) ? $busca['ID_CLIENTE'] : ''); ?>
          </select></td>
        <td><input type="text" name="DESC_PRODUTO" id="DESC_PRODUTO" value="<?php echo !empty($busca['DESC_PRODUTO']) ? $busca['DESC_PRODUTO'] : '' ?>" /></td>
        <td><span class="campo esq">
          <select name="pagina" id="pagina">
            <?php
		$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
		for($i=5; $i<=50; $i+=5){
			printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
		}
		?>
          </select>
          </span></td>
      </tr>
    </table>
    <?php echo form_close();?> </div>
  <?php if( (!empty($lista)) && (is_array($lista))):?>
  <div id="tableObjeto">
  <table border="0" width="100%" class="tableSelection">
    <thead>
        <tr>
          <?php if($podeAlterar): ?>
          <td width="6%">Editar</td>
          <?php endif; ?>
          <td width="48%" align="left">Cliente</td>
          <td width="25%" align="left">Bandeira</td>
          <td width="21%" align="left">Situa&ccedil;&atilde;o</td>
        </tr>
    </thead>
    <?php $cont=0; ?>
    <?php foreach($lista as $item):?>
    <tr>
      <?php if($podeAlterar): ?>
      <td align="center"><a href="<?php echo site_url('produto/form/'.$item['ID_PRODUTO']); ?>"><img src="<?php echo base_url().THEME; ?>img/file_edit.png" alt="Alterar" width="31" height="31" border="0" /></a></td>
      <?php endif; ?>
      <td><?=$item['DESC_CLIENTE'];?></td>
      <td><?=$item['DESC_PRODUTO'];?></td>
      <td><?=$item['STATUS_PRODUTO'] == 1 ? 'ATIVO' : 'INATIVO';?></td>
      <?php $cont++; ?>
    </tr>
    <?endforeach;?>
  </table>
  </div>
  <span class="paginacao">
  <?=(isset($paginacao))?$paginacao:null?>
  </span>
  <?else:?>
  Nenhum resultado
  <?endif;?>
</div>

<?php $this->load->view("ROOT/layout/footer") ?>
