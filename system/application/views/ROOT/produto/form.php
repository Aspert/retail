<?php $this->load->view("ROOT/layout/header"); ?>

<div id="contentGlobal">
  <?php if ( isset($erros) && is_array($erros) ) : ?>
  <ul style="color:#ff0000;font-weight:bold;">
    <?php foreach ( $erros as $e ) : ?>
    <li>
      <?=$e?>
    </li>
    <?php endforeach; ?>
  </ul>
  <?php endif; ?>
  <?php echo form_open_multipart('produto/save/'.$this->uri->segment(3));?>
  <h1>Manter Bandeiras</h1>
  <table width="100%" class="Alignleft">
    <tr>
      <td width="14%">Cliente *</td>
      <td width="86%"><?php if($this->uri->segment(3) == ''): ?>
        <select name="ID_CLIENTE" id="ID_CLIENTE">
          <option value=""></option>
          <?php echo montaOptions($clientes, 'ID_CLIENTE','DESC_CLIENTE', post('ID_CLIENTE',true)); ?>
        </select>
        <?php else: ?>
        <?php post('DESC_CLIENTE'); ?>
        <input name="DESC_CLIENTE" id="DESC_CLIENTE" type="hidden" value="<?php post('DESC_CLIENTE'); ?>" />
        <input name="ID_CLIENTE" id="ID_CLIENTE" type="hidden" value="<?php post('ID_CLIENTE'); ?>" />
        <?php endif; ?></td>
    </tr>
    <tr>
      <td>Nome *</td>
      <td><input name="DESC_PRODUTO" id="DESC_PRODUTO" type="text" value="<?php post('DESC_PRODUTO'); ?>" /></td>
    </tr>
    <tr>
      <td>Situa&ccedil;&atilde;o *</td>
      <td><select name="STATUS_PRODUTO">
          <option value="1">ATIVO</option>
          <option value="0"<?php echo isset($_POST['STATUS_PRODUTO']) && $_POST['STATUS_PRODUTO'] == '0' ? ' selected="selected"' : ''; ?>>INATIVO</option>
        </select></td>
    </tr>
    <tr>
    	<td valign="top">Ag&ecirc;ncias</td>
    	<td>
		<select name="agencias[]" id="agencias" multiple="multiple" size="6">
		<?php
		foreach($agencias as $item){
			echo '<option value="'.$item['ID_AGENCIA'].'"';
			if( isset($_POST['agencias']) && in_array($item['ID_AGENCIA'], $_POST['agencias']) ){
				echo ' selected="selected"';
			}
			echo '>' . $item['DESC_AGENCIA'] . '</option>' . PHP_EOL;
		}
		?>
		</select>
		</td>
    	</tr>
    <tr>
    	<td>Regi&otilde;es</td>
    	<td><select name="regioes[]" id="regioes" multiple="multiple" size="6">
   		<?php
		foreach($regioes as $item){
			echo '<option value="'.$item['ID_REGIAO'].'"';
			if( isset($_POST['regioes']) && in_array($item['ID_REGIAO'], $_POST['regioes']) ){
				echo ' selected="selected"';
			}
			echo '>' . $item['NOME_REGIAO'] . '</option>' . PHP_EOL;
		}
		?>
    		</select></td>
    	</tr>
    <tr>
    	<td>Transformador</td>
    	<td><select name="ID_TRANSFORMADOR" id="ID_TRANSFORMADOR">
    		<option value=""></option>
    		<?php echo montaOptions($transformadores,'ID_TRANSFORMADOR','NOME',post('ID_TRANSFORMADOR',true)); ?>
    		</select></td>
    	</tr>
    <tr>
    <tr>
    	<td>Calendário</td>
    		<td>
	    		<select name="ID_CALENDARIO" id="ID_CALENDARIO">
	    		<option value=""></option>
				<?php foreach($calendarios as $c): ?>
					<?php if($c['ID_CALENDARIO'] == post('ID_CALENDARIO', true)): ?>
						<option value='<?php echo $c['ID_CALENDARIO']; ?>' selected='selected'><?php echo $c['NOME_CALENDARIO']; ?></option>
					<?php else : ?>
						<option value='<?php echo $c['ID_CALENDARIO']; ?>'><?php echo $c['NOME_CALENDARIO']; ?></option>
					<?php endif; ?>
				<?php endforeach; ?>
	    		</select>
    		</td>
    	</tr>
    <tr>
      <td>&nbsp;</td>
      <td><a href="<?php echo site_url('produto/lista'); ?>" class="button"><span>Cancelar</span></a> <a href="javascript:;" onclick="salvar(this);" class="button"><span>Salvar</span></a></td>
    </tr>
  </table>
  <?php echo form_close();?>
</div>

<?php 

// monta a mensagem que aparecera caso o usuario altere o calendario
$msgConfirmaCalendario = '';

if(!empty($jobsProduto)){
	$msgConfirmaCalendario = 'Com a mudança de calendário, os jobs abaixo poderão ter suas datas alteradas e consequentemente bloqueados. São os seguintes:\n\n';
	foreach($jobsProduto as $j){
		$msgConfirmaCalendario .= $j['ID_JOB'] . ' - ' . $j['TITULO_JOB'] . '\n'; 		
	}
	$msgConfirmaCalendario .= '\nDeseja continuar?';
} 

?>


<script type="text/javascript">

	var msgConfirmaCalendario = '<?php echo $msgConfirmaCalendario; ?>';

	// aramzena a ID_CALENDARIO de quando carrega a tela e casa seja alterada poder mostrar o confirm
	var idCalendario = '<?php post('ID_CALENDARIO'); ?>';

	// salva o formulario
	function salvar(obj){
		if(idCalendario != ''){
			if(idCalendario != $j('#ID_CALENDARIO').val()){
				if(msgConfirmaCalendario != '') {
					if(confirm(msgConfirmaCalendario)){
						$j(obj).closest('form').submit();
					}
				} else {
					$j(obj).closest('form').submit();
				}	
			} else {
				$j(obj).closest('form').submit();
			}
		}
		else{
			$j(obj).closest('form').submit();
		}
	}
	
	$('ID_CLIENTE').addEvent('change', function(){
		clearSelect($('agencias'),0);
		montaOptionsAjax($('agencias'),'<?php echo site_url('json/admin/getAgenciasByCliente'); ?>','id=' + this.value,'ID_AGENCIA','DESC_AGENCIA');
		montaOptionsAjax($('regioes'),'<?php echo site_url('json/admin/getRegiaoByCliente'); ?>','id=' + this.value,'ID_REGIAO','NOME_REGIAO');
		clearSelect($('ID_CALENDARIO'), 1);
		montaOptionsAjax($('ID_CALENDARIO'),'<?php echo site_url('json/admin/getCalendarioByCliente'); ?>','id=' + this.value,'ID_CALENDARIO','NOME_CALENDARIO');
	});

</script>
<?php $this->load->view("ROOT/layout/footer") ?>
