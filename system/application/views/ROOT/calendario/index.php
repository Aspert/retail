<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
	<h1>Cadastro de Calendário</h1>
	
	<?php
	if( !empty($msg) ){
		echo '<div class="box_erros">'.$msg.'</div>';
	}
	?>
	
	<form method="post" action="<?php echo site_url('calendario/lista'); ?>" id="pesquisa_simples">
		<table width="100%" class="Alignleft">
			<tr>
				<td width="10%" rowspan="2" align="center">
					<?php if($podeAlterar): ?>
						<a href="<?php echo site_url('calendario/form'); ?>">
							<img src="<?php echo base_url().THEME; ?>img/file_add.png" alt="Novo calendário" title="Novo calendário" width="31" height="31" border="0" />
						</a>
					<?php endif; ?>
				</td>
				<td width="20%">Cliente</td>
				<td width="25%">Nome</td>
				<td width="15%">Situação</td>
				<td width="20%">Itens / p&aacute;gina</td>
				<td width="10%" rowspan="2"><a class="button" href="javascript:" title="Pesquisar" onclick="$j(this).closest('form').submit()"><span>Pesquisar</span></a></td>
			</tr>
			<tr>
				<td>
					<?php
						if( !empty($_sessao['ID_CLIENTE']) ){
							echo sessao_hidden('ID_CLIENTE','DESC_CLIENTE');
						} else {
							echo '<select name="ID_CLIENTE" id="ID_CLIENTE">
								<option value=""></option>
								' . montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE',post('ID_CLIENTE',true)) . '
							</select>';
						}
					?>
				</td>
				<td>
					<input name="NOME_CALENDARIO" type="text" id="NOME_CALENDARIO" value="<?php echo val($busca, 'NOME_CALENDARIO'); ?>" size="30" />
				</td>
				<td>
					<select name="STATUS_CALENDARIO" id="STATUS_CALENDARIO">
						<option value=""></option>
						<option value="1" <?php if(val($busca, 'STATUS_CALENDARIO') == '1'){ echo "selected='selected'"; }?>>Ativo</option>
						<option value="0" <?php if(val($busca, 'STATUS_CALENDARIO') == '0'){ echo "selected='selected'"; }?>>Inativo</option>
					</select>
				</td>
				<td><span class="campo esq">
					<select name="pagina" id="pagina">
						<?php
							$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
							for($i=5; $i<=50; $i+=5){
								printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
							}
						?>
					</select>
					</span></td>
			</tr>
		</table>
	</form>
	<?php if( !empty($lista)) :?>
		<br />
		<div id="tableObjeto">
			<table cellspacing="1" cellpadding="2" border="0" width="100%" class="tableSelection">
				<thead>
					<tr>
						<?php if($podeAlterar == true): ?>
							<td width="10%"><strong>Editar</strong></td>
						<?php endif; ?>
						<td width="30%"><strong>Nome</strong></td>
						<td width="30%"><strong>Cliente</strong></td>
						<td width="30%"><strong>Situa&ccedil;&atilde;o</strong></td>
					</tr>
				</thead>
				<?php foreach($lista as $item): ?>
					<tr>
						<?php if($podeAlterar == true): ?>
							<td align="center">
                  				<a href="<?php echo site_url('calendario/form/' . $item['ID_CALENDARIO']); ?>" title="Editar">
									<img src="<?php echo base_url() . THEME; ?>/img/file_edit.png" />
                   				</a>
							</td>
						<?php endif; ?>
						<td>
							<?php echo $item['NOME_CALENDARIO']; ?>
						</td>
						<td>
							<?php echo $item['DESC_CLIENTE']; ?>
						</td>
						<td>
							<?php if( $item['STATUS_CALENDARIO'] == 1 ){ echo 'Ativo'; } else{echo 'Inativo';} ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
		</div>
		<span class="paginacao"><?php echo $paginacao; ?></span>
	<?php else: ?>
		N&atilde;o h&aacute; resultado para pesquisa
	<?php endif; ?>
</div>

<?php $this->load->view("ROOT/layout/footer") ?>
