<?php $this->load->view("ROOT/layout/header"); ?>

<div id="contentGlobal">

	<h1>Cadastro de Calendário</h1>
	
	<?php if ( !empty($erros) ) : ?>
	<ul style="color:#f00;font-weight:bold;" class="box_erros">
		<?php foreach ( $erros as $e ) : ?>
		<li>
			<?=$e?>
		</li>
		<?php endforeach; ?>
	</ul>
	<?php endif; ?>

	<form method="post" action="<?php echo site_url('calendario/save/'.$this->uri->segment(3)); ?>" id="form1">
		<input type="hidden" name="ID_CALENDARIO" value="<?php echo $this->uri->segment(3); ?>" />
    
		<table width="100%" class="Alignleft">
			<tr>
				<td width="15%">Cliente *</td>
				<td width="85%">
<?php
					if( !empty($_sessao['ID_CLIENTE']) ){
						echo sessao_hidden('ID_CLIENTE','DESC_CLIENTE');
					} else {
						echo '<select name="ID_CLIENTE" id="ID_CLIENTE">
							<option value=""></option>
							' . montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE',post('ID_CLIENTE',true)) . '
						</select>';
					}
?>
				</td>
			</tr>
			<tr>
				<td>Nome *</td>
				<td>
					<input type="text" name="NOME_CALENDARIO" id="NOME_CALENDARIO" value="<?php post('NOME_CALENDARIO') ?>" size="40">
				</td>
			</tr>
			<tr>
				<td>Situa&ccedil;&atilde;o *</td>
				<td>
					<select name="STATUS_CALENDARIO" id="STATUS_CALENDARIO">
						<option value="1">Ativo</option>
						<option value="0"<?php echo post('STATUS_CALENDARIO', true)=='0' ? ' selected="selected"' : ''; ?>>Inativo</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td>
					<strong>FERIADOS</strong>
				</td>
				<td></td>
			</tr>
			<tr>
				<td>Descrição</td>
				<td>
					<input type="text" name="NOME_FERIADO_ADD" id="NOME_FERIADO_ADD" value="" size="40">
				</td>
			</tr>
			<tr>
				<td>Data</td>
				<td>
					<input name="DATA_FERIADO_ADD" id="DATA_FERIADO_ADD" type="text"  size="10" maxlength="10" class="_calendar" value="" />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="" id="FLAG_TODOS_ANOS_ADD" value="S"> Repete todos os anos
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
			    	<a class="button" href="javascript:" onclick="adicionarNovoFeriado();">
			    		<span>Adicionar Feriado</span>
			    	</a>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan="2">

					<table width="100%" class="tabela_padrao">
						<thead>
							<tr>
								<th width="50%">
									Nome
								</th>
								<th width="25%">
									Data
								</th>
								<th width="20%">
									Repete todos os anos
								</th>
								<th width="5%">
									Excluir
								</th>
							<tr>
						</thead>
						<tbody>
							<tr id="modelo">
								<td>
									<input name="ID_CALENDARIO_FERIADO[]" type="hidden" id="ID_CALENDARIO_FERIADO[]" value="{ID_CALENDARIO_FERIADO}" />
									<input name="NOME_FERIADO[]" type="text" id="NOME_FERIADO" class='NOME_FERIADO' value="{NOME_FERIADO}" size="50" />
								</td>
								<td>
									<input name="DATA_FERIADO[]" type="text" id="DATA_FERIADO_{ID_CALENDARIO_FERIADO}" class='DATA_FERIADO _calendar' value="{DATA_FERIADO}" size="10" />
								</td>
								<td>
									<input type="checkbox" class="FLAG_TODOS_ANOS" id="FLAG_TODOS_ANOS_{ID_CALENDARIO_FERIADO}" value="{FLAG_TODOS_ANOS}" onClick="preencheCalendarioHidden(this);">
									<input name="FLAG_TODOS_ANOS[]" class="FLAG_TODOS_ANOS_HIDDEN" type="hidden" value="N" />
								</td>
								<td align="center">
									<a href="#" title="Excluir" onclick="$j(this).closest('tr').remove();">
										<img src="<?php echo base_url().THEME.'img/file_delete.png'; ?>" />
									</a>
								</td>
							<tr>
						</tbody>
					</table>

				</td>
			</tr>
        </table>

        <br><br>    

		<div class="clearBoth"></div>

   		<a class="button" href="<?php echo site_url('calendario/lista'); ?>">
   			<span>Cancelar</span>
   		</a>
    	<a class="button" href="javascript:" onclick="validaFormulario();">
    		<span>Salvar</span>
    	</a>	
		
		<div>&nbsp;</div>
		
	</form>
	
</div> 
<!-- Final de Content Global -->

<script type="text/javascript">
	//FUNCAO PARA ACIDIONAR FERIADO NA PAUTA DE FERIADOS
	function adicionarFeriado(id, nome, data, repete){
		var html = $j('<div></div>').append(modelo.clone()).html();
	
		var obj = {
			ID_CALENDARIO_FERIADO: id,
			NOME_FERIADO: nome,
			DATA_FERIADO: data,
			FLAG_TODOS_ANOS: repete
		};
	
		var m = null;

		while( (m = html.match(/\{(\w+)\}/)) ){		
			html = html.replace(m[0], obj[ m[1] ].toString());
		}

		var html = $j(html);

		if(obj['FLAG_TODOS_ANOS'] == 'S'){
			html.find('.FLAG_TODOS_ANOS').attr('checked', 'checked');
			html.find('.FLAG_TODOS_ANOS_HIDDEN').attr('value', 'S');
		}
		
		container.append( html );	
	}
	
	// armazena o container onde ficara a pauta de feriados
	container = $j('#modelo').parent();

	// armazena a "tr" modelo de feriado
	modelo = $j('#modelo').clone();

	// tira a id da "tr"
	modelo.attr('id','');

	// remove o modelo
	$j('#modelo').remove();
	
<?php
	// Dados que vem do PHP para montar a pauta de feriados
	if( !empty($feriados) ){
		foreach($feriados as $f){
			printf('adicionarFeriado(%d, "%s", "%s", "%s");' . PHP_EOL
				, $f['ID_CALENDARIO_FERIADO']
				, $f['NOME_FERIADO']
				, format_date($f['DATA_FERIADO'], 'd/m/Y')
				, $f['FLAG_TODOS_ANOS']
			);
		}
	}
?>
	
	// funcao q verifica se tem nome ou data repetidas
	function temRepetido(chave, valor){
		var retorno = false;
	
		$j('.' + chave).each( function(){
			if(this.value.toLowerCase() == valor.toLowerCase()){
				retorno = true;
			}
		});
		return retorno;
	}
	
	// funcao para preencher o hidden de TODOS ANOS
	function preencheCalendarioHidden(obj){
		if(obj.checked){
			$j(obj).parent().find('.FLAG_TODOS_ANOS_HIDDEN').attr('value', 'S');
		}
		else{
			$j(obj).parent().find('.FLAG_TODOS_ANOS_HIDDEN').attr('value', 'N');
		}
	}
	
	// funcao para adicionar uma nova categoria na pauta
	function adicionarNovoFeriado(){
		if($j('#NOME_FERIADO_ADD').val() == ""){
			alert('Digite o nome do feriado');
			$j('#NOME_FERIADO_ADD').select();
		}
		else if(temRepetido('NOME_FERIADO', $j('#NOME_FERIADO_ADD').val())){
			alert('Ja existe um feriado cadastrado com esse nome');
		}
		else if(temRepetido('DATA_FERIADO', $j('#DATA_FERIADO_ADD').val())){
			alert('Ja existe um feriado cadastrado nesta data');
		}
		else if($j('#DATA_FERIADO_ADD').val() == ""){
			alert('Selecione a data do feriado');
			$j('#DATA_FERIADO_ADD').select();
		}
		else{
			var check = 'N';
			
			if($j('#FLAG_TODOS_ANOS_ADD').attr('checked')){
				check = 'S';
			}
			adicionarFeriado(0, $j('#NOME_FERIADO_ADD').val(), $j('#DATA_FERIADO_ADD').val(), check);
			$j('#NOME_FERIADO_ADD').val('');
			$j('#DATA_FERIADO_ADD').val('');
		}
	}

	function validaFormulario(){
		var erros = '';
		var nomeRepetido = false;
		var dataRepetida = false;

		$j('.NOME_FERIADO').each( function(){
			var c = 0;
			var valor = this.value;
			
			$j('.NOME_FERIADO').each( function(){
				if(valor == this.value){
					c++;
				}
			});

			if(c > 1){
				nomeRepetido = true;
			}
		});

		$j('.DATA_FERIADO').each( function(){
			var c = 0;
			var valor = this.value;
			
			$j('.DATA_FERIADO').each( function(){
				if(valor == this.value){
					c++;
				}
			});

			if(c > 1){
				dataRepetida = true;
			}
		});
		
		if(dataRepetida == true && nomeRepetido == true){
			alert('Existem feriados com nomes repetidos \nExistem feriados com datas repetidas');
		}
		else if(nomeRepetido == true){
			alert('Existem feriados com nomes repetidos');
		}
		else if(dataRepetida == true){
			alert('Existem feriados com datas repetidas');
		}
		else{
			$j('#form1').submit();
		}
	} 
	
	$j('._calendar').datepicker({dateFormat: 'dd/mm/yy',showOn: 'button', buttonImage: '<?= base_url().THEME ?>img/calendario.png', buttonImageOnly: true});
</script>

<?php $this->load->view("ROOT/layout/footer") ?>