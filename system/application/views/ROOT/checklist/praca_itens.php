<?php
if( empty($itensPai) ){
	exit;
}
?>



	<tbody>
<?php
	$cor = '';
	$i=-1;
	$count = 0;
	$x = $offset;
	foreach($itensPai as $item):
		$cor = $cor == '#121212' ? '#000000' : '#121212';
		$classPraca = 'praca_' . $praca['ID_PRACA'];
		$i = $item['ID_FICHA'];

?>

<?php //print_rr($itensFilha);die;?>
		<tr class="item itemPauta" id="<?php echo $x++; ?>" idExcelItem="<?php echo $item['ID_EXCEL_ITEM']; ?>" idPricing="<?php echo $item['ID_PRICING']; ?>" bgcolor="<?php echo $item['DISPONIVEL_ITEM'] ? '' : '#FEFCD3'; ?>">
			<?php if ($podeEditarFicha):
			$idFicha = !empty($item['ID_FICHA']) ?  $item['ID_FICHA'] : $item['ID_SUBFICHA']?>
				<td><?php echo anchor('ficha/form/'.$idFicha, '<img src="'. base_url().THEME.'img/file_edit.png" />',array('title'=>'Editar Ficha'));?></td>
			<?php  endif; ?>
			
			<?php if($podeEditarProdutos): ?>
				<td><a href="#" class="btnRemove"><img src="<?php echo base_url().THEME; ?>img/trash.png" alt="Remover Ficha"  title="Remover Ficha" border="0" /> </a></td>
			<?php endif; ?>
			
			<td>
			<?php if(!empty($item['FILE_ID'])): ?>
			  <a href="<?= base_url(); ?>img.php?img_id=<?= $item['FILE_ID']; ?>&amp;rand=<?= rand()?>&amp;img_size=big&amp;a.jpg" class="jqzoom"> <img src="<?= base_url(); ?>img.php?img_id=<?= $item['FILE_ID']; ?>&amp;rand=<?= rand()?>&amp;a.jpg" title="<?=$item['NOME_FICHA'];?>" border="0" width="50" /></a>
			  <?php endif; ?>
			</td>
			<td align="center">
				<a class="exibe_filhas_<?php echo $item['ID_FICHA']; ?>_<?php echo $item['PAGINA_ITEM']; ?>_<?php echo $item['ORDEM_ITEM']; ?>" style="display:none;" id="exibe_filhas_<?php echo $item['ID_FICHA']; ?>_<?php echo $item['PAGINA_ITEM']; ?>_<?php echo $item['ORDEM_ITEM']; ?>" href="javascript:new Util().vazio();" title="Exibir Filhas" onclick="$j('.modelo_ficha_pauta_filha_<?php echo $item['ID_FICHA']; ?>_<?php echo $item['PAGINA_ITEM']; ?>_<?php echo $item['ORDEM_ITEM']; ?>').toggle();toggleImgFilha($j(this).closest('td'));">
					<img src="<?= base_url().THEME ?>img/open_filhas.png"  border="0" alt="Exibir Filhas"/>
				</a>
			</td>
			<td>
				<div class="indisponivel_<?php echo $item['ID_PRACA']; ?>" style="display:<?php echo $item['DISPONIVEL_ITEM'] ? 'none' : 'block'; ?>">

				  <img src="<?php echo base_url().THEME; ?>img/error.png" title="Item indisponivel" />
				</div>
				<?php echo substr($item['NOME_FICHA'],0,60); ?><br />
				(<?php echo $item['COD_BARRAS_FICHA']; ?>)
				
			</td>
			<td><input type="number" min="0" max="<?php echo $job['PAGINAS_JOB']; ?>" disabled="disabled"  class="campoEditavelPauta" nomeCampo="PAGINA_ITEM" value="<?php echo $item['PAGINA_ITEM']; ?>" /></td>
			<td><input type="number" min="0" disabled="disabled"  class="campoEditavelPauta" nomeCampo="ORDEM_ITEM" value="<?php echo $item['ORDEM_ITEM']; ?>" /></td>
			
			<td class="grupo1"><input type="text" disabled="disabled" class="campoEditavelPauta real" nomeCampo="PRECO_UNITARIO" value="<?php echo money($item['PRECO_UNITARIO']); ?>" /></td>
			<td class="grupo1"><input type="text" disabled="disabled"  class="campoEditavelPauta" nomeCampo="TIPO_UNITARIO" value="<?php echo $item['TIPO_UNITARIO']; ?>" /></td>
			<td class="grupo1"><input type="text" disabled="disabled"  class="campoEditavelPauta real" nomeCampo="PRECO_CAIXA" value="<?php echo money($item['PRECO_CAIXA']); ?>" /></td>
			<td class="grupo1"><input type="text" disabled="disabled"  class="campoEditavelPauta" nomeCampo="TIPO_CAIXA" value="<?php echo $item['TIPO_CAIXA']; ?>" /></td>
			
			<td class="grupo2"><input type="text" disabled="disabled"  class="campoEditavelPauta real" nomeCampo="PRECO_VISTA" value="<?php echo money($item['PRECO_VISTA']); ?>" /></td>
			<td class="grupo2"><input type="text" disabled="disabled"  class="campoEditavelPauta real" nomeCampo="PRECO_DE" value="<?php echo money($item['PRECO_DE']); ?>" /></td>
			<td class="grupo2"><input type="text" disabled="disabled"  class="campoEditavelPauta real" nomeCampo="PRECO_POR" value="<?php echo money($item['PRECO_POR']); ?>" /></td>
			<td class="grupo2"><input type="text" disabled="disabled"  class="campoEditavelPauta real" nomeCampo="ECONOMIZE" value="<?php echo money($item['ECONOMIZE']); ?>" /></td>
			
			<td class="grupo3"><input type="text" disabled="disabled"  class="campoEditavelPauta real" nomeCampo="ENTRADA" value="<?php echo money($item['ENTRADA']); ?>" /></td>
			<td class="grupo3"><input type="number" min="0" disabled="disabled"  class="campoEditavelPauta" nomeCampo="PARCELAS" value="<?php echo $item['PARCELAS']; ?>" /></td>
			<td class="grupo3"><input type="text" disabled="disabled"  class="campoEditavelPauta real" nomeCampo="PRESTACAO" value="<?php echo money($item['PRESTACAO']); ?>" /></td>
			<td class="grupo3"><input type="text" disabled="disabled"  class="campoEditavelPauta float " nomeCampo="JUROS_AM" value="<?php echo $item['JUROS_AM']; ?>" /></td>
			<td class="grupo3"><input type="text" disabled="disabled"  class="campoEditavelPauta float" nomeCampo="JUROS_AA" value="<?php echo $item['JUROS_AA']; ?>" /></td>
			<td class="grupo3"><input type="text" disabled="disabled"  class="campoEditavelPauta real" nomeCampo="TOTAL_PRAZO" value="<?php echo money($item['TOTAL_PRAZO']); ?>" /></td>
			
			<td class="grupo5"><input type="text" disabled="disabled"  class="campoEditavelPauta" nomeCampo="TEXTO_LEGAL" value="<?php echo $item['TEXTO_LEGAL']; ?>" /></td>
			<td class="grupo5"><input type="text" disabled="disabled"  class="campoEditavelPauta" nomeCampo="EXTRA1_ITEM" value="<?php echo $item['EXTRA1_ITEM']; ?>" /></td>
			<td class="grupo5"><input type="text" disabled="disabled"  class="campoEditavelPauta" nomeCampo="EXTRA2_ITEM" value="<?php echo $item['EXTRA2_ITEM']; ?>" /></td>
			<td class="grupo5"><input type="text" disabled="disabled"  class="campoEditavelPauta" nomeCampo="EXTRA3_ITEM" value="<?php echo $item['EXTRA3_ITEM']; ?>" /></td>
			<td class="grupo5"><input type="text" disabled="disabled"  class="campoEditavelPauta" nomeCampo="EXTRA4_ITEM" value="<?php echo $item['EXTRA4_ITEM']; ?>" /></td>
			<td class="grupo5"><input type="text" disabled="disabled"  class="campoEditavelPauta" nomeCampo="EXTRA5_ITEM" value="<?php echo $item['EXTRA5_ITEM']; ?>" /></td>
			
			<td class="grupo6"><?=ucfirst($item['DESC_APROVACAO_FICHA']);?></td>
			<td class="grupo6">
				<?= $item['FLAG_TEMPORARIO'] == 1? 'Sim' : 'Não';?>
				<?php alerta_ficha($item); ?>
			</td>
			<td class="grupo6"><?=ucfirst(strtolower(ghDigitalReplace($_sessao, $item['TIPO_FICHA'])));?></td>
			
			<td class="grupo4"><textarea class="campoEditavelPauta" nomeCampo="OBSERVACOES" disabled="disabled"><?php echo $item['OBSERVACOES']; ?></textarea></td>
		</tr>
		<?php foreach($itensFilha as $itemFilha): ?>
			<?php if($itemFilha['ID_FICHA_PAI'] == $item['ID_FICHA']): ?>
			<?php if($itemFilha['PAGINA_ITEM'] == $item['PAGINA_ITEM']): ?>
			<?php if($itemFilha['ORDEM_ITEM'] == $item['ORDEM_ITEM']): ?>
			
				<tr class="modelo_ficha_pauta_filha_<?php echo $item['ID_FICHA']; ?>_<?php echo $item['PAGINA_ITEM']; ?>_<?php echo $item['ORDEM_ITEM']; ?>" style="display:none;">
					<?php if ($podeEditarFicha):?>
						<td><?php echo anchor('ficha/form/'.$itemFilha['ID_FICHA'], '<img src="'. base_url().THEME.'img/file_edit.png" />',array('title'=>'Editar Ficha'));?></td>
					<?php  endif; ?>
					<td class="modelo_ficha_pauta_filha">
						<img src="<?= base_url().THEME ?>img/seta_filha.png" />
					</td>
					<td class="modelo_ficha_pauta_filha">
						<?php if(!empty($itemFilha['FILE_ID'])): ?>
						  <a href="<?= base_url(); ?>img.php?img_id=<?= $itemFilha['FILE_ID']; ?>&amp;rand=<?= rand()?>&amp;img_size=big&amp;a.jpg" class="jqzoom"> <img src="<?= base_url(); ?>img.php?img_id=<?= $itemFilha['FILE_ID']; ?>&amp;rand=<?= rand()?>&amp;a.jpg" title="<?=$itemFilha['NOME_FICHA'];?>" border="0" width="50" /></a>
						<?php endif; ?>
					</td>
					<td class="modelo_ficha_pauta_filha">
						<div class="indisponivel_<?php echo $itemFilha['ID_PRACA']; ?>" style="display:<?php echo $itemFilha['DISPONIVEL_ITEM'] ? 'none' : 'block'; ?>">
						  <img src="<?php echo base_url().THEME; ?>img/error.png" title="Item indisponivel" />
						</div>
						(<?php echo $itemFilha['COD_BARRAS_FICHA']; ?>)
						
					</td>
					<td class="modelo_ficha_pauta_filha"><?php echo $itemFilha['DESC_CATEGORIA']; ?></td>				
					<td class="modelo_ficha_pauta_filha"><?php echo $itemFilha['DESC_SUBCATEGORIA']; ?></td>
					<td class="modelo_ficha_pauta_filha"><input type="number" min="0" disabled="disabled"  class="campoEditavelPauta" value="<?php echo $itemFilha['PAGINA_ITEM']; ?>" /></td>
					<td class="modelo_ficha_pauta_filha"><input type="number" min="0" disabled="disabled"  class="campoEditavelPauta" value="<?php echo $itemFilha['ORDEM_ITEM']; ?>" /></td>
					
					<td class="modelo_ficha_pauta_filha grupo1"><input type="text" disabled="disabled"  class="campoEditavelPauta real" value="<?php echo money($itemFilha['PRECO_UNITARIO']); ?>" /></td>
					<td class="modelo_ficha_pauta_filha grupo1"><input type="text" disabled="disabled"  class="campoEditavelPauta" value="<?php echo $itemFilha['TIPO_UNITARIO']; ?>" /></td>
					<td class="modelo_ficha_pauta_filha grupo1"><input type="text" disabled="disabled"  class="campoEditavelPauta real" value="<?php echo money($itemFilha['PRECO_CAIXA']); ?>" /></td>
					<td class="modelo_ficha_pauta_filha grupo1"><input type="text" disabled="disabled"  class="campoEditavelPauta" value="<?php echo $itemFilha['TIPO_CAIXA']; ?>" /></td>
					
					<td class="modelo_ficha_pauta_filha grupo2"><input type="text" disabled="disabled"  class="campoEditavelPauta real" value="<?php echo money($itemFilha['PRECO_VISTA']); ?>" /></td>
					<td class="modelo_ficha_pauta_filha grupo2"><input type="text" disabled="disabled"  class="campoEditavelPauta real" value="<?php echo money($itemFilha['PRECO_DE']); ?>" /></td>
					<td class="modelo_ficha_pauta_filha grupo2"><input type="text" disabled="disabled"  class="campoEditavelPauta real" value="<?php echo money($itemFilha['PRECO_POR']); ?>" /></td>
					<td class="modelo_ficha_pauta_filha grupo2"><input type="text" disabled="disabled"  class="campoEditavelPauta real" value="<?php echo money($itemFilha['ECONOMIZE']); ?>" /></td>
					
					<td class="modelo_ficha_pauta_filha grupo3"><input type="text" disabled="disabled"  class="campoEditavelPauta real" value="<?php echo money($itemFilha['ENTRADA']); ?>" /></td>
					<td class="modelo_ficha_pauta_filha grupo3"><input type="number" min="0" disabled="disabled"  class="campoEditavelPauta" value="<?php echo $itemFilha['PARCELAS']; ?>" /></td>
					<td class="modelo_ficha_pauta_filha grupo3"><input type="text" disabled="disabled"  class="campoEditavelPauta real" value="<?php echo money($itemFilha['PRESTACAO']); ?>" /></td>
					<td class="modelo_ficha_pauta_filha grupo3"><input type="text" disabled="disabled"  class="campoEditavelPauta float" value="<?php echo $itemFilha['JUROS_AM']; ?>" /></td>
					<td class="modelo_ficha_pauta_filha grupo3"><input type="text" disabled="disabled"  class="campoEditavelPauta float" value="<?php echo $itemFilha['JUROS_AA']; ?>" /></td>
					<td class="modelo_ficha_pauta_filha grupo3"><input type="text" disabled="disabled"  class="campoEditavelPauta real" value="<?php echo money($itemFilha['TOTAL_PRAZO']); ?>" /></td>
					
					<td class="modelo_ficha_pauta_filha grupo5"><input type="text" disabled="disabled"  class="campoEditavelPauta" value="<?php echo $itemFilha['TEXTO_LEGAL']; ?>" /></td>
					<td class="modelo_ficha_pauta_filha grupo5"><input type="text" disabled="disabled"  class="campoEditavelPauta" value="<?php echo $itemFilha['EXTRA1_ITEM']; ?>" /></td>
					<td class="modelo_ficha_pauta_filha grupo5"><input type="text" disabled="disabled"  class="campoEditavelPauta" value="<?php echo $itemFilha['EXTRA2_ITEM']; ?>" /></td>
					<td class="modelo_ficha_pauta_filha grupo5"><input type="text" disabled="disabled"  class="campoEditavelPauta" value="<?php echo $itemFilha['EXTRA3_ITEM']; ?>" /></td>
					<td class="modelo_ficha_pauta_filha grupo5"><input type="text" disabled="disabled"  class="campoEditavelPauta " value="<?php echo $itemFilha['EXTRA4_ITEM']; ?>" /></td>
					<td class="modelo_ficha_pauta_filha grupo5"><input type="text" disabled="disabled"  class="campoEditavelPauta" value="<?php echo $itemFilha['EXTRA5_ITEM']; ?>" /></td>
					
					<td class="modelo_ficha_pauta_filha grupo6"><?=ucfirst($itemFilha['DESC_APROVACAO_FICHA']);?></td>
					<td class="modelo_ficha_pauta_filha grupo6">
						<?= $itemFilha['FLAG_TEMPORARIO'] == 1? 'Sim' : 'Não';?>
						<?php alerta_ficha($itemFilha); ?>
					</td>
					<td class="modelo_ficha_pauta_filha grupo6"><?=ucfirst(strtolower(ghDigitalReplace($_sessao, $itemFilha['TIPO_FICHA'])));?></td>
					
					<td class="modelo_ficha_pauta_filha grupo4"><input type="text" class="campoEditavelPauta" disabled="disabled"  value="<?php echo $itemFilha['OBSERVACOES']; ?>" /></td>
				</tr>
				<script>
					$j('#exibe_filhas_<?php echo $item["ID_FICHA"]; ?>_<?php echo $item['PAGINA_ITEM']; ?>_<?php echo $item['ORDEM_ITEM']; ?>').show();

					//alteração feita para pesquisar a subficha e trazer tanto a subficha quanto a ficha
					<?php if(isset($pesquisaSubficha)){?>
						$j('.modelo_ficha_pauta_filha_<?php echo $item['ID_FICHA']; ?>_<?php echo $item['PAGINA_ITEM']; ?>_<?php echo $item['ORDEM_ITEM']; ?>').toggle();toggleImgFilha($j(this).closest('td'));
						$j('#exibe_filhas_<?php echo $item['ID_FICHA']; ?>_<?php echo $item['PAGINA_ITEM']; ?>_<?php echo $item['ORDEM_ITEM']; ?>').children().attr('src','<?= base_url().THEME ?>img/close_filhas.png');
					<?php }?>
				</script>
				
			<?php endif; ?>
			<?php endif; ?>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endforeach; ?>
	<tr>
		<td colspan="21">
		P&aacute;ginas: |
			<?php for($i=0; $i<$totalProdutos/$limit; $i++): ?>
			<a id="pg<?php echo $i; ?>" class="itemPaginacao" href="#" limit="<?php echo $limit; ?>" offset="<?php echo $i*$limit; ?>">
				<?php echo $i*$limit==$offset ? '<strong>' .($i+1) . '</strong>' : $i+1; ?>
			</a> |
			<?php endfor; ?>
		</td>
	</tr>
		</tbody>
