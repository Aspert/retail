<div id="contentGlobal">
  <h1>Histórico de Alteração</h1>
  <div id="tableObjeto">
    <table style="width:100%;" border="0" cellpadding="0" cellspacing="0" class="tableSelection">
      <thead>
        <tr>
          <td>Autor</td>
          <td>Cliente</td>
          <td>Enviado para</td>
          <td>Situação</td>
          <td>Data</td>
        </tr>
      </thead>
      <?php if(!empty($historico) && is_array($historico)): ?>
      <?php foreach($historico as $item): ?>
      <tr>
        <td><?php echo $item['NOME_USUARIO'];?></td>
        <td><?php echo !empty($item['DESC_CLIENTE']) ? $item['DESC_CLIENTE'] : '';?></td>
        <td><?php echo $item['DESC_ETAPA'];?></td>
        <td><?php echo $item['DESC_STATUS'];?></td>
        <td><?php echo format_date_to_form($item['DATA_HISTORICO'], 'd/m/Y H:i:s'); ?></td>
      </tr>
      <?php endforeach; ?>
      <?php endif; ?>
    </table>
    <div style="margin: 20px 0 0 0;"> <a class="button" href="javascript:" onclick="closeMessage();"><span>Cancelar</span></a> </div>
  </div>
  <!-- Content Table Objeto-->
</div>
<!-- Final de Content Global -->
