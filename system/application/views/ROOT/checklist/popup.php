<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
	<head>
		<script language="JavaScript">
		var da = (document.all) ? 1 : 0;
		var pr = (window.print) ? 1 : 0;
		var mac = (navigator.userAgent.indexOf("Mac") != -1);
		
		function printPage() {
		  if (pr) // NS4, IE5
		    window.print()
		  else if (da && !mac) // IE4 (Windows)
		    vbPrintPage()
		  else // other browsers
		    alert("Desculpe seu Navegador n�o suporta impress�o automatica");
		  return false;
		}
		
		if (da && !pr && !mac) with (document) {
		  writeln('<OBJECT ID="WB" WIDTH="0" HEIGHT="0" CLASSID="clsid:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>');
		  writeln('<' + 'SCRIPT LANGUAGE="VBScript">');
		  writeln('Sub window_onunload');
		  writeln('  On Error Resume Next');
		  writeln('  Set WB = nothing');
		  writeln('End Sub');
		  writeln('Sub vbPrintPage');
		  writeln('  OLECMDID_PRINT = 6');
		  writeln('  OLECMDEXECOPT_DONTPROMPTUSER = 1');
		  writeln('  OLECMDEXECOPT_PROMPTUSER = 1');
		  writeln('  On Error Resume Next');
		  writeln('  WB.ExecWB OLECMDID_PRINT, OLECMDEXECOPT_DONTPROMPTUSER');
		  writeln('End Sub');
		  writeln('<' + '/SCRIPT>');
		}
		</script>
	
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>24\7 inteligencia digital</title>
		<link href="<?=base_url();?>css/style.css" rel="stylesheet" type="text/css"/>		
		<link href="<?=base_url();?>css/modal-message.css" rel="stylesheet" type="text/css"/>
		<link href="<?=base_url();?>css/calendar.css" rel="stylesheet" type="text/css"/>

		<script type="text/javascript" src="<?= base_url(); ?>js/mootools.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>js/common.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>js/util.js"></script>	
		
		<?php foreach ( array_diff(scandir('js/classes'), array('.', '..', 'CVS','.svn')) as $file) : ?>
			<script type="text/javascript" src="<?=base_url()?>js/classes/<?=$file?>"></script>
		<?php endforeach; ?>
		
		<script type="text/javascript" src="<?= base_url(); ?>js/calendar.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>js/modal-message.js"></script>
	 </head>	 
	
	<body  style="background: transparent url(<?=base_url()?>img/_bg.gif);padding:20px;" onLoad="printPage()">	
	 <script type="text/javascript">	
		var util = new Util();		
		util.options.base_url = '<?= base_url(); ?>';
		util.options.site_url = '<?= site_url(); ?>';				
	</script>
	<script type="text/javascript">
		messageObj = new DHTML_modalMessage();	// We only create one object of this class
		messageObj.setShadowOffset(5);	// Large shadow
				
		function displayMessage(url)
		{			
			messageObj.setSource(url);
			messageObj.setCssClassMessageBox(false);
			messageObj.setSize(800,500);
			messageObj.setShadowDivVisible(true);	// Enable shadow for these boxes
			messageObj.display();
		}
		
		function displayStaticMessage(messageContent)
		{
			messageObj.setHtmlContent(messageContent);
			messageObj.setSize(800,500);
			//messageObj.setCssClassMessageBox(cssClass);
			messageObj.setSource(false);	// no html source since we want to use a static message here.
			messageObj.setShadowDivVisible(false);	// Disable shadow for these boxes	
			messageObj.display();				
		}
		
		function closeMessage()
		{
			messageObj.close();	
		}
	</script>

<?=$html?>
</body>
</html>