<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.js"></script>
        <link href="<?=base_url().THEME;?>css/default.css" rel="stylesheet" type="text/css"/>
</head>
<body style="background-image:none">
<div id="contentBody" style="width:auto">
<div id="conteudoGeral" style="width:auto">
<!--  inicio formulario de aprovacao -->
<div id="contentGlobal" style="width:auto">

<h1>Checklist</h1>

<form id="formAprovacao" method="post" name="formAprovacao">

<!--  listando o historico de aprovacao/reprovacao -->
<h1>Histórico de Alteração</h1>

<!-- cabecalho fixo do historico -->
<div id="tableObjeto">
    <table width="100%" class="tableSelection">
        <thead>
           <tr>
              <th width="18%">Autor</th>
              <th width="17%">Enviado para</th>
              <th width="10%">Situação</th>
              <th width="10%">Data</th>
              <th width="45%">Comentário</th>
          </tr>
        </thead>
    </table>
</div>
<div style="clear: both"></div>
<!--  div para poder colocar a barra de rolagem -->
<div id="lista" style="overflow: auto; height: 205px; width: 100%; border-bottom: 2px solid #FF9900;">

  <!--  inicio historico  -->
  <table width="100%" border="0" cellpadding="2" cellspacing="0" class="tableSelection">
  <?php if(!empty($historico) && is_array($historico)): ?>
  <?php foreach($historico as $item): ?>
   <tr>
      <td class="linha_baixa" width="18%"><?php echo $item['NOME_USUARIO']?></td>
      <td class="linha_baixa" width="17%"><?php echo $item['DESC_ETAPA']?></td>
      <td class="linha_baixa" width="10%"><?php echo $item['DESC_STATUS']?></td>
      <td class="linha_baixa" width="10%"><?php echo format_date_to_form($item['DATA_JOB_APROVACAO']); ?></td>
      <td class="linha_baixa" width="45%"><?php echo $item['COMENTARIO_JOB_APROVACAO']?></td>
   </tr>
   <?php endforeach; ?>
   <?php endif; ?>
  <!--  fim historico  -->
  </table>

  <!--  fim da div container -->
</div>
<br />
<br />

<!-- inicio campos de aprovacao -->
<table width="98%" border="0" cellpadding="2" cellspacing="0" class="Alignleft">
   <tr>
     <td>Status: <span class="obrigatorio">*</span></td>
     <td><?php echo $status['DESC_STATUS']; ?></td>
   </tr>
   <?php if(!empty($etapas) && is_array($etapas) && !in_array($status['CHAVE_STATUS'],array('ELABORACAO','APROVADO','REPROVADO')) ): ?>
   <tr>
      <td>Enviar para: <span class="obrigatorio">*</span></td>
      <td><?php echo form_dropdown('ID_ETAPA',arraytoselect($etapas,'ID_ETAPA','DESC_ETAPA'),null, 'id="ID_ETAPA"'); ?></td>
   </tr>
   <?php endif; ?>
   <tr>
     <td valign="top">Comentário: <span class="obrigatorio">*</span></td>
     <td><textarea cols="50" rows="5" name="COMENTARIO_JOB_APROVACAO" id="COMENTARIO_JOB_APROVACAO"></textarea></td>
   </tr>
   <tr id="botoes">
     <td>&nbsp;</td>
     <td>
     <a href="javascript:;" onclick="window.opener.finalizarAprovacao($('#COMENTARIO_JOB_APROVACAO').val(), $('#ID_ETAPA').val());" class="button"><span>Concluir</span></a>
     </td>
   </tr>
</table>

<!-- inicio variaveis a serem enviadas por ajax -->
<input type="hidden" value="<?php echo $status['ID_STATUS']; ?>" name="ID_STATUS" id="ID_STATUS" />
<input type="hidden" value="<?php echo $job['ID_JOB']; ?>" name="ID_JOB" id="ID_JOB" />
<input type="hidden" value="<?php echo $status['CHAVE_STATUS']; ?>" name="CHAVE_STATUS" id="CHAVE_STATUS" />
<!-- fim variaveis a serem enviadas por ajax -->

<!-- fim campos de aprovacao -->

<!--  fim formulario de aprovacao -->
</form>

</div> </div></div><!-- Final de Content Global -->
</body></html>