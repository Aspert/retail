<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Relatório de Jobs</title>
    <style type="text/css">
        <!--
        td, th, div {
            font-family: Verdana, Geneva, sans-serif;
            font-size: 11px;
            border: 1px solid black;
            padding: 2px;
        }
        table { border-collapse: collapse;border-spacing: 0;
        }
        th {
            color: #000;
            border-bottom-width: 2px;
            border-bottom-style: solid;
            border-bottom-color: #333;
        }
        -->
    </style>
</head>

<body>

<table border="0" style="width:100%;border:1px solid black;">
    <tr style="background-color:black;color:white;text-align:center;">
        <td><strong>Relat&oacute;rio do Job</strong></td>
    </tr>
    <tr>
        <td>
            <?php if(!empty($EXPORTAR[0])){ ?>

                <table width="100%" border="<?php echo $this->uri->segment(4) == 'html' ? 0 : 1; ?>" cellspacing="0" cellpadding="2">
                    <tr>
                        <td colspan="2"><strong>N&uacute;mero do Job</strong></td>
                        <td colspan="6" align="left"><?php echo $EXPORTAR[0]['ID_JOB']; ?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Título do Job</strong></td>
                        <td colspan="6" align="left"><?php echo $EXPORTAR[0]['TITULO_JOB']; ?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Agência</strong></td>
                        <td colspan="6" align="left"><?php echo $EXPORTAR[0]['DESC_AGENCIA']; ?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Cliente</strong></td>
                        <td colspan="6" align="left"><?php echo $EXPORTAR[0]['DESC_CLIENTE']; ?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Campanha</strong></td>
                        <td colspan="6" align="left"><?php echo $EXPORTAR[0]['DESC_CAMPANHA']; ?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Bandeira</strong></td>
                        <td colspan="6" align="left"><?php echo $EXPORTAR[0]['DESC_PRODUTO']; ?></td>
                    </tr>
                    <tr>
                        <td colspan="8">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="5%" align="left" bgcolor="#E2E2E2"><strong>Praça</strong></td>
                        <td width="5%" align="left" bgcolor="#E2E2E2"><strong>Ordem</strong></td>
                        <td width="5%" align="left" bgcolor="#E2E2E2"><strong>Página</strong></td>
                        <td width="20%" align="left" bgcolor="#E2E2E2"><strong>Subcategoria</strong></td>
                        <td width="12%" align="left" bgcolor="#E2E2E2"><strong><?php ghDigitalReplace($_sessao, 'Código de Barras'); ?></strong></td>
                        <td width="30%" align="left" bgcolor="#E2E2E2"><strong><?php ghDigitalReplace($_sessao, 'Nome da ficha'); ?></strong></td>
                        <td width="30%" align="left" bgcolor="#E2E2E2"><strong>Imagem</strong></td>
                        <td width="30%" align="left" bgcolor="#E2E2E2"><strong><?php ghDigitalReplace($_sessao, 'Tipo de Ficha'); ?></strong></td>
                        <td width="30%" align="left" bgcolor="#E2E2E2"><strong>Texto Legal</strong></td>
                        <td width="30%" align="left" bgcolor="#E2E2E2"><strong>PREÇO UN.</strong></td>
                        <td width="30%" align="left" bgcolor="#E2E2E2"><strong>TIPO UN.</strong></td>
                        <td width="15%" align="left" bgcolor="#E2E2E2"><strong>PREÇO CX.</strong></td>
                        <td width="17%" align="left" bgcolor="#E2E2E2"><strong>TIPO CX.</strong></td>
                        <td width="18%" align="left" bgcolor="#E2E2E2"><strong>A VISTA</strong></td>
                        <td width="30%" align="left" bgcolor="#E2E2E2"><strong>DE </strong></td>
                        <td width="30%" align="left" bgcolor="#E2E2E2"><strong>POR </strong></td>
                        <td width="15%" align="left" bgcolor="#E2E2E2"><strong>ECONOMIZE</strong></td>
                        <td width="17%" align="left" bgcolor="#E2E2E2"><strong>ENTRADA</strong></td>
                        <td width="18%" align="left" bgcolor="#E2E2E2"><strong>PARCELAS</strong></td>
                        <td width="18%" align="left" bgcolor="#E2E2E2"><strong>PRESTAÇÃO</strong></td>
                        <td width="30%" align="left" bgcolor="#E2E2E2"><strong>JUROS AM</strong></td>
                        <td width="30%" align="left" bgcolor="#E2E2E2"><strong>JUROS AA</strong></td>
                        <td width="15%" align="left" bgcolor="#E2E2E2"><strong>TOTAL PRAZO</strong></td>
                        <td width="17%" align="left" bgcolor="#E2E2E2"><strong>OBERVACOES</strong></td>
                        <td width="18%" align="left" bgcolor="#E2E2E2"><strong>EXTRA1</strong></td>
                        <td width="30%" align="left" bgcolor="#E2E2E2"><strong>EXTRA2</strong></td>
                        <td width="30%" align="left" bgcolor="#E2E2E2"><strong>EXTRA3</strong></td>
                        <td width="15%" align="left" bgcolor="#E2E2E2"><strong>EXTRA4</strong></td>
                        <td width="17%" align="left" bgcolor="#E2E2E2"><strong>EXTRA5</strong></td>
                    </tr>
                    <?php
                    $idPraca = "";
                    $nomePraca = "";
                    $CI =& get_instance();
                    foreach ($EXPORTAR as $item) {
                        $num = 1;
                        $cor = '#EFEFEF';
                        $cor = $cor == '#EFEFEF' ? '#FFFFFF' : '#EFEFEF';
                        if ($idPraca != $item['ID_PRACA']) {
                            $idPraca = $item['ID_PRACA'];
                            $praca = $CI->praca->getById($item['ID_PRACA']);
                            $nomePraca = $praca['DESC_PRACA'];
                            /*foreach ($praca as $p) {
                                $nomePraca = $p['DESC_PRACA'];
                            }*/

                        }
                        ?>
                        <tr bgcolor="<?php echo $cor; ?>">
                            <td align="left" ><?php echo $nomePraca; ?></td>
                            <td align="left" ><?php echo $item['ORDEM_ITEM']; ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['PAGINA_ITEM']; ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['DESC_EXCEL']; ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo '&nbsp;'.$item['COD_BARRAS_FICHA']; ?></td>
                            <?php if ( ((empty($item['NOME_FICHA'])) || ($item['NOME_FICHA'] == null)) && ($item['PRINCIPAL'] == 1)) {?>
                                <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['VALOR_CAMPO_FICHA']; ?></td>
                            <?php } else {?>
                                <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['NOME_FICHA']; ?></td>
                            <?php }?>
                            
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['FILENAME']; ?></td>
                            
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php strtoupper(ghDigitalReplace($_sessao, $item['TIPO_FICHA'])); ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['TEXTO_LEGAL']; ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['PRECO_UNITARIO']; ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['TIPO_UNITARIO']; ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['PRECO_CAIXA']; ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['TIPO_CAIXA']; ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['PRECO_VISTA']; ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['PRECO_DE']; ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['PRECO_POR']; ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['ECONOMIZE']; ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['ENTRADA']; ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['PARCELAS']; ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['PRESTACAO']; ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['JUROS_AM']; ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['JUROS_AA']; ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['TOTAL_PRAZO']; ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['OBSERVACOES']; ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['EXTRA1_ITEM']; ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['EXTRA2_ITEM']; ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['EXTRA3_ITEM']; ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['EXTRA4_ITEM']; ?></td>
                            <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['EXTRA5_ITEM']; ?></td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td colspan="8">&nbsp;</td>
                    </tr>

                </table>

            <?php }else{ ?>
                <div>N&atilde;o h&aacute; resultado para a pesquisa.</div>
            <?php } ?>

        </td>
    </tr>
</table>

</body>
</html>