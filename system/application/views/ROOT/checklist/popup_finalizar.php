<!--  inicio formulario de aprovacao -->
<div id="contentGlobal">

<h1>Finalizar</h1>

<form id="formAprovacao" method="post" name="formAprovacao">

<!--  listando o historico de aprovacao/reprovacao -->
<h1>Histórico de Alteração</h1>

<!-- cabecalho fixo do historico -->
<div id="tableObjeto">
    <table width="100%" class="tableSelection">
        <thead>
           <tr>
              <th width="18%">Autor</th>
              <th width="17%"> Enviado para</th>
              <th width="10%">Situação</th>
              <th width="10%">Data</th>
              <th width="45%">Comentário</th>
          </tr>
        </thead>
    </table>
</div>

<!--  div para poder colocar a barra de rolagem -->
<div id="lista" style="overflow: auto; height: 205px; width: 100%; border-bottom: 2px solid #FF9900;">

  <!--  inicio historico  -->
  <table width="100%" border="0" cellpadding="2" cellspacing="0">
  <?php if(!empty($historico) && is_array($historico)): ?>
  <?php foreach($historico as $item): ?>
   <tr>
      <td class="linha_baixa" width="18%"><?php echo $item['NOME_USUARIO']?></td>
      <td class="linha_baixa" width="17%"><?php echo $item['DESC_ETAPA']?></td>
      <td class="linha_baixa" width="10%"><?php echo $item['DESC_STATUS']?></td>
      <td class="linha_baixa" width="10%"><?php echo format_date_to_form($item['DATA_JOB_APROVACAO']); ?></td>
      <td class="linha_baixa" width="45%"><?php echo $item['COMENTARIO_JOB_APROVACAO']?></td>
   </tr>
   <?php endforeach; ?>
   <?php endif; ?>
  <!--  fim historico  -->
  </table>

  <!--  fim da div container -->
</div>

<!-- inicio campos de aprovacao -->
<table width="98%" border="0" cellpadding="2" cellspacing="0">
   <?php if(!empty($etapas) && is_array($etapas) && !in_array($status['CHAVE_STATUS'],array('ELABORACAO','APROVADO','REPROVADO')) ): ?>
   <tr>
      <td>Finalizar Job<span class="obrigatorio"></span></td>
      <td><?php echo form_dropdown('ID_ETAPA',arraytoselect($etapas,'ID_ETAPA','DESC_ETAPA'),null, 'id="ID_ETAPA"'); ?></td>
   </tr>
   <?php endif; ?>
   <tr>
     <td valign="top">Comentário: <span class="obrigatorio">*</span></td>
     <td><textarea cols="50" rows="5" name="COMENTARIO_JOB_APROVACAO" id="COMENTARIO_JOB_APROVACAO"></textarea></td>
   </tr>
   <tr id="botoes">
     <td>&nbsp;</td>
     <td>
     <a href="javascript:;" onclick="finalizar(0)" class="button"><span>Concluir</span></a>
     </td>
   </tr>
</table>


<!-- fim campos de aprovacao -->

<!--  fim formulario de aprovacao -->
</form>

</div> <!-- Final de Content Global -->
