<div id="contentGlobal">
	<h1>Detalhes do Job</h1>
		<form id="formulario" action="<?php echo site_url('checklist/saveSolicitacao/' . $job['ID_JOB']); ?>" method="POST">	
		<input name="ID_TIPO_PECA" type="hidden" id="ID_TIPO_PECA" value="<?php echo($job['ID_TIPO_PECA']); ?>"/>
		<input name="ID_PRODUTO" type="hidden" id="ID_PRODUTO" value="<?php echo($job['ID_PRODUTO']); ?>"/>
		<input name="ID_PROCESSO_ETAPA_HIDDEN" type="hidden" id="ID_PROCESSO_ETAPA_HIDDEN" value="<?php echo($job['ID_PROCESSO_ETAPA']); ?>"/>
		<table>
			<tr>
				<td>Data de in&iacute;cio do Processo *</td>
				<td><input name="DATA_INICIO_PROCESSO" type="text" id="DATA_INICIO_PROCESSO" size="10" maxlength="10" class="_calendar" value="<?php echo(format_date_to_form($job['DATA_INICIO_PROCESSO'])); ?>" readonly="readonly" /></td>
			</tr>
			<tr>
				<td>In&iacute;cio  da Validade *</td>
				<td><input name="DATA_INICIO" type="text" id="DATA_INICIO" size="10" maxlength="10" value="<?php echo(format_date_to_form($job['DATA_INICIO'])); ?>" readonly="readonly" /></td>
			</tr>
			<tr>
				<td>Final da Validade *</td>
				<td><?=form_input(array('id'=>'DATA_TERMINO','name'=>'DATA_TERMINO', 'class'=>'_calendar','size'=>'10', 'value'=> format_date_to_form($job['DATA_TERMINO']), 'readonly'=>'readonly'));?></td>
			</tr>
			<tr>
				<td>Etapa *</td>
				<td><select name="ID_PROCESSO_ETAPA" id="ID_PROCESSO_ETAPA"><? echo(montaOptions($etapas, 'ID_PROCESSO_ETAPA', 'DESC_ETAPA', $job['ID_PROCESSO_ETAPA'] )); ?></select></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><a href="javascript:;" onclick="removeDivModal();" class="button"><span>Cancelar</span></a><a href="javascript:;" onclick="validarFormulario()" class="button"><span>Salvar</span></a></td>
			</tr>
		</table>
	</form>
</div>
<script type="text/javascript">

	$j('._calendar').datepicker({dateFormat: 'dd/mm/yy',showOn: 'button', buttonImage: '<?= base_url().THEME ?>img/calendario.png', buttonImageOnly: true});
	
	//Inicio Calculo da DATA_INICIO
	if($j('#DATA_INICIO_PROCESSO').val() == ""){
	      $j('#DATA_INICIO_PROCESSO').next().hide();
	}
	
	if($j('#DATA_INICIO').val() != ""){
	      var dateArray = $j('#DATA_INICIO').val().split('/');
	      $j('#DATA_TERMINO').datepicker('option',{minDate: new Date(dateArray[2], dateArray[1]-1, dateArray[0])});
	      $j('#DATA_TERMINO').next().show();
	}else{
	      $j('#DATA_TERMINO').next().hide();
	}
	
	//Calcula DATA_INICIO apos escolher DATA_INICIO_PROCESSO
	$j('#DATA_INICIO_PROCESSO').datepicker('option',{
	      onSelect: function(dateText, inst) { 
	            $j.post("<?= site_url('json/admin/calculaDataInicio')?>", 
	                       {date:dateText, idTipoPeca: $j('#ID_TIPO_PECA').val(), idProduto: $j('#ID_PRODUTO').val(), compararDataAtual: '0'}, 
	                       function(data, textStatus, XMLHttpRequest){
	                             data = eval(data);
	                             data = data[0];
	                             if(data.erro == ''){
	                                   $j('#DATA_INICIO').attr('value', data.result);
	                                   $j('#DATA_TERMINO').next().show();
	
	                                   var dateArray = data.result.split('/');
	                                   $j('#DATA_TERMINO').datepicker('option',{minDate: new Date(dateArray[2], dateArray[1]-1, dateArray[0])});
	                                   $j('#DATA_TERMINO').val('');
	                                   
	                             }else{
	                                   $j('#DATA_INICIO').val('');
	                                   $j('#DATA_TERMINO').val('');
	                                   $j('#DATA_TERMINO').next().hide();
	                                   alert(data.erro);
	                                   $j('#DATA_INICIO_PROCESSO').val('');
	                             }
	                       }
	            );
	      }
	});

	//Validar o formulario
	function validarFormulario(){
		
		if($j('#DATA_INICIO_PROCESSO').val() == ""){
		      return 'Data de Inicio do Processo é obrigatorio!';
		} else {
			if($j('#DATA_TERMINO').val() == ""){
		    	  return 'Data Final da Validade é obrigatorio!';
			} else {
				$j.post("<?= site_url('json/admin/verificaDataInicioProcessoXEtapa/' . $job['ID_JOB'])?>",
					{DATA_INICIO_PROCESSO: $j('#DATA_INICIO_PROCESSO').val(), ID_PROCESSO_ETAPA: $j('#ID_PROCESSO_ETAPA').val()}, 
	                       function(data, textStatus, XMLHttpRequest){
	                             data = eval(data);
	                             data = data[0];
	                             if(data.erro != 'ok'){
		                             if (!confirm(data.erro)) {
			                            return false;
		                             }
	                             }
	                             //Salva os dados
	                             salvarSolicitacaoAlteracao();
	                       }
	            );
			}
		}
	}

	//Salvar Solicitacao de Alteracao
	function salvarSolicitacaoAlteracao(){

		var dados = $j('#formulario').serializeArray();
		$j.post($j('#formulario').attr('action'), dados,
				function(data, textStatus, XMLHttpRequest){
	                data = eval(data);
	                data = data[0];
	                if(data.erro != 'ok'){
						alert(data.erro);
	                }
					refresh();
				}
		);
	}
</script>