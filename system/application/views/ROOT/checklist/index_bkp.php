﻿<?php $this->load->view("ROOT/layout/header") ?>
<div id="contentGlobal">
<form action="<?php echo site_url('checklist/index'); ?>" method="post">
<h1>Pauta</h1>
<div id="contador" style="padding-bottom:20px;"></div>

<div id="pesquisa_simples" class="alinha_esquerda">
	<table width="100%" border="0" cellspacing="1" cellpadding="2">
		<tr>
			<td width="85%"><table width="100%" border="0" cellspacing="1" cellpadding="2">
				<tr>
					<?php
					if( !empty($_sessao['ID_CLIENTE'])) {
						echo '<td width="19%">Cliente</td>
							<td width="19%">Agência</td>';
						
					} else {
						echo '<td width="19%">Agência</td>
							<td width="19%">Cliente</td>';
					}
					
					?>
					<td width="19%">Titulo do Job</td>
					<td width="19%">Situa&ccedil;&atilde;o</td>
					<td width="19%">Etapa</td>
					<td width="5%">Itens</td>
				</tr>
				<tr>
					<?php
					if( !empty($_sessao['ID_CLIENTE'])) {
						echo '<td>', sessao_hidden('ID_CLIENTE','DESC_CLIENTE'), '</td>
							<td>
							<select name="ID_AGENCIA" id="ID_AGENCIA" style="width: 180px;">
							<option value=""></option>
							'
							, montaOptions($agencias,'ID_AGENCIA','DESC_AGENCIA', val($busca,'ID_AGENCIA'))
							, '</select></td>';
						
					} else {
						echo '<td>', sessao_hidden('ID_AGENCIA','DESC_AGENCIA'), '</td>
							<td>
							<select name="ID_CLIENTE" id="ID_CLIENTE" style="width: 180px;">
							<option value=""></option>
							'
							, montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', val($busca,'ID_CLIENTE'))
							, '</select></td>';
					}
					
					?>
					<td><span style="text-align: left;">
						<input name="TITULO_JOB" type="text"  id="TITULO_JOB" value="<?php echo !empty($busca['TITULO_JOB']) ? $busca['TITULO_JOB'] : '' ?>" />
					</span></td>
					<td><span style="text-align: left;">
						<select name="ID_STATUS" id="ID_STATUS" style="max-width: 200px;">
							<option value=""></option>
							<?php echo montaOptions($status,'ID_STATUS','DESC_STATUS', !empty($busca['ID_STATUS']) ? $busca['ID_STATUS'] : ''); ?>
						</select>
					</span></td>
					<td><span style="text-align: left;">
						<select name="ID_ETAPA" id="ID_ETAPA" style="max-width: 200px;" >
							<option value=""></option>
							<?php echo montaOptions($etapas,'ID_ETAPA','DESC_ETAPA', !empty($busca['ID_ETAPA']) ? $busca['ID_ETAPA'] : ''); ?>
						</select>
					</span></td>
					<td><span style="text-align: left;">
						<select name="pagina" id="pagina">
							<?php
		$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
		for($i=5; $i<=50; $i+=5){
			printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
		}
		?>
						</select>
						<input name="tipo_pesquisa" type="hidden" id="tipo_pesquisa" value="<?php post('tipo_pesquisa'); ?>" />
					</span></td>
				</tr>
			</table>
				<table width="100%" border="0" cellpadding="2" cellspacing="1" class="tbl_avancada">
					<tr>
						<td width="19%">N&uacute;mero Job</td>
						<td width="19%">Bandeira</td>
						<td width="19%">Campanha</td>
						<td width="19%">Data de In&iacute;cio do Processo</td>
						<td width="19%">Usuário Responsável</td>
						<td width="5%">&nbsp;</td>
					</tr>
					<tr>
						<td><span style="text-align: left;">
							<input name="ID_JOB" type="text"  id="ID_JOB" value="<?php echo !empty($busca['ID_JOB']) ? $busca['ID_JOB'] : '' ?>" size="10" />
						</span></td>
						<td><span style="text-align: left;">
							<?php if(!empty($_sessao['ID_PRODUTO'])): ?>
							<?php sessao_hidden('ID_PRODUTO','DESC_PRODUTO'); ?>
							<?php else: ?>
							<select name="ID_PRODUTO" id="ID_PRODUTO">
								<option value=""></option>
								<?php echo montaOptions($produtos,'ID_PRODUTO','DESC_PRODUTO', !empty($busca['ID_PRODUTO']) ? $busca['ID_PRODUTO'] : ''); ?>
							</select>
							<?php endif; ?>
						</span></td>
						<td><span style="text-align: left;">
							<select name="ID_CAMPANHA" id="ID_CAMPANHA">
								<option value=""></option>
								<?php echo montaOptions($campanhas,'ID_CAMPANHA','DESC_CAMPANHA', !empty($busca['ID_CAMPANHA']) ? $busca['ID_CAMPANHA'] : ''); ?>
							</select>
						</span></td>
						<td><span style="text-align: left;">
							<input name="DATA_INICIO_PROCESSO" type="text"  id="DATA_INICIO_PROCESSO" value="<?php echo val($busca, 'DATA_INICIO_PROCESSO', ''); ?>" size="10" class="calendar" />
						</span></td>
						<td>
							<span style="text-align: left;">
								<select name="ID_USUARIO_RESPONSAVEL" id="ID_USUARIO_RESPONSAVEL">
									<option value="0"></option>
					        	</select>
							</span>
						</td>
						<td>&nbsp;</td>
					</tr>
				</table></td>
			<td width="15%">
				<a class="button" href="javascript:" onclick="$j(this).closest('form').submit();"><span>Pesquisar</span></a>
				
				<div class="both btnPesquisa_avancada" style="padding-top: 10px;">
					<a class="button" href="javascript:" onclick="showPesquisa('avancada')"><span>Pesquisa avançada</span></a>
				</div>
				
				<div class="both btnPesquisa_simples" style="padding-top: 10px;">
					<a class="button" href="javascript:" onclick="showPesquisa('simples')"><span>Pesquisa simples</span></a>
				</div>
			</td>
		</tr>
	</table>
</div>

<div id="tableObjeto">

<?php if( (isset($lista))&& (is_array($lista)) && (sizeof($lista) > 0)):?>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableSelection tableMidia">
<thead>
	<tr>
   		<?php if($podeEnviarEtapa): ?>
        <td>
            <input type="checkbox" value="" name="checkall" class="check_all" />
        </td>
        <?php endif; ?>
		<?php if($podeDesbloquear): ?>
			<th>Desbloquear</th>
		<?php endif; ?>
      <?php if($podeExcluir): ?>
		<th>Excluir</th>
	  <?php endif; ?>
      <?php if($podeAlterar): ?>
		<th>Editar</th>
	  <?php endif; ?>
        <th>Visualizar</th>
        <th align="left" width="3"></th>
		<th class="numeroJob">N&uacute;mero Job</th>
		<th class="tituloJob">T&iacute;tulo Job</th>
		<th class="inicioJob">In&iacute;cio  da Validade</th>
		<th class="terminoJob" align="left">Final da Validade</th>
		<th class="agenciaJob" align="left">Ag&ecirc;ncia</th>
		<th class="clienteJob" align="left">Cliente</th>
		<th class="bandeiraJob" align="left">Bandeira</th>
		<th class="campanhaJob" align="left">Campanha</th>
		<th class="etapaJob" align="left">Etapa</th>
		<th class="mudancaJob" align="left">Mudan&ccedil;a Etapa</th>
		<th class="situacaoJob" align="left">Situa&ccedil;&atilde;o</th>
        <th align="left">&nbsp;</th>
  	</tr>
  </thead>
		<?php $cont=0; ?>
		<?php

        foreach($lista as $item):
        	$img = '<img src="'.base_url().THEME.'img/visualizar.png" />';
			$link = '';
			$idjob = $item['ID_JOB'];
			$idExcel = $item['ID_EXCEL'];
			
			switch($item['CHAVE_ETAPA']){
				case 'SEL_MIDIA':
					if(Sessao::hasPermission('plano_midia','form')){
						$link = sprintf('<a href="%s">%s</a>', site_url('plano_midia/form/'.$idjob), $img);
					}
				break;
				case 'CARRINHO':
					if(Sessao::hasPermission('carrinho','form')){
						$link = sprintf('<a href="%s">%s</a>', site_url('carrinho/form/'.$idjob), $img);
					}
				break;
				case 'PRICING':
					if(Sessao::hasPermission('pricing','form')){
						$link = sprintf('<a href="%s">%s</a>', site_url('pricing/form/'.$idjob), $img);
					}
				break;
				case 'PAGINACAO':
					if(Sessao::hasPermission('paginacao','form')){
						$link = sprintf('<a href="%s">%s</a>', site_url('paginacao/form/'.$idjob), $img);
					}
				break;
				case 'ENVIADO_AGENCIA':
					if(Sessao::hasPermission('gerar_indd','form')){
						$link = sprintf('<a href="%s">%s</a>', site_url('gerar_indd/form/'.$idExcel), $img);
					}
				break;
				case 'LOGISTICA':
					if(Sessao::hasPermission('logistica','form')){
						$link = sprintf('<a href="%s">%s</a>', site_url('logistica/form/'.$idjob), $img);
					}
				break;
				case 'APROVACAO':
					if(Sessao::hasPermission('aprovacao','form_agencia') && !empty($_sessao['ID_AGENCIA'])){
						$link = sprintf('<a href="%s">%s</a>', site_url('aprovacao/historico/'.$idExcel), $img);
					}
					if(Sessao::hasPermission('aprovacao','form_cliente') && !empty($_sessao['ID_CLIENTE'])){
						$link = sprintf('<a href="%s">%s</a>', site_url('aprovacao/form_cliente/'.$idExcel), $img);
					}
				break;

			}
        ?>

		<tr>
        	<?php if($podeEnviarEtapa): ?>
            <td>
				<?php if($item['CHAVE_ETAPA'] != 'FINALIZADO'): ?>
            		<input type="checkbox" value="<?php echo $item['ID_JOB']; ?>" name="jobs[]" class="item" />
				<?php endif; ?>
            </td>
            <?php endif; ?>
             <?php if( $podeDesbloquear ): ?>
	            <td align="center">
	             <?php if( $item['CHAVE_STATUS'] == StatusDB::$BLOQUEADO ): ?>
					<a href="javascript:" onclick="popupDesbloquearJob(<?= $item['ID_JOB'] ?>, '<?= $item['TITULO_JOB'] ?>');"><img src="<?= base_url().THEME ?>img/unlock.png" border="0"/></a>
				<?php endif; ?> 
				</td>
			<?php endif; ?> 
            <?php if($podeExcluir): ?>
	            <td align="center">
	            	<?php if( $item['CHAVE_STATUS'] != StatusDB::$BLOQUEADO ): ?>
					<a href="javascript:" onclick="deleteJob(<?= $item['ID_JOB'] ?>, '<?= $item['TITULO_JOB'] ?>');"><img src="<?= base_url().THEME ?>img/trash.png" border="0"/></a>
					<?php endif; ?> 
				</td>
			<?php endif; ?>            
			<?php if($podeAlterar): ?>
            <td align="center">
			<?php if( $item['CHAVE_STATUS'] != StatusDB::$BLOQUEADO ): ?>
				<?php echo anchor('checklist/form/'. $item['ID_JOB'], '<img src="'. base_url().THEME.'img/file_edit.png" border="0"/>',array('title'=>'Editar produto'));?>			</td>
			<?php endif; ?> 
            <?php endif; ?>
			<td><?php if( $item['CHAVE_STATUS'] != StatusDB::$BLOQUEADO ): ?><?php echo $link; ?><?php endif; ?></td>
			<td <? if(empty($item['DATA_FINALIZADO'])): ?>bgcolor="<?= getCorTempoJob(strtotime($item['DATA_TERMINO'].' 23:59:59') - time()) ?>" <? endif; ?>>&nbsp;</td>
            <td><?=$item['ID_JOB'];?></td>
			<td><?=$item['TITULO_JOB'];?></td>
			<td><?=format_date_to_form($item['DATA_INICIO']);?></td>
			<td>
			<?=format_date_to_form($item['DATA_TERMINO']);?><br />
			</td>
			<td><?=$item['DESC_AGENCIA'];?></td>
			<td><?=$item['DESC_CLIENTE'];?></td>
			<td><?=$item['DESC_PRODUTO'];?></td>
			<td><?=$item['DESC_CAMPANHA'];?></td>
            <td style="padding: 0px; vertical-align:bottom">
          	<?=$item['DESC_ETAPA'];?>
            <div style="background-color:#<?= $item['COR_ETAPA'] ?>; width: 100%; height: 6px"></div>
            </td>
 		   <td class="alter"><?php echo empty($item['DATA_MUDANCA_ETAPA']) ? '---' : date('d/m/Y H:i',strtotime($item['DATA_MUDANCA_ETAPA']));?></td>
		  <td><?=$item['DESC_STATUS'] . getRevisaoJob($item); ?></td>
		  <td><span class="alter"><?php echo sinalizarAlteracoes($item); ?></span></td>
			<?php $cont++; ?>
		</tr>
		<?php endforeach;?>
        
        <?php $colspan = 15 + ($podeExcluir?1:0)+ ($podeAlterar?1:0)+ ($podeEnviarEtapa?1:0) + ($podeDesbloquear?1:0); ?>
        
        <tr>
            <td colspan="<?php echo $colspan; ?>" style="text-align: left; vertical-align:middle">
              <strong>Legenda job:</strong><br />
              <table style="border:none;" height="5">
              	<tr height="5" style="border:none">
                	<td style="border:none"><div style="background-color:<?= CoresTempo::$cores['COR_ATRASADO']?>;width:10px;height:10px"></div></td>
                    <td style="border:none; font-size:10px">Atrasado</td>
                    <td style="border:none"><div style="background-color:<?= CoresTempo::$cores['COR_ABAIXO_24HORAS']?>;width:10px;height:10px"></div></td>
                    <td style="border:none; font-size:10px">T&eacute;rmino em at&eacute; 24h</td>
                    <td style="border:none"><div style="background-color:<?= CoresTempo::$cores['COR_ACIMA_24HORAS']?>;width:10px;height:10px"></td>
                    <td style="border:none; font-size:10px">Mais de 24h para t&eacute;rmino</td>
                </tr>
              </table>
            </td>
        </tr>
</table>

</div> <!-- Final de Table Objeto -->
<span class="paginacao"><?=(isset($paginacao))?$paginacao:null?></span>
<?else:?>
	Não há resultado para pesquisa
<?endif;?>

</form>

<!-- JANELA DE SELECAO DE DELAY DE BLOQUEIO DO JOB -->
<div id="dialog-confirm" title="Confirme o desbloqueio do job" style="display: none;">
	<p>
	<!--<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>-->
	Confirme o desbloqueio do job 
	<span class="titulo" align="center" style="font-weight: bold">{titulo}</span> por:</p>
	<br/>
	<p align="center">
		<select id="delay_bloqueio">
			<option value="1800">30 minutos</option>
			<option value="3600">1 hora</option>
			<option value="7200">2 horas</option>
			<option value="14400">4 horas</option>
			<option value="28800">8 horas</option>
			<option value="86400">1 dia</option>
		</select>
	</p>
</div>


<script type="text/javascript">

<?php if($podeExcluir): ?>
function deleteJob(idJob, tituloJob){
   if(confirm('EXCLUIR JOB '+idJob+'\nNÃO É POSSÍVEL DESFAZER ESTA AÇÃO!')){
	   if(confirm('Número do Job: '+idJob+'\nTítulo: '+tituloJob.toUpperCase()+'\nDeseja realmente excluir o job e todas as informações associadas? (Não é possível desfazer!)')){
		   location.href = '<?php echo site_url('checklist/delete'); ?>/'+idJob;
	   }
   }
}
<?php endif; ?>

var isAtualizar = true;

<?php if($podeDesbloquear): ?>

function desbloquearJob(idJob, tituloJob, tempo){
   //Confirma o desbloqueio do job ‘titulo do Job’ por ‘tempo selecionado para desbloqueio’?
   $j( "#dialog-confirm" ).dialog('close');
   if(confirm('Confirma o desbloqueio do job '+tituloJob+' por '+tempo+'?')){
   	//alert($j("#delay_bloqueio").val());
	   location.href = '<?php echo site_url('checklist/desbloquear'); ?>/'+idJob+'/'+$j("#delay_bloqueio").val();
   }else{
		isAtualizar = true;
   }
}

function popupDesbloquearJob(idJob, tituloJob){
   isAtualizar = false;
   $j( "#dialog-confirm" ).find(".titulo").text(tituloJob);
   $j( "#dialog-confirm" ).dialog({
		resizable: false,
		height:175,
		modal: true,
		buttons: {
			"OK": function() {
				var tempo = $j("#delay_bloqueio option:selected").text();
	  			desbloquearJob(idJob, tituloJob, tempo);
				$j(this).dialog( "close" );
			},
			"Cancela": function() {
				isAtualizar = true;
				$j(this).dialog( "close" );
			}
		}
	}).dialog( "open" );
}
<?php endif; ?>

<?php if(!empty($_sessao['ID_CLIENTE'])): ?>

var idCliente = <?php echo $_sessao['ID_CLIENTE']; ?>;
var idUsuarioResponsavel = <?php if( isset($busca['ID_USUARIO_RESPONSAVEL']) && is_numeric( $busca['ID_USUARIO_RESPONSAVEL'] )){ echo $busca['ID_USUARIO_RESPONSAVEL']; }else{ echo '0'; } ?>;
clearSelect($('ID_USUARIO_RESPONSAVEL'), 1);
montaOptionsAjax($('ID_USUARIO_RESPONSAVEL'),'<?php echo site_url('json/admin/getUsuariosByCliente'); ?>','id=' + idCliente,'ID_USUARIO','NOME_USUARIO', idUsuarioResponsavel);

$('ID_AGENCIA').addEvent('change', function(){
	clearSelect($('ID_CAMPANHA'), 1);
	clearSelect($('ID_PRODUTO'), 1);
	//montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByAgencia'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
});
<?php endif; ?>

<?php if(!empty($_sessao['ID_AGENCIA'])): ?>
$('ID_CLIENTE').addEvent('change', function(){
	clearSelect($('ID_CAMPANHA'), 1);
	clearSelect($('ID_PRODUTO'), 1);
	montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByCliente'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
	var idUsuarioResponsavel = <?php if( isset($busca['ID_USUARIO_RESPONSAVEL']) && is_numeric( $busca['ID_USUARIO_RESPONSAVEL'] )){ echo $busca['ID_USUARIO_RESPONSAVEL']; }else{ echo '0'; } ?>;
	clearSelect($('ID_USUARIO_RESPONSAVEL'), 1);
	montaOptionsAjax($('ID_USUARIO_RESPONSAVEL'),'<?php echo site_url('json/admin/getUsuariosByCliente'); ?>','id=' + this.value,'ID_USUARIO','NOME_USUARIO', idUsuarioResponsavel);
});
<?php endif; ?>

<?php if(!empty($busca['ID_CLIENTE'])): ?>
	var idCliente = <?php echo $busca['ID_CLIENTE']; ?>;
	var idUsuarioResponsavel = <?php if( isset($busca['ID_USUARIO_RESPONSAVEL']) && is_numeric( $busca['ID_USUARIO_RESPONSAVEL'] )){ echo $busca['ID_USUARIO_RESPONSAVEL']; }else{ echo '0'; } ?>;
	clearSelect($('ID_USUARIO_RESPONSAVEL'), 1);
	montaOptionsAjax($('ID_USUARIO_RESPONSAVEL'),'<?php echo site_url('json/admin/getUsuariosByCliente'); ?>','id=' + idCliente,'ID_USUARIO','NOME_USUARIO', idUsuarioResponsavel);
<?php endif; ?>

$('ID_PRODUTO').addEvent('change', function(){
	clearSelect($('ID_CAMPANHA'),1);
	montaOptionsAjax($('ID_CAMPANHA'),'<?php echo site_url('json/admin/getCampanhasByProduto'); ?>','id=' + this.value,'ID_CAMPANHA','DESC_CAMPANHA');
});

$j('.check_all').click(function(){
	$j('.item').attr('checked',this.checked);
});

$j('#novaEtapa').change(function(){
	if(this.value != '' && confirm('Deseja enviar os jobs selecionados para outra etapa?')){
		var url = '<?php echo site_url('checklist/enviar_etapa'); ?>/' + this.value;
		$j('form').attr('action',url).submit();
	} else {
		this.value = '';
	}
});

// atualiza a pagina a cada minuto
var tempo = 30;
function atualizar(){
   if(isAtualizar){
		if( tempo > 0 ){
			$j('#contador').html('Pr&oacute;xima atualiza&ccedil;&atilde;o em ' + tempo + ' ' + (tempo > 1 ? 'segundos' : 'segundo'));
			tempo--;
			setTimeout('atualizar()', 1000);
		} else {
			if( !getDivModal().dialog('isOpen') ){
				location.href = '<?php echo $_SERVER['REQUEST_URI']; ?>';
			} else {
				setTimeout('atualizar()', 1000);
			}
		}
   }else{
	  setTimeout('atualizar()', 1000);
   }
	
}

atualizar();

function showPesquisa( tipo ){
	if( tipo == 'avancada' ){
		$j('.tbl_avancada, .btnPesquisa_simples').show();
		$j('.btnPesquisa_avancada').hide();
		
	} else {
		$j('.tbl_avancada, .btnPesquisa_simples').hide();
		$j('.btnPesquisa_avancada').show();
		
		$j('.tbl_avancada select, .tbl_avancada input').val('');
	}
	
	$j('#tipo_pesquisa').val( tipo );
}

$j(function(){
	configuraPauta('<?php echo $busca['ORDER']; ?>', '<?php echo $busca['ORDER_DIRECTION']; ?>');
	showPesquisa('<?php echo val($busca,'tipo_pesquisa','simples'); ?>');
	$j('.calendar')
		.mask('99/99/9999')
		.datepicker({dateFormat: 'dd/mm/yy'});
	
});

</script>

</div> <!-- Final de Content Global -->

<?php $this->load->view("ROOT/layout/footer") ?>