<?php $this->load->view("ROOT/layout/header");
$_SESSION['idJob'] =  $this->uri->segment(3); ?>

<style type="text/css">

    .apagado{
        opacity:0.5;
        filter: alpha(opacity=50);
    }

    .campoEditavelPauta {
        width: 70px;
    }

    .campoEditavelPauta[disabled] {
        background:rgba(0,0,0,0) !important;
        border:none !important;
    }
</style>

<div id="contentGlobal">
    <h1>Detalhes do Job </h1>
    <div id="tabs">
        <!-- inicio container geral / tabs -->
        <ul>
            <li><a href="#dados">Dados do Job</a></li>

            <?php if($podeVerCronograma): ?>
                <li><a href="#cronograma">Cronograma</a></li>
            <?php endif; ?>

            <?php foreach($pracas as $praca): ?>
                <li><a href="#praca-<?php echo $praca['ID_PRACA']; ?>"><?php echo $praca['DESC_PRACA']; ?></a></li>
            <?php endforeach; ?>
        </ul>
        <div id="dados">
            <!-- inicio dados job -->
            <table width="100%" class="Alignleft">
                <tr>
                    <td width="18%"><strong>Ag&ecirc;ncia</strong></td>
                    <td width="82%"><?php echo $job['DESC_AGENCIA']; ?></td>
                </tr>
                <tr>
                    <td><strong>Cliente</strong></td>
                    <td><?php echo $job['DESC_CLIENTE']; ?></td>
                </tr>
                <tr>
                    <td><strong>Bandeira</strong></td>
                    <td><?php echo $job['DESC_PRODUTO']; ?></td>
                </tr>
                <tr>
                    <td><strong>Campanha</strong></td>
                    <td><?php echo $job['DESC_CAMPANHA']; ?></td>
                </tr>
                <tr>
                    <td><strong>Tipo de pe&ccedil;a</strong></td>
                    <td><?php echo $job['DESC_TIPO_PECA']; ?></td>
                </tr>
                <?php if(!$_sessao['IS_HERMES']){?>
                    <tr>
                        <td><strong>Tipo de Job</strong></td>
                        <td><?php echo $job['DESC_TIPO_JOB']; ?></td>
                    </tr>
                <?php }?>
                <tr>
                    <td><strong>N&uacute;mero do Job</strong></td>
                    <td><?php echo $job['NUMERO_JOB_EXCEL']; ?></td>
                </tr>
                <tr>
                    <td><strong>T&iacute;tulo</strong></td>
                    <td><?php echo $job['TITULO_JOB']; ?></td>
                </tr>
                <tr>
                    <td><strong>Usu&aacute;rios Respons&aacute;veis</strong></td>
                    <td><?php echo $usuariosResponsaveis; ?></td>
                </tr>
                <tr>
                    <td><strong>N&uacute;mero de p&aacute;ginas</strong></td>
                    <td><?php echo $job['PAGINAS_JOB']; ?></td>
                </tr>
                <?php if(!$_sessao['IS_HERMES']){?>
                    <tr>
                        <td><strong>Formato</strong></td>
                        <td><?php echo centimetragem($job['LARGURA_JOB']), ' x ', centimetragem($job['ALTURA_JOB']); ?></td>
                    </tr>
                <?php }?>
                <tr>
                    <td><strong>Data de Início do Processo</strong></td>
                    <td><?php echo format_date_to_form($job['DATA_INICIO_PROCESSO']); ?></td>
                </tr>
                <tr>
                    <td><strong>In&iacute;cio  da Validade</strong></td>
                    <td><?php echo format_date_to_form($job['DATA_INICIO']); ?></td>
                </tr>
                <tr>
                    <td><strong>Final da Validade</strong></td>
                    <td><?php echo format_date_to_form($job['DATA_TERMINO']); ?></td>
                </tr>
                <tr>
                    <td valign="top"><strong>Observa&ccedil;&otilde;es</strong></td>
                    <td><?php echo nl2br($job['OBSERVACOES_EXCEL']); ?></td>
                </tr>
                <tr>
                    <td valign="top"><strong>Situa&ccedil;&atilde;o</strong></td>
                    <td><?php echo $job['DESC_STATUS']; ?></td>
                </tr>
                <tr>
                    <td valign="top"><strong>Etapa atual</strong></td>
                    <td><div style="color:#FFF;height:20px;width:auto;float:left;background-color:#<?php echo $job['COR_ETAPA']; ?>">&nbsp;<?php echo $job['DESC_ETAPA']; ?>&nbsp;</div></td>
                </tr>
            </table>
            <br />
            <br />

            <!-- Retirado linha do tempo na versao 3.6.0 para nao impactar...

			<h1 style="color:#847f78">Linha do tempo do job</h1>
			
			<table>
				<tr>
					<? foreach($etapasProcesso as $etapa): ?>
					<td height="2" valign="middle" align="center" style="font-size:18px; color:#<?= $job['COR_ETAPA'] ?>"><?= ($etapa['ORDEM'] == $job['ORDEM'])? '▼' : ''; ?></td>
					<? endforeach; ?>
				</tr>
				<tr>
					<? foreach($etapasProcesso as $etapa): ?>
					<td height="2" valign="middle" align="center" bgcolor="#<?= ($etapa['ORDEM'] < $job['ORDEM'])? '000000' : 'FFFFFF'; ?>"></td>
					<? endforeach; ?>
				</tr>
				<tr>
					<? foreach($etapasProcesso as $etapa): ?>
					<td height="2" valign="middle" align="center"></td>
					<? endforeach; ?>
				</tr>
				<tr>
					<? foreach($etapasProcesso as $etapa): ?>
						<td height="20" valign="middle" align="center" bgcolor="#<?= $etapa['COR_ETAPA']?>" style="color:#FFF">
                        	&nbsp;&nbsp;
							<?= $etapa['DESC_ETAPA']?>
							&nbsp;&nbsp;
                       	</td>
					<? endforeach; ?>
				</tr>
			</table>
			
 -->

            <?php if(!empty($historico) && is_array($historico)): ?>
                <!-- inicio historico de aprovacao -->
                <div id="both" style="clear: both;"></div>
                <div id="contentHistoricoAprova">
                    <h1>Histórico de Alteração</h1>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableSelection">
                        <thead>
                        <tr>
                            <td>Autor</td>
                            <td>Agência/Cliente</td>
                            <td>Enviado para</td>
                            <td>Situação</td>
                            <td>Data</td>
                        </tr>
                        </thead>
                        <?php foreach($historico as $item): ?>
                            <tr>
                                <td><?php echo $item['NOME_USUARIO'];?></td>
                                <td><?php echo !empty($item['DESC_CLIENTE']) ? $item['DESC_CLIENTE'] : $item['DESC_AGENCIA'];?></td>
                                <td><?php echo $item['DESC_ETAPA'];?></td>
                                <td><?php echo $item['DESC_STATUS'];?></td>
                                <td><?php echo format_date_to_form($item['DATA_HISTORICO'], 'd/m/Y H:i:s'); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>

                    <!--HISTORICO DE PRODUTOS-->
                    <br/>
                    <?php if(count($historicoPraca)): ?>
                        <h1>Histórico de Alteração das Praças</h1>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableSelection">
                            <thead>
                            <tr>
                                <td>Usuário</td>
                                <td>Agência/Cliente</td>
                                <td>Realizou alterações na praça</td>
                                <td>Ação</td>
                                <td>Situação</td>
                                <td>Data</td>
                            </tr>
                            </thead>
                            <?php foreach($historicoPraca as $item): ?>
                                <tr>
                                    <td><?php echo $item['NOME_USUARIO'];?></td>
                                    <td><?php echo !empty($item['DESC_CLIENTE']) ? $item['DESC_CLIENTE'] : $item['DESC_AGENCIA'];?></td>
                                    <td><?php echo $item['DESC_PRACA'];?></td>
                                    <td><?php echo $item['ACAO'];?></td>
                                    <td><?php echo $item['DESC_ETAPA'];?></td>
                                    <td><?php echo format_date_to_form($item['DATA_HISTORICO'], 'd/m/Y H:i:s'); ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    <?php endif; ?>
                    <!--  fim historico  -->
                </div>
                <!-- Final de Content Historico Aprova -->
            <?php endif; ?>
            <!-- fim historico de aprovacao -->
        </div>
        <!-- fim dados job -->

        <?php if($podeVerCronograma): ?>
            <div id="cronograma">
                <div id="cronograma_imagens" style="width: 1100px; height: 200px; overflow: auto; min-height: 350px;">
                    <p><strong>Cronograma programado</strong></p>
                    <img src="<?php echo site_url('json/processo/getGraficoCronograma/'.$job['ID_JOB'].'/planejado/0/0/30/20'); ?>" /><br />
                    <img src="<?php echo site_url('json/processo/getGraficoCronograma/'.$job['ID_JOB'].'/atual/0/0/30/20'); ?>" />
                    <p><strong>Cronograma realizado</strong></p>
                </div>

                <br /><br />

                <table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabela_padrao">
                    <tr>
                        <thead>
                        <tr>
                            <th width="2%">&nbsp;</th>
                            <th width="46%"><strong>Etapa</strong></th>
                            <th width="16%"><strong>Usuário</strong></th>
                            <th width="18%"><strong>Data de Inicio na Etapa</strong></th>
                            <th width="18%"><strong>Data de Término na Etapa</strong></th>
                        </thead>
                    </tr>
                    <?php foreach($etapasRealizadas as $item): ?>
                        <tr>
                            <td style="text-align: left" bgcolor="#<?php echo $item['COR_ETAPA']; ?>">&nbsp;</td>
                            <td style="text-align: left"><?php echo $item['PROXIMA_ETAPA']; ?></td>
                            <td style="text-align: left"><?php echo $item['NOME_USUARIO']; ?></td>
                            <td style="text-align: left"><?php echo format_date($item['DATA_INICIO_ETAPA'], 'd/m/Y H:i:s'); ?></td>
                            <td style="text-align: left">
                                <?php echo $item['CHAVE_ETAPA'] != 'FINALIZADO' ? format_date($item['DATA_FIM_ETAPA'],'d/m/Y H:i:s') : '--'; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        <?php endif; ?>

        <?php foreach($pracas as $praca): ?>
            <!-- inicio praca -->
            <div id="praca-<?php echo $praca['ID_PRACA']; ?>">
                <form>
                    <table width="100%" border="0" cellspacing="1" cellpadding="2">
                        <tr>
                            <td style="text-align:right" width="15%"><?php ghDigitalReplace($_sessao, 'Nome da ficha'); ?>:&nbsp;&nbsp;</td>
                            <td width="85%" style="text-align:left">
                                <input type="text" id="nome_<?php echo $praca['ID_PRACA']; ?>" name="nome" class="nome" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:right"><?php ghDigitalReplace($_sessao, 'Código de Barras'); ?>:&nbsp;&nbsp;</td>
                            <td style="text-align:left">
                                <input type="text" id="codigo_<?php echo $praca['ID_PRACA']; ?>" name="codigo" class="codigo" />
                                <input type="button" id="btnPesquisar_<?php echo $praca['ID_PRACA']; ?>" name="btnPesquisar" class="btnPesquisar" value="Pesquisar" />
                                <input type="button" id="btnPesquisaAvancada_<?php echo $praca['ID_PRACA']; ?>" idPraca="<?php echo $praca['ID_PRACA']; ?>" class="btnPesquisaAvancada1" name="btnPesquisaAvancada" value="Pesquisa Avançada" />
                            </td>
                        </tr>
                    </table>
                    <div id="pesquisa_completo_pauta_<?php echo $praca['ID_PRACA']; ?>" style="display:none">
                        <?php echo form_open('ficha/lista');?>
                        <?=form_hidden('tipo_pesquisa','completa');?>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="contentPcomplete">
                            <tr>
                                <td style="text-align:right" width="15%">Data de criação:&nbsp;&nbsp;</td>
                                <td width="85%" style="text-align:left">
                                    <input type="text" name="DATA_INSERT_INICIO" id="DATA_INSERT_INICIO" size="10" maxlength="10" class="_calendar" value="" readonly="readonly" />
                                    &nbsp;&nbsp;até&nbsp;&nbsp;
                                    <input type="text" name="DATA_INSERT_FIM" id="DATA_INSERT_FIM" size="10" maxlength="10" class="_calendar" value="" readonly="readonly" />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right">Somente com pendências:&nbsp;&nbsp;</td>
                                <td style="text-align:left">
                                    <input name="PENDENTES" type="checkbox" id="PENDENTES" value="1" />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right">Temporário:&nbsp;&nbsp;</td>
                                <td style="text-align:left">
                                    <input type="radio" name="FLAG_TEMPORARIO" id="FLAG_TEMPORARIO" value="1" />
                                    Sim
                                    <input type="radio" name="FLAG_TEMPORARIO" id="FLAG_TEMPORARIO" value="0" />
                                    Não
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td style="text-align:left">
                                    <input type="button" id="btnCancelarPesquisa_<?php echo $praca['ID_PRACA']; ?>" idPraca="<?php echo $praca['ID_PRACA']; ?>" name="btnCancelarPesquisa" value="Cancelar Pesquisa" />
                                    <input type="button" id="btnLimpaPesquisa_<?php echo $praca['ID_PRACA']; ?>" idPraca="<?php echo $praca['ID_PRACA']; ?>" name="btnLimpaPesquisa" value="Limpar Campos" />
                                    <input type="button" id="btnPesquisaAvancada_<?php echo $praca['ID_PRACA']; ?>" name="btnPesquisaAvancada" class="btnPesquisaAvancada" value="Pesquisar" />
                                </td>
                            </tr>
                        </table>
                        <?php echo form_close();?>
                    </div>
                </form>

                <?php if($podeEditarProdutos): ?>
                    <input type="button" class="btnEditar" value="Editar" />
                    <input type="button" class="btnCancelarEdicao" value="Cancelar" style="display:none" />
                <?php endif; ?>

                <?php if($podeAdicionarProdutos): ?>
                    <input type="button" class="btnAdicionar" value="Adicionar" />
                <?php endif; ?>

                <div class="carregando" style="clear:both; padding-bottom: 20px;">Carregando...</div>
                <div id="menu">
                    <ul>
                        <li><a id="btnGrupo-2-<?php echo $praca['ID_PRACA']; ?>" href="javascript:" class="btnGrupos btnGrupos1 gruposPraca-<?php echo $praca['ID_PRACA']; ?>">&Agrave; Vista</a></li>
                        <li><a id="btnGrupo-1-<?php echo $praca['ID_PRACA']; ?>" href="javascript:" class="btnGrupos gruposPraca-<?php echo $praca['ID_PRACA']; ?>">Atacarejo</a></li>
                        <li><a id="btnGrupo-3-<?php echo $praca['ID_PRACA']; ?>" href="javascript:" class="btnGrupos gruposPraca-<?php echo $praca['ID_PRACA']; ?>">Parcelado</a></li>
                        <li><a id="btnGrupo-5-<?php echo $praca['ID_PRACA']; ?>" href="javascript:" class="btnGrupos gruposPraca-<?php echo $praca['ID_PRACA']; ?>">Outros</a></li>
                        <li><a id="btnGrupo-6-<?php echo $praca['ID_PRACA']; ?>" href="javascript:" class="btnGrupos gruposPraca-<?php echo $praca['ID_PRACA']; ?>">Informações da Ficha</a></li>
                    </ul>
                </div>
                <div id="tableObjeto">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableSelection tabelaProdutos" style="margin: 0 0 30px 0;" id="pricing-<?php echo $praca['ID_PRACA']; ?>">
                        <thead>
                        <tr>
                            <?php if($podeEditarFicha): ?>
                                <th width="5%">Editar</th>
                            <?php endif; ?>
                            <?php if($podeEditarProdutos): ?>
                                <th width="5%">Excluir</th>
                            <?php endif; ?>
                            <th width="60">Imagem</th>
                            <th width="60"><?php ghDigitalReplace($_sessao, 'SubFichas'); ?></th>
                            <th width="60">Ficha</th>
                            <th width="60">P&aacute;gina</th>
                            <th width="60">Ordem</th>
                            <th class="grupo1 tituloPrecos" width="60">Pre&ccedil;o Un.</th>
                            <th class="grupo1 tituloPrecos" width="60">Tipo&nbsp;Un.</th>
                            <th class="grupo1 tituloPrecos" width="60">Pre&ccedil;o Cx.</th>
                            <th class="grupo1 tituloPrecos" width="60">Tipo&nbsp;Cx.</th>

                            <th class="grupo2 tituloPrecos" width="60">Pre&ccedil;o&nbsp;Vista</th>
                            <th class="grupo2 tituloPrecos" width="60">Pre&ccedil;o&nbsp;De</th>
                            <th class="grupo2 tituloPrecos" width="60">Pre&ccedil;o&nbsp;Por</th>
                            <th class="grupo2 tituloPrecos" width="60">Economize</th>

                            <th class="grupo3 tituloPrecos" width="60">Entrada</th>
                            <th class="grupo3 tituloPrecos" width="60">Parcelas</th>
                            <th class="grupo3 tituloPrecos" width="60">Presta&ccedil;&atilde;o</th>
                            <th class="grupo3 tituloPrecos" width="60">Juros&nbsp;AM</th>
                            <th class="grupo3 tituloPrecos" width="60">Juros&nbsp;AA</th>
                            <th class="grupo3 tituloPrecos" width="60">Total&nbsp;Prazo</th>

                            <th class="grupo5 tituloPrecos" width="60">Texto Legal</th>
                            <th class="grupo5 tituloPrecos" width="60">Extra 1</th>
                            <th class="grupo5 tituloPrecos" width="60">Extra 2</th>
                            <th class="grupo5 tituloPrecos" width="60">Extra 3</th>
                            <th class="grupo5 tituloPrecos" width="60">Extra 4</th>
                            <th class="grupo5 tituloPrecos" width="60">Extra 5</th>

                            <th class="grupo6 tituloPrecos" width="40">Situação</th>
                            <th class="grupo6 tituloPrecos" width="60">Temporário?</th>
                            <th class="grupo6 tituloPrecos" width="85"><?php ghDigitalReplace($_sessao, 'Tipo de Ficha'); ?></th>

                            <th class="grupo4 tituloPrecos" width="70">Observa&ccedil;&otilde;es</th>
                        </tr>
                        </thead>
                    </table>
                    <!-- <div style="clear:both; padding-top: 10px; padding-bottom:10px">
	 O &iacute;cone <img src="<?php echo base_url().THEME; ?>img/error.png" title="Item indisponivel" /> indica que o produto foi marcado como indispon&iacute;vel pela logística.
	</div> -->
                </div>
                <?php if ($copiarPraca): ?>
                    <div style="border:1px solid #D3D3D3;background-color:#EDECEC;padding:10px;float:left;width:1070px;margin: -15px 0px 15px 10px;">
                        <div style="float: left;">
                            Copiar produtos de outra praça para esta. Praça origem:
                            <select class="copiar" id="drpCopiarPracas">
                                <option value=""></option>
                                <?php foreach($pracas as $prac): ?>
                                    <?php if( $prac['ID_PRACA'] !=  $praca['ID_PRACA']): ?>
                                        <option value="<?php echo $prac['ID_PRACA']; ?>"><?php echo $prac['DESC_PRACA']; ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div style="float: left;padding-left: 60px;">
                            <input type='button' class="btnCopiar" data-copy="Praca" value='Copiar' />
                        </div>
                    </div>
                    <!--COPIAR PRACA DE OUTRO JOB-->
                    <div style="border:1px solid #D3D3D3;background-color:#EDECEC;padding:10px;float:left;width:1070px;margin: -15px 0px 15px 10px;">
                        <p>Copiar praça de outro Job para esta.</p><br/>

                        <div style="float: left;">
                            Job origem:
                            <select class="copiar" name="drpCopiarPracasJob" id="<?php echo $praca['ID_PRACA']; ?>">
                                <option value=""></option>
                                <?php foreach($jobs as $j): ?>
                                    <option value="<?php echo $j['ID_JOB']; ?>"><?php echo $j['TITULO_JOB']; ?></option>
                                <?php endforeach; ?>
                            </select>

                        </div>
                        <div style="float: left; padding-left: 30px;" id="pracaJob_<?php echo $praca['ID_PRACA']; ?>"><?php /*echo $praca['ID_PRACA']; */?></div>
                        <div style="float: left; padding-left: 30px;">
                            <input style="visibility:hidden" type='button'
                                   class="btnCopiar" data-copy="Job" data-job="<?php echo $praca['ID_PRACA']; ?>" value='Copiar'
                            />
                        </div>
                        <div class="clearBoth">
                            <br/><p>Somente os jobs em Pauta e com o mesmo número de Páginas serão listados.</p>
                        </div>

                    </div>
                    <!--FIM COPIAR PRACA DE OUTRO JOB-->
                <?php endif;?>

            </div>
            <!-- fim praca -->
            <div class="clearBoth"></div>
        <?php endforeach; ?>
    </div>
    <!-- fim container geral / tabs -->
    <div align="center">
        <!-- inicio botoes -->
        <a class="button" href="<?php echo site_url('checklist/index'); ?>"><span>Cancelar</span></a>
        <?php if( $job['ID_ETAPA'] == $codigoChecklist && $podeEnviarEtapa && $this->uri->segment(3) != ''):?>
            <a class="button" href="<?php echo site_url('checklist/enviar_etapa/'.$job['ID_JOB']); ?>" ><span>Próxima Etapa</span></a>
        <?php endif;?>

        <?php if( $job['ID_ETAPA'] == $codigoChecklist && $podeVoltarEtapa && $this->uri->segment(3) != ''):?>
            <a class="button" href="<?php echo site_url('checklist/voltar_etapa/'.$job['ID_JOB']); ?>" ><span>Voltar Etapa</span></a>
        <?php endif;?>
        
        <?php if( $podeSolicitaAlteracoes == true ): ?>
            <a class="button" href="javascript:" onClick="SolicitarAlteracao()"><span>Solicitar Alterações</span></a>
        <?php endif;?>
        <?php if( $job['CHAVE_ETAPA'] == 'ENVIADO_AGENCIA' && $correcoes == true): ?>
            <a class="button" href="<?php echo site_url('checklist/correcoes/'.$job['ID_JOB']); ?>" ><span>Solicitar Correções</span></a>
            <!--<a class="button" href="javascript:" onClick="SolicitarAlteracao()"><span>Solicitar Correções</span></a>-->
        <?php endif;?>
    </div>
    <div id="pendencias">
        <ul>
            <li><a href="#pendenciasDadosBasicos">Dados B&aacute;sicos</a></li>
            <li><a href="#pendenciasCampos">Campos</a></li>
        </ul>
        <div id="pendenciasDadosBasicos"> </div>
        <div id="pendenciasCampos"> </div>
    </div>
    <!-- fim botoes -->
    <script type="text/javascript">

        window.onbeforeunload = function() {
            var attr = $j(".campoEditavelPauta").eq(0).attr("disabled");

            if ($j(".campoEditavelPauta").length > 0 && !(typeof attr !== typeof undefined && attr !== false)) {
                return "A edição corrente não foi salva, tem certeza que deseja continuar sem salvar?";
            }
        }

        function notificaEdicao(e){
            var attr = $j(".campoEditavelPauta").eq(0).attr("disabled");

            if ($j(".campoEditavelPauta").length > 0 && !(typeof attr !== typeof undefined && attr !== false)) {
                alert("É necessário concluir a edição corrente");

                e.preventDefault();
                e.stopPropagation();
                e.stopImmediatePropagation();
                return false;
            }
        }

        $j("a,input[type='button']").not('.btnCancelarEdicao').not(".btnEditar").not('.btnGrupos', "#menu").on('click', notificaEdicao);

        var idjob       = '<?php echo $job['ID_JOB']; ?>';
        var grupoAtual = 2;
        var preloaders = {};
        var loader     = null;
        var pracaAtual = 0;
        var urlItens   = base_url + 'index.php/json/fluxo/get_itens_checklist';
        var limit      = 100;

        $j(function(){
            $j('#pendencias').tabs().hide();
            configPreloader();

            $j('.btnPesquisar').click(function(){
                $j(this).attr("disabled", 'disabled');
                pesquisa();
            });
            $j('.btnPesquisaAvancada').click(function(){
                if(valida_campos()){
                    $j(this).attr("disabled", 'disabled');
                    pesquisa();
                }
            });

            $j("input[id^='btnPesquisaAvancada_']").click(function(){
                $j('#pesquisa_completo_pauta_'+$j(this).attr('idPraca')).show();
                $j('#btnPesquisar_'+$j(this).attr('idPraca')).hide();
                $j('#btnPesquisaAvancada_'+$j(this).attr('idPraca')).hide();
            });

            $j("input[id^='btnCancelarPesquisa_']").click(function(){
                $j('#pesquisa_completo_pauta_'+$j(this).attr('idPraca')).hide();
                $j('#btnPesquisar_'+$j(this).attr('idPraca')).show();
                $j('#btnPesquisaAvancada_'+$j(this).attr('idPraca')).show();
            });

            $j("input[id^='btnLimpaPesquisa_']").click(function(){
                limpaForm('#pesquisa_completo_pauta_'+$j(this).attr('idPraca'));
                $j('#nome_'+$j(this).attr('idPraca')).val('');
                $j('#codigo_'+$j(this).attr('idPraca')).val('');
            });


            $j('#tabs').tabs({
                select : function(evt, ui){
                    var parts = ui.panel.id.split('-');
                    if( parts.length > 1 ){
                        pracaAtual = parseFloat( parts[1] );
                        carregaPraca( pracaAtual, 0, limit );
                        initialize_tab($j(ui.panel));
                    }
                }
            });


            var interval = setInterval(function(){
                var a=60;
                var todas = true;
                $j('#cronograma_imagens img').each(function(){
                    if( $j(this).height() == 0 ){
                        todas = false;
                    }
                    a += $j(this).height();
                });

                if( todas ){
                    clearInterval( interval );
                }

                $j('#cronograma_imagens').height( a );
            }, 500);

            $j('._calendar').datepicker({dateFormat: 'dd/mm/yy',showOn: 'button', buttonImage: '<?= base_url().THEME ?>img/calendario.png', buttonImageOnly: true});
        });

        <?php if( $podeSolicitaAlteracoes == true ): ?>
        function SolicitarAlteracao(){
            displayMessagewithparameter('<?php echo site_url('checklist/solicita_alteracoes/'.$job['ID_JOB']); ?>',400,250,function () {} );
        }
        <?php endif; ?>

        function pesquisa(){
            carregaPraca(pracaAtual, 0, limit);
        }

        function configPreloader(){
            loader = new StackLoader(urlItens, {});
            loader.onCompleteBlock = function(loa, container, html){

                $j('.real').maskMoney({symbol: 'R$', thousands: '.', decimal: ','});
                $j('.float').maskMoney({symbol: '', thousands: '', decimal: '.'});
                $j("a,input[type='button']").not(".btnEditar").not('.btnGrupos', "#menu").on('click', notificaEdicao);

                container.find('.jqzoom').fancybox();
                container.find('.grupo1, .grupo2, .grupo3, .grupo6, .grupo5').hide();
                container.find('.grupo'+grupoAtual).show();

                return true;
            }

            loader.onStart = function(){
                $j('#praca-'+this.params.idpraca+' > .carregando').show();
            }

            loader.onComplete = function(loader){
                $j('#praca-'+this.params.idpraca+' > .carregando').hide();
                $j('.btnPesquisar').removeAttr('disabled');
                $j('.btnPesquisaAvancada').removeAttr('disabled');


            }
        }
        if ($j(".btnCopiar").length > 0) {
            $j(".btnCopiar").on("click", function () {
                if ($j(this).attr('data-copy') == 'Praca') {
                    var select = $j("#drpCopiarPracas", $j(this).parent().parent());

                    if (select.val() && confirm("Deseja copiar os produtos da praça selecionada " + $j('option:selected', select).text() + " para a praça atual?")) {


                        displayStaticMessagewithparameter('Salvando dados', 200, 80);

                        $j.ajax({
                            url: base_url + 'index.php/json/fluxo/copiarEntrePracas',
                            type: "Post",
                            data: {ID_JOB: idjob, ID_PRACA_DESTINO: pracaAtual, ID_PRACA_ORIGEM: select.val()},
                            success: function (data) {
                                console.log(data);
                                pesquisa();
                                closeMessage();
                            }
                        });
                    }
                }

                if ($j(this).attr('data-copy') == 'Job') {
                    var dataJob = $j(this).attr('data-job');
                    //alert(dataJob);
                    var jobOrigem = $j("#" + dataJob, $j(this).parent().parent()).val();
                    if (jobOrigem == "") {
                        alert("Selecione um Job de Origem");
                        return false;
                    }
                    var praca = $j("#selectPraca_" + dataJob + " :selected");
                    if (praca.val() == "") {
                        alert("Selecione uma Praça de Origem");
                        return false;
                    }
                    /*alert(praca.val());
                     return;*/
                    if (confirm("Deseja copiar os produtos da praça selecionada " + praca.text() + " para a praça atual?")) {
                        /*alert(jobOrigem);
                         alert(pracaAtual);
                         alert(praca.val());
                         return false;*/
                        displayStaticMessagewithparameter('Salvando dados', 200, 80);

                        $j.ajax({
                            url: base_url + 'index.php/json/fluxo/copiarPracasEntreJobs',
                            type: "Post",
                            data: {
                                ID_JOB: idjob,
                                ID_PRACA_DESTINO: pracaAtual,
                                ID_JOB_ORIGEM: jobOrigem,
                                ID_PRACA_ORIGEM: praca.val()
                            },
                            success: function (data) {
                                console.log(data);
                                pesquisa();
                                closeMessage();
                            }
                        });
                    }
                }
            });
        }
        if ($j("select[name=drpCopiarPracasJob]").length > 0) {
            $j("select[name=drpCopiarPracasJob]").on("change", function () {
                var select = $j($j(this), $j(this).parent().parent());
                var pracaJob = $j("#pracaJob_" + select.attr('id'));
                //alert("#pracaJob_"+select.attr('id'));
                $j('input[data-copy=Job]').css('visibility', 'hidden');
                pracaJob.html('');
                if (select.val() != "") {
                    $j.ajax({
                        url: base_url + 'index.php/json/fluxo/getPracas',
                        type: "Post",
                        dataType: "json",
                        data: {
                            ID_JOB: select.val()
                        },
                        success: function (data) {
                            if (data.length > 0) {
                                var selectPraca = "<select id='selectPraca_" + select.attr('id') + "' class='copiar'>";
                                for (var i = 0; i < data.length; i++) {
                                    selectPraca += '<option value="' + data[i].ID_PRACA + '">' + data[i].DESC_PRACA + '</option>'
                                }
                                selectPraca += '</select>';
                                //selectPraca += '<div style="float: left;  padding-left: 30px;">';
                                //selectPraca += '<input type="button" class="btnCopiar" data-copy="Job" value="Copiar"/>';
                                pracaJob.html(selectPraca);
                                $j('input[data-copy=Job]').css('visibility', 'visible');
                            } else {
                                $j('input[data-copy=Job]').css('visibility', 'hidden');
                                alert("Praças não encontradas");
                            }
                        }
                    });
                }
            });
        }

        function carregaPraca(idpraca, offset, limit){
            $j('.tabelaProdutos > tbody').remove();
            var frm = $j('#praca-'+idpraca).find('form');

            loader.params.nome = frm.find('.nome').val();
            loader.params.codigo = frm.find('.codigo').val();
            loader.params.dtInicio = frm.find('#DATA_INSERT_INICIO').val();
            loader.params.dtFim = frm.find('#DATA_INSERT_FIM').val();
            loader.params.pendentes = frm.find('input[name=PENDENTES]:checked').val();
            loader.params.flagTemporario = frm.find('input[name=FLAG_TEMPORARIO]:checked').val();

            loader.params.idpraca   = idpraca;
            loader.params.idjob     = idjob;
            loader.defaultContainer = '#pricing-' + idpraca;
            loader.firstContainer   = '#pricing-' + idpraca;
            loader.load(offset, limit);
        }

        function valida_campos() {
            var data_inicio = false;
            var data_fim = false;

            if($j("#DATA_INSERT_INICIO").val() != ''){
                data_inicio = checkDate($j("#DATA_INSERT_INICIO").val());
            }

            if($j("#DATA_INSERT_FIM").val() != ''){
                data_fim = checkDate($j("#DATA_INSERT_FIM").val());
            }

            if( $j("#DATA_INSERT_INICIO").val() == "" && $j("#DATA_INSERT_FIM").val() == "") {
                return true;
            }

            if ((data_inicio == false) || (data_fim == false)) {
                alert("A data informada é invalida");
                return false;
            }

            if( $j("#DATA_INSERT_INICIO").val() != '' && $j("#DATA_INSERT_FIM").val() ) {
                data_ini = $j("#DATA_INSERT_INICIO").val().substr(6,4) + $j("#DATA_INSERT_INICIO").val().substr(3,2) + $j("#DATA_INSERT_INICIO").val().substr(0,2);
                data_fim = $j("#DATA_INSERT_FIM").val().substr(6,4) + $j("#DATA_INSERT_FIM").val().substr(3,2) + $j("#DATA_INSERT_FIM").val().substr(0,2);
                if( data_ini > data_fim ) {
                    alert("A data inicio deve ser menor que a data fim!");
                    return false;
                }
            }
            return true;
        }

        function initialize_tab(tab){
            tab.find('.btnGrupos').click(function(){
                var reg = this.id.match(/(\w+)-(\d+)-(\d+)/);
                tab.find('#pricing-' + reg[3]).find('.grupo1, .grupo2, .grupo3, .grupo6, .grupo5').hide();
                tab.find('#pricing-' + reg[3]).find('.grupo'+reg[2]).show();
                tab.find('.gruposPraca-' + reg[3]).removeClass('ativo');
                tab.find( this ).addClass('ativo');

                grupoAtual = reg[2];
                abaAtual = reg[2];
            });

            tab.find('.btnGrupos1').each(function(){
                $j(this).click();
            });
        }

        function exibePendencias(titulo, dadosBasicos, campos){
            if( dadosBasicos == '' && campos == '' ){
                alert('Os campos de pendências não foram preenchidos');
                return false;
            }

            $j('#pendenciasDadosBasicos').html(dadosBasicos);
            $j('#pendenciasCampos').html(campos);
            $j('#pendencias').dialog({
                width: 600,
                height: $j(window).height() - 100,
                title: 'Pend&ecirc;ncias da Ficha ' + titulo,
                autoOpen: false,
                modal: true,
                close: removeDivModal
            }).dialog('open');

            return false;
        }

        ///////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////


        var tipoEscolhido = null;

        function finalizarAprovacao(){
            var erros = [];
            if($j('#COMENTARIO_JOB_APROVACAO').val() == '') erros.push('Informe algum comentario');
            if($j('#ETAPA').length > 0 && $j('#ETAPA').val() == '') erros.push('Informe a etapa');

            if( erros.length > 0 ){
                alert( erros.join('\n') );

            } else {
                $j.post('<?php echo site_url('checklist/aprovacao/'.$job['ID_JOB']); ?>/' + tipoEscolhido,
                        $j('#formAprovacao').serialize(),
                        function(html){
                            location.href = '<?php echo site_url('checklist/index'); ?>';
                        }
                );
            }
        }

        function finalizar(pop){
            if(pop == 1){
                displayMessagewithparameter('<?php echo site_url('checklist/finalizar/'.$job['ID_JOB']); ?>/1', 800, 550);
            }else if(pop == 0){
                $j.post('<?php echo site_url('checklist/finalizar/'.$job['ID_JOB']); ?>/0',
                        $j('#formAprovacao').serialize(),
                        function(html){
                            location.href = '<?php echo site_url('checklist/index'); ?>';
                        }
                );
            }
        }

        $j(".itemPaginacao").live("click", function(){
            carregaPraca(pracaAtual, $j(this).attr("offset"), $j(this).attr("limit"));
        });

        $j('.btnRemove').live('click', function() {
            var $btn = $(this);
            $j.ajax({
                url: base_url + 'index.php/json/fluxo/removeExcelItems',
                type: "Post",
                data: { ID_JOB: idjob, ID_PRACA: pracaAtual,FidExcelItem: $j($btn.closest("tr")).attr("idExcelItem")},
                success: function (data) {
                    console.log(data);
                    $($btn.closest("tr")).remove();
                }
            });

            return false;
        });

        $j('.btnCancelarEdicao').on('click', function() {
            $j(".campoEditavelPauta").each(function(){
                $j(this).val($j(this).attr('originalValue'));
            });

            $j(".campoEditavelPauta").attr("disabled", "disabled");
            $j('.btnEditar').val("Editar");
            $j('.btnCancelarEdicao').hide();
        });

        $j(".btnEditar").live('click', function(){
            var attr = $j(".campoEditavelPauta").eq(0).attr("disabled");

            if (typeof attr !== typeof undefined && attr !== false) {
                $j(".campoEditavelPauta").removeAttr("disabled");

                $j(".campoEditavelPauta").each(function(){
                    $j(this).attr('originalValue', $j(this).val());
                });


                $j(this).val("Salvar edição");
                $j('.btnCancelarEdicao').show();
            }
            else {
                $j('.btnCancelarEdicao').hide();

                var paginacaoData = '';
                var pricingData = '';

                var todasPaginas = $j("[nomecampo=PAGINA_ITEM]");
                var todasOrdens = $j("[nomecampo=ORDEM_ITEM]");
                var todasObervacoes = $j("[nomecampo=OBSERVACOES]");

                todasPaginas.css({ "border": 'inherit'});
                todasOrdens.css({ "border": 'inherit'});

                var erro = false;
                var erroOrdem = false;
                $j('.itemPauta').each(function() {
                    var pagina = $j("[nomecampo=PAGINA_ITEM]", this);
                    var ordem = $j("[nomecampo=ORDEM_ITEM]", this);
                    var observacao = $j("[nomecampo=OBSERVACOES]",this);


                    /*if(!erro && pagina.val() != "0"&& ordem.val() != "0") {
                     var paginas = todasPaginas.not(pagina);
                     var ordens = todasOrdens.not(ordem);

                     for(var i = 0; i < paginas.length; i++){
                     if(paginas.eq(i).val() == pagina.val() && ordens.eq(i).val() == ordem.val()){
                     erro = true;
                     alert("Há paginações e ordem conflitantes, por favor confira o preenchimento dessas informações destacas.");
                     paginas.eq(i).css({ "border": '#FF0000 1px solid'});
                     ordens.eq(i).css({ "border": '#FF0000 1px solid'});
                     pagina.css({ "border": '#FF0000 1px solid'});
                     ordem.css({ "border": '#FF0000 1px solid'});
                     break;
                     }


                     }

                     }*/

                    if(pagina.val() > pagina.attr("max")) {
                        erro = true;
                        /*alert("Verifique as paginas preenchidas");*/
                        pagina.css({ "border": '#FF0000 1px solid'});
                    }

                  // Verifica Ordem
                    if(!erro && pagina.val() != "0" && ordem.val() != "0")  {
                        var paginas = todasPaginas.not(pagina);
                        var ordens = todasOrdens.not(ordem);
                        
                        for(var i = 0; i < paginas.length; i++) {
                            if(paginas.eq(i).val() == pagina.val() && ordens.eq(i).val() == ordem.val())    {
                                erroOrdem = true;
                                ordens.eq(i).css({ "border": '#FF0000 1px solid'});
                                ordem.css({ "border": '#FF0000 1px solid'});
                                break;
                            }
                        }
                    }
                    
                    //Tira o Enter de Observações
                    if(!erro && observacao.val() != "")  {
                        var NovoObs = "";
                        NovoObs = observacao.val();
                        NovoObs = NovoObs.replace(/(\r\n|\n|\r)/gm," ");
                        observacao.val(NovoObs);
                    }

                    /*if(!erro && pagina.val() != "0"&& ordem.val() != "0") {
                     var paginas = todasPaginas.not(pagina);
                     var ordens = todasOrdens.not(ordem);

                     for(var i = 0; i < paginas.length; i++){
                     if(paginas.eq(i).val() == pagina.val() && ordens.eq(i).val() == ordem.val()){
                     erro = true;
                     alert("Há paginações e ordem conflitantes, por favor confira o preenchimento dessas informações destacas.");
                     paginas.eq(i).css({ "border": '#FF0000 1px solid'});
                     ordens.eq(i).css({ "border": '#FF0000 1px solid'});
                     pagina.css({ "border": '#FF0000 1px solid'});
                     ordem.css({ "border": '#FF0000 1px solid'});
                     break;
                     }


                     }

                     }

                     if(pagina.val() > pagina.attr("max"))
                     {
                     erro = true;
                     alert("Verifique as paginas preenchidas");
                     pagina.css({ "border": '#FF0000 1px solid'});
                     }*/
                });

                if (erro) {
                    alert("Verifique as paginas preenchidas");
                    return;
                }
                
                if(erroOrdem)   {
                    alert("Há ordens conflitantes, por favor conferir os campos destacados");
                    return;
                }
                if(!erro && !erroOrdem) {
                    $j('.itemPauta').each(function() {
                        var itemId = $j(this).attr('idExcelItem');
                        var idPricing = $j(this).attr('idPricing');

                        paginacaoData += 'codigo_item=' + itemId + '&';
                        pricingData += 'item[' + idPricing + '][ID_EXCEL_ITEM]=' + itemId + '&';

                        $j('.campoEditavelPauta', this).each(function() {
                            pricingData += 'item[' + idPricing + '][' + $j(this).attr('nomeCampo') +']=' + $j(this).val() + '&';
                        });
                    });

                    displayStaticMessagewithparameter('Salvando dados',200,80);

                    $j.ajax({
                        url: base_url + 'index.php/json/fluxo/salvar_alteracao_checklist',
                        type: "Post",
                        data: pricingData + "idjob=" + idjob + "&idPraca=" + pracaAtual ,
                        success: function (data) {
                            if(data){
                                alert(data);
                            }
                            else {
                                $j(".campoEditavelPauta").attr("disabled", "disabled");
                                $j('.btnEditar').val("Editar");
                            }

                            closeMessage();

                        }
                    });
                }
            }
        });

        function assignModalHandlers(){
            $('ID_CATEGORIA').addEvent('change', function(){
                clearSelect($('ID_SUBCATEGORIA'),1);
                montaOptionsAjax($('ID_SUBCATEGORIA'),'<?php echo site_url('json/admin/getSubcategoriasByCategoria'); ?>','id=' + this.value,'ID_SUBCATEGORIA','DESC_SUBCATEGORIA');
            });
        }

        $j(".btnAdicionar").on("click", function() {
            removeDivModal();
            displayMessagewithparameter('<?php echo base_url().index_page().'/carrinho/form_pesquisa/'.$this->uri->segment(3); ?>',800,600,assignModalHandlers);
        });

        function doPesquisa(pg){
            $j('#resultado').html('Pesquisando...');
            $j.post('<?php echo site_url('json/ficha/search'); ?>/' + pg,

                    $j('#formPesquisa').serialize(),

                    function(html){
                        $j('#resultado').html( html );
                        $j('.jqzoom').fancybox();
                    }
            );
        }

        function fancyboxDetalhes(){
            $j(".jqzoom2").fancybox();
            $j('#fancy_overlay').css('z-index', 1000000);
            $j('#fancy_wrap').css('z-index', 1000001);
            $j('#fancy_outer').css('z-index', 1000002);

        }


        function addLinhasSelecionadas(){
            var postData = [];

            $j('.selecao').each(function(){
                if( this.checked ){
                    var obj = eval('['+$j(this).parent().find('textarea').val()+']')[0];
                    postData.push(obj);
                }
            });

            closeMessage();
            displayStaticMessagewithparameter('Salvando dados',200,80);

            $j.ajax({
                url: base_url + 'index.php/json/fluxo/adicionarCarrinho',
                type: "Post",
                data: { ID_JOB: idjob, ID_PRACA: pracaAtual, ITEMS:  postData },
                success: function (data) {
                    pesquisa();
                    closeMessage();
                }
            });
        }
        
function nl2br (str, is_xhtml) {   
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}
    </script>
</div>
<!-- Final de Content Global -->
<?php $this->load->view("ROOT/layout/footer") ?>
