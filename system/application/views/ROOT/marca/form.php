<?php $this->load->view("ROOT/layout/header"); ?>

<div id="contentGlobal">

<?php if ( isset($erros) ) : ?>
	<ul style="color:#ff0000;font-weight:bold;">
		<?php foreach ( $erros as $e ) : ?>
			<li><?=$e?></li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>

<h1>Marcas Dispon&#237;veis</h1>
	
	<?php echo form_open('marca/save/'.$this->uri->segment(3));?>
    
    	<table width="50%" border="0" cellspacing="0" cellpadding="0" class="Alignleft">
  <tr>
    <td width="100">Fabricante:</td>
    <td>
    <select name="ID_FABRICANTE" id="ID_FABRICANTE">
	        <option value=""></option>
	        <?php echo montaOptions($fabricantes,'ID_FABRICANTE','DESC_FABRICANTE', post('ID_FABRICANTE',true)); ?>
	    </select>
    </td>
  </tr>
  <tr>
    <td>Marca:</td>
    <td>        <input name="DESC_MARCA" type="text" value="<?php post('DESC_MARCA'); ?>" />
        <?php showError('DESC_MARCA'); ?></td>
  </tr>
  <tr>
    <td>Situa&ccedil;&atilde;o:</td>
    <td>        <select name="FLAG_ACTIVE_MARCA">
          <option value="1">Ativo</option>
          <option value="0"<?php echo post('FLAG_ACTIVE_MARCA',true)=='0' ? ' selected="selected"' : ''; ?>>Inativo</option>
        </select></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>


	<a class="button" href="javascript:;" onclick="$j('form').submit()"><span>Salvar</span></a>
	<a class="button" href="<?php echo site_url('marca/lista'); ?>"><span>Cancelar</span></a>
		
	
<div>&nbsp; </div>
		
	<?php echo form_close();?>
	
</div><!-- Final de Content Global -->
	
<?php $this->load->view("ROOT/layout/footer") ?>