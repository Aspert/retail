<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">

<h1>Marcas Dispon&iacute;veis</h1>

<form method="post" action="<?php echo site_url('marca/lista'); ?>">
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="pesquisa_simples">
  <tr>
    <?php if($podeAlterar): ?>
    <td width="8%" rowspan="1" align="center">
      <a href="<?php echo site_url('marca/form'); ?>">
        <img src="<?php echo base_url().THEME; ?>img/file_add.png" alt="Pesquisar"/>      </a>     </td>
    <?php endif; ?>
    <td width="28%" nowrap="nowrap">Fabricante: <select name="ID_FABRICANTE" id="ID_FABRICANTE">
      <option value=""></option>
      <?php echo montaOptions($fabricantes,'ID_FABRICANTE','DESC_FABRICANTE', !empty($busca['ID_FABRICANTE']) ? $busca['ID_FABRICANTE'] : ''); ?>
      </select></td>
    <td width="28%">Marca: 
      <input type="text" name="DESC_MARCA" id="DESC_MARCA" value="<?php echo !empty($busca['DESC_MARCA']) ? $busca['DESC_MARCA'] : ''; ?>" /></td>
    <td width="15%">Itens por p&aacute;gna: <select name="pagina" id="pagina">
      <?php
            $pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
            for($i=5; $i<=50; $i+=5){
				printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
            }
            ?>
      </select></td>
    <td width="22%" rowspan="1"><a class="button" href="javascript:;" onclick="$j('form').submit()"><span>Pesquisar</span></a></td>
  </tr>
  </table>
</form>


<br />
<?php if(!empty($marcas)) :?>

<div id="tableObjeto">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableSelection">
<thead>
        <tr>
           <?php if($podeAlterar): ?>
           <th>Editar</th>
		   <?php endif; ?>
           <th>Fabricante</th>
           <th>Marca</th>
           <th>Situa&ccedil;&atilde;o</th>
        </tr>
</thead>
		<?php foreach($marcas as $item):?>
		<tr>
          <?php if($podeAlterar): ?>
          <td align="center" width="10%"><a href="<?php echo site_url('marca/form/' . $item['ID_MARCA']); ?>"><img src="<?php echo base_url().THEME; ?>img/file_edit.png" alt="Alterar" /></a></td>
          <?php endif; ?>
          <td align="center"><?php echo $item['DESC_FABRICANTE']; ?></td>
		  <td align="center"><?php echo $item['DESC_MARCA']; ?></td>
		  <td align="center"><?php echo $item['FLAG_ACTIVE_MARCA'] == 1 ? 'Ativo' : 'Inativo'; ?></td>
		</tr>
		<?php endforeach; ?>
</table>
</div> <!-- Final de Table Objeto -->

<span class="paginacao"><?php echo $paginacao; ?></span>
<?php else:?>
	Nenhum resultado
<?php endif;?>

</div> <!-- Final de Content Global -->

<?php $this->load->view("ROOT/layout/footer") ?>