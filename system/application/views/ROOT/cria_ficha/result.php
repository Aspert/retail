<?php $this->load->view("ROOT/layout/header"); ?>
<div id="contentGlobal">
<h1>Cria&ccedil;&atilde;o de fichas
  <?= $agencia['DESC_AGENCIA']?>
  /
  <?= $cliente['DESC_CLIENTE'] ?>
  /
  <?= $tipo['DESC_TIPO_OBJETO'] ?>
</h1>
<br />
<br />
<div id="tabs" style="width:90%;">
  <ul>
    <li><a href="#tab0">Objetos encontrados</a></li>
    <li><a href="#tab1">Fichas criadas</a></li>
    <li><a href="#tab2">Fichas j&aacute; existentes (ignoradas)</a></li>
    <li><a href="#tab3">Fichas n&atilde;o criadas</a></li>
  </ul>
  <div id="tab0" style="min-height:250px">
    <h4>Objetos encontrados</h4>
    <?= $objFound ?>
    objetos. <br />
    <br />
    <br />
  </div>
  <div id="tab1" style="min-height:250px">
    <h4>Fichas criadas</h4>
    <?= count($fichasCreated) ?>
    fichas. <br />
    <? if(count($fichasCreated) > 0): ?>
    <h4>C&oacute;digos</h4>
    <?= implode(', ',$fichasCreated) ?>
    <br />
    <? endif; ?>
    <br />
    <br />
    <br />
  </div>
  <div id="tab2" style="min-height:250px">
    <h4>Fichas j&aacute; existentes (ignoradas)</h4>
    <?= count($fichasFound) ?>
    fichas. <br/>
    <? if(count($fichasFound) > 0): ?>
    <h4>C&oacute;digos</h4>
    <div style="width:100%; height:200px; overflow:auto">
      <?= implode('<br />',$fichasFound) ?>
    </div>
    <br/>
    <? endif; ?>
    <br />
    <br />
    <br />
  </div>
  <div id="tab3" style="min-height:250px">
    <h4>Fichas n&atilde;o criadas</h4>
    <?= count($fichasNotCreated) ?>
    fichas. <br/>
    <? if(count($fichasNotCreated) > 0): ?>
    <h4>As fichas n&atilde;o foram criadas para os objetos abaixo por falta de informa&ccedil;&otilde;es na base de dados(ag&ecirc;ncia, cliente,tipo,categoria, subcategoria): </h4>
    <div style="width:100%; height:200px; overflow:auto">
      <?= implode('<br />',$fichasNotCreated) ?>
    </div>
    <br />
    <br />
    <br />
    <? endif; ?>
  </div>
</div>
<div style="width:90%; text-align:right"> <a href="<?php echo site_url('cria_ficha/form'); ?>" class="button"><span>Cancelar</span></a></div>
</div>
<script type="text/javascript">
$j('#tabs').tabs();
</script>
<?php $this->load->view("ROOT/layout/footer") ?>
