<?php $this->load->view("ROOT/layout/header"); ?>

<div id="contentGlobal">
  <?php if ( isset($erros) && is_array($erros) ) : ?>
  <ul style="color:#ff0000;font-weight:bold;">
    <?php foreach ( $erros as $e ) : ?>
    <li>
      <?=$e?>
    </li>
    <?php endforeach; ?>
  </ul>
  <?php endif; ?>
  <?php echo form_open('cria_ficha/create/')?>
  <h1>Criar fichas</h1>
  <h4>Cria fichas apenas para objetos que possuam tipo, categoria, subcategoria. <br />
    Os tipos que aparecem s&atilde;o apenas os que possuem categoria obrigat&oacute;ria.</h4>
    <br />
  <table width="100%" class="Alignleft">
    <tr>
    <td width="23%">Ag&ecirc;ncia *</td>
    <td width="77%"><select name="ID_AGENCIA" id="ID_AGENCIA">
      <option value=""></option>
      <?php echo montaOptions($agencias, 'ID_AGENCIA','DESC_AGENCIA', post('ID_AGENCIA',true)); ?>
    </select></td>
  </tr>
  <tr>
    <td>Cliente *</td>
    <td><select name="ID_CLIENTE" id="ID_CLIENTE">
      <option value=""></option>
    </select></td>
  </tr>
  <tr>
    <td>Tipo *</td>
    <td><select name="ID_TIPO_OBJETO" id="ID_TIPO_OBJETO">
      <option value=""></option>
    </select></td>
  </tr>
  <tr>
    <td>Usa keyword como nome da ficha?</td>
    <td><input name="USA_KEYWORD" type="checkbox" value="1" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><a href="<?= site_url('admin/index') ?>" class="button"><span>Cancelar</span></a>
    <a href="javascript:;" onclick="$j(this).closest('form').submit()" class="button"><span>Salvar</span></a></td>
  </tr>
</table>

  
  <div>&nbsp; </div>
  <?php echo form_close();?> </div>
<script type="text/javascript">

$('ID_AGENCIA').addEvent('change', function(){
	clearSelect($('ID_CLIENTE'),1);
	clearSelect($('ID_TIPO_OBJETO'),1);
	montaOptionsAjax($('ID_CLIENTE'),'<?php echo site_url('json/admin/getClientesByAgencia'); ?>','id=' + this.value,'ID_CLIENTE','DESC_CLIENTE');
});

$('ID_CLIENTE').addEvent('change', function(){
	clearSelect($('ID_TIPO_OBJETO'),1);
	montaOptionsAjax($('ID_TIPO_OBJETO'),'<?php echo site_url('json/admin/getTiposByClienteComCategoria'); ?>','id=' + this.value,'ID_TIPO_OBJETO','DESC_TIPO_OBJETO');
});

</script>
<?php $this->load->view("ROOT/layout/footer") ?>
