<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
<h1>Calculo de parcela dinâmica</h1>
<form method="post" action="<?php echo site_url('parcela_dinamica/index'); ?>" id="pesquisa_simples">
<table width="100%" border="1" cellspacing="1" cellpadding="2" class="AlignLeft" >
  <tr>
    <td width="8%" rowspan="2" align="center">
	<?php if($podeAlterar): ?>
    <a href="<?php echo site_url('parcela_dinamica/form'); ?>"><img src="<?php echo base_url().THEME; ?>/img/file_add.png" alt="Adicionar" width="31" height="31" border="0" /></a>
    <?php endif; ?></td>
    <td width="21%">Cliente</td>
    <td width="15%"></td>
    <td width="19%" rowspan="2"><a class="button" title="Pesquisa" onclick="$j(this).closest('form').submit()"><span>Pesquisa</span></a></td>
  </tr>
  <tr>
    <td>
    <?php if(!empty($_sessao['ID_CLIENTE'])): ?>
		<?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?>
    <?php else: ?>
        <select name="ID_CLIENTE" id="ID_CLIENTE">
            <option value=""></option>
            <?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', empty($busca['ID_CLIENTE']) ? '' : $busca['ID_CLIENTE']); ?>
        </select>
    <?php endif; ?>
    </td>
    <td></td>
    </tr>
</table>
</form>

<?php if( !empty($parcelasDinamicas)) :?>
<br />
    <div id="tableObjeto">
    <table cellspacing="1" cellpadding="2" border="0" width="99%" class="tableSelection">
            <thead>
                <tr>
                    <?php if($podeAlterar): ?>
                    <th width="10%">Editar</th>
                    <?php endif; ?>
                  <th width="90%">Cliente</th>
                </tr>
            </thead>
            <?php foreach($parcelasDinamicas as $item): ?>
            <tr>
              <?php if($podeAlterar): ?>
                <td align="center"><a href="<?php echo site_url('parcela_dinamica/form/'.$item['ID_CLIENTE']); ?>">
                <img src="<?php echo base_url().THEME; ?>/img/file_edit.png" alt="Adicionar" border="0" /></a>
                </td>
              <?php endif; ?>
                <td align="center"><?php echo $item['DESC_CLIENTE']; ?></td>
            </tr>
            <?php endforeach; ?>
    </table>
    </div>
</div>

<?php else: ?>
	Nenhum resultado
<?php endif; ?>

<script type="text/javascript">
if($('ID_AGENCIA')){
	$('ID_AGENCIA').addEvent('change', function(){
		clearSelect($('ID_CLIENTE'),1);
		montaOptionsAjax($('ID_CLIENTE'),'<?php echo site_url('json/admin/getClientesByAgencia'); ?>','id=' + this.value,'ID_CLIENTE','DESC_CLIENTE');
	});
}

</script>

<?php $this->load->view("ROOT/layout/footer") ?>