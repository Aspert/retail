<?php $this->load->view("ROOT/layout/header"); ?>
<?php $_POST['ID_CLIENTE'] = $this->uri->segment(3); ?>
<div id="contentGlobal">
<h1>Cálculo de Parcelas Dinâmicas</h1>
<form method="post" action="<?php echo site_url('parcela_dinamica/save/'.$this->uri->segment(3)); ?>" id="form1">
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabela_pesquisa tabela_form">
	<tr>
		<td>Cliente:
			<?php if(!empty($_sessao['ID_CLIENTE'])): ?>
				<strong><?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?></strong>
			<?php else: ?>
				<select name="ID_CLIENTE" id="ID_CLIENTE">
					<option value=""></option>
					<?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', post('ID_CLIENTE',true)); ?>
				</select>
			<?php endif; ?>
		</td>
	</tr>
	<tr>
		<td>
		
			<br>
			<table width="70%" border="0" cellspacing="1" cellpadding="2" class="AlignLeft" >
				<tr>
					<td>
						Valor Mínimo:
						<input name="VISTA_1" id="VISTA_1" class="VISTA_1" type="text"/>
					</td>
					<td>
						Valor Máximo:
						<input name="VISTA_2" id="VISTA_2" class="VISTA_1" type="text"/>
					</td>
					<td>
						Parcelas:
						<input name="PARCELA" id="PARCELA" type="text" />
					</td>
					<td>
						<a class="button" title="Adicionar" onclick="insRowParcelaDinamica();"><span>Adicionar</span></a>
					</td>
				</tr>
			</table>
			
			<br/><br/>
			
			<div id="tableObjeto">
				<table id="tblParcelaDinamica" cellspacing="1" cellpadding="2" border="0" width="99%" class="tableSelection">
					<thead>
						<tr>
							<td width="30%">Valor Mínimo</td>
							<td width="30%">Valor Máximo</td>
							<td width="30%">Qtde Parcelas</td>
							<td width="10%">Excluir</td>
						</tr>
					</thead>
					<?php $arredonda = 0; ?>
					<?php $ajusta = 0; ?>
					<?php foreach($parcelasDinamicas as $pd): ?>
						<?php $arredonda = $pd['ARREDONDA']; ?>
						<?php $ajusta = $pd['AJUSTA']; ?>
						<tr>
							<td>
								<?php echo money($pd['VALOR_VISTA_1'], 'R$ '); ?>
								<input name="VALOR_VISTA_1[]" class="VALOR_VISTA_1" type="hidden" value="<?php echo money($pd['VALOR_VISTA_1']); ?>"/>
							</td>
							<td>
								<?php echo money($pd['VALOR_VISTA_2'], 'R$ '); ?>
								<input name="VALOR_VISTA_2[]" class="VALOR_VISTA_2" type="hidden" value="<?php echo money($pd['VALOR_VISTA_2']); ?>"/>
							</td>
							<td>
								<?php echo $pd['PARCELAS']; ?>
								<input name="PARCELAS[]" class="PARCELAS" type="hidden" value="<?php echo $pd['PARCELAS']; ?>"/>
							</td>
							<td>
								<img src="<?php echo base_url().THEME; ?>img/trash.png" onClick="$j(this).closest('tr').remove();" style="cursor:pointer;">
							</td>
						</tr>
					<?php endforeach;?>
					<tr id="modelo_linha">
						<td><input name="VALOR_VISTA_1[]" class="VALOR_VISTA_1" type="hidden" value="{VALOR_VISTA_1}"/>{VISTA_1}</td>
						<td><input name="VALOR_VISTA_2[]" class="VALOR_VISTA_2" type="hidden" value="{VALOR_VISTA_2}"/>{VISTA_2}</td>
						<td><input name="PARCELAS[]" type="hidden" value="{PARCELAS}"/>{PARCELA}</td>
						<td><img src="<?php echo base_url().THEME; ?>img/trash.png" onClick="$j(this).closest('tr').remove();" style="cursor:pointer;"></td>
					</tr>
				</table>
				<br>
				<input type="checkbox" name="ARREDONDA" value="1" <?php if($arredonda == 1){echo 'checked="checked"';}?>>
				Arredondar valores das parcelas para 0, 5 ou 9
				<br>
				<input type="checkbox" name="AJUSTA" value="1" <?php if($ajusta == 1){echo 'checked="checked"';}?>>
				Ajustar valor a vista conforme parcelas
			</div>			
		
		</td>
	</tr>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td>
			<a class="button" href="<?php echo site_url('parcela_dinamica/index'); ?>" title="Cancelar"><span>Cancelar</span></a>
			<a class="button" title="Pesquisa" onclick="$j(this).closest('form').submit()"><span>Gravar</span></a>
		</td>
	</tr>
</table>
</form>
</div>

<script type="text/javascript">
var modelo_linha = null;

$j(function($) {
	modelo_linha = $j('#modelo_linha').clone();
	$j('#modelo_linha').remove();
	
	$j('#VISTA_1').maskMoney({symbol: 'R$', thousands: '.', decimal: ','});
	$j('#VISTA_2').maskMoney({symbol: 'R$', thousands: '.', decimal: ','});

	$j('#ID_CLIENTE').change(
			function(){
				location.href = '<?php echo site_url('parcela_dinamica/form/'); ?>'+ '/' +$j('#ID_CLIENTE').val();
	});
});

function verificaRangeRepetido(valor1, valor2){
	valor1 = parseFloat(valor1.replace(/\./g, "").replace(/\,/g, ""));
	valor2 = parseFloat(valor2.replace(/\./g, "").replace(/\,/g, ""));
	var arrVista1 = new Array();
	var arrVista2 = new Array();
	
	$j('.VALOR_VISTA_1').each(function(){
		arrVista1.push(parseFloat($j(this).val().replace(/\./g, "").replace(/\,/g, "")));
	});

	$j('.VALOR_VISTA_2').each(function(){
		arrVista2.push(parseFloat($j(this).val().replace(/\./g, "").replace(/\,/g, "")));
	});

	for(var i = 0; i <= arrVista1.length - 1; i++){
		if(valor1 >= arrVista1[i] && valor1 <= arrVista2[i]){
			return true;
		}
		if(valor2 >= arrVista1[i] && valor2 <= arrVista2[i]){
			return true;
		}
	}

	return false;
}

function insRowParcelaDinamica(){
	if($j('#ID_CLIENTE').val() != ""){
		if(($j('#VISTA_1').val() != '') && ($j('#VISTA_2').val() != '') && ($j('#PARCELA').val() != '')){
			if(!isNaN($j('#PARCELA').val())){
				var vista1 = parseFloat($j('#VISTA_1').val().replace(/\./g, "").replace(/\,/g, ""));
				var vista2 = parseFloat($j('#VISTA_2').val().replace(/\./g, "").replace(/\,/g, ""));
				if(vista1 < vista2){
					if(!verificaRangeRepetido($j('#VISTA_1').val(), $j('#VISTA_2').val())){
						var html = modelo_linha.html();
						html = html.replace('{VISTA_1}', 'R$ '+$j('#VISTA_1').val());
						html = html.replace('{VISTA_2}', 'R$ '+$j('#VISTA_2').val());
						html = html.replace('{PARCELA}', $j('#PARCELA').val());
						html = html.replace('{VALOR_VISTA_1}', $j('#VISTA_1').val());
						html = html.replace('{VALOR_VISTA_2}', $j('#VISTA_2').val());
						html = html.replace('{PARCELAS}', $j('#PARCELA').val());
						$j('#tblParcelaDinamica').append('<tr>'+html+'</tr>');
						$j('#VISTA_1').val('');
						$j('#VISTA_2').val('');
						$j('#PARCELA').val('');
					}
					else{
						alert('Faixa de valores ja cadastrada')
					}
				}
				else{
					alert('Valor mínimo é maior que o valor máximo');
				}
			}
			else{
				alert('Valor da parcela inválido');
				$j('#PARCELA').focus();
			}
		}
		else{
			alert('Preencha todos os campos');
		}
	}
	else{
		alert('Selecione um cliente')
	}
}
</script>

<?php $this->load->view("ROOT/layout/footer") ?>

































