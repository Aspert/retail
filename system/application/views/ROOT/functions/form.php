<?php $this->load->view("ROOT/layout/header") ?>
<div id="contentGlobal"><?php if ( isset($erros) && is_array($erros) ) : ?>
<ul style="color:#ff0000;font-weight:bold;">
  <?php foreach ( $erros as $e ) : ?>
  <li>
    <?=$e?>
  </li>
  <?php endforeach; ?>
</ul>
<?php endif; ?>
<h1>Cadastro de Fun&ccedil;&otilde;es</h1>
<?php echo form_open('functions/save/'.$this->uri->segment(3));?>
<table width="100%" class="Alignleft">
  <tr>
    <td width="14%">Vincular a controller</td>
    <td width="86%"><select name="ID_CONTROLLER" id="ID_CONTROLLER">
        <option value=""></option>
        <?php echo montaOptions($controllers,'ID_CONTROLLER','DESC_CONTROLLER',post('ID_CONTROLLER',true)); ?>
      </select></td>
  </tr>
  <tr>
    <td>Descri&ccedil;&atilde;o da function</td>
    <td><input name="DESC_FUNCTION" type="text" value="<?php post('DESC_FUNCTION'); ?>" /></td>
  </tr>
  <tr>
    <td>Chave interna</td>
    <td><input name="CHAVE_FUNCTION" type="text" value="<?php post('CHAVE_FUNCTION'); ?>" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><a href="<?php echo site_url('functions/lista'); ?>" class="button"><span>Cancelar</span></a> <a href="javascript:;" onclick="$j(this).closest('form').submit()" class="button"><span>Salvar</span></a></td>
  </tr>
</table>
<?php echo form_close(); ?></div>
<?php $this->load->view("ROOT/layout/footer") ?>
