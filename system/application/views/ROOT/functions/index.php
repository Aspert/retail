<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
  <h1>Cadastro de Fun&ccedil;&otilde;es</h1>
  <form action="<?php echo site_url('functions/lista'); ?>" method="post" id="pesquisa_simples" name="form1">
    <table width="100%" class="Alignleft">
      <tr>
        <?php if($podeAlterar): ?>
        <td width="6%" rowspan="2" align="center"><a href="<?php echo site_url('functions/form'); ?>"><img src="<?php echo base_url().THEME; ?>img/file_add.png" alt="Pesquisar" width="31" height="31" border="0" /></a></td>
        <?php endif; ?>
        <td width="20%">Controller</td>
        <td width="28%">Nome da function</td>
        <td width="25%">Itens por p&aacute;gna</td>
        <td width="21%" rowspan="2"><a class="button" href="javascript:" title="Pesquisar" onclick="$j(this).closest('form').submit()"><span>Pesquisar</span></a></td>
      </tr>
      <tr>
        <td><select name="ID_CONTROLLER" id="ID_CONTROLLER">
            <option value=""></option>
            <?php echo montaOptions($controllers,'ID_CONTROLLER','DESC_CONTROLLER', !empty($busca['ID_CONTROLLER']) ? $busca['ID_CONTROLLER'] : ''); ?>
          </select></td>
        <td><input type="text" name="DESC_FUNCTION" id="DESC_FUNCTION" value="<?php echo !empty($busca['DESC_FUNCTION']) ? $busca['DESC_FUNCTION'] : ''; ?>" /></td>
        <td><select name="pagina" id="pagina">
            <?php
            $pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
            for($i=5; $i<=50; $i+=5){
				printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
            }
            ?>
          </select></td>
      </tr>
    </table>
  </form>
  <?php if(!empty($lista)): ?>
  <div id="tableObjeto">
    <table width="100%" class="tableSelection">
      <thead>
        <tr>
          <?php if($podeAlterar): ?>
          <td width="4%">Editar</td>
          <?php endif; ?>
          <td width="6%">Remover</td>
          <?php if($podeRemover): ?>
          <td width="28%">Descri&ccedil;&atilde;o da controller</td>
          <?php endif; ?>
          <td width="35%">Descri&ccedil;&atilde;o da function</td>
          <td width="27%">Chave</td>
        </tr>
      </thead>
      <?php foreach($lista as $item): ?>
      <tr>
        <?php if($podeAlterar): ?>
        <td align="center"><a href="<?php echo site_url('functions/form/' . $item['ID_FUNCTION'] ); ?>"><img src="<?php echo base_url().THEME; ?>img/file_edit.png" alt="Editar" width="31" height="31" border="0" /></a></td>
        <?php endif; ?>
        <?php if($podeRemover): ?>
        <td align="center"><a href="#" onclick="remover(<?php echo $item['ID_FUNCTION']; ?>)"><img src="<?php echo base_url().THEME; ?>img/file_delete.png" alt="Remover" width="31" height="31" border="0" /></a></td>
        <?php endif; ?>
        <td><?php echo $item['DESC_CONTROLLER']; ?></td>
        <td><?php echo $item['DESC_FUNCTION']; ?></td>
        <td><?php echo $item['CHAVE_FUNCTION']; ?></td>
      </tr>
      <?php endforeach; ?>
    </table>
  </div>
  <span class="paginacao"><?php echo $paginacao; ?></span>
  <?php else: ?>
  Nenhum resultado encontrado
  <?php endif ;?>
</div>
<script type="text/javascript">
function remover(id){
	if(confirm('Deseja realmente remover este item?')){
		location.href = '<?php echo base_url().index_page().'/functions/remover/' ?>'+id;
	}
}
</script>
<?php $this->load->view("ROOT/layout/footer") ?>
