<div id="tableObjeto">
<?php if(!empty($lista)): ?>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tableSelection">
	<thead>
		<tr>
			<td style="text-align: left" width="25%"><strong>Usuário</strong></td>
			<td style="text-align: left" width="25%"><strong>Cliente</strong></td>
			<td style="text-align: left" width="18%"><strong>Data</strong></td>
			<td style="text-align: left" width="47%"><strong>Alterações</strong></td>
		</tr>
	</thead>
	<?php foreach($lista as $item): ?>
	<tr>
		<td style="text-align: left"><?php echo $item['NOME_USUARIO']; ?></td>
		<td style="text-align: left"><?php echo !empty($item['DESC_CLIENTE']) ? $item['DESC_CLIENTE'] : ''; ?></td>
		<td style="text-align: left"><?php echo date('d/m/Y H:i', strtotime($item['DATA_ALTERACAO'])); ?></td>
		<td style="text-align: left"><?php
		$data = unserialize($item['ALTERACOES']);
		foreach($data as $key => $val){
			if( preg_match('@^(\d{4})-(\d{2})-(\d{2})$@', $val) ){
				$val = date('d/m/Y', strtotime($val));
			}
			
			// 4456 -> Alterar visualizacao da tag de Status			
			if ($key == 'Status') {
				$key = 'Situação';
				if ($val == 0) {
					$val = 'Inativo';
				} else if ($val == 1) {
					$val = 'Ativo';
				}
			}
			
			echo $key . ': ' . $val . '<br />' . PHP_EOL;
		}
		?></td>
	</tr>
	<?php endforeach; ?>
</table>

<?php else: ?>
Nenhuma alteração encontrada
<?php endif; ?>
</div>