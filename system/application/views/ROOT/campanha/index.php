<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
  <h1>Campanhas</h1>
  <?php echo form_open('campanha/lista', array('id' => 'pesquisa_simples'));?>
  <table width="100%" class="Alignleft">
    <tr>
      <td width="7%" rowspan="4" align="center"><?php if($podeAlterar): ?>
      	<a href="<?php echo site_url('campanha/form'); ?>"> <img src="<?php echo base_url().THEME; ?>img/file_add.png" alt="Adicionar" width="31" height="31" border="0" /> </a>
      	<?php endif; ?></td>
      <td width="16%">Cliente</td>
      <td width="17%">Bandeira</td>
      <td width="19%">Nome da Campanha</td>
      <td width="17%">Itens por p&aacute;gina</td>
      <td width="17%">&nbsp;</td>
      <td width="9%" rowspan="4"><a class="button" href="javascript:" title="Pesquisar" onclick="$j(this).closest('form').submit()"><span>Pesquisar</span></a></td>
    </tr>
    <tr>
    	<td><?php if(!empty($_sessao['ID_CLIENTE'])): ?>
    		<?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?>
    		<?php else: ?>
    		<select name="ID_CLIENTE" id="ID_CLIENTE">
    			<option value=""></option>
    			<?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', !empty($busca['ID_CLIENTE']) ? $busca['ID_CLIENTE'] : ''); ?>
    			</select>
    		<?php endif; ?></td>
    	<td><?php if(!empty($_sessao['ID_PRODUTO'])): ?>
    		<?php sessao_hidden('ID_PRODUTO','DESC_PRODUTO'); ?>
    		<?php else: ?>
    		<select name="ID_PRODUTO" id="ID_PRODUTO">
    			<option value=""></option>
    			<?php echo montaOptions($produtos,'ID_PRODUTO','DESC_PRODUTO', !empty($busca['ID_PRODUTO']) ? $busca['ID_PRODUTO'] : ''); ?>
    			</select>
    		<?php endif; ?></td>
    	<td><input type="text" name="DESC_CAMPANHA" id="DESC_CAMPANHA" value="<?php echo !empty($busca['DESC_CAMPANHA']) ? $busca['DESC_CAMPANHA'] : '' ?>" /></td>
    	<td><span class="campo esq">
    		<select name="pagina" id="pagina">
    			<?php
		$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
		for($i=5; $i<=50; $i+=5){
			printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
		}
		?>
    			</select>
    		</span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Data de In&iacute;cio</td>
      <td>Data de T&eacute;rmino</td>
      <td>Situa&ccedil;&atilde;o</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><input name="DT_INICIO_CAMPANHA" type="text" id="DT_INICIO_CAMPANHA" class="_calendar" value="<?php echo val($busca,'DT_INICIO_CAMPANHA'); ?>" size="12" maxlength="12" /></td>
      <td><input name="DT_FIM_CAMPANHA" type="text" id="DT_FIM_CAMPANHA" class="_calendar" value="<?php echo val($busca,'DT_FIM_CAMPANHA'); ?>" size="12" maxlength="12" /></td>
      <td><select name="STATUS_CAMPANHA" id="STATUS_CAMPANHA">
        <option value="">TODOS</option>
        <option value="1"<?php echo val($busca,'STATUS_CAMPANHA')=='1' ? ' selected="selected"' : ''; ?>>Ativo</option>
        <option value="0"<?php echo val($busca,'STATUS_CAMPANHA')=='0' ? ' selected="selected"' : ''; ?>>Inativo</option>
      </select></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  <?php echo form_close();?>
  <?php if( (!empty($lista))&& (is_array($lista))):?>
  <div id="tableObjeto">
    <table id="tabelaBusca" border="0" width="100%" class="tableSelection">
      <thead>
        <tr>
          <?php if($podeAlterar): ?>
          <th>Editar</th>
          <?php endif; ?>
          <th width="90">Hist&oacute;rico</th>
          <th id="colCampanha" width="92">Campanha</th>
          <th id="colProduto" width="130">Bandeira</th>
          <th id="colCliente" width="162">Cliente</th>
          <th id="colDataInicio" width="101">Data In&iacute;cio</th>
          <th id="colDataTermino" width="109">Data T&eacute;rmino</th>
          <th id="colSituacao" width="114">Situa&ccedil;&atilde;o</th>
		  <th id="colDataAlteracao" width="170">Última Atualiza&ccedil;&atilde;o</th>
        </tr>
      </thead>
      <?php $cont=0; ?>
      <?php foreach($lista as $item):?>
      <tr>
        <?php if($podeAlterar): ?>
        <td align="center" width="69"><a href="<?php echo site_url('campanha/form/'.$item['ID_CAMPANHA']); ?>"><img src="<?php echo base_url().THEME; ?>img/file_edit.png" alt="Adicionar" width="31" height="31" border="0" /></a></td>
        <?php endif; ?>
        <td><a href="#" onclick="historicoAlteracoes(<?php echo $item['ID_CAMPANHA']; ?>); return false;"><img src="<?=base_url().THEME;?>img/visualizar_historico.png" alt="Hist�rico" border="0"/></a></td>
        <td><?=$item['DESC_CAMPANHA'];?></td>
        <td><?=$item['DESC_PRODUTO'];?></td>
        <td><?=$item['DESC_CLIENTE'];?></td>
        <td><?=format_date_to_form($item['DT_INICIO_CAMPANHA']);?></td>
        <td><?=format_date_to_form($item['DT_FIM_CAMPANHA']);?></td>
        <td><?=$item['STATUS_CAMPANHA'] == 1 ? 'ATIVO' : 'INATIVO';?></td>
		<td><?=format_date_to_form($item['DATA_ALTERACAO'], 'd-m-Y H:i:s');?></td>
        <?php $cont++; ?>
      </tr>
      <?endforeach;?>
    </table>
  </div>
  <span class="paginacao">
  <br />Total de resultados: <?php echo $total; ?><br />
  <?=(isset($paginacao))?$paginacao:null?>
  </span>
  <?else:?>
  Nenhum resultado
  <?endif;?>
</div>
<script type="text/javascript">

$('ID_CLIENTE').addEvent('change', function(){
	clearSelect($('ID_PRODUTO'),1);
	montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByCliente'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
});

$j(function(){

	var options = 
			{
			columns:[{col:'#colCampanha',label:'DESC_CAMPANHA'},
			         {col:'#colProduto',label:'DESC_PRODUTO'},
			         {col:'#colCliente',label:'DESC_CLIENTE'},
			         {col:'#colDataInicio',label:'DT_INICIO_CAMPANHA'},
			         {col:'#colDataTermino',label:'DT_FIM_CAMPANHA'},
			         {col:'#colSituacao',label:'STATUS_CAMPANHA'},
			         {col:'#colDataAlteracao',label:'DATA_ALTERACAO'}
	         		],
			form:'#pesquisa_simples',
			orderDirection: '<?= $busca['ORDER_DIRECTION'] ?>',
			selectedField:'<?= $busca['ORDER'] ?>'
			}
			
	$j('#tabelaBusca').sortgrid(options);

	$j('._calendar').datepicker({
		dateFormat: 'dd/mm/yy',
		showOn: 'button',
		buttonImage: new Util().options.theme + 'img/calendario.png',
		buttonImageOnly:true
	}).mask('99/99/9999');

});

function historicoAlteracoes(idcampanha){
	$j.post('<?php echo site_url('json/admin/getHistoricoCampanha'); ?>/'+idcampanha, null, function(html){
		getDivModal().dialog({
			width: 700,
			modal:true,
			autoOpen: false,
			title: 'Hist&oacute;rico de Altera&ccedil;&otilde;es'
		}).dialog('open');
		
		getDivModal().html( html );
	});
}

$j(function(){
	$j('.ui-datepicker').hide();
});

</script>
<?php $this->load->view("ROOT/layout/footer") ?>
