<?php $this->load->view("ROOT/layout/header");
$usuario =Sessao::get('usuario');
//print_rr($_POST);die;?>

<div id="contentGlobal">
  <?php if ( isset($erros) && is_array($erros) ) : ?>
  <ul style="color:#ff0000;font-weight:bold;" class="box_erros">
    <?php foreach ( $erros as $e ) : ?>
    <li>
      <?=$e?>
    </li>
    <?php endforeach; ?>
  </ul>
  <?php endif; ?>
  
  <?php echo form_open('campanha/save/'.$this->uri->segment(3));?>
  <h1>Cadastro de Campanha</h1>
  <table width="100%" class="Alignleft">
    <tr>
    	<td width="14%">Cliente *</td>
    	<td width="86%"><?php if($this->uri->segment(3) == ''): ?>
    		<!-- VERIFICA SE EH CLIENTE LOGADO -->
    		<?php if(!empty($_sessao['ID_CLIENTE'])): ?>
    		<?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?>
    		<br />
    		<?php else: ?>
    		<select name="ID_CLIENTE" id="ID_CLIENTE">
    			<option value=""></option>
    			<?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', post('ID_CLIENTE',true)); ?>
    			</select>
    		<?php endif; ?>
    		<!-- FIM VERIFICA SE EH CLIENTE LOGADO -->
    		<?php else: ?>
    		<?php post('DESC_CLIENTE'); ?>
    		<input name="DESC_CLIENTE" id="DESC_CLIENTE" type="hidden" value="<?php post('DESC_CLIENTE'); ?>" />
    		<input name="ID_CLIENTE" id="ID_CLIENTE" type="hidden" value="<?php post('ID_CLIENTE'); ?>" />
    		<br />
    		<?php endif; ?></td>
    	</tr>
    <tr>
      <td>Bandeira *</td>
      <td><?php if($this->uri->segment(3) == ''): ?>
        <!-- VERIFICA SE EH PRODUTO LOGADO -->
        <?php if(!empty($_sessao['ID_PRODUTO'])): ?>
        <?php sessao_hidden('ID_PRODUTO','DESC_PRODUTO'); ?>
        <br />
        <?php else: ?>
        <select name="ID_PRODUTO" id="ID_PRODUTO">
          <option value=""></option>
          <?php echo montaOptions($produtos,'ID_PRODUTO','DESC_PRODUTO', post('ID_PRODUTO',true)); ?>
        </select>
        <?php endif; ?>
        <!-- FIM VERIFICA SE EH PRODUTO LOGADO -->
        <?php else: ?>
        <?php post('DESC_PRODUTO'); ?>
        <input name="DESC_PRODUTO" id="DESC_PRODUTO" type="hidden" value="<?php post('DESC_PRODUTO'); ?>" />
        <input name="ID_PRODUTO" id="ID_PRODUTO" type="hidden" value="<?php post('ID_PRODUTO'); ?>" />
        <br />
        <?php endif; ?></td>
    </tr>
    <tr>
      <td>Nome *</td>
      <td><input name="DESC_CAMPANHA" id="DESC_CAMPANHA" type="text" value="<?php post('DESC_CAMPANHA'); ?>" /></td>
    </tr>
    <tr style="display: none;" id="codigo_catalogo">
      <td>Código do Catalogo *</td>
      <td><input name="COD_CATALOGO" id="COD_CATALOGO" type="text" size="1" maxlength="1" value="<?php post('COD_CATALOGO'); ?>" /></td>
    </tr>
    <tr>
      <td>Data de In&iacute;cio *</td>
      <td><input id="DT_INICIO_CAMPANHA" name="DT_INICIO_CAMPANHA" type="text" size="10"  readonly="readonly" class="_calendar" value="<?php post('DT_INICIO_CAMPANHA') ?>" /></td>
    </tr>
    <tr>
      <td>Data de T&eacute;rmino *</td>
      <td><input id="DT_FIM_CAMPANHA" name="DT_FIM_CAMPANHA" type="text" size="10" readonly="readonly"  class="_calendar" value="<?php post('DT_FIM_CAMPANHA') ?>" /></td>
    </tr>
    <tr>
      <td>Situa&ccedil;&atilde;o</td>
      <td><select name="STATUS_CAMPANHA">
          <option value="1">ATIVO</option>
          <option value="0"<?php echo isset($_POST['STATUS_CAMPANHA']) && $_POST['STATUS_CAMPANHA'] == '0' ? ' selected="selected"' : ''; ?>>INATIVO</option>
        </select></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><a href="<?php echo site_url('campanha/lista'); ?>" class="button"><span>Cancelar</span></a> <a href="javascript:;" onclick="if(valida_data()){$j(this).closest('form').submit()}" class="button"><span>Salvar</span></a></td>
    </tr>
  </table>
  <?php echo form_close();?></div>
<script type="text/javascript">

$('ID_CLIENTE').addEvent('change', function(){
	clearSelect($('ID_PRODUTO'),1);
	montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByCliente'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
	$j('#ID_CLIENTE option:selected').text();

	if ($j('#ID_CLIENTE option:selected').text().toUpperCase() == 'HERMES'){
		$j('#codigo_catalogo').show();
	} else {
		$j('#codigo_catalogo').hide();
	}
});

$j(function() {
	var is_hermes = "<?php echo $usuario['IS_CLIENTE_HERMES'] ?>";
	var campanha_hermes = "<?php echo isset($_POST['ID_CLIENTE']) ? $_POST['ID_CLIENTE'] : '' ?>";

	//alert(campanha_hermes);
	if ((is_hermes != undefined && is_hermes == 1) || ($j('#DESC_CLIENTE').val() != undefined && $j('#DESC_CLIENTE').val().toUpperCase() == 'HERMES') || (campanha_hermes != undefined && campanha_hermes == 38)) {
		$j('#codigo_catalogo').show();
	} else {
		$j('#codigo_catalogo').hide();
	}
});

$j('._calendar').datepicker({minDate: new Date(),dateFormat: 'dd/mm/yy',showOn: 'button', buttonImage: '<?= base_url().THEME ?>img/calendario.png', buttonImageOnly: true});

function valida_data(){

	if( $j("#DT_INICIO_CAMPANHA").val() != '' && $j("#DT_FIM_CAMPANHA").val() ) {
		data_ini = $j("#DT_INICIO_CAMPANHA").val().substr(6,4) + $j("#DT_INICIO_CAMPANHA").val().substr(3,2) + $j("#DT_INICIO_CAMPANHA").val().substr(0,2);
		data_fim = $j("#DT_FIM_CAMPANHA").val().substr(6,4) + $j("#DT_FIM_CAMPANHA").val().substr(3,2) + $j("#DT_FIM_CAMPANHA").val().substr(0,2);
		if( data_ini > data_fim ) {
			alert("A DATA INICIO deve ser menor que a DATA FIM!");
			return false;
		}
	}
	return true;
}
</script>
<?php $this->load->view("ROOT/layout/footer") ?>
