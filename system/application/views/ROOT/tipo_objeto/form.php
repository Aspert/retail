<?php $this->load->view("ROOT/layout/header"); ?>

<div id="contentGlobal">
  <?php echo form_open('tipo_objeto/save/'.$this->uri->segment(3));?>
  <h1>Cadastro de Tipos de Objeto</h1>
  
  <?php if ( !empty($erros) ) : ?>
	<ul style="color:#f00;font-weight:bold;" class="box_erros">
		<?php foreach ( $erros as $e ) : ?>
			<li><?=$e?></li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>
  
  <table width="100%" class="Alignleft">
    <tr>
    	<td width="16%">Cliente *</td>
    	<td width="84%"><?php if($this->uri->segment(3) == ''): ?>
    		<select name="ID_CLIENTE" id="ID_CLIENTE">
    			<option value=""></option>
    			<?php echo montaOptions($clientes, 'ID_CLIENTE','DESC_CLIENTE', post('ID_CLIENTE',true)); ?>
    			</select>
    		<?php else: ?>
    		<?php post('DESC_CLIENTE'); ?>
    		<input name="DESC_CLIENTE" id="DESC_CLIENTE" type="hidden" value="<?php post('DESC_CLIENTE'); ?>" />
    		<input name="ID_CLIENTE" id="ID_CLIENTE" type="hidden" value="<?php post('ID_CLIENTE'); ?>" />
    		<br />
    		<?php endif; ?></td>
    	</tr>
    <tr>
      <td>Nome *</td>
      <td><input name="DESC_TIPO_OBJETO" type="text" value="<?php post('DESC_TIPO_OBJETO'); ?>" /></td>
    </tr>
    <tr>
      <td>Categoria obrigat&oacute;ria?</td>
      <td><input name="EXIGE_CATEGORIA" type="radio" value="1" <?php echo post('EXIGE_CATEGORIA',true)==1 ? 'checked' : ''; ?> />
        Sim
        <input name="EXIGE_CATEGORIA" type="radio" value="0" <?php echo post('EXIGE_CATEGORIA',true)=='0' ? 'checked' : ''; ?> />
        N&atilde;o        </td>
    </tr>
    <tr>
      <td>Descri&ccedil;&atilde;o para Help</td>
      <td><input name="DESCRICAO" type="text" id="DESCRICAO" value="<?php post('DESCRICAO'); ?>" size="45" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><a href="<?php echo site_url('tipo_objeto/lista'); ?>" class="button"><span>Cancelar</span></a> <a href="javascript:;" onclick="$j(this).closest('form').submit()" class="button"><span>Salvar</span></a></td>
    </tr>
  </table>
  <?php echo form_close();?>
</div>
<script type="text/javascript">
$('ID_AGENCIA').addEvent('change', function(){
	montaOptionsAjax($('ID_CLIENTE'),'<?php echo site_url('json/admin/getClientesByAgencia'); ?>','id=' + this.value,'ID_CLIENTE','DESC_CLIENTE');
});

</script>
<?php $this->load->view("ROOT/layout/footer") ?>
