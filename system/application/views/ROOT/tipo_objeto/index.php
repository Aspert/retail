<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
  <h1>Tipos de Objeto</h1>
  <form method="post" action="<?php echo site_url('tipo_objeto/lista'); ?>" id="pesquisa_simples">
    <table width="100%" class="Alignleft">
      <tr>
        <td width="8%" rowspan="2" align="center"><?php if($podeAlterar): ?>
        	<a href="<?php echo site_url('tipo_objeto/form'); ?>"><img src="<?php echo base_url().THEME; ?>img/file_add.png" alt="Adicionar" width="31" height="31" border="0" /></a>
        	<?php endif; ?></td>
        <td width="22%">Cliente</td>
        <td width="24%">Tipo</td>
        <td width="32%">Itens / p&aacute;gina</td>
        <td width="14%" rowspan="2"><a class="button" href="javascript:" title="Pesquisar" onclick="$j(this).closest('form').submit()"><span>Pesquisar</span></a></td>
      </tr>
      <tr>
        <td><select name="ID_CLIENTE" id="ID_CLIENTE">
        	<option value=""></option>
        	<?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', empty($busca['ID_CLIENTE']) ? '' : $busca['ID_CLIENTE']); ?>
        	</select></td>
        <td><input type="text" name="NOME_CAMPO" id="NOME_CAMPO" value="<?php echo empty($busca['NOME_CAMPO']) ? '' : $busca['NOME_CAMPO']?>" /></td>
        <td><span class="campo esq">
          <select name="pagina" id="pagina">
            <?php
		$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
		for($i=5; $i<=50; $i+=5){
			printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
		}
		?>
          </select>
          </span></td>
      </tr>
    </table>
  </form>
  <?php if( !empty($lista)) :?>
  <br />
  <div id="tableObjeto">
    <table cellspacing="1" cellpadding="2" border="0" width="100%" class="tableSelection">
      <thead>
        <tr>
          <?php if($podeAlterar): ?>
          <td width="5%">Editar</td>
          <?php endif; ?>
          <td width="53%">Cliente</td>
          <td width="42%">Tipo</td>
        </tr>
      </thead>
      <?php foreach($lista as $item): ?>
      <tr>
        <?php if($podeAlterar): ?>
        <td align="center"><a href="<?php echo site_url('tipo_objeto/form/'.$item['ID_TIPO_OBJETO']); ?>"><img src="<?php echo base_url().THEME; ?>img/file_edit.png" alt="Adicionar" width="31" height="31" border="0" /></a></td>
        <?php endif; ?>
        <td align="center"><?php echo $item['DESC_CLIENTE']; ?></td>
        <td align="center"><?php echo $item['DESC_TIPO_OBJETO']; ?></td>
      </tr>
      <?php endforeach; ?>
    </table>
  </div>
  <span class="paginacao"><?php echo $paginacao; ?></span>
  <?php else: ?>
  Nenhum resultado
  <?php endif; ?>
</div>
<script type="text/javascript">
$('ID_AGENCIA').addEvent('change', function(){
	montaOptionsAjax($('ID_CLIENTE'),'<?php echo site_url('json/admin/getClientesByAgencia'); ?>','id=' + this.value,'ID_CLIENTE','DESC_CLIENTE');
});

</script>
<?php $this->load->view("ROOT/layout/footer") ?>
