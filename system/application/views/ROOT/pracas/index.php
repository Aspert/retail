<?php $this->load->view("ROOT/layout/header") ?>
<style type="text/css">
<!--
-->
</style>

<div id="contentGlobal">
  <h1>Praças</h1>
  <div id="pesquisa_simples">
    <form action="<?php echo site_url('pracas/lista'); ?>" method="post" id="formPesquisa">
      <table width="100%" border="0" cellspacing="1" cellpadding="2">
        <tr>
          <td width="6%" rowspan="2" align="center">
            <?php if($podeAlterar): ?>
          	<a href="<?php echo site_url('pracas/form/');?>"><img src="<?php echo base_url().THEME; ?>img/file_add.png" alt="Adicionar" width="31" height="31" border="0" /></a>
            <?php endif; ?>
          </td>
		  <td width="19%">Cliente</td>
		  <td width="19%">Regiao</td>
		  <td width="30%">Nome da praça</td>
          <td width="15%">Itens por página</td>
          <td width="12%" rowspan="2"><a onclick="$j(this).closest('form').submit()" href="javascript:" class="button"><span>Pesquisar</span></a>&nbsp;</td>
        </tr>
        <tr>
			<td>
			<select name="ID_CLIENTE" id="ID_CLIENTE">
					<option value=""></option>
					<?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', empty($busca['ID_CLIENTE']) ? '' : $busca['ID_CLIENTE']); ?>
				</select>
			</td>
			<td>
				<select name="ID_REGIAO" id="ID_REGIAO">
					<option value=""></option>
					<?php echo montaOptions($regioes,'ID_REGIAO','NOME_REGIAO', empty($busca['ID_REGIAO']) ? '' : $busca['ID_REGIAO']); ?>
				</select>
				</td>
			<td><input type="text" name="DESC_PRACA" id="DESC_PRACA" value="<?php echo val($busca,'DESC_PRACA'); ?>" /></td>
          <td><select name="pagina" id="pagina">
              <?php
        $pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
        for($i=5; $i<=50; $i+=5){
            printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
        }
        ?>
            </select></td>
        </tr>
      </table>
    </form>
  </div>
  <?php if(!empty($lista)): ?>
  <div id="tableObjeto">
    <table width="100%" border="0" cellspacing="1" cellpadding="2" class="tableSelection">
      <thead>
        <tr>
          <?php if($podeAlterar): ?>
          <td width="6%" align="center">Editar</td>
          <?php endif; ?>
          <td width="20%" align="center">Cliente</td>
          <td width="22%" align="center">Regiao</td>
		  <td width="27%" align="center">Nome da praça</td>
          <td width="25%" align="center">Status</td>
        </tr>
      </thead>
      <?php foreach($lista as $item): ?>
      <tr>
        <?php if($podeAlterar): ?>
        <td align="center"><a href="<?php echo site_url('pracas/form/'.$item['ID_PRACA']); ?>"><img src="<?php echo base_url().THEME; ?>img/file_edit.png" alt="Editar" width="31" height="31" border="0" /></a></td>
        <?php endif; ?>
        <td align="center"><?php echo $item['DESC_CLIENTE']; ?></td>
        <td align="center"><?php echo $item['NOME_REGIAO']; ?></td>
		 <td align="center"><?php echo $item['DESC_PRACA']; ?></td>
        <td align="center"><?php echo $item['STATUS_PRACA'] == 1 ? 'Ativo' : 'Inativo';?></td>
      </tr>
      <?php endforeach; ?>
   </table>
  </div>
   <table width="100%" style="clear: both">
      <tr>
        <td colspan="2" align="center">
          <div class="paginacao"><?php echo $paginacao; ?></div>
        </td>
        <td width="13%" align="center"><strong>Total de Itens</strong>: <?php echo $total; ?></td>
      </tr>
    </table>
  
  <?php else: ?>
  Nenhum item encontrado
  <?php endif; ?>
</div>
<script type="text/javascript">
$j(function(){
	$j('#ID_CLIENTE').change(function(){
		montaOptionsAjax($('ID_REGIAO'), util.options.site_url + 'json/admin/getRegiaoByCliente','id=' + this.value,'ID_REGIAO','NOME_REGIAO');
	});
});
</script>
<?php $this->load->view("ROOT/layout/footer") ?>