<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
  <?php if ( isset($erros) && is_array($erros) ) : ?>
  <ul style="color:#ff0000;font-weight:bold;" class="box_erros">
    <?php foreach ( $erros as $e ) : ?>
    <li>
      <?=$e?>
    </li>
    <?php endforeach; ?>
  </ul>
  <?php endif; ?>
  <?php echo form_open('pracas/save/'.$this->uri->segment(3));?>
  <h1>Cadastro de Pra&ccedil;as</h1>
  <table width="100%" class="Alignleft">
    <tr>
      <td width="16%">Descri&ccedil;&atilde;o da pra&ccedil;a *</td>
      <td width="84%"><input name="DESC_PRACA" type="text" value="<?php post('DESC_PRACA'); ?>" /></td>
    </tr>
    <tr>
    	<td>Cliente *</td>
    	<td><select name="ID_CLIENTE" id="ID_CLIENTE">
    		<option value=""></option>
    		<?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', post('ID_CLIENTE',true)); ?>
    		</select></td>
    	</tr>
    <tr>
      <td>Regiao *</td>
      <td>

		<select name="ID_REGIAO" id="ID_REGIAO">
		<option value=""></option>
		<?php echo montaOptions($regioes,'ID_REGIAO','NOME_REGIAO', post('ID_REGIAO',true)); ?>
		</select>
	  </td>
    </tr>
    <tr>
      <td>Status</td>
      <td><select name="STATUS_PRACA" id="STATUS_PRACA">
      <option value="1"<?php echo post('STATUS_PRACA',true)==1 ? ' selected="selected"' : ''; ?>>Ativo</option>
      <option value="0"<?php echo post('STATUS_PRACA',true)=='0' ? ' selected="selected"' : ''; ?>>Inativo</option>
      </select></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><a href="<?php echo site_url('pracas/lista'); ?>" class="button"><span>Cancelar</span></a> <a href="javascript:;" onclick="$j(this).closest('form').submit()" class="button"><span>Salvar</span></a></td>
    </tr>
  </table>
  <?php echo form_close(); ?> </div>
  
<script type="text/javascript">
$j(function(){
	$('ID_CLIENTE').addEvent('change', function(){
		clearSelect($('ID_REGIAO'), 1);
		montaOptionsAjax($('ID_REGIAO'),'<?php echo site_url('json/admin/getRegiaoByCliente'); ?>','id=' + this.value,'ID_REGIAO','NOME_REGIAO');
	});
});
</script>
  
<?php $this->load->view("ROOT/layout/footer") ?>
