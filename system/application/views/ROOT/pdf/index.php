<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
  <h1>OPI</h1>
  <div id="pesquisa_simples"> <?php echo form_open('pdf/lista');?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="1%" rowspan="4" align="center"></td>
		
		<?php if(!empty($_sessao['ID_AGENCIA'])): ?>
        <td width="6%" style="text-align:left;">Ag&ecirc;ncia</td>
        <td width="13%" style="text-align:left;">Cliente</td>
		<?php elseif(!empty($_sessao['ID_CLIENTE'])): ?>
        <td width="8%" style="text-align:left;">Cliente</td>
        <td width="17%" style="text-align:left;">Ag&ecirc;ncia</td>
		<?php endif; ?>
		
        <td width="11%" style="text-align:left;">Bandeira</td>
        <td width="10%" style="text-align:left;">Campanha</td>
        <td width="11%" style="text-align:left;">Data</td>
        <td width="9%" style="text-align:left;">Itens por p&aacute;gina</td>
        <td width="14%" rowspan="2"><a id="btnLimpaPesquisa" class="button" href="javascript:"><span>Limpar</span></a> <a class="button" href="javascript:" onclick="$j(this).closest('form').submit();"><span>Pesquisar</span></a></td>
      </tr>
      <tr>
	  
		<?php if(!empty($_sessao['ID_AGENCIA'])): ?>
        <td width="6%" style="text-align:left;"><?php sessao_hidden('ID_AGENCIA','DESC_AGENCIA'); ?></td>
        <td width="13%" style="text-align:left;"><select name="ID_CLIENTE" id="ID_CLIENTE">
            <option value=""></option>
            <?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', !empty($busca['ID_CLIENTE']) ? $busca['ID_CLIENTE'] : ''); ?>
          </select>
		</td>
		<?php elseif(!empty($_sessao['ID_CLIENTE'])): ?>
        <td width="8%" style="text-align:left;"><?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?></td>
		<td width="17%" style="text-align:left;"><select name="ID_AGENCIA" id="ID_AGENCIA">
            <option value=""></option>
            <?php echo montaOptions($agencias,'ID_AGENCIA','DESC_AGENCIA', !empty($busca['ID_AGENCIA']) ? $busca['ID_AGENCIA'] : ''); ?>
          </select>
		  </td>
		<?php endif; ?>
		  
        <td width="11%" style="text-align:left;"><?php if(!empty($_sessao['ID_PRODUTO'])): ?>
          <?php sessao_hidden('ID_PRODUTO','DESC_PRODUTO'); ?>
          <?php else: ?>
          <select name="ID_PRODUTO" id="ID_PRODUTO">
            <option value=""></option>
            <?php echo montaOptions($produtos,'ID_PRODUTO','DESC_PRODUTO', !empty($busca['ID_PRODUTO']) ? $busca['ID_PRODUTO'] : ''); ?>
          </select>
          <?php endif; ?></td>
        <td width="10%" style="text-align:left;"><select name="ID_CAMPANHA" id="ID_CAMPANHA">
            <option value=""></option>
            <?php echo montaOptions($campanhas,'ID_CAMPANHA','DESC_CAMPANHA', !empty($busca['ID_CAMPANHA']) ? $busca['ID_CAMPANHA'] : ''); ?>
          </select></td>
        <td width="11%" style="text-align:left;"><input name="DT_CADASTRO_EXCEL" type="text" id="DT_CADASTRO_EXCEL"  class="_calendar" size="10" maxlength="10"/></td>
        <td width="9%" style="text-align:left;"><span class="campo esq">
          <select name="pagina" id="pagina">
            <?php
		$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
		for($i=5; $i<=50; $i+=5){
			printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
		}
		?>
          </select>
          </span></td>
      </tr>
    </table>
    <?php echo form_close();?> </div>
  <?php if( (!empty($lista))&& (is_array($lista))):?>
  <div id="tableObjeto">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableSelection tableMidia" id="tabelaBusca">
      <thead>
        <tr>
          <th align="left" class="codigoJob">N&uacute;mero Job</th>
		  <th align="left" class="tituloJob">Job</th>
          <th align="left" class="agenciaJob">Ag&ecirc;ncia</th>
          <th align="left" class="clienteJob">Cliente</th>
          <th align="left" class="bandeiraJob">Bandeira</th>
          <th align="left" class="campanhaJob">Campanha</th>
		  <th align="left" class="inicioJob">In&iacute;cio  da Validade</th>
		  <th align="left" class="terminoJob">Final da Validade</th>
		  <th align="left" class="mudancaJob">Mudan&ccedil;a Etapa</th>
          <th align="left">Data Cadastro</th>
          <?php if($podeUpload): ?>
          <th width="75" align="left">Gerar PDF/OPI</th>
          <?php endif; ?>
        </tr>
      </thead>
      <?php $cont=0; ?>
      <?php foreach($lista as $item):?>
      <tr>
        <td><?=$item['ID_JOB'];?></td>
		<td><?=$item['DESC_EXCEL'];?></td>
        <td><?=$item['DESC_AGENCIA'];?></td>
        <td><?=$item['DESC_CLIENTE'];?></td>
        <td><?=$item['DESC_PRODUTO'];?></td>
        <td><?=$item['DESC_CAMPANHA'];?></td>
		<td><?=format_date($item['DATA_INICIO'],'d/m/Y');?></td>
		<td><?=format_date($item['DATA_TERMINO'],'d/m/Y');?></td>
		<td><?=empty($item['DATA_MUDANCA_ETAPA']) ? '---' : date('d/m/Y H:i',strtotime($item['DATA_MUDANCA_ETAPA']));?></td>
        <td><?= date_format(date_create($item['DT_CADASTRO_EXCEL']), "d/m/Y H:i:s") ;?></td>
        <?php if($podeUpload): ?>
        <td align="center"><a href="<?php echo site_url('pdf/opi/'. $item['ID_EXCEL']); ?>"><img src="<?php echo base_url().THEME; ?>img/file_add.png" alt="Upload" border="0" /></a></td>
        <?php endif; ?>
      </tr>
      <?endforeach;?>
    </table>
  </div>
  <span class="paginacao">
  <?=(isset($paginacao))?$paginacao:null?>
  </span>
  <?else:?>
  N&Atilde;O  H&Aacute; RESULTADO PARA A PESQUISA
  <?endif;?>
  <script type="text/javascript">

$j(function(){
	$j('._calendar').datepicker({dateFormat: 'dd/mm/yy',showOn: 'button', buttonImage: '<?= base_url().THEME ?>img/calendario.png', buttonImageOnly: true});
	
	configuraPauta('<?php echo $busca['ORDER']; ?>', '<?php echo $busca['ORDER_DIRECTION']; ?>');
	
	$j("#btnLimpaPesquisa").click(function(){
		limpaForm('#pesquisa_simples');
		<?php if(!empty($_sessao['ID_CLIENTE'])): ?>
		$('ID_AGENCIA').value = '';
		<?php elseif(!empty($_sessao['ID_AGENCIA'])): ?>
		$('ID_CLIENTE').value = '';
		<?php endif; ?>
		clearSelect($('ID_PRODUTO'),1);
		clearSelect($('ID_CAMPANHA'),1);
	});
	
	<?php if(!empty($_sessao['ID_CLIENTE'])): ?>
	$('ID_AGENCIA').addEvent('change', function(){
		clearSelect($('ID_PRODUTO'),1);
		clearSelect($('ID_CAMPANHA'),1);
		montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByAgencia'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
	});
	<?php endif; ?>
	
	<?php if(!empty($_sessao['ID_AGENCIA'])): ?>
	$('ID_CLIENTE').addEvent('change', function(){
		clearSelect($('ID_CAMPANHA'),1);
		clearSelect($('ID_PRODUTO'),1);
		montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByCliente'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
	});
	<?php endif; ?>
	
	$('ID_PRODUTO').addEvent('change', function(){
		clearSelect($('ID_CAMPANHA'),1);
		montaOptionsAjax($('ID_CAMPANHA'),'<?php echo site_url('json/admin/getCampanhasByProduto'); ?>','id=' + this.value,'ID_CAMPANHA','DESC_CAMPANHA');
	});
});

$j(function(){
	$j('.ui-datepicker').hide();
});
</script>
</div>
<?php $this->load->view("ROOT/layout/footer") ?>
