<?php $this->load->view("ROOT/layout/header"); ?>

<style type="text/css">
.minhaClasse {
	background-color: #EFEFEF;
	border-spacing: 1px;
}
.minhaClasse td {
	padding: 6px;
	text-align: left;
	background-color:#FFF;
}
</style>

<div id="contentGlobal">
  <?php if ( isset($erros) && is_array($erros) ) : ?>
  <ul style="color:#ff0000;font-weight:bold;">
    <?php foreach ( $erros as $e ) : ?>
    <li>
      <?=$e?>
    </li>
    <?php endforeach; ?>
  </ul>
  <?php endif; ?>
  <h1>Upload de PDF para OPI</h1>
  <form id="formImportacao" action="<?php echo base_url().index_page().'/pdf/upload/'.sprintf('%d',post('ID_EXCEL',true)); ?>" method="post" enctype="multipart/form-data">
      <table width="100%" class="minhaClasse" cellspacing="1" border="0">
        <tr>
          <td width="12%" style="text-align:left"><strong>Ag&ecirc;ncia</strong></td>
          <td width="34%" style="text-align:left"><? post('DESC_AGENCIA') ?></td>
          <td width="54%"><strong>Arquivos j&aacute; processados</strong></td>
        </tr>
        <tr>
          <td style="text-align:left"><strong>Cliente</strong></td>
          <td style="text-align:left"><? post('DESC_CLIENTE') ?></td>
          <td rowspan="7"><table width="100%" border="0">
              <tr>
                <td width="250" scope="col">Arquivo</td>
                <td width="74" scope="col">DPI</td>
                <td width="108" scope="col">Data</td>
                <td width="138" align="center" scope="col">Status</td>
              </tr>
            </table>
            <div style="width:100%;height:200px; overflow:auto">
              <table width="100%" border="0" id="arquivos">
                <tbody>
                  <tr id="modelo">
                    <td width="248"><span class="nome">teste de pdf 1.pdf</span></td>
                    <td width="72"><span class="dpi">dpi</span></td>
                    <td width="111" align="center"><span class="data">15/09/2009</span></td>
                    <td width="139" align="center"><span class="ok"></span></td>
                  </tr>
                </tbody>
              </table>
          <span id="aguarde">Aguarde, verificando arquivos disponiveis...</span> </div></td>
        </tr>
        <tr>
          <td style="text-align:left"><strong>Bandeira</strong></td>
          <td style="text-align:left"><? post('DESC_PRODUTO') ?></td>
        </tr>
        <tr>
          <td style="text-align:left"><strong>Campanha</strong></td>
          <td style="text-align:left"><? post('DESC_CAMPANHA') ?></td>
        </tr>
        <tr>
          <td style="text-align:left"><strong>T&iacute;tulo</strong></td>
          <td style="text-align:left"><?php post('TITULO_JOB'); ?></td>
        </tr>
        <tr>
          <td style="text-align:left"><strong>Data da &uacute;ltima importa&ccedil;&atilde;o</strong></td>
          <td style="text-align:left"><?= date_format(date_create(post('DT_CADASTRO_EXCEL',true)), "d/m/Y H:i:s") ;?></td>
        </tr>
        <tr>
	      <td>Resolução (DPI)</td>
	      <td>
		    <select name="FILA_OPI_DPI">
				<?php echo montaOptions($dpiList, 'FILA_OPI_DPI','FILA_OPI_DPI', post('FILA_OPI_DPI',true)); ?>
		    </select>  
			DPI
		  </td>
	    </tr>
        <tr>
          <td style="text-align:left"><strong>Arquivo PDF</strong></td>
          <td style="text-align:left"><input type="file" name="file" id="file" />
            ( *.pdf )</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>
           <a class="button" href="<?php echo site_url('pdf/lista'); ?>"><span>Cancelar</span></a>
           <a class="button" href="javascript:" onclick="$j(this).closest('form').submit()"><span>Upload</span></a>
           </td>
           
        </tr>
      </table>
  </form>
  <script type="text/javascript">

var modeloLinha;

$j(function(){
	modeloLinha = $j('#modelo');
	modeloLinha.remove();
	verifica();
	setInterval(verifica,5000);
});


function verifica(){
	$j.get("<?=base_url().index_page()?>/json/pdf/verifica/<? post('ID_EXCEL') ?>",
		function(data){
			$j('#arquivos tbody').html('');
			var arquivos = eval(data);
			
			if(arquivos.length > 0){
				$j('#aguarde').text('');
			}else{
				$j('#aguarde').text('Nenhum arquivo disponível.');
			}
			
			for(var i = 0; i < arquivos.length; i++){
				var arq = arquivos[i]; 
				
				var linha = modeloLinha.clone();
				
				//se for permitido download, cria o link
				if(<?= sprintf('%d',$podeDownload); ?> && arq['PROCESSADO_EXCEL_OPI'] == 1){
					linha.find(".nome").text(arq['ARQUIVO_EXCEL_OPI']).wrap('<a class="button" href="<?= base_url().index_page()?>/pdf/download/'+arq['ID_EXCEL_OPI']+'" target="_blank" ></a>');
				}else{
					//senao apenas exibe o nome dos arquivos e o status
					linha.find(".nome").text(arq['ARQUIVO_EXCEL_OPI']);
				}
				linha.find(".dpi").text(arq['FILA_OPI_DPI'] + " dpi");
				linha.find(".data").text(arq['DATA2_EXCEL_OPI']);
				if(arq['PROCESSADO_EXCEL_OPI'] == 1){
					linha.find(".ok").html('Processado<br />'+arq['DIF']);
				}else{
				
					imgProcessando = $j('<img/>');
					imgProcessando.attr('src','<?= base_url()?>/img/AJAX_LoadingBar.gif');
					imgProcessando.attr('alt','processando...');
					//imgProcessando.attr('width',126);
					//imgProcessando.attr('height',22);
					imgProcessando.attr('align','absmiddle');
					
					linha.find(".ok").append(imgProcessando);
				}
				$j("#arquivos tbody").append(linha);
			}
		}
	);

}

</script>
  <?php $this->load->view("ROOT/layout/footer"); ?>
</div>
