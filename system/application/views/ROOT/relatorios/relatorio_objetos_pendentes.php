<?php $this->load->view('ROOT/layout/header.php');
if(isset($_SESSION['relatorioObjetoPendente']) && !empty($_SESSION['relatorioObjetoPendente']) && empty($_POST)){
	$_POST = $_SESSION['relatorioObjetoPendente'];
} else {
	$_SESSION['relatorioObjetoPendente'] =  $_POST; 
}
//print_rr($pracas);die;?>
<div id="contentGlobal">
  <form id="formRelatorioObjetosPendentes" method="post">
  	<?php if ( isset($erros) && is_array($erros) && !empty($erros)) : ?>
		<ul style="color:#ff0000;font-weight:bold;" class="box_erros">
			<?php foreach ( $erros as $e ) : ?>
				<li><?=$e?></li>
			<?php endforeach; ?>
		</ul>
	<?php endif; ?>
    <h1>Relat&oacute;rio de Objetos Pendentes</h1>
    <table width="100%" class="Alignleft" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td width="10%" scope="col" style="padding-top: 0px;text-align: right;" >Nome do arquivo</td>
        <td width="70%" scope="col" >
        	<input name="NOME" type="text" id="NOME"  value="<?= isset($_POST['NOME']) ? $_POST['NOME'] : '';?>" />
        	<br /> 
        	<a style="font-size: xx-small;">(Informe o nome do arquivo completo - Ex.: Arquivo.jpg)</a>
        </td>
        <td width="10%">&nbsp;</td>
      </tr>
      <tr>
        <td style="text-align: right;">N&uacute;mero do Job</td>
        <td >
        	<select style="width:100px;text-align: center;" name="ID_JOB" id="ID_JOB">
      		<?php
			if(!empty($jobs)){
				echo '<option value=""></option>';
	        	echo montaOptions($jobs,'ID_JOB','ID_JOB',isset($_POST['ID_JOB']) ? $_POST['ID_JOB'] : '');
			} else {
				echo '<option value=""></option>';
			}
			?>
      		</select>
      	</td>
      	<td>&nbsp;</td>
      </tr>
      <tr>
      	<td style="text-align: right;">Pra&ccedil;a</td>
      	<td>
      		<select style="width:160px;text-align: center;" name="ID_PRACA" id="ID_PRACA">
      		<?php
			if(!empty($pracas)){
				echo '<option value=""></option>';
	        	echo montaOptions($pracas,'ID_PRACA','DESC_PRACA', isset($_POST['ID_PRACA']) ? $_POST['ID_PRACA'] : '');
			} else {
				echo '<option value=""></option>';
			}
			?>
      		</select>
      		<input type="hidden" name='DESC_PRACA' id="DESC_PRACA" value="">
      	</td>
      	<td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>
        	<a href="javascript:" id="limpar" class="button"><span>Limpar Campos</span></a>
            <a href="javascript:" id="pesquisar" class="button"><span>Pesquisar</span></a>
            <a href="javascript:" id="gerar" class="button"><span>Gerar Excel</span></a>
            <a href="javascript:new Util().vazio()" onClick="location.href='<?= base_url().index_page() ?>/relatorios/index';" id="cancelar" class="button"><span>Cancelar Pesquisa</span></a>
        </td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </form>
  <?php if (isset($lista) && !empty($lista)) {?>
  <div id="resultadoPesquisa">
  	<table width="90%" class="tableSelection" border="0" cellspacing="0" cellpadding="0" align="center" style="margin: auto!important;">
	  	<tr style="background-color: #EDECEC;">
	  		<?php if ($podeEditarFicha): ?>
	  		<td style="width: 40px;text-align: left;">Editar</td>
	  		<?php endif; ?>
	  		<td style="width: 310px;text-align: left;"><?php ghDigitalReplace($_sessao, 'Nome da ficha'); ?></td>
	  		<td style="width: 100px;text-align: left;"><?php ghDigitalReplace($_sessao, 'Código de Barras'); ?></td>
	  		<td style="width: 160px;text-align: left;">Página</td>
	  		<td style="width: 160px;text-align: left;"><?php ghDigitalReplace($_sessao, 'Objetos'); ?></td>
	  		<td style="width: 200px;text-align: left;">Praça</td>
	  	</tr>
	  	<?php foreach ($lista as $list) {?>
	  	<tr>
	  		<?php if ($podeEditarFicha): ?>
			<td style="width: 40px;text-align: center;"><?php echo anchor('ficha/form/'.$list['ID_FICHA'], '<img src="'. base_url().THEME.'img/file_edit.png" />',array('title'=>'Editar Ficha'));?></td>
			<?php endif; ?>
	  		<td style="width: 310px;text-align: left;"><?=$list['NOME_FICHA'];?></td>
	  		<td style="width: 100px;text-align: left;"><?=$list['COD_BARRAS_FICHA'];?></td>
	  		<td style="width: 160px;text-align: left;"><?=$list['PAGINA_ITEM'];?></td>
	  		<td style="width: 160px;text-align: left;"><?=$list['FILENAME'];?></td>
	  		<td style="width: 200px;text-align: left;"><?=$list['DESC_PRACA'];?></td>
	  	</tr>
	  	<?php }?>
  	</table>
  </div>
  <?php } else if($this->uri->segment(3) == 'tela') {?>
  <div id="resultadoNaoEncontrado">
  	<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center" style="margin: auto!important;">
	  	<tr>
	  		<td style="text-align: left;"><?=$msg;?></td>
	  	</tr>
	  </table>
  </div>
  <?php }?>
</div>

<script type="text/javascript">

$j(function(){	
	$j('.jqzoom').fancybox();

	 <?php if($this->uri->segment(3) != 'tela') {?>
	 	$j("#ID_PRACA option[value!='']").remove();
	 <?php }?>
});			

$('ID_JOB').addEvent('change', function(){
	montaOptionsAjax($('ID_PRACA'),'<?php echo site_url('json/admin/getPracasByIdJob'); ?>','id=' + this.value,'ID_PRACA','DESC_PRACA');
});

$j("#pesquisar").click(function(){
	$j('#DESC_PRACA').val($j('select[name=ID_PRACA] option:selected').text());
	gerarRelatorio('tela');
});

$j("#gerar").click(function(){
	$j('#DESC_PRACA').val($j('select[name=ID_PRACA] option:selected').text());
	gerarRelatorio('excel');
});

$j("#limpar").click(function(){
	$j('#NOME').val('');
	$j('#ID_JOB').val('');
	$j("#ID_PRACA option[value!='']").remove();
	$j('#resultadoPesquisa').remove();
	$j('#resultadoNaoEncontrado').remove();
	$j('.fileID').remove();
});
function gerarRelatorio(acao){
	var url = '<?php echo site_url('relatorios/relatorio_objetos_pendentes/'); ?>/' + acao;
	$j('#formRelatorioObjetosPendentes').attr('action', url).submit();
}
</script>

<?php $this->load->view('ROOT/layout/footer.php'); ?>
