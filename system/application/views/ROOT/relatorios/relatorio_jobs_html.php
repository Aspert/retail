<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Relatório de Jobs</title>
<style type="text/css">
<!--
td, th, div {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 11px;
	border: 1px solid black;
	padding: 2px;
}
table { border-collapse: collapse;border-spacing: 0; }
th {
	color: #000;
	border-bottom-width: 2px;
	border-bottom-style: solid;
	border-bottom-color: #333;
}
-->
</style>
</head>

<body>

<table border="0" style="width:100%;border:1px solid black;">
	<tr style="background-color:black;color:white;text-align:center;">
		<td><strong>Relat&oacute;rio de Jobs</strong></td>
	</tr>
	<tr>
		<td>
			<?php if(!empty($lista)): ?>
			
				<table width="100%" border="<?php echo $this->uri->segment(4) == 'html' ? 0 : 1; ?>" cellspacing="0" cellpadding="2">
				  <tr>
				    <td colspan="2"><strong>Data Início</strong></td>
				    <td colspan="6" align="left"><?php echo $_POST['DATA_INICIO']; ?></td>
				  </tr>
				  <tr>
				    <td colspan="2"><strong>Data Término</strong></td>
				    <td colspan="6" align="left"><?php echo $_POST['DATA_TERMINO']; ?></td>
				  </tr>
				  <tr>
				    <td colspan="2"><strong>N&uacute;mero do Job</strong></td>
				    <td colspan="6" align="left"><?php echo implode(', ', $cabecalho['ID_JOB']); ?></td>
				  </tr>
				  <tr>
				    <td colspan="2"><strong>Título do Job</strong></td>
				    <td colspan="6" align="left"><?php echo implode(', ', array_unique($cabecalho['TITULO_JOB'])); ?></td>
				  </tr>
				  <tr>
				    <td colspan="2"><strong>Agência</strong></td>
				    <td colspan="6" align="left"><?php echo implode(', ', array_unique($cabecalho['DESC_AGENCIA'])); ?></td>
				  </tr>
				  <tr>
				    <td colspan="2"><strong>Cliente</strong></td>
				    <td colspan="6" align="left"><?php echo implode(', ', array_unique($cabecalho['DESC_CLIENTE'])); ?></td>
				  </tr>
				  <tr>
				    <td colspan="2"><strong>Bandeira</strong></td>
				    <td colspan="6" align="left"><?php echo implode(', ', array_unique($cabecalho['DESC_PRODUTO'])); ?></td>
				  </tr>
				  <tr>
				    <td colspan="2"><strong>Campanha</strong></td>
				    <td colspan="6" align="left"><?php echo implode(', ', array_unique($cabecalho['DESC_CAMPANHA'])); ?></td>
				  </tr>
				  <tr>
				    <td colspan="8">&nbsp;</td>
				  </tr>
				<?php
				
				foreach($lista as $grupo => $pracas):
				?>
				  <tr>
				    <td colspan="8" align="left" style="border-top: 2px solid #000;"><strong><?php echo $grupo; ?></strong></td>
				  </tr>
				  <?php foreach($pracas as $praca => $data): ?>
				  <!-- <tr>
				    <td colspan="7" align="left" style="border-bottom: 2px solid #000;"><strong>Praça:</strong> <?php //echo $praca ?></td>
				  </tr>
				  <tr> -->
				    <!-- <td width="4%" align="left" bgcolor="#E2E2E2"><strong>Seq</strong></td> -->
				    <td width="5%" align="left" bgcolor="#E2E2E2"><strong>Página</strong></td>
				    <td width="5%" align="left" bgcolor="#E2E2E2"><strong>Ordem</strong></td>
				    <td width="30%" align="left" bgcolor="#E2E2E2"><strong>Nome da Ficha</strong></td>
				    <td width="12%" align="left" bgcolor="#E2E2E2"><strong>Código de Barras</strong></td>
				    <!-- <td width="14%" align="left" bgcolor="#E2E2E2"><strong>Nome Produto</strong></td> -->
				    <td width="15%" align="left" bgcolor="#E2E2E2"><strong>Categoria</strong></td>
				    <td width="20%" align="left" bgcolor="#E2E2E2"><strong>Subcategoria</strong></td>
				    <td width="17%" align="left" bgcolor="#E2E2E2"><strong>Fabricante</strong></td>
				    <td width="18%" align="left" bgcolor="#E2E2E2"><strong>Marca</strong></td>
				  </tr>
				  <?php
				  $num = 1;
				  $cor = '#EFEFEF';
				  foreach($data['ITENS'] as $item):
				  	$cor = $cor == '#EFEFEF' ? '#FFFFFF' : '#EFEFEF';
				  	if(!$item['ID_FICHA_PAI'] || $item['ID_FICHA_PAI'] == 0):
				  ?>
					  <tr>
					    <!-- <td align="left" bgcolor="<?php //echo $cor; ?>"><?php //printf('%03d', $num++); ?></td> -->
					    <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['PAGINA_ITEM']; ?></td>
					    <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['ORDEM_ITEM']; ?></td>
					    <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['NOME_FICHA']; ?></td>
					    <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo '&nbsp;'.$item['COD_BARRAS_FICHA']; ?></td>
					    <!-- <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['NOME_FICHA']; ?></td> -->
					    <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['DESC_CATEGORIA']; ?></td>
					    <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['DESC_SUBCATEGORIA']; ?></td>
					    <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['DESC_FABRICANTE']; ?></td>
					    <td align="left" bgcolor="<?php echo $cor; ?>"><?php echo $item['DESC_MARCA']; ?></td>
					  </tr>
					 <?php endif; ?>
				  <?php endforeach; ?>
				  <tr>
				    <td colspan="8">&nbsp;</td>
				  </tr>
				  <?php endforeach; ?>
				  <tr>
				    <td colspan="8">&nbsp;</td>
				  </tr>
				<?php endforeach; ?>
				
				</table>
				
			<?php else: ?>
				<div>N&atilde;o h&aacute; resultado para a pesquisa.</div>
			<?php endif; ?>
			
		</td>
	</tr>
</table>

</body>
</html>