<?php $this->load->view('ROOT/layout/header.php'); ?>

<div id="contentGlobal">
  <h1>Relat&oacute;rios</h1>
  <div id="contentListaAdmin">
    <ul style="width:100%">
      <?php
    	foreach($relatorios as $item) {
			printf('<li style="width: 100%%"><a href="%s"> %s </a></li>'.PHP_EOL,
				site_url('relatorios/' . $item['CHAVE_FUNCTION']),
				$item['DESC_FUNCTION']
			);
		}
	?>
    </ul>
  </div>
</div>
<?php $this->load->view('ROOT/layout/footer.php'); ?>
