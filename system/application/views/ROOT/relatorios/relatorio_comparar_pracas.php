<?php $this->load->view("ROOT/layout/header") ?>

<div id="contentGlobal">
	<h1>Relatorio comparativo entre pra&ccedil;as</h1>
	<div id="pesquisa_simples"> <?php echo form_open('relatorios/comparar_pracas/0');?>
		<table width="100%" border="0" cellspacing="1" cellpadding="2" style="margin: 0 0 0 20px;">
			<tr>
				<?php if(!empty($_sessao['ID_AGENCIA']) && empty($_sessao['ID_CLIENTE'])): ?>
					<td width="15%" style="text-align:left">Ag&ecirc;ncia</td>
					<td style="text-align:left">Cliente</td>
				<?php elseif(empty($_sessao['ID_AGENCIA']) && !empty($_sessao['ID_CLIENTE'])): ?>
					<td width="15%" style="text-align:left">Cliente</td>
					<td style="text-align:left">Ag&ecirc;ncia</td>
				<?php endif; ?>
				<td style="text-align:left">Bandeira</td>
				<td style="text-align:left">Campanha</td>
				<td style="text-align:left">T&iacute;tulo do Job</td>
				<td style="text-align:left">Itens por p&aacute;gina</td>
				<td width="10%" rowspan="2"  style="text-align:center"><a class="button" href="javascript:" onclick="$j(this).closest('form').submit();"><span>Pesquisa</span></a></td>
			</tr>
			<tr>
				<?php if(!empty($_sessao['ID_AGENCIA']) && empty($_sessao['ID_CLIENTE'])): ?>
					<td style="text-align:left"><?php sessao_hidden('ID_AGENCIA','DESC_AGENCIA'); ?></td>
					<td style="text-align:left"><select name="ID_CLIENTE" id="ID_CLIENTE">
						<option value=""></option>
						<?php echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE', !empty($busca['ID_CLIENTE']) ? $busca['ID_CLIENTE'] : ''); ?>
					</select></td>
				<?php elseif(empty($_sessao['ID_AGENCIA']) && !empty($_sessao['ID_CLIENTE'])): ?>
					<td style="text-align:left"><?php sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?></td>
					<td style="text-align:left"><select name="ID_AGENCIA" id="ID_AGENCIA">
						<option value=""></option>
						<?php echo montaOptions($agencias,'ID_AGENCIA','DESC_AGENCIA', !empty($busca['ID_AGENCIA']) ? $busca['ID_AGENCIA'] : ''); ?>
					</select></td>
				<?php endif; ?>
				
				<td style="text-align:left"><?php if(!empty($_sessao['ID_PRODUTO'])): ?>
					<?php sessao_hidden('ID_PRODUTO','DESC_PRODUTO'); ?>
					<?php else: ?>
					<select name="ID_PRODUTO" id="ID_PRODUTO">
						<option value=""></option>
						<?php echo montaOptions($produtos,'ID_PRODUTO','DESC_PRODUTO', !empty($busca['ID_PRODUTO']) ? $busca['ID_PRODUTO'] : ''); ?>
					</select>
					<?php endif; ?></td>
				<td style="text-align:left"><select name="ID_CAMPANHA" id="ID_CAMPANHA">
						<option value=""></option>
						<?php echo montaOptions($campanhas,'ID_CAMPANHA','DESC_CAMPANHA', !empty($busca['ID_CAMPANHA']) ? $busca['ID_CAMPANHA'] : ''); ?>
					</select></td>
				<td style="text-align:left"><input type="text" name="TITULO_JOB" id="TITULO_JOB" value="<?php echo !empty($busca['TITULO_JOB']) ? $busca['TITULO_JOB'] : '' ?>" /></td>
				<td style="text-align:left"><select name="pagina" id="pagina">
						<?php
		$pagina_atual = empty($busca['pagina']) ? 0 : $busca['pagina'];
		for($i=5; $i<=50; $i+=5){
			printf('<option value="%d" %s> %d </option>'.PHP_EOL, $i, $pagina_atual == $i ? 'selected="selected"' : '', $i);
		}
		?>
					</select></td>
			</tr>
		</table>
		<?php echo form_close();?> </div>
	<?php if( (!empty($lista))&& (is_array($lista))):?>
	<div id="tableObjeto">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableSelection tableMidia">
			<thead>
				<tr>
					<th width="7%" align="left" class="numeroJob">N&ordm; Job</th>
					<th width="17%" class="tituloJob">T&iacute;tulo Job</th>
					<th width="14%" align="left" class="agenciaJob">Ag&ecirc;ncia</th>
					<th width="12%" align="left" class="clienteJob">Cliente</th>
					<th width="12%" align="left" class="bandeiraJob">Bandeira</th>
					<th width="14%" align="left" class="campanhaJob">Campanha</th>
					<th width="14%" class="inicioProcessoJob">In&iacute;cio do processo</th>
					<th width="4%">Tela</th>
					<th width="6%">Excel</th>
				</tr>
			</thead>
			<?php foreach($lista as $item):?>
			<tr>
				<td class="alter"><?=$item['ID_JOB'];?></td>
				<td class="alter"><?=$item['TITULO_JOB'];?></td>
				<td class="alter"><?=$item['DESC_AGENCIA'];?></td>
				<td class="alter"><?=$item['DESC_CLIENTE'];?></td>
				<td class="alter"><?=$item['DESC_PRODUTO'];?></td>
				<td class="alter"><?=$item['DESC_CAMPANHA'];?></td>
				<td class="alter">
					<?php
						try {
							echo format_date($item['DATA_INICIO_PROCESSO'],'d/m/Y');
						} catch(Exception $e) {
							echo '--';
						}
					?>
				</td>
              	<td align="center">
					<a href="<?php echo site_url('relatorios/comparar_pracas/0/gerar/tela/'.$item['ID_JOB']); ?>" target="_blank">
						<img src="<?php echo base_url().THEME; ?>img/visualizar.png" class="information" alt="Gerar Relat&oacute;rio em tela" />
					</a>
				</td>
              	<td align="center">
					<a href="<?php echo site_url('relatorios/comparar_pracas/0/gerar/excel/'.$item['ID_JOB']); ?>" target="_blank">
						<img src="<?php echo base_url().THEME; ?>img/file_download.png" class="information" alt="Gerar Relat&oacute;rio em Planilha Excel" />
					</a>
				</td>
			</tr>
			<?endforeach;?>
		</table>
	</div>
	<!-- Final de Table Selection -->
	<span class="paginacao">
	<?=(isset($paginacao))?$paginacao:null?>
	</span>
	<?else:?>
	Nenhum resultado
	<?endif;?>
	<script type="text/javascript">
		<?php if(!empty($_sessao['ID_CLIENTE'])): ?>
		$('ID_AGENCIA').addEvent('change', function(){
			clearSelect($('ID_CAMPANHA'), 1);
			clearSelect($('ID_PRODUTO'), 1);
			montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByAgencia'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
		});
		<?php endif; ?>
		
		<?php if(!empty($_sessao['ID_AGENCIA'])): ?>
		$('ID_CLIENTE').addEvent('change', function(){
			clearSelect($('ID_CAMPANHA'), 1);
			clearSelect($('ID_PRODUTO'),1);
			montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByCliente'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
		});
		<?php endif; ?>
		
		$('ID_PRODUTO').addEvent('change', function(){
			clearSelect($('ID_CAMPANHA'),1);
			montaOptionsAjax($('ID_CAMPANHA'),'<?php echo site_url('json/admin/getCampanhasByProduto'); ?>','id=' + this.value,'ID_CAMPANHA','DESC_CAMPANHA');
		});
		
		$j(function(){
			configuraPauta('<?php echo $field; ?>','<?php echo $direction; ?>');
		});
		
	</script>
	</div>
<!-- Final de Content Global -->
<?php $this->load->view("ROOT/layout/footer") ?>
