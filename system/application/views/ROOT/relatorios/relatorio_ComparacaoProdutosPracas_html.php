<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Relat&oacute;rio de Comparação de Produtos entre Praças</title>
<style type="text/css">
<!--
td, th, div {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 11px;
	border: 1px solid black;
	padding: 2px;
}
table { border-collapse: collapse;border-spacing: 0; }
th {
	color: #000;
	border-bottom-width: 2px;
	border-bottom-style: solid;
	border-bottom-color: #333;
}
-->
</style>
</head>

<body>
<?php if(isset($lista) && !empty($lista)): ?>
<div id="resultadoPesquisa" style="border: 1px solid #D3D3D3;width:100%;">
  	<table width="100%" class="tableSelection_relatorio" border="0" cellspacing="0" cellpadding="0" align="center" style="margin: auto!important;">
	  	<tr style="background-color:black;color:white;text-align:center;">
			<td colspan="2"><strong>Relat&oacute;rio de Comparação de Produtos entre Praças</strong></td>
		</tr>
		<tr>
	        <td colspan="2" style="border: 0">&nbsp;</td>
	    </tr>
	  	<tr class="tableSelection" style="background-color: #EDECEC;">
	  		<td colspan="2" style="text-align: center;font-weight: bold;">Job: <?=$dadosJob[0]['TITULO_JOB'];?></td>
	  	</tr>
	  	<tr class="tableSelection" style="background-color: #EDECEC;">
	  		<td colspan="2" style="text-align: center;font-weight: bold;">Praça: <?=$pracaSelecionada['DESC_PRACA'];?></td>
	  	</tr>
	  	<tr>
	        <td colspan="2" style="border: 0">&nbsp;</td>
	    </tr>
	    <tr>
	    <?php $count=1;
	    	foreach($lista as $key => $praca){ ?>
	    	
		    	<td>
			    	<table <?php echo count($lista) == 1 ? 'width="100%"' : 'width="95%"';?> class="tableSelection" border="0" cellspacing="0" cellpadding="0" <?php echo $count % 2 == 0 ? 'align="right"' : 'align="left"';?>>
			    		<tr style="background-color: #EDECEC;" idPraca='<?=$key;?>'>
					        <td style="text-align: left;font-weight: bold;"><?=$count;?></td>
					        <td colspan="3" style="text-align: center;font-weight: bold;">Praça: <?=$praca['T_DESC_PRACA'];?></td>
					    </tr>
					    <tr>
					    	<td style="text-align: center;font-weight: bold;">Página</td>
					    	<td style="text-align: center;font-weight: bold;">Qtdade de Produtos</td>
					    	<td style="text-align: center;font-weight: bold;">Produtos Repetidos</td>
					    	<td style="text-align: center;font-weight: bold;">Porcentagem</td>
					    </tr>
					    
					    <?php foreach($praca['PAGINAS'] as $pagina){ ?>
				    	<tr>
					    	<td style="text-align: center;"><?=$pagina['PAGINA_ITEM'];?></td>
					    	<td style="text-align: center;"><?=$pagina['QTO_TOTAL'];?></td>
					    	<td style="text-align: center;"><?=$pagina['REPETIDOS'];?></td>
					    	<td style="text-align: center;"><?=$pagina['PORCENTAGEM'];?>%</td>
					    </tr>
					    <?php }?>
					    <tr style="background-color: #EDECEC;">
						    	<td style="text-align: center;font-weight: bold;">Total</td>
						    	<td style="text-align: center;font-weight: bold;"><?=$praca['TOTAL_PRODUTOS'];?></td>
						    	<td style="text-align: center;font-weight: bold;"><?=$praca['TOTAL_REPETIDOS'];?></td>
						    	<td style="text-align: center;font-weight: bold;"><?=$praca['TOTAL_PORCENTAGEM'];?>%</td>
						    </tr>
			    	</table>
		    	</td>
		    	<?php if($count % 2 == 0){?>
		    	</tr>
		    	<tr>
		    		<td colspan="3">&nbsp;</td>
		    	</tr>
		    	<tr>
		    		<td colspan="3">&nbsp;</td>
		    	</tr>
		    	<tr>
		    	<?php }?>
	    <?php $count++; }?>
    	</tr>
  	</table>
  </div>
<?php else: ?>
	<div><?=$msg;?></div>
<?php endif; ?>
<script type="text/javascript">			
	$j(function(){	
		
	});	
</script>
</body>
</html>