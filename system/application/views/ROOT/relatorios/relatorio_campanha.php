<?php $this->load->view('ROOT/layout/header.php'); ?>

<div id="contentGlobal">
  <form id="formRelatorio" target="_blank" method="post">
  <?php if ( isset($erros) && is_array($erros) && !empty($erros)) : ?>
		<ul style="color:#ff0000;font-weight:bold;" class="box_erros">
			<?php foreach ( $erros as $e ) : ?>
				<li><?=$e?></li>
			<?php endforeach; ?>
		</ul>
	<?php endif; ?>
    <h1>Relat&oacute;rio por Campanha</h1>
    <table class="Alignleft">
    	<?php if(!empty($_sessao['ID_AGENCIA'])): ?>
    	<tr>
    		<td>Ag&ecirc;ncia</td>
    		<td><?php echo sessao_hidden('ID_AGENCIA','DESC_AGENCIA'); ?></td>
    		</tr>
    	<tr>
    		<td>Cliente</td>
    		<td><select name="ID_CLIENTE" id="ID_CLIENTE">
    			<?php
				echo '<option value=""></option>';
	        	echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE','');
			?>
    			</select></td>
    		</tr>
    	<?php elseif(!empty($_sessao['ID_CLIENTE'])): ?>
    	<tr>
    		<td>Cliente</td>
    		<td><?php echo sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?></td>
    		</tr>
    	<tr>
    		<td>Ag&ecirc;ncia*</td>
    		<td><select name="ID_AGENCIA" id="ID_AGENCIA">
    			<?php
				echo '<option value=""></option>';
	        	echo montaOptions($agencias,'ID_AGENCIA','DESC_AGENCIA','');
			?>
    			</select></td>
    		</tr>
    	<?php endif; ?>
    	<tr>
    		<td>Bandeira*</td>
    		<td><select name="ID_PRODUTO" id="ID_PRODUTO">
    			<?php
			echo '<option value=""></option>';
        	echo montaOptions($produtos,'ID_PRODUTO','DESC_PRODUTO','');
		?>
    			</select></td>
    		</tr>
    	<tr>
    		<td>Campanha*</td>
    		<td><select name="ID_CAMPANHA" id="ID_CAMPANHA">
    			<?php
		if(!empty($campanhas2)){
			echo '<option value=""></option>';
        	echo montaOptions($campanhas2,'ID_CAMPANHA','DESC_CAMPANHA','');
		} else {
			echo '<option value=""></option>';
		}
		?>
    			</select></td>
    		</tr>
    		<tr>
    		<td>Tipo de Peça</td>
    		<td><select name="ID_TIPO_PECA" id="ID_TIPO_PECA">
    		<?php
			echo '<option value=""></option>';
        	echo montaOptions($tipopeca,'ID_TIPO_PECA','DESC_TIPO_PECA','');
		?>
    		</select></td>
    		</tr>
    		<tr>
    		<td><input type="hidden" name="SAIDA" id="SAIDA" value="excel" /></td>
    		</tr>
    	<tr>
    		<td>&nbsp;</td>
    		<td><a href="<?php echo site_url('relatorios/index'); ?>" class="button"><span>Cancelar</span></a> <a href="javascript:" onclick="gerarRelatorio()" class="button"><span>Gerar Relat&oacute;rio</span></a></td>
    		</tr>
    	</table>
  </form>
</div>

<script type="text/javascript">
function gerarRelatorio(){
	var url = '<?php echo site_url('relatorios/relatorio_campanha/gerar'); ?>/' + $j('#SAIDA').val();
	$j('#formRelatorio').attr('action', url).submit();
}

$j(function(){
	<?php if(!empty($_sessao['ID_CLIENTE'])): ?>
	$('ID_AGENCIA').addEvent('change', function(){
		clearSelect($('ID_PRODUTO'),1);
		clearSelect($('ID_CAMPANHA'),1);
		montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByAgencia'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
	});
	<?php endif; ?>
	
	<?php if(!empty($_sessao['ID_AGENCIA'])): ?>
	$('ID_CLIENTE').addEvent('change', function(){
		clearSelect($('ID_CAMPANHA'),1);
		clearSelect($('ID_PRODUTO'),1);
		montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByCliente'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
	});
	<?php endif; ?>
	
	$('ID_PRODUTO').addEvent('change', function(){
		montaOptionsAjax($('ID_CAMPANHA'),'<?php echo site_url('json/admin/getCampanhasByProduto'); ?>','id=' + this.value,'ID_CAMPANHA','DESC_CAMPANHA');
	});
	
	atribuiCalendarios('._calendar');

});
</script>

<?php $this->load->view('ROOT/layout/footer.php'); ?>
