<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Relat&oacute;rio de Objetos Pendentes</title>
<style type="text/css">
<!--
td, th, div {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 11px;
	border: 1px solid black;
	padding: 2px;
}
table { border-collapse: collapse;border-spacing: 0; }
th {
	color: #000;
	border-bottom-width: 2px;
	border-bottom-style: solid;
	border-bottom-color: #333;
}
-->
</style>
</head>

<body>
<table border="0" style="width:100%;border:1px solid black;">
	<tr style="background-color:black;color:white;text-align:center;">
		<td><strong>Relat&oacute;rio de Objetos Pendentes</strong></td>
	</tr>
	<tr>
		<td>
		<table width="100%" border="<?php echo $this->uri->segment(3) == 'excel' ? 1 : 0; ?>" cellspacing="0" cellpadding="2">
			<tr>
			    <td width="16%"><strong>Nome do arquivo</strong></td>
			    <td colspan="3" align="left"><?php echo $_POST['NOME']; ?></td>
			</tr>
			<tr>
			    <td width="16%"><strong>Número do Job</strong></td>
			    <td colspan="3" align="left"><?php echo $_POST['ID_JOB']; ?></td>
			</tr>
			<tr>
			    <td width="16%"><strong>Praça</strong></td>
			    <?php if(isset($_POST['DESC_PRACA']) && !empty($_POST['DESC_PRACA'])){?>
			    <td colspan="3" align="left"><?php echo $_POST['DESC_PRACA']; ?></td>
			    <?php }else { ?>
			    <td colspan="3" align="left">
	    			<?php foreach ($pracas as $key => $praca){?>
	    					<?php if($key != 0){?>
	    						<?php echo ', '.$praca['DESC_PRACA']; ?> 
	    				<?php } else { ?>
		    					<?php echo $praca['DESC_PRACA']; ?>
						<?php }
	     				}?>
			    </td>
	     		<?php }?>
			</tr>
		</table>
			<br><br><br><br>
		<?php if(!empty($lista)): ?>
			<div id="resultadoPesquisa">
			  	<table width="100%" class="tableSelection" border="0" cellspacing="0" cellpadding="0" align="center" style="margin: auto!important;">
				  	<tr style="background-color: #EDECEC;">
				  		<td style="width: 310px;text-align: left;"><?php ghDigitalReplace($_sessao, 'Nome da ficha'); ?></td>
				  		<td style="width: 100px;text-align: left;"><?php ghDigitalReplace($_sessao, 'Código de Barras'); ?></td>
				  		<td style="width: 160px;text-align: left;">Página</td>
				  		<td style="width: 200px;text-align: left;"><?php ghDigitalReplace($_sessao, 'Objetos'); ?></td>
				  		<td style="width: 70px;text-align: left;">Praça</td>
				  		<td style="width: 70px;text-align: left;">Recebimento</td>
				  	</tr>
				  	<?php foreach ($lista as $list) {?>
				  	<tr>
				  		<td style="width: 310px;text-align: left;"><?=$list['NOME_FICHA'];?></td>
				  		<td style="width: 100px;text-align: left;"><?=$list['COD_BARRAS_FICHA'];?></td>
				  		<td style="width: 160px;text-align: left;"><?=$list['PAGINA_ITEM'];?></td>
				  		<td style="width: 200px;text-align: left;"><?=$list['FILENAME'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$list['DESC_PRACA'];?></td>
				  		<td style="width: 70px;text-align: left;">&nbsp;</td>
				  	</tr>
				  	<?php }?>
			  	</table>
		  	</div>
			<?php else: ?>
				<div><?=$msg;?></div>
			<?php endif; ?>
		</td>
	</tr>
</table>
</body>
</html>