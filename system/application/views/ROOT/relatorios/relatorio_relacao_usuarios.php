<?php $this->load->view('ROOT/layout/header.php'); ?>

<div id="contentGlobal">
<form id="formRelatorio" target="_blank" method="post">
<h1>Relatório de Relação de Usuários</h1>
<table width="100%" class="Alignleft">
	<tr>
		<td width="15%" scope="col">Data Inicial</td>
		<td width="85%" scope="col"><input name="DATA_INICIO" type="text"
			id="DATA_INICIO" size="12" maxlength="10" class="_calendar" /></td>
	</tr>
	<tr>
		<td>Data Final</td>
		<td><input name="DATA_TERMINO" type="text" id="DATA_TERMINO" size="12"
			maxlength="10" class="_calendar" /></td>
	</tr>
	<tr <?php if($usuario_cliente){ echo "style='display:none;'"; }?>>
		<td>Agência</td>
		<td>
			<select name="ID_AGENCIA" id="ID_AGENCIA">
			<?php
			echo '<option value=""></option>';
			echo montaOptions($agencias,'ID_AGENCIA','DESC_AGENCIA','');
			?>
			</select>
		</td>
	</tr>
	<tr <?php if(!$usuario_cliente){ echo "style='display:none;'"; }?>>
		<td>Cliente</td>
		<td><select name="ID_CLIENTE" id="ID_CLIENTE">
		<?php
		echo '<option value=""></option>';
		echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE','');
		?>
		</select></td>
	</tr>
	<tr>
		<td>Status</td>
		<td><select name="STATUS_USUARIO" id="STATUS_USUARIO">
			<option>&nbsp;</option>
			<option value="1">Ativo</option>
			<option value="0">Inativo</option>
		</select></td>
	</tr>
	<tr>
	<td>Saída do relatório</td>
	<td><select name="SAIDA" id="SAIDA">
		<option value="html">Na tela</option>
		<option value="excel">Planilha em Excel</option>
	</select></td>
	<tr>
		<td>&nbsp;</td>
		<td><a href="<?php echo site_url('relatorios/index'); ?>"
			class="button"><span>Cancelar</span></a> <a href="javascript:"
			onclick="gerarRelatorio()" class="button"><span>Gerar Relatório</span></a>
		</td>
	</tr>
</table>
</form>
</div>

<script type="text/javascript">
var usuario_cliente = <?php echo $usuario_cliente; ?>;

function gerarRelatorio(){

	if(usuario_cliente){
		if ($j('#ID_CLIENTE').val() == '') {
			alert("Selecione o Cliente");
		}
		else{
			$j('#ID_AGENCIA').val('');
			var url = '<?php echo site_url('relatorios/relatorio_relacao_usuarios/gerar'); ?>/' + $j('#SAIDA').val();
			$j('#formRelatorio').attr('action', url).submit();
		}
	}
	else{
		if ($j('#ID_AGENCIA').val() == '') {
			alert("Selecione a Agencia");
		}
		else{
			var url = '<?php echo site_url('relatorios/relatorio_relacao_usuarios/gerar'); ?>/' + $j('#SAIDA').val();
			$j('#formRelatorio').attr('action', url).submit();
		}
	}	

}
$j(function(){
	atribuiCalendarios('._calendar');
});
</script>
		<?php $this->load->view('ROOT/layout/footer.php'); ?>
