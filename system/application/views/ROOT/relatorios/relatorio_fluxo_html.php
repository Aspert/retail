<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Relatório de Fluxos de Job</title>
<style type="text/css">
<!--
td, th, div {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 11px;
	border: 1px solid black;
	padding: 2px;
}
table { border-collapse: collapse;border-spacing: 0; }
th {
	color: #000;
	border-bottom-width: 2px;
	border-bottom-style: solid;
	border-bottom-color: #333;
}
-->
</style>
</head>

<body>
<table border="0" style="width:100%;border:1px solid black;">
	<tr style="background-color:black;color:white;text-align:center;">
		<td><strong>Relat&oacute;rio de Fluxo de Jobs</strong></td>
	</tr>
	<tr>
		<td>
		
			<?php if(!empty($lista)): ?>
				<table width="100%" border="<?php echo $this->uri->segment(4) == 'excel' ? 1 : 0; ?>" cellspacing="0" cellpadding="2">
				  <tr>
				    <td width="16%"><strong>Data Início</strong></td>
				    <td colspan="3" align="left"><?php echo $_POST['DATA_INICIO']; ?></td>
				  </tr>
				  <tr>
				    <td><strong>Data Término</strong></td>
				    <td colspan="3" align="left"><?php echo $_POST['DATA_TERMINO']; ?></td>
				  </tr>
				  <tr>
				    <td><strong>N&uacute;mero do Job</strong></td>
				    <td colspan="3" align="left"><?php echo implode(', ', $cabecalho['ID_JOB']); ?></td>
				  </tr>
				  <tr>
				    <td><strong>Título do Job</strong></td>
				    <td colspan="3" align="left"><?php echo implode(', ', $cabecalho['TITULO_JOB']); ?></td>
				  </tr>
				  <tr>
				    <td><strong>Agência</strong></td>
				    <td colspan="3" align="left"><?php echo implode(', ', $cabecalho['DESC_AGENCIA']); ?></td>
				  </tr>
				  <tr>
				    <td><strong>Cliente</strong></td>
				    <td colspan="3" align="left"><?php echo implode(', ', $cabecalho['DESC_CLIENTE']); ?></td>
				  </tr>
				  <tr>
				    <td><strong>Bandeira</strong></td>
				    <td colspan="3" align="left"><?php echo implode(', ', $cabecalho['DESC_PRODUTO']); ?></td>
				  </tr>
				  <tr>
				    <td><strong>Campanha</strong></td>
				    <td colspan="3" align="left"><?php echo implode(', ', $cabecalho['DESC_CAMPANHA']); ?></td>
				  </tr>
				  <tr>
				    <td colspan="4">&nbsp;</td>
				  </tr>
				  
				  <?php
				  $totalGeral = 0;
				  $total = 0;
				  foreach($lista as $grupo => $data):
				  ?>
				  <tr>
				    <td colspan="4" style="border-bottom:2px solid #000; border-top:2px solid #000;"><strong><?php echo $grupo; ?></strong></td>
				  </tr>
				  <tr align="left">
				    <td bgcolor="#E2E2E2"><strong>Autor</strong></td>
				    <td width="30%" bgcolor="#E2E2E2"><strong>Enviado para</strong></td>
				    <td width="22%" bgcolor="#E2E2E2"><strong>Data de envio</strong></td>
				    <td width="32%" bgcolor="#E2E2E2"><strong>Tempo</strong></td>
				  </tr>
				  <?php
				  $old = null;
				  $cor = '#EFEFEF';
				  
				  $total = 0;
				  foreach($data['ITENS'] as $item):
				  	$cor = $cor == '#EFEFEF' ? '#FFFFFF' : '#EFEFEF';
				  ?>
				  <tr align="left">
				    <td bgcolor="<?php echo $cor; ?>"><?php echo $item['NOME_USUARIO']; ?></td>
				    <td bgcolor="<?php echo $cor; ?>"><?php echo $item['ETAPA_ENVIADA']; ?></td>
				    <td bgcolor="<?php echo $cor; ?>"><?php echo !empty($old) ? format_date_to_form($item['DATA_HISTORICO']) : '--'; ?></td>
				    <td bgcolor="<?php echo $cor; ?>"><?php
					if(empty($old)){
						$diff = 0;
						echo '---';
					} else {
						$diff = strtotime($item['DATA_HISTORICO']) -  strtotime($old['DATA_HISTORICO']);
						echo segundosToHora($diff);
					}
					
					$totalGeral += $diff;
					$total += $diff;
				
				    ?></td>
				  </tr>
				  <?php
					  $old = $item;
				  endforeach;
				  ?>
				  <tr>
				    <td colspan="3" align="right" bgcolor="#E2E2E2"><strong>Total:</strong></td>
				    <td align="left" bgcolor="#E2E2E2"><?php echo segundosToHora($total); ?></td>
				  </tr>
				  <tr>
				    <td colspan="4">&nbsp;</td>
				  </tr>
				  <?php endforeach; ?>
				  
				</table>
			<?php else: ?>
				<div>N&atilde;o h&aacute; resultado para a pesquisa.</div>
			<?php endif; ?>
		</td>
	</tr>
</table>
</body>
</html>