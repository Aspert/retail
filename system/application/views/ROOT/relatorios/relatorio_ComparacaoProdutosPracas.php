<?php $this->load->view('ROOT/layout/header.php');
if(isset($_SESSION['RelatorioComparacaoProdutosPracas']) && !empty($_SESSION['RelatorioComparacaoProdutosPracas']) && empty($_POST)){
	$_POST = $_SESSION['RelatorioComparacaoProdutosPracas'];
} else {
	$_SESSION['RelatorioComparacaoProdutosPracas'] =  $_POST; 
}
//print_rr($pracas);die;?>
<div id="contentGlobal">
  <form id="formRelatorioComparacaoProdutosPracas" method="post">
  	<?php if ( isset($erros) && is_array($erros) && !empty($erros)) : ?>
		<ul style="color:#ff0000;font-weight:bold;" class="box_erros">
			<?php foreach ( $erros as $e ) : ?>
				<li><?=$e?></li>
			<?php endforeach; ?>
		</ul>
	<?php endif; ?>
    <h1>Relat&oacute;rio de Comparação de Produtos entre Praças</h1>
    <table width="100%" class="Alignleft" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td style="text-align: right;width: 10%;">N&uacute;mero do Job</td>
        <td >
        	<select style="width:100px;text-align: center;" name="ID_JOB" id="ID_JOB">
      		<?php
			if(!empty($jobs)){
				echo '<option value=""></option>';
	        	echo montaOptions($jobs,'ID_JOB','ID_JOB',isset($_POST['ID_JOB']) ? $_POST['ID_JOB'] : '');
			} else {
				echo '<option value=""></option>';
			}
			?>
      		</select>
      	</td>
      </tr>
      <tr>
      	<td style="text-align: right;">Pra&ccedil;a</td>
      	<td>
      		<select style="width:160px;text-align: center;" name="ID_PRACA" id="ID_PRACA">
      		<?php
			if(!empty($pracas)){
				echo '<option value=""></option>';
	        	echo montaOptions($pracas,'ID_PRACA','DESC_PRACA', isset($_POST['ID_PRACA']) ? $_POST['ID_PRACA'] : '');
			} else {
				echo '<option value=""></option>';
			}
			?>
      		</select>
      		<input type="hidden" name='DESC_PRACA' id="DESC_PRACA" value="">
      	</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>
        	<a href="javascript:" id="limpar" class="button"><span>Limpar Campos</span></a>
            <a href="javascript:" id="pesquisar" class="button"><span>Pesquisar</span></a>
            <a href="javascript:" id="imprimir" class="button"><span>Imprimir</span></a>
            <a href="javascript:new Util().vazio()" onClick="location.href='<?= base_url().index_page() ?>/relatorios/index';" id="cancelar" class="button"><span>Cancelar Pesquisa</span></a>
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </form>
  <div id="processando" style="text-align: left;font-weight: bold;color:black;">Processando...</div>
  <?php if (isset($lista) && !empty($lista)) {?>
  <div id="resultadoPesquisa" style="border: 1px solid #D3D3D3;width:90%;">
  	<table width="100%" class="tableSelection_relatorio" border="0" cellspacing="0" cellpadding="0" align="center" style="margin: auto!important;">
	  	<tr class="tableSelection" style="background-color: #EDECEC;">
	  		<td colspan="2" style="width: 100%;text-align: center;border-right:none!important;font-weight: bold;">Job: <?=$dadosJob[0]['TITULO_JOB'];?></td>
	  	</tr>
	  	<tr class="tableSelection" style="background-color: #EDECEC;">
	  		<td colspan="2" style="width: 100%;text-align: center;font-weight: bold;">Praça: <?=$pracaSelecionada['DESC_PRACA'];?></td>
	  	</tr>
	  	<tr>
	        <td colspan="2" style="border: 0">&nbsp;</td>
	    </tr>
	    <tr>
	    <?php $count=1;
	    	foreach($lista as $key => $praca){ ?>
	    	
		    	<td>
			    	<table <?php echo count($lista) == 1 ? 'width="100%"' : 'width="95%"';?> class="tableSelection" border="0" cellspacing="0" cellpadding="0" <?php echo $count % 2 == 0 ? 'align="right"' : 'align="left"';?>>
			    		<tr style="background-color: #EDECEC;" idPraca='<?=$key;?>'>
					        <td style="text-align: left;font-weight: bold;"><?=$count;?></td>
					        <td colspan="3" style="text-align: center;font-weight: bold;">Praça: <?=$praca['T_DESC_PRACA'];?></td>
					    </tr>
					    <tr>
					    	<td style="text-align: center;font-weight: bold;">Página</td>
					    	<td style="text-align: center;font-weight: bold;">Qtdade de Produtos</td>
					    	<td style="text-align: center;font-weight: bold;">Produtos Repetidos</td>
					    	<td style="text-align: center;font-weight: bold;">Porcentagem</td>
					    </tr>
					    
					    <?php foreach($praca['PAGINAS'] as $pagina){ ?>
				    	<tr>
					    	<td style="text-align: center;"><?=$pagina['PAGINA_ITEM'];?></td>
					    	<td style="text-align: center;"><?=$pagina['QTO_TOTAL'];?></td>
					    	<td style="text-align: center;"><?=$pagina['REPETIDOS'];?></td>
					    	<td style="text-align: center;"><?=$pagina['PORCENTAGEM'];?>%</td>
					    </tr>
					    <?php }?>
					    <tr style="background-color: #EDECEC;">
						    	<td style="text-align: center;font-weight: bold;">Total</td>
						    	<td style="text-align: center;font-weight: bold;"><?=$praca['TOTAL_PRODUTOS'];?></td>
						    	<td style="text-align: center;font-weight: bold;"><?=$praca['TOTAL_REPETIDOS'];?></td>
						    	<td style="text-align: center;font-weight: bold;"><?=$praca['TOTAL_PORCENTAGEM'];?>%</td>
						    </tr>
			    	</table>
		    	</td>
		    	<?php if($count % 2 == 0){?>
		    	</tr>
		    	<tr>
		    		<td colspan="4">&nbsp;</td>
		    	</tr>
		    	<tr>
		    		<td colspan="4">&nbsp;</td>
		    	</tr>
		    	<tr>
		    	<?php }?>
	    <?php $count++; }?>
    	</tr>
  	</table>
  </div>
  <?php }?>
</div>

<script type="text/javascript">

$j(function(){	
	$j('#processando').hide();
	 <?php if($this->uri->segment(3) != 'tela') {?>
	 	$j("#ID_PRACA option[value!='']").remove();
	 <?php }?>

	//aqui fazemos a pesquisa das praças caso tenha algum job selecionado assim que a página é carregada
	if($j('#ID_JOB option:selected').val() != ""){
		montaOptionsAjax($('ID_PRACA'),'<?php echo site_url('json/admin/getPracasByIdJob'); ?>','id=' + $j('#ID_JOB option:selected').val(),'ID_PRACA','DESC_PRACA');
	}
});			

$('ID_JOB').addEvent('change', function(){
	montaOptionsAjax($('ID_PRACA'),'<?php echo site_url('json/admin/getPracasByIdJob'); ?>','id=' + this.value,'ID_PRACA','DESC_PRACA');
});

$j("#pesquisar").click(function(){
	$j('#processando').show();
	$j('#resultadoPesquisa').remove();
	$j('#pesquisar').hide();
	$j('#imprimir').hide();
	$j('#DESC_PRACA').val($j('select[name=ID_PRACA] option:selected').text());
	$j('#formRelatorioComparacaoProdutosPracas').attr('target','');
	gerarRelatorio('tela');
});

$j("#imprimir").click(function(){
	$j('#DESC_PRACA').val($j('select[name=ID_PRACA] option:selected').text());
	$j('#formRelatorioComparacaoProdutosPracas').attr('target','_blank');
	gerarRelatorio('imprimir');
});

$j("#limpar").click(function(){
	$j('#ID_JOB').val('');
	$j("#ID_PRACA option[value!='']").remove();
	$j('#resultadoPesquisa').remove();
	$j('#processando').hide();
	$j('#pesquisar').show();
	$j('#imprimir').show();
});
function gerarRelatorio(acao){
	var url = '<?php echo site_url('relatorios/relatorio_ComparacaoProdutosPracas/'); ?>/' + acao;
	$j('#formRelatorioComparacaoProdutosPracas').attr('action', url).submit();
}
</script>

<?php $this->load->view('ROOT/layout/footer.php'); ?>
