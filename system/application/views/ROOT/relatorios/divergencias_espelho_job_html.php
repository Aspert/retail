<?php
function getNomes($lista, $chave){
	$list = array();
	
	foreach($lista as $job){
		foreach($job['pracas'] as $praca){
			foreach($praca['itens'] as $item){
				if( !in_array($item[ $chave ], $list) ){
					$list[] = $item[ $chave ];
				}
			}
		}
	}
	sort($list);
	return $list;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Relatorio de Divergências entre Espelho e Carrinho</title>
<style type="text/css">
<!--
td, th, div {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 11px;
	border: 1px solid black;
	padding: 2px;
}
table { border-collapse: collapse;border-spacing: 0; }
th {
	color: #000;
	border-bottom-width: 2px;
	border-bottom-style: solid;
	border-bottom-color: #333;
}
-->
</style>
</head>

<body>

<table border="0" style="width:100%;border:1px solid black;">
	<tr style="background-color:black;color:white;text-align:center;">
		<td><strong>Relat&oacute;rio de Diverg&ecirc;ncias entre Espelho e Carrinho</strong></td>
	</tr>
	<tr>
		<td>
		
		<?php if(!empty($lista)): ?>
			<br/>
			<table width="100%" border="0" cellpadding="2" cellspacing="0" style="border: 2px solid black;">
			  <tr>
			    <td width="10%" valign="top" scope="col"><strong>Data Início</strong></td>
			    <td width="90%" align="left" colspan="6"><?php post('DATA_INICIO'); ?></td>
			  </tr>
			  <tr>
			    <td valign="top" scope="col"><strong>Data Término</strong></td>
			    <td align="left" colspan="6"><?php post('DATA_TERMINO'); ?></td>
			  </tr>
			  <tr>
			    <td valign="top" scope="col"><strong>N&uacute;mero do Job</strong></td>
			    <td align="left" colspan="6"><?php echo implode(', ', getNomes($lista, 'ID_JOB')); ?></td>
			  </tr>
			  <tr>
			     <td valign="top" scope="col"><strong>Título do Job</strong></td>
			    <td align="left" colspan="6"><?php echo implode(', ', getNomes($lista, 'TITULO_JOB')); ?></td>
			  </tr>
			  <tr>
			    <td valign="top" scope="col"><strong>Agência</strong></td>
			    <td align="left" colspan="6"><?php echo implode(', ', getNomes($lista, 'DESC_AGENCIA')); ?></td>
			  </tr> 
			  <tr>
			    <td valign="top" scope="col"><strong>Cliente</strong></td>
			    <td align="left" colspan="6"><?php echo implode(', ', getNomes($lista, 'DESC_CLIENTE')); ?></td>
			  </tr> 
			  <tr>
			    <td valign="top" scope="col"><strong>Bandeira</strong></td>
			    <td align="left" colspan="6"><?php echo implode(', ', getNomes($lista, 'DESC_PRODUTO')); ?></td>
			  </tr>
			  <tr>
			    <td valign="top" scope="col"><strong>Campanha</strong></td>
			    <td align="left" colspan="6"><?php echo implode(', ', getNomes($lista, 'DESC_CAMPANHA')); ?></td>
			  </tr>
			  <tr>
			  	<td valign="top" scope="col"><strong>Praça</strong></td>
			  	<td align="left" colspan="6"><?php echo implode(', ', getNomes($lista, 'DESC_PRACA')); ?></td>
			  	</tr>
			</table>
			<br />
			<?php
			foreach($lista as $job):
				$totalMaisGeral = 0;
				$totalMenosGeral = 0;
			
				$totalMaisGeralLimite = 0;
				$totalMenosGeralLimite = 0;
			?>
			<table cellpadding="4" cellspacing="0" width="100%" style="border: 2px solid black;">
				<tr>
					<td colspan="11" bgcolor="#FFCC99"><strong><?php echo $job['DESC_AGENCIA'] . ' / ' . $job['DESC_CLIENTE'] . ' / ' . $job['DESC_PRODUTO'] . ' / ' . $job['DESC_CAMPANHA']; ?>&nbsp;</strong></td>
				</tr>
				<tr>
					<td colspan="11" bgcolor="#FFCC99"><strong><?php echo $job['ID_JOB']; ?> - <?php echo $job['TITULO_JOB']; ?>&nbsp;</strong></td>
				</tr>
				<?php
				foreach($job['pracas'] as $idpraca => $praca):
					$totalMais = 0;
					$totalMenos = 0;
			
					$totalMaisLimite = 0;
					$totalMenosLimite = 0;
				
				?>
				<tr>
					<td colspan="11" bgcolor="#EFEFEF"><strong><?php echo $praca['DESC_PRACA']; ?>&nbsp;</strong></td>
				</tr>
				<tr>
					<td width="6%" align="center"><strong>Página</strong></td>
					<td width="10%" align="center"><strong>Categoria</strong></td>
					<td width="18%" align="center"><strong>Subcategoria</strong></td>
					<td width="8%" align="center"><strong>Qtde. Esperada</strong></td>
					<td width="5%" align="center"><strong>Limite a mais</strong></td>
					<td width="5%" align="center"><strong>Limite a menos</strong></td>
					<td width="8%" align="center"><strong>Qtde. Informada</strong></td>
					<td width="10%" align="center"><strong>Valor excedido a mais</strong></td>
					<td width="10%" align="center"><strong>Diferença a mais</strong></td>
					<td width="10%" align="center"><strong>Valor excedido a menos</strong></td>
					<td width="10%" align="center"><strong>Diferença a menos</strong></td>
				</tr>
				<?php
					foreach($praca['itens'] as $item):
					
						//Diferenca a mais
						//Qtd do Carrinho - Qtd do Espelho
						$mais = max(0, $item['QTDE_INFORMADA'] - $item['QUANTIDADE']);
						
						//Diferenca a menos
						//Qtd do Espelho - Qtd do Carrinho
						$menos = max(0, $item['QUANTIDADE'] - $item['QTDE_INFORMADA']);
						
						//Total Diferenca a mais
						$totalMais += $mais;
						
						//Total Diferenca a menos
						$totalMenos += $menos;
						
						//Total Geral Diferenca a mais
						$totalMaisGeral += $mais;
						
						//Total Geral Diferenca a menos
						$totalMenosGeral += $menos;
						
						//Se diferenca a mais > 0
						if ( $mais > 0 ) {
							//Valor que excedeu o limite
							//(Diferenca - Qtd Limite a mais) 
							$maisLimite = max(0, $mais - $item['LIMITE_MAXIMO']);
						} else {
							$maisLimite = 0;
						}
						
						//Se diferenca a menos > 0
						if ( $menos > 0 ) {
							//Valor que excedeu o limite
							//(Diferenca - Qtd Limite a menos) 
							$menosLimite = max(0, $menos - $item['LIMITE_MINIMO']);
						} else {
							$menosLimite = 0;
						}
						
						$totalMaisLimite += $maisLimite;
						$totalMenosLimite += $menosLimite;
						
						$totalMaisGeralLimite += $maisLimite;
						$totalMenosGeralLimite += $menosLimite;
				?>
				<tr>
					<td align="center"><?php echo $item['PAGINA'] ?>&nbsp;</td>
					<td align="center"><?php echo $item['DESC_CATEGORIA'] ?>&nbsp;</td>
					<td align="center"><?php echo $item['DESC_SUBCATEGORIA'] ?>&nbsp;</td>
					<td align="center"><?php echo $item['QUANTIDADE'] ?>&nbsp;</td>
					<td align="center"><?php echo $item['LIMITE_MAXIMO'] ?>&nbsp;</td>
					<td align="center"><?php echo $item['LIMITE_MINIMO'] ?>&nbsp;</td>
					<td align="center"><?php echo $item['QTDE_INFORMADA'] ?>&nbsp;</td>
					<td align="center"><?php echo $maisLimite ?>&nbsp;</td>
					<td align="center"><?php echo $mais ?>&nbsp;</td>
					<td align="center"><?php echo $menosLimite ?>&nbsp;</td>
					<td align="center"><?php echo $menos ?>&nbsp;</td>
				</tr>
				<?php endforeach; ?>
				<tr>
					<td height="23" colspan="6">&nbsp;</td>
					<td align="center" bgcolor="#EFEFEF"><strong>Total</strong></td>
					<td align="center" bgcolor="#EFEFEF"><strong><?php echo $totalMaisLimite; ?>&nbsp;</strong></td>
					<td align="center" bgcolor="#EFEFEF"><strong><?php echo $totalMais; ?>&nbsp;</strong></td>
					<td align="center" bgcolor="#EFEFEF"><strong><?php echo $totalMenosLimite; ?>&nbsp;</strong></td>
					<td align="center" bgcolor="#EFEFEF"><strong><?php echo $totalMenos; ?>&nbsp;</strong></td>
				</tr>
				<?php
					endforeach;
				?>
				<tr>
					<td height="23" colspan="6">&nbsp;</td>
					<td align="center" bgcolor="#CCCCCC"><strong>Total Geral</strong></td>
					<td align="center" bgcolor="#CCCCCC"><strong><?php echo $totalMaisGeralLimite; ?>&nbsp;</strong></td>
					<td align="center" bgcolor="#CCCCCC"><strong><?php echo $totalMaisGeral; ?>&nbsp;</strong></td>
					<td align="center" bgcolor="#CCCCCC"><strong><?php echo $totalMenosGeralLimite; ?>&nbsp;</strong></td>
					<td align="center" bgcolor="#CCCCCC"><strong><?php echo $totalMenosGeral; ?>&nbsp;</strong></td>
				</tr>
			</table>
			<?php endforeach; ?>
			
			<?php else: ?>
				<div>N&atilde;o h&aacute; resultado para a pesquisa.</div>
			<?php endif; ?>
		</td>
	</tr>
</table>
</body>
</html>