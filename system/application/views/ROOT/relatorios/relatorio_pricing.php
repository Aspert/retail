<?php $this->load->view('ROOT/layout/header.php'); ?>

<div id="contentGlobal">
  <form id="formRelatorio" target="_blank" method="post" autocomplete='off'>
    <h1>Relat&oacute;rio de Jobs/Pricing</h1>
    <table width="100%" class="Alignleft">
      <tr>
        <td width="15%" scope="col">Data inicial</td>
        <td width="85%" scope="col"><input name="DATA_INICIO" type="text" id="DATA_INICIO" size="12" maxlength="10" class="_calendar" /></td>
      </tr>
      <tr>
        <td>Data final</td>
        <td><input name="DATA_TERMINO" type="text" id="DATA_TERMINO" size="12" maxlength="10" class="_calendar" /></td>
      </tr>
      <tr>
        <td>N&uacute;mero do Job</td>
        <td><input name="ID_JOB" type="text" id="ID_JOB" size="12" maxlength="10" onkeypress="return new Util().soNums(event);"/></td>
      </tr>
      <tr>
      	<td>T&iacute;tulo do Job</td>
      	<td><input name="TITULO_JOB" type="text" id="TITULO_JOB" size="30" /></td>
      </tr>
		<?php if(!empty($_sessao['ID_AGENCIA'])): ?>
    	<tr>
    		<td>Ag&ecirc;ncia</td>
    		<td><?php echo sessao_hidden('ID_AGENCIA','DESC_AGENCIA'); ?></td>
    		</tr>
    	<tr>
    		<td>Cliente</td>
    		<td><select name="ID_CLIENTE" id="ID_CLIENTE">
    			<?php
				echo '<option value=""></option>';
	        	echo montaOptions($clientes,'ID_CLIENTE','DESC_CLIENTE','');
			?>
    			</select></td>
    		</tr>
    	<?php elseif(!empty($_sessao['ID_CLIENTE'])): ?>
    	<tr>
    		<td>Cliente</td>
    		<td><?php echo sessao_hidden('ID_CLIENTE','DESC_CLIENTE'); ?></td>
    		</tr>
    	<tr>
    		<td>Ag&ecirc;ncia</td>
    		<td><select name="ID_AGENCIA" id="ID_AGENCIA">
    			<?php
				echo '<option value=""></option>';
	        	echo montaOptions($agencias,'ID_AGENCIA','DESC_AGENCIA','');
			?>
    			</select></td>
    		</tr>
    	<?php endif; ?>
      <tr>
        <td>Bandeira</td>
        <td><select name="ID_PRODUTO" id="ID_PRODUTO">
		<?php
		if(empty($sessao['ID_PRODUTO'])){
			echo '<option value=""></option>';
        	echo montaOptions($produtos,'ID_PRODUTO','DESC_PRODUTO','');
		} else {
			printf('<option value="%d">%s</option>',$sessao['ID_PRODUTO'],$sessao['DESC_PRODUTO']);
		}
		?>
        </select></td>
      </tr>
      <tr>
        <td>Campanha</td>
        <td><select name="ID_CAMPANHA" id="ID_CAMPANHA">
		<?php
		if(!empty($campanhas)){
			echo '<option value=""></option>';
        	echo montaOptions($campanhas,'ID_CAMPANHA','DESC_CAMPANHA','');
		} else {
			echo '<option value=""></option>';
		}
		?>
        </select></td>
      </tr>
      <tr>
        <td>Pra&ccedil;a</td>
        <td><select name="ID_PRACA" id="ID_PRACA">
          <?php
		if(!empty($pracas)){
			echo '<option value=""></option>';
        	echo montaOptions($pracas,'ID_PRACA','DESC_PRACA','');
		} else {
			echo '<option value=""></option>';
		}
		?>
        </select></td>
      </tr>
      <tr>
        <td>Saida do Relat&oacute;rio</td>
        <td><select name="SAIDA" id="SAIDA">
          <option value="html">Na tela</option>
          <option value="excel">Planilha em Excel</option>
          
        </select></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>
        	<a href="<?php echo site_url('relatorios/index'); ?>" class="button"><span>Cancelar</span></a>
            <a href="javascript:" onclick="gerarRelatorio()" class="button"><span>Gerar Relat&oacute;rio</span></a>
        </td>
      </tr>
    </table>
  </form>
</div>

<script type="text/javascript">

$j('._calendar').datepicker({dateFormat: 'dd/mm/yy',showOn: 'button', buttonImage: '<?= base_url().THEME ?>img/calendario.png', buttonImageOnly: true});

function gerarRelatorio(){
	var url = '<?php echo site_url('relatorios/relatorio_pricing/gerar'); ?>/' + $j('#SAIDA').val();
	$j('#formRelatorio').attr('action', url).submit();
}

$j(function(){
	<?php if(!empty($_sessao['ID_CLIENTE'])): ?>
	$('ID_AGENCIA').addEvent('change', function(){
		clearSelect($('ID_PRODUTO'),1);
		clearSelect($('ID_CAMPANHA'),1);
		montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByAgencia'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
	});
	<?php endif; ?>
	
	<?php if(!empty($_sessao['ID_AGENCIA'])): ?>
	$('ID_CLIENTE').addEvent('change', function(){
		clearSelect($('ID_CAMPANHA'),1);
		clearSelect($('ID_PRODUTO'),1);
		montaOptionsAjax($('ID_PRODUTO'),'<?php echo site_url('json/admin/getProdutosByCliente'); ?>','id=' + this.value,'ID_PRODUTO','DESC_PRODUTO');
	});
	<?php endif; ?>
	
	$('ID_PRODUTO').addEvent('change', function(){
		montaOptionsAjax($('ID_CAMPANHA'),'<?php echo site_url('json/admin/getCampanhasByProduto'); ?>','id=' + this.value,'ID_CAMPANHA','DESC_CAMPANHA');
	});
	
	atribuiCalendarios('._calendar');

});
</script>

<?php $this->load->view('ROOT/layout/footer.php'); ?>
