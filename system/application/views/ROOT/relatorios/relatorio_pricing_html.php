<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Relat&oacute;rio de Jobs/Pricing</title>
<style type="text/css">
<!--
td, th, div {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 11px;
	border: 1px solid black;
	padding: 2px;
}
table { border-collapse: collapse;border-spacing: 0; }
th {
	color: #000;
	border-bottom-width: 2px;
	border-bottom-style: solid;
	border-bottom-color: #333;
}
-->
</style>
</head>

<body>
<table border="0" style="width:100%;border:1px solid black;">
	<tr style="background-color:black;color:white;text-align:center;">
		<td><strong>Relat&oacute;rio de Jobs/Pricing</strong></td>
	</tr>
	<tr>
		<td>
		
			<?php if(!empty($lista)): ?>
				<table border="0" cellspacing="0" cellpadding="2" width="100%">
				  <tr>
				    <td scope="col" width="180"><strong>Data Início</strong></td>
				    <td colspan="16" scope="col"><?php post('DATA_INICIO'); ?></td>
				  </tr>
				  <tr>
				    <td scope="col"><strong>Data Término</strong></td>
				    <td colspan="16" scope="col"><?php post('DATA_TERMINO'); ?></td>
				  </tr>
				  <tr>
				    <td valign="top" scope="col"><strong>N&uacute;mero do Job</strong></td>
				    <td align="left" colspan="6"><?php echo implode(', ', $cabecalho['ID_JOB']); ?></td>
				  </tr>
				  <tr>
				    <td scope="col"><strong>Título do Job</strong></td>
				    <td colspan="16" scope="col"><?php echo implode(', ', array_unique($cabecalho['TITULO_JOB'])); ?></td>
				  </tr>
				  <tr>
				    <td scope="col"><strong>Agência</strong></td>
				    <td colspan="16" scope="col"><?php echo implode(', ', $cabecalho['DESC_AGENCIA']); ?></td>
				  </tr> 
				  <tr>
				    <td scope="col"><strong>Cliente</strong></td>
				    <td colspan="16" scope="col"><?php echo implode(', ', $cabecalho['DESC_CLIENTE']); ?></td>
				  </tr> 
				  <tr>
				    <td scope="col"><strong>Bandeira</strong></td>
				    <td colspan="16" scope="col"><?php echo implode(', ', $cabecalho['DESC_PRODUTO']); ?></td>
				  </tr>
				  <tr>
				    <td scope="col"><strong>Campanha</strong></td>
				    <td colspan="16" scope="col"><?php echo implode(', ', $cabecalho['DESC_CAMPANHA']); ?></td>
				  </tr>
				  <tr>
				  	<td scope="col"><strong>Praça</strong></td>
				  	<td colspan="16" scope="col"><?php echo implode(', ', $cabecalho['DESC_PRACA']); ?></td>
				  	</tr>
				</table>
				<table border="0" cellspacing="0" cellpadding="2">
				  <tr>
				    <td colspan="17" scope="col">&nbsp;</td>
				  </tr>
				<?php
				
				$agencia = '';
				$cliente = '';
				$bandeira = '';
				$campanha = '';
				$job = '';
				$id_job = '';
				$praca = '';
				
				$m_agencia 	= 	false;
				$m_cliente 	= 	false;
				$m_bandeira = 	false;
				$m_campanha = 	false;
				$m_job 		= 	false;
				$m_praca 	= 	false;
				
				$cor = '';
				
				$secao = '';
				
				$totalValor = array();
				$totalGeral = 0;
				$total = array();
				$i = 0;
				$count = count($lista);
				
				foreach($lista as $item):
					
					if($agencia != $item['DESC_AGENCIA']){
						$cor = '';
						$m_agencia = true;
						$cliente = '';
						$bandeira = '';
						$campanha = '';
						$praca = '';
						$job = '';
						$agencia = $item['DESC_AGENCIA'];
					}else{
						$m_agencia = false;
					}
					
					if($cliente != $item['DESC_CLIENTE']){
				        $m_cliente = true;
						$bandeira = '';
						$campanha = '';
						$praca = '';
						$job = '';
						$cliente = $item['DESC_CLIENTE'];
				    }else{
						$m_cliente = false;
					}
					
					if($bandeira != $item['DESC_PRODUTO']){
				        $m_bandeira = true;
						$campanha = '';
						$praca = '';
						$job = '';
						$bandeira = $item['DESC_PRODUTO'];
				    }else{
						$m_bandeira = false;
					}
					
					if($campanha != $item['DESC_CAMPANHA']){
				    	$m_campanha = true;
						$praca = '';
						$job = '';
						$campanha = $item['DESC_CAMPANHA'];
				    }else{
						$m_campanha = false;
					}
					
					if($praca != $item['DESC_PRACA']){
				    	$m_praca = true;
						$praca = $item['DESC_PRACA'];
								
						/*$campos = array('PRECO_VISTA','PRECO_DE','PRECO_POR','ECONOMIZE','PRECO_UNITARIO',
							  'TIPO_UNITARIO','PRECO_CAIXA','TIPO_CAIXA',
							  'ENTRADA','PARCELAS','PRESTACAO','TOTAL_PRAZO');*/
						
						$campos = array('PRECO_VISTA','PRECO_DE','PRECO_POR','ECONOMIZE','PRECO_UNITARIO','PRECO_CAIXA','TOTAL_PRAZO');
				
						foreach($campos as $campo){
							$totalValor['OLD_'.$campo] = empty($totalValor[$campo])?0:$totalValor[$campo];
							$totalValor[$campo] = empty($item[$campo])?0:$item[$campo]; 
						}
						$total[$item['ID_PRACA']] = 1;
				    }else{
						//$totalValor['PRECO_VISTA'] += $item['PRECO_VISTA'];
						foreach($campos as $campo){
							$totalValor[$campo] += $item[$campo]; 
						}
						$total[$item['ID_PRACA']]++;
						$m_praca = false;
					}
				
					if($job != $item['TITULO_JOB']){
				        $m_job = true;
						$job = $item['TITULO_JOB'];
						$id_job = $item['ID_JOB'];
				    }else{
						$m_job = false;
					}
				  
				  ?>
				
				  <? if($m_praca): ?>
					  <? if($i > 0): ?>
				          <tr>
				            <td align="right" bgcolor="#CCCCCC">&nbsp;</td>
				            <td align="right" bgcolor="#CCCCCC">&nbsp;</td>
				            <td align="right" bgcolor="#CCCCCC"><strong>Total:</strong></td>
				            <td align="right" bgcolor="#CCCCCC"><?php echo money($totalValor['OLD_PRECO_VISTA'],'R$ ') ?></td>
				            <td align="right" bgcolor="#CCCCCC"><?php echo money($totalValor['OLD_PRECO_DE'],'R$ ') ?></td>
				            <td align="right" bgcolor="#CCCCCC"><?php echo money($totalValor['OLD_PRECO_POR'],'R$ ') ?></td>
				            <td align="right" bgcolor="#CCCCCC"><?php echo money($totalValor['OLD_ECONOMIZE'],'R$ ') ?></td>
				            <td align="right" bgcolor="#CCCCCC"><?php echo money($totalValor['OLD_PRECO_UNITARIO'],'R$ ') ?></td>
				            <td align="right" bgcolor="#CCCCCC">&nbsp;</td>
				            <td align="right" bgcolor="#CCCCCC"><?php echo money($totalValor['OLD_PRECO_CAIXA'],'R$ ') ?></td>
				            <td align="right" bgcolor="#CCCCCC">&nbsp;</td>
				            <td align="right" bgcolor="#CCCCCC">&nbsp;</td>
				            <td align="right" bgcolor="#CCCCCC">&nbsp;</td>
				            <td align="right" bgcolor="#CCCCCC">&nbsp;</td>
				            <td align="right" bgcolor="#CCCCCC">&nbsp;</td>
				            <td align="right" bgcolor="#CCCCCC">&nbsp;</td>
				            <td align="right" bgcolor="#CCCCCC"><?php echo money($totalValor['OLD_TOTAL_PRAZO'],'R$ ') ?></td>
				          </tr>
				      <? endif; ?>
				  <? endif; ?>
				  
				  <? if($m_agencia || $m_cliente || $m_bandeira || $m_campanha): ?>
				  <tr>
				    <td height="10" colspan="17" align="left">
				    <h3><? echo $agencia,' / ' , $cliente,' / ', $bandeira,' / ', $campanha ?></h3>
				     <? if($m_job): ?>
				     <h4>Job: <? echo $id_job ?> - <? echo $job ?></h4>
				     <? endif; ?>
				    </td>
				  </tr>
				  <? endif; ?>
				  <? if($m_praca): ?>
				  
				  <tr>
				    <td height="10" colspan="17" align="left">
				    <h4><? echo $praca ?></h4>
				    </td>
				  </tr>
				    <tr>
				    <th scope="col">&nbsp;</th>
				    <th scope="col">&nbsp;</th>
				    <th scope="col">&nbsp;</th>
				    <th colspan="4" bgcolor="#efefef" scope="col">A Vista</th>
				    <th colspan="4" bgcolor="#dedede" scope="col">Atacarejo</th>
				    <th colspan="7" bgcolor="#ededed" scope="col">A Prazo</th>
				  </tr>
				  <tr>
					<th scope="col">Ficha</th>
				    <th scope="col">Código de Barras</th>
				    <th scope="col">Descritivo</th>
				    <th scope="col">A VISTA</th>
				    <th scope="col">DE</th>
				    <th scope="col">POR</th>
				    <th scope="col">ECONOMIZE</th>
				    <th scope="col">Preço Un.</th>
				    <th scope="col">Tipo Un.</th>
				    <th scope="col">Preço Cx.</th>
				    <th scope="col">Tipo Cx.</th>
				    <th scope="col">Entrada</th>
				    <th scope="col">Parcelas</th>
				    <th scope="col">Prestações</th>
				    <th scope="col">Juros AM</th>
				    <th scope="col">Juros AA</th>
				    <th scope="col">Total Prazo</th>
				  </tr>
				  <? $cor = '#FFFFFF'; ?>
				  <? endif; ?>
				
				<?php
					// incrementa o total geral
					$totalGeral++;
					// cor zebrada
					$cor = $cor == '#EFEFEF' ? '#FFFFFF' : '#EFEFEF';
				?>
					<tr style="background-color:#FFFFFF;text-align:left;">
						<td style="border-top:1px solid #676767;"><?php echo $item['TIPO']; ?></td>
						<td style="border-top:1px solid #676767;"><?php echo $item['COD_BARRAS_FICHA']; ?></td>
						<td style="border-top:1px solid #676767;"><?php echo $item['NOME_FICHA']; ?></td>
						<td style="border-top:1px solid #676767;"><?php echo money($item['PRECO_VISTA'],'R$ '); ?></td>
						<td style="border-top:1px solid #676767;"><?php echo money($item['PRECO_DE'],'R$ '); ?></td>
						<td style="border-top:1px solid #676767;"><?php echo money($item['PRECO_POR'],'R$ '); ?></td>
						<td style="border-top:1px solid #676767;"><?php echo money($item['ECONOMIZE'],'R$ '); ?></td>
						<td style="border-top:1px solid #676767;"><?php echo money($item['PRECO_UNITARIO'],'R$ '); ?></td>
						<td style="border-top:1px solid #676767;"><?php echo $item['TIPO_UNITARIO']; ?></td>
						<td style="border-top:1px solid #676767;"><?php echo money($item['PRECO_CAIXA'],'R$ '); ?></td>
						<td style="border-top:1px solid #676767;"><?php echo $item['TIPO_CAIXA']; ?></td>
						<td style="border-top:1px solid #676767;"><?php echo money($item['ENTRADA'],'R$ '); ?></td>
						<td style="border-top:1px solid #676767;"><?php echo $item['PARCELAS']; ?></td>
						<td style="border-top:1px solid #676767;"><?php echo money($item['PRESTACAO'],'R$ '); ?></td>
						<td style="border-top:1px solid #676767;"><?php echo $item['JUROS_AM']; ?>%</td>
						<td style="border-top:1px solid #676767;"><?php echo $item['JUROS_AA']; ?>%</td>
						<td style="border-top:1px solid #676767;"><?php echo money($item['TOTAL_PRAZO'],'R$ '); ?></td>
					</tr>
				
					<?php foreach($listaFilha as $filha): ?>
						<?php if(($item['ID_FICHA'] == $filha['ID_FICHA_PAI']) && ($item['ID_PRACA'] == $filha['ID_PRACA'])): ?>
							
							<tr style="background-color:#EFEFEF;text-align:left;">
								<td><?php echo $filha['TIPO']; ?></td>
								<td><?php echo $filha['COD_BARRAS_FICHA']; ?></td>
								<td><?php echo $filha['NOME_FICHA']; ?></td>
								<td><?php echo money($filha['PRECO_VISTA'],'R$ '); ?></td>
								<td><?php echo money($filha['PRECO_DE'],'R$ '); ?></td>
								<td><?php echo money($filha['PRECO_POR'],'R$ '); ?></td>
								<td><?php echo money($filha['ECONOMIZE'],'R$ '); ?></td>
								<td><?php echo money($filha['PRECO_UNITARIO'],'R$ '); ?></td>
								<td><?php echo $filha['TIPO_UNITARIO']; ?></td>
								<td><?php echo money($filha['PRECO_CAIXA'],'R$ '); ?></td>
								<td><?php echo $filha['TIPO_CAIXA']; ?></td>
								<td><?php echo money($filha['ENTRADA'],'R$ '); ?></td>
								<td><?php echo $filha['PARCELAS']; ?></td>
								<td><?php echo money($filha['PRESTACAO'],'R$ '); ?></td>
								<td><?php echo $filha['JUROS_AM']*100; ?>%</td>
								<td><?php echo $filha['JUROS_AA']*100; ?>%</td>
								<td><?php echo money($filha['TOTAL_PRAZO'],'R$ '); ?></td>
							</tr>
							
						<?php endif;?>
					<?php endforeach;?>
				  
				  <? if($i > 0 && $i == $count-1): ?>
				  <tr>
				    <td align="right" bgcolor="#CCCCCC">&nbsp;</td>
				    <td align="right" bgcolor="#CCCCCC">&nbsp;</td>
				    <td align="right" bgcolor="#CCCCCC"><strong>Total:</strong></td>
				    <td align="right" bgcolor="#CCCCCC"><?php echo money($totalValor['PRECO_VISTA'], 'R$ '); ?></td>
				    <td align="right" bgcolor="#CCCCCC"><?php echo money($totalValor['PRECO_DE'], 'R$ '); ?></td>
				    <td align="right" bgcolor="#CCCCCC"><?php echo money($totalValor['PRECO_POR'], 'R$ '); ?></td>
				    <td align="right" bgcolor="#CCCCCC"><?php echo money($totalValor['ECONOMIZE'], 'R$ '); ?></td>
				    <td align="right" bgcolor="#CCCCCC"><?php echo money($totalValor['PRECO_UNITARIO'], 'R$ '); ?></td>
				    <td align="right" bgcolor="#CCCCCC">&nbsp;</td>
				    <td align="right" bgcolor="#CCCCCC"><?php echo money($totalValor['PRECO_CAIXA'], 'R$ '); ?></td>
				    <td align="right" bgcolor="#CCCCCC">&nbsp;</td>
				    <td align="right" bgcolor="#CCCCCC">&nbsp;</td>
				    <td align="right" bgcolor="#CCCCCC">&nbsp;</td>
				    <td align="right" bgcolor="#CCCCCC">&nbsp;</td>
				    <td align="right" bgcolor="#CCCCCC">&nbsp;</td>
				    <td align="right" bgcolor="#CCCCCC">&nbsp;</td>
				    <td align="right" bgcolor="#CCCCCC"><?php echo money($totalValor['TOTAL_PRAZO'], 'R$ '); ?></td>
				  </tr>
				  <? endif; ?>
				  
				  
				<?php 
				$i++;
				$idOldPraca = $item['ID_PRACA'];
				endforeach;
				?>
				
				</table>
				<? else: ?>
					<div>N&atilde;o h&aacute; resultado para a pesquisa.</div>
				<? endif; ?>
		</td>
	</tr>
</table>

</body>
</html>