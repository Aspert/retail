<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Relat&oacute;rio de Objetos Pendentes</title>
<style type="text/css">
<!--
td, th, div {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 11px;
	border: 1px solid black;
	padding: 2px;
}
table { border-collapse: collapse;border-spacing: 0; }
th {
	color: #000;
	border-bottom-width: 2px;
	border-bottom-style: solid;
	border-bottom-color: #333;
}
-->
</style>
</head>

<body>
<table border="0" style="width:100%;border:1px solid black;">
	<tr style="background-color:black;color:white;text-align:center;">
		<td><strong>Relat&oacute;rio de Campanha</strong></td>
	</tr>
	<tr>
		<td>
		<table width="100%" border="<?php echo $this->uri->segment(3) == 'excel' ? 1 : 0; ?>" cellspacing="0" cellpadding="2">
		<?php if(!empty($dados))	{ ?>
			<tr>
			    <td width="16%"><strong>Campanha</strong></td>
			    <td colspan="3" align="left"><?php echo $dados[0]['DESC_CAMPANHA']; ?></td>
			</tr>
			<?php } ?>
			<?php if(!empty($dados) && $_POST['ID_TIPO_PECA'] != 0) { ?>
			<tr>
			    <td width="16%"><strong>Tipo de Peça</strong></td>
			    <td colspan="3" align="left"><?php echo $dados[0]['DESC_TIPO_PECA']; ?></td>
			</tr>
			<?php } ?>
		</table>
			<br><br>
		<?php if(!empty($dados)): ?>
			<div id="resultadoPesquisa">
			  	<table width="100%" class="tableSelection" border="0" cellspacing="0" cellpadding="0" align="center" style="margin: auto!important;">
				  	<tr style="background-color: #EDECEC;">
				  		<td style="width: 100px;text-align: left;">Pagina</td>
				  		<td style="width: 310px;text-align: left;">Ordem</td>
				  		<td style="width: 160px;text-align: left;">Código de Barras</td>
				  		<td style="width: 200px;text-align: left;">Nome</td>
				  		<td style="width: 70px;text-align: left;">Praça</td>
				  		<td style="width: 70px;text-align: left;">Tipo de Peça</td>
				  		<td style="width: 70px;text-align: left;">Preço Unitário</td>
				  		<td style="width: 70px;text-align: left;">Tipo Unitário</td>
				  		<td style="width: 70px;text-align: left;">Preço Caixa</td>
				  		<td style="width: 70px;text-align: left;">Tipo Caixa</td>
				  		<td style="width: 70px;text-align: left;">Preço Vista</td>
				  		<td style="width: 70px;text-align: left;">Preço De</td>
				  		<td style="width: 70px;text-align: left;">Preço Por</td>
				  		<td style="width: 70px;text-align: left;">Economize</td>
				  		<td style="width: 70px;text-align: left;">Entrada</td>
				  		<td style="width: 70px;text-align: left;">Prestação</td>
				  		<td style="width: 70px;text-align: left;">Juros A.M</td>
				  		<td style="width: 70px;text-align: left;">Juros A.A</td>
				  		<td style="width: 70px;text-align: left;">Prestação</td>
				  		<td style="width: 70px;text-align: left;">Total Prazo</td>
				  		<td style="width: 70px;text-align: left;">Observações</td>
				  		<td style="width: 70px;text-align: left;">Extra 1</td>
				  		<td style="width: 70px;text-align: left;">Extra 2</td>
				  		<td style="width: 70px;text-align: left;">Extra 3</td>
				  		<td style="width: 70px;text-align: left;">Extra 4</td>
				  		<td style="width: 70px;text-align: left;">Extra 5</td>
				  		<td style="width: 70px;text-align: left;">Job</td>
				  		
				  	</tr>
				  	<?php foreach ($dados as $dado) {?>
				  	<tr>
				  		<td style="width: 100px;text-align: left;"><?=$dado['PAGINA_ITEM'];?></td>
				  		<td style="width: 310px;text-align: left;"><?=$dado['ORDEM_ITEM'];?></td>
				  		
				  		<td style="width: 160px;text-align: left;"><?=$dado['COD_BARRAS_FICHA'];?></td>
				  		<td style="width: 200px;text-align: left;"><?=$dado['NOME_FICHA'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$dado['DESC_PRACA'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$dado['DESC_TIPO_PECA'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$dado['PRECO_UNITARIO'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$dado['TIPO_UNITARIO'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$dado['PRECO_CAIXA'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$dado['TIPO_CAIXA'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$dado['PRECO_VISTA'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$dado['PRECO_DE'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$dado['PRECO_POR'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$dado['ECONOMIZE'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$dado['ENTRADA'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$dado['PARCELAS'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$dado['PRESTACAO'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$dado['JUROS_AM'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$dado['JUROS_AA'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$dado['TOTAL_PRAZO'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$dado['OBSERVACOES'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$dado['EXTRA1_ITEM'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$dado['EXTRA2_ITEM'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$dado['EXTRA3_ITEM'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$dado['EXTRA4_ITEM'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$dado['EXTRA5_ITEM'];?></td>
				  		<td style="width: 70px;text-align: left;"><?=$dado['TITULO_JOB'];?></td>
				  		
				  	</tr>
				  	<?php }?>
			  	</table>
		  	</div>
			<?php else: ?>
				<div><?=$msg;?></div>
			<?php endif; ?>
		</td>
	</tr>
</table>
</body>
</html>