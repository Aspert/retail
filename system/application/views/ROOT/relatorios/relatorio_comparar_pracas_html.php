<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Relatorio de comparação entre praças</title>
<style type="text/css">
<!--
td, th, div {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 11px;
	border: 1px solid black;
	padding: 2px;
}
table { border-collapse: collapse;border-spacing: 0; }
th {
	color: #000;
	border-bottom-width: 2px;
	border-bottom-style: solid;
	border-bottom-color: #333;
	background-color:#EFEFEF;
}

.resultado {
	border-top: 2px solid black;
}

.resultado td, .resultado th {
	text-align: center;
	vertical-align: top;
}
-->
</style>
</head>

<body>
<?php
$colspan = count($pracas) + 1;
?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr>
		<td width="14%" bgcolor="#EFEFEF">Titulo do Job</td>
		<td width="86%" colspan="<?php echo $colspan ?>"><?php echo $job['TITULO_JOB']; ?></td>
	</tr>
	<tr>
		<td bgcolor="#EFEFEF">Agencia</td>
		<td colspan="<?php echo $colspan ?>"><?php echo $job['DESC_AGENCIA']; ?></td>
	</tr>
	<tr>
		<td bgcolor="#EFEFEF">Cliente</td>
		<td colspan="<?php echo $colspan ?>"><?php echo $job['DESC_CLIENTE']; ?></td>
	</tr>
	<tr>
		<td bgcolor="#EFEFEF">Bandeira</td>
		<td colspan="<?php echo $colspan ?>"><?php echo $job['DESC_PRODUTO']; ?></td>
	</tr>
	<tr>
		<td bgcolor="#EFEFEF">Campanha</td>
		<td colspan="<?php echo $colspan ?>"><?php echo $job['DESC_CAMPANHA']; ?></td>
	</tr>
</table>
<?php

$quebra = '<br />';
if (count($pracas)>0){
	$larguraColunas = floor( 90 / count($pracas) ) . '%';
} else {
	$larguraColunas = floor( 0 ) . '%';
} 
if(!empty($relatorio)):

?>


<table border="0" cellspacing="0" cellpadding="2" class="resultado" width="100%">
	<tr>
		<th width="5%">Página</th>
		<th width="5%">Ordem</th>
		<?php foreach($pracas as $praca): ?>
		<th width="<?php echo $larguraColunas; ?>"><?php echo $praca['DESC_PRACA']; ?></th>
		<?php endforeach; ?>
	</tr>
	<?php
		foreach($relatorio as $pagina => $ordens):
			foreach($ordens as $ordem => $praca):
	?>
	<tr>
		<td><?php echo $pagina; ?></td>
		<td><?php echo $ordem; ?></td>
		
		<?php foreach($pracas as $praca): ?>
		<td height="60" width="<?php echo $larguraColunas; ?>">
			<?php
			$item = $relatorio[ $pagina ][ $ordem ][ $praca['ID_PRACA'] ];
			
			if( empty($item) ){
				echo '&nbsp;';
			} else {
				echo $item['NOME_FICHA']
					, $quebra, '&nbsp;', $item['COD_BARRAS_FICHA'], '&nbsp;'
					, $quebra, $item['DESC_CATEGORIA']
					, $quebra, $item['DESC_SUBCATEGORIA'];
			}
			?>
		</td>
		<?php endforeach; ?>
		
	</tr>
	<?php
			endforeach;
		endforeach;
	?>
</table>
<?php else: ?>
NÃO HÁ RESULTADO PARA A PESQUISA.
<?php endif; ?>
</body>
</html>