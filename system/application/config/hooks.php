<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/



$hook['post_controller_constructor'][] = array(
                                'class'    => 'Sessao',
                                'function' => 'checaSessao',
                                'filename' => 'Sessao.php',
                                'filepath' => 'hooks'
);

$hook['post_controller_constructor'][] = array(
                                'class'    => 'LogAcoes',
                                'function' => 'log',
                                'filename' => 'LogAcoes.php',
                                'filepath' => 'hooks'
                                );

$hook['pre_system'][] = array(
                                'class'    => '',
                                'function' => '_setSessionId',
                                'filename' => 'Sessao.php',
                                'filepath' => 'hooks'
);
?>