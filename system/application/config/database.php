<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the "Database Connection"
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the "default" group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = "prod";
$active_record = TRUE;

$db['dev']['hostname'] = "localhost";
$db['dev']['username'] = "root";
$db['dev']['password'] = "@p1c4k&w";
$db['dev']['database'] = "retailh";
$db['dev']['dbdriver'] = "mysql";
$db['dev']['dbprefix'] = "";
$db['dev']['pconnect'] = FALSE;
$db['dev']['db_debug'] = TRUE;
$db['dev']['cache_on'] = FALSE;
$db['dev']['cachedir'] = "";
$db['dev']['char_set'] = "utf8";
$db['dev']['dbcollat'] = "utf8_general_ci";

$db['homolog']['hostname'] = "sw24703.247id.com.br";
$db['homolog']['username'] = "s247";
$db['homolog']['password'] = "@p1c4k&w";
$db['homolog']['database'] = "retail3h";
$db['homolog']['dbdriver'] = "mysql";
$db['homolog']['dbprefix'] = "";
$db['homolog']['pconnect'] = FALSE;
$db['homolog']['db_debug'] = TRUE;
$db['homolog']['cache_on'] = FALSE;
$db['homolog']['cachedir'] = "";
$db['homolog']['char_set'] = "utf8";
$db['homolog']['dbcollat'] = "utf8_general_ci";

$db['prod']['hostname'] = "localhost";
$db['prod']['username'] = "root";
$db['prod']['password'] = "root";
$db['prod']['database'] = "retail";
$db['prod']['dbdriver'] = "mysql";
$db['prod']['dbprefix'] = "";
$db['prod']['pconnect'] = FALSE;
$db['prod']['db_debug'] = TRUE;
$db['prod']['cache_on'] = FALSE;
$db['prod']['cachedir'] = "";
$db['prod']['char_set'] = "utf8";
$db['prod']['dbcollat'] = "utf8_general_ci";

$db['xinet']['hostname'] = "localhost";
$db['xinet']['username'] = "root";
$db['xinet']['password'] = "root";
$db['xinet']['database'] = "test";
$db['xinet']['dbdriver'] = "mysql";
$db['xinet']['dbprefix'] = "";
$db['xinet']['pconnect'] = FALSE;
$db['xinet']['db_debug'] = TRUE;
$db['xinet']['cache_on'] = FALSE;
$db['xinet']['cachedir'] = "";
//$db['xinet']['char_set'] = "utf8";
//$db['xinet']['dbcollat'] = "utf8_general_ci";

$db2['xinet']['hostname'] = "test";
$db2['xinet']['username'] = "root";
$db2['xinet']['password'] = "root";
$db2['xinet']['database'] = "webnative";
$db2['xinet']['dbdriver'] = "mysql";
$db2['xinet']['dbprefix'] = "";
$db2['xinet']['pconnect'] = TRUE;
$db2['xinet']['db_debug'] = TRUE;
$db2['xinet']['cache_on'] = FALSE;
$db2['xinet']['cachedir'] = "";
$db2['xinet']['char_set'] = "utf8";
$db2['xinet']['dbcollat'] = "utf8_general_ci";

