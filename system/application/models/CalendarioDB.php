<?php
class CalendarioDB  extends GenericModel{
	### START
	protected function _initialize(){
		
		$this->addField('ID_CALENDARIO','int','',11,1);
		$this->addField('ID_CLIENTE','int','',11,0);
		$this->addField('NOME_CALENDARIO','string','',45,0);
		$this->addField('STATUS_CALENDARIO','int','',1,0);
	}
	### END


	var $tableName = 'TB_CALENDARIO';  	
	
	/**
	 * Construtor
	 * @author Hugo Ferreira da Silva
	 * @return CalendarioDB
	 */
	public function __construct(){
		parent::GenericModel();

		$this->addValidation('ID_CLIENTE','requiredString','Por favor, selecionar o cliente');
		$this->addValidation('NOME_CALENDARIO','requiredString','Por favor, digite o nome do calendário');
	}
	
	/**
	 * Feriados por Job
	 * @author Nelson Martucci
	 * @param unknown_type $idJob
	 * @return unknown_type
	 */
	public function getFeriadosByJob($idJob=0) {

		//Array com os jobs
		$arrayFeriadosJob = array();
		
		$rs = $this->db
			->select('CF.DATA_FERIADO, CF.FLAG_TODOS_ANOS')
			->from('TB_CALENDARIO_FERIADO CF')
			->join('TB_JOB J','J.ID_JOB = ' . $idJob )
			->join('TB_CAMPANHA C','C.ID_CAMPANHA = J.ID_CAMPANHA')
			->join('TB_PRODUTO P','P.ID_PRODUTO = C.ID_PRODUTO')
			->where('CF.ID_CALENDARIO = P.ID_CALENDARIO')
			->get();

		// se encontrou alguma coisa
		if( $rs->num_rows() > 0 ){
			
			$feriadosCadastrados = $rs->result_array();
			
			//Monta array de retorno e verifica os feriados para todos os anos
			foreach ( $feriadosCadastrados as $feriado ) {
				
				$diaFeriado = format_date($feriado['DATA_FERIADO'],'d');
				$mesFeriado = format_date($feriado['DATA_FERIADO'],'m');
				 
				//Vale para todos os anos ?
				if ( strtoupper($feriado['FLAG_TODOS_ANOS']) == 'S' ) {
					$anoFeriado = date('Y');
				} else {
					$anoFeriado = format_date($feriado['DATA_FERIADO'],'Y');
				}
				
				$arrayFeriadosJob[] = mktime( 0, 0, 0, $mesFeriado, $diaFeriado, $anoFeriado);
				
			}
			
		}

		// retorna o resultado
		return $arrayFeriadosJob;
		
	}
	
	/**
	 * Feriados por Produto/Bandeira
	 * @author Nelson Martucci
	 * @param unknown_type $idProduto
	 * @return unknown_type
	 */
	public function getFeriadosByProduto($idProduto=0) {
		
		//Array com os jobs
		$arrayFeriadosProduto = array();
		
		$rs = $this->db
			->select('CF.DATA_FERIADO, CF.FLAG_TODOS_ANOS')
			->from('TB_CALENDARIO_FERIADO CF')
			->join('TB_PRODUTO P','P.ID_PRODUTO = ' . $idProduto )
			->where('CF.ID_CALENDARIO = P.ID_CALENDARIO')
			->get();

		// se encontrou alguma coisa
		if( $rs->num_rows() > 0 ){
			$feriadosCadastrados = $rs->result_array();
			
			//Monta array de retorno e verifica os feriados para todos os anos
			foreach ( $feriadosCadastrados as $feriado ) {
				
				$diaFeriado = format_date($feriado['DATA_FERIADO'],'d');
				$mesFeriado = format_date($feriado['DATA_FERIADO'],'m');
				 
				//Vale para todos os anos ?
				if ( strtoupper($feriado['FLAG_TODOS_ANOS']) == 'S' ) {
					$anoFeriado = date('Y');
				} else {
					$anoFeriado = format_date($feriado['DATA_FERIADO'],'Y');
				}
				
				$arrayFeriadosProduto[] = mktime( 0, 0, 0, $mesFeriado, $diaFeriado, $anoFeriado);
				
			}
			
		}

		// retorna o resultado
		return $arrayFeriadosProduto;
		
	}
	
	/**
	 * Lista os processos cadastrados
	 *
	 * @author Sdinei Tertuliano Junior
	 * @param array $filters
	 * @param int $page
	 * @param int $limit
	 * @param string $field
	 * @param string $direction
	 * @return array
	 */
	public function listItems($filters, $page=0, $limit=5, $field='NOME_CALENDARIO', $direction='ASC'){
		$this->setWhereParams($filters);
		if(isset($filters['STATUS_CALENDARIO']) && $filters['STATUS_CALENDARIO'] == '0'){
			$this->db->where('STATUS_CALENDARIO', 0);
		}
		$this->db->join('TB_CLIENTE', 'TB_CALENDARIO.ID_CLIENTE = TB_CLIENTE.ID_CLIENTE');
		return $this->execute($page, $limit, $field, $direction);
	}
	
	/**
	 * Pega todos os feriados de um calendario
	 *
	 * @author Sdinei Tertuliano Junior
	 * @param string $idCalendario
	 * @return array
	 */
	public function getFeriadosByIdCalendario($idCalendario){
		$rs = $this->db->select('ID_CALENDARIO_FERIADO, NOME_FERIADO, DATA_FERIADO, FLAG_TODOS_ANOS')
				->from('TB_CALENDARIO_FERIADO')
				->where('ID_CALENDARIO', $idCalendario)
				->get();
				
		return $rs->result_array();
	}
	
	/**
	 * Deleta os feriados q foram apagados da pauta
	 * Recebe uma array de paramatro e faz o delete em todos os itens q NÃÃÃÃO ESTÃÃÃÃÃÃÃO nessa array 
	 *
	 * @author Sdinei Tertuliano Junior
	 * @param string $idCalendario
	 * @param array $itens
	 * @return array
	 */
	public function deletaFeriados($idCalendario, $itens){
		$this->db->where('ID_CALENDARIO', $idCalendario);

		if(count($itens) > 0){
			$this->db->where_not_in('ID_CALENDARIO_FERIADO', $itens );
		}
		//$this->db->delete('TB_CALENDARIO_FERIADO');
		$this->db->delete('TB_CALENDARIO_FERIADO');
	}
	
	/**
	 * Adiciona feriado ao calendario 
	 *
	 * @author Sidnei Tertuliano Junior
	 * @param int $idCalendario
	 * @param array $campos
	 * @return int
	 */
	public function saveFeriado($idCalendario, $campos){		
		if($campos['ID_CALENDARIO_FERIADO'] == '0'){
			$campos['ID_CALENDARIO'] = $idCalendario;
			unset($campos['ID_CALENDARIO_FERIADO']);
			$this->db->insert('TB_CALENDARIO_FERIADO', $campos);
		}
		else{
			$this->db->where('ID_CALENDARIO_FERIADO', $campos['ID_CALENDARIO_FERIADO']);
			$this->db->update('TB_CALENDARIO_FERIADO', $campos);
		}
		
	}
	
	/**
	 * @author Sidnei Tertuliano Junior
	 * Get calendarios(s) by ID_CLIENTE
	 * @param $idCliente
	 */
	public function getByCliente($idCliente){
		$rs = $this->db->where('STATUS_CALENDARIO', '1')
				->where('ID_CLIENTE', $idCliente)
				->get('TB_CALENDARIO');
		return $rs->result_array();
	}
}	

