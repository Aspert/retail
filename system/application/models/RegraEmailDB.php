<?php
/**
 * Model que representa a tabela TB_REGRA_EMAIL
 * @author Juliano Polito
 * @link http://www.247id.com.br
 */
class RegraEmailDB extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_REGRA_EMAIL','int','',1,1);
		$this->addField('CHAVE_REGRA_EMAIL','string','',12,0);
		$this->addField('DESC_REGRA_EMAIL','string','',42,0);
		$this->addField('STATUS_REGRA_EMAIL','int','',1,0);
		$this->addField('FLAG_ENVIA_LOGADO','int','',1,0);
	}
	### END

	var $tableName = 'TB_REGRA_EMAIL';
	
	/**
	 * Construtor
	 * 
	 * @author Juliano Polito
	 * @link http://www.247id.com.br
	 * @return RegraEmailDB
	 */
	function __construct(){
		parent::GenericModel();
		
		$this->addValidation('CHAVE_REGRA_EMAIL','requiredString','Informe a chave');
		$this->addValidation('DESC_REGRA_EMAIL','requiredString','Informe o nome');
		
	}
	
	/**
	 * Lista as regras cadastradas
	 * 
	 * @author Juliano Polito
	 * @link http://www.247id.com.br
	 * @param array $filters
	 * @param int $page Pagina atual
	 * @param int $limit numero de itens por pagina
	 * @return array
	 */
	public function listItems($filters, $page=0, $limit=5){
		$this->db->order_by('CHAVE_REGRA_EMAIL ASC');
		$this->setWhereParams($filters);
		return $this->execute($page,$limit);
	}
	
	/**
	 * Recupera as regras de email ativas por controller e funcao
	 * @author juliano.polito
	 * @param $controller string Nome da controller
	 * @param $function string Nome da funcao
	 * @return array
	 */
	public function getByChave($chave){
		$rs = $this->db->from($this->tableName . ' RE' )
			->where('RE.CHAVE_REGRA_EMAIL',$chave)
			->where('RE.STATUS_REGRA_EMAIL',1)
			->get();

		return $rs->row_array();
		
	}
	
	
}






















