<?php
/**
 * Classe de modelo para o gerenciamento de campanha
 * @author Juliano Polito
 * @link http://www.247id.com.br
 */
class CampanhaDB extends GenericModel {
	### START
	protected function _initialize(){
		$this->addField('ID_CAMPANHA','int','',1,1);
		$this->addField('COD_CATALOGO','int','',1,0);
		$this->addField('ID_PRODUTO','int','',1,0);
		$this->addField('DESC_CAMPANHA','string','',10,0);
		$this->addField('DT_INICIO_CAMPANHA','string','',10,0);
		$this->addField('DT_FIM_CAMPANHA','string','',10,0);
		$this->addField('STATUS_CAMPANHA','int','',1,0);
		$this->addField('CHAVE_STORAGE_CAMPANHA','string','',1,0);
		$this->addField('DATA_ALTERACAO','datetime','',10,0);
		$this->addField('DIRETORIO_CRIADO','int','',1,0);
			}
	### END

	
	var $tableName = 'TB_CAMPANHA';
	
	/**
	 * Construtor
	 *  
	 * @author Juliano Polito
	 * @link http://www.247id.com.br
	 * @return CampanhaDB
	 */
	function __construct(){
		parent::GenericModel();
		
		// adiciona as validacoes
		$this->addValidation('ID_PRODUTO','requiredNumber','Informe a linha de produto');
		$this->addValidation('DESC_CAMPANHA','requiredString','Informe o nome');
		$this->addValidation('DESC_CAMPANHA','function',array($this,'checaNomeDuplicado'));
		$this->addValidation('DT_INICIO_CAMPANHA','requiredDate','Informe a data de inicio');
		$this->addValidation('DT_FIM_CAMPANHA','requiredDate','Informe data de término');
		$this->addValidation('DT_FIM_CAMPANHA','function',array($this,'checaDataTermino'));
		$this->addValidation('STATUS_CAMPANHA','requiredNumber','Informe o status');
		$this->addValidation('CHAVE_STORAGE_CAMPANHA','function',array($this,'checaChaveStorage'));
	}
	
	/**
	 * Metodo para listar itens
	 * @author Juliano Polito
	 * @link http://www.247id.com.br
	 * @param array $data Dados para filtro
	 * @param int $pagina numero da pagina atual
	 * @param int $limit limit de itens por pagina
	 * @return array
	 */
	public function listItems($data, $pagina=0, $limit = 5, $order = '', $orderDirection = 'ASC'){

		//print_rr($data);die;
		// se informou a agencia, pegamos os clientes da agencia
		// fazemos isso em primeiro lugar por que o codeigniter se
		// confunde com o active record.
		if( !empty($data['ID_AGENCIA']) ){
			// pega os clientes da agencia
			$list = $this->cliente->getByAgencia($data['ID_AGENCIA']);
			// lista de codigos
			$codes = array(0);
			// para cada cliente da agencia
			foreach($list as $item){
				// coloca o codigo na lista
				$codes[] = $item['ID_CLIENTE'];
			}
			// coloca a condicao
			$this->db->where_in('TB_CLIENTE.ID_CLIENTE', $codes);
		}
		
		
		if( !empty($data['PRODUTOS']) ){
			$this->db->where_in('TB_CAMPANHA.ID_PRODUTO', $data['PRODUTOS']);
		}
		
		if( !empty($data['ID_CLIENTE']) ){
			$this->db->where('TB_CLIENTE.ID_CLIENTE', $data['ID_CLIENTE']);
		}
		
		$this->db->join('TB_PRODUTO', 'TB_CAMPANHA.ID_PRODUTO = TB_PRODUTO.ID_PRODUTO')
			->join('TB_CLIENTE', 'TB_PRODUTO.ID_CLIENTE = TB_CLIENTE.ID_CLIENTE');
		
		if(is_numeric(Sessao::get('usuario'))){
			$this->db->where('TB_AGENCIA.ID_AGENCIA', $data['ID_AGENCIA']);
		}

		if(isset($data['STATUS_CAMPANHA'])){
			$this->db->where('TB_CAMPANHA.STATUS_CAMPANHA', $data['STATUS_CAMPANHA']);
		}
		
		if( !empty($data['DT_INICIO_CAMPANHA']) && !empty($data['DT_FIM_CAMPANHA']) ){
			$this->db->where('TB_CAMPANHA.DT_INICIO_CAMPANHA >= ', format_date($data['DT_INICIO_CAMPANHA']) );
			$this->db->where('TB_CAMPANHA.DT_FIM_CAMPANHA <= ', format_date($data['DT_FIM_CAMPANHA']) );
					
			/*$sql = sprintf("
				((TB_CAMPANHA.DT_INICIO_CAMPANHA BETWEEN '%1\$s' AND '%2\$s') OR
				(TB_CAMPANHA.DT_FIM_CAMPANHA BETWEEN '%1\$s' AND '%2\$s') OR
				('%1\$s' BETWEEN TB_CAMPANHA.DT_INICIO_CAMPANHA AND TB_CAMPANHA.DT_FIM_CAMPANHA) OR
				('%2\$s' BETWEEN TB_CAMPANHA.DT_INICIO_CAMPANHA AND TB_CAMPANHA.DT_FIM_CAMPANHA))",
				format_date($data['DT_INICIO_CAMPANHA']),
				format_date($data['DT_FIM_CAMPANHA'])
			);*/
			
			//$this->db->where($sql);
			
			unset($data['DT_INICIO_CAMPANHA'], $data['DT_FIM_CAMPANHA']);
			
		} else if( !empty($data['DT_INICIO_CAMPANHA'])){
			$this->db->where('TB_CAMPANHA.DT_INICIO_CAMPANHA >= ', format_date($data['DT_INICIO_CAMPANHA']) );
			unset($data['DT_INICIO_CAMPANHA']);
			
		} else if( !empty($data['DT_FIM_CAMPANHA'])){
			$this->db->where('TB_CAMPANHA.DT_FIM_CAMPANHA <= ', format_date($data['DT_FIM_CAMPANHA']) );
			unset($data['DT_FIM_CAMPANHA']);
		}
		
		$this->setWhereParams($data);
		
		return $this->execute($pagina, $limit, $order, $orderDirection);
		 
	}
	
	/**
	 * Recupera um registro de campanha com todos os joins (Cliente, Produto, Agencia)
	 * 
	 * @author Juliano Polito
	 * @param $id
	 * @return array
	 */
	public function getByIdJoin($id){
		
		if($id==null){
			return null;
		}
		
		if ( isset($this->statusField) ){
			$this->db->where($this->statusField, MODEL_STATUS_ATIVO);
		}
		
		$this->db
			->join('TB_PRODUTO', 'TB_PRODUTO.ID_PRODUTO = TB_CAMPANHA.ID_PRODUTO')
			->join('TB_CLIENTE','TB_CLIENTE.ID_CLIENTE=TB_PRODUTO.ID_CLIENTE');
		
		$this->db->where($this->primaryKey, $id);
		
		$rs = $this->db->get($this->tableName);

		if ( $rs->num_rows() == 1 ){
			return $rs->row_array();
		}else{
			return null;
		}
	}
	
	/**
	 * recupera as campanhas de um cliente
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idCliente
	 * @return array
	 */
	public function getByCliente($idCliente){
		$rs = $this->db->from($this->tableName.' CA')
			->join('TB_CLIENTE C','CA.ID_CLIENTE = C.ID_CLIENTE')
			->where('CA.STATUS_CAMPANHA = 1')
			->where('C.ID_CLIENTE', $idCliente)
			->order_by('DESC_CAMPANHA')
			->get();
			
		return $rs->result_array();
	}
	
	/**
	 * recupera as campanhas de um produto
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idProduto
	 * @return array
	 */
	public function getByProduto($idProduto, $tipo = NULL){
		$rs = $this->db->from($this->tableName.' CA')
			->where('CA.STATUS_CAMPANHA = 1')
			->where('CA.ID_PRODUTO', $idProduto)
			->order_by('DESC_CAMPANHA')
			->get();
			
		return $rs->result_array();
	}
	public function getByProduto2($idProduto){
		$rs = $this->db->from($this->tableName.' CA')
			->where('CA.ID_PRODUTO', $idProduto)
			->order_by('DESC_CAMPANHA')
			->get();
			
		return $rs->result_array();
	}
	/**
	 * Recupera itens filtrando a partir de sessao
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $session Dados da sessao
	 * @param array $filters Demais dados para filtro
	 * @return array
	 */
	public function filterBySession($session, $filters=array()){
		$this->db->from($this->tableName)
			->join('TB_CLIENTE C','C.ID_CLIENTE = TB_CAMPANHA.ID_CLIENTE');
		
		if(!empty($session['ID_AGENCIA'])){
			$this->db->where('C.ID_AGENCIA', $session['ID_AGENCIA']);
		}
		
		if(!empty($session['C.ID_CLIENTE'])){
			$this->db->where('C.ID_CLIENTE', $session['ID_CLIENTE']);
		}
		
		$this->db->order_by('DESC_CAMPANHA');
		$this->setWhereParams($filters);
		
		return $this->db->get()->result_array();
	}
	
	/**
	 * verifica se a data de termino eh menor que a data atual
	 * @author Hugo Ferreira da Silva
	 * @param array $data
	 * @return mixed
	 */
	public function checaDataTermino($data){
		$time = strtotime(format_date($data['DT_FIM_CAMPANHA'],'Y-m-d'));
		$hoje = strtotime(date('Y-m-d'));
		
		if( $time <= $hoje ){
			return 'A data final da campanha deve ser maior que a data atual!';
		}
		
		return true;
	}
	
	/**
	 * Checa nome duplicado de campanha
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data
	 * @return mixed
	 */
	public function checaNomeDuplicado($data){
		$id = empty($data['ID_CAMPANHA']) ? 0 : sprintf('%d',$data['ID_CAMPANHA']);
		
		$rs = $this->db->where('ID_PRODUTO', $data['ID_PRODUTO'])
			->where('DESC_CAMPANHA',$data['DESC_CAMPANHA'])
			->where('ID_CAMPANHA != ', $id)
			->get($this->tableName);

		if($rs->num_rows() > 0){
			return 'Já existe uma campanha com este nome para este produto';
		}
		
		return true;
	}
	
	/**
	 * verifica se a chave de storage ja existe cadastrada para outra campanha
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param array $data
	 * @return boolean|string
	 */
	public function checaChaveStorage($data){
		$id = empty($data['ID_CAMPANHA']) ? 0 : sprintf('%d',$data['ID_CAMPANHA']);
		
		$rs = $this->db->where('ID_PRODUTO', $data['ID_PRODUTO'])
			->where('CHAVE_STORAGE_CAMPANHA', empty($data['CHAVE_STORAGE_CAMPANHA']) ? '' : $data['CHAVE_STORAGE_CAMPANHA'])
			->where('ID_CAMPANHA != ', $id)
			->get($this->tableName);

		if($rs->num_rows() > 0){
			return 'Já existe uma chave de storage com este nome para outra campanha deste produto';
		}
		
		return true;
	}
	
	/**
	 * Retorna a pasta de uma campanha
	 * 
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idCampanha codigo da campanha
	 * @return string
	 */
	public function getPath($idCampanha){
		$rs = $this->getById($idCampanha);
		$path = $this->produto->getPath($rs['ID_PRODUTO']);
		$path .= $rs['CHAVE_STORAGE_CAMPANHA'] . '/';
			
		return $path;
	}
	
	/**
	 * Retorna uma lista com as campanhas ativas
	 * @param array $filters filtros adicionais
	 * @author Hugo Ferreira da Silva
	 * @return array
	 */
	public function getAtivas(array $filters = array()){
		
		$this->db
			->join('TB_CLIENTE C','C.ID_CLIENTE = CA.ID_CLIENTE')
			->join('TB_AGENCIA A','C.ID_AGENCIA = A.ID_AGENCIA')
			->join('TB_PRODUTO P','P.ID_PRODUTO = CA.ID_PRODUTO')
			->where('CURRENT_DATE BETWEEN DT_INICIO_CAMPANHA AND DT_FIM_CAMPANHA', false);
			
		if( !empty($filters['ID_AGENCIA']) ){
			$this->db->where('A.ID_AGENCIA', $filters['ID_AGENCIA']);
		}
		
		if( !empty($filters['ID_CLIENTE']) ){
			$this->db->where('C.ID_CLIENTE', $filters['ID_CLIENTE']);
		}
		
		if( !empty($filters['ID_PRODUTO']) ){
			$this->db->where('CA.ID_PRODUTO', $filters['ID_PRODUTO']);
		}
		
		$rs = $this->db
			->get($this->tableName . ' CA')
			->result_array();
			
		return $rs;
	}
	
	/**
	 * Recupera somente o codigo das campanhas atiavs
	 * @author Hugo Ferreira da Silva
	 * @param array $filters filtros adicionais
	 * @return array
	 */
	public function getCodigoAtivas(array $filters = array()){
		$codes = array(0);
		
		$list = $this->getAtivas($filters);
		foreach($list as $item){
			$codes[] = $item['ID_CAMPANHA'];
		}
		
		return $codes;
	}
	
	/**
	 * inativamos as campanhas que estao com prazo expirado
	 * 
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function inativarPorPeriodo() {
		$data = array('STATUS_CAMPANHA' => 0);
		
		$this->db
			->where('DT_FIM_CAMPANHA < CURRENT_DATE')
			->update($this->tableName, $data);
			
	}
	
	/**
	 * verifica se o diretorio da campanha existe
	 * 
	 * @author Sidnei Tertuliano Junior
	 * @return void
	 */
	public function verificaPathCampanha( $idCampanha ){
		$path = $this->getPath( $idCampanha );
		if( is_dir( $path ) ){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	/**
	 * seleciona as campanhas por diretorio criado ou nao
	 * 
	 * @author Sidnei Tertuliano Junior
	 * @return void
	 */
	public function getByDiretorioCriado( $criado=0 ){
		$rs = $this->db->from($this->tableName)
			->where('diretorio_criado', $criado)
			->get();
			
		return $rs->result_array();
	}
}

