<?php
class FichaDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_FICHA','int','',1,1);
		$this->addField('ID_USUARIO','int','',2,0);
		$this->addField('COD_FICHA','string','',6,0);
		$this->addField('COD_BARRAS_FICHA','string','',13,0);
		$this->addField('COD_CIP_FICHA','string','',1,0);
		$this->addField('NOME_FICHA','string','',35,0);
		$this->addField('ID_CATEGORIA','int','',1,0);
		$this->addField('ID_SUBCATEGORIA','int','',2,0);
		$this->addField('FABRICANTE_FICHA','string','',0,0);
		$this->addField('MARCA_FICHA','string','',0,0);
		$this->addField('MODELO_FICHA','string','',0,0);
		$this->addField('FLAG_ACTIVE_FICHA','int','',1,0);
		$this->addField('ID_APROVACAO_FICHA','int','',1,0);
		$this->addField('DT_EXPIRA_FICHA','timestamp','',0,0);
		$this->addField('DT_INSERT_FICHA','timestamp','',19,0);
		$this->addField('TIPO_COMBO_FICHA','int','',1,0);
		$this->addField('ID_FABRICANTE','int','',1,0);
		$this->addField('ID_MARCA','int','',1,0);
		$this->addField('FLAG_FULL_FICHA','string','',1,0);
		$this->addField('FLAG_TEMPORARIO','int','',1,0);
		$this->addField('ID_CLIENTE','int','',1,0);
		$this->addField('ID_SEGMENTO','int','',1,0);
		$this->addField('DATA_ALTERACAO','datetime','',19,0);
		$this->addField('OBS_FICHA','blob','',0,0);
		$this->addField('PENDENCIAS_DADOS_BASICOS','blob','',0,0);
		$this->addField('PENDENCIAS_CAMPOS','blob','',0,0);
		$this->addField('TIPO_FICHA','string','',0,0);
		$this->addField('DATA_INICIO','date','',0,0);
		$this->addField('DATA_VALIDADE','date','',0,0);
		
	}
	### END


	var $tableName = 'TB_FICHA';

	function __construct(){
		parent::GenericModel();
/*
		$this->addValidation('ID_CLIENTE','requiredNumber','Selecione um cliente');

		//$this->addValidation('COD_FICHA','requiredString','Insira o codigo da ficha');
		//$this->addValidation('COD_FICHA','function',array($this, 'checaCodigoDuplicado'));

		$this->addValidation('NOME_FICHA','requiredString','Insira o nome da ficha');
		$this->addValidation('ID_CATEGORIA','requiredNumber','Selecione uma categoria');
		$this->addValidation('ID_SUBCATEGORIA','requiredNumber','Selecione uma sub-categoria');

		//$this->addValidation('ID_SEGMENTO','requiredNumber','Selecione um segmento');
		//$this->addValidation('COD_BARRAS_FICHA','requiredString','Insira o codigo de barras');
		$this->addValidation('COD_BARRAS_FICHA','function', array($this, 'checaBarrasDuplicado'));

		$this->addValidation('DATA_INICIO','function', array($this, 'validateDataInicio'));
		$this->addValidation('DATA_VALIDADE','function', array($this, 'validateDataValidade'));
*/
	}

	/**
	 * override para se nao informar as datas necessarias, grava como NULL
	 * @see system/application/libraries/GenericModel::save()
	 */
	public function save($data, $id = null){

		if( array_key_exists('DATA_INICIO', $data) ){
			if( empty($data['DATA_INICIO']) ){
				$data['DATA_INICIO'] = null;
			} else {
				$data['DATA_INICIO'] = format_date($data['DATA_INICIO']);
			}
		}

		if( array_key_exists('DATA_VALIDADE', $data) ){
			if( empty($data['DATA_VALIDADE']) ){
				$data['DATA_VALIDADE'] = null;
			} else {
				$data['DATA_VALIDADE'] = format_date($data['DATA_VALIDADE']);
			}
		}
		
		if(isset($data['COD_CIP_FICHA']) && !is_numeric($data['COD_CIP_FICHA'])){
			unset($data['COD_CIP_FICHA']);
		}

		return parent::save($data, $id);

	}
	
	public function updateFicha($data,$id) {
		
		$this->db->where('ID_FICHA', $id);
		$this->db->update('TB_FICHA', $data);
		
	}
	
	public function updateFichaByIds($data,$id) {
	
		$this->db->where_in('ID_FICHA', $id);
		$this->db->update('TB_FICHA', $data);
	}
	

	/**
	 * valida a data de inicio da ficha
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data dados vindos do formulario
	 * @return mixed
	 */
	public function validateDataInicio($data){
		// se informou a data de inicio
		if( !empty($data['DATA_INICIO']) ){

			$hoje = date('Ymd');
			$inicio = format_date($data['DATA_INICIO'], 'Ymd');

			//PA 8001 - Data de inicio tem que ser maior ou igual que data atual
			if ($inicio < $hoje) {
				return 'Por favor, informe uma data igual ou maior que a data atual.';				
			}
			//Fim PA 8001
			
			// se tambem informou a data de validade
			if( !empty($data['DATA_VALIDADE']) ){

				$validade = format_date($data['DATA_VALIDADE'], 'Ymd');

				// se o inicio for maior ou igual a validade
				if( $inicio >= $validade ){
					return 'A data de inicio não poder ser maior ou igual a data de validade';
				}
			}
		}

		return true;
	}

	/**
	 * valida a data de validade da ficha
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data dados vindos do formulario
	 * @return mixed
	 */
	public function validateDataValidade($data){
		// se informou a data de inicio
		if( !empty($data['DATA_VALIDADE']) ){

			$hoje = date('Ymd');
			$validade = format_date($data['DATA_VALIDADE'], 'Ymd');

			// verifica se a data de validade e menor ou igual a data atual
			if( $validade < $hoje ){
				return 'A data de validade deve ser maior ou igual a data atual';
			}

		}

		return true;
	}

	/**
	 * Verifica se a ficha sendo cadastrada tem codigo duplicado
	 * @author juliano.polito
	 * @param $data
	 * @return mixed
	 */
	function checaCodigoDuplicado($data){

		$codficha = empty($data['COD_FICHA']) ? 0 : $data['COD_FICHA'];

		$rs = $this->db
			->where('ID_CLIENTE',$data['ID_CLIENTE'])
			->where('COD_FICHA = ', $codficha)
			->where('ID_FICHA !=', empty($data['ID_FICHA']) ? 0 : $data['ID_FICHA'])
			->get($this->tableName);

		if($rs->num_rows() > 0){
			return 'Já existe uma ficha para este codigo';
		}

		return true;
	}

	/**
	 * Verifica se existem codigos de barra duplicados
	 *
	 * @author juliano.polito
	 * @param $data
	 * @return mixed
	 */
	function checaBarrasDuplicado($data){
		if( !empty($data['TIPO_FICHA']) && $data['TIPO_FICHA'] == 'COMBO' ){
			return true;
		}

		$codBarrasficha = empty($data['COD_BARRAS_FICHA']) ? '' : sprintf('%s',$data['COD_BARRAS_FICHA']);

		if( empty($codBarrasficha) ){
			return 'Informe o código de barras';
		}

		$rs = $this->db
			->where('ID_CLIENTE',$data['ID_CLIENTE'])
			->where('COD_BARRAS_FICHA = ', $codBarrasficha)
			->where('COD_BARRAS_FICHA != ', '')
			->where('ID_FICHA !=', empty($data['ID_FICHA']) ? 0 : $data['ID_FICHA'])
			->get($this->tableName);

		if($rs->num_rows() > 0){
			$fic = $rs->row_array();
			return 'A ficha '.$fic['COD_FICHA'].' já possui o código de barras: '.$fic['COD_BARRAS_FICHA'];
		}

		return true;
	}


	/**
	 * Verifica se a ficha sendo cadastrada tem segmento+codigo duplicado
	 * @author juliano.polito
	 * @param $data
	 * @return mixed
	 */
	function checaSegmentoDuplicado($data){

		throw new Exception('Este metodo foi depreciado por que o segmento nao existe mais');

		$idsegmento = empty($data['ID_SEGMENTO']) ? 0 : sprintf('%d',$data['ID_SEGMENTO']);
		$codficha = empty($data['COD_FICHA']) ? 0 : sprintf('%d',$data['COD_FICHA']);

		$rs = $this->db->where('ID_AGENCIA', $data['ID_AGENCIA'])
			->where('ID_CLIENTE',$data['ID_CLIENTE'])
			->where('ID_SEGMENTO = ', $idsegmento)
			->where('COD_FICHA = ', $codficha)
			->where('ID_FICHA !=', empty($data['ID_FICHA']) ? 0 : $data['ID_FICHA'])
			->get($this->tableName);

		if($rs->num_rows() > 0){
			return 'Já existe uma ficha para este segmento e codigo';
		}

		return true;
	}

	/**
	 * Recupera ficha por cliente/codigo
	 *
	 * @author juliano.polito
	 * @param $idCliente
	 * @param $codFicha
	 * @return array
	 */
	public function getByClienteCodigo($idCliente, $codFicha){
		$codficha = empty($data['COD_FICHA']) ? 0 : sprintf('%d',$data['COD_FICHA']);

		$rs = $this->db->where('ID_CLIENTE',$idCliente)
			->where('COD_BARRAS_FICHA = ', $codFicha)
			->get($this->tableName);

		return $rs->row_array();
	}

	public function listItems($data, $pagina=0, $limit = 5, $order = '', $orderDirection = 'ASC'){
		if( empty($limit) ){
			$limit = 5;
		}
		
		// se informou a agencia
		if( !empty($data['ID_AGENCIA']) ){
			// pega os clientes da agencia
			$clientes = $this->cliente->getByAgencia($data['ID_AGENCIA']);
			// lista de codigos de clientes
			$codes = array(0);
			// para cada cliente
			foreach($clientes as $item){
				// coloca o codigo na lista
				$codes[] = $item['ID_CLIENTE'];
			}
			// filtra pelos codigos dos clientes
			$this->db->where_in('TB_FICHA.ID_CLIENTE', $codes);
		}


		$this->db->select(	' *,' .
							' TB_FICHA.ID_FICHA,(' .
							' SELECT NOME_USUARIO FROM TB_HISTORICO_FICHA ' .
							' JOIN TB_USUARIO ON TB_USUARIO.ID_USUARIO = TB_HISTORICO_FICHA.ID_USUARIO' .
							' WHERE TB_HISTORICO_FICHA.ID_FICHA = TB_FICHA.ID_FICHA' .
							' ORDER BY DT_INSERT_HISTORICO DESC' .
							' LIMIT 1) AS USER_APROVACAO '
						 );

		//$this->db->join('TB_SEGMENTO SE', 'SE.ID_SEGMENTO = TB_FICHA.ID_SEGMENTO','left');
		$this->db->join('TB_APROVACAO_FICHA', 'TB_APROVACAO_FICHA.ID_APROVACAO_FICHA = TB_FICHA.ID_APROVACAO_FICHA','left');
		$this->db->join('TB_OBJETO_FICHA OF', 'TB_FICHA.ID_FICHA = OF.ID_FICHA and OF.PRINCIPAL = 1','left');
		$this->db->join('TB_OBJETO O', 'O.ID_OBJETO = OF.ID_OBJETO','left');
		$this->db->join('TB_CATEGORIA', 'TB_FICHA.ID_CATEGORIA = TB_CATEGORIA.ID_CATEGORIA','LEFT');
		$this->db->join('TB_SUBCATEGORIA', 'TB_FICHA.ID_SUBCATEGORIA = TB_SUBCATEGORIA.ID_SUBCATEGORIA','LEFT');
		$this->db->join('TB_FABRICANTE', 'TB_FICHA.ID_FABRICANTE = TB_FABRICANTE.ID_FABRICANTE','LEFT');
		$this->db->join('TB_FICHA_CODIGO FC', 'TB_FICHA.ID_FICHA = FC.ID_FICHA','LEFT');
		$this->db->join('TB_MARCA', 'TB_FICHA.ID_MARCA = TB_MARCA.ID_MARCA','LEFT');
		$this->db->join('TB_FICHA_PAI_FILHA', 'TB_FICHA.ID_FICHA = TB_FICHA_PAI_FILHA.ID_FICHA_PAI','LEFT');
		$this->db->join('TB_CLIENTE', 'TB_FICHA.ID_CLIENTE = TB_CLIENTE.ID_CLIENTE');
		$this->db->join('TB_SUBFICHA', 'TB_FICHA.ID_FICHA = TB_SUBFICHA.ID_FICHA1','LEFT');
		
		//puxa somente fichas de categorias e sub ativas
		$this->db->where("TB_CATEGORIA.FLAG_ACTIVE_CATEGORIA",1);
		$this->db->where("TB_SUBCATEGORIA.FLAG_ACTIVE_SUBCATEGORIA",1);
	
		if(isset($data["FLAG_ACTIVE_FICHA"]) && $data["FLAG_ACTIVE_FICHA"] == "0"){
			$this->db->where("TB_FICHA.FLAG_ACTIVE_FICHA",0);
		}

		if(isset($data['FLAG_TEMPORARIO']) && $data['FLAG_TEMPORARIO'] == 0){
			$this->db->where("TB_FICHA.FLAG_TEMPORARIO",0);
		}

		if(isset($data['NOME_FICHA']) && $data['NOME_FICHA'] != ""){
			$words = explode(' ',trim(str_replace('  ',' ',$data['NOME_FICHA'])));
			foreach ($words as $word) {
				$this->db->like("TB_FICHA.NOME_FICHA",$word);
			}
			unset($data['NOME_FICHA']);
		}

		if(isset($data['ID_APROVACAO_FICHA']) && is_array($data['ID_APROVACAO_FICHA'])){
			$this->db->where_in("TB_FICHA.ID_APROVACAO_FICHA",$data['ID_APROVACAO_FICHA']);
			unset($data['ID_APROVACAO_FICHA']);
		}

		if( !empty($data['CATEGORIAS']) ){
			$this->db->where_in("TB_FICHA.ID_CATEGORIA",$data['CATEGORIAS']);
		}

		if( !empty($data['CODIGO_REGIONAL']) ){
			$this->db->where("FC.CODIGO",$data['CODIGO_REGIONAL']);
		}

		if( !empty($data['REGIOES']) ){
			$this->db->where_in('FC.ID_REGIAO',$data['REGIOES'])
				->where('FC.CODIGO != "" AND FC.CODIGO IS NOT NULL');
		}

		if( !empty($data['CODIGOS_BARRA']) && is_array($data['CODIGOS_BARRA']) ){
			$codigos = array();
			foreach($data['CODIGOS_BARRA'] as $codigo){
				if(!empty($codigo)){
					$codigos[] = $codigo;
				}
			}
			if(!empty($codigos)){
				$this->db->where_in('TB_FICHA.COD_BARRAS_FICHA',$codigos);
			}
		}
		
		if(isset($data['TIPO_FICHA']) && is_array($data['TIPO_FICHA'])){
			//print_rr($data['TIPO_FICHA']);
			$this->db->where_in('TB_FICHA.TIPO_FICHA',$data['TIPO_FICHA']);
			unset($data['TIPO_FICHA']);
		}

		// datas de criacao
		if( !empty($data['DATA_INSERT_INICIO']) && !empty($data['DATA_INSERT_FIM']) ){
			$this->db->where("TB_FICHA.DT_INSERT_FICHA >= ", format_date_to_db( $data['DATA_INSERT_INICIO'], 2) );
			$this->db->where("TB_FICHA.DT_INSERT_FICHA <= ", format_date_to_db( $data['DATA_INSERT_FIM'], 3) );
		}
		elseif( !empty($data['DATA_INSERT_INICIO']) ){
			$this->db->where("TB_FICHA.DT_INSERT_FICHA >= ", format_date_to_db( $data['DATA_INSERT_INICIO'], 2) );
		}
		elseif( !empty($data['DATA_INSERT_FIM']) ){
			$this->db->where("TB_FICHA.DT_INSERT_FICHA <= ", format_date_to_db( $data['DATA_INSERT_FIM'], 3) );
		}

		// se for para listar somente as pendentes
		if( !empty($data['PENDENTES'])){
			//$where = '(TB_FICHA.FLAG_TEMPORARIO = 1 OR TB_FICHA.ID_APROVACAO_FICHA != 1)';
			//alteração chamado 30167
			$where = ' ((TB_FICHA.PENDENCIAS_DADOS_BASICOS != "" AND TB_FICHA.PENDENCIAS_DADOS_BASICOS IS NOT NULL) OR';
			$where .= ' (TB_FICHA.PENDENCIAS_CAMPOS != "" AND TB_FICHA.PENDENCIAS_CAMPOS IS NOT NULL))';
			$this->db->where($where);
		}

		if(!empty($order)){
	    	if(!empty($orderDirection)){
		    	$this->db->order_by($order.' '.$orderDirection);
	    	}
	    }


		$this->setWhereParams($data);

		$old = $this->db->_count_string;
		$this->db->_count_string = 'SELECT COUNT(DISTINCT TB_FICHA.ID_FICHA) AS';
		$total = $this->db->count_all_results($this->tableName, false);
		$this->db->_count_string = $old;

	    $this->db->group_by('TB_FICHA.ID_FICHA');
		$res = $this->execute($pagina, $limit, $order, $orderDirection);

		$res['total'] = $total;
		$res['nPaginas'] = ceil($total/$limit);

		return $res;

	}
	
	/**
	 * Recupera as fichas combo que contém pelo menos uma das fichas listadas em $fichasIDs
	 * 
	 * @param array $fichaIDs array de IDs de fichas para buscar
	 */
	public function listCombosIn($fichaIDs, $somenteAtivos = false){
		//se não houver fichas, retorna vazio
		if(empty($fichaIDs)){
			return array(
				'data'=>array(),
				'total'=>0,
				'pagina'=>0,
				'nPaginas'=>0,
			);
		}
		
		//recupera as fichas combos que contem as fichas recebidas no parametro
		$resFC = $this->db->from('TB_FICHA_COMBO FC')
				 ->select('FC.ID_FICHA_COMBO')
				 ->join('TB_FICHA F', 'FC.ID_FICHA_PRODUTO = F.ID_FICHA')
				 ->where_in('FC.ID_FICHA_PRODUTO', $fichaIDs)
				 ->get()
				 ->result_array();
		
		 //recupera um array com os ids
		$combosIDs = getValoresChave($resFC,'ID_FICHA_COMBO');
		
		//se não houver combos, retorna vazio
		if(empty($combosIDs)){
			return array(
				'data'=>array(),
				'total'=>0,
				'pagina'=>0,
				'nPaginas'=>0,
			);
		}
		
		//zera a query do active record
		$this->db->flush_cache();
		
		//monta a query das combos
		$this->db->select(	' *,' .
							' TB_FICHA.ID_FICHA,(' .
							' SELECT NOME_USUARIO FROM TB_HISTORICO_FICHA ' .
							' JOIN TB_USUARIO ON TB_USUARIO.ID_USUARIO = TB_HISTORICO_FICHA.ID_USUARIO' .
							' WHERE TB_HISTORICO_FICHA.ID_FICHA = TB_FICHA.ID_FICHA' .
							' ORDER BY DT_INSERT_HISTORICO DESC' .
							' LIMIT 1) AS USER_APROVACAO '
						 );

		//$this->db->join('TB_SEGMENTO SE', 'SE.ID_SEGMENTO = TB_FICHA.ID_SEGMENTO','left');
		$this->db->join('TB_APROVACAO_FICHA', 'TB_APROVACAO_FICHA.ID_APROVACAO_FICHA = TB_FICHA.ID_APROVACAO_FICHA','left');
		$this->db->join('TB_OBJETO_FICHA OF', 'TB_FICHA.ID_FICHA = OF.ID_FICHA and OF.PRINCIPAL = 1','left');
		$this->db->join('TB_OBJETO O', 'O.ID_OBJETO = OF.ID_OBJETO','left');
		$this->db->join('TB_CATEGORIA', 'TB_FICHA.ID_CATEGORIA = TB_CATEGORIA.ID_CATEGORIA','LEFT');
		$this->db->join('TB_SUBCATEGORIA', 'TB_FICHA.ID_SUBCATEGORIA = TB_SUBCATEGORIA.ID_SUBCATEGORIA','LEFT');
		$this->db->join('TB_FABRICANTE', 'TB_FICHA.ID_FABRICANTE = TB_FABRICANTE.ID_FABRICANTE','LEFT');
		$this->db->join('TB_FICHA_CODIGO FC', 'TB_FICHA.ID_FICHA = FC.ID_FICHA','LEFT');
		$this->db->join('TB_MARCA', 'TB_FICHA.ID_MARCA = TB_MARCA.ID_MARCA','LEFT');
		$this->db->join('TB_CLIENTE', 'TB_FICHA.ID_CLIENTE = TB_CLIENTE.ID_CLIENTE');
		
		//puxa somente fichas de categorias e sub ativas
		$this->db->where("TB_CATEGORIA.FLAG_ACTIVE_CATEGORIA",1);
		$this->db->where("TB_SUBCATEGORIA.FLAG_ACTIVE_SUBCATEGORIA",1);
		
		//só traz combos 
		$this->db->where("TB_FICHA.TIPO_FICHA",'COMBO');
		
		if($somenteAtivos){
			$this->db->where("TB_FICHA.FLAG_ACTIVE_FICHA", 1);
		}
		//traz apenas combos ativos???
		//$this->db->where("TB_FICHA.FLAG_ACTIVE_FICHA",1);
		
		//cujos ids estejam na lista de combos encontrados
		$this->db->where_in("TB_FICHA.ID_FICHA",$combosIDs);
		
		//conta o numero real de fichas
		$old = $this->db->_count_string;
		$this->db->_count_string = 'SELECT COUNT(DISTINCT TB_FICHA.ID_FICHA) AS';
		$total = $this->db->count_all_results($this->tableName, false);
		$this->db->_count_string = $old;
		
		//agrupa após a contagem
	    $this->db->group_by('TB_FICHA.ID_FICHA');
	    $this->db->order_by('TB_FICHA.FLAG_ACTIVE_FICHA DESC');
	    $this->db->order_by('TB_FICHA.NOME_FICHA ASC');
		$res = $this->execute();
		
		$res['total'] = $total;

		return $res;

	}

	/**
	 * Retorna o objeto principal de uma ficha
	 * @author juliano.polito
	 * @param $id id da ficha
	 * @return array
	 */
	function getMainObjectByFichaId($id){
		$rs = $this->db->where('ID_FICHA',$id)
			->join('TB_OBJETO O','O.ID_OBJETO = OF.ID_OBJETO')
			->where('PRINCIPAL',1)
			->get('TB_OBJETO_FICHA OF');

		return $rs->row_array();
	}



	function getById($id){

		$rs = $this->db
			->join('TB_SUBCATEGORIA S', 'S.ID_SUBCATEGORIA=T.ID_SUBCATEGORIA')
			->join('TB_CATEGORIA C', 'C.ID_CATEGORIA=S.ID_CATEGORIA')
			->join('TB_CLIENTE CL', 'CL.ID_CLIENTE = T.ID_CLIENTE')
			->join('TB_FABRICANTE F', 'F.ID_FABRICANTE = T.ID_FABRICANTE','LEFT')
			->join('TB_MARCA M', 'M.ID_MARCA = T.ID_MARCA','LEFT')
			->join('TB_USUARIO U', 'U.ID_USUARIO = T.ID_USUARIO','LEFT')
			->where('T.ID_FICHA',$id)
			->get($this->tableName.' T')
			->row_array();
//echo $this->db->last_query();die;
		return $rs;

	}



	function getAllByAprovacao($aprovacao)
	{
		if($this->session->userdata[USUARIO]['ID_GROUP']==$this->config->item('grupo_categoria'))
		{
			$data = $this->db->query('SELECT ID_CATEGORIA FROM TB_USER_CATEGORIA WHERE ID_USER = "'.$this->session->userdata[USUARIO]['ID_USER'].'"');
			if($data->num_rows()>0)
			{
				$lista_categorias = 'SELECT ID_CATEGORIA FROM TB_USER_CATEGORIA WHERE ID_USER = "'.$this->session->userdata[USUARIO]['ID_USER'].'"';
			}else{
				$lista_categorias = 'SELECT ID_CATEGORIA FROM TB_CATEGORIA';
			}

			$rs = $this->db->query($lista_categorias);

			$categorias = $rs->result_array();

			foreach($categorias as $item)
			{
				$categ[] = $item['ID_CATEGORIA'];
			}

			$this->db->where_in('ID_CATEGORIA',$categ);


		}
		$this->db->where('ID_APROVACAO_FICHA', $aprovacao);

		$this->db->order_by('DT_INSERT_FICHA', 'DESC');

		$this->db->limit(5);

		$rs = $this->db->get('TB_FICHA');

		if ( $rs->num_rows() > 0 )
			return $rs->result_array();
		else
			return false;
	}
	function getAllByUser($user)
	{
		$this->db->where('ID_USER', $user);

		$this->db->order_by('DT_INSERT_FICHA', 'DESC');


		$this->db->limit(5);

		$rs = $this->db->get('TB_FICHA');

		if ( $rs->num_rows() > 0 )
			return $rs->result_array();
		else
			return false;
	}
	function getByLike($dados, $pagina = 0,$limit=null,$id_carrinho=null)
	{

		$sql = ' SELECT *,TB_FICHA.ID_FICHA AS ID_FICHA,' .
					'(
							SELECT TB_OBJ_FICHA.ID_OBJ_FICHA
							FROM TB_OBJ_FICHA
							WHERE TB_OBJ_FICHA.ID_FICHA = TB_FICHA.ID_FICHA
							AND TB_OBJ_FICHA.FLAG_MAIN_OBJ_FICHA =1
							LIMIT 1' .
					') AS "ID_OBJ_FICHA",';
		$sql.= ' ( 							' .
				'			SELECT TB_OBJ_FICHA.THUMB_OBJ_FICHA
							FROM TB_OBJ_FICHA
							WHERE TB_OBJ_FICHA.ID_FICHA = TB_FICHA.ID_FICHA
							AND TB_OBJ_FICHA.FLAG_MAIN_OBJ_FICHA =1
							LIMIT 1
				 ) AS "THUMB_OBJ_FICHA"';
		$sql.= ' FROM TB_FICHA';
		$sql.= ' LEFT JOIN TB_APROVACAO_FICHA ON TB_APROVACAO_FICHA.ID_APROVACAO_FICHA = TB_FICHA.ID_APROVACAO_FICHA';
		$sql.= ' LEFT JOIN TB_FABRICANTE ON TB_FABRICANTE.ID_FABRICANTE = TB_FICHA.ID_FABRICANTE';
		$sql.= ' LEFT JOIN TB_MARCA ON TB_MARCA.ID_MARCA = TB_FICHA.ID_MARCA';
		$sql.= ' LEFT JOIN TB_CATEGORIA ON TB_CATEGORIA.ID_CATEGORIA = TB_FICHA.ID_CATEGORIA';
		$sql.= ' WHERE 1 = 1';

		$sql.= ' AND TB_CATEGORIA.FLAG_ACTIVE_CATEGORIA = 1';
		$sql.= ' AND TB_SUBCATEGORIA.FLAG_ACTIVE_SUBCATEGORIA = 1';
		
		if(isset($dados['ID_APROVACAO_FICHA'])&&$dados['ID_APROVACAO_FICHA']!=null)
		{
			$sql.= ' AND TB_APROVACAO_FICHA.ID_APROVACAO_FICHA = "'.$dados['ID_APROVACAO_FICHA'].'"';
		}

		if ($dados['tipo_pesquisa'] == 'simples')
		{
			if ($dados['COD_FICHA'] != null)
				$sql.= ' AND TB_FICHA.COD_FICHA = "'.$dados['COD_FICHA'].'"';

		}
		else
		{
			if ($dados['COD_FICHA'] != null)
				$sql.= ' AND TB_FICHA.COD_FICHA LIKE "%'.$dados['COD_FICHA'].'%"';

			if ($dados['NOME_FICHA'] != null)
				$sql.= ' AND TB_FICHA.NOME_FICHA LIKE "%'.$dados['NOME_FICHA'].'%"';

			if ($dados['FABRICANTE_FICHA'] != null)
				$sql.= ' AND TB_FABRICANTE.ID_FABRICANTE = '.$dados['FABRICANTE_FICHA'].' ';

			if ($dados['MARCA_FICHA'] != null)
				$sql.= ' AND TB_FICHA.ID_MARCA = '.$dados['MARCA_FICHA'].' ';

			if (isset($dados['FLAG_FULL_FICHA']) && $dados['FLAG_FULL_FICHA'] != null)
				$sql.= ' AND TB_FICHA.FLAG_FULL_FICHA = '.$dados['FLAG_FULL_FICHA'].'';

			if($id_carrinho!=null)
				$categorias = $this->categoria->getAllByUserAndCarrinhoInCategoria($id_carrinho);

			if(isset($categorias)&&is_array($categorias))
			{
				foreach($categorias as $item)
				{
					$valores[] = $item['ID_CATEGORIA'];
				}
			}

			if ($dados['ID_CATEGORIA'] != null && $dados['ID_CATEGORIA'] != 0)
			{
				$sql.= ' AND TB_FICHA.ID_CATEGORIA = "'.$dados['ID_CATEGORIA'].'"';
			}else{
				if(isset($valores))
				{
					$sql.= ' AND TB_FICHA.ID_CATEGORIA IN('.join(',',$valores).') ';
				}
			}

			if ($dados['ID_SUBCATEGORIA'] != null && $dados['ID_SUBCATEGORIA'] != 0)
				$sql.= ' AND TB_FICHA.ID_SUBCATEGORIA = "'.$dados['ID_SUBCATEGORIA'].'"';

			if (isset($dados['TIPO_COMBO_FICHA'])&&$dados['TIPO_COMBO_FICHA'] != null && $dados['TIPO_COMBO_FICHA'] != 0)
				$sql.= ' AND TB_FICHA.TIPO_COMBO_FICHA = "'.$dados['TIPO_COMBO_FICHA'].'"';

			if (isset($dados['aprovado']) || isset($dados['em_aprovacao']) || isset($dados['reprovado']))
			{
				$sql.= ' AND TB_FICHA.ID_APROVACAO_FICHA IN ' . '(';

				if (isset($dados['aprovado']))
					$sql.= $dados['aprovado'] . ',';

				if (isset($dados['em_aprovacao']))
					$sql.= $dados['em_aprovacao'] . ',';

				if (isset($dados['reprovado']))
					$sql.= $dados['reprovado'] . ',';

				$sql.= ')' ;

				$sql = str_replace(",)",')',$sql);
			}

		}



		return $this->executeQuery($sql,$pagina, $limit, 'TB_FICHA.DT_INSERT_FICHA', 'DESC');
	}
	
	
	function getByLikeInCarrinho($dados, $pagina = 0, $limit) {

		$this->db->select('*, TB_FICHA.ID_FICHA AS ID_FICHA');

		if ($dados['tipo_pesquisa'] == 'simples') {
			if ($dados['COD_FICHA'] != null)
				$this->db->where('COD_FICHA', $dados['COD_FICHA']);

		} else {
			if ($dados['COD_FICHA'] != null)
				$this->db->where('COD_FICHA', $dados['COD_FICHA']);

			if ($dados['NOME_FICHA'] != null)
				$this->db->like('NOME_FICHA', $dados['NOME_FICHA']);

			if ($dados['FABRICANTE_FICHA'] != null)
				$this->db->like('FABRICANTE_FICHA', $dados['FABRICANTE_FICHA']);

			if ($dados['MARCA_FICHA'] != null)
				$this->db->like('MARCA_FICHA', $dados['MARCA_FICHA']);

			if ($dados['ID_CATEGORIA'] != null && $dados['ID_CATEGORIA'] != 0)
				$this->db->where('ID_CATEGORIA', $dados['ID_CATEGORIA']);

			if ($dados['ID_SUBCATEGORIA'] != null && $dados['ID_SUBCATEGORIA'] != 0)
				$this->db->where('ID_SUBCATEGORIA', $dados['ID_SUBCATEGORIA']);

		}


		$this->db->join('TB_OBJ_FICHA', '(TB_FICHA.ID_FICHA = TB_OBJ_FICHA.ID_FICHA and TB_OBJ_FICHA.FLAG_MAIN_OBJ_FICHA = 1)');

		return $this->execute($pagina, $limit, 'TB_FICHA.DT_INSERT_FICHA', 'DESC');
	}

	function getHistoryByFichaId($id=null)
	{
		if($id!=null)
		{
			$sql  = ' SELECT * FROM TB_HISTORICO_FICHA ' .
					' INNER JOIN TB_USUARIO ON TB_USUARIO.ID_USUARIO = TB_HISTORICO_FICHA.ID_USUARIO ' .
					' WHERE ID_FICHA = '.$id;

			$data = $this->db->query($sql);

			if($data->num_rows()>0)
				return $data->result_array();
			return false;
		}
		return false;
	}
	function getByCarrinhoAndPaginacao($id_paginacao)
	{

		$this->db->select(	' *,' .
							' TB_FICHA.ID_FICHA,(' .
							' SELECT NAME_USER FROM TB_HISTORICO_FICHA ' .
							' JOIN TB_USER ON TB_USER.ID_USER = TB_HISTORICO_FICHA.ID_USER' .
							' WHERE TB_HISTORICO_FICHA.ID_FICHA = TB_FICHA.ID_FICHA' .
							' ORDER BY DT_INSERT_HISTORICO DESC' .
							' LIMIT 1) AS USER_APROVACAO '
						 );

		$this->db->join('TB_F_C_P', 'TB_F_C_P.ID_FICHA = TB_FICHA.ID_FICHA','left');
		$this->db->join('TB_CARRINHO', 'TB_CARRINHO.ID_CARRINHO = TB_F_C_P.ID_CARRINHO','left');
		$this->db->join('TB_OBJ_FICHA', 'TB_FICHA.ID_FICHA = TB_OBJ_FICHA.ID_FICHA and TB_OBJ_FICHA.FLAG_MAIN_OBJ_FICHA = 1','left');
		$this->db->where('TB_CARRINHO.ID_PAGINACAO', $id_paginacao);

		$rs = $this->db->get('TB_FICHA');

			if($rs->num_rows()>0)
				return $rs->result_array();
			return false;
	}

	function export_relatorio($filtro = null){
		$this->load->dbutil();

		$strCVS = "";

		$strSQL = "";
		$strSQL .= " SELECT ";
		$strSQL .= " 	COD_FICHA AS Codigo, ";
		$strSQL .= " 	NOME_FICHA AS Nome, ";
		$strSQL .= " 	DESC_CATEGORIA AS Categoria, ";
		$strSQL .= " 	DESC_SUBCATEGORIA AS 'Sub-categoria', ";
		$strSQL .= " 	FABRICANTE_FICHA AS Fabricante, ";
		$strSQL .= " 	MARCA_FICHA AS Marca, ";
		$strSQL .= " 	MODELO_FICHA AS Modelo, ";
		$strSQL .= " 	NAME_USER AS Usuario ";
		$strSQL .= " FROM ";
		$strSQL .= " 	TB_FICHA ";
		$strSQL .= " LEFT JOIN ";
		$strSQL .= " 	TB_USER ON TB_USER.ID_USER = TB_FICHA.ID_USER ";
		$strSQL .= " LEFT JOIN ";
		$strSQL .= " 	TB_CATEGORIA ON TB_CATEGORIA.ID_CATEGORIA = TB_FICHA.ID_CATEGORIA ";
		$strSQL .= " LEFT JOIN ";
		$strSQL .= " 	TB_SUBCATEGORIA ON TB_SUBCATEGORIA.ID_SUBCATEGORIA = TB_FICHA.ID_SUBCATEGORIA ";

		if(isset($filtro) && is_array($filtro)){
			$count = 1;
			foreach($filtro as $index => $value){
				$index_temp = split(":", $index);
				$index = trim($index_temp[0]);
				$comparer_str = trim($index_temp[1]);
				$formate_str = trim($index_temp[2]);

				switch($comparer_str){
					case "major":
						$comparer = ">";
						break;
					case "majorequal":
						$comparer = ">=";
						break;
					case "minor":
						$comparer = "<";
						break;
					case "minorequal":
						$comparer = "<=";
						break;
					default:
						$comparer = "=";
						break;
				}

				switch($formate_str){
					case "date":
						$value = format_date_to_db($value, 2);
						break;
					default:
						break;
				}

				$strSQL .= ($count == 1) ? " WHERE " : " AND ";
				$strSQL .= " 	(". $index ." ". $comparer ." '". $value ."') ";

				$count++;
			}
		}

		$obj_recordset = $this->db->query($strSQL);
		if($obj_recordset && $obj_recordset->num_rows > 0){
			$strCVS .= $this->dbutil->csv_from_result($obj_recordset, ";", "\r\n");
		}
		else{
			$strCVS .= "Nenhuma ficha encontrada"."\n";
		}

		return $strCVS;
	}
	function getByCarrinhoAndPeriodo($id,$id_carrinho=0)
	{
		$sql = 'SELECT * FROM TB_F_C_P
				JOIN TB_FICHA ON TB_F_C_P.ID_FICHA = TB_FICHA.ID_FICHA
				JOIN TB_CARRINHO ON TB_F_C_P.ID_CARRINHO = TB_CARRINHO.ID_CARRINHO
				WHERE  TB_CARRINHO.DT_INSERT_CARRINHO  >= (SELECT DATE_FORMAT(DATE_SUB(CURDATE(),INTERVAL 7 DAY),"%Y-%m-%d %H:%i:%s"))
				AND TB_FICHA.ID_FICHA = '.$id;

		if($id_carrinho!=0)
		{
			$sql.= ' AND TB_CARRINHO.ID_CARRINHO <> '.$id_carrinho;
		}

		$rs = $this->db->query($sql);

		if ($rs->num_rows()>0)
			return $rs->result_array();
		return false;
	}

	function getByLikeByCarrinho($dados, $pagina = 0, $limit) {
			$sql = '';
			$sql .= ' SELECT *, TB_F_C_P.ID_F_C_P,' .
					' ( ' .
						' SELECT  THUMB_OBJ_FICHA' .
						' FROM TB_OBJ_FICHA ' .
						' WHERE TB_OBJ_FICHA.ID_FICHA = TB_FICHA.ID_FICHA' .
						' AND TB_OBJ_FICHA.FLAG_MAIN_OBJ_FICHA = 1' .
						' LIMIT 1 ' .
					' ) ' .
					' AS THUMB_OBJ_FICHA ' .
					' FROM (`TB_F_C_P`) ' .
					' JOIN `TB_FICHA` ON TB_F_C_P.ID_FICHA = TB_FICHA.ID_FICHA ' .
					' LEFT JOIN `TB_APROVACAO_FICHA` ON TB_APROVACAO_FICHA.ID_APROVACAO_FICHA = TB_FICHA.ID_APROVACAO_FICHA ' .
					' LEFT JOIN `TB_PRICING` ON TB_F_C_P.ID_F_C_P = TB_PRICING.ID_F_C_P	' .
					' INNER JOIN `TB_CARRINHO` ON TB_F_C_P.ID_CARRINHO = TB_CARRINHO.ID_CARRINHO' .
					' WHERE TB_APROVACAO_FICHA.ID_APROVACAO_FICHA = 1 ' .
					' AND TB_F_C_P.ID_CARRINHO IS NOT NULL '.
					' AND TB_CARRINHO.FLAG_SITUACAO_CARRINHO = 1 '.
					' AND TB_CARRINHO.DT_EXPIRA_CARRINHO > "'. date("Y-m-d H:i:s") .'"' ;


		if ($dados['DESC_CARRINHO'] != null)
			$sql .= ' AND TB_CARRINHO.DESC_CARRINHO LIKE "%' . $dados['DESC_CARRINHO'] . '%"';

		if ($dados['COD_FICHA'] != null)
			$sql .= ' AND COD_FICHA = '. $dados['COD_FICHA'] ;

		if ($dados['NOME_FICHA'] != null)
			$sql .= ' AND NOME_FICHA LIKE "%' . $dados['NOME_FICHA'] . '%"';

		if ($dados['ID_CATEGORIA'] != null && $dados['ID_CATEGORIA'] != 0)
			$sql .= ' AND ID_CATEGORIA = '.  $dados['ID_CATEGORIA'];

		if ($dados['ID_SUBCATEGORIA'] != null && $dados['ID_SUBCATEGORIA'] != 0)
			$sql .= ' AND ID_SUBCATEGORIA = ' . $dados['ID_SUBCATEGORIA'];

		if (isset($dados['TIPO_COMBO_FICHA'])&&$dados['TIPO_COMBO_FICHA'] != null && $dados['TIPO_COMBO_FICHA'] != 0)
			$sql .= ' AND TIPO_COMBO_FICHA =' . $dados['TIPO_COMBO_FICHA'];


		return $this->executeQuery($sql,$pagina, $limit, 'TB_FICHA.DT_INSERT_FICHA', 'DESC');

	}


	function getByIdEmail($id)
	{
		$this->db->select(	'TB_FICHA.TIPO_FICHA AS Tipo, ' .
							'TB_FICHA.NOME_FICHA AS Nome, ' .
							'TB_FICHA.COD_FICHA AS Sku, ' .
							'TB_CATEGORIA.DESC_CATEGORIA AS Categoria, ' .
							'TB_SUBCATEGORIA.DESC_SUBCATEGORIA AS SubCategoria, ' .
							'TB_FICHA.FABRICANTE_FICHA AS Fabricante,' .
							'TB_FICHA.MARCA_FICHA AS Marca, ' .
							'TB_FICHA.MODELO_FICHA AS Modelo,  ' .
							'TB_FICHA.TITULO_ENC_FICHA AS Titulo, ' .
							'TB_FICHA.SUB_ENC_FICHA AS Subtitulo, ' .
							'TB_FICHA.PV_ENC_FICHA AS PVAnuncio, ' .
							'TB_FICHA.TITULO_MO_FICHA AS MegaOferta, ' .
							'TB_FICHA.SUB_MO_FICHA AS SubMegaOferta,' .
							'TB_FICHA.PV_MO_FICHA AS PVMegaOferta, ' .
							'TB_FICHA.TITULO_CAT_FICHA AS Catalogo, ' .
							'TB_FICHA.SUB_CAT_FICHA AS SubCatalogo, ' .
							'TB_FICHA.CODIGOBARRA_CAT_FICHA AS Barra, ' .
							'TB_FICHA.PV_CAT_FICHA AS PvCatalogo');

		$this->db->join('TB_CATEGORIA', 'TB_CATEGORIA.ID_CATEGORIA=TB_FICHA.ID_CATEGORIA','LEFT');
		$this->db->join('TB_SUBCATEGORIA', 'TB_SUBCATEGORIA.ID_SUBCATEGORIA=TB_FICHA.ID_SUBCATEGORIA','LEFT');
		$this->db->where('ID_FICHA', $id);
		if($rs = $this->db->get('TB_FICHA'))
			return $rs->row_array();
		else
			return false;
	}

	function getFichaByCarrinho($id_carrinho)
	{
		$sql = ' SELECT * FROM TB_FICHA ' .
			   ' INNER JOIN TB_F_C_P ON TB_F_C_P.ID_FICHA = TB_FICHA.ID_FICHA' .
			   ' INNER JOIN TB_OBJ_FICHA ON TB_OBJ_FICHA.ID_FICHA = TB_FICHA.ID_FICHA' .
			   ' LEFT JOIN TB_MARCA ON TB_MARCA.ID_MARCA = TB_FICHA.ID_MARCA' .
			   ' WHERE TB_F_C_P.ID_CARRINHO = "'.$id_carrinho.'"';
		$data = $this->db->query($sql);
		if($data->num_rows()>0)
			return $data->result_array();
		return false;
	}

	/**
	 * Retorna todas as fichas de uma pagina de paginacao
	 *
	 * @param $id_pagina ID da pagina
	 * @return DB_Result
	 */
	function getAllByPagina($id_pagina){
		$sql = "SELECT  T.* ".
		"FROM ".
  			"TB_FICHA T, TB_PAGINA P, TB_F_C_P FCP ".
		"WHERE ".
		  "P.ID_F_C_P = FCP.ID_F_C_P ".
		 "AND FCP.ID_FICHA = T.ID_FICHA ".
		 "AND P.ID_PAGINA = $id_pagina";

		return $sql;

		$data = $this->db->query($sql);
		if($data->num_rows()>0){
			return $data->result_array();
		}
		return false;
	}

	/**
	 * Recupera as fichas pelo SKU e/ou midia
	 * @author Hugo Silva
	 * @param string $sku
	 * @param int $midia
	 * @return array
	 */
	public function getBySkuMidia($sku, $midia=null){
		if(!is_null($midia)){
			$this->db->where('TB_TIPO_LAYOUT_FICHA.ID_MIDIA', $midia);
		}

		$rs = $this->db->join('TB_OBJ_FICHA','TB_OBJ_FICHA.ID_FICHA=TB_FICHA.ID_FICHA')
			->join('TB_TIPO_LAYOUT_FICHA','TB_TIPO_LAYOUT_FICHA.ID_FICHA=TB_FICHA.ID_FICHA')
			->like('FILE_OBJ_FICHA',$sku,'after')
			->where('FLAG_MAIN_OBJ_FICHA',1)
			->where('FLAG_ACTIVE_FICHA',1)
			->where('ID_APROVACAO_FICHA',1)
			->get($this->tableName);

		if($rs->num_rows() == 0){
			return array();
		}

		return $rs->result_array();
	}

	/**
	 * Recupera uma ficha da midia informada.
	 * Se ela nao tiver layout, pega outra ficha que tenha layout
	 *
	 * @author Hugo Silva
	 * @param string $sku
	 * @param int $midia
	 * @return array
	 */
	public function getFichaComLayout($sku, $midia){

		$rs = $this->db->join('TB_OBJ_FICHA','TB_OBJ_FICHA.ID_FICHA=TB_FICHA.ID_FICHA')
			->join('TB_TIPO_LAYOUT_FICHA','TB_TIPO_LAYOUT_FICHA.ID_FICHA=TB_FICHA.ID_FICHA')
			->join('TB_ELEM_FICHA','TB_TIPO_LAYOUT_FICHA.ID_TIPO_LAYOUT_FICHA=TB_ELEM_FICHA.ID_TIPO_LAYOUT_FICHA')
			//->like('FILE_OBJ_FICHA',$sku,'after')
			->where('COD_FICHA',$sku)
			->where('TB_TIPO_LAYOUT_FICHA.ID_MIDIA', $midia)
			->where('FLAG_MAIN_OBJ_FICHA',1)
			->where('FLAG_ACTIVE_FICHA',1)
			->where('ID_APROVACAO_FICHA',1)
			->limit(1)
			->get($this->tableName);

		// Se nao encontrou a ficha na midia especificada
		if($rs->num_rows() == 0){
			// procuramos por outra midia (a primeira que vier pela frente)
			$rs = $this->db->join('TB_OBJ_FICHA','TB_OBJ_FICHA.ID_FICHA=TB_FICHA.ID_FICHA')
				->join('TB_TIPO_LAYOUT_FICHA','TB_TIPO_LAYOUT_FICHA.ID_FICHA=TB_FICHA.ID_FICHA')
				->join('TB_ELEM_FICHA','TB_TIPO_LAYOUT_FICHA.ID_TIPO_LAYOUT_FICHA=TB_ELEM_FICHA.ID_TIPO_LAYOUT_FICHA')
				//->like('FILE_OBJ_FICHA',$sku,'after')
				->where('COD_FICHA',$sku)
				->where_not_in('TB_TIPO_LAYOUT_FICHA.ID_MIDIA', array($midia))
				->where('FLAG_MAIN_OBJ_FICHA',1)
				->where('FLAG_ACTIVE_FICHA',1)
				->where('ID_APROVACAO_FICHA',1)
				->limit(1)
				->get($this->tableName);

			// se nao encontrou nada
			if($rs->num_rows() == 0){
				return array();
			}
		}

		// retornamos a ficha encontrada
		return $rs->row_array();
	}

	/**
	 * Recupera fichas com segmento
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $cliente Codigo do cliente
	 * @param string $sku Codigo da ficha (SKU do produto na ficha)
	 * @param int $segmento Codigo do segmento
	 * @return array
	 */
	public function getFichaComSegmento($cliente, $sku, $segmento){

		throw new Exception('Este metodo foi depreciado pois nao existe mais segmento');

		$rs = $this->db
			->where('COD_FICHA',$sku)
			->where('ID_SEGMENTO', $segmento)
			->where('ID_CLIENTE', $cliente)
			->where('FLAG_ACTIVE_FICHA',1)
			->limit(1)
			->get($this->tableName);

		//LogUtil::log('getFichaComSegmento.txt',$this->db->last_query().PHP_EOL);

		// Se nao encontrou a ficha no segmento especificado
		if($rs->num_rows() == 0){
			// procuramos por outro segmento (a primeira que vier pela frente)
			$rs = $this->db
				->where('COD_FICHA',$sku)
				->where_not_in('ID_SEGMENTO', array($segmento))
				->where('ID_CLIENTE', $cliente)
				->where('FLAG_ACTIVE_FICHA',1)
				->limit(1)
				->get($this->tableName);

			// se nao encontrou nada
			if($rs->num_rows() == 0){
				return array();
			}
		}

		// retornamos a ficha encontrada
		return $rs->row_array();
	}

	/**
	 * Recupera fichas por cliente e codigo
	 *
	 * @author juliano.polito
	 * @link http://www.247id.com.br
	 * @param int $cliente Codigo do cliente
	 * @param string $sku Codigo da ficha (SKU do produto na ficha)
	 * @param int $segmento Codigo do segmento
	 * @return array
	 */
	public function getFichaClienteProduto($cliente, $sku){
		$rs = $this->db
			->where('COD_BARRAS_FICHA',$sku)
			->where('ID_CLIENTE', $cliente)
			->where('FLAG_ACTIVE_FICHA',1)
			->limit(1)
			->get($this->tableName);

		// Se nao encontrou a ficha no segmento especificado
		if($rs->num_rows() == 0){
			return array();
		}

		// retornamos a ficha encontrada
		return $rs->row_array();
	}

	/**
	 * recupera as fichas de um cliente atraves de uma lista de codigos de produtos
	 *
	 * 15/07/2010 - Recupera a regiao da ficha para exibir o codigo regional
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $cliente
	 * @param array $listSKU
	 * @param int $idRegiao Codigo da regiao para obter o codigo regional
	 * @return array
	 */
	public function getFichasClienteProdutos($cliente, array $listSKU, $idRegiao = null){
		$this->db
			->where_in('COD_BARRAS_FICHA',$listSKU)
			->where('ID_CLIENTE', $cliente);

		//Alterado na PA 5842 para trazer fichas inativas.
		//A ficha serah avisada ao usuario que estah inativa
		//->where('FLAG_ACTIVE_FICHA',1);

		if( !is_null($idRegiao) ){
			$this->db
				->select('FC.CODIGO AS CODIGO_REGIONAL')
				->join(
					'TB_FICHA_CODIGO FC'
					, 'TB_FICHA.ID_FICHA = FC.ID_FICHA AND FC.ID_REGIAO = '.$idRegiao
					, 'LEFT'
				);
		}

		$rs = $this->db
			->select('TB_FICHA.*')
			->get($this->tableName);

		$list = $rs->result_array();
		$result = array();

		foreach($list as $item){
			$result[ $item['COD_BARRAS_FICHA'] ] = $item;
		}

		return $result;
	}

	/**
	 * Verifica se uma ficha tem elementos
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idFicha Codigo da ficha
	 * @param int $idSegmento Codigo do segmento da ficha
	 * @return boolean
	 */
	public function hasElementos($idFicha, $idSegmento = null){
		/////////////////////////////////
		// contamos os objetos
		/////////////////////////////////
		$this->db->join('TB_OBJETO_FICHA O','O.ID_FICHA = T.ID_FICHA')
			->where('T.ID_FICHA', $idFicha);

		// se for por segmento
		if(isset($idSegmento)){
			$this->db->where('T.ID_SEGMENTO',$idSegmento);
		}

		$totalObjetos = $this->db->get($this->tableName.' T')->num_rows();

		/////////////////////////////////
		// contamos os campos
		/////////////////////////////////
		$this->db->join('TB_CAMPO_FICHA C','C.ID_FICHA = T.ID_FICHA')
			->where('T.ID_FICHA', $idFicha);

		// se for por segmento
		if(isset($idSegmento)){
			$this->db->where('T.ID_SEGMENTO',$idSegmento);
		}

		$totalCampos = $this->db->get($this->tableName.' T')->num_rows();

		// retorna true se houverem objetos e campos
		return $totalCampos > 0 || $totalObjetos > 0;
	}

	/**
	 * Verifica em uma lista de codigos de fichas, quais possuem objetos
	 *
	 * @author Hugo Ferreira da Silva
	 * @param array $list codigos das fichas
	 * @param int $idsegmento codigo do segmento
	 * @return array
	 */
	public function hasElementosList(array $list, $idsegmento = null){

		foreach($list as $idx => $code){
			$list[ $idx ] = "'" . addslashes($code) . "'";
		}

		$query = "(
			SELECT T.* FROM TB_FICHA T
			INNER JOIN TB_OBJETO_FICHA O ON (T.ID_FICHA = O.ID_FICHA)
				WHERE T.COD_BARRAS_FICHA IN (" . implode(', ', $list) . ")

		) UNION (

			SELECT T.* FROM TB_FICHA T
			INNER JOIN TB_CAMPO_FICHA O ON (T.ID_FICHA = O.ID_FICHA)
				WHERE T.COD_BARRAS_FICHA IN (" . implode(', ', $list) . ")
		)";

		$result = array();

		foreach($this->db->query($query)->result_array() as $item){
			$result[ $item['ID_FICHA'] ] = $item;
		}

		return $result;
	}

	/**
	 * Cria um snapshot dos dados da ficha para gravar no banco de dados
	 *
	 * @author Hugo Silva
	 * @param int $idficha
	 * @param int $idusuario
	 * @return void
	 */
	public function snapshot($idficha, $idusuario){
		// TB_FICHA_HISTORICO
		// ID_FICHA_HISTORICO, ID_USUARIO, ID_FICHA, DATA_FICHA_HISTORICO, LOG_FICHA_HISTORICO

		if(is_null($idficha)){
			return;
		}

		// pega os dados da ficha
		$ficha = $this->getById($idficha);
		$ficha['objetos'] = $this->objeto_ficha->getByFicha($idficha);
		$ficha['campos'] = $this->campo_ficha->getByFicha($idficha);

		// monta o objeto serializado
		$obj = serialize($ficha);

		// monta o array para poder gravar o log
		$data['ID_FICHA'] = $idficha;
		$data['ID_USUARIO'] = $idusuario;
		$data['DATA_FICHA_HISTORICO'] = date('Y-m-d H:i:s');
		$data['LOG_FICHA_HISTORICO'] = $obj;

		// efetua a insercao
		$this->db
			->insert('TB_FICHA_HISTORICO', $data);

	}

	/**
	 * faz o historico do ficha
	 *
	 * @author Hugo Silva
	 * @param int $idficha
	 * @return array Lista das alteracoes da ficha
	 */
	public function getHistory($idficha){
		$log = $this->db
			->select('TB_FICHA_HISTORICO.*, U.*, C.DESC_CLIENTE, A.DESC_AGENCIA')
			->join('TB_USUARIO U','U.ID_USUARIO = TB_FICHA_HISTORICO.ID_USUARIO')
			->join('TB_GRUPO G','G.ID_GRUPO = U.ID_GRUPO')
			->join('TB_CLIENTE C','C.ID_CLIENTE = G.ID_CLIENTE','LEFT')
			->join('TB_AGENCIA A','A.ID_AGENCIA = G.ID_AGENCIA','LEFT')
			->where('ID_FICHA', $idficha)
			->order_by('ID_FICHA_HISTORICO DESC')
			->limit(1)
			->get('TB_FICHA_HISTORICO')
			->row_array();

		if( empty($log) ){
			return array();
		}

		$ficha = $this->getById($idficha);
		$obj = @unserialize($log['LOG_FICHA_HISTORICO']);

		// se deu problema na desserializacao
		if( $obj === false ){
			$obj = array(
				'campos' => array(),
				'objetos' => array()
			);
		}

		$user = $this->usuario->getById($log['ID_USUARIO']);

		// alteracoes das fichas
		foreach($ficha as $key => $val){

			if( preg_match('@^(ID_|FLAG_)@', $key)){
				continue;
			}

			if(isset($obj[$key]) && $ficha[$key] != $obj[$key]){
				$alteracoes[$key]['novo'] = $ficha[$key];
				$alteracoes[$key]['antigo'] = $obj[$key];
			}
		}

		$alteracoes['campos_novos'] = $this->campo_ficha->getByFicha($idficha);
		$alteracoes['objetos_novos'] = $this->objeto_ficha->getByFicha($idficha);


		$log['alteracoes'] = $alteracoes;
		$log['campos'] = $obj['campos'];
		$log['objetos'] = $obj['objetos'];

		return $log;
	}


	/**
	 * Recupera os codigos regionais de uma ficha
	 * @author Hugo Ferreira da Silva
	 * @param int $idficha Codigo da ficha
	 * @return array
	 */
	public function getCodigosRegionais($idficha){
		$rs = $this->db
			->from('TB_FICHA_CODIGO FC')
			->select('FC.*, R.*')
			->join('TB_FICHA F','F.ID_FICHA = FC.ID_FICHA')
			->join('TB_REGIAO R','R.ID_REGIAO = FC.ID_REGIAO')
			->join('TB_CLIENTE C','C.ID_CLIENTE = R.ID_CLIENTE AND F.ID_CLIENTE = C.ID_CLIENTE')
			->where('FC.ID_FICHA', $idficha)
			->get()
			->result_array();

		return $rs;
	}

	/**
	 * Grava os codigos reginais da ficha
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idficha Codigo da ficha
	 * @param array $codigos Codigos regionais
	 * @return void
	 */
	public function salvarCodigosRegionais($idficha, array $codigos){
		foreach($codigos as $regiao => $codigo){
			$rs = $this->db
				->where('ID_FICHA', $idficha)
				->where('ID_REGIAO', $regiao)
				->get('TB_FICHA_CODIGO')
				->row_array();

			if( empty($rs) ){
				$rs['ID_FICHA'] = $idficha;
				$rs['ID_REGIAO'] = $regiao;
				$rs['CODIGO'] = $codigo;
				$this->db->insert('TB_FICHA_CODIGO', $rs);

			} else {
				$rs['CODIGO'] = $codigo;
				$this->db
					->where('ID_FICHA', $idficha)
					->where('ID_REGIAO', $regiao)
					->update('TB_FICHA_CODIGO', $rs);
			}
		}
	}

	/**
	 * Valida os codigos regionais de uma ficha
	 *
	 * Procuramos tambem pelo codigo do cliente por que
	 * dois clientes diferentes podem ter o mesmo codigo regional.
	 *
	 * E usado mais em caso de novas fichas
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idficha   Codigo da ficha
	 * @param int $idcliente Codigo do cliente
	 * @param array $codigos
	 * @return array Lista de erros encontrados
	 */
	public function validarCodigosRegionais($idficha, $idcliente, array $codigos){
		$erros = array();

		$idficha = sprintf('%d', $idficha);
		$idcliente = sprintf('%d', $idcliente);

		foreach($codigos as $idregiao => $codigo){
			// se o codigo estiver vazio
			if( empty($codigo) ){
				// deixa passar - nao validamos codigos vazios
				continue;
			}
			$rs = $this->db
				->from('TB_FICHA F')
				->join('TB_FICHA_CODIGO FC','FC.ID_FICHA = F.ID_FICHA')
				->where('F.ID_FICHA != ', $idficha)
				->where('F.ID_CLIENTE', $idcliente)
				->where('FC.ID_REGIAO', $idregiao)
				->where('CODIGO', $codigo)
				->get()
				->result_array();

			if( !empty($rs) ){
				$regiao = $this->regiao->getById($idregiao);
				$erros[] = 'O código '.$codigo.' da região '.$regiao['NOME_REGIAO'] . ' já está cadastrado para outra ficha';
			}
		}

		return $erros;
	}

	/**
	 * Recupera fichas por uma lista de codigos de ficha
	 *
	 * Os codigos das fichas sao os ID's criados internamente no sistema.
	 * Tambem ja faz um join com as tabelas
	 * - Categoria
	 * - Subcategoria
	 * - Objeto
	 *
	 * @author Hugo Ferreira da Silva
	 * @param array $lista Lista de codigos de fichas
	 * @return array Lista de fichas encontradas
	 */
	public function getByListaId(array $lista){
		$result = $this->db
			->select('F.*, C.*, S.*, OF.ID_OBJETO, OF.PRINCIPAL, O.*, F.ID_SUBCATEGORIA AS ID_SUBCATEGORIA')
			->from($this->tableName . ' F')
			->join('TB_CATEGORIA C','C.ID_CATEGORIA = F.ID_CATEGORIA')
			->join('TB_SUBCATEGORIA S','S.ID_SUBCATEGORIA = F.ID_SUBCATEGORIA')
			->join('TB_OBJETO_FICHA OF','OF.ID_FICHA = F.ID_FICHA','LEFT')
			->join('TB_OBJETO O','OF.ID_OBJETO = O.ID_OBJETO','LEFT')
			->where_in('F.ID_FICHA', $lista)
			->group_by('F.ID_FICHA')
			->get()
			->result_array();

		return $result;
	}

	/**
	 * Recupera as fichas-produto associadas a uma ficha-combo
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idFichaCombo Codigo da ficha combo no sistema
	 * @return array Lista de fichas-produto encontrada
	 */
	public function getFichasInCombo($idFichaCombo){
		$rs = $this->db
			->from($this->tableName . ' F')
			->join('TB_CATEGORIA C','C.ID_CATEGORIA = F.ID_CATEGORIA')
			->join('TB_SUBCATEGORIA S','S.ID_SUBCATEGORIA = F.ID_SUBCATEGORIA')
			->join('TB_FICHA_COMBO FC','FC.ID_FICHA_PRODUTO = F.ID_FICHA')
			->where('FC.ID_FICHA_COMBO', $idFichaCombo)
			->get()
			->result_array();

		return $rs;
	}
	
	/**
	 * Recupera as FICHAS-PAI associadas a uma ficha
	 *
	 * @author Sidnei Tertuliano Junior
	 * @param int $idFichaFilha ID da ficha filha
	 * @return array Lista de fichas-produto encontrada
	 */
	public function getFichasPai($idFichaFilha){
		$rs = $this->db
			->from($this->tableName . ' F')
			->join('TB_CATEGORIA C','C.ID_CATEGORIA = F.ID_CATEGORIA')
			->join('TB_SUBCATEGORIA S','S.ID_SUBCATEGORIA = F.ID_SUBCATEGORIA')
			->join('TB_FICHA_PAI_FILHA FPF','FPF.ID_FICHA_PAI = F.ID_FICHA')
			->where('FPF.ID_FICHA_FILHA', $idFichaFilha)
			->get()
			->result_array();

		return $rs;
	}
	
	/**
	 * Recupera as FICHAS-PAI associadas a uma ficha
	 *
	 * @author Sidnei Tertuliano Junior
	 * @param int $idFichaFilha ID da ficha filha
	 * @return array Lista de fichas-produto encontrada
	 */
	public function getFichasFilha($idFichaPai){
		$rs = $this->db
			->from($this->tableName . ' F')
			->join('TB_CATEGORIA C','C.ID_CATEGORIA = F.ID_CATEGORIA')
			->join('TB_SUBCATEGORIA S','S.ID_SUBCATEGORIA = F.ID_SUBCATEGORIA')
			->join('TB_FICHA_PAI_FILHA FPF','FPF.ID_FICHA_FILHA = F.ID_FICHA')
			->join('TB_APROVACAO_FICHA AF' ,'F.ID_APROVACAO_FICHA = AF.ID_APROVACAO_FICHA')
			->join('TB_OBJETO_FICHA OF' ,'OF.ID_FICHA = F.ID_FICHA', 'LEFT')
			->join('TB_OBJETO O' ,'O.ID_OBJETO = OF.ID_OBJETO', 'LEFT')
			->where('FPF.ID_FICHA_PAI', $idFichaPai)
			->get()
			->result_array();

		return $rs;
	}

	/**
	 * Recupera os codigos de fichas a partir de uma ficha combo e objeto
	 *
	 * Utilizado principalmente na edicao de fichas-combo
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idFichaCombo Codigo da ficha combo
	 * @param int $idObjeto Codigo do objeto desejado
	 * @return array Lista de codigos de ficha
	 */
	public function getCodigosByComboObjeto( $idFichaCombo, $idObjeto ){
		$rs = $this->db
			->from($this->tableName . ' F')
			->join('TB_OBJETO_FICHA OF','OF.ID_FICHA = F.ID_FICHA')
			->join('TB_OBJETO O','O.ID_OBJETO = OF.ID_OBJETO')
			->join('TB_FICHA_COMBO FC','FC.ID_FICHA_PRODUTO = F.ID_FICHA')
			->where('FC.ID_FICHA_COMBO', $idFichaCombo)
			->where('O.ID_OBJETO', $idObjeto)
			->get()
			->result_array();

		$codes = getValoresChave($rs, 'ID_FICHA');

		return $codes;
	}

	public function removeFichasInCombo($idFichaCombo){
		$this->db
			->from('TB_FICHA_COMBO')
			->where('ID_FICHA_COMBO', $idFichaCombo)
			->delete();
	}

	public function addFichaInCombo($idFichaCombo, $idFichaProduto){
		$data['ID_FICHA_COMBO'] = $idFichaCombo;
		$data['ID_FICHA_PRODUTO'] = $idFichaProduto;

		$this->db->insert('TB_FICHA_COMBO', $data);
	}

	public function getFichasComboByFichaProduto($idFichaProduto){
		$rs = $this->db
			->select('*, F.*')
			->from($this->tableName . ' F')
			->join('TB_CATEGORIA C','C.ID_CATEGORIA = F.ID_CATEGORIA')
			->join('TB_SUBCATEGORIA S','S.ID_SUBCATEGORIA = F.ID_SUBCATEGORIA')
			->join('TB_FICHA_COMBO FC','FC.ID_FICHA_COMBO = F.ID_FICHA')
			->join('TB_APROVACAO_FICHA A','A.ID_APROVACAO_FICHA = F.ID_APROVACAO_FICHA','LEFT')
			->join('TB_OBJETO_FICHA OF','OF.ID_FICHA = F.ID_FICHA AND OF.PRINCIPAL = 1','LEFT')
			->join('TB_OBJETO O','O.ID_OBJETO = OF.ID_OBJETO','LEFT')

			->where('FC.ID_FICHA_PRODUTO', $idFichaProduto)
			->get()
			->result_array();

		return $rs;
	}

	public function adicionarObjeto( $idFichaCombo, $idObjeto ){
		$rs = $this->db
			->where('ID_FICHA', $idFichaCombo)
			->where('ID_OBJETO', $idObjeto)
			->get('TB_OBJETO_FICHA')
			->result_array();

		if( empty($rs) ){
			$data['ID_FICHA'] = $idFichaCombo;
			$data['ID_OBJETO'] = $idObjeto;
			$data['PRINCIPAL'] = 0;

			$this->objeto_ficha->save($data);
		}
	}

	/**
	 * Retorna todas as fichas que foram encontradas pelos filtros encontrados
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $filters
	 * @return array
	 */
	public function relatorioFichas(array $filters){
		// se informou a agencia
		if( !empty($filters['ID_AGENCIA']) ){
			// pega os clientes da agencia
			$clientes = $this->cliente->getByAgencia($filters['ID_AGENCIA']);
			// lista de codigos de clientes
			$codes = array(0);
			// para cada cliente
			foreach($clientes as $item){
				// coloca o codigo na lista
				$codes[] = $item['ID_CLIENTE'];
			}
			// filtra pelos codigos dos clientes
			$this->db->where_in('TB_FICHA.ID_CLIENTE', $codes);
		}

		if(isset($filters["FLAG_ACTIVE_FICHA"]) && $filters["FLAG_ACTIVE_FICHA"] == 0){
			$this->db->where("TB_FICHA.FLAG_ACTIVE_FICHA",0);
		}

		if(isset($filters['FLAG_TEMPORARIO']) && $filters['FLAG_TEMPORARIO'] == 0){
			$this->db->where("TB_FICHA.FLAG_TEMPORARIO",0);
		}

		if(isset($filters['NOME_FICHA'])){
			$words = explode(' ',trim(str_replace('  ',' ',$filters['NOME_FICHA'])));
			foreach ($words as $word) {
				$this->db->like("TB_FICHA.NOME_FICHA",$word);
			}
			unset($filters['NOME_FICHA']);
		}

		if(isset($filters['ID_APROVACAO_FICHA']) && is_array($filters['ID_APROVACAO_FICHA'])){
			$this->db->where_in("TB_FICHA.ID_APROVACAO_FICHA",$filters['ID_APROVACAO_FICHA']);
			unset($filters['ID_APROVACAO_FICHA']);
		}

		if( !empty($filters['CATEGORIAS']) ){
			$this->db->where_in("TB_FICHA.ID_CATEGORIA",$filters['CATEGORIAS']);
		}

		if( !empty($filters['CODIGO_REGIONAL']) ){
			$this->db->where('FC.CODIGO',$filters['CODIGO_REGIONAL']);
		}

		if( !empty($filters['CODIGOS_BARRA']) && is_array($filters['CODIGOS_BARRA']) ){
			$codigos = array();
			foreach($filters['CODIGOS_BARRA'] as $codigo){
				if(!empty($codigo)){
					$codigos[] = $codigo;
				}
			}
			if(!empty($codigos)){
				$this->db->where_in('TB_FICHA.COD_BARRAS_FICHA',$codigos);
			}
		}

		// se for para listar somente as pendentes
		if( !empty($filters['PENDENTES']) ){
			$where = '(TB_FICHA.FLAG_TEMPORARIO = 1 OR TB_FICHA.ID_APROVACAO_FICHA != 1)';
			$this->db->where($where);
		}

		if(!empty($order)){
	    	if(!empty($orderDirection)){
		    	$this->db->order_by($order.' '.$orderDirection);
	    	}
	    }

	    $this->setWhereParams($filters);

		$this->db->from($this->tableName)
			->join('TB_APROVACAO_FICHA', 'TB_APROVACAO_FICHA.ID_APROVACAO_FICHA = TB_FICHA.ID_APROVACAO_FICHA','LEFT')
			->join('TB_CATEGORIA', 'TB_FICHA.ID_CATEGORIA = TB_CATEGORIA.ID_CATEGORIA','LEFT')
			->join('TB_SUBCATEGORIA', 'TB_FICHA.ID_SUBCATEGORIA = TB_SUBCATEGORIA.ID_SUBCATEGORIA','LEFT')
			->join('TB_FICHA_CODIGO FC', 'TB_FICHA.ID_FICHA = FC.ID_FICHA','LEFT')
			->join('TB_CLIENTE', 'TB_FICHA.ID_CLIENTE = TB_CLIENTE.ID_CLIENTE')
			->group_by('TB_FICHA.ID_FICHA')
			->order_by('DESC_CLIENTE, NOME_FICHA ASC');


	    // vamos fazer a consulta
	    $rs = $this->db
	    	->get()
	    	->result_array();

	    // resultado final
	    $result = array();

	    // troca a chave do resultado
	    $new = array();
	    foreach($rs as $row){
	    	$new[$row['ID_FICHA']] = $row;
	    }

	    $rs = $new;
	    unset($new);

	    // codigos de fichas
	    $fichas = array_keys($rs);

	    // vamos pegar todos os codigos regionais
	    while( !empty($fichas) ){
	    	$list = array_splice($fichas, 0, 500);

	    	$codigos = $this->db
	    		->from('TB_FICHA_CODIGO')
	    		->join('TB_REGIAO R', 'TB_FICHA_CODIGO.ID_REGIAO = R.ID_REGIAO')
	    		->where_in('ID_FICHA', $list)
	    		->get()
	    		->result_array();

	    	foreach($codigos as $row){
	    		$rs[ $row['ID_FICHA'] ]['CODIGOS_REGIONAIS'][ $row['NOME_REGIAO'] ] = $row['CODIGO'];
	    	}
	    }

	    // codigos de fichas
	    $fichas = array_keys($rs);

	    // vamos pegar todos os codigos regionais
	    while( !empty($fichas) ){
	    	$list = array_splice($fichas, 0, 500);

	    	$codigos = $this->db
	    		->from('TB_CAMPO_FICHA')
	    		->where_in('ID_FICHA', $list)
	    		->get()
	    		->result_array();

	    	foreach($codigos as $row){
	    		$rs[ $row['ID_FICHA'] ]['CAMPOS'][ $row['LABEL_CAMPO_FICHA'] ] = $row['VALOR_CAMPO_FICHA'];
	    	}
	    }

	    // codigos de fichas
	    $fichas = array_keys($rs);

	    // vamos pegar todos os codigos regionais
	    while( !empty($fichas) ){
	    	$list = array_splice($fichas, 0, 500);

	    	$codigos = $this->db
	    		->from('TB_OBJETO_FICHA OF')
	    		->join('TB_OBJETO O','OF.ID_OBJETO = O.ID_OBJETO')
	    		->select('FILE_ID, ID_FICHA')
	    		->where_in('ID_FICHA', $list)
	    		->order_by('PRINCIPAL DESC')
	    		->get()
	    		->result_array();

	    	foreach($codigos as $row){
	    		$rs[ $row['ID_FICHA'] ]['OBJETOS'][] = $row['FILE_ID'];
	    	}
	    }

	    return $rs;
	}

	/**
	 * Ativa/Inativa as fichas pelo prazo de validade
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function atualizarStatusPorValidade(){

		// ativando as fichas que estao dentro do periodo de validade
		// PA 8001 -> Ativa somente as fichas que comecam a valer a partir de hoje (DATA_INICIO) 
		$this->db
			->where('DATA_INICIO IS NOT NULL AND CURRENT_DATE = DATA_INICIO')
			->set('FLAG_ACTIVE_FICHA', 1)
			->update($this->tableName);

		// inativando as fichas fora do periodo
		$this->db
			->where('(DATA_INICIO IS NOT NULL AND CURRENT_DATE < DATA_INICIO)')
			->or_where('(DATA_VALIDADE IS NOT NULL AND CURRENT_DATE > DATA_VALIDADE)')
			->set('FLAG_ACTIVE_FICHA', 0)
			->update($this->tableName);
			
		//Verifica as fichas que nao sao utilizadas a x dias parametrizado por cliente 		
		$sql  = 'SELECT  F.ID_FICHA, ';
		$sql .= 		' C.ID_CLIENTE, ';
		$sql .= 		' C.QTD_DIAS_INATIVACAO_FICHA, ';
		$sql .= 		' F.DATA_INICIO, ';
		$sql .= 		' F.DT_INSERT_FICHA, ';
		$sql .= 		' F.FLAG_ACTIVE_FICHA, ';
		$sql .= 		' E.DT_CADASTRO_EXCEL, ';
		$sql .= 		' EI.ID_EXCEL_ITEM, ';
		$sql .= 		' DATE_SUB(CURRENT_DATE, INTERVAL C.QTD_DIAS_INATIVACAO_FICHA DAY) ';
		$sql .= ' FROM TB_FICHA F ';
		$sql .= ' JOIN  TB_CLIENTE C ON C.ID_CLIENTE = F.ID_CLIENTE ';
		//O LEFT JOIN abaixo eh necessario para saber se as fichas nunca foram usadas
		$sql .= ' LEFT JOIN  TB_EXCEL_ITEM EI ON EI.ID_FICHA = F.ID_FICHA ';
		$sql .= ' LEFT JOIN  TB_EXCEL E ON E.ID_EXCEL = EI.ID_EXCEL ';
		//Fichas que nunca foram usadas
		$sql .= ' WHERE  ( (  E.DT_CADASTRO_EXCEL IS NULL ) OR ';
		//Fichas já usadas porem nao sao usadas a x dias cadastrados para inativacao
        $sql .= ' 			( E.DT_CADASTRO_EXCEL IS NOT NULL AND '; 
    	$sql .= '             NOT EXISTS( SELECT * FROM TB_EXCEL_ITEM EIS '; 
   		$sql .= '						  JOIN TB_EXCEL ES ON ES.ID_EXCEL = EIS.ID_EXCEL ';
   		$sql .= '						  WHERE EIS.ID_FICHA = F.ID_FICHA ';
   		$sql .= '						  AND   ES.DT_CADASTRO_EXCEL > DATE_SUB(CURRENT_DATE, INTERVAL C.QTD_DIAS_INATIVACAO_FICHA DAY) ) '; 
		$sql .= '						 ) ';
		$sql .= '			) ';
		//Que tenham o campo qtd de dias de fichas não usadas para inativação
		$sql .= ' AND      C.QTD_DIAS_INATIVACAO_FICHA > 0 ';
		$sql .= ' AND      C.QTD_DIAS_INATIVACAO_FICHA IS NOT NULL ';
		//Fichas com data inicio ou que foram inseridas (dt_insert_ficha) x dias antes da qtd de dias para inativação no cadastro do cliente
		$sql .= ' AND      (    ( F.DATA_INICIO     IS NOT NULL AND F.DATA_INICIO     < DATE_SUB(CURRENT_DATE, INTERVAL C.QTD_DIAS_INATIVACAO_FICHA DAY) ) ';
		$sql .= ' 			 OR ( F.DT_INSERT_FICHA IS NOT NULL AND F.DT_INSERT_FICHA < DATE_SUB(CURRENT_DATE, INTERVAL C.QTD_DIAS_INATIVACAO_FICHA DAY) ) ';
		$sql .= '           ) ';
		//Somente fichas ativas
		$sql .= ' AND      F.FLAG_ACTIVE_FICHA = 1 ';
		//Agrupado por ficha (só precisamos de um registro da ficha para inativá-la)
		$sql .= ' GROUP BY F.ID_FICHA ';

		$fichasNaoUsadas = $this->db->query($sql)
									->result_array();
		
		//Percorre array de fichas encontrados para inativacao
		foreach ($fichasNaoUsadas as $ficha) {
			
			//Inativando a ficha
			$this->db
				->where('ID_FICHA', $ficha['ID_FICHA'])
				->set('FLAG_ACTIVE_FICHA', 0)
				->update($this->tableName);
		}
	}
		
	/**
	 * Remove as fichas filhas de uma ficha pai
	 *
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function removeFichasFilhas($idFichaPai){
		$this->db
			->from('TB_FICHA_PAI_FILHA')
			->where('ID_FICHA_PAI', $idFichaPai)
			->delete();
	}
	
	/**
	 * Remove as fichas pais de uma ficha filha
	 *
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function removeFichasPais($idFichaFilha){
		$this->db
			->from('TB_FICHA_PAI_FILHA')
			->where('ID_FICHA_FILHA', $idFichaFilha)
			->delete();
	}
	
	/**
	 * Adiciona fichas filhas e fichas pais
	 *
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function addFichasPaisFilhas($idFichaPai, $idFichaFilha){
		$data['ID_FICHA_PAI'] = $idFichaPai;
		$data['ID_FICHA_FILHA'] = $idFichaFilha;

		$this->db->insert('TB_FICHA_PAI_FILHA', $data);
	}
	
	/**
	 * Recupera ficha por cliente/codigo de barras
	 *
	 * @author juliano.polito
	 * @param $idCliente
	 * @param $codFicha
	 * @return array
	 */
	public function getByClienteCodigoBarras($idCliente, $codBarras){
		$rs = $this->db->where('ID_CLIENTE',$idCliente)
			->where('COD_BARRAS_FICHA = ', $codBarras)
			->get($this->tableName);

		return $rs->row_array();
	}
	
	/**
	 * recupera as fichas de um cliente atraves de uma lista de codigos de produtos
	 *
	 * 15/07/2010 - Recupera a regiao da ficha para exibir o codigo regional
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $cliente
	 * @param array $listSKU
	 * @param int $idRegiao Codigo da regiao para obter o codigo regional
	 * @return array
	 */
	public function getSubichasClienteProdutos($cliente, array $listSKU, $idRegiao = null, $codBarrasFichaPai = null){
		$this->db
			->where_in('COD_BARRAS_FICHA',$listSKU)
			->where('ID_CLIENTE', $cliente)
			->where('TIPO_FICHA', 'SUBFICHA');

		//Alterado na PA 5842 para trazer fichas inativas.
		//A ficha serah avisada ao usuario que estah inativa
		//->where('FLAG_ACTIVE_FICHA',1);

		if( !is_null($idRegiao) ){
			$this->db
				->select('FC.CODIGO AS CODIGO_REGIONAL')
				->join(
					'TB_FICHA_CODIGO FC'
					, 'TB_FICHA.ID_FICHA = FC.ID_FICHA AND FC.ID_REGIAO = '.$idRegiao
					, 'LEFT'
				);
		}
		
		$this->db->join('TB_SUBFICHA', 'TB_FICHA.ID_FICHA = TB_SUBFICHA.ID_FICHA2');

		$rs = $this->db
			->select('TB_FICHA.*')
			->get($this->tableName);

		$list = $rs->result_array();
		$result = array();

		foreach($list as $item){
			$result[ $item['COD_BARRAS_FICHA'] ] = $item;
		}

		return $result;
	}
	
	// VERIFICA SE EXISTE ALGUMA FICHA PRODUTO JA CADASTRADA NO SISTEMA COM O DETERMINADO CIP
	public function getCipExistente($cip){
		$rs = $this->db
			->where('COD_CIP_FICHA', $cip)
			->where('TIPO_FICHA', 'PRODUTO')
			->where('FLAG_ACTIVE_FICHA', 1)
			->get($this->tableName);
			
		if($rs->num_rows() >= 1){
			return $rs->row_array();
		}
		return false;
	}
	
	// EXCLUIR FICHA
	public function excluir($idFicha){
		$this->db->delete($this->tableName, array('ID_FICHA' => $idFicha));
		$this->db->delete('TB_EXCEL_ITEM', array('ID_FICHA' => $idFicha));
		$this->db->delete('TB_EXCEL_ITEM', array('ID_FICHA_PAI' => $idFicha));		
		$this->db->delete('TB_SUBFICHA', array('ID_FICHA1' => $idFicha));
		$this->db->delete('TB_SUBFICHA', array('ID_FICHA2' => $idFicha));	
	}
	
	// EXCLUIR FICHA
	public function excluirDoJob($idFicha){
		$this->db->delete('TB_EXCEL_ITEM', array('ID_FICHA' => $idFicha));
		$this->db->delete('TB_EXCEL_ITEM', array('ID_FICHA_PAI' => $idFicha));	
	}
}





