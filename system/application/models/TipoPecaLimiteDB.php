<?php
class TipoPecaLimiteDB  extends GenericModel {
	### START
	protected function _initialize(){
		$this->addField('ID_TIPO_PECA','int','',1,1);
		$this->addField('ID_SUBCATEGORIA','int','',1,1);
		$this->addField('LIMITE_MINIMO','int','',1,0);
		$this->addField('LIMITE_MAXIMO','int','',1,0);
	}
	### END

	var $tableName = 'TB_TIPO_PECA_LIMITE';

	/**
	 * Construtor 
	 * 
	 * @author Esdras Eduardo
	 * @link http://www.247id.com.br
	 * @return TipoPecaLimiteDB
	 */
	function __construct(){
		parent::GenericModel();
	}
	
	/**
	 * Lista os itens cadastrados
	 * @author Esdras Eduardo
	 * @link http://www.247id.com.br
	 * @param array $filters array com os filtros
	 * @param int $page numero da pagina
	 * @param int $limit numero de itens por pagina
	 * @return array
	 */
	public function listItems($filters, $page=0, $limit=5){
		$this->setWhereParams($filters);
		$this->db->join('TB_SUBCATEGORIA', 'TB_SUBCATEGORIA.ID_SUBCATEGORIA = TB_TIPO_PECA_LIMITE.ID_SUBCATEGORIA');
		$this->db->join('TB_CATEGORIA', 'TB_CATEGORIA.ID_CATEGORIA = TB_SUBCATEGORIA.ID_CATEGORIA');
		return $this->execute($page, $limit);
	}
	
	/**
	 * Pega por subcategoria
	 * @author Sidnei Tertuliano
	 * @link http://www.247id.com.br
	 * @param int $idSubcategoria id da subcategoria
	 * @return array
	 */	
	public function getByPecaAndSubcategoria($idSubcategoria, $idTipoPeca){
		$rs = $this->db
			->join('TB_SUBCATEGORIA', 'TB_SUBCATEGORIA.ID_SUBCATEGORIA = ' .$this->tableName. '.ID_SUBCATEGORIA')
			->join('TB_CATEGORIA', 'TB_CATEGORIA.ID_CATEGORIA = TB_SUBCATEGORIA.ID_CATEGORIA')
			->where($this->tableName . '.ID_SUBCATEGORIA', $idSubcategoria)
			->where($this->tableName . '.ID_TIPO_PECA', $idTipoPeca)
			->get($this->tableName)
			->result_array();
			
		return $rs;
	}
	
	
	public function deleteAll( Array $params ){
		$this->db->delete($this->tableName, $params);
	}
}

