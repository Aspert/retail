<?php
/**
 * Model que representa a tabela TB_REGRA_EMAIL_PROCESSO_ETAPA_USUARIO
 * @author Esdras Eduardo
 * @link http://www.247id.com.br
 */
class RegraEmailProcessoEtapaUsuarioDB extends GenericModel {
	
	### START
	protected function _initialize(){
		$this->addField('ID_REGRA_EMAIL_PROCESSO_ETAPA','int','',11,1);
		$this->addField('ID_PROCESSO_ETAPA','int','',11,1);
		$this->addField('ID_USUARIO','int','',11,1);
	}
	### END

	var $tableName = 'TB_REGRA_EMAIL_PROCESSO_ETAPA_USUARIO';
	
	/**
	 * Construtor
	 * 
	 * @author Esdras Eduardo
	 * @link http://www.247id.com.br
	 * @return RegraEmailProcessoEtapaUsuarioDB
	 */
	function __construct(){
		parent::GenericModel();
	}
	
	/**
	 * Encontrar todas as regras de email de processo
	 * 
	 * @author esdras.filho
	 * @param Number $idRegraProcesso
	 * @param Number $idProcessoEtapa
	 */
	public function findAllByRegraProcesso( $idRegraProcesso, $idProcessoEtapa ){
		$resultado = $this->db->from($this->tableName)->select('ID_USUARIO')
			->where('ID_REGRA_EMAIL_PROCESSO_ETAPA', $idRegraProcesso)
			->where('ID_PROCESSO_ETAPA', $idProcessoEtapa)
			->get();
		return $resultado->result_array();
	}
	
	public function findHorasByRegraProcesso( $idRegraProcesso, $idProcessoEtapa ){
		$resultado = $this->db->from($this->tableName)->select('QTD_HORAS_ANTES_ENVIO')
			->where('ID_REGRA_EMAIL_PROCESSO_ETAPA', $idRegraProcesso)
			->where('ID_PROCESSO_ETAPA', $idProcessoEtapa)
			->get()
			->result_array();
			if( count($resultado) > 0 ){
				return $resultado[0]['QTD_HORAS_ANTES_ENVIO'];			
			} else {
				return '';				
			}
	}
	
	/**
	 * Recupera a lista de codigos de grupos pela regra de email
	 * @author Hugo Ferreira da Silva
	 * @param int $idRegra
	 * @return array
	 */
	public function getGruposByRegra($idRegra){
		$rs = $this->db->from($this->tableName.' RU')
			->select('U.ID_GRUPO, RU.ID_REGRA_EMAIL_PROCESSO_ETAPA')
			->join('TB_USUARIO U','U.ID_USUARIO = RU.ID_USUARIO')
			->where('RU.ID_PROCESSO_ETAPA', $idRegra)
			->group_by('RU.ID_REGRA_EMAIL_PROCESSO_ETAPA, U.ID_GRUPO')
			->get();
		$lista = array();
		foreach($rs->result_array() as $item){
			if( !isset($lista[$item['ID_REGRA_EMAIL_PROCESSO_ETAPA']]) ){
				$lista[$item['ID_REGRA_EMAIL_PROCESSO_ETAPA']] = Array();
			}
			$lista[$item['ID_REGRA_EMAIL_PROCESSO_ETAPA']][] = $item['ID_GRUPO'];
		}
			
		return $lista;
	}
	
	/**
	 * Recupera os usuarios de um grupo / regra processo
	 * 
	 * Sempre retorna os usuarios do grupo.
	 * A regra e para saber se o campo "SELECIONADO" vira como 1 OU 0
	 * 
	 * @author Esdras Eduardo
	 * @param int $idgrupo
	 * @param int $idregra
	 * @return array
	 */
	public function getUsuariosGrupoRegra($idEtapa, $idGrupo, $idRegra){
		$rs = $this->db->from('TB_USUARIO U')
			->join($this->tableName . ' RU', 'U.ID_USUARIO = RU.ID_USUARIO AND RU.ID_REGRA_EMAIL_PROCESSO_ETAPA = '.$idRegra, 'LEFT')
			->join('TB_GRUPO G','G.ID_GRUPO = U.ID_GRUPO')
			->select('U.*, G.*')
			->where('G.ID_GRUPO', $idGrupo)
			->where('U.STATUS_USUARIO', 1)
			->group_by('U.ID_USUARIO')
			->order_by('U.NOME_USUARIO')
			->get()
			->result_array();
			
		foreach( $rs as &$usuario ){
			$arr_where = Array();
			$arr_where['ID_USUARIO'] = $usuario['ID_USUARIO'];
			$arr_where['ID_PROCESSO_ETAPA'] = $idEtapa;
			$arr_where['ID_REGRA_EMAIL_PROCESSO_ETAPA'] = $idRegra;
			$count = $this->db->from($this->tableName)->where($arr_where)->count_all_results();
			$usuario['SELECIONADO'] = ($count > 0)?'1':'0';
		}
			
		return $rs;
	}
	
	public function removeAll( $idProcessoEtapa ){
		$resultado = $this->db->from($this->tableName)->where('ID_PROCESSO_ETAPA', $idProcessoEtapa)->get()->result_array();
		$this->db->where('ID_PROCESSO_ETAPA', $idProcessoEtapa)->delete($this->tableName);
		return $resultado;
	}
	
	/**
	 * Salvar os usuario da regra de email do processo
	 * 
	 * @author esdras.filho
	 * @param int $idRegraEmailProcessoEtapa
	 * @param int $idProcessoEtapa
	 * @param int $idUsuario
	 * @return int
	 */
	public function saveUsuario( $idRegraEmailProcessoEtapa, $idProcessoEtapa, $idUsuario, $hora ){
		if( $hora == '' ){
			$hora = '00:00';
		}
		$this->db->set('ID_REGRA_EMAIL_PROCESSO_ETAPA', $idRegraEmailProcessoEtapa)
		->set('QTD_HORAS_ANTES_ENVIO', $hora)
		->set('ID_PROCESSO_ETAPA', $idProcessoEtapa)
		->set('ID_USUARIO', $idUsuario)
		->insert($this->tableName);
		return $this->db->insert_id();
	}
	
	/**
	 * Recupera todos os usuarios de uma regra de email de processo.
	 * A consulta já faz join com todas as tabelas relacionadas a GACP
	 * @author esdras.filho
	 * @param $idRegra Id da regra de email
	 * @return array
	 */
	function getByRegraEmail($idRegra, $idProcessoEtapa){
		$rs = $this->db->from($this->tableName.' RU')
			->select('RU.*,U.*,RE.*')
			->join('TB_REGRA_EMAIL_PROCESSO_ETAPA RE','RE.ID_REGRA_EMAIL_PROCESSO_ETAPA = RU.ID_REGRA_EMAIL_PROCESSO_ETAPA')
			->join('TB_USUARIO U','U.ID_USUARIO = RU.ID_USUARIO AND U.STATUS_USUARIO = 1')
			->where('RU.ID_REGRA_EMAIL_PROCESSO_ETAPA', $idRegra)
			->where('RU.ID_PROCESSO_ETAPA', $idProcessoEtapa)
			->get()
			->result_array();
		return $rs;
	}	
}