<?php
class Pedido_FichaDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_PEDIDO_FICHA','int','',1,1);
		$this->addField('COD_SKU_PEDIDO_FICHA','string','',6,0);
		$this->addField('IMAGE_PEDIDO_FICHA','string','',11,0);
		$this->addField('BRIEFING_PEDIDO_FICHA','blob','',26,0);
		$this->addField('DT_INSERT_PEDIDO_FICHA','timestamp','',19,0);
		$this->addField('ID_SOLICITANTE','int','',2,0);
		$this->addField('ID_EXECUTOR','int','',2,0);
		$this->addField('STATUS_PEDIDO_FICHA','string','',8,0);
		$this->addField('ID_FICHA','int','',2,0);
	}
	### END

	var $tableName = 'TB_PEDIDO_FICHA';
	
	function getByLike($dados, $pagina = 0, $limit=null) 
	{
		$sql = 'SELECT * FROM '.$this->tableName.' WHERE 1 = 1';
		
		if (isset($dados['COD_SKU_PEDIDO_FICHA'])&&$dados['COD_SKU_PEDIDO_FICHA'] != null)
		{
			$sql.=' AND COD_SKU_PEDIDO_FICHA LIKE "%'.$dados['COD_SKU_PEDIDO_FICHA'].'%"';
		}
		
		if (isset($dados['STATUS_PEDIDO_FICHA']))
		{
			$sql.=' AND STATUS_PEDIDO_FICHA = "'.$dados['STATUS_PEDIDO_FICHA'].'"';
		}		

		return $this->executeQuery($sql,$pagina,$limit,'DT_INSERT_PEDIDO_FICHA','DESC');		
	}
	
	function getAll($pagina=0,$limit=12,$status='aguardando')
	{
		$sql = ' SELECT * FROM '.$this->tableName.' ' .
			   ' JOIN TB_USER ON TB_USER.ID_USER = '.$this->tableName.'.ID_SOLICITANTE' .
			   ' WHERE '.$this->tableName.'.STATUS_PEDIDO_FICHA = "'.$status.'"';
		return $this->executeQuery($sql,$pagina,$limit);
	}
	
	function getById($id=null)
	{
		if($id==null)
			return false;
		$sql = ' SELECT * FROM '.$this->tableName.' ' .
			   ' JOIN TB_USER ON TB_USER.ID_USER = '.$this->tableName.'.ID_SOLICITANTE' .
			   ' WHERE '.$this->tableName.'.ID_PEDIDO_FICHA = "'.$id.'"';
			   
		$data = $this->db->query($sql);	   
		if($data->num_rows()>0)
			return $data->row_array();
		return false;		
	}
	function finalizapedido($sku,$id_ficha,$id_executor)
	{
		$sql = 	' SELECT ID_PEDIDO_FICHA ' .
				' FROM TB_PEDIDO_FICHA ' .
				' WHERE COD_SKU_PEDIDO_FICHA = "'.$sku.'"';
		
		$data = $this->db->query($sql);
		
		if($data->num_rows()>0)
		{			
			$rows = $data->result_array();
			foreach($rows as $item)
			{
				$this->modifyState($item['ID_PEDIDO_FICHA'],'aprovado',$id_ficha,$id_executor);	
			}
			return true; 
		}	
		return false;		
	}
	function modifyState($pedido,$state='aguardando',$id_ficha,$id_executor)
	{
		$sql = 	' UPDATE TB_PEDIDO_FICHA ' .
				' SET STATUS_PEDIDO_FICHA = "'.$state.'" , ID_FICHA = "'.$id_ficha.'" , ID_EXECUTOR = "'.$id_executor.'" ' .
				' WHERE ID_PEDIDO_FICHA = '.$pedido;
		return $this->db->query($sql); 
	}
	
}	

