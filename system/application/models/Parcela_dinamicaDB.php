<?php
// Model para a Parcela Dinamica

class Parcela_dinamicaDB extends GenericModel{
	protected function _initialize(){
		$this->addField('ID_PARCELA_DINAMICA','int','',1,1);
		$this->addField('ID_CLIENTE','int','',1,1);
		$this->addField('VALOR_VISTA_1','real','',5,0);
		$this->addField('VALOR_VISTA_2','real','',5,0);
		$this->addField('PARCELAS','int','',1,0);
		$this->addField('ARREDONDA','int','',1,0);
		$this->addField('AJUSTA','int','',1,0);
	}

	var $tableName = 'TB_PARCELA_DINAMICA';

	/**
	 * Construtor
	 *
	 * @link http://www.247id.com.br
	 */
	function __construct(){
		parent::GenericModel();
	}
	
	public function joia(){
		
	}
	
	public function listItems($filters, $page=0, $limit=5){
		$this->db->join('TB_CLIENTE C','C.ID_CLIENTE = TB_PARCELA_DINAMICA.ID_CLIENTE','JOIN');
		$this->db->group_by('TB_PARCELA_DINAMICA.ID_CLIENTE');
		$this->setWhereParams($filters);
		return $this->execute($page,$limit,'DESC_CLIENTE');
	}
	
	public function listByCliente($idCliente){
		$rs = $this->db->from($this->tableName)
			->select('*')
			->join('TB_CLIENTE C','C.ID_CLIENTE = TB_PARCELA_DINAMICA.ID_CLIENTE','JOIN')
			->where($this->tableName.'.ID_CLIENTE', $idCliente)
			->order_by('VALOR_VISTA_2')
			->get()
			->result_array();
			
		return $rs;		
	}
	
	public function deleteByCliente($idCliente){
		$this->db->delete($this->tableName, array('ID_CLIENTE' => $idCliente));
	}
	
	public function getByClienteValor($idCliente, $valor){
		$rs = $this->db->select('*')
			->from($this->tableName)
			->where('ID_CLIENTE', $idCliente)
			->where('VALOR_VISTA_1 <= ', $valor)
			->where('VALOR_VISTA_2 >= ', $valor)
			->get()
			->result_array();
		
			return $rs;
	}
	
	public function calculaParcelaDinamica($idCliente, $valor=null, $money=false){
		$retorno = array();

		if($valor != null){
			$valor = number_format($valor, 2, '.', '');
			
			$rs = $this->db->select('*')
				->from($this->tableName)
				->where('ID_CLIENTE', $idCliente)
				->where('VALOR_VISTA_1 <= ', $valor)
				->where('VALOR_VISTA_2 >= ', $valor)
				->get()
				->result_array();

			if(count($rs) > 0){
				$valorParcela = number_format(($valor/$rs['0']['PARCELAS']), 2);

				if($rs['0']['ARREDONDA'] == 1){
					$ultimoDigito = substr($valorParcela, -1);
					if($ultimoDigito != 0 && $ultimoDigito != 5 && $ultimoDigito != 9){
						if(($ultimoDigito > 0) && ($ultimoDigito < 5)){
							$ultimoDigito = 0;
						}
						if(($ultimoDigito > 5) && ($ultimoDigito < 9)){
							$ultimoDigito = 5;
						}
					}
					$valorParcela = substr($valorParcela, 0, -1) . $ultimoDigito;
				}

				if($rs['0']['AJUSTA'] == 1){
					$retorno['PRECO_VISTA'] = number_format($valorParcela*$rs['0']['PARCELAS'],2);
				}
				else{
					$retorno['PRECO_VISTA'] = $valor;
				}

				$retorno['PARCELAS'] = $rs['0']['PARCELAS'];
				if($money){
					$retorno['PRESTACAO'] = money($valorParcela);
				}
				else{
					$retorno['PRESTACAO'] = $valorParcela;
				}
			}
		}

		return $retorno;
	}
}



























