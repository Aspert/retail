<?php
class Aprovacao_fichaDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_APROVACAO_FICHA','int','',1,1);
		$this->addField('DESC_APROVACAO_FICHA','string','',8,0);
		$this->addField('CHAVE','string','',20,0);
	}
	### END

	var $tableName = 'TB_APROVACAO_FICHA';  		

	/**
	 * Recupera a descricao pelo codigo
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id
	 * @return string 
	 */
	public function getDescricao($id){
		$rs = $this->db->where('ID_APROVACAO_FICHA', $id)
			->get($this->tableName)
			->row_array();
			
		return empty($rs['DESC_APROVACAO_FICHA']) ? '' : $rs['DESC_APROVACAO_FICHA'];
	}
	
	/**
	 * Recupera o registro pela chave
	 * @author Nelson Martucci Jr
	 * @param string $key nome da chave
	 * @return array Array contendo os valores do registro
	 */
	
	public function getByKey($key){
		
		$rs = $this->db->where('CHAVE',$key)->get($this->tableName);
		
		if($rs->num_rows() == 0){
			return array();
		}
		
		return $rs->row_array();
	}

	/**
	 * Recupera o ID do pela chave
	 * @author Nelson Martucci jr
	 * @param $key A chave do status
	 * @return O ID caso encontre, ou null caso não encontre
	 */
	public function getIdByKey($key){
		
		$row = $this->getByKey($key);
		
		return !empty($row['ID_APROVACAO_FICHA']) ? $row['ID_APROVACAO_FICHA'] : 0;
		
	}	
	
}	

