<?php
class TemplateDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_TEMPLATE','int','',1,1);
		$this->addField('ID_PRODUTO','int','',2,0);
		$this->addField('ID_CAMPANHA','int','',2,0);
		$this->addField('DESC_TEMPLATE','string','',8,0);
		$this->addField('DATA_CADASTRO_TEMPLATE','datetime','',19,0);
		$this->addField('LARGURA_TEMPLATE','real','',4,0);
		$this->addField('ALTURA_TEMPLATE','real','',4,0);
		$this->addField('STATUS_TEMPLATE','int','',1,0);
		$this->addField('ORIGEM_TEMPLATE','string','',3,0);
		$this->addField('TEMPLATE_EXISTS','int','',1,0);
		$this->addField('NUMERO_PAGINAS','int','',1,0);
	}
	### END

	var $tableName = 'TB_TEMPLATE';

	/**
	 * Construtor 
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return TemplateDB
	 */
	function __construct(){
		parent::GenericModel();
		
		$this->addValidation('ID_PRODUTO','requiredNumber','Informe a bandeira');
		$this->addValidation('ID_CAMPANHA','requiredNumber','Informe a campanha');
		$this->addValidation('DESC_TEMPLATE','requiredString','Informe o nome do template');
		//$this->addValidation('LARGURA_TEMPLATE','requiredNumber','Informe a largura');
		//$this->addValidation('ALTURA_TEMPLATE','requiredNumber','Informe a altura');
		$this->addValidation('ORIGEM_TEMPLATE','requiredString','Informe a origem do template');
		$this->addValidation('NUMERO_PAGINAS','requiredNumber','Informe o numero de paginas');
	}
	
	/**
	 * (non-PHPdoc)
	 * @see system/application/libraries/GenericModel#getById($id)
	 */
	public function getById($id){
		$rs = $this->db->from($this->tableName. ' T')
			->join('TB_CAMPANHA CA','CA.ID_CAMPANHA = T.ID_CAMPANHA')
			->join('TB_PRODUTO P','P.ID_PRODUTO = T.ID_PRODUTO')
			->join('TB_CLIENTE C','C.ID_CLIENTE = P.ID_CLIENTE')
			->where('T.ID_TEMPLATE',$id)
			->get();
			
		return $rs->row_array();
	}
	
	/**
	 * Lista os itens cadastrados
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $filters array com os filtros
	 * @param int $page numero da pagina
	 * @param int $limit numero de itens por pagina
	 * @return array
	 */
	public function listItems($filters, $page=0, $limit=5, $order = 'DESC_TEMPLATE', $direction = 'ASC'){
		$this->db->join('TB_PRODUTO P','P.ID_PRODUTO=TB_TEMPLATE.ID_PRODUTO')
			->join('TB_CAMPANHA C','C.ID_CAMPANHA = TB_TEMPLATE.ID_CAMPANHA')
			->join('TB_CLIENTE CL','CL.ID_CLIENTE = P.ID_CLIENTE')
			;
			
		if(!empty($filters['PRODUTOS'])){
			$this->db->where_in('P.ID_PRODUTO',$filters['PRODUTOS']);
		}
		
		if(!empty($filters['ID_AGENCIA'])){
			$this->db
				->join('TB_FORNECEDOR_CLIENTE F','F.ID_CLIENTE = CL.ID_CLIENTE')
				->where('F.ID_AGENCIA',$filters['ID_AGENCIA']);
		}
		
		if(!empty($filters['ID_CLIENTE'])){
			$this->db->where('CL.ID_CLIENTE',$filters['ID_CLIENTE']);
		}
		
		if(isset($filters['TEMPLATE_EXISTS']) && $filters['TEMPLATE_EXISTS'] != ''){
			$this->db->where('TEMPLATE_EXISTS',$filters['TEMPLATE_EXISTS']);
		}
		
		if(isset($filters['STATUS_TEMPLATE']) && $filters['STATUS_TEMPLATE'] != ''){
			$this->db->where('STATUS_TEMPLATE',$filters['STATUS_TEMPLATE']);
		}
		
		if(!empty($filters['DATA_CADASTRO_TEMPLATE'])){
			$data = format_date($filters['DATA_CADASTRO_TEMPLATE'],'Y-m-d');
			$this->db->where('DATA_CADASTRO_TEMPLATE >=', $data.' 00:00:00');
			$this->db->where('DATA_CADASTRO_TEMPLATE <=', $data.' 23:59:59');
			unset($filters['DATA_CADASTRO_TEMPLATE']);
		}
		
		$this->setWhereParams($filters);
		return $this->execute($page, $limit, $order, $direction);
	}

	/**
	 * Adiciona uma referencia de link para o indesng
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param $id
	 * @param $link
	 * @param $idtemplateitem
	 * @return void
	 */
	public function addLink($id, $link, $idtemplateitem=null){
		$rs = $this->db->from('TB_TEMPLATE_LINK')
			->where('ID_TEMPLATE',$id)
			->where('ID_TEMPLATE_ITEM',$idtemplateitem)
			->where('ARQUIVO_LINK', $link)
			->get();
			
		if($rs->num_rows() == 0){
			$data = array(
				'ID_TEMPLATE' => $id,
				'ARQUIVO_LINK' => $link,
				'ID_TEMPLATE_ITEM' => $idtemplateitem,
				'STATUS_LINK' => 0
			);
			
			$this->db->insert('TB_TEMPLATE_LINK', $data);
			
		} else {
			$this->setLinkDown($id,$link);
		}
	}
	
	/**
	 * Retorna os links pendentes
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id codigo do template
	 * @return array
	 */
	public function getBrokenLinks($id){
		$rs = $this->db->from('TB_TEMPLATE_LINK')
			->where('ID_TEMPLATE',$id)
			->where('STATUS_LINK', '0')
			->order_by('ARQUIVO_LINK')
			->get();
			
		return $rs->result_array();
	}
	
	/**
	 * Retorna os links
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id codigo do template
	 * @return array
	 */
	public function getLinks($id){
		$rs = $this->db->from('TB_TEMPLATE_LINK')
			->where('ID_TEMPLATE',$id)
			->order_by('ARQUIVO_LINK')
			->get();
			
		return $rs->result_array();
	}
	
	/**
	 * Seta um link como ok
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id coidgo do template
	 * @param string $link nome do link
	 * @return void
	 */
	public function setLinkOk($id, $link){
		$data = array(
				'STATUS_LINK' => 1
			);
			
		$this->db->where('ID_TEMPLATE',$id)
			->where('ARQUIVO_LINK', $link)
			->update('TB_TEMPLATE_LINK', $data);
			
	}
	
	/**
	 * Seta um link como quebrado
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id coidgo do template
	 * @param string $link nome do link
	 * @param int $idtemplateitem codigo do template item
	 * @return void
	 */
	public function setLinkDown($id, $link,$idtemplateitem=null){
		$data = array(
				'STATUS_LINK' => 0
			);
			
		if( !is_null($idtemplateitem) ){
			$this->db->where('ID_TEMPLATE_ITEM',$idtemplateitem);
		}
		
		$this->db->where('ID_TEMPLATE',$id)
			->where('ARQUIVO_LINK', $link)
			->update('TB_TEMPLATE_LINK', $data);
	}
	
	/**
	 * Recupera o caminho completo de onde ficam os arquivos deste template
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id
	 * @return string
	 */
	public function getPath($id){
		$rs = $this->getById($id);
		$path = $this->campanha->getPath($rs['ID_CAMPANHA'])
			.'BANCO_DE_TEMPLATES/TEMPLATE_' . $id . '/';
		
		return $path;
	}
	
	/**
	 * Exclui um registro de template e os arquivos associados
	 * 
	 * @param int $id
	 */
	public function deleteTemplate($id){
		$dir = $this->getPath($id);
		rrmdir($dir);

		if(!is_dir($dir)){
			$this->delete($id);
			return true;
		}

		return false;
	}
	
	/**
	 * Pega o caminho completo do template
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id
	 * @return string 
	 */
	public function getFile($id){
		$file = utf8MAC($this->getPath($id)) . $id . '.indd';
		if(!file_exists($file)){
			return '';
		}
		
		return $file;
	}
	
	/**
	 * Remove links antigos
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id
	 * @return void
	 */
	public function removeOldLinks($id){
		$this->db->where('ID_TEMPLATE', $id)
			->delete('TB_TEMPLATE_LINK');
	}
	
	/**
	 * Pega os arquivos na mesma pasta do template
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id codigo do template
	 * @return array
	 */
	public function getFilesInPath($id){
		$path = utf8MAC($this->getPath($id));
		//echo $path;
		$files = array();
		
		if(is_dir($path)){
			$files = array_diff( scandir($path), array('.', '..'));
		}
		
		return $files;
	}
	
	/**
	 * Recupera as tags disponiveis para uso nbos templates
	 * @author juliano.polito
	 * @return array
	 */
	public function getTemplateTags($idCliente){
				
		$data = array();
		
		$data['template'][] = 'texto_legal_template - ' . $this->config->item('descricao_texto_legal_template');
		
		$fichas = array('nome_ficha',
						'codigo_ficha',
						'agencia',
						'cliente',
						'categoria',
						'subcategoria',
						'fabricante',
						'marca',
						'modelo',
						'texto_legal',);
		
		$desc = $this->config->item('descricoes_ficha');
		
		foreach ($fichas as $idx => $ficha) {
			$data['ficha'][] = $ficha . ' - ' . (isset($desc[$idx]) ? $desc[$idx] : '');
		}
		
		$pricings = array('preco_unitario',
							'preco_caixa',
							'preco_vista',
							'preco_de',
							'preco_por',
							'economize',
							'entrada',
							'prestacao',
							'parcelas',
							'juros_am',
							'juros_aa',
							'total_prazo');
		
		$desc = $this->config->item('descricoes_pricing');
		
		foreach ($pricings as $idx => $pricing) {
			$data['pricing'][] = $pricing . ' - ' . (isset($desc[$idx]) ? $desc[$idx] : '');
		}
		
		$campos = $this->campo->getByCliente($idCliente);

		foreach ($campos as $campo) {
			$data['campos'][] = strtolower(removeAcentos($campo['NOME_CAMPO'])) . ' - ' . $campo['DESCRICAO'];
		}
		
		$tipos = $this->tipo_objeto->getByCliente($idCliente);
		$data['tipos'][] = 'imagem_p - ' . $this->config->item('descricao_imagem_principal');
		foreach ($tipos as $tipo) {
			if(strtolower(removeAcentos($tipo['DESC_TIPO_OBJETO'])) == 'produto'){
				$tipo['DESC_TIPO_OBJETO'] = 'produto';
			}
			$data['tipos'][] = strtolower(removeAcentos($tipo['DESC_TIPO_OBJETO'])).'1...n - ' . $tipo['DESCRICAO'];
		}
		
		return $data;
	}
	
	/**
	 * Pega o preview do template com a pagina inicial menor
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idtemplate Codigo do template
	 * @return int FileID no xinet
	 */
	public function getFileIdPreview($idtemplate){
		// caminho no storage
		$path = $this->getPath($idtemplate);
		// pega os templates relacionados
		$parts = $this->templateItem->getByTemplate($idtemplate);
		
		// se nao tiver nenhum
		if( empty($parts) ){
			// retorna zero
			return 0;
		}
		
		// pega o primeiro
		$tpl = array_shift($parts);
		
		// pega a pagina inicial
		$start = $tpl['PAGINA_INICIO_TEMPLATE_ITEM'];
		// pega a pagina final
		$end = $tpl['PAGINA_FIM_TEMPLATE_ITEM'];
		
		// pega o nome do arquivo
		$filename = $this->templateItem->getFilenameByTemplateRange($idtemplate, $start, $end);
		// troca a extensao
		$filename = preg_replace('@\.indd$@i', '.jpg', $filename);
		
		
		// nome completo
		$fullFilename = $path . $filename;
		$fullFilename = utf8MAC($fullFilename);
		
		// se nao existir
		if( !is_file($fullFilename) ){
			return 0;
		}
		
		// pega o fileid do xinet
		$fileid = $this->xinet->getFilePorPath(utf8MAC($path), $filename);
		
		//Paliativo para resolver problema de geracao de preview de template do indesign 
		//nos modulos template/form e gerar_indd/form
//		if(empty($fileid)){
			XinetKiwi::getInstance()->kats($fullFilename,'JPEG', '8BIM');
			$this->xinet->sincroniza($fullFilename);
			$fileid = $this->xinet->getFilePorPath(utf8MAC($path), $filename);
//		}
		
		return $fileid;
	}
	
	/**
	 * verifica se todos os "pedacos" de template foram enviados corretamente
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idtemplate codigo do template
	 * @return boolean
	 */
	public function checaTemplateCompleto($idtemplate){
		// pegamos os itens relacionados
		$parts = $this->templateItem->getByTemplate($idtemplate);
		// pegamos os dados do template
		$data = $this->getById($idtemplate);
		
		// maior pagina
		$max = $data['NUMERO_PAGINAS'];
		// paginas informadas
		$paginas = array();
		
		// para cada item
		foreach($parts as $part){
			// para cada pagina do inicio ao fim
			for($i=$part['PAGINA_INICIO_TEMPLATE_ITEM']; $i<=$part['PAGINA_FIM_TEMPLATE_ITEM']; $i++){
				// coloca no array
				$paginas[] = $i;
			}
		}
		
		// remove os duplicados (se houver)
		$paginas = array_unique($paginas);
		
		// retorna se os numeros batem (true) ou nao (false)
		return count($paginas) == $max;
	}
	
	/**
	 * Atualiza o preview em PDF do template
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idtemplate
	 * @return void
	 */
	public function atualizarPDFPreview($idtemplate){
		$path = utf8MAC( $this->getPath($idtemplate) );
		$fullfilename = $path . '/' . $idtemplate . '.pdf';
		
		// se o arquivo existe
		if( file_exists($fullfilename) ){
			// apagamos
			unlink($fullfilename);
		}
		
		// pegamos os itens
		$list = $this->templateItem->getByTemplate($idtemplate);
		// lista de jpgs
		$jpgList = array();
		// para cada templateItem
		foreach($list as $item){
			for($i=$item['PAGINA_INICIO_TEMPLATE_ITEM'], $sfx = 1; $i<=$item['PAGINA_FIM_TEMPLATE_ITEM']; $i++){
				if( $i > $item['PAGINA_INICIO_TEMPLATE_ITEM'] ){
					$sfx++;
				}
				
				// nome do jpg
				$nome = 'template_'.$idtemplate.'_'.
					$item['PAGINA_INICIO_TEMPLATE_ITEM'] . '-' . $item['PAGINA_FIM_TEMPLATE_ITEM'] . 
					($sfx > 1 ? $sfx : '') . 
					'.jpg';
				
				// se o arquivo existe
				if( file_exists($path.'/'.$nome) ){
					// coloca na lista
					$jpgList[] = $nome;
				}
			}
		}
		
		if( !empty($jpgList) ){
			$argumentos = array(
				array('name' => 'save_pdf', 'value' => rawurlencode($fullfilename)),
				array('name' => 'path_files', 'value' => rawurlencode($path.'/')),
				array('name' => 'files', 'value' => rawurlencode(implode(',',$jpgList))),
			);
			
			// vamos chamar o kiwi
			$this->ids->kiwi('retail', 'preview_pdf', $argumentos, site_url('idskiwi/callback_preview_pdf/'.$idtemplate));
		}
		
	}
	
	/**
	 * Seta o EMBED_CRIACAO para true ou false
	 * 
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @param int $id coidgo do template
	 * @return void
	 */
	public function setEmbedCriacao($id, $embed=0){
		$data = array(
				'EMBED_CRIACAO' => $embed
			);
			
		$this->db->where('ID_TEMPLATE',$id)
			->update('TB_TEMPLATE', $data);			
	}
	
	/**
	 * Deleta os arquivos do disco
	 * 
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @param int $id job
	 * @return void
	 */
	public function deletarTemplatesByJob( $idJob ){
		$templates = Array();
		$pracas = Array();
		$nomeIndd = null;
		$pathIndd = null;

		$job = $this->job->getById( $idJob );
		$pracas = $this->praca->getByExcel( $job['ID_EXCEL'] );
		$templates = $this->templateItem->getByTemplate( $job['ID_TEMPLATE'] );

		foreach( $pracas as $p ){
			foreach( $templates as $t ){
				$nomeIndd  = sprintf('%d-%s-%d-%d',
					$job['ID_EXCEL'],
					$p['DESC_PRACA'],
					$t['PAGINA_INICIO_TEMPLATE_ITEM'],
					$t['PAGINA_FIM_TEMPLATE_ITEM']
				);
				
				$pathIndd = 'files/layout/' . $nomeIndd . '.indd';
				if( file_exists( $pathIndd ) ){
					unlink( $pathIndd );
				}
			}
		}
	}

}

