<?php
class TransformadorAcaoDB extends GenericModel {
	### START
	protected function _initialize(){
		$this->addField('ID_TRANSFORMADOR_ACAO','int','',0,1);
		$this->addField('ID_TRANSFORMADOR','int','',0,0);
		$this->addField('ACAO','string','',0,0);
		$this->addField('CAMPO','string','',0,0);
		$this->addField('PARAMETROS','blob','',0,0);
	}
	### END

	var $tableName = 'TB_TRANSFORMADOR_ACAO';

	/**
	 * Construtor
	 *
	 * @author Juliano Polito
	 * @link http://www.247id.com.br
	 * @return CampanhaDB
	 */
	function __construct(){
		parent::GenericModel();
	}

	/**
	 * Remove as acoes de um determinado transformador
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int4 $idtransformador
	 * @return void
	 */
	public function removeFrom($idtransformador){
		$this->db
			->where('ID_TRANSFORMADOR', $idtransformador)
			->delete( $this->tableName );
	}

	public function getPossibleActions(){
		$list = array();

		$dir = 'system/application/libraries/transformadores/actions/';
		$dh = dir($dir);

		while( ($file = $dh->read()) !== false ){
			if( preg_match('@^(ActionTransform.*?)\.php$@', $file, $reg) ){
				require_once $dir . $file;

				$ref = new ReflectionClass($reg[1]);
				$inst = $ref->newInstance();

				$list[] = array(
					'CLASSE' => $reg[1],
					'NOME' => $inst->getName(),
					'QTD_PARAMETROS' => $inst->getParamCount(),
					'DESCRICAO' => $inst->getDescription()
				);
			}
		}

		$dh->close();

		return $list;
	}
}

