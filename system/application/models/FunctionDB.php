<?php
/**
 * Model que representa a TB_FUNCTION
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class FunctionDB  extends GenericModel {
	### START
	protected function _initialize(){
		$this->addField('ID_FUNCTION','int','',1,1);
		$this->addField('ID_CONTROLLER','int','',1,0);
		$this->addField('DESC_FUNCTION','string','',6,0);
		$this->addField('CHAVE_FUNCTION','string','',4,0);
	}
	### END

	var $tableName = 'TB_FUNCTION';
	
	/**
	 * Construtor
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return FunctionDB
	 */
	function __construct(){
		parent::GenericModel();
		
		$this->addValidation('ID_CONTROLLER','requiredNumber','Informe a controller');
		$this->addValidation('DESC_FUNCTION','requiredString','Informe a descrição');
		$this->addValidation('CHAVE_FUNCTION','requiredString','Informe a chave');
	}
	
	/**
	 * Lista as functions cadastradas
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $filters Filtros para pesquisa
	 * @param int $page Pagina atual
	 * @param int $limit limit de registros por pagina
	 * @return array
	 */
	public function listItems($filters, $page=0, $limit=5){
		$this->setWhereParams($filters);
		$this->db->join('TB_CONTROLLER C','C.ID_CONTROLLER = TB_FUNCTION.ID_CONTROLLER');
		return $this->execute($page,$limit,'DESC_CONTROLLER, DESC_FUNCTION');
	}
	
	/**
	 * Remove uma function
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo da function
	 * @return void
	 */
	public function remove($id){
		// primeiro, removemos as permissoes
		$this->db->where('ID_FUNCTION',$id)
			->delete('TB_PERMISSAO');
		
		// removendo itens de menu
		$this->db->where('ID_FUNCTION',$id)
			->delete('TB_ITEM_MENU');
			
		// removendo a function
		$this->db->where('ID_FUNCTION',$id)
			->delete($this->tableName);
	}
	
	/**
	 * Recupera as funcoes pelo codigo da controller
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idController Codigo da controller
	 * @return array
	 */
	public function getByController($idController){
		$rs = $this->db->where('ID_CONTROLLER', $idController)
			->order_by('DESC_FUNCTION')
			->get($this->tableName);
			
		return $rs->result_array();
	}
}	

