<?php
class VeiculoDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_VEICULO','int','',1,1);
		$this->addField('ID_MIDIA','int','',1,0);
		$this->addField('DESC_VEICULO','string','',14,0);
		$this->addField('ID_CENTIMETRAGEM','int','',1,0);
		$this->addField('FLAG_ACTIVE_VEICULO','string','',1,0);
		$this->addField('ID_CLASSIFICACAO','int','',1,0);
	}
	### END

	var $tableName = 'TB_VEICULO';
	var $statusField = 'FLAG_ACTIVE_VEICULO';
	
	function getById($id)
	{		
		
		$this->db->join('TB_CENTIMETRAGEM','TB_CENTIMETRAGEM.ID_CENTIMETRAGEM = TB_VEICULO.ID_CENTIMETRAGEM','left');
		$this->db->where($this->statusField, MODEL_STATUS_ATIVO);

		$this->db->where('ID_VEICULO', $id);

		$rs = $this->db->get('TB_VEICULO');

		if ( $rs->num_rows() == 1 )
			return $rs->row_array();
		else
			return null;
	}
	function getByLike($dados = null, $pagina=0, $limit=null)
	{
		
		$sql = 	' SELECT * FROM TB_VEICULO ' .								 			 
			 	' WHERE FLAG_ACTIVE_VEICULO = 1';
				
		if( isset($dados['DESC_VEICULO']) && $dados['DESC_VEICULO']!= null)
			$sql.=" AND DESC_VEICULO LIKE '%".$dados['DESC_VEICULO']."%'";
				
		return $this->executeQuery($sql,$pagina, $limit, 'DESC_VEICULO', 'DESC');
	}	
	function getAllByMidia($id_midia)
	{
		$this->db->where('ID_MIDIA', $id_midia);
		$this->db->where('FLAG_ACTIVE_VEICULO','1');
		$this->db->order_by('DESC_VEICULO', 'ASC');
		
		$rs = $this->db->get('TB_VEICULO');
		
		if ( $rs->num_rows() > 0 )
			return $rs->result_array();
		else
			return false;
	}  
	function getAllFormato($id_veiculo)
	{
		
		$sql = 	' SELECT TB_FORMATO.* FROM TB_FORMATO '.
				' JOIN TB_VEICULO_FORMATO ON (TB_FORMATO.ID_FORMATO=TB_VEICULO_FORMATO.ID_FORMATO) ' .
				' WHERE TB_VEICULO_FORMATO.ID_VEICULO=' . $id_veiculo .
				' GROUP BY TB_FORMATO.DESC_FORMATO ';
		
		$rs = $this->db->query($sql);	
				
		if ( $rs->num_rows() > 0 )
			return $rs->result_array();
		else
			return false;
	}
	function getDisponivelByPraca($id_praca)
	{
		$sql = 	' SELECT * FROM TB_VEICULO ' .
				' WHERE FLAG_ACTIVE_VEICULO = 1 ' .
				' AND ID_VEICULO NOT IN ' .
				' (' .
				'   SELECT TB_PRACA_VEICULO.ID_VEICULO ' .
				'   FROM TB_PRACA_VEICULO ' .
				'   WHERE ID_PRACA = '. $id_praca . 
				' ) ' .
				' ORDER BY TB_VEICULO.DESC_VEICULO ASC';
				
				
		$rs = $this->db->query($sql);	
		
		if ( $rs->num_rows() > 0 )
			return $rs->result_array();
		else
			return false;
	}
	function getDisponivelByFormato($id_formato)
	{
		$sql = 	' SELECT * FROM TB_VEICULO ' .
				' WHERE FLAG_ACTIVE_VEICULO = 1' .
				' AND ID_VEICULO NOT IN ' .
				' (' .
				'   SELECT TB_VEICULO_FORMATO.ID_VEICULO ' .
				'   FROM TB_VEICULO_FORMATO ' .
				'   WHERE ID_FORMATO = '. $id_formato .				
				' ) ' .
				' ORDER BY TB_VEICULO.DESC_VEICULO ASC';
				
				
		$rs = $this->db->query($sql);	
		
		if ( $rs->num_rows() > 0 )
			return $rs->result_array();
		else
			return false;
	}
	function getSelecionadoByFormato($id_formato)
	{
		$sql = 	' SELECT * ' .
				' FROM TB_VEICULO_FORMATO ' .
				' JOIN TB_VEICULO ON TB_VEICULO_FORMATO.ID_VEICULO = TB_VEICULO.ID_VEICULO ' .
				' WHERE TB_VEICULO_FORMATO.ID_FORMATO = ' . $id_formato .
				' AND TB_VEICULO.FLAG_ACTIVE_VEICULO = 1' . 
				' ORDER BY TB_VEICULO.DESC_VEICULO ASC';
								
		$rs = $this->db->query($sql);	
		
		if ( $rs->num_rows() > 0 )
			return $rs->result_array();
		else
			return false;
	}
	function saveVeiculoFormato($veiculos,$id_formato)
	{		
		//deletando todos os veiculos do formato inserido;
		$this->db->delete('TB_VEICULO_FORMATO', array('ID_FORMATO' => $id_formato)); 
		
		//Inserindo todos os veiculos selecionados para o formato;
		foreach($veiculos as $v )						
			$this->db->insert('TB_VEICULO_FORMATO',array('ID_VEICULO' => $v,'ID_FORMATO' => $id_formato));
	}			

	/**
	 * Pega os veiculos pela classificacao e midia
	 * @author Hugo Silva
	 * @param int $idclassificacao
	 * @param int $idmidia
	 * @return array
	 */
	function getByClassificacaoMidia($idclassificacao,$idmidia){
		$rs = $this->db->where('ID_CLASSIFICACAO', $idclassificacao)
			->where('ID_MIDIA', $idmidia)
			->where('FLAG_ACTIVE_VEICULO',1)
			->order_by('DESC_VEICULO')
			->get($this->tableName);
			
		return array('data' => $rs->result_array());
	}
}	

