<?php
class TemplateItemDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_TEMPLATE_ITEM','int','',1,1);
		$this->addField('ID_TEMPLATE','int','',1,0);
		$this->addField('PAGINA_INICIO_TEMPLATE_ITEM','int','',1,0);
		$this->addField('PAGINA_FIM_TEMPLATE_ITEM','int','',1,0);
		$this->addField('DATA_CADASTRO','datetime','',19,0);
	}
	### END

	var $tableName = 'TB_TEMPLATE_ITEM';

	/**
	 * Construtor 
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return TemplateDB
	 */
	function __construct(){
		parent::GenericModel();
	}
	
	/**
	 * retorna a lista de itens pelo codigo do template
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idtemplate
	 * @return array
	 */
	public function getByTemplate($idtemplate){
		$rs = $this->db
			->where('ID_TEMPLATE', $idtemplate)
			->order_by('PAGINA_INICIO_TEMPLATE_ITEM, PAGINA_FIM_TEMPLATE_ITEM')
			->get($this->tableName)
			->result_array();
			
		return $rs;
	}
	
	/**
	 * verifica se o range solicitado intersecciona com algum ja cadastrado
	 * 
	 * <p>Se for exatamente o mesmo range de algum, deixa passar, porque pode sobrescrever</p>
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idtemplate Codigo do template
	 * @param int $startPage Pagina inicial
	 * @param int $endPage Pagina final
	 * @return boolean 
	 */
	public function validateRange($idtemplate, $startPage, $endPage){
		$where = sprintf('(
				(PAGINA_INICIO_TEMPLATE_ITEM BETWEEN %1$d AND %2$d) OR
				(PAGINA_FIM_TEMPLATE_ITEM BETWEEN %1$d AND %2$d) OR
				(%1$d BETWEEN PAGINA_INICIO_TEMPLATE_ITEM AND PAGINA_FIM_TEMPLATE_ITEM) OR
				(%2$d BETWEEN PAGINA_INICIO_TEMPLATE_ITEM AND PAGINA_FIM_TEMPLATE_ITEM)
			) AND ID_TEMPLATE = %3$d', $startPage, $endPage, $idtemplate);
		
		$rs = $this->db
			->where($where)
			->get($this->tableName)
			->result_array();
		
		// se nao tem itens
		if( count($rs) == 0 ){
			// pode enviar
			return true;
		}
			
		// para cada item de resultado
		foreach($rs as $item){
			// se as paginas baterem com o digitado
			if( $item['PAGINA_INICIO_TEMPLATE_ITEM'] == $startPage && $item['PAGINA_FIM_TEMPLATE_ITEM'] == $endPage ){
				// pode enviar
				return true;
			}
		}
			
		return false;
	}
	
	public function debug($txt){
		// $hora = date('d/m/Y H:i:s - ');
		// file_put_contents('debug_template_item.txt', $hora . $txt . PHP_EOL, FILE_APPEND);
	}

	/**
	 * Salva o arquivo de template
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idtemplate Codigo do template
	 * @param string $arquivo Caminho do arquivo original
	 * @param int $startPage pagina inicial
	 * @param int $endPage pagina final
	 * @return int codigo do arquivo salvo
	 */
	public function salvarItem($idtemplate, $arquivo, $startPage, $endPage){
		$path = $this->template->getPath($idtemplate);
		$path = utf8MAC($path);
		
		$this->debug('Caminho: '.$path);
		
		// busca pelo range
		$item = $this->getByTemplateRange($idtemplate,$startPage,$endPage);
		// se nao encontrar
		if( empty($item) ){
			// gravamos o novo item
			$item['PAGINA_INICIO_TEMPLATE_ITEM'] = $startPage;
			$item['PAGINA_FIM_TEMPLATE_ITEM']     = $endPage;
			$item['ID_TEMPLATE']                  = $idtemplate;
			$item['ID_TEMPLATE_ITEM']             = $this->save($item);
		}
		// sempre atualiza a data de cadastro
		$item['DATA_CADASTRO']                = date('Y-m-d H:i:s');
		$this->save($item, $item['ID_TEMPLATE_ITEM']);
		// setando o novo nome do arquivo
		$novoNome = $this->getFilenameByTemplateRange($idtemplate, $startPage, $endPage);
		
		$this->debug('Novo nome: '.$novoNome);
		
		//recupera a configuracao do site
		$site = $this->config->item("site");
		
		// movendo o arquivo temporario para um lugar no servidor
		if( @move_uploaded_file($arquivo, 'files/'.$novoNome) == false ){
			$ex = Exception('Nao foi possivel mover o arquivo de ' .$arquivo.' para files/'.$novoNome);
			$this->debug('Excecao: '.$e->getMessage());
			throw new $ex;
		}
		
		// mudando as permissoes
		chmod('files/'.$novoNome,0777);
		
		
		// origem do arquivo
		$origem = $this->config->item('ponto_montagem') . $this->config->item('files_path') . "/$novoNome";
		// arquivo destino
		$destino = $path . $novoNome;
		$this->debug('Destino: ' . $destino);
		
		// chama o script para criar o diretorio
		$this->debug('Criando diretorio: ' . $path);
		
		/*$res = $this->ids->call('criadiretorio', array(
		  array('name' => 'caminho', 'value' => rawurlencode($path))
		));*/
		
		$res = $this->xinet->createPath($path);
		
		
		//parametros do indesign
		$argumentos = array(
			array('name' => 'origem', 'value' => rawurlencode($origem)),
			array('name' => 'destino', 'value' => rawurlencode($destino))
		);
		
		$this->debug('Retornando links: ' . print_r($argumentos,true));
		
		//recupera o retorno da execucao do script 
		$scriptResult = $this->ids->call('retorna_link',$argumentos);
		// sincronizamos a pasta
		$this->xinet->sincroniza($path);
		
		// removemos o arquivo
		if( file_exists('files/'.$novoNome) ){
			unlink('files/'.$novoNome);
		}
		
		// vamos remover os links deste template item
		$this->db->where('ID_TEMPLATE_ITEM', $item['ID_TEMPLATE_ITEM'])
			->delete('TB_TEMPLATE_LINK');
		
		// se nao deu erro
		if(empty($scriptResult['errorNumber'])){
			$root = $scriptResult['scriptResult'];
			// se retornou um unico item
			if(!empty($root['data']['item']['data'])) {
				// eh indd?
				if(preg_match('@\.indd$@i', $novoNome)){
					// coloca na lista de links
					$str = str_replace('\\','/', fromUtf8MAC($root['data']['item']['data']));
					$str = str_replace(':','/',$str);
					$parts = explode('/', $str);
					$this->template->addLink($idtemplate, array_pop($parts), $item['ID_TEMPLATE_ITEM']);
					
				// se nao eh, procura para tirar do array
				} else {
					$this->template->setLinkOk($idtemplate, $novoNome);
				}
				
			// se retornou uma lista de itens
			// coloca na lista de links
			} else if( isset($root['data']['item']) && is_array($root['data']['item']) ) {
				foreach($root['data']['item'] as $node){
					$str = str_replace('\\','/', fromUtf8MAC($node['data']));
					$str = str_replace(':','/',$str);
					$parts = explode('/', $str);
					$this->template->addLink($idtemplate, array_pop($parts), $item['ID_TEMPLATE_ITEM']);
				}
			}
		} 
		
		// atualiza os links conforme os arquivos que estao
		// na mesma pasta que o template
		$files = $this->template->getFilesInPath($idtemplate);
		foreach($files as $file){
			$this->template->setLinkOk($idtemplate, $file);
		}
		
		return $item['ID_TEMPLATE_ITEM'];
	}
	
	/**
	 * recupera os dados de um item pelo codigo do template e o range
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idtemplate codigo do template
	 * @param int $startPage pagina inicial
	 * @param int $endPage pagina final
	 * @return array dados do item
	 */
	public function getByTemplateRange($idtemplate,$startPage,$endPage){
		$rs = $this->db
			->where('ID_TEMPLATE',$idtemplate)
			->where('PAGINA_INICIO_TEMPLATE_ITEM',$startPage)
			->where('PAGINA_FIM_TEMPLATE_ITEM',$endPage)
			->get($this->tableName)
			->row_array();
			
		return $rs;
	}
	
	/**
	 * Recupera o nome formatado pelos dados do item
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idtemplate
	 * @param int $startPage
	 * @param int $endPage
	 * @return string
	 */
	public function getFilenameByTemplateRange($idtemplate,$startPage,$endPage){
		return sprintf('template_%d_%d-%d.indd', $idtemplate, $startPage, $endPage);
	}
	
	/**
	 * 
	 * @param int $idExcel
	 * @param int $idPraca
	 * @param int $pgInicio
	 * @param int $pgFim
	 * @return string
	 */
	public function getFilenameByExcelPracaRange($idExcel,$idPraca,$pgInicio,$pgFim){
		return sprintf('%d-%s-%d-%d',
						$idExcel,
						$idPraca,
						$pgInicio,
						$pgFim
					);
	}
	
	
	/**
	 * Remove um item de template
	 * 
	 * <p>tambem remove o arquivo de template relacionado no storage</p>
	 * @author Hugo Ferreira da Silva
	 * @param int $iditem codigo do item
	 * @return void
	 */
	public function remover($iditem){
		$data = $this->getById($iditem);
		$filename = $this->getFilenameByTemplateRange($data['ID_TEMPLATE'], $data['PAGINA_INICIO_TEMPLATE_ITEM'], $data['PAGINA_FIM_TEMPLATE_ITEM']);
		
		$path = $this->template->getPath($data['ID_TEMPLATE']);
		$filename = $path . $filename;
		
		if( file_exists($filename) && is_writable($filename) ){
			unlink($filename);
		}
		
		$this->xinet->sincroniza( $this->template->getPath($data['ID_TEMPLATE']) );
		
		$this->delete($iditem);
	}
	
	/**
	 * Pega o caminho completo do template
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id
	 * @return string 
	 */
	public function getFile($id){
		$item = $this->getById($id);
		
		$file = utf8MAC($this->template->getPath($item['ID_TEMPLATE'])); 
		
		$file .= $this->getFilenameByTemplateRange(
			$item['ID_TEMPLATE'],
			$item['PAGINA_INICIO_TEMPLATE_ITEM'],
			$item['PAGINA_FIM_TEMPLATE_ITEM']
		);
		
		/*if(!file_exists($file)){
			return '';
		}*/
		
		return $file;
	}
	
	
}

