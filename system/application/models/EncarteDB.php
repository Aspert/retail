<?php
/**
 * Classe de modelo para o gerenciamento de encartes
 * @author Bruno Oliveira
 * @link http://www.247id.com.br
 */
class EncarteDB extends GenericModel {
	### START
	protected function _initialize(){
		$this->addField('FILE','string','',250,0);
		$this->addField('OBSERVACAO','string','',1000,0);
			}
	### END

	
	var $tableName = 'TB_ENCARTE';
	
	/**
	 * Construtor
	 *  
	 * @author Bruno Oliveira
	 * @link http://www.247id.com.br
	 * @return EncarteDB
	 */
	function __construct(){
		parent::GenericModel();
		
		// adiciona as validacoes
		$this->addValidation('FILE','requiredString','Selecione um arquivo .PDF');
		$this->addValidation('OBSERVACAO','requiredString','Informe as observações');
	}
	
	function save($data){
		
		$this->db->insert($this->tableName, $data);
		
		return $this->db->insert_id();;
	}
	
	function getAll(){
		
		$rs = $this->db
			->from($this->tableName)
			->get()
			->result_array();
			
		return $rs;
	}
	
	function getByFileID($idEncarte){
		
		$rs = $this->db
		->where('ID_ENCARTE', $idEncarte)
		->get($this->tableName)
		->result_array();
			
		return $rs;
		
	}
	
}