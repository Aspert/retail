<?php
class MarcaDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_MARCA','int','',1,1);
		$this->addField('ID_FABRICANTE','int','',1,0);
		$this->addField('DESC_MARCA','string','',4,0);
		$this->addField('FLAG_ACTIVE_MARCA','string','',1,0);
	}
	### END

	var $tableName = 'TB_MARCA';
	
	/**
	 * Lista as marcas cadastradas
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $filters filtros para pesquisa
	 * @param int $page Pagina atual
	 * @param int $limit numero de itens por pagina
	 * @return array
	 */
	public function listItems($filters, $page=0, $limit=5){
		$this->db->join('TB_FABRICANTE F','F.ID_FABRICANTE = TB_MARCA.ID_FABRICANTE');
		
		$this->setWhereParams($filters);
		return $this->execute($page,$limit,'DESC_FABRICANTE, DESC_MARCA');
	}
	
	/**
	 * retorna a descricao pelo id
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id
	 * @return string
	 */
	public function getDescricao($id){
		$rs = $this->getById($id);
		if(!empty($rs)){
			return $rs['DESC_MARCA'];
		}
		
		return '';
	}
	
	/**
	 * Pega marcas pelo codigo do fabricante
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idFabricante Codigo do fabricante
	 * @return array
	 */
	public function getByFabricante($idFabricante){
		$rs = $this->db->where('ID_FABRICANTE',$idFabricante)
			->order_by('ID_FABRICANTE')
			->get($this->tableName);
			
		return $rs->result_array();
	}
	
	/**
	 * recupera as marcas
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param $lista
	 * @return array
	 */
	public function getMarcas(array $lista){
		$result = array();
		$rs = $this->db->where_in('ID_MARCA', $lista)
			->order_by('DESC_MARCA')
			->get($this->tableName);
			
		foreach($rs->result_array() as $item){
			$result[$item['ID_MARCA']] = $item['DESC_MARCA'];
		}
		
		return $result;
	}
	
	/********************************************************************/
	/************************* FUNCOES ANTIGAS **************************/
	/********************************************************************/
	
	function getAllByFabricante($id)
	{
		if($id!=null)
		{
			$data = $this->db->getwhere('TB_MARCA',array('ID_FABRICANTE'=>$id));
			if($data->num_rows()>0)
				return $data->result_array();
			return false;	
		}
		return false;
	}
	
	function getByLike($dados, $pagina = 0, $limit) 
	{
		if (isset($dados['DESC_MARCA'])) {			
			$sql = 'SELECT * FROM '.$this->tableName.' WHERE DESC_MARCA LIKE "%'.$dados['DESC_MARCA'].'%"';
			return $this->executeQuery($sql,$pagina,$limit);			
		}
		else{
			return false;
		}
	}
	function getByDesc($desc){
		
		$this->db->like('DESC_MARCA', $desc);
		$data = $this->db->get('TB_MARCA');
		if($data->num_rows()>=1){
			return $data->row_array();
		}
		return false;
		
	}
	
	function getByDescFabricante($desc, $idFabricante){
		
		$this->db->where('DESC_MARCA', $desc);
		$this->db->where('ID_FABRICANTE', $idFabricante);
		$data = $this->db->get('TB_MARCA');
		if($data->num_rows()>=1){
			return $data->row_array();
		}
		return false;
		
	}
	  				
}	
