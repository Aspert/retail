<?php
class Tipo_objetoDB extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_TIPO_OBJETO','int','',1,1);
		$this->addField('DESC_TIPO_OBJETO','string','',7,0);
		$this->addField('KEY_OBJETO','string','',0,0);
		$this->addField('EXIGE_CATEGORIA','int','',1,0);
		$this->addField('ID_CLIENTE','int','',1,0);
		$this->addField('DESCRICAO','string','',0,0);
		$this->addField('CHAVE_STORAGE_TIPO_OBJETO','string','',0,0);
	}
	### END

	var $tableName = 'TB_TIPO_OBJETO';
	
	function __construct(){
		parent::GenericModel();
		
		$this->addValidation('ID_CLIENTE', 'requiredNumber','Informe o cliente');
		$this->addValidation('DESC_TIPO_OBJETO', 'requiredString','Informe o nome');
		$this->addValidation('DESC_TIPO_OBJETO', 'function',array($this,'verificaNomeDuplicado'));
		$this->addValidation('CHAVE_STORAGE_TIPO_OBJETO', 'function',array($this,'checaChaveStorage'));
		$this->addValidation('EXIGE_CATEGORIA', 'requiredNumber','Informe se a categoria será obrigatória');
	}
	
	/**
	 * Usado na validacao do campo DESC_TIPO_OBJETO
	 * @author juliano.polito
	 * @param $data
	 * @return bool
	 */
	public function verificaNomeDuplicado($data){
		$idcliente = empty($data['ID_CLIENTE']) ? 0 : sprintf('%d',$data['ID_CLIENTE']);
		$idtipo = empty($data['ID_TIPO_OBJETO']) ? 0 : sprintf('%d',$data['ID_TIPO_OBJETO']);
		
		$rs = $this->db
			->where('DESC_TIPO_OBJETO',$data['DESC_TIPO_OBJETO'])
			->where('ID_CLIENTE = ', $idcliente)
			->where('ID_TIPO_OBJETO != ', $idtipo)
			->get($this->tableName);

		if($rs->num_rows() > 0){
			return 'Já existe um tipo com este nome neste cliente';
		}
		
		return true;
	}
	
	public function getById($id){
		$rs = $this->db->from('TB_TIPO_OBJETO T')
			->join('TB_CLIENTE C','C.ID_CLIENTE=T.ID_CLIENTE')
			->where('ID_TIPO_OBJETO', $id)
			->get();
			
		return $rs->row_array();
	}
	
	/**
	 * retorna a descricao pelo id
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id
	 * @return string
	 */
	public function getDescricao($id){
		$rs = $this->getById($id);
		if(!empty($rs)){
			return $rs['DESC_TIPO_OBJETO'];
		}
		
		return '';
	}
	
	/**
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param $name
	 * @return unknown_type
	 */
	function getIdByName($name)
	{
		$sql = 'SELECT ID_TIPO_OBJETO FROM '.$this->tableName.' WHERE DESC_TIPO_OBJETO = "'.$name.'"';
		$data = $this->db->query($sql);
		$row = $data->row_array();
		return $row['ID_TIPO_OBJETO']; 
	}
	
	
	/**
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param $lista
	 * @return array
	 */
	public function getTipos(array $lista){
		$result = array();
		$rs = $this->db->where_in('ID_TIPO_OBJETO', $lista)
			->order_by('DESC_TIPO_OBJETO')
			->get($this->tableName);
			
		foreach($rs->result_array() as $item){
			$result[$item['ID_TIPO_OBJETO']] = $item['DESC_TIPO_OBJETO'];
		}
		
		return $result;
	}
	
	/**
	 * recupera os tipos do cliente
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idCliente
	 * @return array
	 */
	public function getByCliente($idCliente){
		$rs = $this->db->from('TB_TIPO_OBJETO T')
			->join('TB_CLIENTE C','C.ID_CLIENTE=T.ID_CLIENTE')
			->where('T.ID_CLIENTE', $idCliente)
			->get();
			
		return $rs->result_array();
	}
	
	/**
	 * recupera os tipos por cliente, que possuam categoria obrigatoria
	 * @author juliano.polito
	 * @param $idCliente
	 * @return array
	 */
	public function getByClienteComCategoria($idCliente){
		$rs = $this->db->from('TB_TIPO_OBJETO T')
			->join('TB_CLIENTE C','C.ID_CLIENTE=T.ID_CLIENTE')
			->where('T.ID_CLIENTE', $idCliente)
			->where('T.EXIGE_CATEGORIA', 1)
			->get();
			
		return $rs->result_array();
	}
	
	/**
	 * Lista os itens cadastrados
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $filters
	 * @param int $page
	 * @param int $limit
	 * @return array
	 */
	public function listItems($filters,$page=0,$limit=5){
		$this->db->join('TB_CLIENTE C','C.ID_CLIENTE = TB_TIPO_OBJETO.ID_CLIENTE');
			
		$this->setWhereParams($filters);
		return $this->execute($page,$limit, 'DESC_CLIENTE,DESC_TIPO_OBJETO');
	}

	/**
	 * Retorna a pasta de um tipo de objeto
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idTipo codigo do tipo de objeto
	 * @return string
	 */
	public function getPath($idTipo){
		$rs = $this->getById($idTipo);
		$path = $this->cliente->getPath($rs['ID_CLIENTE']);
		$path .= 'BANCO DE IMAGENS/';
		$path .= $rs['CHAVE_STORAGE_TIPO_OBJETO'] . '/';
			
		return $path;
	}
	
	/**
	 * Recupera o codigo do tipo de imagem avulsa
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $cliente
	 * @return int
	 */
	public function getTipoAvulsa($cliente){
		$rs = $this->db->where('DESC_TIPO_OBJETO','Avulsa')
			->where('ID_CLIENTE',$cliente)
			->get($this->tableName)
			->row_array();
			
		$id = empty($rs['ID_TIPO_OBJETO']) ? null : $rs['ID_TIPO_OBJETO'];
		$data = array(
			'DESC_TIPO_OBJETO'          => 'Avulsa',
			'CHAVE_STORAGE_TIPO_OBJETO' => 'Avulsa',
			'ID_CLIENTE'                => $cliente
		);
		
		return $this->save($data, $id);
	}

	/**
	 * Verifica se a chave do storage ja nao pertence a outro tipo
	 * @author Hugo Ferreira da Silva
	 * @param array $data
	 * @return mixed
	 */
	public function checaChaveStorage($data){
		if( !empty($data['CHAVE_STORAGE_TIPO_OBJETO']) ){
			$id = empty($data['ID_TIPO_OBJETO']) ? 0 : sprintf('%d',$data['ID_TIPO_OBJETO']);
			
			$rs = $this->db->where('CHAVE_STORAGE_TIPO_OBJETO',$data['CHAVE_STORAGE_TIPO_OBJETO'])
				->where('ID_TIPO_OBJETO != ', $id)
				->where('ID_CLIENTE', $data['ID_CLIENTE'])
				->get($this->tableName);
	
			if($rs->num_rows() > 0){
				return 'Já existe uma chave de storage com este nome';
			}
		}
		
		return true;
	}
	
	/**
	 * Recupera um tipo de objeto pela chave no storage e pelo codigo do cliente
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param string $chave
	 * @param int $idcliente
	 * @return array
	 */
	public function getByChaveCliente($chave,$idcliente){
		$rs = $this->db
			->where('CHAVE_STORAGE_TIPO_OBJETO', $chave)
			->where('ID_CLIENTE', $idcliente)
			->get($this->tableName)
			->row_array();
			
		return $rs;
	}

	/**
	 * Cria os tipos de objeto padrao para um cliente
	 * @author Hugo Ferreira da Silva
	 * @param int $idcliente
	 * @return void
	 */
	public function criarTiposPadrao($idcliente){
		$tipos = array(
			array('DESC_TIPO_OBJETO' => 'Produto', 'CHAVE_STORAGE_TIPO_OBJETO'=>'PRODUTO','EXIGE_CATEGORIA'=>1,'DESCRICAO'=>'Imagens de produtos'),
			array('DESC_TIPO_OBJETO' => 'Logo', 'CHAVE_STORAGE_TIPO_OBJETO'=>'LOGO','EXIGE_CATEGORIA'=>0,'DESCRICAO'=>'Imagens de logos'),
			array('DESC_TIPO_OBJETO' => 'Selo', 'CHAVE_STORAGE_TIPO_OBJETO'=>'SELO','EXIGE_CATEGORIA'=>0,'DESCRICAO'=>'Imagens de selos'),
			array('DESC_TIPO_OBJETO' => 'Splash', 'CHAVE_STORAGE_TIPO_OBJETO'=>'SPLASH','EXIGE_CATEGORIA'=>0,'DESCRICAO'=>'Imagens de splashs'),
			array('DESC_TIPO_OBJETO' => 'Acessório', 'CHAVE_STORAGE_TIPO_OBJETO'=>'ACESSORIO','EXIGE_CATEGORIA'=>0,'DESCRICAO'=>'Acessórios'),
		);
		
		foreach($tipos as $tipo){
			$tipo['ID_CLIENTE'] = $idcliente;
			
			$rs = $this->getByChaveCliente($tipo['CHAVE_STORAGE_TIPO_OBJETO'],$idcliente);
			
			if( empty($rs) ){
				$this->save($tipo,null);
			}
		}
	}
}

