<?php
class SubcategoriaDB extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_SUBCATEGORIA','int','',1,1);
		$this->addField('ID_CATEGORIA','int','',1,0);
		$this->addField('DESC_SUBCATEGORIA','string','',16,0);
		$this->addField('CODIGO_SUBCATEGORIA','string','',2,0);
		$this->addField('FLAG_ACTIVE_SUBCATEGORIA','int',1,1,0);
	}
	### END

	var $tableName = 'TB_SUBCATEGORIA';
	
	/**
	 * Construtor
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return unknown_type
	 */
	function __construct(){
		parent::GenericModel();
		$this->addValidation('ID_CATEGORIA','requiredNumber','Informe a categoria principal');
		$this->addValidation('DESC_SUBCATEGORIA','requiredString','Informe o nome');
		$this->addValidation('DESC_SUBCATEGORIA','function', array($this,'checaNomeDuplicado'));
		
		$this->addValidation('CODIGO_SUBCATEGORIA','requiredString','Informe o código');
		$this->addValidation('CODIGO_SUBCATEGORIA','function', array($this,'checaCodigoDuplicado'));
	}
	
	/**
	 * 
	 * @see system/application/libraries/GenericModel#getById($id)
	 */
	public function getById($id, $active = false){
		if($active){
			$this->db->where('FLAG_ACTIVE_SUBCATEGORIA', 1);
		}
		$rs = $this->db->join('TB_CATEGORIA CA','CA.ID_CATEGORIA = TB_SUBCATEGORIA.ID_CATEGORIA')
			->join('TB_CLIENTE C','C.ID_CLIENTE = CA.ID_CLIENTE','LEFT')
			->where('ID_SUBCATEGORIA',$id)
			->get($this->tableName);
			
		return $rs->row_array();
	}
	
	/**
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param $id
	 * @return unknown_type
	 */
	public function getDescricao($id){
		$rs = $this->db->where('ID_SUBCATEGORIA', $id)
			->get($this->tableName)
			->row_array();
			
		return empty($rs['DESC_SUBCATEGORIA']) ? '' : $rs['DESC_SUBCATEGORIA'];
	}
	
	/**
	 * Retorna as subcategorias a partir de um categoria
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idCategoria Codigo da categoria
	 * @return array
	 */
	public function getByCategoria($idCategoria, $active = true){
		if($active){
			$this->db->where('FLAG_ACTIVE_SUBCATEGORIA', 1);
		}
		$rs = $this->db
			->where('ID_CATEGORIA', $idCategoria)
			->where('FLAG_ACTIVE_SUBCATEGORIA', 1)
			->order_by('DESC_SUBCATEGORIA')
			->get($this->tableName);
			
		return $rs->result_array();
	}
	
	/**
	 * Retorna as subcategorias a partir de um cliente
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idCliente Codigo do cliente
	 * @return array
	 */
	public function getByCliente($idCliente, $active = true){
		if($active){
			$this->db->where('FLAG_ACTIVE_SUBCATEGORIA', 1);
		}
		$rs = $this->db->from($this->tableName.' S')
			->join('TB_CATEGORIA C','C.ID_CATEGORIA = S.ID_CATEGORIA')
			->where('ID_CLIENTE', $idCliente)
			->order_by('DESC_SUBCATEGORIA')
			->get();
			
		return $rs->result_array();
	}
	
	/**
	 * Recupera pela linha de produto
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idProduto Codigo da linha de produto
	 * @return array
	 */
	public function getByProduto($idProduto, $active = true){
		if($active){
			$this->db->where('FLAG_ACTIVE_SUBCATEGORIA', 1);
		}
		$rs = $this->db->from($this->tableName.' S')
			->join('TB_CATEGORIA C','C.ID_CATEGORIA = S.ID_CATEGORIA')
			->where('ID_PRODUTO', $idProduto)
			->order_by('DESC_SUBCATEGORIA')
			->get();
			
		return $rs->result_array();
	}
	
	/**
	 * Lista as subcategorias cadastradas
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $filters
	 * @param int $page Pagina atual
	 * @param int $limit numero de itens por pagina
	 * @return array
	 */
	public function listItems($filters, $page=0, $limit=5){
		$this->db->join('TB_CATEGORIA CA','CA.ID_CATEGORIA = TB_SUBCATEGORIA.ID_CATEGORIA')
			->join('TB_CLIENTE C','C.ID_CLIENTE = CA.ID_CLIENTE','LEFT');
			
		if(isset($filters['FLAG_ACTIVE_SUBCATEGORIA']) && $filters['FLAG_ACTIVE_SUBCATEGORIA'] === '0'){
			$this->db->where('FLAG_ACTIVE_SUBCATEGORIA', 0);
		}
			
		$this->setWhereParams($filters);
			
		if(!empty($filters['ID_CLIENTE'])){
			$this->db->where('CA.ID_CLIENTE',$filters['ID_CLIENTE']);
		}
		
		return $this->execute($page,$limit,'DESC_CLIENTE,DESC_CATEGORIA,DESC_SUBCATEGORIA');
	}
		
	
	function getByLike($dados = null, $pagina=0, $limit=null)
	{
		
		$sql = 	' SELECT * FROM TB_SUBCATEGORIA ' .
				' JOIN TB_CATEGORIA ON TB_CATEGORIA.ID_CATEGORIA = TB_SUBCATEGORIA.ID_CATEGORIA ' .								 			 
			 	' WHERE 1 = 1';
				
		if( isset($dados['DESC_SUBCATEGORIA']) && $dados['DESC_SUBCATEGORIA']!= null)
			$sql.=" AND TB_SUBCATEGORIA.DESC_SUBCATEGORIA LIKE '%".$dados['DESC_SUBCATEGORIA']."%'";
				
		return $this->executeQuery($sql,$pagina, $limit, 'DESC_CATEGORIA', 'DESC');
	}	  
	
	function getByDesc($desc){
		$this->db->like('DESC_SUBCATEGORIA', $desc);
		$data = $this->db->get($this->tableName);
		if($data->num_rows()>=1)
			return $data->row_array();
		return false;
	}
	
	/**
	 * Checa se o codigo esta duplicado
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param $data
	 * @return mixed
	 */
	public function checaCodigoDuplicado($data){
		$total = $this->db->join('TB_CATEGORIA CA','CA.ID_CATEGORIA=S.ID_CATEGORIA')
			->join('TB_CLIENTE C','C.ID_CLIENTE = CA.ID_CLIENTE')
			->where('C.ID_CLIENTE',$data['ID_CLIENTE'])
			->where('CODIGO_SUBCATEGORIA',$data['CODIGO_SUBCATEGORIA'])
			->where('ID_SUBCATEGORIA !=', $data['ID_SUBCATEGORIA'])
			->get($this->tableName . ' S')
			->num_rows();
			
		if($total > 0){
			return 'Já existe uma subcategoria com este codigo para nesta categoria';
		}
		
		return true;
	}
	
	/**
	 * checa nome duplicado
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param $data
	 * @return mixed
	 */
	public function checaNomeDuplicado($data){
		$total = $this->db->where('ID_CATEGORIA',$data['ID_CATEGORIA'])
			->where('DESC_SUBCATEGORIA',$data['DESC_SUBCATEGORIA'])
			->where('ID_SUBCATEGORIA !=', $data['ID_SUBCATEGORIA'])
			->get($this->tableName)
			->num_rows();
			
		if($total > 0){
			return 'Já existe uma subcategoria com este nome para nesta categoria';
		}
		
		return true;
	}
	
	/**
	 * recupera sub categorias
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param $lista
	 * @return array
	 */
	public function getSubcategorias(array $lista){
		$result = array();
		$rs = $this->db->where_in('ID_SUBCATEGORIA', $lista)
			->order_by('DESC_SUBCATEGORIA')
			->get($this->tableName);
			
		foreach($rs->result_array() as $item){
			$result[$item['ID_SUBCATEGORIA']] = $item['DESC_SUBCATEGORIA'];
		}
		
		return $result;
	}
	
	public function pegarCor( $idSubcategoria, $cliente ){
		$colors = array( 0xFFFFCC
						,0xFFFF99
						,0xFFFF66
						,0xFFFF33
						,0xFFFF00
						,0xFFCCFF
						,0xFFCCCC
						,0xFFCC99
						,0xFFCC66
						,0xFFCC33
						,0xFFCC00
						,0xFF99FF
						,0xFF99CC
						,0xFF9999
						,0xFF9966
						,0xFF9933
						,0xFF9900
						,0xFF66FF
						,0xFF66CC
						,0xFF6699
						,0xFF6666
						,0xFF6633
						,0xFF6600
						,0xFF33FF
						,0xFF33CC
						,0xFF3399
						,0xFF3366
						,0xFF3333
						,0xFF3300
						,0xFF00FF
						,0xFF00CC
						,0xFF0099
						,0xFF0066
						,0xFF0033
						,0xFF0000
						,0xCCFFFF
						,0xCCFFCC
						,0xCCFF99
						,0xCCFF66
						,0xCCFF33
						,0xCCFF00
						,0xCCCCFF
						,0xCCCCCC
						,0xCCCC99
						,0xCCCC66
						,0xCCCC33
						,0xCCCC00
						,0xCC99FF
						,0xCC99CC
						,0xCC9999
						,0xCC9966
						,0xCC9933
						,0xCC9900
						,0xCC66FF
						,0xCC66CC
						,0xCC6699
						,0xCC6666
						,0xCC6633
						,0xCC6600
						,0xCC33FF
						,0xCC33CC
						,0xCC3399
						,0xCC3366
						,0xCC3333
						,0xCC3300
						,0xCC00FF
						,0xCC00CC
						,0xCC0099
						,0xCC0066
						,0xCC0033
						,0xCC0000
						,0x99FFFF
						,0x99FFCC
						,0x99FF99
						,0x99FF66
						,0x99FF33
						,0x99FF00
						,0x99CCFF
						,0x99CCCC
						,0x99CC99
						,0x99CC66
						,0x99CC33
						,0x99CC00
						,0x9999FF
						,0x9999CC
						,0x999999
						,0x999966
						,0x999933
						,0x999900
						,0x9966FF
						,0x9966CC
						,0x996699
						,0x996666
						,0x996633
						,0x996600
						,0x9933FF
						,0x9933CC
						,0x993399
						,0x993366
						,0x993333
						,0x993300
						,0x9900FF
						,0x9900CC
						,0x990099
						,0x990066
						,0x990033
						,0x990000
						,0x66FFFF
						,0x66FFCC
						,0x66FF99
						,0x66FF66
						,0x66FF33
						,0x66FF00
						,0x66CCFF
						,0x66CCCC
						,0x66CC99
						,0x66CC66
						,0x66CC33
						,0x66CC00
						,0x6699FF
						,0x6699CC
						,0x669999
						,0x669966
						,0x669933
						,0x669900
						,0x6666FF
						,0x6666CC
						,0x666699
						,0x666666
						,0x666633
						,0x666600
						,0x6633FF
						,0x6633CC
						,0x663399
						,0x663366
						,0x663333
						,0x663300
						,0x6600FF
						,0x6600CC
						,0x660099
						,0x660066
						,0x660033
						,0x660000
						,0x33FFFF
						,0x33FFCC
						,0x33FF99
						,0x33FF66
						,0x33FF33
						,0x33FF00
						,0x33CCFF
						,0x33CCCC
						,0x33CC99
						,0x33CC66
						,0x33CC33
						,0x33CC00
						,0x3399FF
						,0x3399CC
						,0x339999
						,0x339966
						,0x339933
						,0x339900
						,0x3366FF
						,0x3366CC
						,0x336699
						,0x336666
						,0x336633
						,0x336600
						,0x3333FF
						,0x3333CC
						,0x333399
						,0x333366
						,0x333333
						,0x333300
						,0x3300FF
						,0x3300CC
						,0x330099
						,0x330066
						,0x330033
						,0x330000
						,0x00FFFF
						,0x00FFCC
						,0x00FF99
						,0x00FF66
						,0x00FF33
						,0x00FF00
						,0x00CCFF
						,0x00CCCC
						,0x00CC99
						,0x00CC66
						,0x00CC33
						,0x00CC00
						,0x0099FF
						,0x0099CC
						,0x009999
						,0x009966
						,0x009933
						,0x009900
						,0x0066FF
						,0x0066CC
						,0x006699
						,0x006666
						,0x006633
						,0x006600
						,0x0033FF
						,0x0033CC
						,0x003399
						,0x003366
						,0x003333
						,0x003300
						,0x0000FF
						,0x0000CC
						,0x000099
						,0x000066
						,0x000033
						,0x000000);
						
		$randSeed = srand($idSubcategoria + date('d'));
		$rand = rand(0, $idSubcategoria);
		$color = dechex($colors[$rand%214]);
		$retorno = str_pad($color, 6, "0", STR_PAD_LEFT);
		return '#'.$retorno;
	}
	
	public function getByDescCategoria($descricao, $idCategoria){
		$rs = $this->db
			->where('ID_CATEGORIA', $idCategoria)
			->where('DESC_SUBCATEGORIA', $descricao)
			->where('FLAG_ACTIVE_SUBCATEGORIA', 1)
			->order_by('DESC_SUBCATEGORIA')
			->get($this->tableName);

		if($rs->num_rows()>=1){
			return $rs->row_array();
		}
		return false;
	}
}

