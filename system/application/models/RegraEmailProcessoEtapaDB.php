<?php
/**
 * Model que representa a tabela TB_REGRA_EMAIL_PROCESSO_ETAPA
 * @author Esdras Eduardo
 * @link http://www.247id.com.br
 */
class RegraEmailProcessoEtapaDB extends GenericModel {
	
	### START
	protected function _initialize(){
		$this->addField('ID_REGRA_EMAIL_PROCESSO_ETAPA','int','',11,1);
		$this->addField('CHAVE_REGRA_EMAIL_PROCESSO_ETAPA','string','',50,0);
		$this->addField('DESC_REGRA_EMAIL_PROCESSO_ETAPA','string','',250,0);
		$this->addField('QTD_HORAS_ANTES_ENVIO','string','',6,0);
		$this->addField('STATUS_REGRA_EMAIL_PROCESSO_ETAPA','int','1',1,0);
		$this->addField('FLAG_ENVIA_LOGADO','int','1',1,0);
	}
	### END

	var $tableName = 'TB_REGRA_EMAIL_PROCESSO_ETAPA';
	
	/**
	 * Construtor
	 * 
	 * @author Esdras Eduardo
	 * @link http://www.247id.com.br
	 * @return RegraEmailProcessoEtapaDB
	 */
	function __construct(){
		parent::GenericModel();
	}
	
	/**
	 * Lista as regras cadastradas
	 * 
	 * @author Esdras Eduardo
	 * @link http://www.247id.com.br
	 * @param array $filters
	 * @param int $page Pagina atual
	 * @param int $limit numero de itens por pagina
	 * @return array
	 */
	public function listItems($filters, $page=0, $limit=5){
		$this->db->order_by('CHAVE_REGRA_EMAIL_PROCESSO_ETAPA ASC');
		$this->setWhereParams($filters);
		return $this->execute($page,$limit);
	}
	
	/**
	 * Recupera as regras de email ativas por controller e funcao
	 * @author Esdras Eduardo
	 * @param $chave string com a chave
	 * @return array
	 */
	public function getByChave($chave){
		$rs = $this->db->from($this->tableName . ' REPE' )
			->where('REPE.CHAVE_REGRA_EMAIL_PROCESSO_ETAPA',$chave)
			->where('REPE.STATUS_REGRA_EMAIL_PROCESSO_ETAPA',1)
			->get();
		return $rs->row_array();
	}
	
	/**
	 * Encontrar todas as regras de email de processo
	 * 
	 * @author esdras.filho
	 * @param Number $idProcessoEtapa
	 */
	public function findAll( $idProcessoEtapa ){
		$rs = $this->db->from($this->tableName)->get()->result_array();
		foreach( $rs as &$item ){
			$regra_usuario = new RegraEmailProcessoEtapaUsuarioDB();
			$item['qtaHoras'] = $regra_usuario->findHorasByRegraProcesso($item['ID_REGRA_EMAIL_PROCESSO_ETAPA'], $idProcessoEtapa);
			$item['usuarios'] = $regra_usuario->findAllByRegraProcesso($item['ID_REGRA_EMAIL_PROCESSO_ETAPA'], $idProcessoEtapa);
		}
		return $rs;
	}

	/**
	 * Recupera o ID da regra pela chave
	 * @author Nelson Martucci 
	 * @param $chave A chave da regra
	 * @return O ID caso encontre, ou null caso não encontre
	 */
	public function getIdByChave($chave){
		$row = $this->getByChave($chave);
		return !empty($row["ID_REGRA_EMAIL_PROCESSO_ETAPA"]) ? $row["ID_REGRA_EMAIL_PROCESSO_ETAPA"] : '';
	}
	
}