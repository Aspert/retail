<?php

require_once dirname(FCPATH) . '/system/application/libraries/adaptadores/ExcelImporterFactory.php';

/**
 *
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class ExcelDB extends GenericModel {
	### START
	protected function _initialize(){
		$this->addField('ID_EXCEL','int','',1,1);
		$this->addField('ID_SEGMENTO','int','',0,0);
		$this->addField('ID_JOB','int','',1,0);
		$this->addField('ID_TEMPLATE','int','',1,0);
		$this->addField('DESC_EXCEL','string','',23,0);
		$this->addField('OBSERVACOES_EXCEL','blob','',0,0);
		$this->addField('DT_CADASTRO_EXCEL','datetime','',19,0);
		$this->addField('NUMERO_JOB_EXCEL','string','',5,0);
		$this->addField('NOVO_EXCEL','int','',1,0);
	}
	### END

/**
	 * nome da tabela
	 * @var string
	 */
	public $tableName = 'TB_EXCEL';

	/**
	 * Linha inicial de leitura
	 * @var int
	 */
	const ROW_INIT_ITEMS = 5;

	/**
	 * Celula do codigo do job
	 * @var string
	 */
	const CELL_JOB_NUMBER = 'B1';

	/**
	 * Mapeamento das colunas
	 * @var array
	 */
	private $map = array(
		'ORDEM_ITEM' => 1,
		'PAGINA_ITEM' => 2,
		'SUBCATEGORIA' => 3,
		'PRODUTO' => 4,
		'DESCRICAO' => 5,
		'TEXTO_LEGAL' => 6,
		'PRECO_UNITARIO' => 7,
		'TIPO_UNITARIO' => 8,
		'PRECO_CAIXA' => 9,
		'TIPO_CAIXA' => 10,
		'PRECO_VISTA' => 11,
		'PRECO_DE' => 12,
		'PRECO_POR' => 13,
		'ECONOMIZE' => 14,
		'ENTRADA' => 15,
		'PARCELAS' => 16,
		'PRESTACAO' => 17,
		'JUROS_AM' => 18,
		'JUROS_AA' => 19,
		'TOTAL_PRAZO' => 20,
		'OBSERVACOES_ITEM' => 21,
		'EXTRA1_ITEM' => 22,
		'EXTRA2_ITEM' => 23,
		'EXTRA3_ITEM' => 24,
		'EXTRA4_ITEM' => 25,
		'EXTRA5_ITEM' => 26,
		'ID_FICHA_PAI' => 27,
	);

	/**
	 * Dados do formulario
	 * @var array
	 */
	private $_formData = array();

	/**
	 * Dados do excel
	 * @var array
	 */
	private $_xlsData = array();
	/**
	 * codigo do job
	 * @var string
	 */
	private $_jobID = '';
	/**
	 * texto legal do job
	 * @var texto legal
	 */
	private $_textoLegal = '';

	/**
	 * Erros encontrados na analise do excel
	 * @var array
	 */
	private $errorsList = array();
	/**
	 * Codigos regionais das fichas na validacao
	 * @var array
	 */
	private $codigosRegionaisValidacao = array();

	/**
	 * Fichas encontradas
	 * @var array
	 */
	private $cacheFichas = array();

	/**
	 * Contrutor
	 * @author Hugo Silva
	 * @return ExcelDB
	 */
	function __construct(){
		parent::GenericModel();

		set_time_limit(0);

		//$this->addValidation('ID_SEGMENTO','requiredNumber','Informe o segmento');
		//$this->addValidation('ID_TEMPLATE','requiredNumber','Informe o template');
		//$this->addValidation('ID_TEMPLATE','function', array($this,'checaDimensaoIndd'));
		//$this->addValidation('NUMERO_JOB_EXCEL','requiredString', 'Informe o número do job');
		$this->addValidation('NUMERO_JOB_EXCEL','function', array($this,'checaAcentuacao'));

	}

	public function checaAcentuacao($data){
		if(isset($data['NUMERO_JOB_EXCEL'])){
			if( preg_match('@[\\\/\^\?\:|*\(\)\%\$\#\@\!\-\+\=\,\.\;]+@', $data['NUMERO_JOB_EXCEL']) ){
				return 'O número do job não pode conter caracteres especiais';
			}
		}

		return true;
	}

	/**
	 * (non-PHPdoc)
	 * @see system/application/libraries/GenericModel#getById($id)
	 */
	public function getById($id){
		$rs = $this->db->from($this->tableName.' E')
			->join('TB_JOB J','E.ID_JOB = J.ID_JOB')
			->join('TB_CAMPANHA CA', 'CA.ID_CAMPANHA = J.ID_CAMPANHA')
			->join('TB_PRODUTO P', 'P.ID_PRODUTO = CA.ID_PRODUTO')
			->join('TB_CLIENTE C', 'C.ID_CLIENTE = J.ID_CLIENTE')
			->join('TB_AGENCIA AG', 'J.ID_AGENCIA = AG.ID_AGENCIA')
			->where('E.ID_EXCEL', $id)
			->get();

		return $rs->row_array();
	}

	/**
	 * Retorna um excel reader para um arquivo
	 *
	 * @author Hugo Ferreira da Silva
	 * @param string $filename
	 * @return ExcelReader
	 */
	public function getReader($filename){
		if( empty($this->readers[ $filename ]) ){
			$reader = null;

			try {
				$reader = new ExcelReader($filename);
				$reader->open();
				$this->readers[ $filename ] = $reader;

			} catch( Exception $e ) {
				die('Excecao: ' . $e->getMessage() );
			}

		}

		return $this->readers[ $filename ];
	}
	
	/**
	 * Retorna o IdExcel de um IdJob
	 *
	 * @author Bruno de Oliveira
	 * @param int $idJob
	 * @return int $idExcel
	 */
	public function getIdExcelByIdJob($idJob){
		$this->db
				->from('TB_JOB J')
				->select('E.ID_EXCEL') 
				->join('TB_EXCEL E','J.ID_JOB = E.ID_JOB')
				->where('J.ID_JOB',$idJob);
		
		$result = $this->db->get()->result_array();
		$result = $result[0]['ID_EXCEL'];
				
		return $result;
	}

	/**
	 * Lista os itens cadastrados
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $filters array com os filtros
	 * @param int $page numero da pagina
	 * @param int $limit numero de itens por pagina
	 * @return array
	 */
	public function listItems($filters, $page=0, $limit=5, $order='DESC_AGENCIA,DESC_CLIENTE,DESC_PRODUTO,DESC_CAMPANHA,DESC_EXCEL', $dir = 'ASC'){

		$this->db
			->select('*')
			->select('(SELECT DATA_HISTORICO FROM TB_JOB_HISTORICO H WHERE H.ID_JOB=B.ID_JOB ORDER BY ID_JOB_HISTORICO DESC LIMIT 1) AS DATA_MUDANCA_ETAPA')
			->select('(SELECT COUNT(*) FROM TB_APROVACAO AP WHERE AP.ID_JOB = B.ID_JOB AND ENVIADO_APROVACAO = 1) AS PROVAS')
			->join('TB_JOB B','B.ID_JOB=TB_EXCEL.ID_JOB')
			->join('TB_CAMPANHA C','C.ID_CAMPANHA = B.ID_CAMPANHA')
			->join('TB_PRODUTO P','P.ID_PRODUTO=C.ID_PRODUTO')
			->join('TB_CLIENTE CL','CL.ID_CLIENTE = P.ID_CLIENTE')
			->join('TB_AGENCIA A','B.ID_AGENCIA = A.ID_AGENCIA')
			->join('TB_TIPO_PECA TP','TP.ID_TIPO_PECA = B.ID_TIPO_PECA')
			->join('TB_TIPO_JOB TJ','TJ.ID_TIPO_JOB = B.ID_TIPO_JOB')
			->join('TB_STATUS S', 'S.ID_STATUS = B.ID_STATUS','LEFT')
			->join('TB_ETAPA E', 'E.ID_ETAPA = B.ID_ETAPA')
			;

		if(!empty($filters['ID_AGENCIA'])){
			$this->db->where('A.ID_AGENCIA',$filters['ID_AGENCIA']);
		}
		if(!empty($filters['ID_CLIENTE'])){
			$this->db->where('CL.ID_CLIENTE',$filters['ID_CLIENTE']);
		}
		if(!empty($filters['ID_PRODUTO'])){
			$this->db->where('P.ID_PRODUTO',$filters['ID_PRODUTO']);
		}
		if(!empty($filters['ID_CAMPANHA'])){
			$this->db->where('C.ID_CAMPANHA',$filters['ID_CAMPANHA']);
		}
		if(!empty($filters['ID_TIPO_JOB'])){
			$this->db->where('B.ID_TIPO_JOB',$filters['ID_TIPO_JOB']);
		}
		if(!empty($filters['ID_TIPO_PECA'])){
			$this->db->where('B.ID_TIPO_PECA',$filters['ID_TIPO_PECA']);
		}


		if( !empty($filters['COM_REVISAO']) ){
			$this->db->where('(SELECT COUNT(*) FROM TB_APROVACAO AP WHERE AP.ID_JOB = B.ID_JOB AND AP.ENVIADO_APROVACAO = 1) > 0');
		} else {
			if(!empty($filters['ID_ETAPA']) && !is_array($filters['ID_ETAPA'])){
				$this->db->where('B.ID_ETAPA',$filters['ID_ETAPA']);
			}else if(!empty($filters['ID_ETAPA']) && is_array($filters['ID_ETAPA']) ){
				$this->db->where_in('B.ID_ETAPA',$filters['ID_ETAPA']);
			}
		}


		if(!empty($filters['PRODUTOS'])){
			$this->db->where_in('P.ID_PRODUTO',$filters['PRODUTOS']);
		}

		if(!empty($filters['STATUS_JOB'])){
			$this->db->where('B.ID_STATUS',$filters['STATUS_JOB']);
		}

		if(!empty($filters['STATUS_CAMPANHA']) && $filters['STATUS_CAMPANHA'] == 1){
			$this->db->where('C.STATUS_CAMPANHA',$filters['STATUS_CAMPANHA'])
				->where('C.DT_FIM_CAMPANHA >= CURRENT_DATE');
		}

		if(!empty($filters['DT_CADASTRO_EXCEL'])){
			$data = format_date($filters['DT_CADASTRO_EXCEL'],'Y-m-d');
			$this->db->where('DT_CADASTRO_EXCEL >=', $data.' 00:00:00');
			$this->db->where('DT_CADASTRO_EXCEL <=', $data.' 23:59:59');
			unset($filters['DT_CADASTRO_EXCEL']);
		}
		
		//Retira os jobs bloqueados, caso tenha vindo no filtro
		if(!empty($filters['NOT_ID_STATUS']) && is_array($filters['NOT_ID_STATUS'])) {
			$this->db->where_not_in('B.ID_STATUS', $filters['NOT_ID_STATUS'] );
		}

		$this->setWhereParams($filters);

		$rs = $this->execute($page, $limit, $order,$dir);

		return $rs;
	}

	/**
	 * Recupera um excel pelo numero do job da planilha
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $jobID Numero do job
	 * @param int $clienteID Codigo do cliente
	 * @return array
	 */
	public function getByJobCliente($jobID, $clienteID){
		$rs = $this->db->from($this->tableName.' E')
			->join('TB_JOB J','E.ID_JOB = J.ID_JOB')
			->join('TB_CAMPANHA CA', 'CA.ID_CAMPANHA = J.ID_CAMPANHA')
			->join('TB_PRODUTO P', 'P.ID_PRODUTO = CA.ID_PRODUTO')
			->join('TB_CLIENTE C', 'C.ID_CLIENTE = P.ID_CLIENTE')
			->where('E.NUMERO_JOB_EXCEL', $jobID)
			->where('C.ID_CLIENTE', $clienteID)
			->get();

		return $rs->row_array();
	}

	/**
	 * Metodo de importacao de planilha de excel
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data Dados do job e excel
	 * @param string $file Endereco do arquivo
	 * @return array Erros encontrados
	 * @throws Exception
	 */
	public function validateImport($data, $file){
		$user = Sessao::get('usuario');
		$retorno = array();
		
		// dados do formulario
		$this->_formData = $data;

		// erros encontrados
		$erros = array();
		

		// recupera a regiao
		$regiao_list = $this->regiao->getByProduto(val($data,'ID_PRODUTO',0));

		// se nao encontrou a regiao nem continua
		if( empty($regiao_list) ){
			$this->addMsgErro('','JOB','','','','A região do Job não foi encontrada');
			$erros['JOB'] = $this->errorsList;
			return $erros;
		}

		if( count($regiao_list) > 1){
			$this->addMsgErro('','JOB','','','','A bandeira deve estar somente em uma região!');
			$erros['JOB'] = $this->errorsList;
			return $erros;
		}

		// pega somente a primeira
		$regiao = $regiao_list[0];

		// indica a regiao no job
		$this->_formData['ID_REGIAO'] = $regiao['ID_REGIAO'];

		try {
			// pega o cliente
			$cliente = $this->cliente->getById( $data['ID_CLIENTE'] );

			if( empty($cliente) ){
				throw new Exception('Cliente nao informado/nao encontrado no banco: '.$data['ID_CLIENTE']);
			}

			// pega o leitor apropriado para o arquivo enviado
			$formatador = ExcelTransform::loadForClienteOrProduto($data['ID_CLIENTE'], $data['ID_PRODUTO']);
			$wb = ExcelImporterFactory::getImporter( $cliente['CLASSE_IMPORTADOR'] )->import($file, $formatador);
			// planilhas
			$sheets = $wb->getSheets();
			// numero total de planilhas
			$totalSheets = count($sheets);

			// para cada planilha
			for($i=0; $i<$totalSheets; $i++){
				// reinicia as mensagens de erro
				$this->errorsList = array();

				// carrega os dados da planilha
				$sheet = $wb->getSheetByIndex($i);

				if($i==0){
					// pega o numero do job
					$job = $sheet->getCellData(self::CELL_JOB_NUMBER);
					$job = trim($job);
					/*
					if(empty($job)){
						$this->addMsgErro(
							'',
							'',
							'',
							'',
							null,
							'Numero do JOB não informado na planilha'
						);
					} else {*/
						$this->_jobID = $job;
					//}
				}

				// pega o nome da praca
				$praca = $sheet->getSheetName();
				// pega o texto legal
				$textoLegal = $sheet->getCellData('B2');

				/*------------ Recuperando os itens ----------*/

				$linhaInicial = self::ROW_INIT_ITEMS;
				$linhaFinal = $sheet->getMaxRow();
				$values = array();
				$erroSemPaginaOrdem = false;
				$erroNumericoPaginaOrdem = false;

				// para cada linha encontrada na planilha
				for($j=$linhaInicial; $j<=$linhaFinal; $j++){
					$cod    = $sheet->getCellDataByColumnRow($this->map['PRODUTO']-1, $j);
					$pagina = $sheet->getCellDataByColumnRow($this->map['PAGINA_ITEM']-1, $j);
					$ordem  = $sheet->getCellDataByColumnRow($this->map['ORDEM_ITEM']-1, $j);

					// se nao informou o  codigo
					// vai para o proximo item
					if($cod != ''){
						if( empty($pagina) || empty($ordem)) {
							$erroSemPaginaOrdem = true;
							continue;
						}
						else{
							if( !is_numeric($pagina) || !is_numeric($ordem)) {
								$erroNumericoPaginaOrdem = true;
								continue;
							}
						}
					}

					if( empty($cod) ){
						continue;
					}

					// pega as celulas conforme o mapeamento
					foreach($this->map as $name => $column) {
						// grava os valores em um array
						$values[$j][$name] = $sheet->getCellDataByColumnRow($column-1, $j);
					}
				}

				// armazenando os dados do excel
				$this->_xlsData[$praca]['ITENS'] = $values;
				$this->_xlsData[$praca]['TEXTO_LEGAL'] = $textoLegal . '';

				// vamos ver se a praca existe
				$objPraca = $this->praca->getByDescricaoCliente($praca, $this->_formData['ID_CLIENTE'], $regiao['ID_REGIAO']);

				// se nao existe
				if( empty($objPraca) ){
					$this->addMsgErro('','','','',null,'A praça '.$praca.' informada no Excel não foi escolhida para o Job');
					$erros[$praca] = $this->errorsList;
					// nem valida os itens
					continue;

				} 
				else{ 
					if( $erroSemPaginaOrdem ){
						$this->addMsgErro('','','','',null,'Página ou Ordem não informado para a praça '.$praca.'!');
						$erros[$praca] = $this->errorsList;
						continue;
					}
					else{
						if( $erroNumericoPaginaOrdem ){
							$this->addMsgErro('','','','',null,'Página e Ordem da praça '.$praca.' devem conter apenas valores numéricos!');
							$erros[$praca] = $this->errorsList;
							continue;
						}
					}
					
					// se nao estiver ativa
					if( $objPraca['STATUS_PRACA'] != 1 ){
						$this->addMsgErro('','','','',null,'A praça '.$praca.' não está ativa!');
						$erros[$praca] = $this->errorsList;
						// nem valida os itens
						continue;
					}

					// se nao for a regiao do job
					if( $objPraca['ID_REGIAO'] != $regiao['ID_REGIAO'] ){
						// informa que nao pode
						$this->addMsgErro('','','','',null,'A praça '.$praca.' não pertence a mesma região do Job');
						$erros[$praca] = $this->errorsList;
						// nem valida os itens
						continue;
					}
					if($this->_formData['CADASTRAR_EM_LOTE'] == 1){
						$c = $this->cliente->getById($this->_formData['ID_CLIENTE']);
						if(!is_numeric($c['SUBCATEGORIA_LOTE']) || $c['SUBCATEGORIA_LOTE'] == 0){
							$this->addMsgErro('','','','',null,'Não existe subcategoria cadastrada para o cliente para realizar a inserção em lote.');
							$erros[$praca] = $this->errorsList;
							// nem valida os itens
							continue;
						}
					}
				}

				//aqui fazemos uma validação para ver se não existe produtos repeditos na planilha
				$codigosNaoDuplicados = 0;
				if(isset($this->_formData['FICHAS_NAO_ENCONTRADAS']) && $this->_formData['CADASTRAR_EM_LOTE'] == 1){
					$codigosNaoDuplicados = array_values(array_unique($this->_formData['FICHAS_NAO_ENCONTRADAS']));
				}
				// cadastro em lotes
				$fichasAdicionadas = array();
				if($this->_formData['CADASTRAR_EM_LOTE'] == 1){

					if(count($codigosNaoDuplicados) > 0){
						foreach ($codigosNaoDuplicados as $fnc){
							foreach($values as $v){
								if($fnc == $v['PRODUTO']){
									//gambi honesta pois haviam varios casos onde o mesmo codigo de barras se repetia em praças diferentes na mesma planilha
									$consulta =  $this->ficha->getByClienteCodigoBarras($this->_formData['ID_CLIENTE'],$fnc);
									$subcategoriaLote = $this->cliente->getById($this->_formData['ID_CLIENTE']);
									$subcategoriaLote = $c['SUBCATEGORIA_LOTE'];
									$categoriaLote = $this->subcategoria->getById($subcategoriaLote);
									$categoriaLote = $categoriaLote['ID_CATEGORIA'];
									
									$fichaLote = array();
									$fichaLote['ID_USUARIO'] = $user['ID_USUARIO'];
									$fichaLote['COD_FICHA'] = $fnc;
									$fichaLote['COD_BARRAS_FICHA'] = $fnc;
									$fichaLote['NOME_FICHA'] = $v['DESCRICAO'];
									$fichaLote['ID_CATEGORIA'] = $categoriaLote;
									$fichaLote['ID_SUBCATEGORIA'] = $subcategoriaLote;
									$fichaLote['FLAG_ACTIVE_FICHA'] = 1;
									$fichaLote['ID_APROVACAO_FICHA'] = 1;
									$fichaLote['TIPO_COMBO_FICHA'] = 1;
									$fichaLote['FLAG_TEMPORARIO'] = 1;
									$fichaLote['ID_CLIENTE'] = $this->_formData['ID_CLIENTE'];
									$fichaLote['TIPO_FICHA'] = 'PRODUTO';
									$fichaLote['PENDENCIAS_DADOS_BASICOS'] = 'Ficha cadastrada em lote';
										
										
									if(empty($consulta)){
										$idFicha = $this->ficha->save($fichaLote);
										
										$fichaCodigo = array();
										$fichaCodigo[$this->_formData['ID_REGIAO']] = $v['DESCRICAO'];
										
										$this->ficha->salvarCodigosRegionais($idFicha,$fichaCodigo);
										$fa['ID_FICHA'] = $idFicha;
									}
									
									$fa['COD_BARRAS_FICHA'] = $fnc;
									$fa['NOME_FICHA'] = $v['DESCRICAO'];
									$fa['ORDEM'] = $v['ORDEM_ITEM'];
									$fa['PAGINA'] = $v['PAGINA_ITEM'];
									$fa['PRODUTO'] = $v['PRODUTO'];

									$this->db->from('TB_OBJETO');
									$this->db->where('ID_CLIENTE', $this->_formData['ID_CLIENTE']);
									
									if($user['IS_CLIENTE_PAODEACUCAR'] == 1){
										$this->db->like('TRIM(FILENAME)', $fnc.'\_', 'after');
										$this->db->or_like('TRIM(FILENAME)', $fnc.'\.', 'after');
									} else {
										$this->db->like('TRIM(FILENAME)', $fnc.'\.', 'after');
									}
									
									$obj = $this->db->get()->result_array();

									if(empty($consulta)){
										if(count($obj) > 0){
											$objetoFicha = array();
											$objetoFicha['ID_OBJETO'] = $obj[0]['ID_OBJETO'];
											$objetoFicha['ID_FICHA'] = $idFicha;
											$objetoFicha['PRINCIPAL'] = 1;
											$this->objeto_ficha->save($objetoFicha);
											$fa['OBJETO'] = $obj[0]['FILENAME'];
										}
										
										//caso não encontre o objeto, salva um objeto defaulf
										else{
											//adicionado um TRIM na busca do filename pois alguns casos o nome da ficha tinha espaço antes do nome
											$obj1 = $this->db->from('TB_OBJETO')
											->where('TRIM(FILENAME)', 'PENDENTE.PSD')
											->get()
											->result_array();
											//echo $this->db->last_query();die;
											
											if(count($obj1) > 0){
												$objetoFicha = array();
												$objetoFicha['ID_OBJETO'] = $obj1[0]['ID_OBJETO'];
												$objetoFicha['ID_FICHA'] = $idFicha;
												$objetoFicha['PRINCIPAL'] = 1;
												$this->objeto_ficha->save($objetoFicha);
												$fa['OBJETO'] = $obj1[0]['FILENAME'];
											} else {
												$fa['OBJETO'] = 'Não foi encontrado um objeto para a ficha';
											}
										}
									}
									
									$fichasAdicionadas[] = $fa;

									break;
								}
							}
						}						
					}
				}
				// agora ja temos todos
				// os produtos desta praca
				// vamos validar!
				
				$this->validateItems($values, $regiao['ID_REGIAO']);

				// indicamos que o resultado e para a praca
				$result = array();
				$result[ $praca ] = $this->errorsList;

				// adiciona as mensagens de erro
				$erros = array_merge($erros, $result);
				
				// liberando memoria
				unset($result);
				
				// se houverem erros na lista
				if( !empty($this->errorsList) ){
					// indicamos que o resultado e para a praca
					$result = array();
					$result[ $praca ] = $this->errorsList;
	
					// adiciona as mensagens de erro
					$erros = array_merge($erros, $result);
				}

				foreach($erros as $key => $value){
					if(isset($retorno[$key]) && !empty($retorno[$key])){
						continue;
					}
					$retorno[$key] = $value;
					$retorno[$key]['fichas_adicionadas'] = $fichasAdicionadas;
				}
			}

		} catch (Exception $e) {
			throw $e;
		}	
		
		// se tiver regioes encontradas
		if( !empty($regioes) ){
			// indica a regiao no form data
			$this->_formData['ID_REGIAO'] = $regioes[0];
		}

		//aqui ordenamos o array primeiro pela página, depois pela ordem
		//alteração solicitada pelo chamado 33338
		foreach($retorno as $key1 => $er){
		
			if(isset($retorno[$key1]['avisos'])){
				$pracaItensAvisos = $retorno[$key1]['avisos'];
				$pracaOrdenadaAvisos = $this->ordenar_array($pracaItensAvisos, 'PAGINA', SORT_ASC, 'ORDEM', SORT_ASC);
				$retorno[$key1]['avisos'] = $pracaOrdenadaAvisos;
			}
		
			if(isset($retorno[$key1]['erros'])){
				$pracaItensErros = $retorno[$key1]['erros'];
				$pracaOrdenadaErros = $this->ordenar_array($pracaItensErros, 'PAGINA', SORT_ASC, 'ORDEM', SORT_ASC);
				$retorno[$key1]['erros'] = $pracaOrdenadaErros;
			}
		
		}
		
		foreach($erros as $key2 => $err){
			
			if(isset($erros[$key2]['avisos'])){
				$pracaItensAvisos = $erros[$key2]['avisos'];
				$pracaOrdenadaAvisos = $this->ordenar_array($pracaItensAvisos, 'PAGINA', SORT_ASC, 'ORDEM', SORT_ASC);
				$erros[$key2]['avisos']= $pracaOrdenadaAvisos;
			}
		
			if(isset($erros[$key2]['erros'])){
				$pracaItensErros = $erros[$key2]['erros'];
				$pracaOrdenadaErros = $this->ordenar_array($pracaItensErros, 'PAGINA', SORT_ASC, 'ORDEM', SORT_ASC);
				$erros[$key2]['erros'] = $pracaOrdenadaErros;
			}
			
		}
		
		//print_rr($erros);die;
		
		if(!isset($retorno) || $this->_formData['CADASTRAR_EM_LOTE'] == 0){
			return $erros;
		} else {
			// retorna o resultado da validacao
			return $retorno;
		}
	}

	/**
	 * Salva os dados do excel
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return int codigo do excel
	 */
	public function saveData( $idjob = null ){

		if(empty($this->_formData) || empty($this->_xlsData)){
			throw new Exception('A validação ainda não foi efetuada');
		}

		/* -------------------- salvando os dados do excel ------------ */
		$id     = $this->saveExcel( $idjob );
		$versao = $this->excelVersao->getNewVersion($id);
		$excel  = $this->getById($id);

		/* -------------------- salvando os itens do excel ------------ */
		// lista de itens salvos
		$listItems = array();

		// codigos das subcategorias
		$subcategorias = array();

		// para cada praca
		foreach($this->_xlsData as $desc => $data){
			// pega os dados da praca
			$pracaData = $this->praca->getByDescricaoCliente($desc, $this->_formData['ID_CLIENTE']);

			// salva os dados do excel na praca
			$this->saveExcelPraca($id, $pracaData['ID_PRACA'], $versao, array('TEXTO_LEGAL'=>$data['TEXTO_LEGAL']));
			
			// codigo da praca
			$idpraca = $pracaData['ID_PRACA'];

			$tempItens = array();
			
			// tratamos as fichas que contem subfichas que vem com barras "/" no campo da planilha
			$paginaAnterior = null;
			$ordemAnterior = null;
			$countLinhaExcel = 1;

			$primeiraChave = 0;
			foreach($data['ITENS'] as $key => $value){
				$primeiraChave = $key;
				break;
			}
					//print_rr($data['ITENS']);die;
			for($x = $primeiraChave; $x <= count($data['ITENS']) + $primeiraChave - 1; $x++){
				//validação para o caso de terem deixado o codigo de barras em branco na planilha
				$data['ITENS'][$x]['SUBFICHAS'] = array();
				if(isset($data['ITENS'][$x]['PRODUTO'])){
					if( ($paginaAnterior != $data['ITENS'][$x]['PAGINA_ITEM']) || ($ordemAnterior != $data['ITENS'][$x]['ORDEM_ITEM']) ){
						$c = 0;
						
						if((isset($data['ITENS'][$x]['PRODUTO'])) && strpos($data['ITENS'][$x]['PRODUTO'], "/")){
							$arr = explode("/", $data['ITENS'][$x]['PRODUTO']);
		
							$data['ITENS'][$x]['PRODUTO'] = $arr[0];
							$data['ITENS'][$x]['LINHA_EXCEL'] = $countLinhaExcel;
							for($i = 0; $i <= count($arr) - 1; $i++){
								if($i > 0){ 
									$data['ITENS'][$x]['SUBFICHAS'][$arr[$i]] = $data['ITENS'][$x]; 
									$data['ITENS'][$x]['SUBFICHAS'][$arr[$i]]['PRODUTO'] = $arr[$i];
									$data['ITENS'][$x]['SUBFICHAS'][$arr[$i]]['LINHA_EXCEL'] = $countLinhaExcel;
								}
							}
						}
						else{
							$data['ITENS'][$x]['LINHA_EXCEL'] = $countLinhaExcel;
						}	
						$countLinhaExcel++;				
						$paginaAnterior = $data['ITENS'][$x]['PAGINA_ITEM'];
						$ordemAnterior = $data['ITENS'][$x]['ORDEM_ITEM'];
					}
					else if( ($paginaAnterior == $data['ITENS'][$x]['PAGINA_ITEM']) && ($ordemAnterior == $data['ITENS'][$x]['ORDEM_ITEM']) ){
						if(strpos($data['ITENS'][$x]['PRODUTO'], "/")){
							$arr = explode("/", $data['ITENS'][$x]['PRODUTO']);
							for($i = 0; $i <= count($arr) - 1; $i++){						
								$data['ITENS'][$x -1 - $c]['SUBFICHAS'][$arr[$i]] = $data['ITENS'][$x];
								$data['ITENS'][$x -1 - $c]['SUBFICHAS'][$arr[$i]]['PRODUTO'] = $arr[$i];
								$data['ITENS'][$x -1 - $c]['SUBFICHAS'][$arr[$i]]['LINHA_EXCEL'] = $countLinhaExcel;
							}
							
							$paginaAnterior = $data['ITENS'][$x]['PAGINA_ITEM'];
							$ordemAnterior = $data['ITENS'][$x]['ORDEM_ITEM'];
							
							unset($data['ITENS'][$x]);
							$primeiraChave++;
							$c++;
						}
						else{
							$data['ITENS'][$x]['LINHA_EXCEL'] = $countLinhaExcel;
							$data['ITENS'][$x - 1 - $c]['SUBFICHAS'][] = $data['ITENS'][$x];
							$paginaAnterior = $data['ITENS'][$x]['PAGINA_ITEM'];
							$ordemAnterior = $data['ITENS'][$x]['ORDEM_ITEM'];
							unset($data['ITENS'][$x]);
							$primeiraChave++;
							$c++;
						}
						$countLinhaExcel++;	
					}
				}
			}

			$data['ITENS'] = realocarArray($data['ITENS']);

			// para cada item da praca
			foreach($data['ITENS'] as $chave => $row){
				//se não tiver codigo de barras não continua
				if(!isset($row['PRODUTO'])){
					continue;
				}
				// se a ficha tiver um codigo
				if(!empty($row['PRODUTO']) && isset($this->cacheFichas[$row['PRODUTO']])){
					// pega a ficha
					//$ficha = $this->ficha->getFichaClienteProduto($excel['ID_CLIENTE'], $row['PRODUTO']);
					$ficha = $this->cacheFichas[$row['PRODUTO']];
					$row['ID_FICHA'] = $ficha['ID_FICHA'];
					$row['CODIGO_ITEM'] = $row['PRODUTO'];
					$row['DESCRICAO_ITEM'] = $row['DESCRICAO'];
					$row['SUBCATEGORIA'] = $ficha['ID_SUBCATEGORIA'];
					if(isset($ficha['ID_FICHA_PAI'])){
						$row['ID_FICHA_PAI'] = $ficha['ID_FICHA_PAI'];
					}

					// pegamos o codigo da subcategoria
					$idsub = $ficha['ID_SUBCATEGORIA'];
					// pegamos a pagina que o usuario indicou
					$pagina = sprintf('%d', $row['PAGINA_ITEM']);
					// se a pagina for zero
					if( $pagina <= 0 ){
						// colocamos na pagina um
						$pagina = 1;
					}

					if(!isset($ficha['ID_FICHA_PAI'])){
						// guardamos os codigos das subcategorias
						// indicamos quantos itens vao e a pagina
						if( !isset($subcategorias[$idpraca][$idsub][$pagina]) ){
							$subcategorias[$idpraca][$idsub][$pagina] = 0;
						}
						// incrementamos o numero de elementos da subcategoria na pagina
						$subcategorias[$idpraca][$idsub][$pagina]++;
					}
				}

				// indica o excel
				$row['ID_EXCEL'] = $id;
				// indica a praca
				$row['ID_PRACA'] = $pracaData['ID_PRACA'];
				// indica a qual versao pertence
				$row['ID_EXCEL_VERSAO'] = $versao;
				// status do item
				$row['STATUS_ITEM'] = 1;
				// observacoes do planilha (para gravar na tabela pricing)
				//calcula e insere as parcelas dinamicas caso existam
				$parcelaDinamica = $this->parcela_dinamica->calculaParcelaDinamica($excel['ID_CLIENTE'], @$row['PRECO_VISTA']);
				if(count($parcelaDinamica) > 0){
					$row['PRESTACAO'] = $parcelaDinamica['PRESTACAO'];
					$row['PARCELAS'] = $parcelaDinamica['PARCELAS'];
					$row['PRECO_VISTA'] = $parcelaDinamica['PRECO_VISTA'];
				}
				
				$row['OBSERVACOES'] = $row['OBSERVACOES_ITEM'];
				// limpa observacoes item para nao gravar na planilha excel item
				// este campo deve ser utilizado apenas na logistica.
				unset($row['OBSERVACOES_ITEM']);
				// coloca na lista de itens temporarios
				$tempItens[] = $row;
			}
			$tempSubfichas = array();

			for($i = 0; $i <= count($tempItens) - 1; $i++){
				if(isset($tempItens[$i]['SUBFICHAS'])){
					foreach($tempItens[$i]['SUBFICHAS'] as $s){
						$ficha = $this->cacheFichas[$tempItens[$i]['PRODUTO']];
						$subficha = $this->ficha->getSubichasClienteProdutos($ficha['ID_CLIENTE'], array('0'=>$s['PRODUTO']));
						$subficha = $subficha[$s['PRODUTO']];
						$s['SUBCATEGORIA'] = $ficha['ID_SUBCATEGORIA'];
						$s['ID_FICHA'] = $subficha['ID_FICHA'];
						$s['CODIGO_ITEM'] = $s['PRODUTO'];
						$s['ID_EXCEL'] = $tempItens[$i]['ID_EXCEL'];
						$s['ID_PRACA'] = $tempItens[$i]['ID_PRACA'];
						$s['ID_EXCEL_VERSAO'] = $tempItens[$i]['ID_EXCEL_VERSAO'];
						$s['STATUS_ITEM'] = $tempItens[$i]['STATUS_ITEM'];				
						$s['ID_FICHA_PAI'] = $tempItens[$i]['ID_FICHA'];
						//calcula e insere as parcelas dinamicas caso existam
						$parcelaDinamica = $this->parcela_dinamica->calculaParcelaDinamica($excel['ID_CLIENTE'], $s['PRECO_VISTA']);
						if(count($parcelaDinamica) > 0){
							$s['PRESTACAO'] = $parcelaDinamica['PRESTACAO'];
							$s['PARCELAS'] = $parcelaDinamica['PARCELAS'];
							$s['PRECO_VISTA'] = $parcelaDinamica['PRECO_VISTA'];
						}	
						unset($s['SUBFICHAS']);
						
						$tempSubfichas[] = $s;
	
					}
				}
				unset($tempItens[$i]['SUBFICHAS']);
			}

			// enquanto houverem itens para serem salvos
			while( !empty($tempItens) ){
				$partial = array_splice($tempItens, 0, 300);
				$partial = $this->excelItem->saveItems($partial);
				$this->pricing->savePricingsExcel($partial);
			}

			while( !empty($tempSubfichas) ){
				$partial = array_splice($tempSubfichas, 0, 300);
				$partial = $this->excelItem->saveItems($partial);
				$this->pricing->savePricingsExcel($partial);
			}

			// agora, desabilitamos os itens que nao estavam na ficha nesta praca
			$this->excelItem->desativaPorVersaoPraca($versao, $pracaData['ID_PRACA']);
		}

		// salvamos o plano de markting pela importacao
		$this->job->savePlanoMidiaByImportacao($excel['ID_JOB'], $versao, $subcategorias);

		// retorna o id do excel
		return $id;
	}

	/**
	 * Efetua todas as validacoes dos itens
	 *
	 * 15/07/2010 - As mensagens de erro sao adicionadas com o metodo addMsgErro
	 * 15/07/2010 - Recupera a regiao da ficha para exibir o codigo regional
	 *
	 * @see ExcelDB::addMsgErro
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $list Lista contendo todos os itens de uma planilha
	 * @param int $idRegiao Codigo da regiao do job
	 * @param str $tipoImportacao adicionar ou substituir
	 * @return void
	 */
	private function validateItems($list, $idRegiao = null, $tipoImportacao = null){
		
		if(!isset($_POST['ID_TIPO_PECA'])){
			$serializedData = $_POST['post'];
			$post = array();
			parse_str($serializedData,$post);
			$_POST['ID_TIPO_PECA'] = $post['ID_TIPO_PECA'];
		}
		// codigos dos itens
		$codigos = array();
			
		// resultado da validacao
		$resultado = array();
		
		// ordens
		$ordens = array();
		
		// tipo de peca
		$idTipoPeca = isset($_POST['ID_TIPO_PECA']) ? $_POST['ID_TIPO_PECA'] : null ;
		
		// armazena a maior pagina especificada na planilha
		$maiorPaginaPlanilha = 0;

		$cliente = !empty($this->_formData['ID_CLIENTE']) ? $this->_formData['ID_CLIENTE'] : 0;

		// codigos de ficha
		$codes = array('0');

		// tratamos as fichas que contem subfichas que vem com barras "/" no campo da planilha
		$paginaAnterior = null;
		$ordemAnterior = null;
		
		$primeiraChave = 0;
		foreach($list as $key => $value){
			$primeiraChave = $key;
			break;
		}
		
		$listOriginal = $list;

		// isso aki faz um verdadeiro furducuo para organizar mais ou menos a as fichas em array
		// da form que veem da planilha, eh coisa da hermes				
		for($x = $primeiraChave; $x <= count($list) + $primeiraChave - 1; $x++){
			if(isset($list[$x])){
				if(is_numeric($list[$x]['PAGINA_ITEM']) && is_numeric($list[$x]['ORDEM_ITEM']) && $list[$x]['PRODUTO'] != ''){
					$list[$x]['SUBFICHAS'] = array();
		
					if( ($paginaAnterior != $list[$x]['PAGINA_ITEM']) || ($ordemAnterior != $list[$x]['ORDEM_ITEM']) ){
						$c = 0;
						if(strpos($list[$x]['PRODUTO'], "/")){
							$arr = explode("/", $list[$x]['PRODUTO']);
		
							$list[$x]['PRODUTO'] = $arr[0];
							
							for($i = 0; $i <= count($arr) - 1; $i++){
								if($i > 0){ 
									$list[$x]['SUBFICHAS'][] = $arr[$i]; 
								}
							}
						}				
						$paginaAnterior = $list[$x]['PAGINA_ITEM'];
						$ordemAnterior = $list[$x]['ORDEM_ITEM'];
					}
					else if( ($paginaAnterior == $list[$x]['PAGINA_ITEM']) && ($ordemAnterior == $list[$x]['ORDEM_ITEM']) ){
						if(strpos($list[$x]['PRODUTO'], "/")){
							$arr = explode("/", $list[$x]['PRODUTO']);
							for($i = 0; $i <= count($arr) - 1; $i++){
								$list[$x - 1 - $c]['SUBFICHAS'][] = $arr[$i]; 
							}
							
							$paginaAnterior = $list[$x]['PAGINA_ITEM'];
							$ordemAnterior = $list[$x]['ORDEM_ITEM'];
							
							unset($list[$x]);
							$primeiraChave++;
							$c++;
						}
						else{
							$list[$x - 1 - $c]['SUBFICHAS'][] = $list[$x]['PRODUTO'];
							$paginaAnterior = $list[$x]['PAGINA_ITEM'];
							$ordemAnterior = $list[$x]['ORDEM_ITEM'];
							unset($list[$x]);
							$primeiraChave++;
							$c++;
						}
					}
				}
			}
		}

		$list = realocarArray($list);

		// para cada ficha
		foreach($list as $line => $data){
			if(is_numeric($data['PAGINA_ITEM'])){
				if( $maiorPaginaPlanilha < $data['PAGINA_ITEM'] ){
					$maiorPaginaPlanilha = $data['PAGINA_ITEM'];
				}
			}
			
			// se tem o codigo do produto
			if( !empty($data['PRODUTO']) ){
				// coloca na lista de codigos
				$codes[] = (string)$data['PRODUTO'];
			}
		}
		
		//  se tem menos paginas na planilha doque no job em si
		if($maiorPaginaPlanilha < $this->_formData['PAGINAS_JOB']){
			$this->addMsgErro('','JOB','','','','Planilha contém menos páginas que o job');
		}

		// remove os duplicados
		$codes = array_unique($codes);
		// pega todas as fichas
		$fichas = $this->ficha->getFichasClienteProdutos($cliente, $codes, $idRegiao);

		// fichas sem elementos
		$fichasComElementos = $this->ficha->hasElementosList($codes);

		// vamos colocar as fichas encontradas
		// no cache de fichas, por codigo
		foreach($fichas as $ficha){
			if( !array_key_exists($ficha['COD_BARRAS_FICHA'], $this->cacheFichas) ){
				$this->cacheFichas[$ficha['COD_BARRAS_FICHA']] = $ficha;
			}
		}

		// vamos checar os itens
		foreach($list as $line => $data){
					
			// o array de codigos é por pagina, pois o produto
			// pode se repetir em paginas diferentes
			if(!isset($codigos[$data['PAGINA_ITEM']])){
				$codigos[$data['PAGINA_ITEM']] = array();
			}

			// nome da ficha
			// trocamos com expressao regular no final
			// para mostrar a lista de pendencias.
			$nomeFicha = '{'.$data['PRODUTO'].':NOME_FICHA:'.$data['DESCRICAO'].'}';

			// primeiro, checamos a ordem
			if(!empty($ordens[$data['PAGINA_ITEM']][$data['ORDEM_ITEM']])){
				$subfichaEmLinha = false;
				
				// pegamos a referencia do produto que ja existe na planilha
				$referenciaExistente = $ordens[$data['PAGINA_ITEM']][$data['ORDEM_ITEM']];
				$referenciaExistente = substr($referenciaExistente, strpos($referenciaExistente, '(') + 1, -1);
				$referenciaExistente = (string)$referenciaExistente;

				// procura a ficha dentro jo array ja populado
				$idFichaExistente = null;
				$idFichaAtual = null;

				foreach($fichas as $key => $value){
					if($key == $referenciaExistente){
						$idFichaExistente = $value['ID_FICHA'];
						if(isset($fichas[$data['PRODUTO']])){
							$idFichaAtual = $fichas[$data['PRODUTO']]['ID_FICHA'];	
						}
						$this->cacheFichas[$data['PRODUTO']]['ID_FICHA_PAI'] = $idFichaExistente;	
						break;
					}
				}

				// verica se eh uma subficha em linha separada
				if(is_numeric($idFichaExistente) && is_numeric($idFichaAtual)){
					$idFichaPrincipal = $this->subficha->getIdFichaPrincipal($idFichaExistente, $idFichaAtual);
					if(($idFichaPrincipal == $idFichaExistente) || ($idFichaPrincipal == $idFichaAtual)){
						$subfichaEmLinha = true;
					}
				}

				if(!$subfichaEmLinha){
					if(empty($data['DESCRICAO']) && empty($data['PRODUTO'])){
						$this->addMsgErro(
							$data['PRODUTO'],
							$data['DESCRICAO'],
							$data['PAGINA_ITEM'],
							$data['ORDEM_ITEM'],
							null,
							'A ficha em branco na pagina '.$data['PAGINA_ITEM'].' e ordem ' . $data['ORDEM_ITEM'] . ' esta com a mesma ordem que o produto ' . $ordens[$data['PAGINA_ITEM']][$data['ORDEM_ITEM']]
						);
					} else {
						$this->addMsgErro(
							$data['PRODUTO'],
							$data['DESCRICAO'],
							$data['PAGINA_ITEM'],
							$data['ORDEM_ITEM'],
							null,
							'O produto '.$nomeFicha. ' (' . $data['PRODUTO'] . ') esta com a mesma ordem que o produto ' . $ordens[$data['PAGINA_ITEM']][$data['ORDEM_ITEM']]
						);
					}
				}
			} else {
				if(empty($data['DESCRICAO']) && empty($data['PRODUTO'])){
					$ordens[$data['PAGINA_ITEM']][$data['ORDEM_ITEM']] = '[ FICHA EM BRANCO ]';
				} else {
					$ordens[$data['PAGINA_ITEM']][$data['ORDEM_ITEM']] = $nomeFicha . ' ('.$data['PRODUTO'].')';
				}
			}

			// se nao tem codigo de produto
			// deixa continuar, porque eh uma ficha em branco
			if(empty($data['PRODUTO'])){
				continue;
			}

			if($data['PAGINA_ITEM'] > $this->_formData['PAGINAS_JOB']){
				$this->addMsgErro(
					$data['PRODUTO'],
					$data['DESCRICAO'],
					$data['PAGINA_ITEM'],
					$data['ORDEM_ITEM'],
					null,
					'O produto '.$nomeFicha. ' (' . $data['PRODUTO'] . ') esta em página inexistente no job'
				);
			}

			// se o produto esta repetido na mesma pagina da planilha
			if( !in_array($data['PRODUTO'], $codigos[$data['PAGINA_ITEM']]) ){
				$codigos[$data['PAGINA_ITEM']][] = $data['PRODUTO'];
			} else {
				$this->addMsgErro(
					$data['PRODUTO'],
					$data['DESCRICAO'],
					$data['PAGINA_ITEM'],
					$data['ORDEM_ITEM'],
					null,
					'O produto '.$nomeFicha. ' (' . $data['PRODUTO'] . ') se repete na mesma pagina',
					'avisos'
				);
			}


			$codigo = (string)$data['PRODUTO'];
			$ficha = !isset($fichas[$codigo]) ? array() : $fichas[$codigo];

			// se o produto nao tem ficha
			if(empty($ficha)){
				// adiciona a mensagem de erro
				$this->addMsgErro(
					$data['PRODUTO'],
					$data['DESCRICAO'],
					$data['PAGINA_ITEM'],
					$data['ORDEM_ITEM'],
					null,
					'O produto '.$nomeFicha. ' (' . $data['PRODUTO'] . ') nao possui ficha'
				);

			} else {
								
				// se tiver codigo regional
				if( !empty($ficha['CODIGO_REGIONAL']) ){
					// coloca na lista de codigos
					$this->codigosRegionaisValidacao[ $ficha['COD_BARRAS_FICHA'] ] = $ficha['CODIGO_REGIONAL'];
				}
				
				// SE A FICHA ESTIVER COM STATUS ATIVO
				if ($ficha['FLAG_ACTIVE_FICHA'] == 1) { 

					// SE A FICHA ESTIVER APROVADA
					if($ficha['ID_APROVACAO_FICHA'] == $this->aprovacao_ficha->getIdByKey('aprovado')){
						 
						//verifica se ha limites parametrizados para as subcategorias
						if( isset($ficha['ID_SUBCATEGORIA']) && is_numeric($ficha['ID_SUBCATEGORIA']) ){
							$limitesCategoria = $this->tipoPecaLimite->getByPecaAndSubcategoria($ficha['ID_SUBCATEGORIA'], $idTipoPeca);
							if(count($limitesCategoria) <= 0){
								$subcategoria = $this->subcategoria->getById($ficha['ID_SUBCATEGORIA']);
	
								$this->addMsgErro(
									$data['PRODUTO'],
									$data['DESCRICAO'],
									$data['PAGINA_ITEM'],
									$data['ORDEM_ITEM'],
									$ficha,
									'Não há limite parametrizado para a categoria ' .$subcategoria['DESC_CATEGORIA']. ' e subcategoria ' .$subcategoria['DESC_SUBCATEGORIA']. ' para este tipo de peça'
								);
							}							
						}
	
						// se for o tipo de importacao para adicionar novos itens
						if( ($tipoImportacao != null) && ($tipoImportacao == 'adicionar') ){
							// id excel atual
							$idExcel =  $_POST['ID_EXCEL'];
							
							// verifica se a pagina e ordem se repetem no job
							$mesmaPaginaOrdem = $this->excelItem->getByExcelPracaPaginaOrdem($idExcel, $data['ID_PRACA'], $data['PAGINA_ITEM'], $data['ORDEM_ITEM'], true);
							if(count($mesmaPaginaOrdem) > 0){
								$this->addMsgErro(
									$data['PRODUTO'],
									$data['DESCRICAO'],
									$data['PAGINA_ITEM'],
									$data['ORDEM_ITEM'],
									$ficha,
									'Pagina e ordem ja existente no job'
								);
							}
							
							// verifica se a ficha ja esta cadastrada
							$fichaNaPagina = $this->excelItem->getByExcelCodigoPracaPagina($idExcel, $data['PRODUTO'], $data['ID_PRACA'], $data['PAGINA_ITEM'], true);
							if(count($fichaNaPagina) > 0){
									$this->addMsgErro(
										$data['PRODUTO'],
										$data['DESCRICAO'],
										$data['PAGINA_ITEM'],
										$data['ORDEM_ITEM'],
										$ficha,
										'O produto '.$ficha['NOME_FICHA']. ' (' . $data['PRODUTO'] . ') se repete na mesma pagina',
										'avisos'
									);
							}
						}
						
						/*
						// se a ficha nao tem elementos
						if( !isset($fichasComElementos[ $ficha['ID_FICHA'] ]) ){
							$this->addMsgErro(
								$data['PRODUTO'],
								$data['DESCRICAO'],
								$data['PAGINA_ITEM'],
								$data['ORDEM_ITEM'],
								$ficha,
								'A ficha do produto '.$ficha['NOME_FICHA']. ' (' . $data['PRODUTO'] . ') não possui elementos layoutados'
							);
						}
						*/
	
						if($ficha['FLAG_TEMPORARIO'] == 1){
							$this->addMsgErro(
								$data['PRODUTO'],
								$data['DESCRICAO'],
								$data['PAGINA_ITEM'],
								$data['ORDEM_ITEM'],
								$ficha,
								'A ficha do produto '.$ficha['NOME_FICHA']. ' (' . $data['PRODUTO'] . ') está marcada como temporária',
								'avisos'
							);
						}
	
					// SE A FICHA ESTIVER REPROVADA
					} else if($ficha['ID_APROVACAO_FICHA'] == $this->aprovacao_ficha->getIdByKey('reprovado')){
						$this->addMsgErro(
							$data['PRODUTO'],
							$data['DESCRICAO'],
							$data['PAGINA_ITEM'],
							$data['ORDEM_ITEM'],
							$ficha,
							'A ficha do produto '.$ficha['NOME_FICHA']. ' (' . $data['PRODUTO'] . ') está reprovada'
						);
						
					// SE A FICHA ESTIVER EM APROVACAO
					} else if($ficha['ID_APROVACAO_FICHA'] == $this->aprovacao_ficha->getIdByKey('em_aprovacao')){
						$this->addMsgErro(
							$data['PRODUTO'],
							$data['DESCRICAO'],
							$data['PAGINA_ITEM'],
							$data['ORDEM_ITEM'],
							$ficha,
							'A ficha do produto '.$ficha['NOME_FICHA']. ' (' . $data['PRODUTO'] . ') está em aprovação'
						);
					}
				//SE A FICHA ESTIVER INATIVA
				} else {
					
					$this->addMsgErro(
						$data['PRODUTO'],
						$data['DESCRICAO'],
						$data['PAGINA_ITEM'],
						$data['ORDEM_ITEM'],
						$ficha,
						'A ficha do produto '.$ficha['NOME_FICHA']. ' (' . $data['PRODUTO'] . ') está inativa'
					);
					
				}
			}

			if(isset($data['SUBFICHAS']) && is_array($data['SUBFICHAS']) && count($data['SUBFICHAS']) > 0){
				// validamos as subfichas encontradas
				$subfichas = $this->ficha->getSubichasClienteProdutos($cliente, $data['SUBFICHAS'], $idRegiao);

				foreach($data['SUBFICHAS'] as $sub){

					// verifica se a subficha existe
					if(isset($subfichas[$sub])){
						// verifica se a subficha esta ativa
						if($subfichas[$sub]['FLAG_ACTIVE_FICHA'] == 0){
							$this->addMsgErro(
								$data['PRODUTO'],
								$data['DESCRICAO'],
								$data['PAGINA_ITEM'],
								$data['ORDEM_ITEM'],
								null,
								'A subficha ' .$sub. ' do produto '.$nomeFicha. ' (' . $data['PRODUTO'] . ') esta inativa'
							);
						}
						
						/*
						//verifica se a subficha esta relacionada a ficha
						if(isset($ficha['ID_FICHA']) && isset($subfichas[$sub]['ID_FICHA'])){
							$vinculo = $this->subficha->getByFichas($ficha['ID_FICHA'], $subfichas[$sub]['ID_FICHA']);
							if(count($vinculo) == 0){
								$this->addMsgErro(
									$data['PRODUTO'],
									$data['DESCRICAO'],
									$data['PAGINA_ITEM'],
									$data['ORDEM_ITEM'],
									null,
									'A subficha ' .$sub. ' não esta vinculada ao produto '.$nomeFicha. ' (' . $data['PRODUTO'] . ')'
								);
							}
						}
						*/
					}
					else{
						$nomeCliente = $this->cliente->getById($cliente);
						$nomeCliente = $nomeCliente['DESC_CLIENTE'];
						
						$subExiste = $this->subficha->getByClienteCodigo($cliente, $sub, true);

						if(count($subExiste) == 0){
							if((strtolower($nomeCliente) == 'ccz comunicacao') || (strtolower($nomeCliente) == 'condor') || (strtolower($nomeCliente) == 'leo madeiras') || (strtolower($nomeCliente) == 'praxis')) {
								$nomeSubficha = "";
								foreach ($listOriginal as $lo){
									if($lo['PRODUTO'] == $sub){
										$nomeSubficha = $lo['DESCRICAO'];
									}
								}
	
								$this->addMsgErro(
									$sub,
									$nomeSubficha,
									$data['PAGINA_ITEM'],
									$data['ORDEM_ITEM'],
									null,
									'A subficha do produto (' . $data['PRODUTO'] . ') ' .$data['DESCRICAO']. ' nao foi encontrada'
								);
							}
							else{
								$this->addMsgErro(
									$data['PRODUTO'],
									$data['DESCRICAO'],
									$data['PAGINA_ITEM'],
									$data['ORDEM_ITEM'],
									null,
									'A subficha ' .$sub. ' do produto '.$nomeFicha. ' (' . $data['PRODUTO'] . ') nao foi encontrada'
								);
							}
						}
						else{
							// Validação para verificar se a subficha (ficha) está vinculada a uma ficha
							/*$this->addMsgErro(
								$data['PRODUTO'],
								$data['DESCRICAO'],
								$data['PAGINA_ITEM'],
								$data['ORDEM_ITEM'],
								null,
								'A subficha ' .$sub. ' não esta vinculada ao produto '.$nomeFicha. ' (' . $data['PRODUTO'] . ')'
							);*/
						}
					}
				}	
			}
		}

		//gera um array com todos os codigos da planilha para
		//verificar duplicidades na planilha toda gerando avisos
		//$codigosFlat = (object) array('flat'=>array());
		//array_walk_recursive($codigos, create_function('&$v, $k, &$t', '$t->flat[] = $v;'), $codigosFlat);

		$codigosRepetidos = array();
		//itera pelos objetos de novo para ver se estao em paginas diferentes
		foreach($list as $line => $data){

			// se nao tiver codigo de produto, continua
			if(empty($data['PRODUTO'])){
				continue;
			}

			if(!isset($codigosRepetidos[$data['PRODUTO']])){
				$codigosRepetidos[$data['PRODUTO']] = $data['PAGINA_ITEM'];
			}else{
				if($data['PAGINA_ITEM'] != $codigosRepetidos[$data['PRODUTO']]){
					$this->addMsgErro(
						$data['PRODUTO'],
						$data['DESCRICAO'],
						$data['PAGINA_ITEM'],
						$data['ORDEM_ITEM'],
						$ficha,
						'O produto '.$data['DESCRICAO']. ' (' . $data['PRODUTO'] . ') se repete na página '.$data['PAGINA_ITEM'],
						'avisos'
					);
					$codigosRepetidos[$data['PRODUTO']] = $data['PAGINA_ITEM'];
				}
			}
		}

		// 15/07/2010 - adicionando os codigos regionais
		foreach($this->errorsList as $tipo => &$itens){
			foreach($itens as &$item){
				if( array_key_exists($item['PRODUTO'], $this->codigosRegionaisValidacao) ){
					$item['CODIGO_REGIONAL'] = $this->codigosRegionaisValidacao[ $item['PRODUTO'] ];
				}

				// se encontrou a ficha
				if( !empty($this->cacheFichas[ $item['PRODUTO'] ]) ){
					// fazendo replace de nomes
					$item['MENSAGEM'] = preg_replace('@\{(\w*\d*):(\w+):(.*?)\}@e', 'empty($this->cacheFichas["$1"]["$2"]) ? "$3" : $this->cacheFichas["$1"]["$2"]', $item['MENSAGEM']);
					if(isset($this->cacheFichas[ $item['PRODUTO'] ]['NOME_FICHA'])){
						// trocando o nome
						$item['NOME_FICHA'] = $this->cacheFichas[ $item['PRODUTO'] ]['NOME_FICHA'];
					}

				} else {
					// fazendo replace de nomes
					$item['MENSAGEM'] = preg_replace('@\{(\w*\d*):(\w+):(.*?)\}@e', 'empty($item["$2"]) ? "$3" : $item["$2"]', $item['MENSAGEM']);
				}
			}
		}

	}

	/**
	 * Persiste os dados do excel
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return int codigo do registro gerado
	 */
	private function saveExcel( $idjob = null ){
		
		//Recupera informacoes do usuario logado
		$user = Sessao::get('usuario');
		
		// primeiro, vamos ver se ja existe algum
		// excel com o codigo informado
		//$excel = $this->getByJobCliente($this->_jobID, $this->_formData['ID_CLIENTE']);
		$excel = $this->getByJob($idjob);

		$this->_formData['NUMERO_JOB_EXCEL'] = $this->_jobID;

		// pega o processo do tipo de peca
		$proc = $this->processo->getByTipoPeca( $this->_formData['ID_TIPO_PECA'] );

		//Recupera o primeiro ID_PROCESSO_ETAPA de 'SEL_MIDIA' (Plano de Marketing) do Processo 
		$processoEtapa = $this->processoEtapa->getByProcessoChaveEtapa($proc['ID_PROCESSO'], 'SEL_MIDIA');
		
		// coloca o codigo do processo nos dados
		$this->_formData['ID_PROCESSO'] = $proc['ID_PROCESSO'];

		// ajusta as datas para o banco
		$this->_formData['DATA_INICIO_PROCESSO'] = format_date($this->_formData['DATA_INICIO_PROCESSO']);
		$this->_formData['DATA_TERMINO'] = format_date_to_db($this->_formData['DATA_TERMINO']);
		$this->_formData['DATA_INICIO'] = format_date_to_db($this->_formData['DATA_INICIO']);
		$this->_formData['DATA_ULTIMA_IMPORTACAO'] = date('Y-m-d H:i:s');
		
		$this->_formData['ID_ETAPA'] = $processoEtapa['ID_ETAPA']; 
		$this->_formData['ID_STATUS'] = $processoEtapa['ID_STATUS'];
		$this->_formData['ID_PROCESSO_ETAPA'] = $processoEtapa['ID_PROCESSO_ETAPA'];
		
		// se nao encontrou o excel
		if(empty($excel)){
			
			// vamos gravar o job
			$this->_formData['NOVO_EXCEL'] = 1;
			$this->_formData['STATUS_JOB'] = 1;
			$idjob = $this->job->save($this->_formData);
			$this->_formData['ID_JOB'] = $idjob;
			
			//Prepara dados do historico para gravar primeira etapa
			$data = array(
				'ID_ETAPA' => $processoEtapa['ID_ETAPA'],
				'ID_JOB' => $idjob,
				'DATA_HISTORICO' => date('Y-m-d H:i:s'),
				'ID_USUARIO' => $user['ID_USUARIO'],
				'ID_PROCESSO_ETAPA' => $processoEtapa['ID_PROCESSO_ETAPA'],
				'ID_STATUS' => $processoEtapa['ID_STATUS']
			);
			
			//Salva historico
			$this->db->insert('TB_JOB_HISTORICO', $data);

			// gravando o excel
			return $this->save($this->_formData);

		// se encontrou excel
		} else {
			$this->_formData['NOVO_EXCEL'] = 0;
			$this->job->save($this->_formData, $excel['ID_JOB']);
			
			//Prepara dados do historico para gravar primeira etapa
			$data = array(
				'ID_ETAPA' => $processoEtapa['ID_ETAPA'],
				'ID_JOB' => $excel['ID_JOB'],
				'DATA_HISTORICO' => date('Y-m-d H:i:s'),
				'ID_USUARIO' => $user['ID_USUARIO'],
				'ID_PROCESSO_ETAPA' => $processoEtapa['ID_PROCESSO_ETAPA'],
				'ID_STATUS' => $processoEtapa['ID_STATUS']
			);
			
			//Salva historico
			$this->db->insert('TB_JOB_HISTORICO', $data);

			return $this->save($this->_formData, $excel['ID_EXCEL']);
		}
	}

	/**
	 * Salva os dados de um excel relacionado a uma praca
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idExcel Codigo do excel
	 * @param int $idPraca Codigo da praca
	 * @param int $idVersao Codigo da versao do excel
	 * @param array $data dados para gravacao
	 * @return void
	 */
	public function saveExcelPraca($idExcel, $idPraca, $idVersao, $data=array()){
			$data['ID_PRACA'] = $idPraca;
			$data['ID_EXCEL'] = $idExcel;
			$data['ID_EXCEL_VERSAO'] = $idVersao;

			$rs = $this->db
				->where('ID_EXCEL', $idExcel)
				->where('ID_PRACA', $idPraca)
				->where('ID_EXCEL_VERSAO', $idVersao)
				->get('TB_EXCEL_PRACA')
				->num_rows();

			if($rs == 0){
				$this->db->insert('TB_EXCEL_PRACA', $data);
			}

	}

	/**
	 * Checa se as dimensoes no arquivo estao de acordo com o indd
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data
	 * @return mixed
	 */
	public function checaDimensaoIndd($data){
		if(empty($data['ID_TEMPLATE'])){
			return 'Template não identificado';
		}

		if(!empty($data['LARGURA_JOB']) && !empty($data['ALTURA_JOB'])){
			$tpl = $this->template->getById($data['ID_TEMPLATE']);

			if($data['LARGURA_JOB'] == $tpl['LARGURA_TEMPLATE'] && $data['ALTURA_JOB'] == $tpl['ALTURA_TEMPLATE']){
				return true;
			}
		}

		return 'As dimensões informadas são diferentes do template';
	}
	
	// formata os valores de elementos personalizados que vem la do indesign
	private function formataElementosPersonalizados($arrElementos){
		$retorno = array();
		
		if($arrElementos != null){		
			if(strpos($arrElementos, '.')){
				$retorno = explode('.', $arrElementos);
			}
			else{
				$retorno[] = $arrElementos;
			}
		}
		
		return $retorno;
	}
	
	// formata os valores de elementos(fichas) selecionadas que vem la do indesign retrona array(pagina( arrya(ordens) ))
	private function formataElementosSelecionados($arrElementos){
		$arrPaginaOrdemSelecionadas = array();
		
		if($arrElementos != null){
			if(strpos($arrElementos, '.')){
				$arrElementosSelecionados = explode('.', $arrElementos);
			}
			else{
				$arrElementosSelecionados[] = $arrElementos;
			}
			
			if($arrElementosSelecionados > 0){
				foreach($arrElementosSelecionados as $aes){
					if(strpos($aes, '-')){
						$aes = explode('-', $aes);
						if((is_numeric($aes[0])) && is_numeric($aes[1])){
							$arrPaginaOrdemSelecionadas[$aes[0]][] = $aes[1];
						}
					}
				}
			}
		}
		
		return $arrPaginaOrdemSelecionadas;
	}

	/**
	 * Gera o XML para gerar o INDD
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idExcel Codigo da subida do excel
	 * @param boolean $retorno Indica se eh a primeira vez ou nao deste arquivo
	 * @param int $idpraca Codigo da para se quiser fazer somente de uma praca
	 * @return void
	 */
	public function gerarXML($tipoAtualizacao, $idExcel, $retorno = false, $idpraca = null, $paginaInicio = null, $paginaFim = null, $argumentos = null){
		$versao = $this->excelVersao->getVersaoAtual($idExcel);
		$idversao = $versao['ID_EXCEL_VERSAO'];
		$excel = $this->getById($idExcel);

		$pracas = $this->getPracas($idExcel, $idversao);
		$arrElementosPersonalizados = array();
		$arrElementosSelecionados = array();
		$arrElementosSelecionados = array();
		$arrPaginaOrdemSelecionadas = array();
		
		$keys1 = &PaginacaoDB::$arrayTraducao;

		// lista de tipos por codigo
		$result = $this->tipo_objeto->getAll();
		$tipos = arraytoselect( $result['data'], 'ID_TIPO_OBJETO', 'DESC_TIPO_OBJETO' );

		foreach($pracas as $praca){
			// se informou uma praca, e a atual na eh igual a informada
			if(!is_null($idpraca) && $praca['ID_PRACA'] != $idpraca){
				// vai para a proxima praca
				continue;
			}

			$xml = new DOMDocument('1.0','UTF-8');
			$root = $xml->createElement('Root');

			// informacoes do job
			$jobinfo = $xml->createElement("jobinfo");
			$jobinfo->setAttribute("id_job",$excel['ID_JOB']);
			$jobinfo->setAttribute("id_template",$excel['ID_TEMPLATE']);
			$jobinfo->setAttribute("id_praca", $praca['ID_PRACA']);
			$jobinfo->setAttribute("id_excel", $idExcel);
			$jobinfo->setAttribute("nome_praca", $praca['DESC_PRACA']);
			$jobinfo->setAttribute("versao",time());
			$jobinfo->setAttribute("servidor", base_url());

			// pegamos o templateitem
			if( !empty($paginaInicio) && !empty($paginaFim) ){
				$tplItem = $this->templateItem->getByTemplateRange($excel['ID_TEMPLATE'],$paginaInicio, $paginaFim);

				// se acharmos, colocamos a data no jobinfo
				if( !empty($tplItem) ){
					$jobinfo->setAttribute('data_cadastro', $tplItem['DATA_CADASTRO']);
				}
			}

			if( !is_null($paginaInicio) && !is_null($paginaFim) ){
				$jobinfo->setAttribute("pagina_inicio", $paginaInicio);
				$jobinfo->setAttribute("pagina_fim", $paginaFim);
			}

			$caracteresEspeciais = $xml->createElement("especiais", "•");
			$textoLegal = $xml->createElement("texto_legal_template", $praca['TEXTO_LEGAL']);

			$jobinfo->appendChild($caracteresEspeciais);
			$jobinfo->appendChild($textoLegal);

			$root->appendChild($jobinfo);
			$xml->appendChild($root);
		
			// se for um UPDATE RETAIL personalizado, montamos a array com o s elementos
			if($tipoAtualizacao == 'completa'){
				// caso haja elementos personalizados formata
				$arrElementosPersonalizados = $this->formataElementosPersonalizados($argumentos);
				
				if( !is_null($paginaInicio) && !is_null($paginaFim) ){
					$itens = $this->excelItem->getByExcelPracaVersaoRange($idExcel,$praca['ID_PRACA'], $idversao, true, $paginaInicio, $paginaFim);
				} else {
					$itens = $this->excelItem->getByExcelPracaVersao($idExcel,$praca['ID_PRACA'], $idversao, true);
				}
			}
			else if($tipoAtualizacao = 'selecionada'){
				// caso haja elementos selecionados formata
				$arrPaginaOrdemSelecionadas = $this->formataElementosSelecionados($argumentos);
				$itens = $this->excelItem->getByExcelPracaVersaoPaginaOrdem($idExcel,$praca['ID_PRACA'], $idversao, true, $arrPaginaOrdemSelecionadas);
			} 
			else if($tipoAtualizacao = 'personalizadoSelecionado')	{
				$arrElementosPersonalizados = $this->formataElementosPersonalizados($argumentos);
				$arrPaginaOrdemSelecionadas = $this->formataElementosSelecionados($argumentos);
				$itens = $this->excelItem->getByExcelPracaVersaoPaginaOrdem($idExcel,$praca['ID_PRACA'], $idversao, true, $arrPaginaOrdemSelecionadas);
			}

			// ADICIONAMOS AS SUBFICHAS AS FICHAS ----------------------------------
			$subfichas = array();
			for ($i = count($itens) - 1; $i >= 0; $i--){
				if(is_numeric($itens[$i]['ID_FICHA_PAI']) && $itens[$i]['ID_FICHA_PAI'] > 0){
					$subfichas[] = $itens[$i];
					unset($itens[$i]);
				}
			}

			$itens = array_values($itens);

			for ($i = count($subfichas) - 1; $i >= 0; $i--){
				for ($y = count($itens) - 1; $y >= 0; $y--){
					if(isset($itens[$y]) && isset($subfichas[$i])){
						if($subfichas[$i]['ID_FICHA_PAI'] == $itens[$y]['ID_FICHA']){
							$itens[$y]['SUBFICHAS'][] = $subfichas[$i];
							unset($subfichas[$i]);
						}
					}
				}
			}
			// ----------------------------------------------------------------------

			// recupera os cenarios deste excel para esta praca
			$cenarios = $this->cenario->getByExcelPracaVersao($idExcel, $praca['ID_PRACA'], $idversao, true);

			// pegamos os tipos dos objetos no xinet
			$hashset = $this->objeto_ficha->getTiposByExcelVersaoPraca($idExcel, $idversao, $praca['ID_PRACA']);

			// datas de alteracao
			$datasAlteracao = $this->objeto_ficha->getDataAlteracaoByExcelVersaoPraca($idExcel, $idversao, $praca['ID_PRACA']);

			//ordem dos itens
			$i = 0;
			
			// linha excel
			$linhaExcelAtual = null;
			$linhaExcelAnterior = null;

			// para cada item
			foreach($itens as $item){
				// variavel para ir armazenado os itens e comparando se sao repetidos
				$cacheItens = array();
				
				// se o item esta desativado e nao e um retorno
				if($item['STATUS_ITEM'] == 0 && !$retorno){
					// passa para o proximo item
					continue;
				}

				$fichaEl = $xml->createElement("ficha");

				// se tiver codigo na ficha e tiver id de ficha
				if(!empty($item['CODIGO_ITEM']) && !empty($item['ID_FICHA'])){
					$ficha = $this->ficha->getbyId($item['ID_FICHA']);
					$pricing = $this->excelItem->getPricing($item['ID_EXCEL_ITEM']);

					$fichaEl->setAttribute("id",$ficha["ID_FICHA"]);
					$fichaEl->setAttribute("id_cenario",$item["ID_CENARIO"]);
					$fichaEl->setAttribute("pagina",$item["PAGINA_ITEM"]);
					$fichaEl->setAttribute("data_alteracao",$ficha["DATA_ALTERACAO"]);
					$fichaEl->setAttribute('acao', $item['STATUS_ITEM'] == 1 ? '' : 'delete');
				// se nao tem, colocamos uma ficha em branco
				} else {
					$fichaEl->setAttribute('acao', $item['STATUS_ITEM'] == 1 ? 'vazia' : 'delete');
					$fichaEl->setAttribute("pagina",$item["PAGINA_ITEM"]);
				}

				//cria um atributo extra com uma informacao necessaria ao plugin do indesign
				$fichaEl->setAttribute('extra', "{$item['PAGINA_ITEM']}-{$item['ORDEM_ITEM']}");

				if($item['STATUS_ITEM'] == 1 && !empty($item['ID_FICHA'])){
					$linhaExcelAtual = $item['LINHA_EXCEL'];
					
					$textos = $this->campo_ficha->getByFicha($item['ID_FICHA'], 0);

					$objetos = $this->objeto_ficha->getByFicha($item['ID_FICHA']);

					foreach($textos as $txt){
						$tagname = strtolower(removeAcentos($txt['LABEL_CAMPO_FICHA']));

						// bug encontrado em producao dia 02/09
						// por algum motivo, algum label ficou vazio
						// vamos tratar
						if( empty($tagname) ){
							$tagname = 'tagbranco';
						}

						// esses IF's sao utilizados para vericar as tags do UPDATE PERSONALIZADO ou UPDATE TOTAL
						if( count($arrElementosPersonalizados) > 0 ){
							if( in_array($txt['LABEL_CAMPO_FICHA'], $arrElementosPersonalizados) ){
								$tag = $xml->createElement($tagname, escapaStringIndesign(str_replace(PHP_EOL,"\r\n",$txt['VALOR_CAMPO_FICHA'])));
								$fichaEl->appendChild($tag);
								
								$cacheItens['0'][$tagname] = escapaStringIndesign(str_replace(PHP_EOL,"\r\n",$txt['VALOR_CAMPO_FICHA']));
							}
						}
						else{
							$tag = $xml->createElement($tagname, escapaStringIndesign(str_replace(PHP_EOL,"\r\n",$txt['VALOR_CAMPO_FICHA'])));
							$fichaEl->appendChild($tag);
							
							$cacheItens['0'][$tagname] = escapaStringIndesign(str_replace(PHP_EOL,"\r\n",$txt['VALOR_CAMPO_FICHA']));
						}						
					}

					// adiciona os objetos (imagens) ao xml da ficha
					$this->adicionaObjetosNoXml($objetos, $xml, $fichaEl, $arrElementosPersonalizados, null, $hashset, $tipos, null);

					// aplica a mascara de codigo de barras da compra facil
					if(strtolower($excel['DESC_CLIENTE']) == 'comprafacil'){
						$ficha['COD_BARRAS_FICHA'] = mascaraReferenciaCompraFacil($ficha['COD_BARRAS_FICHA']);
					}
					
					// adiciona digito verificador e mascara pra cliente hermes
					if ( (strtolower($excel['DESC_CLIENTE']) == 'hermes') ) {
						$numero = $ficha['COD_BARRAS_FICHA'].$excel['COD_CATALOGO'];
						$ficha['COD_BARRAS_FICHA'] = digitoVerificadorHermes($numero);			
					}
					
					// mais uma gambi no sistema, sim pode acreditar que ainda muuuuuitas outras virao
					// aki se o cliente for condor ou agencia ccz e isso for uma subficha "solitaria", sem ficha principal, iremos 
					// buscar o nome da ficha em alguma de suas fichas principais, sim, em qualquer uma
					if ((strtolower($excel['DESC_AGENCIA']) == 'ccz comunicacao') || (strtolower($excel['DESC_CLIENTE']) == 'condor') ) {
						if($ficha['TIPO_FICHA'] == "SUBFICHA"){
							$nomeFichaCCZ = $this->subficha->getNomeFichaPrincipal($ficha['ID_FICHA']);
							if(isset($nomeFichaCCZ['NOME_FICHA'])){
								$nomeFichaCCZ = $nomeFichaCCZ['NOME_FICHA'];
							}
							else{
								$nomeFichaCCZ = "";
							}
						}
					}
					
					/*-------------------- CAMPOS DA FICHA ------------------------------*/
					if( count($arrElementosPersonalizados) > 0 ){
						if( in_array('nome_ficha', $arrElementosPersonalizados) ){
							// mais uma gambi no sistema, sim pode acreditar que ainda muuuuuitas outras virao
							// aki se o cliente for condor ou agencia ccz e isso for uma subficha "solitaria", sem ficha principal, iremos 
							// buscar o nome da ficha em alguma de suas fichas principais, sim, em qualquer uma
							if ((strtolower($excel['DESC_AGENCIA']) == 'ccz comunicacao') || (strtolower($excel['DESC_CLIENTE']) == 'condor') ) {
								if($ficha['TIPO_FICHA'] == "SUBFICHA"){
									$nomeFichaCCZ = $this->subficha->getNomeFichaPrincipal($ficha['ID_FICHA']);
									if(isset($nomeFichaCCZ['NOME_FICHA'])){
										$fichaEl->appendChild($xml->createElement('nome_ficha', escapaStringIndesign($nomeFichaCCZ['NOME_FICHA'])) );
									}
								}
								else{
									$fichaEl->appendChild($xml->createElement('nome_ficha', escapaStringIndesign($ficha['NOME_FICHA'])) );
								}
							}
							else{
								$fichaEl->appendChild($xml->createElement('nome_ficha', escapaStringIndesign($ficha['NOME_FICHA'])) );
							}
						}
						if( in_array('codigo_ficha', $arrElementosPersonalizados) )
							$fichaEl->appendChild($xml->createElement('codigo_ficha', $ficha['COD_BARRAS_FICHA']));
						if( in_array('cliente', $arrElementosPersonalizados) )
							$fichaEl->appendChild($xml->createElement('cliente', escapaStringIndesign($ficha['DESC_CLIENTE'])) );
						if( in_array('categoria', $arrElementosPersonalizados) )
							$fichaEl->appendChild($xml->createElement('categoria', escapaStringIndesign($ficha['DESC_CATEGORIA']))) ;
						if( in_array('subcategoria', $arrElementosPersonalizados) )
							$fichaEl->appendChild($xml->createElement('subcategoria', escapaStringIndesign($ficha['DESC_SUBCATEGORIA'])) );
						if( in_array('fabricante', $arrElementosPersonalizados) )
							$fichaEl->appendChild($xml->createElement('fabricante', escapaStringIndesign($ficha['DESC_FABRICANTE'])) );
						if( in_array('marca', $arrElementosPersonalizados) )
							$fichaEl->appendChild($xml->createElement('marca', escapaStringIndesign($ficha['DESC_MARCA'])) );
						if( in_array('modelo', $arrElementosPersonalizados) )
							$fichaEl->appendChild($xml->createElement('modelo', escapaStringIndesign($ficha['MODELO_FICHA'])) );
						if( in_array('ficha_temporaria', $arrElementosPersonalizados) )
							$fichaEl->appendChild($xml->createElement('ficha_temporaria', $ficha['FLAG_TEMPORARIO']));
					}
					else{
						// mais uma gambi no sistema, sim pode acreditar que ainda muuuuuitas outras virao
						// aki se o cliente for condor ou agencia ccz e isso for uma subficha "solitaria", sem ficha principal, iremos 
						// buscar o nome da ficha em alguma de suas fichas principais, sim, em qualquer uma
						if ((strtolower($excel['DESC_AGENCIA']) == 'ccz comunicacao') || (strtolower($excel['DESC_CLIENTE']) == 'condor') ) {
							if($ficha['TIPO_FICHA'] == "SUBFICHA"){
								$nomeFichaCCZ = $this->subficha->getNomeFichaPrincipal($ficha['ID_FICHA']);
								if(isset($nomeFichaCCZ['NOME_FICHA'])){
									$fichaEl->appendChild($xml->createElement('nome_ficha', escapaStringIndesign($nomeFichaCCZ['NOME_FICHA'])) );
								}
							}
							else{
								$fichaEl->appendChild($xml->createElement('nome_ficha', escapaStringIndesign($ficha['NOME_FICHA'])) );
							}
						}
						else{
							$fichaEl->appendChild($xml->createElement('nome_ficha', escapaStringIndesign($ficha['NOME_FICHA'])) );
						}
						$fichaEl->appendChild($xml->createElement('codigo_ficha', $ficha['COD_BARRAS_FICHA']));
						$fichaEl->appendChild($xml->createElement('cliente', escapaStringIndesign($ficha['DESC_CLIENTE'])) );
						$fichaEl->appendChild($xml->createElement('categoria', escapaStringIndesign($ficha['DESC_CATEGORIA']))) ;
						$fichaEl->appendChild($xml->createElement('subcategoria', escapaStringIndesign($ficha['DESC_SUBCATEGORIA'])) );
						$fichaEl->appendChild($xml->createElement('fabricante', escapaStringIndesign($ficha['DESC_FABRICANTE'])) );
						$fichaEl->appendChild($xml->createElement('marca', escapaStringIndesign($ficha['DESC_MARCA'])) );
						$fichaEl->appendChild($xml->createElement('modelo', escapaStringIndesign($ficha['MODELO_FICHA'])) );
						$fichaEl->appendChild($xml->createElement('ficha_temporaria', $ficha['FLAG_TEMPORARIO']));
					}

					/*-------------------- SETA O ATRIBUTO TIPO  ------------------------------*/
					foreach($fichaEl->childNodes as $node){
						$node->setAttribute('tipo', 'ficha');
					}

					// texto legal
					if( count($arrElementosPersonalizados) > 0 ){
						if( in_array('texto_legal', $arrElementosPersonalizados) ){
							$txtLegal = $xml->createElement('texto_legal', $item['TEXTO_LEGAL']);
							$txtLegal->setAttribute('tipo','excel');
							$fichaEl->appendChild($txtLegal);
						}
					} 
					else{
						$txtLegal = $xml->createElement('texto_legal', $item['TEXTO_LEGAL']);
						$txtLegal->setAttribute('tipo','excel');
						$fichaEl->appendChild($txtLegal);
					} 

					$idx = 0;
					$chaves = array(
						'PRECO_UNITARIO','PRECO_CAIXA','PRECO_VISTA','PRECO_DE','PRECO_POR',
						'ECONOMIZE','ENTRADA','PRESTACAO','JUROS_AM','JUROS_AA','TOTAL_PRAZO','PARCELAS'
					);

					foreach($pricing as $key => $val){
						if(++$idx > 2){

							$val = trim($val);

							if(in_array($key, $chaves)){
								if(!empty($val)){
									if($key == 'PARCELAS'){
										$val = $val . 'x';
									}
									else{
										$val = number_format($val, 2, ',','.');
									}
								}
							}
							
							$tag = $xml->createElement(strtolower($key), $val);
							
							$tag->setAttribute('tipo','excel');
							
							if( count($arrElementosPersonalizados) > 0 ){
								if( in_array(strtolower($key), $arrElementosPersonalizados) ){
									$fichaEl->appendChild($tag);
									$cacheItens['0'][strtolower($key)] = $val;
								}
							}
							else{
								$fichaEl->appendChild($tag);
								$cacheItens['0'][strtolower($key)] = $val;
							}
							
						}
					}
					
					$linhaExcelAnterior = $linhaExcelAtual;

					if(isset($item['SUBFICHAS']) && count($item['SUBFICHAS']) > 0){
						$y = 1;
						$arrCountCampo = array();
						$arrCountPricing = array();
						
						foreach ($item['SUBFICHAS'] as $keySubficha=>$subficha){
							$objetos = $this->objeto_ficha->getByFicha($subficha['ID_FICHA']);

							// adiciona os objetos (imagens) ao xml da subficha
							$this->adicionaObjetosNoXml($objetos, $xml, $fichaEl, $arrElementosPersonalizados, $keySubficha, $hashset, $tipos);

							$linhaExcelAtual = $subficha['LINHA_EXCEL'];
							
							

							$ficha = $this->ficha->getbyId($subficha['ID_FICHA']);
							
							// aplica a mascara de codigo de barras da compra facil
							if(strtolower($excel['DESC_CLIENTE']) == 'comprafacil'){
								$ficha['COD_BARRAS_FICHA'] = mascaraReferenciaCompraFacil($ficha['COD_BARRAS_FICHA']);
							}
							
							// adiciona digito verificador e mascara pra cliente hermes
							if ( (strtolower($excel['DESC_CLIENTE']) == 'hermes') ) {
								$numero = $ficha['COD_BARRAS_FICHA'].$excel['COD_CATALOGO'];
								$ficha['COD_BARRAS_FICHA'] = digitoVerificadorHermes($numero);			
							}
							
							if( count($arrElementosPersonalizados) > 0 ){
								if( in_array('codigo_ficha'.$y, $arrElementosPersonalizados) ){
									$tag = $xml->createElement('codigo_ficha'.$y, $ficha['COD_BARRAS_FICHA']);
									$tag->setAttribute('tipo','ficha');
									$fichaEl->appendChild($tag);	
								}
							}
							else{
								$tag = $xml->createElement('codigo_ficha'.$y, $ficha['COD_BARRAS_FICHA']);
								$tag->setAttribute('tipo','ficha');
								$fichaEl->appendChild($tag);	
							}

							// CAMPOS DA SUBFICHA
							$textos = $this->campo_ficha->getByFicha($subficha['ID_FICHA']);
						
							foreach($textos as $txt){
								$tagname = strtolower(removeAcentos($txt['LABEL_CAMPO_FICHA']));
		
								if( empty($tagname) ){
									$tagname = 'tagbranco';
								}
								else{
									$tagname = $tagname . $y;
								}
		
								if( count($arrElementosPersonalizados) > 0 ){
									if( in_array($txt['LABEL_CAMPO_FICHA'], $arrElementosPersonalizados) ){
										if( $txt['PRINCIPAL'] == '1' || !verificaElementosDuplicados($cacheItens, strtolower(removeAcentos($txt['LABEL_CAMPO_FICHA'])), escapaStringIndesign(str_replace(PHP_EOL,"\r\n",$txt['VALOR_CAMPO_FICHA']))) ){
											$tag = $xml->createElement($tagname, escapaStringIndesign(str_replace(PHP_EOL,"\r\n",$txt['VALOR_CAMPO_FICHA'])));
											$tag->setAttribute('tipo','ficha');
											$fichaEl->appendChild($tag);
										}
									}
								}
								else{
									if( $txt['PRINCIPAL'] == '1' || !verificaElementosDuplicados($cacheItens, strtolower(removeAcentos($txt['LABEL_CAMPO_FICHA'])), escapaStringIndesign(str_replace(PHP_EOL,"\r\n",$txt['VALOR_CAMPO_FICHA']))) ){
										$tag = $xml->createElement($tagname, escapaStringIndesign(str_replace(PHP_EOL,"\r\n",$txt['VALOR_CAMPO_FICHA'])));
										$tag->setAttribute('tipo','ficha');
										$fichaEl->appendChild($tag);
									}							
								}	
								
								$cacheItens[$y][strtolower(removeAcentos($txt['LABEL_CAMPO_FICHA']))] = escapaStringIndesign(str_replace(PHP_EOL,"\r\n",$txt['VALOR_CAMPO_FICHA']));
							}

							// PRICING DA SUBFICHA
							$pricing = $this->excelItem->getPricing($subficha['ID_EXCEL_ITEM']);
							$fields = explode(',','PRECO_UNITARIO,PRECO_CAIXA,PRECO_VISTA,PRECO_DE,PRECO_POR,ECONOMIZE,ENTRADA,PRESTACAO,TOTAL_PRAZO,JUROS_AM,JUROS_AA,PARCELAS');
							
							if(($linhaExcelAtual != $linhaExcelAnterior) || ($linhaExcelAtual == 0)){
								foreach($pricing as $key => $val){
									if(in_array($key, $fields)){
										$val = trim($val);
										if(in_array($key, $fields)){
											if(!empty($val)){
												if($key == 'PARCELAS'){
													$val = $val . 'x';
												}
												else{
													$val = number_format($val, 2, ',','.');
												}
											}
										}
	
										if(!isset($arrCountPricing[strtolower($key)])){
											$arrCountPricing[strtolower($key)] = 1;
										}
										
										$tag = $xml->createElement(strtolower($key) . $arrCountPricing[strtolower($key)], $val);
	
										$tag->setAttribute('tipo','excel');
										
										if( count($arrElementosPersonalizados) > 0 ){
											if( in_array(strtolower($key), $arrElementosPersonalizados) ){
												$fichaEl->appendChild($tag);
												$arrCountPricing[strtolower($key)]++;
											}
										}
										else{										
											$fichaEl->appendChild($tag);
											$arrCountPricing[strtolower($key)]++;
										}	
										
										$cacheItens[$y][strtolower($key)] = $val;
									}
								}
							}
							
							$linhaExcelAnterior = $linhaExcelAtual;
							$y++;
						}
					}
				}
 
				$root->appendChild($fichaEl);

				$i++;
			}

			// para cada cenario encontrado
			if( count($arrElementosPersonalizados) <= 0 ){
				foreach ( $cenarios as $cenario ){
					// cria a tag cenario
					$cenarioEl = $xml->createElement( "cenario" );
					
					// atributos da tag cenario
					$cenarioEl->setAttribute( "id", $cenario["ID_CENARIO"] );
					$cenarioEl->setAttribute( "codigo", $cenario["COD_CENARIO"] );
					$cenarioEl->setAttribute( "nome", $cenario["NOME_CENARIO"] );
					$cenarioEl->setAttribute( "pagina", $cenario["PAGINA_ITEM"] );
					
					// cria a tag IAMGEM_P, que ficará dentro do cenário
					$imagemP = $xml->createElement('imagem_p');
					
					// trata o link da imagem do cenario
					$link = str_replace(
								$this->config->item('caminho_storage'),
								$this->config->item('caminho_storage_fpo'),
								$cenario["PATH"]
							);
					$link = fromUtf8MAC( $link . $cenario['FILENAME'] );
	
					// adciona o atribulo link na IMAGEM_P, que contem o caminha d aimagem
					$imagemP->setAttribute( 'link', $link );
					
					// adcioana a tag IMAGEM_P dentro da tag de cenario
					$cenarioEl->appendChild( $imagemP );			
					
					$root->appendChild( $cenarioEl );
				}
			}

			$filename  = sprintf('%d-%s-%d-%d-%d',
				$excel['ID_EXCEL'],
				$excel['ID_JOB'],
				$praca['ID_PRACA'],
				$paginaInicio,
				$paginaFim
			);

			$filename = 'files/xml/'.$filename.'.xml';
			$xml->save($filename);
		}
	}
	
	/**
	 * Gera o XML para gerar o INDD (HERMES E COMPRAFACIL)
	 *
	 * @link http://www.247id.com.br
	 * @param int $idExcel Codigo da subida do excel
	 * @param boolean $retorno Indica se eh a primeira vez ou nao deste arquivo
	 * @param int $idpraca Codigo da para se quiser fazer somente de uma praca
	 * @return void
	 */
	public function gerarXMLHermes($tipoAtualizacao, $idExcel, $retorno = false, $idpraca = null, $paginaInicio = null, $paginaFim = null, $argumentos = null){
		$versao = $this->excelVersao->getVersaoAtual($idExcel);
		$idversao = $versao['ID_EXCEL_VERSAO'];
		$excel = $this->getById($idExcel);

		$pracas = $this->getPracas($idExcel, $idversao);
		$arrElementosPersonalizados = array();
		$arrElementosSelecionados = array();
			
		$keys1 = &PaginacaoDB::$arrayTraducao;

		// lista de tipos por codigo
		$result = $this->tipo_objeto->getAll();
		$tipos = arraytoselect( $result['data'], 'ID_TIPO_OBJETO', 'DESC_TIPO_OBJETO' );

		foreach($pracas as $praca){
			// se informou uma praca, e a atual na eh igual a informada
			if(!is_null($idpraca) && $praca['ID_PRACA'] != $idpraca){
				// vai para a proxima praca
				continue;
			}

			$xml = new DOMDocument('1.0','UTF-8');
			$root = $xml->createElement('Root');

			// informacoes do job
			$jobinfo = $xml->createElement("jobinfo");
			$jobinfo->setAttribute("id_job",$excel['ID_JOB']);
			$jobinfo->setAttribute("id_template",$excel['ID_TEMPLATE']);
			$jobinfo->setAttribute("id_praca", $praca['ID_PRACA']);
			$jobinfo->setAttribute("id_excel", $idExcel);
			$jobinfo->setAttribute("nome_praca", $praca['DESC_PRACA']);
			$jobinfo->setAttribute("versao",time());
			$jobinfo->setAttribute("servidor", base_url());

			// pegamos o templateitem
			if( !empty($paginaInicio) && !empty($paginaFim) ){
				$tplItem = $this->templateItem->getByTemplateRange($excel['ID_TEMPLATE'],$paginaInicio, $paginaFim);

				// se acharmos, colocamos a data no jobinfo
				if( !empty($tplItem) ){
					$jobinfo->setAttribute('data_cadastro', $tplItem['DATA_CADASTRO']);
				}
			}

			if( !is_null($paginaInicio) && !is_null($paginaFim) ){
				$jobinfo->setAttribute("pagina_inicio", $paginaInicio);
				$jobinfo->setAttribute("pagina_fim", $paginaFim);
			}

			$caracteresEspeciais = $xml->createElement("especiais", "•");
			$textoLegal = $xml->createElement("texto_legal_template", $praca['TEXTO_LEGAL']);

			$jobinfo->appendChild($caracteresEspeciais);
			$jobinfo->appendChild($textoLegal);

			$root->appendChild($jobinfo);
			$xml->appendChild($root);
			
			// se for um UPDATE RETAIL personalizado, montamos a array com o s elementos
			if($tipoAtualizacao == 'completa'){
				// caso haja elementos personalizados formata
				$arrElementosPersonalizados = $this->formataElementosPersonalizados($argumentos);
				
				if( !is_null($paginaInicio) && !is_null($paginaFim) ){
					$itens = $this->excelItem->getByExcelPracaVersaoRange($idExcel,$praca['ID_PRACA'], $idversao, true, $paginaInicio, $paginaFim);
				} else {
					$itens = $this->excelItem->getByExcelPracaVersao($idExcel,$praca['ID_PRACA'], $idversao, true);
				}
			}
			else if($tipoAtualizacao = 'selecionada'){
				// caso haja elementos selecionados formata
				$arrPaginaOrdemSelecionadas = $this->formataElementosSelecionados($argumentos);
				$itens = $this->excelItem->getByExcelPracaVersaoPaginaOrdem($idExcel,$praca['ID_PRACA'], $idversao, true, $arrPaginaOrdemSelecionadas);
			}

			// ADICIONAMOS AS SUBFICHAS AS FICHAS ----------------------------------
			$subfichas = array();
			for ($i = count($itens) - 1; $i >= 0; $i--){
				if(is_numeric($itens[$i]['ID_FICHA_PAI']) && $itens[$i]['ID_FICHA_PAI'] > 0){
					$subfichas[] = $itens[$i];
					unset($itens[$i]);
				}
			}
			
			$itens = array_values($itens);

			for ($i = count($subfichas) - 1; $i >= 0; $i--){
				for ($y = count($itens) - 1; $y >= 0; $y--){
					if(isset($itens[$y]) && isset($subfichas[$i])){
						if($subfichas[$i]['ID_FICHA_PAI'] == $itens[$y]['ID_FICHA']){
							$itens[$y]['SUBFICHAS'][] = $subfichas[$i];
							unset($subfichas[$i]);
						}
					}
				}
			}
			// ----------------------------------------------------------------------

			// aplica o numero do grupo a cada item do array, fichas e subfichas
			$itens = aplicaGruposHermes($itens);

			// pegamos os tipos dos objetos no xinet
			$hashset = $this->objeto_ficha->getTiposByExcelVersaoPraca($idExcel, $idversao, $praca['ID_PRACA']);
			
			// datas de alteracao
			$datasAlteracao = $this->objeto_ficha->getDataAlteracaoByExcelVersaoPraca($idExcel, $idversao, $praca['ID_PRACA']);
			
			// linha excel
			$linhaExcelAtual = null;
			$linhaExcelAnterior = null;
			
			// grupo anterior
			$grupoAnterior = 1;
			
			// contador de elementos, campos, pricing etc
			$countElementos = 1;
			
			// array para armazenar os grupos com pricing
			$arrGrupoComPricing = array();

			// para cada item
			foreach($itens as $item){
				// numero do grupo de fichas de fichas
				$grupoAtual = $item['GRUPO'];

				// contador 
				$countElementos = 1;

				// nome do grupo atual
				$nomeGrupoAtual = 'preco' . $grupoAtual . '_';
				
				// variavel para ir armazenado os itens e comparando se sao repetidos
				$cacheItens = array();
				
				// se o item esta desativado e nao e um retorno
				if($item['STATUS_ITEM'] == 0 && !$retorno){
					// passa para o proximo item
					continue;
				}

				$fichaEl = $xml->createElement("ficha");

				// se tiver codigo na ficha e tiver id de ficha
				if(!empty($item['CODIGO_ITEM']) && !empty($item['ID_FICHA'])){
					$ficha = $this->ficha->getbyId($item['ID_FICHA']);
					$pricing = $this->excelItem->getPricing($item['ID_EXCEL_ITEM']);

					$fichaEl->setAttribute("id",$ficha["ID_FICHA"]);
					$fichaEl->setAttribute("pagina",$item["PAGINA_ITEM"]);
					$fichaEl->setAttribute("data_alteracao",$ficha["DATA_ALTERACAO"]);
					$fichaEl->setAttribute('acao', $item['STATUS_ITEM'] == 1 ? '' : 'delete');

				// se nao tem, colocamos uma ficha em branco
				} else {
					$fichaEl->setAttribute('acao', $item['STATUS_ITEM'] == 1 ? 'vazia' : 'delete');
					$fichaEl->setAttribute("pagina",$item["PAGINA_ITEM"]);
				}

				//cria um atributo extra com uma informacao necessaria ao plugin do indesign
				$fichaEl->setAttribute('extra', "{$item['PAGINA_ITEM']}-{$item['ORDEM_ITEM']}");

				if($item['STATUS_ITEM'] == 1 && !empty($item['ID_FICHA'])){
					$linhaExcelAtual = $item['LINHA_EXCEL'];

					$textos = $this->campo_ficha->getByFicha($item['ID_FICHA'], 0);

					$objetos = $this->objeto_ficha->getByFicha($item['ID_FICHA']);
					
					// adiciona os objetos ao xml da subficha
					$this->adicionaObjetosNoXml($objetos, $xml, $fichaEl, $arrElementosPersonalizados, null, $hashset, $tipos, $nomeGrupoAtual);

					foreach($textos as $txt){
						$tagname = strtolower(removeAcentos($txt['LABEL_CAMPO_FICHA']));

						// tratar para label nao ficar vazio
						if( empty($tagname) ){
							$tagname = 'tagbranco';
						}
						else{
							$tagname = $tagname;
						}

						// esses IF's sao utilizados para vericar as tags do UPDATE PERSONALIZADO ou UPDATE TOTAL
						if( count($arrElementosPersonalizados) > 0 ){
							if( in_array($nomeGrupoAtual.$tagname.$countElementos, $arrElementosPersonalizados) ){
								$tag = $xml->createElement($nomeGrupoAtual.$tagname.$countElementos, escapaStringIndesign(str_replace(PHP_EOL,"\r\n",$txt['VALOR_CAMPO_FICHA'])));
								$fichaEl->appendChild($tag);
								
								$cacheItens['0'][$tagname] = escapaStringIndesign(str_replace(PHP_EOL,"\r\n",$txt['VALOR_CAMPO_FICHA']));
							}
						}
						else{
							$tag = $xml->createElement($nomeGrupoAtual.$tagname.$countElementos, escapaStringIndesign(str_replace(PHP_EOL,"\r\n",$txt['VALOR_CAMPO_FICHA'])));
							$fichaEl->appendChild($tag);
							
							$cacheItens['0'][$tagname] = escapaStringIndesign(str_replace(PHP_EOL,"\r\n",$txt['VALOR_CAMPO_FICHA']));
						}						
					}

					// aplica a mascara de codigo de barras da compra facil
					if(strtolower($excel['DESC_CLIENTE']) == 'comprafacil'){
						$ficha['COD_BARRAS_FICHA'] = mascaraReferenciaCompraFacil($ficha['COD_BARRAS_FICHA']);
					}
					
					// adiciona digito verificador e mascara pra cliente hermes
					if ( (strtolower($excel['DESC_CLIENTE']) == 'hermes') ) {
						$numero = $ficha['COD_BARRAS_FICHA'].$excel['COD_CATALOGO'];
						$ficha['COD_BARRAS_FICHA'] = digitoVerificadorHermes($numero);			
					}

					/*-------------------- CAMPOS DA FICHA ------------------------------*/
					if( count($arrElementosPersonalizados) > 0 ){
						if( in_array('nome_ficha'.$countElementos, $arrElementosPersonalizados) )
							$fichaEl->appendChild($xml->createElement('nome_ficha'.$countElementos, escapaStringIndesign($ficha['NOME_FICHA'])) );
						if( in_array($nomeGrupoAtual.'codigo_ficha'.$countElementos, $arrElementosPersonalizados) )
							$fichaEl->appendChild($xml->createElement($nomeGrupoAtual.'codigo_ficha'.$countElementos, $ficha['COD_BARRAS_FICHA']));
						if( in_array($nomeGrupoAtual.'cliente'.$countElementos, $arrElementosPersonalizados) )
							$fichaEl->appendChild($xml->createElement($nomeGrupoAtual.'cliente'.$countElementos, escapaStringIndesign($ficha['DESC_CLIENTE'])) );
						if( in_array($nomeGrupoAtual.'categoria'.$countElementos, $arrElementosPersonalizados) )
							$fichaEl->appendChild($xml->createElement($nomeGrupoAtual.'categoria'.$countElementos, escapaStringIndesign($ficha['DESC_CATEGORIA']))) ;
						if( in_array($nomeGrupoAtual.'subcategoria'.$countElementos, $arrElementosPersonalizados) )
							$fichaEl->appendChild($xml->createElement($nomeGrupoAtual.'subcategoria', escapaStringIndesign($ficha['DESC_SUBCATEGORIA'])) );
						if( in_array($nomeGrupoAtual.'fabricante'.$countElementos, $arrElementosPersonalizados) )
							$fichaEl->appendChild($xml->createElement($nomeGrupoAtual.'fabricante'.$countElementos, escapaStringIndesign($ficha['DESC_FABRICANTE'])) );
						if( in_array($nomeGrupoAtual.'marca'.$countElementos, $arrElementosPersonalizados) )
							$fichaEl->appendChild($xml->createElement($nomeGrupoAtual.'marca'.$countElementos, escapaStringIndesign($ficha['DESC_MARCA'])) );
						if( in_array($nomeGrupoAtual.'modelo'.$countElementos, $arrElementosPersonalizados) )
							$fichaEl->appendChild($xml->createElement($nomeGrupoAtual.'modelo'.$countElementos, escapaStringIndesign($ficha['MODELO_FICHA'])) );
						if( in_array($nomeGrupoAtual.'ficha_temporaria'.$countElementos, $arrElementosPersonalizados) )
							$fichaEl->appendChild($xml->createElement('ficha_temporaria'.$countElementos, $ficha['FLAG_TEMPORARIO']));
					}
					else{
						$fichaEl->appendChild($xml->createElement('nome_ficha'.$countElementos, escapaStringIndesign($ficha['NOME_FICHA'])) );
						$fichaEl->appendChild($xml->createElement($nomeGrupoAtual.'codigo_ficha'.$countElementos, $ficha['COD_BARRAS_FICHA']));
						$fichaEl->appendChild($xml->createElement($nomeGrupoAtual.'cliente'.$countElementos, escapaStringIndesign($ficha['DESC_CLIENTE'])) );
						$fichaEl->appendChild($xml->createElement($nomeGrupoAtual.'categoria'.$countElementos, escapaStringIndesign($ficha['DESC_CATEGORIA']))) ;
						$fichaEl->appendChild($xml->createElement($nomeGrupoAtual.'subcategoria'.$countElementos, escapaStringIndesign($ficha['DESC_SUBCATEGORIA'])) );
						$fichaEl->appendChild($xml->createElement($nomeGrupoAtual.'fabricante'.$countElementos, escapaStringIndesign($ficha['DESC_FABRICANTE'])) );
						$fichaEl->appendChild($xml->createElement($nomeGrupoAtual.'marca'.$countElementos, escapaStringIndesign($ficha['DESC_MARCA'])) );
						$fichaEl->appendChild($xml->createElement($nomeGrupoAtual.'modelo'.$countElementos, escapaStringIndesign($ficha['MODELO_FICHA'])) );
						$fichaEl->appendChild($xml->createElement($nomeGrupoAtual.'ficha_temporaria'.$countElementos, $ficha['FLAG_TEMPORARIO']));
					}

					/*-------------------- SETA O ATRIBUTO TIPO  ------------------------------*/
					foreach($fichaEl->childNodes as $node){
						$node->setAttribute('tipo', 'ficha');
					}

					// texto legal
					if( count($arrElementosPersonalizados) > 0 ){
						if( in_array('texto_legal', $arrElementosPersonalizados) ){
							$txtLegal = $xml->createElement($nomeGrupoAtual.'texto_legal', $item['TEXTO_LEGAL']);
							$txtLegal->setAttribute('tipo','excel');
							$fichaEl->appendChild($txtLegal);
						}
					} 
					else{
						$txtLegal = $xml->createElement($nomeGrupoAtual.'texto_legal'.$countElementos, $item['TEXTO_LEGAL']);
						$txtLegal->setAttribute('tipo','excel');
						$fichaEl->appendChild($txtLegal);
					} 

					$idx = 0;
					$chaves = array(
						'PRECO_UNITARIO','PRECO_CAIXA','PRECO_VISTA','PRECO_DE','PRECO_POR',
						'ECONOMIZE','ENTRADA','PRESTACAO','JUROS_AM','JUROS_AA','TOTAL_PRAZO','PARCELAS'
					);

					$chavesExtra = array(
						'EXTRA1', 'EXTRA2', 'EXTRA3', 'EXTRA4', 'EXTRA5', 'OBSERVACOES'
					);
					
					foreach($pricing as $key => $val){
						if(++$idx > 2){

							$val = trim($val);

							if(in_array($key, $chaves)){
								if(!empty($val)){
									if($key == 'PARCELAS'){
										$val = $val . 'x';
									}
									else{
										$val = number_format($val, 2, ',','.');
									}
								}
							}
							
							if(in_array($key, $chavesExtra)){
								$tag = $xml->createElement(strtolower($key), $val);
							}
							else{
								$tag = $xml->createElement(strtolower($key).'1', $val);
							}
							$tag->setAttribute('tipo','excel');
							
							if( count($arrElementosPersonalizados) > 0 ){
								if( in_array(strtolower($key), $arrElementosPersonalizados) ){
									$fichaEl->appendChild($tag);
									$cacheItens['0'][strtolower($key)] = $val;
								}
							}
							else{
								$fichaEl->appendChild($tag);
								$cacheItens['0'][strtolower($key)] = $val;
							}
							
						}
					}
					
					$arrGrupoComPricing = array();
					
					$arrGrupoComPricing[$grupoAtual] = 1;
										
					$linhaExcelAnterior = $linhaExcelAtual;
					$grupoAnterior = $grupoAtual;

					if(isset($item['SUBFICHAS']) && count($item['SUBFICHAS']) > 0){						
						$y++;
						$arrCountCampo = array();
						$arrCountPricing = array();
						$count = 0;
						$countPricing = 2;

						foreach ($item['SUBFICHAS'] as $keySubficha=>$subficha){
							$grupoAtual = $subficha['GRUPO'];
							
							// compara akela porra toda de grupo da hermes
							if($grupoAtual == $grupoAnterior){
								$countElementos++;
							}
							else{
								$countElementos = 1;
							}

							$count++;
						
							// nome do grupo atual
							$nomeGrupoAtual = 'preco' . $grupoAtual . '_';

							$ficha = $this->ficha->getbyId($subficha['ID_FICHA']);
							
							$objetos = $this->objeto_ficha->getByFicha($subficha['ID_FICHA']);
							
							// adiciona os objetos ao xml da subficha
							$this->adicionaObjetosNoXml($objetos, $xml, $fichaEl, $arrElementosPersonalizados, $keySubficha, $hashset, $tipos, $nomeGrupoAtual);
							
							// aplica a mascara de codigo de barras da compra facil
							if(strtolower($excel['DESC_CLIENTE']) == 'comprafacil'){
								$ficha['COD_BARRAS_FICHA'] = mascaraReferenciaCompraFacil($ficha['COD_BARRAS_FICHA']);
							}
							
							// adiciona digito verificador e mascara pra cliente hermes
							if ( (strtolower($excel['DESC_CLIENTE']) == 'hermes') ) {
								$numero = $ficha['COD_BARRAS_FICHA'].$excel['COD_CATALOGO'];
								$ficha['COD_BARRAS_FICHA'] = digitoVerificadorHermes($numero);			
							}
							
							if( count($arrElementosPersonalizados) > 0 ){
								if( in_array($nomeGrupoAtual.'codigo_ficha'.$y, $arrElementosPersonalizados) ){
									$tag = $xml->createElement($nomeGrupoAtual.'codigo_ficha'.$y, $ficha['COD_BARRAS_FICHA']);
									$tag->setAttribute('tipo','ficha');
									$fichaEl->appendChild($tag);	
								}
							}
							else{
								$tag = $xml->createElement($nomeGrupoAtual.'codigo_ficha'.$countElementos, $ficha['COD_BARRAS_FICHA']);
								$tag->setAttribute('tipo','ficha');
								$fichaEl->appendChild($tag);	
							}

							// CAMPOS DA SUBFICHA
							$textos = $this->campo_ficha->getByFicha($subficha['ID_FICHA']);

							foreach($textos as $txt){
								$tagname = strtolower(removeAcentos($txt['LABEL_CAMPO_FICHA']));
		
								if( empty($tagname) ){
									$tagname = 'tagbranco';
								}
								else{
									$tagname = $nomeGrupoAtual . $tagname . $countElementos;
								}

								if( count($arrElementosPersonalizados) > 0 ){
									if( in_array($txt['LABEL_CAMPO_FICHA'], $arrElementosPersonalizados) ){
										//if( $txt['PRINCIPAL'] == '1' || !verificaElementosDuplicados($cacheItens, strtolower(removeAcentos($txt['LABEL_CAMPO_FICHA'])), escapaStringIndesign(str_replace(PHP_EOL,"\r\n",$txt['VALOR_CAMPO_FICHA']))) ){
											$tag = $xml->createElement($tagname, escapaStringIndesign(str_replace(PHP_EOL,"\r\n",$txt['VALOR_CAMPO_FICHA'])));
											$tag->setAttribute('tipo','ficha');
											$fichaEl->appendChild($tag);
										//}
									}
								}
								else{
									//if( $txt['PRINCIPAL'] == '1' || !verificaElementosDuplicados($cacheItens, strtolower(removeAcentos($txt['LABEL_CAMPO_FICHA'])), escapaStringIndesign(str_replace(PHP_EOL,"\r\n",$txt['VALOR_CAMPO_FICHA']))) ){
										$tag = $xml->createElement($tagname, escapaStringIndesign(str_replace(PHP_EOL,"\r\n",$txt['VALOR_CAMPO_FICHA'])));
										$tag->setAttribute('tipo','ficha');
										$fichaEl->appendChild($tag);
									//}				
								}	
								
								$cacheItens[$y][strtolower(removeAcentos($txt['LABEL_CAMPO_FICHA']))] = escapaStringIndesign(str_replace(PHP_EOL,"\r\n",$txt['VALOR_CAMPO_FICHA']));
							}

							// PRICING DA SUBFICHA
							$pricing = $this->excelItem->getPricing($subficha['ID_EXCEL_ITEM']);
							$fields = explode(',','PRECO_UNITARIO,PRECO_CAIXA,PRECO_VISTA,PRECO_DE,PRECO_POR,ECONOMIZE,ENTRADA,PRESTACAO,TOTAL_PRAZO,JUROS_AM,JUROS_AA,PARCELAS');

							if(!isset($arrGrupoComPricing[$grupoAtual])){
								foreach($pricing as $key => $val){
									if(in_array($key, $fields)){
										$val = trim($val);
										if(in_array($key, $fields)){
											if(!empty($val)){
												if($key == 'PARCELAS'){
													$val = $val . 'x';
												}
												else{
													$val = number_format($val, 2, ',','.');
												}
											}
										}
										
										$tag = $xml->createElement(strtolower($key).$countPricing, $val);
										$tag->setAttribute('tipo','excel');
										
										if( count($arrElementosPersonalizados) > 0 ){
											if( in_array(strtolower($key), $arrElementosPersonalizados) ){
												$fichaEl->appendChild($tag);
											}
										}
										else{										
											$fichaEl->appendChild($tag);
										}	
										
										$cacheItens[$y][$nomeGrupoAtual.strtolower($key)] = $val;
									}
								}
								$countPricing++;
								$arrGrupoComPricing[$grupoAtual] = 1;
							}
							
							$linhaExcelAnterior = $linhaExcelAtual;			
							$grupoAnterior = $grupoAtual;
							$y++;
						}
					}
				}

				$root->appendChild($fichaEl);
				
				$i++;
			}

			$filename  = sprintf('%d-%s-%d-%d-%d',
				$excel['ID_EXCEL'],
				$excel['ID_JOB'],
				$praca['ID_PRACA'],
				$paginaInicio,
				$paginaFim
			);
			$filename = 'files/xml/'.$filename.'.xml';
			$xml->save($filename);
		}
	}

	/**
	 * Recupera as pracas vinculadas deste excel
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idExcel
	 * @param int $idVersao
	 * @return array
	 */
	public function getPracas($idExcel, $idVersao, $pracasAtivas = false){
		$this->db->from('TB_PRACA P')
			->join('TB_EXCEL_PRACA EP','EP.ID_PRACA = P.ID_PRACA')
			->where('EP.ID_EXCEL',$idExcel)
			->where('EP.ID_EXCEL_VERSAO',$idVersao);

		if( $pracasAtivas == true ){
			$this->db->where('P.STATUS_PRACA',1);
		}

		$rs = $this->db->get();

		return $rs->result_array();
	}

	/**
	 * Validacao
	 *
	 * <p>Efetuamos a validacao do job e do excel</p>
	 *
	 * @see system/application/libraries/GenericModel#validate()
	 */
	public function validate($data){

		$job = $this->job->validate($data);

		$res = array_merge(parent::validate($data), $job);

		return $res;

	}

	/**
	 * Recupera as pracas e os arquivos de INDD relacionados
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idExcel
	 * @param int $versao
	 * @return array
	 */
	public function getPracasAndFiles($idExcel, $versao){
		// vamos apagar todos os arquivos desta importacao
		// que foram feitos a mais de 24 h
		$excel = $this->getById($idExcel);
		$path = 'files/layout/';
		$pracas = $this->excel->getPracas($excel['ID_EXCEL'], $versao);
		$dia = 3600*24;

		foreach($pracas as $id => $praca){
			$filename = $excel['ID_EXCEL'].'-'.$excel['DESC_EXCEL'] . '_' . $praca['DESC_PRACA'] . '.indd';

			if(file_exists($path.$filename)){
				$diff = time() - filemtime($path.$filename);

				if($diff > $dia && is_writable($path.$filename)){
					unlink($path.$filename);

					// tambem removemos o jpg
					$jpg = $excel['DESC_EXCEL'] . '_' . $praca['DESC_PRACA'] . '.jpg';
					if(file_exists($path.$jpg) && is_writable($path.$jpg)){
						unlink($path.$jpg);
					}

					$pracas[$id]['FILENAME'] = '';

				} else {
					$pracas[$id]['FILENAME'] = $path.$filename;

				}
			} else {
				$pracas[$id]['FILENAME'] = '';
			}
		}

		return $pracas;
	}

	/**
	 * Recupera pracas e arquivos de template por praca
	 * @param $idExcel
	 * @param $versao
	 * @return array
	 */
	function getTemplateFilesAndPracas($idExcel, $versao){
		$pracas = $this->excel->getPracas($idExcel, $versao);
		$excel = $this->excel->getById($idExcel);

		$data = array();

		foreach ($pracas as $praca) {

			$data[$praca['DESC_PRACA']] = array();

			$titems = $this->templateItem->getByTemplate($excel['ID_TEMPLATE']);
			$nomePraca = $praca['DESC_PRACA'];
			$idPraca = $praca['ID_PRACA'];
			foreach ($titems as $titem) {
				$idTitem = $titem['ID_TEMPLATE_ITEM'];
				$inddName  = sprintf('%d-%s-%d-%d',
						$excel['ID_EXCEL'],
						$nomePraca,
						$titem['PAGINA_INICIO_TEMPLATE_ITEM'],
						$titem['PAGINA_FIM_TEMPLATE_ITEM']
					);

				$file = 'files/layout/'.rawurlencode($inddName).'.indd';
				$check_file = 'files/layout/'.($inddName).'.indd';
				$exists = is_file($check_file);

				$data[$nomePraca][$idTitem]['nome'] = $inddName.'.indd';
				$data[$nomePraca][$idTitem]['url'] = base_url().$file;
				$data[$nomePraca][$idTitem]['exists'] = $exists;
				$data[$nomePraca][$idTitem]['inicio'] = $titem['PAGINA_INICIO_TEMPLATE_ITEM'];
				$data[$nomePraca][$idTitem]['fim'] = $titem['PAGINA_FIM_TEMPLATE_ITEM'];
			}
		}

		return $data;
	}


	/**
	 * recupera um excel pelo job
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idjob
	 * @return array
	 */
	public function getByJob($idjob){
		$rs = $this->db->from($this->tableName.' E')
			->join('TB_JOB J','E.ID_JOB = J.ID_JOB')
			->join('TB_CAMPANHA CA', 'CA.ID_CAMPANHA = J.ID_CAMPANHA')
			->join('TB_PRODUTO P', 'P.ID_PRODUTO = CA.ID_PRODUTO')
			->join('TB_CLIENTE C', 'C.ID_CLIENTE = J.ID_CLIENTE')
			->join('TB_AGENCIA AG', 'J.ID_AGENCIA = AG.ID_AGENCIA')
			->where('E.ID_JOB', $idjob)
			->get();

		return $rs->row_array();
	}
	
	/**
	 * Recupera os itens de um job e praca
	 *
	 * <p>sempre recupera os itens relacionados a ultima versao</p>
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idjob
	 * @param int $idpraca
	 * @param int $offset
	 * @param int $limit
	 * @param array $filtros
	 * @return array
	 */
	public function getItensForJobPraca($idjob, $idpraca,$offset=null,$limit=null,array $filtros=array(), $buscaFilhas = false,array $pesquisaAvancada=array()){
		$idpraca = sprintf('%d', $idpraca);
		$idjob = sprintf('%d', $idjob);

		$sub = '(SELECT ID_EXCEL_VERSAO
			FROM TB_EXCEL_VERSAO
			JOIN TB_EXCEL USING(ID_EXCEL)
			WHERE TB_EXCEL.ID_EXCEL = E.ID_EXCEL
			AND TB_EXCEL.ID_JOB = '.$idjob.'
			ORDER BY ID_EXCEL_VERSAO DESC LIMIT 1)';

		$this->db
			->select('*, C.ID_CATEGORIA, SC.ID_SUBCATEGORIA, CAMP.COD_CATALOGO')
			->join('TB_EXCEL E','E.ID_EXCEL = I.ID_EXCEL')
			->join('TB_FICHA F','F.ID_FICHA = I.ID_FICHA')
			->join('TB_CENARIO CE','CE.ID_CENARIO = I.ID_CENARIO', 'LEFT')
			->join('TB_FICHA_CODIGO FC', 'F.ID_FICHA = FC.ID_FICHA','LEFT')
			->join('TB_SUBCATEGORIA SC','SC.ID_SUBCATEGORIA = F.ID_SUBCATEGORIA','LEFT')
			->join('TB_CATEGORIA C','C.ID_CATEGORIA = SC.ID_CATEGORIA','LEFT')
			->join('TB_OBJETO_FICHA OF','OF.ID_FICHA = F.ID_FICHA AND OF.PRINCIPAL = 1','LEFT')
			->join('TB_OBJETO O', 'O.ID_OBJETO = OF.ID_OBJETO','LEFT')
			->join('TB_PRICING P','P.ID_EXCEL_ITEM = I.ID_EXCEL_ITEM')
			->join('TB_APROVACAO_FICHA AP','AP.ID_APROVACAO_FICHA = F.ID_APROVACAO_FICHA')
			->join('TB_JOB JOB','JOB.ID_JOB = E.ID_JOB')
			->join('TB_CAMPANHA CAMP','CAMP.ID_CAMPANHA = JOB.ID_CAMPANHA')

			->where('I.ID_EXCEL_VERSAO = ' . $sub)
			->where('ID_PRACA',$idpraca)
			->where('E.ID_JOB',$idjob)
			->where('STATUS_ITEM', 1)
			->order_by('I.PAGINA_ITEM, I.ORDEM_ITEM, I.ID_CENARIO, F.NOME_FICHA')
			->group_by('I.ID_EXCEL_ITEM')
			->limit($limit, $offset);
			
		if($buscaFilhas == false){
			$this->db->where('(ID_FICHA_PAI = 0 or ID_FICHA_PAI is null)');
		}

		//nova pesquisa avançada da pauta
		if( (isset($pesquisaAvancada['dtInicio']) && !empty($pesquisaAvancada['dtInicio'])) 
			&& (isset($pesquisaAvancada['dtFim']) && !empty($pesquisaAvancada['dtFim'])) ){
			$this->db->where("F.DT_INSERT_FICHA > ",format_date_to_db($pesquisaAvancada['dtInicio'],2));
			$this->db->where("F.DT_INSERT_FICHA < ",format_date_to_db($pesquisaAvancada['dtFim'],3));
		} else if ( (isset($pesquisaAvancada['dtInicio']) && $pesquisaAvancada['dtInicio'] != '') 
				&& (isset($pesquisaAvancada['dtFim']) && $pesquisaAvancada['dtFim'] == '') ) {
					$this->db->where("F.DT_INSERT_FICHA >", format_date_to_db($pesquisaAvancada['dtInicio'],2));
		} else if ( isset($pesquisaAvancada['dtFim']) && $pesquisaAvancada['dtFim'] != '' ) {
					$this->db->like("F.DT_INSERT_FICHA", format_date_to_db($pesquisaAvancada['dtFim'],3));
		}
		if(isset($pesquisaAvancada['PENDENTES']) && $pesquisaAvancada['PENDENTES'] == 1){
			$where = ' ((F.PENDENCIAS_DADOS_BASICOS != "" AND F.PENDENCIAS_DADOS_BASICOS IS NOT NULL) OR';
			$where .= ' (F.PENDENCIAS_CAMPOS != "" AND F.PENDENCIAS_CAMPOS IS NOT NULL))';
			$this->db->where($where);
		}
		foreach($filtros as $key => $val){
			if( is_array($val)){
				$this->db->where_in($key, $val);

			} else {
				if( is_numeric($val)){
					$this->db->where($key, $val);

				} else {
					$this->db->like($key, $val);
				}
			}
		}

		$rs = $this->db
			->get('TB_EXCEL_ITEM I')
			->result_array();

		return $rs;
	}
	
	
	public function getExcelItensForJobPraca($idjob, $idpraca){
		$idpraca = sprintf('%d', $idpraca);
		$idjob = sprintf('%d', $idjob);

		$sub = '(SELECT ID_EXCEL_VERSAO
			FROM TB_EXCEL_VERSAO
			JOIN TB_EXCEL USING(ID_EXCEL)
			WHERE TB_EXCEL.ID_EXCEL = E.ID_EXCEL
			AND TB_EXCEL.ID_JOB = '.$idjob.'
			ORDER BY ID_EXCEL_VERSAO DESC LIMIT 1)';

		$this->db
			->select('*')
			->join('TB_EXCEL E','E.ID_EXCEL = I.ID_EXCEL')
			->where('I.ID_EXCEL_VERSAO = ' . $sub)
			->where('ID_PRACA',$idpraca)
			->where('E.ID_JOB',$idjob)
			->where('STATUS_ITEM', 1)
			->order_by('I.PAGINA_ITEM, I.ORDEM_ITEM, I.ID_CENARIO')
			->group_by('I.ID_EXCEL_ITEM');
			
		$rs = $this->db
			->get('TB_EXCEL_ITEM I')
			->result_array();

		return $rs;
	}
	
	/**
	 * Recupera os itens de um job e praca os itens filhos
	 *
	 * <p>sempre recupera os itens relacionados a ultima versao os filhos</p>
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idjob
	 * @param int $idpraca
	 * @param int $offset
	 * @param int $limit
	 * @param array $filtros
	 * @return array
	 */
	public function getItensFilhosForJobPraca($idjob, $idpraca, $idfichapai,$idfichafilha=null){
		$idpraca = sprintf('%d', $idpraca);
		$idjob = sprintf('%d', $idjob);
		$idfichapai = sprintf('%d', $idfichapai);

		$sub = '(SELECT ID_EXCEL_VERSAO
			FROM TB_EXCEL_VERSAO
			JOIN TB_EXCEL USING(ID_EXCEL)
			WHERE TB_EXCEL.ID_EXCEL = E.ID_EXCEL
			AND TB_EXCEL.ID_JOB = '.$idjob.'
			ORDER BY ID_EXCEL_VERSAO DESC LIMIT 1)';

		$this->db
			->select('*, C.ID_CATEGORIA, SC.ID_SUBCATEGORIA, CAMP.COD_CATALOGO, I.ID_FICHA')
			->join('TB_EXCEL E','E.ID_EXCEL = I.ID_EXCEL')
			->join('TB_FICHA F','F.ID_FICHA = I.ID_FICHA')
			->join('TB_FICHA_CODIGO FC', 'F.ID_FICHA = FC.ID_FICHA','LEFT')
			->join('TB_SUBCATEGORIA SC','SC.ID_SUBCATEGORIA = F.ID_SUBCATEGORIA','LEFT')
			->join('TB_CATEGORIA C','C.ID_CATEGORIA = SC.ID_CATEGORIA','LEFT')
			->join('TB_OBJETO_FICHA OF','OF.ID_FICHA = F.ID_FICHA AND OF.PRINCIPAL = 1','LEFT')
			->join('TB_OBJETO O', 'O.ID_OBJETO = OF.ID_OBJETO','LEFT')
			->join('TB_PRICING P','P.ID_EXCEL_ITEM = I.ID_EXCEL_ITEM')
			->join('TB_JOB JOB','JOB.ID_JOB = E.ID_JOB')
			->join('TB_CAMPANHA CAMP','CAMP.ID_CAMPANHA = JOB.ID_CAMPANHA')
			->join('TB_APROVACAO_FICHA AP','AP.ID_APROVACAO_FICHA = F.ID_APROVACAO_FICHA')

			->where('I.ID_EXCEL_VERSAO = ' . $sub)
			->where('ID_PRACA',$idpraca)
			->where('E.ID_JOB',$idjob)
			->where('STATUS_ITEM', 1);
		
		if(!empty($idfichafilha)){
			$this->db->where('F.ID_FICHA', $idfichafilha);
		} else {
			$this->db->where('ID_FICHA_PAI', $idfichapai);
		}
			
			$this->db->order_by('I.PAGINA_ITEM, I.ORDEM_ITEM, F.NOME_FICHA');
			$this->db->group_by('I.ID_EXCEL_ITEM');

		$rs = $this->db
			->get('TB_EXCEL_ITEM I')
			->result_array();

		return $rs;
	}
	
	/**
	 * Recupera os itens de um job e praca os itens filhos
	 *
	 * <p>sempre recupera os itens relacionados a ultima versao os filhos</p>
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idjob
	 * @param int $idpraca
	 * @param int $offset
	 * @param int $limit
	 * @param array $filtros
	 * @return array
	 */
	public function getItensFilhosForJobPracaPaginaOrdem($idjob, $idpraca, $idfichapai,$idfichafilha=null,$pagina,$ordem){
		$idpraca = sprintf('%d', $idpraca);
		$idjob = sprintf('%d', $idjob);
		$idfichapai = sprintf('%d', $idfichapai);

		$sub = '(SELECT ID_EXCEL_VERSAO
			FROM TB_EXCEL_VERSAO
			JOIN TB_EXCEL USING(ID_EXCEL)
			WHERE TB_EXCEL.ID_EXCEL = E.ID_EXCEL
			AND TB_EXCEL.ID_JOB = '.$idjob.'
			ORDER BY ID_EXCEL_VERSAO DESC LIMIT 1)';

		$this->db
			->select('*, C.ID_CATEGORIA, SC.ID_SUBCATEGORIA, CAMP.COD_CATALOGO, I.ID_FICHA')
			->join('TB_EXCEL E','E.ID_EXCEL = I.ID_EXCEL')
			->join('TB_FICHA F','F.ID_FICHA = I.ID_FICHA')
			->join('TB_FICHA_CODIGO FC', 'F.ID_FICHA = FC.ID_FICHA','LEFT')
			->join('TB_SUBCATEGORIA SC','SC.ID_SUBCATEGORIA = F.ID_SUBCATEGORIA','LEFT')
			->join('TB_CATEGORIA C','C.ID_CATEGORIA = SC.ID_CATEGORIA','LEFT')
			->join('TB_OBJETO_FICHA OF','OF.ID_FICHA = F.ID_FICHA AND OF.PRINCIPAL = 1','LEFT')
			->join('TB_OBJETO O', 'O.ID_OBJETO = OF.ID_OBJETO','LEFT')
			->join('TB_PRICING P','P.ID_EXCEL_ITEM = I.ID_EXCEL_ITEM')
			->join('TB_JOB JOB','JOB.ID_JOB = E.ID_JOB')
			->join('TB_CAMPANHA CAMP','CAMP.ID_CAMPANHA = JOB.ID_CAMPANHA')
			->join('TB_APROVACAO_FICHA AP','AP.ID_APROVACAO_FICHA = F.ID_APROVACAO_FICHA')

			//->where('I.ID_EXCEL_VERSAO = ' . $sub)
			->where('ID_PRACA',$idpraca)
			->where('E.ID_JOB',$idjob)
			->where('STATUS_ITEM', 1)
			->where('PAGINA_ITEM', $pagina)
			->where('ORDEM_ITEM', $ordem);
		
		if(!empty($idfichafilha)){
			$this->db->where('F.ID_FICHA', $idfichafilha);
		} else {
			$this->db->where('ID_FICHA_PAI', $idfichapai);
		}
			
			$this->db->order_by('I.PAGINA_ITEM, I.ORDEM_ITEM, F.NOME_FICHA');
			$this->db->group_by('I.ID_EXCEL_ITEM');

		$rs = $this->db
			->get('TB_EXCEL_ITEM I')
			->result_array();

		return $rs;
	}
	
	
	
	/**
	 * Retorna o numero de elementos para um job e praca
	 *
	 * Sempre leva em consideracao a ultima versao de excel cadastrada
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idjob
	 * @param int $idpraca
	 * @param array $filtros
	 * @return int
	 */
	public function countItensForJobPraca($idjob, $idpraca, array $filtros = array()){
		$idpraca = sprintf('%d', $idpraca);
		$idjob = sprintf('%d', $idjob);

		$sub = '(SELECT ID_EXCEL_VERSAO
			FROM TB_EXCEL_VERSAO
			JOIN TB_EXCEL USING(ID_EXCEL)
			WHERE TB_EXCEL.ID_EXCEL = E.ID_EXCEL
			AND TB_EXCEL.ID_JOB = '.$idjob.'
			ORDER BY ID_EXCEL_VERSAO DESC LIMIT 1)';

		foreach($filtros as $key => $val){
			if( is_array($val) ){
				$this->db->where_in($key, $val);

			} else {
				if( is_numeric($val) ){
					$this->db->where($key, $val);

				} else {
					$this->db->like($key, $val);
				}
			}
		}

		// armazenamos a string de contagem atual
		$old = $this->db->_count_string;

		// alteramos a string de contagem
		$this->db->_count_string = 'SELECT COUNT(DISTINCT I.ID_EXCEL_ITEM) AS';

		$rs = $this->db
			->join('TB_SUBCATEGORIA SC','SC.ID_SUBCATEGORIA = I.SUBCATEGORIA','LEFT')
			->join('TB_CATEGORIA C','C.ID_CATEGORIA = SC.ID_CATEGORIA','LEFT')
			->join('TB_EXCEL E','E.ID_EXCEL = I.ID_EXCEL')
			->join('TB_FICHA F','F.ID_FICHA = I.ID_FICHA')
			->join('TB_FICHA_CODIGO FC', 'F.ID_FICHA = FC.ID_FICHA','LEFT')
			->join('TB_OBJETO_FICHA OF','OF.ID_FICHA = F.ID_FICHA AND OF.PRINCIPAL = 1','LEFT')
			->join('TB_OBJETO O', 'O.ID_OBJETO = OF.ID_OBJETO','LEFT')
			->join('TB_PRICING P','P.ID_EXCEL_ITEM = I.ID_EXCEL_ITEM')
			->join('TB_APROVACAO_FICHA AP','AP.ID_APROVACAO_FICHA = F.ID_APROVACAO_FICHA')

			->where('I.ID_EXCEL_VERSAO = ' . $sub)
			->where('ID_PRACA',$idpraca)
			->where('ID_JOB',$idjob)
			->where('STATUS_ITEM', 1)
			->order_by('I.PAGINA_ITEM, I.ORDEM_ITEM, F.NOME_FICHA')
			->count_all_results('TB_EXCEL_ITEM I');

		// voltamos a string de contagem original
		$this->db->_count_string = $old;

		return $rs;
	}
	
	/**
	 * Pega a praça base caso existir, para fazer as verificaçoes
	 *
	 * @param int $idjob Numero do job
	 * @param string $filename nome do arquivo de excel
	 * @return integer Retorna a Id da praça base
	 */
	public function getPracaBase($idjob, $filename){
		// Dados do Cliente
		$cliente = $this->job->getCliente($idjob);

		// Importa o excel
		$importer = ExcelImporterFactory::getImporter( $cliente['CLASSE_IMPORTADOR'] );
		$wb = $importer->import($filename, ExcelTransform::loadForJob($idjob));
		$sheets = $wb->getSheets();
		
		// pega a primeira planilha
		$sheet = $wb->getSheetByIndex(0);
		
		// pega a pasta base
		$praca = $sheet->getCellDataByColumnRow(1, 1);

		// Array de informacoes sobre a pasta base
		$pracaBase = $this->praca->getByDescricaoCliente($praca, $cliente['ID_CLIENTE']);
		
		if(count($pracaBase) > 0){
			return $pracaBase;
		}
		else{
			return null;
		}
	}

	/**
	 * Faz o parse do Excel que sera usado no carrinho
	 *
	 * <p>Este metodo retorna um array associativo com duas chaves: erros e itens.
	 * Isto porque podem haver problemas na importacao que o usuario deve saber,
	 * mas eles nao impedem o prosseguimento do processo.</p>
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idjob Numero do job
	 * @param string $filename nome do arquivo de excel
	 * * @param string $tipoImportacao tipo da importacao, se é pra adicionar ou substituir
	 * @return array Dados encontrados no Excel e os erros encontrados
	 */
	public function parseExcelCarrinho($idjob, $filename, $tipoImportacao=null){
		try {
			$cliente = $this->job->getCliente($idjob);

			$importer = ExcelImporterFactory::getImporter( $cliente['CLASSE_IMPORTADOR'] );
			$wb = $importer->import($filename, ExcelTransform::loadForJob($idjob));

			// planilhas
			$sheets = $wb->getSheets();
			// pega o numero total de planilhas
			$totalSheets = count($sheets);

			// pega os dados basicos de importacao
			$excel = $this->getByJob($idjob);
			$versao = $this->excelVersao->getVersaoAtual($excel['ID_EXCEL']);
			$pracas = $this->getPracas($excel['ID_EXCEL'], $versao['ID_EXCEL_VERSAO']);
			$idcliente = $excel['ID_CLIENTE'];

			// dados do job
			$this->_formData = $this->job->getById($idjob);

			// codigos das pracas
			$codigos_pracas = getValoresChave($pracas,'ID_PRACA');

			// erros encontrados
			$erros = array();

			// itens encontrados
			$itens = array();

			// para cada planilha encontrada
			for($j=0; $j<$totalSheets; $j++){
				$sheet = $wb->getSheetByIndex($j);
				$sheetName = $sheet->getSheetName();

				$this->errorsList = array();

				// indica que a praca existe no excel
				$pracaExiste = false;
				$codigoPraca = 0;

				// checa se o nome da praca existe
				// pega somente se a praca estiver ativa
				$praca = $this->praca->getByDescricaoCliente($sheetName, $idcliente, $this->_formData['ID_REGIAO']);

				// se encontrou a praca mas esta inativa
				if( !empty($praca) && $praca['STATUS_PRACA'] == 0 ){
					$this->addMsgErro('','JOB','','','','A praça '.$praca['DESC_PRACA'].' está inativa');
					$erros[$sheetName] = $this->errorsList;
					continue;
				}

				// se a praca existe mas nao eh da mesma regiao do JOB
				if( !empty($praca) && $praca['ID_REGIAO'] != $this->_formData['ID_REGIAO']){
					$this->addMsgErro('','JOB','','','','A praça '.$praca['DESC_PRACA'].' não é da mesma região do job');
					$erros[$sheetName] = $this->errorsList;
					continue;
				}

				// se encontrou a praca e esta na lista de pracas do job
				if( !empty($praca) && in_array($praca['ID_PRACA'], $codigos_pracas) ){
					$pracaExiste = true;
					$codigoPraca = $praca['ID_PRACA'];
				}

				// se a praca nao existe
				if(!$pracaExiste){
					$this->addMsgErro('','JOB','','','','A praça '.$sheetName.' informada no Excel não foi escolhida para o Job');
					$erros[$sheetName] = $this->errorsList;
					continue;
				}

				// resgata o valor da praca base na planilha
				$pracaBase = $sheet->getCellDataByColumnRow(1, 1);
				
				if(!empty($pracaBase)){
					$pracaBaseExiste = false;
					
					// caso exista uma praca base na planilha
					$pracaBase = $this->excel->getPracaBase($idjob, $filename);		

					// se encontrou a praca base mas esta inativa
					if( !empty($pracaBase) && $pracaBase['STATUS_PRACA'] == 0 ){
						$this->addMsgErro('','JOB','','','','A praça base '.$pracaBase['DESC_PRACA'].' está inativa');
						$erros[$sheetName] = $this->errorsList;
						continue;
					}
					
					// se a praca base existe mas nao eh da mesma regiao do JOB
					if( !empty($pracaBase) && $pracaBase['ID_REGIAO'] != $this->_formData['ID_REGIAO']){
						$this->addMsgErro('','JOB','','','','A praça base '.$pracaBase['DESC_PRACA'].' não é da mesma região do job');
						$erros[$sheetName] = $this->errorsList;
						continue;
					}
					
					// se encontrou a praca base e esta na lista de pracas do job
					if( !empty($pracaBase) && in_array($pracaBase['ID_PRACA'], $codigos_pracas) ){
						$pracaBaseExiste = true;
					}
					
								// se a praca nao existe
					if(!$pracaBaseExiste){
						$this->addMsgErro('','JOB','','','','A praça base '.$pracaBase['DESC_PRACA'].' informada no Excel não foi escolhida para o Job');
						$erros[$sheetName] = $this->errorsList;
						continue;
					}
				}

				// criamos o array de itens para a praca
				$itens[ $codigoPraca ] = array();

				// pegamos os dados dos produtos
				for($i=self::ROW_INIT_ITEMS, $max = $sheet->getMaxRow(); $i<=$max; $i++){
					// codigo do produto
					$codigo = $sheet->getCellDataByColumnRow($this->map['PRODUTO']-1, $i);

					// se nao informou o codigo
					if(empty($codigo)){
						// passa para a proxima linha
						continue;
					}
					
					// linha
					$row = array();
					
					// reiniciamos o array de mapeamento
					reset($this->map);
					
					// pegamos todos os dados da linha
					foreach($this->map as $key => $val){					
						$row[ $key ] = $sheet->getCellDataByColumnRow($val-1, $i);
					}

					//ADCIONA A ID DA PRA CORRENTE NA FICHA
					$row['ID_PRACA'] = $codigoPraca;

					$itens[ $codigoPraca ][] = $row;
				}

				foreach ($itens as $key => $val){
					foreach ($val as $v){
						if( empty($v['PAGINA_ITEM']) || empty($v['ORDEM_ITEM']) ){
							$this->addMsgErro('','','','',null,'Página ou Ordem não informado para a praça '.$sheetName.'!');
							continue;
						}
						else{
							if( !is_numeric($v['PAGINA_ITEM']) || !is_numeric($v['ORDEM_ITEM']) ){
								$this->addMsgErro('','','','',null,'Página e Ordem da praça '.$sheetName.' devem conter apenas valores numéricos!');
							}
						}
					}
					continue;
				}

				// usando a mesma validacao dos itens
				$this->validateItems($itens[ $codigoPraca ], $this->_formData['ID_REGIAO'], $tipoImportacao);

				// se houve erros
				if( !empty($this->errorsList) ){
					// indicamos que o resultado e para a praca
					$result = array();
					$result[ $sheetName ] = $this->errorsList;
					// adiciona as mensagens de erro
					$erros = array_merge($erros, $result);

					// liberando memoria
					unset($result);
				}

			}

		} catch(Exception $e) {
			throw $e;

		}

		//if(count($erros) <= 0){
			$data = array();
	
			foreach( $itens as $codpraca => $list ){
				// tratamos as fichas que contem subfichas que vem com barras "/" no campo da planilha
				$paginaAnterior = null;
				$ordemAnterior = null;
				$countLinhaExcel = 1;
	
				$primeiraChave = 0;
				foreach($list as $key => $value){
					$primeiraChave = $key;
					break;
				}
				
				for($x = $primeiraChave; $x <= count($list) + $primeiraChave - 1; $x++){
					$list[$x]['SUBFICHAS'] = array();
		
					if( ($paginaAnterior != $list[$x]['PAGINA_ITEM']) || ($ordemAnterior != $list[$x]['ORDEM_ITEM']) ){
						$c = 0;
						
						if(strpos($list[$x]['PRODUTO'], "/")){
							$arr = explode("/", $list[$x]['PRODUTO']);
		
							$list[$x]['PRODUTO'] = $arr[0];
							$list[$x]['LINHA_EXCEL'] = $countLinhaExcel;
							for($i = 0; $i <= count($arr) - 1; $i++){
								if($i > 0){ 
									$list[$x]['SUBFICHAS'][$arr[$i]] = $list[$x]; 
									$list[$x]['SUBFICHAS'][$arr[$i]]['PRODUTO'] = $arr[$i];
									$list[$x]['SUBFICHAS'][$arr[$i]]['LINHA_EXCEL'] = $countLinhaExcel;
								}
							}
						}
						else{
							$list[$x]['LINHA_EXCEL'] = $countLinhaExcel;
						}
						$countLinhaExcel++;				
						$paginaAnterior = $list[$x]['PAGINA_ITEM'];
						$ordemAnterior = $list[$x]['ORDEM_ITEM'];
					}
					else if( ($paginaAnterior == $list[$x]['PAGINA_ITEM']) && ($ordemAnterior == $list[$x]['ORDEM_ITEM']) ){
						if(strpos($list[$x]['PRODUTO'], "/")){
							$arr = explode("/", $list[$x]['PRODUTO']);
							for($i = 0; $i <= count($arr) - 1; $i++){	
									$list[$x -1 - $c]['SUBFICHAS'][$arr[$i]] = $list[$x];
									$list[$x -1 - $c]['SUBFICHAS'][$arr[$i]]['PRODUTO'] = $arr[$i];
									$list[$x -1 - $c]['SUBFICHAS'][$arr[$i]]['LINHA_EXCEL'] = $countLinhaExcel;
							}
							
							$paginaAnterior = $list[$x]['PAGINA_ITEM'];
							$ordemAnterior = $list[$x]['ORDEM_ITEM'];
							
							unset($list[$x]);
							$primeiraChave++;
							$c++;
						}
						else{
							$list[$x]['LINHA_EXCEL'] = $countLinhaExcel;
							$list[$x - 1 - $c]['SUBFICHAS'][] = $list[$x];
							$paginaAnterior = $list[$x]['PAGINA_ITEM'];
							$ordemAnterior = $list[$x]['ORDEM_ITEM'];
							unset($list[$x]);
							$primeiraChave++;
							$c++;
						}
						$countLinhaExcel++;
					}
				}
				
				$data[$codpraca] = realocarArray($list);
			}
			foreach( $data as $codpraca => $list ){
				foreach($list as $idx => $item){
					if(isset($this->cacheFichas[$item['PRODUTO']]['ID_FICHA'])){
						if( array_key_exists($item['PRODUTO'], $this->cacheFichas) ){
							$data[$codpraca][ $idx ]['ID_FICHA'] = $this->cacheFichas[$item['PRODUTO']]['ID_FICHA'];
		
							$data[$codpraca][ $idx ]['ID_CATEGORIA'] = $this->cacheFichas[$item['PRODUTO']]['ID_CATEGORIA'];
							$data[$codpraca][ $idx ]['ID_SUBCATEGORIA'] = $this->cacheFichas[$item['PRODUTO']]['ID_SUBCATEGORIA'];
							$data[$codpraca][ $idx ]['CATEGORIA'] = $this->cacheFichas[$item['PRODUTO']]['ID_CATEGORIA'];
							$data[$codpraca][ $idx ]['SUBCATEGORIA'] = $this->cacheFichas[$item['PRODUTO']]['ID_SUBCATEGORIA'];
							$data[$codpraca][ $idx ]['DESCRICAO_ITEM'] = $this->cacheFichas[$item['PRODUTO']]['NOME_FICHA'];
							$data[$codpraca][ $idx ]['CODIGO_ITEM'] = $this->cacheFichas[$item['PRODUTO']]['COD_BARRAS_FICHA'];
							$data[$codpraca][ $idx ]['ID_EXCEL'] = $excel['ID_EXCEL'];
							$data[$codpraca][ $idx ]['ID_PRACA'] = $codpraca;
							$data[$codpraca][ $idx ]['ID_EXCEL_VERSAO'] = $versao['ID_EXCEL_VERSAO'];
		
							foreach($data[$codpraca][ $idx ]['SUBFICHAS'] as $k => $sf){
								$subficha = $this->ficha->getSubichasClienteProdutos($cliente['ID_CLIENTE'], array('0'=>$sf['PRODUTO']));
							
								if(isset($subficha[$sf['PRODUTO']])){
									$sf['ID_FICHA'] =  $subficha[$sf['PRODUTO']]['ID_FICHA'];
									$sf['ID_CATEGORIA'] =  $subficha[$sf['PRODUTO']]['ID_CATEGORIA'];
									$sf['ID_SUBCATEGORIA'] =  $subficha[$sf['PRODUTO']]['ID_SUBCATEGORIA'];
									$sf['CATEGORIA'] =  $subficha[$sf['PRODUTO']]['ID_CATEGORIA'];
									$sf['SUBCATEGORIA'] =  $subficha[$sf['PRODUTO']]['ID_SUBCATEGORIA'];
									$sf['DESCRICAO_ITEM'] =  $subficha[$sf['PRODUTO']]['NOME_FICHA'];
									$sf['CODIGO_ITEM'] =  $subficha[$sf['PRODUTO']]['COD_BARRAS_FICHA'];
								}
								$sf['ID_EXCEL'] = $excel['ID_EXCEL'];
								$sf['STATUS_ITEM'] = 1;
								$sf['ID_PRACA'] = $codpraca;
								$sf['ID_EXCEL_VERSAO'] = $versao['ID_EXCEL_VERSAO'];
								$sf['ID_FICHA_PAI'] = !empty($data[$codpraca][ $idx ]['ID_FICHA']) ? $data[$codpraca][ $idx ]['ID_FICHA'] : "0";
								unset($sf['SUBFICHAS']);
								$data[$codpraca][ $idx ]['SUBFICHAS'][$k] = $sf;
							}
						}
					}
				}
			}
		//}

		// prepara os valores de retorno
		$retorno = array(
			'erros' => $erros,

			// so enviamos os itens se nao houve erro algum
			'itens' => empty($erros) ? $data : array()
		);

		Sessao::set('itens_excel', $data);

	//aqui ordenamos o array primeiro pela página, depois pela ordem
	//alteração solicitada pelo chamado 33338	
	foreach($retorno['erros'] as $key1 => $erros){
		
		if(isset($retorno['erros'][$key1]['avisos'])){
			$pracaItensAvisos = $retorno['erros'][$key1]['avisos'];
			$pracaOrdenadaAvisos = $this->ordenar_array($pracaItensAvisos, 'PAGINA', SORT_ASC, 'ORDEM', SORT_ASC);
			$retorno['erros'][$key1]['avisos'] = $pracaOrdenadaAvisos;
		}
		
		if(isset($retorno['erros'][$key1]['erros'])){
			$pracaItensErros = $retorno['erros'][$key1]['erros'];
			$pracaOrdenadaErros = $this->ordenar_array($pracaItensErros, 'PAGINA', SORT_ASC, 'ORDEM', SORT_ASC);
			$retorno['erros'][$key1]['erros'] = $pracaOrdenadaErros;
		}

	}
	
		return $retorno;
	}
	
	/**
	 * Função para ordenar o array primeiro pela página, depois pela ordem.
	 *
	 * @author Bruno de Oliveira
	 * @return Array
	 */
	
	private function ordenar_array() {
		$n_parametros = func_num_args();
		if ($n_parametros<3 || $n_parametros%2!=1) {
			return false;
		} else { 
			$arg_list = func_get_args();
	
			if (!(is_array($arg_list[0]) && is_array(current($arg_list[0])))) {
				return false;
			}
			for ($i = 1; $i<$n_parametros; $i++) {
				if ($i%2!=0) {
					if (!array_key_exists($arg_list[$i], current($arg_list[0]))) {
						return false;
					}
				} else { 
					if ($arg_list[$i]!=SORT_ASC && $arg_list[$i]!=SORT_DESC) {
						return false;
					}
				}
			}
			$array_salida = $arg_list[0];
	
			$a_evaluar = "foreach (\$array_salida as \$fila){\n";
			for ($i=1; $i<$n_parametros; $i+=2) {
				$a_evaluar .= "  \$campo{$i}[] = \$fila['$arg_list[$i]'];\n";
			}
			$a_evaluar .= "}\n";
					$a_evaluar .= "array_multisort(\n";
							for ($i=1; $i<$n_parametros; $i+=2) { 
								$a_evaluar .= "  \$campo{$i}, SORT_REGULAR, \$arg_list[".($i+1)."],\n";
							}
							$a_evaluar .= "  \$array_salida);";
	
							eval($a_evaluar);
							return $array_salida;
		}
	}
	
	
	
	/**
	 * Adiciona uma mensagem de erro na lista de erros
	 *
	 * Alterado para utilizar este metodo para centralizar
	 * a forma como as mensagens de erros sao adicionadas apos
	 * a validacao de uma ou planilhas de excel.
	 *
	 * @author Hugo Ferreira da Silva
	 * @param string $produto Codigo do produto informado na planilha
	 * @param string $nome Nome do produto indicado na planilha ou da ficha se informada
	 * @param string $pg Numero da pagina informado na planilha
	 * @param string $ordem Ordem do produto informado na planilha
	 * @param array $ficha Dados da ficha (se encontrado)
	 * @param string $msg Mensagem de erro a ser adicionada
	 * @param string $tipo Tipo de erro (erros | avisos)
	 * @return void
	 */
	private function addMsgErro($produto, $nome, $pg, $ordem, $ficha, $msg, $tipo = 'erros'){
		if( empty($ficha) ){
			$ficha = array();
		}

		if( !array_key_exists('NOME_FICHA', $ficha) ){
			$ficha['NOME_FICHA'] = $nome;
		}

		$ficha['PRODUTO'] = $produto;
		$ficha['PAGINA'] = $pg;
		$ficha['ORDEM'] = $ordem;
		$ficha['MENSAGEM'] = $msg;

		$this->errorsList[ $tipo ][] = $ficha;
	}
	
	/**
	 * Adiciona ao xml os objetos que contem na ficha em forma de elementos filhos do elemento ficha
	 *
	 * @author Sidnei
	 * @param array $objetos Array contendo os objetos da ficha
	 * @param objeto $xml Objeto xml
	 * @param objeto $fichaEl Elemento ficha dentro do xml onde serao inseridos os objetos (imagens)
	 * @param array $ElPersonalizados Array de elementos peronalizado, apenas elementos que serao inserido
	 * @param integer $ficha Dados da ficha (se encontrado)
	 * @return void
	 */
	private function adicionaObjetosNoXml($objetos, $xml, $fichaEl, $ElPersonalizados, $keySubficha, $hashset, $tipos, $hermes=null){
		$codes = array();

		foreach($objetos as $obj){
			$nomeTag = null;
			$tag = null;
			$path = $obj['PATH'];

			if( !empty($hashset[$obj['FILE_ID']]) ){
				$tipo = $tipos[ $hashset[$obj['FILE_ID']] ];
			} else {
				$tipo = 'Imagem';
			}

			$tipo = removeAcentos($tipo);
			
			if($hermes == null){
				// se nao for imagem principal
				if($obj['PRINCIPAL'] == '0'){
					if(empty($codes[$tipo])){
						$codes[$tipo] = 1;
					} else {
						$codes[$tipo]++;
					}

					if($keySubficha === null){
						$nomeTag = strtolower($tipo) . $codes[$tipo];	
					}
					else{
						$nomeTag = strtolower($tipo) . $codes[$tipo] . '_sub' . ($keySubficha+1);
					}
					
					$tag = $xml->createElement($nomeTag);
				// se for imagem principal
				} 
				else{
					if($keySubficha === null){
						$nomeTag = 'imagem_p';
					}
					else{
						$nomeTag = 'imagem_p_sub' . ($keySubficha+1);
					}
					
					$tag = $xml->createElement($nomeTag);
				}
			}
			else{
				// se nao for imagem principal
				if($obj['PRINCIPAL'] == 0){
					if(empty($codes[$tipo])){
						$codes[$tipo] = 1;
					} else {
						$codes[$tipo]++;
					}
					
					if($keySubficha === null){
						$nomeTag = strtolower($tipo) . $codes[$tipo];	
					}
					else{
						$nomeTag = strtolower($tipo) . $codes[$tipo] . '_sub' . ($keySubficha+1);
					}
					
					$tag = $xml->createElement($nomeTag);
				// se for imagem principal
				}
				else{
					if($keySubficha === null){
						$nomeTag = 'imagem_p';
					}
					else{
						$nomeTag = 'imagem_p_sub' . ($keySubficha+1);
					}

					$tag = $xml->createElement($nomeTag);
				}
			}
			
			if( $tag != null ){
				$link = str_replace(
							$this->config->item('caminho_storage'),
							$this->config->item('caminho_storage_fpo'),
							$path
						);

				$file  = $obj['FILENAME']; //substr($path, strrpos($path,'/')+1);
				$link = fromUtf8MAC($link.$file);

				$tag->setAttribute('link', $link);
				//$tag->setAttribute('data_alteracao', $this->xinet->getDataAlteracao($pathFile,$file));

				if( empty($datasAlteracao[$obj['FILE_ID']]) ){
					$time = time();
				} else {
					$time = $datasAlteracao[$obj['FILE_ID']];
				}

				$tag->setAttribute('data_alteracao', date('Y-m-d H:i:s', $time));
				
				// esses IF's sao utilizados para vericar as tags do UPDATE PERSONALIZADO ou UPDATE TOTAL
				if( count($ElPersonalizados) > 0 ){
					if( in_array($nomeTag, $ElPersonalizados) ){
						$fichaEl->appendChild($tag);
					}
				}
				else{
					$fichaEl->appendChild($tag);
				}
				
			}
		}	
	}
	
}


