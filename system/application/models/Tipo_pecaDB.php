<?php
class Tipo_pecaDB extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_TIPO_PECA','int','',1,1);
		$this->addField('DESC_TIPO_PECA','string','',8,0);
		$this->addField('FLAG_ANUNCIO_TIPO_PECA','int','',1,0);
		$this->addField('FLAG_ATIVO_TIPO_PECA','int','',1,0);
		$this->addField('FLAG_BLOQUEIO_LIMITE','int','',1,0);
		$this->addField('ID_CLIENTE','int','',1,0);
		$this->addField('MASCARA','string','',1,0);
		$this->addField('TEXTO_EXPLICATIVO','string','',1,0);
	}
	### END

	// nome da tabela
	var $tableName = 'TB_TIPO_PECA';

	function __construct(){
		parent::GenericModel();

		$this->addValidation('ID_CLIENTE', 'requiredString','Informe o cliente');
		$this->addValidation('DESC_TIPO_PECA', 'requiredString','Informe o nome');
		$this->addValidation('DESC_TIPO_PECA', 'function', array($this,'validate_descTipoPeca'));
	}

	/**
	 * Validacao por cliente
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data
	 * @return mixed True se passar, string se der erro
	 */
	public function validate_descTipoPeca( $data ){
		$rs = $this->db
			->where('ID_CLIENTE', val($data,'ID_CLIENTE',0))
			->where('DESC_TIPO_PECA', val($data,'DESC_TIPO_PECA'))
			->where('DESC_TIPO_PECA !=', empty($data['DESC_TIPO_PECA']) ? "" : $data['DESC_TIPO_PECA'])
			->get($this->tableName)
			->result_array();
 
		if( !empty($rs) ){
			return 'Tipo de peça já cadastrado ';
		}

		return true;
	}

	/**
	 * Lista os itens cadastrados
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $filters
	 * @param int $page
	 * @param int $limit
	 * @return array
	 */
	public function listItems($filters,$page=0,$limit=5){

		$this->db
			->join('TB_CLIENTE C','C.ID_CLIENTE = TB_TIPO_PECA.ID_CLIENTE');

		$this->setWhereParams($filters);
		return $this->execute($page,$limit, 'DESC_TIPO_PECA');
	}

	/**
	 * Recupera todos os tipo de peca que estao ativos no sistema
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idcliente codigo do cliente
	 * @return array lista contendo tipos de pecas ativos
	 */
	public function getTiposAtivos( $idcliente = null ){

		if( !is_null($idcliente) ){
			$this->db->where('ID_CLIENTE', $idcliente);
		}

		$rs = $this->db
			->where('FLAG_ATIVO_TIPO_PECA',1)
			->order_by('DESC_TIPO_PECA')
			->get($this->tableName)
			->result_array();

		return $rs;
	}


}

