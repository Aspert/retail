<?php
/**
 * Classe de modelo para o gerenciamento de aprovacao de producao
 * @author Juliano Polito
 * @link http://www.247id.com.br
 */
class AprovacaoComentarioDB extends GenericModel {
	### START
	protected function _initialize(){
		$this->addField('ID_APROVACAO_COMENTARIO','int','',1,1);
		$this->addField('ID_USUARIO','int','',1,0);
		$this->addField('COMENTARIO','string','',10,0);
		$this->addField('DATA_COMENTARIO','string','',10,0);
	}
	### END

	var $tableName = 'TB_APROVACAO_COMENTARIO';
	
	/**
	 * Construtor
	 *  
	 * @author Juliano Polito
	 * @link http://www.247id.com.br
	 * @return CampanhaDB
	 */
	function __construct(){
		parent::GenericModel();
		// adiciona as validacoes
		//$this->addValidation('OBS_APROVACAO','requiredString','Informe o nome');
	}
	
	
	public function getByArquivo($idArquivo){
		$comentarios = $this->db->join('TB_USUARIO U', 'U.ID_USUARIO = TB_APROVACAO_COMENTARIO.ID_USUARIO')
			->join('TB_APROVACAO_COMENTARIO_ARQUIVO ACA', 'ACA.ID_APROVACAO_COMENTARIO = TB_APROVACAO_COMENTARIO.ID_APROVACAO_COMENTARIO')
			->join('TB_GRUPO G', 'U.ID_GRUPO = G.ID_GRUPO')
			->join('TB_AGENCIA A', 'A.ID_AGENCIA = G.ID_AGENCIA')
			->where('ACA.ID_APROVACAO_ARQUIVO',$idArquivo)->get($this->tableName)->result_array();
		return $comentarios;
	}
	
	/**
	 * Metodo para listar todos os comentários na prova
	 * @author Bruno de Oliveira
	 * @link http://www.247id.com.br
	 * @param int idAprovacão
	 * @return $comentarios
	 */
	public function getByAP($AP){
		$comentarios = $this->db
			->select('TB_APROVACAO_COMENTARIO.*, J.TITULO_JOB, U.NOME_USUARIO, AG.DESC_AGENCIA, CG.DESC_CLIENTE, ACA.*, AA.TITULO_APROVACAO_ARQUIVO')
			->join('TB_APROVACAO_COMENTARIO_ARQUIVO ACA', 'ACA.ID_APROVACAO_COMENTARIO = TB_APROVACAO_COMENTARIO.ID_APROVACAO_COMENTARIO')
			->join('TB_APROVACAO_ARQUIVO AA', 'ACA.ID_APROVACAO_ARQUIVO = AA.ID_APROVACAO_ARQUIVO')
			->join('TB_APROVACAO AP', 'AP.ID_APROVACAO = AA.ID_APROVACAO')
			->join('TB_JOB J','J.ID_JOB = AP.ID_JOB')
			->join('TB_USUARIO U','U.ID_USUARIO = TB_APROVACAO_COMENTARIO.ID_USUARIO')
			->join('TB_GRUPO G','U.ID_GRUPO = G.ID_GRUPO')
			->join('TB_AGENCIA AG','AG.ID_AGENCIA = G.ID_AGENCIA','LEFT')
			->join('TB_CLIENTE CG','CG.ID_CLIENTE = G.ID_CLIENTE','LEFT')
			->where('AP.ID_APROVACAO',$AP)
			->group_by('TB_APROVACAO_COMENTARIO.ID_APROVACAO_COMENTARIO')
			->get($this->tableName)->result_array();
		
		return $comentarios;;
	}
	
}

