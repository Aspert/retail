<?php

/**
 * Model que representa a tabela TB_PRACA
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */

class PracaDB extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_PRACA','int','',1,1);
		$this->addField('DESC_PRACA','string','',8,0);
		$this->addField('STATUS_PRACA','int','',1,0);
		$this->addField('ID_REGIAO','int','',1,0);
	}
	### END

/**
	 * nome da tabela
	 * @var string
	 */
	var $tableName = 'TB_PRACA';

	/**
	 * Construtor
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return PracaDB
	 */
	function __construct(){
		parent::GenericModel();

		$this->addValidation('ID_REGIAO', 'requiredNumber','Informe a região');
		$this->addValidation('DESC_PRACA', 'requiredString','Informe o nome da praça');
		$this->addValidation('DESC_PRACA', 'function',array($this,'checaNomeDuplicado'));
	}

	/**
	 * Recupera uma praca pela descricao
	 *
	 * <p>Caso nao exista uma praca com a descricao informada, cria e retorna o registro</p>
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $desc descricao da praca
	 * @return array
	 */
	public function getByDescricao($desc){
		$rs = $this->db->where('DESC_PRACA',$desc)
			->get($this->tableName);

		if($rs->num_rows()==0){
			$this->save(array('DESC_PRACA'=>$desc,'STATUS_PRACA'=>1));

			$rs = $this->db->where('DESC_PRACA',$desc)
				->get($this->tableName);
		}

		return $rs->row_array();
	}

	public function getById($desc){
		$rs = $this->db->where('ID_PRACA',$desc)
					   ->get($this->tableName);
		return $rs->row_array();
	}

	/**
	 * Recupera uma praca pelo nome da praca e pelo codigo do cliente
	 *
	 * @author Hugo Ferreira da Silva
	 * @param string $desc Nome da praca
	 * @param int $idcliente Codigo do cliente
	 * @return array
	 */
	public function getByDescricaoCliente($desc,$idcliente, $idregiao = null, $somenteAtivo = false){
		$this->db
			->join('TB_REGIAO R','R.ID_REGIAO = P.ID_REGIAO')
			->where('P.DESC_PRACA',$desc)
			->where('R.ID_CLIENTE',$idcliente);

		if( !is_null($idregiao) ){
			$this->db->where('P.ID_REGIAO',$idregiao);
		}

		if( $somenteAtivo ){
			$this->db->where('P.STATUS_PRACA',1);
		}

		$rs = $this->db
			->get($this->tableName . ' P')
			->row_array();

		return $rs;
	}

	/**
	 * Lista as pracas cadastradas
	 * @author Hugo Ferreira da Silva
	 * @param array $filters
	 * @param int $page
	 * @param int $limit
	 * @return array
	 */
	public function listItems($filters, $page=0, $limit=5){
		$this->db
			->join('TB_REGIAO R', 'TB_PRACA.ID_REGIAO = R.ID_REGIAO', 'LEFT')
			->join('TB_CLIENTE C', 'R.ID_CLIENTE = C.ID_CLIENTE', 'LEFT');

		if( !empty($filters['ID_CLIENTE']) ){
			$this->db->where('C.ID_CLIENTE', $filters['ID_CLIENTE']);
		}

		$this->setWhereParams($filters);
		return $this->execute($page, $limit, 'DESC_CLIENTE, NOME_REGIAO, DESC_PRACA');
	}

	/**
	 * Recupera as pracas relacionadas a um excel
	 * @author Hugo Ferreira da Silva
	 * @param int $idExcel
	 * @return array
	 */
	public function getByExcel($idExcel){
		$ultima = $this->excelVersao->getVersaoAtual($idExcel);

		$rs = array();

		if( !empty($ultima) ){
			$rs = $this->db
				->join('TB_EXCEL_PRACA EP', 'EP.ID_PRACA = P.ID_PRACA')
				->where('ID_EXCEL_VERSAO', $ultima['ID_EXCEL_VERSAO'])
				->where('EP.ID_EXCEL', $idExcel)
				->get($this->tableName . ' P')
				->result_array();
		}

		return $rs;
	}

	/**
	 * Checa se o nome da praca nao esta duplicado para a regiao
	 * @author Hugo Ferreira da Silva
	 * @param array $data
	 * @return mixed
	 */
	public function checaNomeDuplicado( $data ){
		$total = $this->db
			->where('DESC_PRACA', $data['DESC_PRACA'])
			->where('ID_REGIAO', $data['ID_REGIAO'])
			->where('ID_PRACA != ', $data['ID_PRACA'])
			->from($this->tableName)
			->count_all_results();

		if( $total > 0 ){
			return 'Já existe uma praça com este nome para esta região';
		}

		return true;
	}

	/**
	 * Recupera as pracas por regiao
	 * @author Hugo Ferreira da Silva
	 * @param int $idregiao
	 * @return array
	 */
	public function getByRegiao($idregiao){
		$rs = $this->db
			->where('ID_REGIAO', $idregiao)
			->where('STATUS_PRACA', 1)
			->order_by('DESC_PRACA')
			->get($this->tableName)
			->result_array();

		return $rs;
	}

	/**
	 * Recupera pracas pelo codigo de varias regioes
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $regioes codigos das regioes desejadas
	 * @return array dados das pracas encontradas
	 */
	public function getByRegioes(array $regioes){
		$rs = $this->db
			->where_in('ID_REGIAO', $regioes)
			->where('STATUS_PRACA', 1)
			->order_by('DESC_PRACA')
			->get($this->tableName)
			->result_array();

		return $rs;
	}

	/**
	 * Recupera as pracas por codigos de produtos
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $produtos codigos dos produtos a serem pesquisados
	 * @return array dados das pracas encontradas
	 */
	public function getByProdutos(array $produtos){
		$rs = $this->db
			->from($this->tableName . ' P')

			->select('DISTINCT P.*')

			->join('TB_REGIAO R','R.ID_REGIAO = P.ID_REGIAO')
			->join('TB_REGIAO_PRODUTO RP','RP.ID_REGIAO = R.ID_REGIAO')

			->where_in('RP.ID_PRODUTO', $produtos)
			->where('P.STATUS_PRACA', 1)

			->order_by('P.DESC_PRACA')
			->get($this->tableName)
			->result_array();

		return $rs;
	}
	
	/**
	 * Recupera as pracas por id do cliente
	 *
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @param integer $idCliente id do cliente
	 * @return array dados das pracas encontradas
	 */
	public function getByCliente($idCliente){
		$rs = $this->db->from($this->tableName . ' P')
				->select('P.ID_PRACA, P.DESC_PRACA')
				->join('TB_REGIAO R', 'P.ID_REGIAO = R.ID_REGIAO')
				->where('R.ID_CLIENTE', $idCliente)
				->where('P.STATUS_PRACA', 1)
				->order_by('P.DESC_PRACA')
				->get()
				->result_array();
				
		return $rs;		
	}

	/**
	 * Recupera as pracas por id do Usuario
	 *
	 * @author Wagner Silveira
	 * @link http://www.gwdesenvolvimento.com.br
	 * @param integer $idUsuario id do Usuario
	 * @return array dados das pracas encontradas
	 */
	public function getByUsersPraca($idPraca){
		$rs = $this->db->from('tb_usuario_praca P')
				->select('P.ID_USUARIO')
				->where('P.ID_PRACA', $idPraca)
				->get()
				->result_array();
				
		return $rs;		
	}

	/**
	 * Recupera as pracas por id do Usuario
	 *
	 * @author Wagner Silveira
	 * @link http://www.gwdesenvolvimento.com.br
	 * @param integer $idUsuario id do Usuario
	 * @return array dados das pracas encontradas
	 */
	public function getByUsuario($idUsuario){
		$rs = $this->db->from('tb_usuario_praca P')
				->select('P.ID_PRACA')
				->where('P.ID_USUARIO', $idUsuario)
				->get()
				->result_array();
				
		return $rs;		
	}
	
	/**
	 * Recupera as pracas por id do job
	 *
	 * @author Bruno de Oliveira
	 * @link http://www.247id.com.br
	 * @param integer $idJob id do job
	 * @return array dados das pracas encontradas
	 */
	public function getByIdJob($idJob){
		$rs = $this->db->from($this->tableName . ' P')
		->select('P.ID_PRACA, P.DESC_PRACA')
		->join('TB_EXCEL_ITEM EI', 'P.ID_PRACA = EI.ID_PRACA')
		->join('TB_EXCEL E', 'EI.ID_EXCEL = E.ID_EXCEL')
		->join('TB_JOB J', 'E.ID_JOB = J.ID_JOB')
		->where('P.STATUS_PRACA', '1')
		->where('EI.STATUS_ITEM', '1')
		->where('J.ID_JOB', $idJob)
		->group_by('P.ID_PRACA')
		->get()
		->result_array();
	
		return $rs;
	}
}

