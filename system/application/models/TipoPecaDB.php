<?php
class TipoPecaDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_TIPO_PECA','int','',1,1);
		$this->addField('DESC_TIPO_PECA','string','',8,0);
		$this->addField('FLAG_ANUNCIO_TIPO_PECA','int','',1,0);
		$this->addField('FLAG_ATIVO_TIPO_PECA','int','',1,0);
	}
	### END

	var $tableName = 'TB_TIPO_PECA';

	/**
	 * Construtor 
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return JobDB
	 */
	function __construct(){
		parent::GenericModel();
		$this->addValidation('DESC_TIPO_PECA','requiredString','Informe o nome');
		$this->addValidation('FLAG_ATIVO_TIPO_PECA','requiredNumber','Informe o status');		
	}
	
	/**
	 * Lista os itens cadastrados
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $filters array com os filtros
	 * @param int $page numero da pagina
	 * @param int $limit numero de itens por pagina
	 * @return array
	 */
	public function listItems($filters, $page=0, $limit=5){
		$this->setWhereParams($filters);
		return $this->execute($page, $limit, 'DESC_TIPO_PECA');
	}
	
	
}

