<?php
/**
 *
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class ExcelItemDB extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_EXCEL_ITEM','int','',1,1);
		$this->addField('ID_EXCEL','int','',1,0);
		$this->addField('ID_PRACA','int','',2,0);
		$this->addField('ID_FICHA','int','',5,0);
		$this->addField('CODIGO_ITEM','string','',6,0);
		$this->addField('DESCRICAO_ITEM','string','',0,0);
		$this->addField('STATUS_ITEM','int','',1,0);
		$this->addField('ORDEM_ITEM','int','',1,0);
		$this->addField('PAGINA_ITEM','int','',1,0);
		$this->addField('ID_EXCEL_VERSAO','int','',1,0);
		$this->addField('TEXTO_LEGAL','blob','',0,0);
		$this->addField('SUBCATEGORIA','string','',3,0);
		$this->addField('DISPONIVEL_ITEM','int','',1,0);
		$this->addField('OBSERVACOES_ITEM','blob','',0,0);
		$this->addField('EXTRA1_ITEM','blob','',0,0);
		$this->addField('EXTRA2_ITEM','blob','',0,0);
		$this->addField('EXTRA3_ITEM','blob','',0,0);
		$this->addField('EXTRA4_ITEM','blob','',0,0);
		$this->addField('EXTRA5_ITEM','blob','',0,0);
		$this->addField('ID_FICHA_PAI','int','',0,0);
		$this->addField('ID_CENARIO','int',0,0,0);
		$this->addField('LINHA_EXCEL','int','',0,0);
	}
	### END

	var $tableName = 'TB_EXCEL_ITEM';

	/**
	 * Persiste um item do excel item
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data Dados para serem salvos
	 * @return int id do item salvo
	 */
	public function saveItem($data){
		// agora sempre vamos gravar
		$data['STATUS_ITEM'] = 1;
		return $this->save($data);
	}

	/**
	 * Salva multiplos itens e atribui o valor da chave salva no elemento da lista
	 *
	 * @author Hugo Ferreira da Silva
	 * @param array $lista Lista de elementos a serem salvos
	 * @return array A propria lista com o valor de cada chave atribuido a cada elemento
	 */
	public function saveItems(array $lista){
		$sql = 'INSERT INTO TB_EXCEL_ITEM (';
		$fields = array();
		$lines = array();

		foreach($lista as $row){
			if(empty($fields)){
				foreach($row as $key => $value){
					if( array_key_exists($key, $this->mapping) && $key != 'ID_EXCEL_ITEM'){
						$fields[] = $key;
					}
				}
				$sql .= implode(', ', $fields) . ') VALUES ';
			}

			$values = array();
			foreach($row as $key => $value){
				if( array_key_exists($key, $this->mapping) && $key != 'ID_EXCEL_ITEM'){
					$values[] = sprintf("'%s'", mysql_escape_string($value));
				}
			}

			$lines[] = '(' . implode(', ', $values) . ')';
		}

		if(!empty($lines)){
			$sql .= implode(', ' . PHP_EOL, $lines);

			$this->db->query($sql);
			$id = $this->db->insert_id();

			foreach($lista as $idx => $row){
				$lista[$idx]['ID_EXCEL_ITEM'] = $id++;
			}
		}

		return $lista;
	}

	/**
	 * Desativa os itens que nao estao na lista de encontrados
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $itemsFound lista de codigos com os itens encontrados
	 * @return void
	 */
	public function deactivateItemsNotFound($idExcel, $itemsFound){
		$this->db->where_not_in('ID_EXCEL_ITEM', $itemsFound)
			->where('ID_EXCEL',$idExcel)
			->update($this->tableName, array('STATUS_ITEM'=>0));

		// agora, vamos procurar os itens que tem a mesma ordem
		// e removemos os que estao inativos
		foreach($itemsFound as $id){
			$rs = $this->db->where('ID_EXCEL_ITEM',$id)
				->get($this->tableName)
				->row_array();

			if(!empty($rs)){
				$this->db->where('ID_EXCEL', $idExcel)
					->where('ORDEM_ITEM', $rs['ORDEM_ITEM'])
					->where('STATUS_ITEM', '0')
					->delete($this->tableName);

			}
		}

	}

	/**
	 * Desativa itens da versao atual
	 *
	 * <p>Quando um elemento nao esta presente na versao atual mas estava
	 * na anterior, ele insere o item na atual e o marca como "desativado",
	 * para sabermos como vai ficar no XML para o InDesign</p>
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idVersao Codigo da versao do excel
	 * @return void
	 */
	public function desativaPorVersaoPraca($idVersao, $idPraca){
		$atual = $this->excelVersao->getById($idVersao);
		$anterior = $this->excelVersao->getVersaoAnterior($idVersao);

		// so continuamos se tem versao anterior
		// do contrario, caimos fora
		if(empty($anterior)){
			return;
		}

		// ok, tem versao anterior, vamos comparar
		$itens_anterior = array();

		// pegamos os itens da versao anterior
		$tmp = $this->getByVersaoPraca($anterior['ID_EXCEL_VERSAO'], $idPraca);
		foreach($tmp as $item){
			$itens_anterior[ $item['PAGINA_ITEM'] ][ $item['ORDEM_ITEM'] ] = $item;
		}

		// agora ja temos os anteriores
		// vamos remover dos anteriores os atuais que temos
		$tmp = $this->getByVersaoPraca($atual['ID_EXCEL_VERSAO'], $idPraca);
		foreach($tmp as $item){
			unset($itens_anterior[ $item['PAGINA_ITEM'] ][ $item['ORDEM_ITEM'] ]);
		}

		// agora, dos anteriores, sobraram somente os que NAO estao
		// presentes no atual. vamos grava-los, entao, como inativos
		// SE nao tiver nenhum na ordem da versao atual
		foreach($itens_anterior as $pagina => $data){
			foreach($data as $ordem => $item){
				// tiramos o id
				unset($item['ID_EXCEL_ITEM']);
				// informamos a versao
				$item['ID_EXCEL_VERSAO'] = $idVersao;
				// colocamos como inativo
				$item['STATUS_ITEM'] = 0;
				// gravamos o item
				$this->save($item);
			}
		}
	}

	/**
	 * Recupera os itens por um excel e praca
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idExcel
	 * @param int $idPraca
	 * @param int $idVersao
	 * @param boolean $somenteAtivos
	 * @return void
	 */
	public function getByExcelPracaVersao($idExcel, $idPraca, $idVersao, $somenteAtivos=false){
		$this->db
			->where('ID_PRACA',$idPraca)
			->where('ID_EXCEL',$idExcel)
			->where('ID_EXCEL_VERSAO',$idVersao)
			->order_by('PAGINA_ITEM, ORDEM_ITEM, LINHA_EXCEL, ID_FICHA_PAI');

		if($somenteAtivos){
			$this->db->where('STATUS_ITEM', 1);
		}

		return $this->db->get($this->tableName)->result_array();
	}

	/**
	 * Recupera os itens por um excel, praca, versao e paginas de inicio e fim
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idExcel
	 * @param int $idPraca
	 * @param int $idVersao
	 * @param boolean $somenteAtivos
	 * @param int $inicio
	 * @param int $fim
	 * @return array
	 */
	public function getByExcelPracaVersaoRange($idExcel, $idPraca, $idVersao, $somenteAtivos=false, $inicio, $fim){
		$this->db
			->where('ID_PRACA',$idPraca)
			->where('ID_EXCEL',$idExcel)
			->where('ID_EXCEL_VERSAO',$idVersao)
			->where(sprintf('PAGINA_ITEM BETWEEN %d AND %d', $inicio, $fim))
			->order_by('PAGINA_ITEM, ORDEM_ITEM, LINHA_EXCEL, ID_FICHA_PAI');

		if($somenteAtivos){
			$this->db->where('STATUS_ITEM', 1);
		}

		return $this->db->get($this->tableName)->result_array();
	}
	
	/**
	 * Deleta os itens filhos pro versao
	 *
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @param int $idVersao
	 * @return void
	 */
	public function deleteFilhasByVersaoPraca($idVersao, $idPraca){
		$this->db
			->where('ID_PRACA', $idPraca)
			->where('ID_EXCEL_VERSAO', $idVersao)
			->where('ID_FICHA_PAI > ', 0)
			->delete('TB_EXCEL_ITEM');
	}
	
	public function deleteByPraca($idJob, $idPraca){
		
		
		//Busca o idExcel do Job
		$xls = $this->excel->getByJob($idJob);
		
		$this->db
			->where('ID_PRACA', $idPraca)
		    ->where('ID_EXCEL', $xls['ID_EXCEL'])
			->delete('TB_EXCEL_ITEM');
	}

	/**
	 * Recupera os itens por versao e praca
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idVersao
	 * @param int $idPraca
	 * @param boolean $comPricing indica se e para trazer o pricing junto
	 * @return array
	 */
	public function getByVersaoPraca($idVersao, $idPraca, $comPricing = false){
		$this->db
			->where('ID_PRACA',$idPraca)
			->where('ID_EXCEL_VERSAO',$idVersao)
			->where('(ID_FICHA_PAI = 0 or ID_FICHA_PAI is null)')
			->order_by('PAGINA_ITEM, ORDEM_ITEM');

		if( $comPricing ){
			$this->db
				->select($this->tableName.'.*, P.ID_PRICING')
				->join('TB_PRICING P','TB_EXCEL_ITEM.ID_EXCEL_ITEM = P.ID_EXCEL_ITEM','LEFT');
		}

		return $this->db->get($this->tableName)->result_array();
	}
	
	/**
	 * Recupera os itens filhos por versao e praca
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idVersao
	 * @param int $idPraca
	 * @param boolean $comPricing indica se e para trazer o pricing junto
	 * @return array
	 */
	public function getFilhasByVersaoPraca($idVersao, $idPraca, $idPai, $comPricing = false){
		$this->db
			->where($this->tableName . '.ID_PRACA',$idPraca)
			->where('ID_EXCEL_VERSAO',$idVersao)
			->where('ID_FICHA_PAI', $idPai)
			->join('TB_SUBCATEGORIA S', $this->tableName . '.SUBCATEGORIA = S.ID_SUBCATEGORIA')
			->join('TB_CATEGORIA C', 'S.ID_CATEGORIA = C.ID_CATEGORIA')
			->join('TB_PRACA PR', $this->tableName . '.ID_PRACA = PR.ID_PRACA')
			->order_by('PAGINA_ITEM, ORDEM_ITEM');

		if( $comPricing ){
			$this->db
				->select($this->tableName.'.*, P.ID_PRICING')
				->join('TB_PRICING P','TB_EXCEL_ITEM.ID_EXCEL_ITEM = P.ID_EXCEL_ITEM','LEFT');
		}

		return $this->db->get($this->tableName)->result_array();
	}

	/**
	 * Recupera o pricing de um item em uma praca
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idItem codigo do item
	 * @return array
	 */
	public function getPricing($idItem){
		$rs = $this->db->select('P.*,
									I.EXTRA1_ITEM AS EXTRA1,
									I.EXTRA2_ITEM AS EXTRA2,
									I.EXTRA3_ITEM AS EXTRA3,
									I.EXTRA4_ITEM AS EXTRA4,
									I.EXTRA5_ITEM AS EXTRA5')
			->from($this->tableName.' I')
			->join('TB_EXCEL E','E.ID_EXCEL = I.ID_EXCEL')
			->join('TB_PRICING P','P.ID_EXCEL_ITEM = I.ID_EXCEL_ITEM')
			->join('TB_JOB J','J.ID_JOB = E.ID_JOB')
			->where('I.ID_EXCEL_ITEM', $idItem)
			->get();

		return $rs->row_array();
	}

	/**
	 * Zera a paginacao dos itens de um excel
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idexcel
	 * @param int $idversao
	 * @return void
	 */
	public function zeraPaginacao($idexcel, $idversao = -1){
		if( $idversao == -1 ){
			$versao = $this->excelVersao->getVersaoAtual($idexcel);
			$idversao = $versao['ID_EXCEL_VERSAO'];
		}

		$data = array('PAGINA_ITEM' => 0, 'ORDEM_ITEM' => 0);

		$this->db
			->where('ID_EXCEL_VERSAO', $idversao)
			->update($this->tableName, $data);
	}


	public function atualizarLista( array $lista, $insertPricing = false ){
		$fields = array();
		$values = array();

		foreach($this->mapping as $field){
			$fields[] = $field->name;
		}

		foreach($lista as $item){
			$row = array();
			foreach($this->mapping as $field){
				$row[] = !isset($item[$field->name]) || is_null($item[$field->name])
					? 'NULL'
					:  $this->db->escape( $item[$field->name] );
			}

			$values[] = '(' . implode(', ', $row) . ')';
		}

		$str = 'INSERT INTO TB_EXCEL_ITEM ';
		$str .= '(' . implode(', ', $fields) .') VALUES ' . implode(', '.PHP_EOL, $values);
		$str .= ' ON DUPLICATE KEY UPDATE STATUS_ITEM = 1, ';

		foreach($this->mapping as $field){
			if( empty($field->primary) && $field->name != 'STATUS_ITEM' ){
				$str .= $field->name.' = VALUES(' . $field->name . '), ';
			}
		}

		// tira a virgula final
		$str = substr($str, 0, -2);

		$this->db->query($str);


		// se for para atualizar o pricing
		// todos os itens de EXCEL_ITEM NAO PODEM TER ID!!!
		// so passar este parametro quando realmente estiver
		// inserindo todos os itens
		if( $insertPricing ){
			$last = $this->db->insert_id();

			foreach($lista as $idx => $item){
				$lista[$idx]['ID_EXCEL_ITEM'] = $last++;
			}

			$this->pricing->atualizarLista($lista);
		}
	}

	/**
	 * Recupera a praca do item
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idItem codigo do item
	 * @return array
	 */
	public function getById($idItem){
		$rs = $this->db->select('*')
				->from($this->tableName)
				->where('ID_EXCEL_ITEM', $idItem)
				->join('TB_SUBCATEGORIA S', $this->tableName . '.SUBCATEGORIA = S.ID_SUBCATEGORIA')
				->join('TB_CATEGORIA C', 'S.ID_CATEGORIA = C.ID_CATEGORIA')
				->join('TB_PRACA PR', $this->tableName . '.ID_PRACA = PR.ID_PRACA')
				->join('TB_OBJETO_FICHA OF', $this->tableName . '.ID_FICHA = OF.ID_FICHA')
				->join('TB_OBJETO O', 'OF.ID_OBJETO = O.ID_OBJETO')
				->get();
		return $rs->row_array();
	}
	
	/**
	 * Pegar itens pela praça e job
	 *
	 * @author Esdras Eduardo
	 * @link http://www.247id.com.br
	 * @param int $idJob codigo do job
	 * @param int $idPraca codigo da praça
	 * @return array
	 */
	public function getByJobPraca($idJob=0, $idPraca){
		
		//Array do xls
		$xls = array();
		
		//Busca o idExcel do Job
		$xls = $this->excel->getByJob($idJob);
		
		//Se encontrou
		if (!empty($xls['ID_EXCEL'])) {
			$rs = $this->db->select('*')
				->from($this->tableName)
				->where('ID_EXCEL', $xls['ID_EXCEL'])
				->where('ID_PRACA', $idPraca)
				->where('PAGINA_ITEM !=', 0)
				->where('ORDEM_ITEM !=', 0)
				->get();
			return $rs->result_array();
		} else {
			return array();
		}
	}
	
	public function getAllByJobPraca($idJob=0, $idPraca){
		
		//Array do xls
		$xls = array();
		
		//Busca o idExcel do Job
		$xls = $this->excel->getByJob($idJob);
		
		//Se encontrou
		if (!empty($xls['ID_EXCEL'])) {
			$rs = $this->db->select('*')
				->from($this->tableName)
			    ->join('TB_PRICING P','P.ID_EXCEL_ITEM = TB_EXCEL_ITEM.ID_EXCEL_ITEM')
				->where('ID_EXCEL', $xls['ID_EXCEL'])
				->where('ID_PRACA', $idPraca)
				->get();
			return $rs->result_array();
		} else {
			return array();
		}
	}
	
	/**
	 * Zera a paginacao dos itens de um excel por job e praça 
	 *
	 * @author Esdras Eduardo
	 * @param int $idJob
	 * @param int $idPraca
	 * @param int $idItem
	 * @return void
	 */
	public function zeraPaginacaoByPraca($idJob=0, $idPraca, $idItem){

		//Array do xls
		$xls = array();
		
		//Busca o idExcel do Job
		$xls = $this->excel->getByJob($idJob);
		
		//Se encontrou
		if (!empty($xls['ID_EXCEL'])) {
			$data = array('PAGINA_ITEM' => 0, 'ORDEM_ITEM' => 0);
			$this->db
				->where('ID_EXCEL', $xls['ID_EXCEL'])
				->where('ID_PRACA', $idPraca)
				->where('ID_FICHA', $idItem)
				->update($this->tableName, $data);
		}
	}
	
	/**
	 * Copiar paginação de praças diferentes e retornar se a posição estava diferente
	 *
	 * @author Esdras Eduardo
	 * @param int $idJob
	 * @param int $idPracaTo
	 * @param int $idPracaFrom
	 * @param int $idItem
	 * @return bool $retorno
	 */
	public function copiarPaginacao($idJob=0, $idPracaTo, $idPracaFrom, $idItem){
		
		$retorno = false;
		
		//Array do xls
		$xls = array();
		
		//Busca o idExcel do Job
		$xls = $this->excel->getByJob($idJob);
		
		//Se encontrou
		if (!empty($xls['ID_EXCEL'])) {
			
			//Busca Ficha da Praca destino
			$this->db
					->where('ID_EXCEL', $xls['ID_EXCEL'])
					->where('ID_PRACA', $idPracaTo)
					->where('ID_FICHA', $idItem)
					->from($this->tableName);
					
					$count_ficha = $this->db->count_all_results();
					
			//Se encontrou
			if( $count_ficha > 0 ){
				
				//Busca pagina e ordem da ficha da praca destino
				$toItem = $this->db->select('PAGINA_ITEM, ORDEM_ITEM')
					->where('ID_EXCEL', $xls['ID_EXCEL'])
					->where('ID_PRACA', $idPracaTo)
					->where('ID_FICHA', $idItem)
					->get($this->tableName)->row();
					
				//Busca pagina e ordem da ficha da praca origem
				$fromItem = $this->db->select('PAGINA_ITEM, ORDEM_ITEM')
					->where('ID_EXCEL', $xls['ID_EXCEL'])
					->where('ID_PRACA', $idPracaFrom)
					->where('ID_FICHA', $idItem)
					->get($this->tableName)->row();
				
				//Verifica se a pagina/ordem da ficha destino eh igual a pagina/ordem da ficha origem
				$retorno = !( ($toItem->PAGINA_ITEM == $fromItem->PAGINA_ITEM) && ($toItem->ORDEM_ITEM == $fromItem->ORDEM_ITEM) );
				
				//Se forem diferentes
				if( $retorno ){
					//Copia a pagina/ordem da ficha origem para a pagina/ordem da ficha destino
					$data = array('PAGINA_ITEM' => $fromItem->PAGINA_ITEM, 'ORDEM_ITEM' => $fromItem->ORDEM_ITEM);
					$this->db
					->where('ID_EXCEL', $xls['ID_EXCEL'])
					->where('ID_PRACA', $idPracaTo)
					->where('ID_FICHA', $idItem)
					->update($this->tableName, $data);
				}
			}
		}
		
		return $retorno;
			
	}
	
	/**
	 * Recupera os itens por um excel e praca
	 *
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @param int $idExcel
	 * @param int $idPraca
	 * @param boolean $somenteAtivos
	 * @return void
	 */
	public function getByExcelCodigoPracaPagina($idExcel, $codigo, $idPraca, $pagina, $somenteAtivos=false, $subfichas=true){
		$this->db
			->join('TB_FICHA F', 'EI.ID_FICHA = F.ID_FICHA')
			->where('ID_EXCEL',$idExcel)
			->where('CODIGO_ITEM',$codigo)
			->where('ID_PRACA',$idPraca)
			->where('PAGINA_ITEM',$pagina)
			->order_by('PAGINA_ITEM, ORDEM_ITEM');

		if($somenteAtivos){
			$this->db->where('STATUS_ITEM', 1);
		}
		
		if($subfichas == false){
			$this->db->where('ID_FICHA_PAI', 0);
		}

		return $this->db->get($this->tableName.' EI')->result_array();
	}	
	
	/**
	 * Recupera os itens por um excel e praca
	 *
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @param int $idExcel
	 * @param int $idPraca
	 * @param boolean $pagina
	 * * @param boolean $ordem
	 * @return void
	 */
	public function getByExcelPracaPaginaOrdem($idExcel, $idPraca, $pagina, $ordem, $somenteAtivos=false){
		$this->db
			->join('TB_FICHA F', 'EI.ID_FICHA = F.ID_FICHA')
			->where('ID_EXCEL',$idExcel)
			->where('ID_PRACA',$idPraca)
			->where('PAGINA_ITEM',$pagina)
			->where('ORDEM_ITEM',$ordem)
			->order_by('PAGINA_ITEM, ORDEM_ITEM');

		if($somenteAtivos){
			$this->db->where('STATUS_ITEM', 1);
		}

		return $this->db->get($this->tableName.' EI')->result_array();
	}	
	
	/**
	 * Recupera o total de cada subcategoria
	 *
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @param int $idExcel
	 * @param int $idPraca
	 * @param boolean $pagina
	 * * @param boolean $ordem
	 * @return void
	 */
	public function getTotalSubcategoriasByExcelPraca($idExcel, $idPraca){	
		$this->db->select('EC.ID_SUBCATEGORIA, count(EI.ID_EXCEL_ITEM) as TOTAL')
			->join($this->tableName . ' EI', 'EC.ID_EXCEL = EI.ID_EXCEL and EC.ID_PRACA = EI.ID_PRACA and EC.ID_SUBCATEGORIA = EI.SUBCATEGORIA and EI.STATUS_ITEM = 1 and (EI.ID_FICHA_PAI = 0 or EI.ID_FICHA_PAI is null)', 'left')
			->where('EC.ID_EXCEL', $idExcel)
			->where('EC.ID_PRACA', $idPraca)
			->group_by('EC.ID_SUBCATEGORIA');
		return $this->db->get('TB_EXCEL_CATEGORIA EC')->result_array();
	}	
	
	/**
	 * Recupera a ID do Cenario pelo ID DO EXCEL, ID DA PRACA, ID DA VERSAO E ID DA FICHA
	 *
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @param int $idExcel
	 * @param int $idPraca
	 * @param boolean $idVersao
	 * * @param boolean $idFicha
	 * @return int
	 */
	public function getCenarioByIdExcelPracaVersaoFicha($idExcel, $idPraca, $idVersao, $idFicha){	
		$this->db->select('ID_CENARIO')
			->where('ID_EXCEL', $idExcel)
			->where('ID_PRACA', $idPraca)
			->where('ID_EXCEL_VERSAO', $idVersao)
			->where('ID_FICHA', $idFicha);
		$rs = $this->db->get($this->tableName)->result_array();
		
		if( count($rs) == 1){
			return $rs[0]['ID_CENARIO'];
		}
		else{
			return 0;
		}
	}
	
	/**
	 * Recupera todas fichas (excel item)  relacionadas a um cenario
	 *
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @param int $idExcel
	 * @param int $idVersao
	 * @param boolean $somenteAtivos
	 * @return void
	 */
	public function getUnicosByExcelVersaoComCenarios($idExcel, $idVersao, $somenteAtivos=true){
		$this->db
			->select('F.COD_BARRAS_FICHA, F.NOME_FICHA, E.ID_FICHA, C.ID_CENARIO')
			->select("GROUP_CONCAT(DISTINCT C.COD_CENARIO ORDER BY C.COD_CENARIO DESC SEPARATOR ', ') AS CENARIOS")
			->join('TB_CENARIO C', 'E.ID_CENARIO = C.ID_CENARIO')
			->join('TB_FICHA F', 'E.ID_FICHA = F.ID_FICHA')
			->where('ID_EXCEL',$idExcel)
			->where('ID_EXCEL_VERSAO',$idVersao)
			->group_by('E.ID_FICHA')
			->order_by('PAGINA_ITEM, ORDEM_ITEM');

		if($somenteAtivos){
			$this->db->where('STATUS_ITEM', 1);
		}

		return $this->db->get($this->tableName . ' E')->result_array();
	}
	
	/**
	 * Recupera os itens subfichas por versao e praca
	 *
	 * @author Sidnei
	 * @link http://www.247id.com.br
	 * @param int $idVersao
	 * @param int $idPraca
	 * @param boolean $comPricing indica se e para trazer o pricing junto
	 * @return array
	 */
	public function getSubfichasByVersaoPraca($idVersao, $idPraca, $idPai, $comPricing = false){
		$this->db
			->where($this->tableName . '.ID_PRACA',$idPraca)
			->where('ID_EXCEL_VERSAO',$idVersao)
			->where('ID_FICHA_PAI', $idPai)
			->join('TB_SUBCATEGORIA S', $this->tableName . '.SUBCATEGORIA = S.ID_SUBCATEGORIA')
			->join('TB_CATEGORIA C', 'S.ID_CATEGORIA = C.ID_CATEGORIA')
			->join('TB_PRACA PR', $this->tableName . '.ID_PRACA = PR.ID_PRACA')
			->order_by('PAGINA_ITEM, ORDEM_ITEM');

		if( $comPricing ){
			$this->db
				->select($this->tableName.'.*, P.ID_PRICING')
				->join('TB_PRICING P','TB_EXCEL_ITEM.ID_EXCEL_ITEM = P.ID_EXCEL_ITEM','LEFT');
		}

		return $this->db->get($this->tableName)->result_array();
	}
	
	//recupera os itens por idjob
	//chamado 25346
	
	public function getByIdJob($idjob) {
		
		$sql = "SELECT ITEM.ORDEM_ITEM, ITEM.PAGINA_ITEM, EX.DESC_EXCEL, FIC.COD_BARRAS_FICHA,
					FIC.NOME_FICHA, ITEM.TEXTO_LEGAL, ITEM.ID_PRACA, PRI.PRECO_UNITARIO, PRI.TIPO_UNITARIO, PRI.PRECO_CAIXA,
					PRI.TIPO_CAIXA, PRI.PRECO_VISTA, PRI.PRECO_DE, PRI.PRECO_POR, PRI.ECONOMIZE, PRI.ENTRADA,
					PRI.PARCELAS, PRI.PRESTACAO, PRI.JUROS_AM, PRI.JUROS_AA, PRI.TOTAL_PRAZO, PRI.OBSERVACOES,
					ITEM.EXTRA1_ITEM, ITEM.EXTRA2_ITEM, ITEM.EXTRA3_ITEM, ITEM.EXTRA4_ITEM, ITEM.EXTRA5_ITEM, 
					CAMP.VALOR_CAMPO_FICHA, CAMP.PRINCIPAL, FIC.TIPO_FICHA, JOB.TITULO_JOB, AG.DESC_AGENCIA,
					CLI.DESC_CLIENTE, CAM.DESC_CAMPANHA, JOB.ID_JOB, PRO.DESC_PRODUTO, FIC.TIPO_FICHA, FILENAME
					FROM TB_EXCEL EX
					JOIN TB_EXCEL_ITEM ITEM ON EX.ID_EXCEL = ITEM.ID_EXCEL
					JOIN TB_JOB JOB ON EX.ID_JOB = JOB.ID_JOB
					JOIN TB_AGENCIA AG ON JOB.ID_AGENCIA = AG.ID_AGENCIA
					JOIN TB_CLIENTE CLI ON JOB.ID_CLIENTE = CLI.ID_CLIENTE
					JOIN TB_CAMPANHA CAM ON CAM.ID_CAMPANHA = JOB.ID_CAMPANHA
					JOIN TB_PRODUTO PRO ON PRO.ID_PRODUTO = CAM.ID_PRODUTO
					JOIN TB_PRICING PRI ON PRI.ID_EXCEL_ITEM = ITEM.ID_EXCEL_ITEM
					JOIN TB_FICHA FIC ON FIC.ID_FICHA = ITEM.ID_FICHA
					JOIN TB_CATEGORIA CAT ON CAT.ID_CATEGORIA = FIC.ID_CATEGORIA
					JOIN TB_OBJETO_FICHA  OBJ ON OBJ.ID_FICHA = FIC.ID_FICHA
					JOIN TB_OBJETO OBJN ON OBJN.ID_OBJETO = OBJ.ID_OBJETO
					LEFT JOIN TB_CAMPO_FICHA CAMP ON CAMP.ID_FICHA = FIC.ID_FICHA
					WHERE EX.ID_JOB = $idjob
					AND FIC.FLAG_ACTIVE_FICHA = 1
					AND ITEM.DISPONIVEL_ITEM = 1
					GROUP BY FIC.COD_BARRAS_FICHA
					ORDER BY ITEM.PAGINA_ITEM ASC, ITEM.ORDEM_ITEM ASC";
		
		$res = $this->db->query($sql)->result_array();
		return $res;
		
	}
	
	public function updateExcelItemByFichaJob($data,$id_ficha,$id_excel) {
	
		$this->db->where('TB_EXCEL_ITEM.ID_FICHA', $id_ficha);
		$this->db->where('TB_EXCEL_ITEM.ID_EXCEL', $id_excel);
		$this->db->update('TB_EXCEL_ITEM', $data);
	
	}
	
	public function updateExcelItemByIdsJobFicha($data,$id_ficha,$id_excel) {
	
		$this->db->where_in('TB_EXCEL_ITEM.ID_FICHA', $id_ficha);
		$this->db->where('TB_EXCEL_ITEM.ID_EXCEL', $id_excel);
		$this->db->update('TB_EXCEL_ITEM', $data);
	}
	
	// pega o valor total de ordens de produtos na pagina, ou seja, a ultima ordem
	public function getTotalOrdemByExcelPracaPagina($idExcel, $idPraca, $pagina){
		$this->db
			->select('MAX(ORDEM_ITEM) AS ORDEM_MAXIMA')
			->where('ID_EXCEL',$idExcel)
			->where('PAGINA_ITEM',$pagina)
			->where('ID_PRACA',$idPraca);
		$retorno = $this->db->get($this->tableName)->result_array();
		return $retorno['0']['ORDEM_MAXIMA'];
	}
	
	// Recupera os itens por um excel, praca, versao e paginas e suas respectivas ordens
	public function getByExcelPracaVersaoPaginaOrdem($idExcel, $idPraca, $idVersao, $somenteAtivos=false, $paginaOrdem=array()){
		$sql = "SELECT * FROM (TB_EXCEL_ITEM)
					WHERE ID_PRACA = '" .$idPraca. "'
					AND ID_EXCEL = '" .$idExcel. "'
					AND ID_EXCEL_VERSAO = '" .$idVersao. "' ";
		
		if($somenteAtivos){
			$sql = $sql . "AND STATUS_ITEM = 1 ";
		}
		
		if(count($paginaOrdem)){
			$sql = $sql . "AND (";
			foreach($paginaOrdem as $key=>$value){
				$sql = $sql . "PAGINA_ITEM = " .$key. " AND ORDEM_ITEM IN (" .implode(',', $value). ") OR ";
			}
			$sql = $sql . " 1=2 )";
		}
		
		$sql = $sql . "ORDER BY PAGINA_ITEM, ORDEM_ITEM, LINHA_EXCEL, ID_FICHA_PAI;";
	
		$res = $this->db->query($sql)->result_array();
		
		return $res;
	}

}

