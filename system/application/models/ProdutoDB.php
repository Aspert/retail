<?php
/**
 * Classe de modelo para o gerenciamento de linha de produto
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class ProdutoDB extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_PRODUTO','int','',1,1);
		$this->addField('ID_CLIENTE','int','',1,0);
		$this->addField('DESC_PRODUTO','string','',3,0);
		$this->addField('STATUS_PRODUTO','int','',1,0);
		$this->addField('CHAVE_STORAGE_PRODUTO','string','',0,0);
		$this->addField('ID_TRANSFORMADOR','int','',1,0);
		$this->addField('ID_CALENDARIO','int','',1,0);
	}
	### END

	var $tableName = 'TB_PRODUTO';
	
	/**
	 * Construtor
	 *  
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return ProdutoDB
	 */
	function __construct(){
		parent::GenericModel();
		
		// adiciona as validacoes
		$this->addValidation('ID_CLIENTE','requiredNumber','Informe o cliente');
		$this->addValidation('DESC_PRODUTO','requiredString','Informe o nome');
		$this->addValidation('DESC_PRODUTO','function',array($this,'checaNomeDuplicado'));
		$this->addValidation('STATUS_PRODUTO','requiredNumber','Informe o status');
		$this->addValidation('CHAVE_STORAGE_PRODUTO','function',array($this,'checaChaveStorage'));
	}
	
	public function getById($id){
		$rs = $this->db
			->select('C.*, P.*')
			->join('TB_CLIENTE C','C.ID_CLIENTE = P.ID_CLIENTE')
			->where('ID_PRODUTO',$id)
			->get($this->tableName.' P')
			->row_array();
			
		return $rs;
	}
	
	public function save($dados, $id = null){
		$id = parent::save($dados, $id);

		///////////////////////////////////////////////////////////////
		///////////////// SALVANDO OS FORNECEDORES ////////////////////
		///////////////////////////////////////////////////////////////
		$this->db
			->where('ID_PRODUTO', $id)
			->delete('TB_FORNECEDOR_CLIENTE_PRODUTO');
		
		if( !empty($dados['agencias']) ){
			foreach($dados['agencias'] as $idagencia){
				$item = array(
					'ID_AGENCIA' => $idagencia,
					'ID_PRODUTO' => $id,
					'ID_CLIENTE' => $dados['ID_CLIENTE'],
				);
				$this->db->insert('TB_FORNECEDOR_CLIENTE_PRODUTO', $item);
			}
		}
		
		///////////////////////////////////////////////////////////////
		//////////////////// SALVANDO AS REGIOES //////////////////////
		///////////////////////////////////////////////////////////////
		$this->db
			->where('ID_PRODUTO', $id)
			->delete('TB_REGIAO_PRODUTO');
			
		if( !empty($dados['regioes']) ){
			foreach($dados['regioes'] as $idregiao){
				$item = array(
					'ID_REGIAO' => $idregiao,
					'ID_PRODUTO' => $id,
				);
				$this->db->insert('TB_REGIAO_PRODUTO', $item);
			}
		}
		
		return $id;
	}
	
	/**
	 * Metodo para listar itens
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data Dados para filtro
	 * @param int $pagina numero da pagina atual
	 * @param int $limit limit de itens por pagina
	 * @return array
	 */
	public function listItems($data, $pagina=0, $limit = 5){
		$this->db->join('TB_CLIENTE','TB_CLIENTE.ID_CLIENTE=TB_PRODUTO.ID_CLIENTE');
			
		$this->setWhereParams($data);
		return $this->execute($pagina, $limit, 'DESC_CLIENTE, DESC_PRODUTO');
	}
	
	/**
	 * 
	 * @author Juliano Polito
	 * @param $idCliente
	 * @return array
	 */
	public function getByCliente($idCliente){
		$res = $this->db->where('ID_CLIENTE', $idCliente)
			->where('STATUS_PRODUTO', 1)
			->order_by('DESC_PRODUTO')
			->get($this->tableName);
		
		return $res->result_array();
	}
	
	/**
	 * Recupera os produtos pela agencia
	 * @author Hugo Ferreira da Silva
	 * @param int $idAgencia
	 * @param int $idCliente
	 * @return array
	 */
	public function getByAgencia($idAgencia, $idCliente = null){
		if( !is_null($idCliente) ){
			$this->db->where('FCP.ID_CLIENTE', $idCliente);
		}
		$res = $this->db
			->join('TB_FORNECEDOR_CLIENTE_PRODUTO FCP','FCP.ID_PRODUTO = P.ID_PRODUTO')
			->where('FCP.ID_AGENCIA', $idAgencia)
			->where('STATUS_PRODUTO', 1)
			->order_by('DESC_PRODUTO')
			->get($this->tableName . ' P');
			
		return $res->result_array();
	}
	
	/**
	 * Recupera os produtos filtrando por agencia e usuario
	 * 
	 * Opcionalmente, pode-se filtrar pelo cliente
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idAgencia
	 * @param int $idUsuario
	 * @param int $idCliente
	 * @return array
	 */
	public function getByAgenciaUsuario($idAgencia, $idUsuario, $idCliente = null){
		if( !is_null($idCliente) ){
			$this->db->where('FCP.ID_CLIENTE', $idCliente);
		}
		$res = $this->db
			->join('TB_FORNECEDOR_CLIENTE_PRODUTO FCP','FCP.ID_PRODUTO = P.ID_PRODUTO')
			->join('TB_PRODUTO P2','P2.ID_PRODUTO = P.ID_PRODUTO')
			->join('TB_GRUPO_PRODUTO GP','GP.ID_PRODUTO = P2.ID_PRODUTO AND P2.STATUS_PRODUTO = 1')
			->join('TB_USUARIO U','U.ID_GRUPO = GP.ID_GRUPO')
			->select('P.*')
			->where('FCP.ID_AGENCIA', $idAgencia)
			->where('U.ID_USUARIO', $idUsuario)
			->where('P.STATUS_PRODUTO', 1)
			->order_by('P.DESC_PRODUTO')
			->get($this->tableName . ' P');
			
		return $res->result_array();
	}
	
	/**
	 * Recupera os produtos por cliente e usuario
	 * 
	 * @param int $idCliente
	 * @param int $idUsuario
	 * @return array
	 */
	public function getByClienteUsuario($idCliente, $idUsuario){
		
		$sql = '(
			SELECT P.* FROM TB_PRODUTO P
			JOIN TB_GRUPO_PRODUTO GP ON GP.ID_PRODUTO = P.ID_PRODUTO
			JOIN TB_USUARIO U ON U.ID_GRUPO = GP.ID_GRUPO AND U.ID_USUARIO = %1$d
			WHERE P.ID_CLIENTE = %2$d AND P.STATUS_PRODUTO = 1
		) UNION (
			SELECT P.* FROM TB_PRODUTO P
			JOIN TB_FORNECEDOR_CLIENTE_PRODUTO FP ON FP.ID_PRODUTO = P.ID_PRODUTO AND FP.ID_CLIENTE = %2$d
			JOIN TB_GRUPO G ON FP.ID_AGENCIA = G.ID_AGENCIA AND G.ID_CLIENTE IS NULL
			JOIN TB_USUARIO U ON U.ID_GRUPO = G.ID_GRUPO AND U.ID_USUARIO = %1$d
			WHERE P.ID_CLIENTE = %2$d AND P.STATUS_PRODUTO = 1
		) ORDER BY DESC_PRODUTO';
		
		$sql = sprintf($sql, $idUsuario, $idCliente);
		$res = $this->db->query($sql);
		
		return $res->result_array();
	}
	
	/**
	 * Recupera os codigos dos produtos associados a um cliente e usuario
	 * @author Hugo Ferreira da Silva
	 * @param int $idCliente
	 * @param int $idUsuario
	 * @return array
	 */
	public function getCodigosByClienteUsuario($idCliente, $idUsuario){
		$codes = array();
		$lista = $this->getByClienteUsuario($idCliente, $idUsuario);
		foreach($lista as $item){
			$codes[] = $item['ID_PRODUTO'];
		}
		
		return $codes;
	}
	
	/**
	 * Recupera todos os produtos associados a um usuario
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idusuario
	 * @return array
	 */
	public function getByUsuario($idusuario){
		$sql = '(
			SELECT P.* FROM TB_PRODUTO P
			JOIN TB_GRUPO_PRODUTO GP ON GP.ID_PRODUTO = P.ID_PRODUTO
			JOIN TB_USUARIO U ON U.ID_GRUPO = GP.ID_GRUPO AND U.ID_USUARIO = %1$d
			WHERE P.STATUS_PRODUTO = 1
		) UNION (
			SELECT P.* FROM TB_PRODUTO P
			JOIN TB_FORNECEDOR_CLIENTE_PRODUTO FP ON FP.ID_PRODUTO = P.ID_PRODUTO
			JOIN TB_GRUPO G ON FP.ID_AGENCIA = G.ID_AGENCIA AND G.ID_CLIENTE IS NULL
			JOIN TB_USUARIO U ON U.ID_GRUPO = G.ID_GRUPO AND U.ID_USUARIO = %1$d 
			WHERE  P.STATUS_PRODUTO = 1
		) ORDER BY DESC_PRODUTO';
		
		$sql = sprintf($sql, $idusuario);
		$res = $this->db->query($sql);
		
		return $res->result_array();
	}
	
	/**
	 * Recupera os codigos dos produtos associados a um usuario
	 * @author Hugo Ferreira da Silva
	 * @param int $idUsuario
	 * @return array
	 */
	public function getCodigosByUsuario($idUsuario){
		$codes = array();
		$lista = $this->getByUsuario($idUsuario);
		foreach($lista as $item){
			$codes[] = $item['ID_PRODUTO'];
		}
		
		return $codes;
	}
	
	/**
	 * Recupera os produtos vinculados a um grupo
	 * 
	 * @param int $idGrupo
	 * @return array
	 */
	public function getByGrupo($idGrupo){
		$res = $this->db
			->join('TB_GRUPO_PRODUTO GP','GP.ID_PRODUTO = P.ID_PRODUTO')
			->where('GP.ID_GRUPO', $idGrupo)
			->where('STATUS_PRODUTO', 1)
			->order_by('DESC_PRODUTO')
			->get($this->tableName . ' P');
		
		return $res->result_array();
	}
	
	/**
	 * Recupera itens filtrando a partir de sessao
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $session Dados da sessao
	 * @param array $filters Demais dados para filtro
	 * @return array
	 */
	public function filterBySession($session, $filters=array()){
		$this->db->from($this->tableName);
		
		if(!empty($session['ID_CLIENTE'])){
			$this->db->where('ID_CLIENTE', $session['ID_CLIENTE']);
		}
		
		$this->db->order_by('DESC_PRODUTO');
		$this->setWhereParams($filters);
		
		return $this->db->get()->result_array();
	}
	
	/**
	 * Checa nome duplicado de produto
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data
	 * @return mixed
	 */
	public function checaNomeDuplicado($data){
		$idproduto = empty($data['ID_PRODUTO']) ? 0 : sprintf('%d',$data['ID_PRODUTO']);
		
		$rs = $this->db->where('ID_CLIENTE', $data['ID_CLIENTE'])
			->where('DESC_PRODUTO',$data['DESC_PRODUTO'])
			->where('ID_PRODUTO != ', $idproduto)
			->get($this->tableName);

		if($rs->num_rows() > 0){
			return 'Já existe um produto com este nome para este cliente';
		}
		
		return true;
	}
	
	/**
	 * Retorna a pasta de uma linha de produto
	 * 
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idProduto codigo do produto
	 * @return string
	 */
	public function getPath($idProduto){
		
		$path = '';
		
		//Busca o produto
		$rs = $this->getById($idProduto);
		
		//Verifica se encontrou
		if (!empty($rs) && is_array($rs)) {
			$path = $this->cliente->getPath($rs['ID_CLIENTE']);
			$path .= $rs['CHAVE_STORAGE_PRODUTO'] . '/';
		}
			
		return $path;
	}
	
	/**
	 * Verifica se a chave do storage ja nao pertecen a outro produto
	 * @author Hugo Ferreira da Silva
	 * @param array $data
	 * @return mixed
	 */
	public function checaChaveStorage($data){
		if( !empty($data['CHAVE_STORAGE_PRODUTO']) ){
			$id = empty($data['ID_PRODUTO']) ? 0 : sprintf('%d',$data['ID_PRODUTO']);
			
			$rs = $this->db->where('CHAVE_STORAGE_PRODUTO',$data['CHAVE_STORAGE_PRODUTO'])
				->where('ID_PRODUTO != ', $id)
				->where('ID_CLIENTE', $data['ID_CLIENTE'])
				->get($this->tableName);
	
			if($rs->num_rows() > 0){
				return 'Já existe uma chave de storage com este nome';
			}
		}
		
		return true;
	}
	
	/**
	 * Recupera os fornecedores de um produto
	 * @author Hugo Ferreira da Silva
	 * @param int $idproduto
	 * @return array
	 */
	public function getFornecedores($idproduto){
		$rs = $this->db
			->join('TB_FORNECEDOR_CLIENTE_PRODUTO FCP','FCP.ID_AGENCIA = A.ID_AGENCIA')
			->where('FCP.ID_PRODUTO', $idproduto)
			->order_by('DESC_AGENCIA')
			->get('TB_AGENCIA A')
			->result_array();
			
		return $rs;
	}
}





