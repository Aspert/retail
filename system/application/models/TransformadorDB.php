<?php
class TransformadorDB extends GenericModel {
	### START
	protected function _initialize(){
		$this->addField('ID_TRANSFORMADOR','int','',0,1);
		$this->addField('NOME','string','',0,0);
		$this->addField('DESCRICAO','blob','',0,0);
		$this->addField('ATIVO','int','',0,0);
	}
	### END

	var $tableName = 'TB_TRANSFORMADOR';

	/**
	 * Construtor
	 *
	 * @author Juliano Polito
	 * @link http://www.247id.com.br
	 * @return CampanhaDB
	 */
	function __construct(){
		parent::GenericModel();

		$this->addValidation('NOME', 'requiredString', 'Informe o nome do transformador');
		$this->addValidation('ATIVO', 'function', array($this,'checaAcoes'));
	}

	public function checaAcoes($data){
		if( empty($data['ACOES']) ){
			return 'Informe ao menos uma ação';
		}

		if( !is_array($data['ACOES']) ){
			return 'O parametro ACOES não é um array';
		}

		foreach($data['ACOES'] as $item){
			if( empty($item['ACAO']) ){
				return 'Um ou mais itens de ações não foram selecionados';
			}
		}

		return true;
	}

	/**
	 * Lista os transformadores cadastrados
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data
	 * @param int $pagina
	 * @param int $limit
	 * @return array
	 */
	public function listItems(array $data, $pagina=0, $limit = 5){
		$this->setWhereParams($data);
		return $this->execute($pagina, $limit, 'NOME', 'ASC');
	}

	public function save($data, $id = null){

		$id = parent::save($data, $id);

		$this->transformadorAcao->removeFrom($id);

		if( !empty($data['ACOES']) ){

			foreach($data['ACOES'] as $acao){
				if( !empty($acao['ACAO']) ){
					$acao['ID_TRANSFORMADOR'] = $id;
					$acao['PARAMETROS'] = serialize($acao['PARAMETROS']);
					$this->transformadorAcao->save($acao);
				}
			}
		}

	}

	/**
	 * Carrega as acoes de um transformador
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do transformador
	 * @return array lista de acoes de transformacao
	 */
	public function getActions($id){
		$rs = $this->db
			->from( 'TB_TRANSFORMADOR_ACAO' )
			->where('ID_TRANSFORMADOR', $id )
			->order_by('ID_TRANSFORMADOR_ACAO ASC')
			->get()
			->result_array();

		$rs = array_map(
			create_function('$row',
				'$row["PARAMETROS"] = unserialize($row["PARAMETROS"]);
				if( empty($row["PARAMETROS"]) ){
					$row["PARAMETROS"] = array();
				}
				return $row;'
			)
			, $rs
		);

		return $rs;
	}
}

