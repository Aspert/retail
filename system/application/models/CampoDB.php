<?php
/**
 * Model para controle de dados da tabela TB_CAMPO
 * @author Hugo Silva
 */
class CampoDB extends GenericModel {
	### START
	protected function _initialize(){
		$this->addField('ID_CAMPO','int','',1,1);
		$this->addField('NOME_CAMPO','string','',9,0);
		$this->addField('ID_CLIENTE','int','',1,0);
		$this->addField('DESCRICAO','string','',0,0);
		$this->addField('STATUS_CAMPO','int','',1,0);
	}
	### END

	/**
	 * nome da tabela
	 * @var string
	 */
	public $tableName = 'TB_CAMPO'; 
	
	/**
	 * Construtor
	 * @author Hugo Silva
	 * @return CampoDB
	 */
	function __construct(){
		parent::GenericModel();
		$this->addValidation('ID_CLIENTE','requiredNumber','Informe o cliente');
		$this->addValidation('NOME_CAMPO','requiredString','Informe o nome do campo');
		$this->addValidation('NOME_CAMPO','function',array($this,'validateNome'));
	}
	
	/**
	 * Recupera um id pelo nome do campo
	 * @author Hugo Silva
	 * @param string $nome
	 * @return int
	 */
	public function getIdByNome($nome){
		$rs = $this->db->where('NOME_CAMPO',$nome)->get($this->tableName);
		return $rs['ID_CAMPO'];
	}
	
	/**
	 * Recupera um nome pelo ID
	 * @author Hugo Silva
	 * @param int $id
	 * @return string
	 */
	public function getNomeById($id){
		$rs = $this->db->where('ID_CAMPO',$id)->get($this->tableName);
		return $rs['NOME_CAMPO'];
	}
	
	/**
	 * Recupera todos os registros e coloca no padrao para elemento SELECT do html do CodeIgniter
	 * @author Hugo Silva
	 * @return array
	 */
	public function getForDropdown(){
		$rs = $this->order('NOME_CAMPO ASC')->get($this->tableName);
		$list = array();
		foreach($rs->result_array() as $item){
			$list[$item['ID_CAMPO']] = $item['NOME_CAMPO'];
		}
		
		return $list;
	}
	
	/**
	 * Lista os itens cadastrados
	 * 
	 * @author Hugo Silva
	 * @param array $filters
	 * @param int   $page
	 * @param int   $limit
	 * @return array 
	 */
	public function listItems(array $filters, $page=0, $limit=20){
		$this->db
			->join('TB_CLIENTE C','C.ID_CLIENTE = TB_CAMPO.ID_CLIENTE','LEFT')
			->order_by('DESC_CLIENTE, NOME_CAMPO');
			if(isset($filters['STATUS_CAMPO']) && $filters['STATUS_CAMPO'] == '0'){
				$this->db->where('STATUS_CAMPO', 0);
			}
		$this->setWhereParams($filters);
		return $this->execute($page, $limit);
	}
	
	/**
	 * pega os campos para o cliente
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $cliente
	 * @return array
	 */
	public function getByCliente($cliente){
		$rs = $this->db
			->where('ID_CLIENTE', $cliente)
			->where('STATUS_CAMPO', 1)
			->order_by('NOME_CAMPO','ASC')
			->get($this->tableName)
			->result_array();
			
		return $rs;
	}
	
	/**
	 * Checa se o nome tem 
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param $data
	 * @return boolean
	 */
	public function validateNome($data){
		if(!preg_match('@^[a-z,A-Z,0-9,\s]+$@', $data['NOME_CAMPO'])){
			return 'São aceitos somente letras e numeros';
		}
		
		return true;
	}
	
	function getIdByName($NOME){
		$sql = "SELECT DISTINCT C.ID_CAMPO FROM TB_CAMPO C
					INNER JOIN TB_FICHA F ON  C.ID_CLIENTE = F.ID_CLIENTE
					INNER JOIN TB_CAMPO_FICHA CF ON F.ID_FICHA = CF.ID_FICHA
					WHERE C.NOME_CAMPO = '".$NOME."'";
	
	
		$rs = $this->db->query($sql);
		$arr = $rs->result_array();
		
		return $arr;
	}	
}