<?php
/**
 * Classe de modelo para o gerenciamento de itens que aparecem no menu
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class ItemMenuDB extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_ITEM_MENU','int','',1,1);
		$this->addField('ID_PAI','int','',0,0);
		$this->addField('ID_FUNCTION','int','',2,0);
		$this->addField('ID_ITEM_MENU_CATEGORIA','int','',0,0);
		$this->addField('DESC_ITEM_MENU','string','',6,0);
		$this->addField('ORDEM_ITEM_MENU','int','',1,0);
		$this->addField('SHOW_ITEM_MENU','int','',1,0);
	}
	### END

	var $tableName = 'TB_ITEM_MENU';
	
	const ICON_PATH = 'img/menu/';
	
	/**
	 * Construtor
	 *  
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return ItemMenuDB
	 */
	function __construct(){
		parent::GenericModel();
		
		// adiciona as validacoes
		$this->addValidation('DESC_ITEM_MENU','requiredString','Informe o nome');
		$this->addValidation('ID_FUNCTION','requiredNumber','Informe a função que será chamada');
		$this->addValidation('ORDEM_ITEM_MENU','requiredNumber','Informe a ordem do menu');
	}
	
	/**
	 * (non-PHPdoc)
	 * @see system/application/libraries/GenericModel#getById($id)
	 */
	public function getById($id){
		$rs = $this->db->from($this->tableName.' I')
			->join('TB_FUNCTION F','F.ID_FUNCTION=I.ID_FUNCTION')
			->join('TB_CONTROLLER C','C.ID_CONTROLLER=F.ID_CONTROLLER')
			->where('ID_ITEM_MENU', $id)
			->get();
		
		return $rs->row_array();
	}
	
	/**
	 * Metodo para listar itens
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data Dados para filtro
	 * @param int $pagina numero da pagina atual
	 * @param int $limit limit de itens por pagina
	 * @return array
	 */
	public function listItems($data, $pagina=0, $limit = 5){
		$this->db->join('TB_FUNCTION F','F.ID_FUNCTION = TB_ITEM_MENU.ID_FUNCTION')
			->join('TB_CONTROLLER C','C.ID_CONTROLLER = F.ID_CONTROLLER');
			
		if(!empty($data['ID_CONTROLLER'])){
			$this->db->where('C.ID_CONTROLLER',$data['ID_CONTROLLER']);
		}
			
		$this->setWhereParams($data);
		return $this->execute($pagina, $limit, 'ORDEM_ITEM_MENU');
	}
	
	/**
	 * Recupera os itens que aparecerao no menu para um usuario
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idUsuario Codigo do usuario
	 * @return array
	 */
	public function getItemsByUsuario($idUsuario){
		$res = $this->db->from('TB_USUARIO U')
			->select('IM.DESC_ITEM_MENU, IM.ID_ITEM_MENU, IM.ORDEM_ITEM_MENU')
			->select('C.CHAVE_CONTROLLER, F.CHAVE_FUNCTION')
			->join('TB_GRUPO G','G.ID_GRUPO = U.ID_GRUPO')
			->join('TB_PERMISSAO P','P.ID_GRUPO = G.ID_GRUPO')
			->join('TB_FUNCTION F','F.ID_FUNCTION = P.ID_FUNCTION')
			->join('TB_CONTROLLER C','C.ID_CONTROLLER = F.ID_CONTROLLER')
			->join('TB_ITEM_MENU IM','F.ID_FUNCTION = IM.ID_FUNCTION')
			->where('U.ID_USUARIO',$idUsuario)
			->where('IM.SHOW_ITEM_MENU',1)
			->order_by('IM.ORDEM_ITEM_MENU')
			->get();
			
		return $res->result_array();
	}
	
	/**
	 * Atualiza a ordem de um item do menu
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do item de menu
	 * @param int $order Nova ordem
	 * @return void
	 */
	public function updateOrder($id, $order){
		$this->save(array('ORDEM_ITEM_MENU'=>$order), $id);
	}
	
	/**
	 * Recupera a URL de um icone (se existir)
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id codigo do item do menu
	 * @return string
	 */
	public function getIcon($id){
		$path = self::ICON_PATH;
		$ext = array('gif','png','jpg');
		
		foreach($ext as $item){
			$name = $id.'.'.$item;
			if(file_exists($path.$name)){
				return base_url().$path.$name;
			}
		}
		
		return '';
	}
	
	/**
	 * Grava um icone na pasta certa
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id codigo do menu
	 * @param string $tmp nome temporario do arquivo
	 * @param string $filename nome do arquivo enviado
	 * @return void
	 */
	public function saveIcon($id, $tmp, $filename){
		$path = self::ICON_PATH;
		
		if(!is_dir($path)){
			mkdir($path,0777,true);
		}
		
		$ext = substr($filename, strrpos($filename,'.')+1);
		$name = $id . '.' . $ext;
		
		move_uploaded_file($tmp,$path.$name);
	}
}

