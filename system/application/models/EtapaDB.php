<?php
/**
 * Classe para gerenciamento dos dados da tabela TB_ETAPA
 *
 * @author Hugo Silva
 *
 */
class EtapaDB extends GenericModel {
	### START
	protected function _initialize(){
		$this->addField('ID_ETAPA','int','',1,1);
		$this->addField('CHAVE_ETAPA','string','',9,0);
		$this->addField('DESC_ETAPA','string','',18,0);
		$this->addField('COR_ETAPA','string','',6,0);
		$this->addField('ORDEM_ETAPA','int','',1,0);
	}
	### END



	/**
	 * Nome da tabela que sera acessada
	 * @var string
	 */
	public $tableName = 'TB_ETAPA';

	/**
	 * Recupera o registro pela chave
	 * @author Hugo Silva
	 * @param string $key nome da chave
	 * @return array Array contendo os valores do registro
	 */
	public function getByKey($key){
		$rs = $this->db->where('CHAVE_ETAPA',$key)->get($this->tableName);

		if($rs->num_rows() == 0){
			return array();
		}

		return $rs->row_array();
	}

	/**
	 * Recupera o ID da etapa pela chave
	 * @author Juliano Polito
	 * @param $key A chave da etapa
	 * @return O ID caso encontre, ou null caso não encontre
	 */
	public function getIdByKey($key){
		$row = $this->getByKey($key);
		return $row["ID_ETAPA"];
	}

	/**
	 * Recupera os itens que podem ser exibidos na aprovacao
	 *
	 * @author Hugo Ferreira da Silva
	 * @return array
	 */
	public function getItensForAprovacao(){
		$rs = $this->db
			->where_not_in('CHAVE_ETAPA', array('ENVIADO_AGENCIA','CHECKLIST','APROVACAO','FINALIZADO','PDF_OPI'))
			->order_by('DESC_ETAPA ASC')
			->get($this->tableName)
			->result_array();

		return $rs;
	}

	/**
	 * Recupera todos os itens para o controle de processo
	 *
	 * @author Hugo Ferreira da Silva
	 * @return array
	 */
	public function getItensControleProcessos(){
		
		//$denied = array('FINALIZADO','CHECKLIST');

		if( !empty($denied) ){
			$this->db->where_not_in('CHAVE_ETAPA', $denied);
		}

		$rs = $this->db
			->order_by('ORDEM_ETAPA')
			->get($this->tableName)
			->result_array();

		return $rs;
	}

}
