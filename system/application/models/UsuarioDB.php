<?php
/**
 * Classe de modelo para gerenciamento dos dados do usuario
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class UsuarioDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_USUARIO','int','',1,1);
		$this->addField('NOME_USUARIO','string','',22,0);
		$this->addField('EMAIL_USUARIO','string','',23,0);
		$this->addField('LOGIN_USUARIO','string','',10,0);
		$this->addField('DOMINIO_USUARIO','string','',8,0);
		$this->addField('STATUS_USUARIO','int','',1,0);
		$this->addField('DT_CADASTRO_USUARIO','timestamp','',19,0);
		$this->addField('ID_GRUPO','int','',1,0);
	}
	### END

	/**
	 * Nome da tabela
	 * @var string
	 */
	var $tableName = 'TB_USUARIO';
	var $statusField = 'STATUS_USUARIO';
	
	/**
	 * Construtor
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return UsuarioDB
	 */
	function __construct(){
		parent::GenericModel();
		
		$this->addValidation('NOME_USUARIO','requiredString','Informe o nome');
		$this->addValidation('DOMINIO_USUARIO','requiredString','Informe o dominio');
		$this->addValidation('LOGIN_USUARIO','requiredString','Informe o login');
		$this->addValidation('LOGIN_USUARIO','requiredUnique','Login já cadastrado');
		$this->addValidation('STATUS_USUARIO','requiredNumber','Informe o status');
		$this->addValidation('ID_GRUPO','requiredNumber','Informe o grupo do usuário');
	}
	
	/**
	 * Recupera lista de usuarios ativos por GACP.
	 * Os registros retornados ja possuem joins com tabelas relacionadas ao GACP
	 * @author Juliano Polito
	 * @param $idGACP
	 * @return array
	 */
	public function getByGACP($idGACP) {
		$rs = $this->db->from($this->tableName.' U')
			->select('U.*,G.*,A.ID_AGENCIA, A.DESC_AGENCIA,C.ID_CLIENTE, C.DESC_CLIENTE,P.ID_PRODUTO,P.DESC_PRODUTO')
			->join('TB_G_A_C_P GACP','U.ID_G_A_C_P = GACP.ID_G_A_C_P')
			->join('TB_GRUPO G','G.ID_GRUPO = GACP.ID_GRUPO')
			->join('TB_AGENCIA A','A.ID_AGENCIA = GACP.ID_AGENCIA AND A.STATUS_AGENCIA = 1','LEFT')
			->join('TB_CLIENTE C','C.ID_CLIENTE = GACP.ID_CLIENTE AND C.STATUS_CLIENTE = 1','LEFT')
			->join('TB_PRODUTO P','P.ID_PRODUTO = GACP.ID_PRODUTO AND P.STATUS_PRODUTO = 1','LEFT')
			->where('U.STATUS_USUARIO',1)
			->where('U.ID_G_A_C_P', $idGACP)
			->order_by('U.NOME_USUARIO ASC')
			->get();

		return $rs->result_array();
	}
	
	/**
	 * Recupera os usuarios de um grupo
	 * @author Hugo Ferreira da Silva
	 * @param int $idGrupo Codigo do grupo
	 * @return array
	 */
	public function getByGrupo($idGrupo){
		$rs = $this->db
			->from($this->tableName . ' U')
			->join('TB_GRUPO G','G.ID_GRUPO = U.ID_GRUPO')
			->join('TB_AGENCIA A','A.ID_AGENCIA = G.ID_AGENCIA','LEFT')
			->join('TB_CLIENTE C','C.ID_CLIENTE = G.ID_CLIENTE','LEFT')
			->where('U.STATUS_USUARIO', 1)
			->where('U.ID_GRUPO', $idGrupo)
			->order_by('NOME_USUARIO')
			->get()
			->result_array();
			
		return $rs;
	}
	
	/**
	 * 
	 * @see GenericModel::getById
	 */
	public function getById($id=null) {
		$rs = $this->db->from($this->tableName.' U')
			->join('TB_GRUPO G','G.ID_GRUPO = U.ID_GRUPO')
			->where('ID_USUARIO', $id)
			->get();

		return $rs->row_array();
	}
	
	/**
	 * Recupera um usuario pelo login
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param $login
	 * @return array
	 */
	function getByLogin($login) {
		$rs = $this->db->from($this->tableName.' U')
			->join('TB_GRUPO G','G.ID_GRUPO = U.ID_GRUPO')
			->join('TB_AGENCIA A','A.ID_AGENCIA = G.ID_AGENCIA','LEFT')
			->join('TB_CLIENTE C','C.ID_CLIENTE = G.ID_CLIENTE','LEFT')
			->select('U.ID_USUARIO,U.NOME_USUARIO, U.LOGIN_USUARIO, U.DOMINIO_USUARIO, U.STATUS_USUARIO, U.EMAIL_USUARIO')
			->select('C.DESC_CLIENTE, C.LOGO_CLIENTE, A.DESC_AGENCIA, A.LOGO_AGENCIA, G.DESC_GRUPO')
			->select('C.ID_CLIENTE, A.ID_AGENCIA, G.ID_GRUPO')
			->select('C.FEED_CLIENTE, A.FEED_AGENCIA')
			->where('U.LOGIN_USUARIO',$login)
			->get();
		return $rs->row_array();
	}
	
	/**
	 * Retorna uma lista de usuarios
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $filters Filtros a serem aplicados
	 * @param int $page Pagina atual
	 * @param int $limit Limite de registros por pagina
	 * @return array
	 */
	public function listItems($filters,$page=0,$limit=5){
		
		$this->db
			->join('TB_GRUPO G','G.ID_GRUPO = TB_USUARIO.ID_GRUPO')
			->join('TB_AGENCIA A','A.ID_AGENCIA = G.ID_AGENCIA','LEFT')
			->join('TB_CLIENTE C','C.ID_CLIENTE = G.ID_CLIENTE','LEFT')
			;
			
		if( !empty($filters['ID_AGENCIA']) ){
			$this->db->where('G.ID_AGENCIA', $filters['ID_AGENCIA']);
		}
		
		if( !empty($filters['ID_GRUPO']) ){
			$this->db->where('G.ID_GRUPO', $filters['ID_GRUPO']);
		}
		
		if( !empty($filters['ID_CLIENTE']) ){
			$this->db->where('G.ID_CLIENTE', $filters['ID_CLIENTE']);
		}
		
		if( isset($filters['STATUS_USUARIO']) && $filters['STATUS_USUARIO'] != '' ){
			$this->db->where('STATUS_USUARIO', $filters['STATUS_USUARIO']);
			unset($filters['STATUS_USUARIO']);
		}
		
		$this->setWhereParams($filters);
		return $this->execute($page,$limit, 'DESC_GRUPO ASC, DESC_AGENCIA ASC, DESC_CLIENTE ASC, NOME_USUARIO');
	}

	/**
	 * Checa se o usuario tem acesso a uma funcao
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idUsuario Codigo do usuario
	 * @param string $controller Nome da controller
	 * @param string $method Nome do metodo
	 * @return boolean
	 */
	public function checaPermissao($idUsuario, $controller, $method){
		// checa o permissionamento do usuario
		$total = $this->db->from('TB_USUARIO U')
			->join('TB_GRUPO G','G.ID_GRUPO = U.ID_GRUPO')
			->join('TB_PERMISSAO P','P.ID_GRUPO = G.ID_GRUPO')
			->join('TB_FUNCTION F','F.ID_FUNCTION = P.ID_FUNCTION')
			->join('TB_CONTROLLER C','C.ID_CONTROLLER = F.ID_CONTROLLER')
			->where('C.CHAVE_CONTROLLER',$controller)
			->where('F.CHAVE_FUNCTION',$method)
			->where('U.ID_USUARIO',$idUsuario)
			->get()
			->num_rows();

		return $total > 0;
	}
	
	/**
	 * Recupera as permissoes de um usuario
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idUsuario Codigo do usuario
	 * @return array
	 */
	public function getPermissoes($idUsuario){
		$res = $this->db->from('TB_USUARIO U')
			->join('TB_GRUPO G','G.ID_GRUPO = U.ID_GRUPO')
			->join('TB_PERMISSAO P','P.ID_GRUPO = G.ID_GRUPO')
			->join('TB_FUNCTION F','F.ID_FUNCTION = P.ID_FUNCTION')
			->join('TB_CONTROLLER C','C.ID_CONTROLLER = F.ID_CONTROLLER')
			->where('U.ID_USUARIO',$idUsuario)
			->get();
			
		$list = array();
		
		foreach($res->result_array() as $item){
			$list[$item['CHAVE_CONTROLLER']][$item['CHAVE_FUNCTION']] = 1;
		}
		
		return $list;
	}
	
	/**
	 * Recupera os usuarios pelo ID do cliente
	 * 
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @param int $idCliente id do cliente
	 * @return array
	 */
	public function getByCliente($idCliente){
		$rs = $this->db->from('TB_USUARIO U')
			->join('TB_GRUPO G','G.ID_GRUPO = U.ID_GRUPO')
			->where('U.STATUS_USUARIO', 1)
			->where('G.ID_CLIENTE', $idCliente)
			->order_by('U.NOME_USUARIO')
			->get()
			->result_array();
				
		return $rs;
	}
	
	
	/**************************************************************************/
	/**************************** FUNCOES ANTIGAS *****************************/
	/************************** FEITAS PELA SAVOIR ****************************/
	/**************************************************************************/
	
	function getAllByGrupo($id_user)
	{
		$this->db->join('TB_USER_GROUP', 'TB_USER_GROUP.ID_USER=TB_USER.ID_USER', 'left');
		$this->db->where('TB_USER.ID_USER',$id_user);
		$rs=$this->db->get('TB_USER');
		if ($rs->num_rows() == 1)
			return $rs->row_array();
		else
			return false;
	}
	
	function saveUserInCategoria($usuario,$categorias)
	{		
		//deletando todos os veiculos do formato inserido;
		//$this->db->delete('TB_USER_CATEGORIA', array('ID_USER' => $usuario));
		$sql = 'DELETE FROM TB_USER_CATEGORIA WHERE ID_USER = '.$usuario; 
		$this->db->query($sql);
		//Inserindo todos os veiculos selecionados para o formato;
		foreach($categorias as $c )						
			$this->db->insert('TB_USER_CATEGORIA',array('ID_CATEGORIA' => $c,'ID_USER' => $usuario));
	}	
	function getAllDisponivel($id)
	{
		$sql = " SELECT * FROM TB_USER " .
				" WHERE ID_USER NOT IN (SELECT ID_USER FROM TB_C_U  where ID_ACAO_EMAIL = '".$id."')" .
				" ORDER BY NAME_USER ASC ";
		
		$rs = $this->db->query($sql);
			
		if ($rs->num_rows() > 0)
			return $rs->result_array();
		else
			return false;
	}

	function DeleteAllPracas($idUsuario){
		$sql = "DELETE FROM tb_usuario_praca WHERE ID_USUARIO = ".$idUsuario;
		$this->db->query($sql);
	}

	function getAllSelecionado($id)
	{		
		/*$this->db->select('DISTINCT TB_C_U.ID_USER');
		$this->db->select('TB_USER.*');
		$this->db->select('TB_C_U.*');
		$this->db->from('TB_C_U');
		$this->db->where('TB_C_U.ID_ACAO_EMAIL', $id);
		$this->db->join('TB_USER', 'TB_C_U.ID_USER = TB_USER.ID_USER');
		$this->db->orderby("LOGIN_USER", "desc"); 
		$rs = $this->db->get();*/
			
		$sql = 'SELECT DISTINCT ID_USER,(SELECT NAME_USER FROM TB_USER WHERE ID_USER = TB_C_U.ID_USER) AS NAME_USER FROM TB_C_U WHERE TB_C_U.ID_ACAO_EMAIL = '.$id.' ORDER BY NAME_USER DESC';
		$rs  = $this->db->query($sql);
		if ($rs->num_rows() > 0)
			return $rs->result_array();
		else
			return false;
			
	}

	function savePracas($data){
        $this->db->insert('tb_usuario_praca', $data);         
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;       
    }
	
	function save($data, $id=null) {
		if ($id == null) {
			$data['DT_CADASTRO_USUARIO'] = date('y-m-d H:i:s');
		}
		return parent::save($data, $id);
	}
	
}	

