<?php
/**
 * Model que representa a tabela TB_REGRA_USUARIO
 * @author Juliano Polito
 * @link http://www.247id.com.br
 */
class RegraUsuarioDB extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_REGRA_USUARIO','int','',3,1);
		$this->addField('ID_REGRA_EMAIL','int','',1,0);
		$this->addField('ID_USUARIO','int','',1,0);
		$this->addField('DT_TIMESTAMP','timestamp','',19,0);
	}
	### END

	var $tableName = 'TB_REGRA_USUARIO';
	
	/**
	 * Construtor
	 * 
	 * @author Juliano Polito
	 * @link http://www.247id.com.br
	 * @return RegraUsuarioDB
	 */
	function __construct(){
		parent::GenericModel();
		
		$this->addValidation('ID_REGRA_EMAIL','requiredNumber','Informe a regra');
		$this->addValidation('ID_USUARIO','requiredNumber','Informe usuario');		
	}
	
	/**
	 * Exclui todos os usuarios associados a uma regra de email
	 * @author juliano.polito
	 * @param $idRegra
	 * @return unknown_type
	 */
	function deleteByRegraEmail($idRegra){
		return $this->db->delete($this->tableName, array('ID_REGRA_EMAIL' => $idRegra));
	}
	
	/**
	 * Recupera todos os usuarios de uma regra de email.
	 * A consulta já faz join com todas as tabelas relacionadas a GACP
	 * @author Juliano Polito
	 * @param $idRegra Id da regra de email
	 * @return array
	 */
	function getByRegraEmail($idRegra){

		$rs = $this->db->from($this->tableName.' RU')
			->join('TB_REGRA_EMAIL RE','RE.ID_REGRA_EMAIL = RU.ID_REGRA_EMAIL')
			->join('TB_USUARIO U','U.ID_USUARIO = RU.ID_USUARIO AND U.STATUS_USUARIO = 1')
			->join('TB_GRUPO G','G.ID_GRUPO = U.ID_GRUPO')
			->join('TB_AGENCIA A','A.ID_AGENCIA = G.ID_AGENCIA AND A.STATUS_AGENCIA = 1','LEFT')
			->join('TB_CLIENTE C','C.ID_CLIENTE = G.ID_CLIENTE AND C.STATUS_CLIENTE = 1','LEFT')
			->where('RU.ID_REGRA_EMAIL',$idRegra)
			->get()
			->result_array();
		
		return $rs;
	}
	
	/**
	 * Recupera todos os usuarios de uma regra de email.
	 * A consulta já faz join com todas as tabelas relacionadas a GACP
	 * @author Juliano Polito
	 * @param $idRegra Id da regra de email
	 * @return array
	 */
	function getByRegraEmailAgenciaCliente($idRegra, $agencia, $cliente=null){

		$rs = $this->db->from($this->tableName.' RU')
			->select('RU.*,U.*,G.*,A.ID_AGENCIA, A.DESC_AGENCIA,C.ID_CLIENTE, C.DESC_CLIENTE,RE.*')
			->join('TB_REGRA_EMAIL RE','RE.ID_REGRA_EMAIL = RU.ID_REGRA_EMAIL')
			->join('TB_USUARIO U','U.ID_USUARIO = RU.ID_USUARIO AND U.STATUS_USUARIO = 1')
			->join('TB_GRUPO G','G.ID_GRUPO = U.ID_GRUPO')
			->join('TB_AGENCIA A','A.ID_AGENCIA = G.ID_AGENCIA AND A.STATUS_AGENCIA = 1','LEFT')
			->join('TB_CLIENTE C','C.ID_CLIENTE = G.ID_CLIENTE AND C.STATUS_CLIENTE = 1','LEFT')
			->where('RU.ID_REGRA_EMAIL',$idRegra)
			->where('G.ID_AGENCIA',$agencia)
			->where('G.ID_CLIENTE',null)
			->get()
			->result_array();
			
		$rs1 = $this->db->from($this->tableName.' RU')
			->select('RU.*,U.*,G.*,A.ID_AGENCIA, A.DESC_AGENCIA,C.ID_CLIENTE, C.DESC_CLIENTE,RE.*')
			->join('TB_REGRA_EMAIL RE','RE.ID_REGRA_EMAIL = RU.ID_REGRA_EMAIL')
			->join('TB_USUARIO U','U.ID_USUARIO = RU.ID_USUARIO AND U.STATUS_USUARIO = 1')
			->join('TB_GRUPO G','G.ID_GRUPO = U.ID_GRUPO')
			->join('TB_AGENCIA A','A.ID_AGENCIA = G.ID_AGENCIA AND A.STATUS_AGENCIA = 1','LEFT')
			->join('TB_CLIENTE C','C.ID_CLIENTE = G.ID_CLIENTE AND C.STATUS_CLIENTE = 1','LEFT')
			->where('RU.ID_REGRA_EMAIL',$idRegra)
			->where('G.ID_GRUPO IS NOT NULL')
			->where('G.ID_AGENCIA',null)
			->where('G.ID_CLIENTE',null)
			->get()
			->result_array();
			
		$rs2 = array();
		
		if(!empty($cliente)){	
			$rs2 = $this->db->from($this->tableName.' RU')
				->select('RU.*,U.*,G.*,A.ID_AGENCIA, A.DESC_AGENCIA,C.ID_CLIENTE, C.DESC_CLIENTE,RE.*')
				->join('TB_REGRA_EMAIL RE','RE.ID_REGRA_EMAIL = RU.ID_REGRA_EMAIL')
				->join('TB_USUARIO U','U.ID_USUARIO = RU.ID_USUARIO AND U.STATUS_USUARIO = 1')
				->join('TB_GRUPO G','G.ID_GRUPO = U.ID_GRUPO')
				->join('TB_AGENCIA A','A.ID_AGENCIA = G.ID_AGENCIA AND A.STATUS_AGENCIA = 1','LEFT')
				->join('TB_CLIENTE C','C.ID_CLIENTE = G.ID_CLIENTE AND C.STATUS_CLIENTE = 1')
				->where('RU.ID_REGRA_EMAIL',$idRegra)
				->where('G.ID_AGENCIA',null)
				->where('G.ID_CLIENTE',$cliente)
				->get()
				->result_array();
		}
		
		return array_merge($rs,$rs1,$rs2);
	}	

	/**
	 * Recupera uma lista de GACPs que estao presentes nos usuarios da regra de email
	 * @author juliano.polito
	 * @param $idRegra Id da regra de email
	 * @return array
	 */
	function getGACPByRegra($idRegra){
		
		$rs = $this->db->from($this->tableName.' RU')
			->select('U.ID_G_A_C_P')
			->distinct()
			->join('TB_USUARIO U','U.ID_USUARIO = RU.ID_USUARIO')
			->where('RU.ID_REGRA_EMAIL',$idRegra)
			->get();
			
		return $rs->result_array();
	}
	
	/**
	 * Recupera a lista de codigos de grupos pela regra de email
	 * @author Hugo Ferreira da Silva
	 * @param int $idRegra
	 * @return array
	 */
	public function getGruposByRegra($idRegra){
		$rs = $this->db->from($this->tableName.' RU')
			->select('U.ID_GRUPO')
			->distinct()
			->join('TB_USUARIO U','U.ID_USUARIO = RU.ID_USUARIO')
			->where('RU.ID_REGRA_EMAIL',$idRegra)
			->group_by('U.ID_GRUPO')
			->get();
			
		$lista = array();
		foreach($rs->result_array() as $item){
			$lista[] = $item['ID_GRUPO'];
		}
			
		return $lista;
	}
	
	/**
	 * Recupera os usuarios de um grupo / regra
	 * 
	 * Sempre retorna os usuarios do grupo.
	 * A regra e para saber se o campo "SELECIONADO" vira como 1 OU 0
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idgrupo
	 * @param int $idregra
	 * @return array
	 */
	public function getUsuariosGrupoRegra($idgrupo, $idregra=null){
		$idregra = sprintf('%d', $idregra);
		
		$rs = $this->db
			->from('TB_USUARIO U')
			->join('TB_REGRA_USUARIO RU','U.ID_USUARIO = RU.ID_USUARIO AND RU.ID_REGRA_EMAIL = '.$idregra,'LEFT')
			->join('TB_GRUPO G','G.ID_GRUPO = U.ID_GRUPO')
			->select('U.*, G.*, IF(RU.ID_REGRA_EMAIL IS NULL, 0, 1) AS SELECIONADO', false)
			->where('G.ID_GRUPO', $idgrupo)
			->where('U.STATUS_USUARIO', 1)
			->order_by('U.NOME_USUARIO')
			->get()
			->result_array();
			
		return $rs;
	}
	
}

















