<?php
class Historico_fichaDB extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_HISTORICO_FICHA','int','',1,1);
		$this->addField('ID_USUARIO','int','',2,0);
		$this->addField('ID_FICHA','int','',1,0);
		$this->addField('NEW_STATUS','int','',0,0);
		$this->addField('DT_INSERT_HISTORICO','timestamp','',19,0);
		$this->addField('COMENT_HISTORICO_FICHA','blob','',2,0);
	}
	### END

		var $tableName = 'TB_HISTORICO_FICHA';
}

