<?php
class Praca_veiculoDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_PRACA','int','',2,0);
		$this->addField('ID_VEICULO','int','',2,0);
	}
	### END

	var $tableName = 'TB_PRACA_VEICULO'; 
	
	function getSelecionadoByPraca($id_praca)
	{
		$sql = 	' SELECT * ' .
				' FROM TB_PRACA_VEICULO ' .
				' JOIN TB_VEICULO ON TB_PRACA_VEICULO.ID_VEICULO = TB_VEICULO.ID_VEICULO     ' .
				' WHERE TB_PRACA_VEICULO.ID_PRACA = ' . $id_praca .
				' AND TB_VEICULO.FLAG_ACTIVE_VEICULO = 1 '.
				' ORDER BY TB_VEICULO.DESC_VEICULO ASC';
								
		$rs = $this->db->query($sql);	
		
		if ( $rs->num_rows() > 0 )
			return $rs->result_array();
		else
			return false;
	}	
	function getAllByMidiaAndPraca($id_midia, $id_praca)
	{
		
		$sql = 	' SELECT * FROM TB_PRACA_VEICULO ' .
				' JOIN TB_VEICULO ON TB_PRACA_VEICULO.ID_VEICULO  = TB_VEICULO.ID_VEICULO ' .
				' WHERE ID_MIDIA = ' . $id_midia .
				' AND TB_VEICULO.FLAG_ACTIVE_VEICULO = 1';
								 
		if($id_praca != null )		
		$sql .= 	' AND ID_PRACA = ' . $id_praca ;
		 
		$sql .=	' ORDER BY TB_VEICULO.DESC_VEICULO ASC ';
				
		$rs = $this->db->query($sql);	
		
		if ( $rs->num_rows() > 0 )
			return $rs->result_array();
		else
			return false;				
	}
	function getAllByMidiaAndTargetAndPraca($id_target,$id_midia, $id_praca)
	{
		$sql = 	' SELECT * FROM TB_PRACA_VEICULO ' .
				' JOIN TB_VEICULO ON TB_PRACA_VEICULO.ID_VEICULO  = TB_VEICULO.ID_VEICULO ' .
				' LEFT JOIN TB_CLASSIFICACAO ON TB_CLASSIFICACAO.ID_CLASSIFICACAO  = TB_VEICULO.ID_CLASSIFICACAO ' .
				' WHERE TB_VEICULO.FLAG_ACTIVE_VEICULO = 1 ' ;
				
		if($id_midia!=null)
		$sql .= 	' AND TB_VEICULO.ID_MIDIA = ' . $id_midia ;
		
		if($id_target!=null)
		$sql .= 	' AND TB_VEICULO.ID_CLASSIFICACAO = ' . $id_target ;
								 
		if($id_praca != null )		
		$sql .= 	' AND ID_PRACA = ' . $id_praca ;
		 
		$sql .=	' ORDER BY TB_VEICULO.DESC_VEICULO ASC ';
		
		$rs = $this->db->query($sql);	
		
		if ( $rs->num_rows() > 0 )
			return $rs->result_array();
		else
			return false;				
		
	}
	function save($veiculos,$id_praca)
	{		
		//deletando todos os veiculos do formato inserido;
		$this->db->delete('TB_PRACA_VEICULO', array('ID_PRACA' => $id_praca)); 
				
		//Inserindo todos os veiculos selecionados para o formato;
		foreach($veiculos as $v )						
			$this->db->insert('TB_PRACA_VEICULO',array('ID_VEICULO' => $v,'ID_PRACA' => $id_praca));
	} 			
}	

