<?php
class CenarioDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_CENARIO','int','',1,1);
		$this->addField('ID_CLIENTE','int','',0,0);
		$this->addField('ID_OBJETO','int','',0,0);
		$this->addField('NOME_CENARIO','string','',250,0);
		$this->addField('COD_CENARIO','string','',13,0);
		$this->addField('DT_CRIACAO_CENARIO','timestamp','',0,0);
		$this->addField('DT_INICIO_CENARIO','datetime','',0,0);
		$this->addField('DT_VALIDA_CENARIO','datetime','',0,0);
		$this->addField('OBS_CENARIO','blob','',0,0);
		$this->addField('STATUS_CENARIO','string','',1,0);
	}
	### END


	var $tableName = 'TB_CENARIO';

	function __construct(){
		parent::GenericModel();

		$this->addValidation('ID_CLIENTE','requiredNumber','Selecione um cliente');
		$this->addValidation('NOME_CENARIO','requiredString','Insira o nome da cenário');
		$this->addValidation('DT_INICIO_CENARIO','function', array($this, 'validateDataInicio'));
		$this->addValidation('DT_VALIDA_CENARIO','function', array($this, 'validateDataValidade'));
		$this->addValidation('STATUS_CENARIO','requiredNumber','Selecione o status');

	}
	
	/**
	 * valida a data de inicio do cenário
	 *
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @param array $data dados vindos do formulario
	 * @return mixed
	 */
	public function validateDataInicio($data){
		// se informou a data de inicio
		if( !empty($data['DT_INICIO_CENARIO']) ){

			$hoje = date('Ymd');
			$inicio = format_date($data['DT_INICIO_CENARIO'], 'Ymd');
			
			// se tambem informou a data de validade
			if( !empty($data['DT_VALIDA_CENARIO']) ){

				$validade = format_date($data['DT_VALIDA_CENARIO'], 'Ymd');

				// se o inicio for maior ou igual a validade
				if( $inicio >= $validade ){
					return 'A data de inicio não poder ser maior ou igual a data de validade';
				}
			}
		}

		return true;
	}
	
	/**
	 * valida a data de validade do cenario
	 *
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @param array $data dados vindos do formulario
	 * @return mixed
	 */
	public function validateDataValidade($data){
		// se informou a data de inicio
		if( !empty($data['DT_VALIDA_CENARIO']) ){

			$hoje = date('Ymd');
			$validade = format_date($data['DT_VALIDA_CENARIO'], 'Ymd');

			// verifica se a data de validade e menor ou igual a data atual
			if( $validade < $hoje ){
				return 'A data de validade deve ser maior ou igual a data atual';
			}

		}

		return true;
	}

	/**
	 * override para se nao informar as datas necessarias, grava como NULL
	 * @see system/application/libraries/GenericModel::save()
	 */
	public function save($data, $id = null){

		if( array_key_exists('DT_INICIO_CENARIO', $data) ){
			if( empty($data['DT_INICIO_CENARIO']) ){
				$data['DT_INICIO_CENARIO'] = null;
			} else {
				$data['DT_INICIO_CENARIO'] = format_date($data['DT_INICIO_CENARIO']);
			}
		}

		if( array_key_exists('DT_VALIDA_CENARIO', $data) ){
			if( empty($data['DT_VALIDA_CENARIO']) ){
				$data['DT_VALIDA_CENARIO'] = null;
			} else {
				$data['DT_VALIDA_CENARIO'] = format_date($data['DT_VALIDA_CENARIO']);
			}
		}

		return parent::save($data, $id);
	}

	/**
	 * Recupera as Fichas Cenário para exibir na pauta
	 * 
	 * @param array $data array com os itens de busca
	 * @param int $pagina pagina de inicio
	 * @param int $limit limite de resultado por pagina
	 * @param string $order elemento que sera ordenado por ele
	 * @param int $orderDirection ordenacao em ACS ou DESC
	 * @param string $order 
	**/
	public function listItems($data, $pagina=0, $limit = 5, $order = '', $orderDirection = 'ASC'){
		// limite para a paginacao
		if( empty($limit) ){
			$limit = 5;
		}

		$this->db->select('TB_CENARIO.ID_CENARIO, TB_CENARIO.NOME_CENARIO, TB_CENARIO.COD_CENARIO');
		$this->db->select('TB_CENARIO.DT_CRIACAO_CENARIO, TB_CENARIO.STATUS_CENARIO');
		$this->db->distinct();
		
		// Filtro por Cliente
		if( (isset($data['ID_CLIENTE'])) && (is_numeric($data['ID_CLIENTE'])) ){
			$this->db->where('TB_CENARIO.ID_CLIENTE', $data['ID_CLIENTE']);
		}
		
		// Filtro pelo codigo do Cenário
		if( (isset($data['COD_CENARIO'])) && ($data['COD_CENARIO'] != '') ){
			$this->db->where('TB_CENARIO.COD_CENARIO', $data['COD_CENARIO']);
		}
		
		// Pesquisa as fichas relacionadas ao cenario
		if( (isset($data['CODIGO_BARRAS_FICHA'])) && (is_array($data['CODIGO_BARRAS_FICHA'])) && (count($data['CODIGO_BARRAS_FICHA']) > 0) ){
			if( implode(',', $data['CODIGO_BARRAS_FICHA']) != '' ){
				$this->db->join('TB_FICHA_CENARIO', 'TB_CENARIO.ID_CENARIO = TB_FICHA_CENARIO.ID_CENARIO');
				$this->db->join('TB_FICHA', 'TB_FICHA_CENARIO.ID_FICHA = TB_FICHA.ID_FICHA');
				$this->db->where_in('TB_FICHA.COD_BARRAS_FICHA', $data['CODIGO_BARRAS_FICHA']);
			}
		} 
		
		// Pesquisa pela data de criacao
		if( (isset($data['DT_CRIACAO_CENARIO'])) && (($data['DT_CRIACAO_CENARIO'] != '')) ){
			$this->db->where( "TB_CENARIO.DT_CRIACAO_CENARIO between '" .format_date_to_db($data['DT_CRIACAO_CENARIO'], 2). "' and '" .format_date_to_db($data['DT_CRIACAO_CENARIO'], 3). "'" );
		}
		
		// Pesquisa pelo status
		if( (isset($data['STATUS_CENARIO'])) && (($data['STATUS_CENARIO'] != '')) ){
			$this->db->where( "TB_CENARIO.STATUS_CENARIO", $data['STATUS_CENARIO']);
		}

		// Join com a Tabela de clientes
		$this->db->join('TB_CLIENTE', 'TB_CENARIO.ID_CLIENTE = TB_CLIENTE.ID_CLIENTE');
		
		$res = $this->execute($pagina, $limit, $order, $orderDirection);

		return $res;
	}

	/**
	 * Recupera as fichas relacionadas a um cenario pelo ID do cnario
	 * 
	 * @param int $id do cenario
	**/
	function getById($id){
		$rs = $this->db
			//->join('TB_OBJETO O', 'C.ID_OBJETO = O.ID_OBJETO', 'LEFT')
			->join('TB_CLIENTE CL', 'C.ID_CLIENTE = CL.ID_CLIENTE')
			->where('C.ID_CENARIO', $id)
			->get($this->tableName.' C')
			->row_array();

		return $rs;

	}
		
	/**
	 * Recupera os dados da ficha cenario pelo ID
	 * 
	 * @param int $id do cenario
	**/
	function getFichasByIdCenario($id){
		$rs = $this->db
			->join('TB_FICHA', 'TB_FICHA.ID_FICHA = TB_FICHA_CENARIO.ID_FICHA')
			->join('TB_CATEGORIA', 'TB_CATEGORIA.ID_CATEGORIA = TB_FICHA.ID_CATEGORIA')
			->join('TB_SUBCATEGORIA', 'TB_SUBCATEGORIA.ID_SUBCATEGORIA = TB_FICHA.ID_SUBCATEGORIA')
			->join('TB_CLIENTE', 'TB_CLIENTE.ID_CLIENTE = TB_FICHA.ID_CLIENTE')
			->join('TB_FABRICANTE', 'TB_FABRICANTE.ID_FABRICANTE = TB_FICHA.ID_FABRICANTE','LEFT')
			->join('TB_MARCA', 'TB_MARCA.ID_MARCA = TB_FICHA.ID_MARCA','LEFT')
			->join('TB_APROVACAO_FICHA', "TB_FICHA.ID_APROVACAO_FICHA = TB_APROVACAO_FICHA.ID_APROVACAO_FICHA AND TB_APROVACAO_FICHA.CHAVE = 'aprovado'")
			->join('TB_OBJETO_FICHA', 'TB_OBJETO_FICHA.ID_FICHA = TB_FICHA.ID_FICHA', 'LEFT')
			->join('TB_OBJETO', 'TB_OBJETO.ID_OBJETO = TB_OBJETO_FICHA.ID_OBJETO', 'LEFT')
			->where('TB_FICHA_CENARIO.ID_CENARIO', $id)
			->where('TB_FICHA.FLAG_ACTIVE_FICHA', 1)
			->where('TB_OBJETO_FICHA.PRINCIPAL', 1)
			->get('TB_FICHA_CENARIO')
			->result_array();
		return $rs;
	}
	
	/**
	 * Recupera os dados da ficha cenario pelo ID ficha
	 * 
	 * @param int $id do cenario
	**/
	function getCenariosByIdFicha($id){
		$rs = $this->db
			->distinct()
			->join('TB_CENARIO', 'TB_CENARIO.ID_CENARIO = TB_FICHA_CENARIO.ID_CENARIO')
			->where('TB_FICHA_CENARIO.ID_FICHA', $id)
			->where('TB_CENARIO.STATUS_CENARIO', 1)
			->get('TB_FICHA_CENARIO')
			->result_array();
		return $rs;
	}
	
	public function adicionarObjeto( $idFichaCombo, $idObjeto ){
		$rs = $this->db
			->where('ID_FICHA', $idFichaCombo)
			->where('ID_OBJETO', $idObjeto)
			->get('TB_OBJETO_FICHA')
			->result_array();

		if( empty($rs) ){
			$data['ID_FICHA'] = $idFichaCombo;
			$data['ID_OBJETO'] = $idObjeto;
			$data['PRINCIPAL'] = 0;

			$this->objeto_ficha->save($data);
		}
	}
	
	/**
	 * Gera um codigo para o cenario
	 * 
	 * @param int $idCliente pagina de inicio
	**/
	public function geraCodigoCenario($idCliente){	
		$rs = $this->db
			->where('ID_CLIENTE', $idCliente)
			->order_by('ID_CENARIO', 'DESC')
			->limit('1')
			->get($this->tableName)
			->result_array();

		if( is_array($rs) && (count($rs) > 0) ){
			$ultimoCodigo = $rs['0']['COD_CENARIO'];
			
			if(strlen($ultimoCodigo) == 13){
				$ultimoCodigo = str_replace('CN', '', $ultimoCodigo);
				$novoCodigo = floor($ultimoCodigo) + 1;
				
				while(strlen($novoCodigo) < 11){
					$novoCodigo = '0' . $novoCodigo;
				}
				$novoCodigo = 'CN' . $novoCodigo;
			}
			else{
				$novoCodigo = 'CN00000000001';
			}
		}
		else{
			$novoCodigo = 'CN00000000001';
		}
		
		return $novoCodigo;
	}
	
	/**
	 * Atualiza as fichas que compoem o cenario
	 * 
	 * @param int $idCenario ID da ficha cenario
	 * * @param array $idFichas array de fichas a serem inseridas no cenario
	**/
	public function atulizaFichas($idCenario = null, $fichas = null){
		if($idCenario != null){
			$this->db->where('ID_CENARIO', $idCenario)->delete('TB_FICHA_CENARIO');
			if( (is_array($fichas)) && (count($fichas) > 0) ){
				foreach($fichas as $f){
					$this->db->insert('TB_FICHA_CENARIO', array('ID_CENARIO' => $idCenario, 'ID_FICHA' => $f)); 
				}
			}
		}
	}
	
	/**
	 * Pega uma cor para o cenario, pelo id do mesmo
	 * 
	 * @param int $idCenario ID da ficha cenario
	**/
	public function pegarCor( $idCenario ){
		$colors = array( 0xFFFFFF
						,0xFFFFCC
						,0xFFFF99
						,0xFFFF66
						,0xFFFF33
						,0xFFFF00
						,0xFFCCFF
						,0xFFCCCC
						,0xFFCC99
						,0xFFCC66
						,0xFFCC33
						,0xFFCC00
						,0xFF99FF
						,0xFF99CC
						,0xFF9999
						,0xFF9966
						,0xFF9933
						,0xFF9900
						,0xFF66FF
						,0xFF66CC
						,0xFF6699
						,0xFF6666
						,0xFF6633
						,0xFF6600
						,0xFF33FF
						,0xFF33CC
						,0xFF3399
						,0xFF3366
						,0xFF3333
						,0xFF3300
						,0xFF00FF
						,0xFF00CC
						,0xFF0099
						,0xFF0066
						,0xFF0033
						,0xFF0000
						,0xCCFFFF
						,0xCCFFCC
						,0xCCFF99
						,0xCCFF66
						,0xCCFF33
						,0xCCFF00
						,0xCCCCFF
						,0xCCCCCC
						,0xCCCC99
						,0xCCCC66
						,0xCCCC33
						,0xCCCC00
						,0xCC99FF
						,0xCC99CC
						,0xCC9999
						,0xCC9966
						,0xCC9933
						,0xCC9900
						,0xCC66FF
						,0xCC66CC
						,0xCC6699
						,0xCC6666
						,0xCC6633
						,0xCC6600
						,0xCC33FF
						,0xCC33CC
						,0xCC3399
						,0xCC3366
						,0xCC3333
						,0xCC3300
						,0xCC00FF
						,0xCC00CC
						,0xCC0099
						,0xCC0066
						,0xCC0033
						,0xCC0000
						,0x99FFFF
						,0x99FFCC
						,0x99FF99
						,0x99FF66
						,0x99FF33
						,0x99FF00
						,0x99CCFF
						,0x99CCCC
						,0x99CC99
						,0x99CC66
						,0x99CC33
						,0x99CC00
						,0x9999FF
						,0x9999CC
						,0x999999
						,0x999966
						,0x999933
						,0x999900
						,0x9966FF
						,0x9966CC
						,0x996699
						,0x996666
						,0x996633
						,0x996600
						,0x9933FF
						,0x9933CC
						,0x993399
						,0x993366
						,0x993333
						,0x993300
						,0x9900FF
						,0x9900CC
						,0x990099
						,0x990066
						,0x990033
						,0x990000
						,0x66FFFF
						,0x66FFCC
						,0x66FF99
						,0x66FF66
						,0x66FF33
						,0x66FF00
						,0x66CCFF
						,0x66CCCC
						,0x66CC99
						,0x66CC66
						,0x66CC33
						,0x66CC00
						,0x6699FF
						,0x6699CC
						,0x669999
						,0x669966
						,0x669933
						,0x669900
						,0x6666FF
						,0x6666CC
						,0x666699
						,0x666666
						,0x666633
						,0x666600
						,0x6633FF
						,0x6633CC
						,0x663399
						,0x663366
						,0x663333
						,0x663300
						,0x6600FF
						,0x6600CC
						,0x660099
						,0x660066
						,0x660033
						,0x660000
						,0x33FFFF
						,0x33FFCC
						,0x33FF99
						,0x33FF66
						,0x33FF33
						,0x33FF00
						,0x33CCFF
						,0x33CCCC
						,0x33CC99
						,0x33CC66
						,0x33CC33
						,0x33CC00
						,0x3399FF
						,0x3399CC
						,0x339999
						,0x339966
						,0x339933
						,0x339900
						,0x3366FF
						,0x3366CC
						,0x336699
						,0x336666
						,0x336633
						,0x336600
						,0x3333FF
						,0x3333CC
						,0x333399
						,0x333366
						,0x333333
						,0x333300
						,0x3300FF
						,0x3300CC
						,0x330099
						,0x330066
						,0x330033
						,0x330000
						,0x00FFFF
						,0x00FFCC
						,0x00FF99
						,0x00FF66
						,0x00FF33
						,0x00FF00
						,0x00CCFF
						,0x00CCCC
						,0x00CC99
						,0x00CC66
						,0x00CC33
						,0x00CC00
						,0x0099FF
						,0x0099CC
						,0x009999
						,0x009966
						,0x009933
						,0x009900
						,0x0066FF
						,0x0066CC
						,0x006699
						,0x006666
						,0x006633
						,0x006600
						,0x0033FF
						,0x0033CC
						,0x003399
						,0x003366
						,0x003333
						,0x003300
						,0x0000FF
						,0x0000CC
						,0x000099
						,0x000066
						,0x000033
						,0x000000);
		
		if( $idCenario > 0 ){
			$idxCor = $idCenario%(count($colors)-1);
			$color = dechex($colors[$idxCor]);
			$retorno = str_pad($color, 6, "0", STR_PAD_LEFT);
		}
		else{
			$retorno = 'ffffff';
		}
		
		return '#'.$retorno;
	}
	/**
	 * desassocia uma ficha dos cenarios pelo id da ficha
	 * 
	 * @param int $idCenario ID da ficha
	**/
	public function desassociaFichaCenarioByIdFicha( $idFicha ){
		$this->db->where('ID_FICHA', $idFicha)->delete('TB_FICHA_CENARIO');
		$this->db->where('ID_FICHA', $idFicha)->update('TB_EXCEL_ITEM', array('ID_CENARIO'=>'0'));
	}
	
	/**
	 * Recupera os cenarios por um excel, praca e versao
	 *
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @param int $idExcel
	 * @param int $idPraca
	 * @param int $idVersao
	 * @param boolean $somenteAtivos
	 * @return array
	 */
	public function getByExcelPracaVersao($idExcel, $idPraca, $idVersao, $somenteAtivos=false){
		$this->db->select('C.*, O.*, EI.PAGINA_ITEM')
			->join('TB_FICHA_CENARIO FC', 'C.ID_CENARIO = FC.ID_CENARIO')
			->join('TB_EXCEL_ITEM EI', 'FC.ID_CENARIO = EI.ID_CENARIO and FC.ID_FICHA = EI.ID_FICHA')
			->join('TB_OBJETO O', 'C.ID_OBJETO = O.ID_OBJETO')
			->where('EI.ID_EXCEL', $idExcel)
			->where('EI.ID_PRACA', $idPraca)
			->where('EI.ID_EXCEL_VERSAO', $idVersao)
			->where('EI.PAGINA_ITEM >', 0);
			
		if ( $somenteAtivos ) {
			$this->db->where('EI.STATUS_ITEM', 1);
		}
		
		$this->db->group_by('C.ID_CENARIO');
		
		$rs = $this->db->get($this->tableName . ' C')->result_array();
		
		return $rs;
	}
	
	/**
	 * Inativa as fichas pelo prazo de validade
	 *
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 */
	public function atualizarStatusPorValidade(){
		$this->db
				->where('(DT_VALIDA_CENARIO IS NOT NULL AND CURRENT_DATE >= DT_VALIDA_CENARIO)')
				->set('STATUS_CENARIO', 0)
				->update($this->tableName);
	}
}





