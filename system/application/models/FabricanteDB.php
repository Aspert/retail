<?php
class FabricanteDB  extends GenericModel {
	### START
	protected function _initialize(){
		$this->addField('ID_FABRICANTE','int','',1,1);
		$this->addField('DESC_FABRICANTE','string','',13,0);
		$this->addField('FLAG_ACTIVE_FABRICANTE','string','',1,0);
	}
	### END

	var $tableName = 'TB_FABRICANTE';
	function getByLike($dados, $pagina = 0, $limit) {
		if (isset($dados['DESC_FABRICANTE'])) {
			$sql = 'SELECT * FROM '.$this->tableName.' WHERE DESC_FABRICANTE LIKE "%'.$dados['DESC_FABRICANTE'].'%"';
			return $this->executeQuery($sql,$pagina,$limit);
		}
		else{
			return false;
		}
	}
	
	function getByDescricao($descricao) {		
		$rs = $this->db
			->from($this->tableName)
			->where('DESC_FABRICANTE', $descricao)
			->where('FLAG_ACTIVE_FABRICANTE', 1)
			->get()
			->result_array();

		return $rs;				
	}

	/**
	 * Lista as categorias cadastradas
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $filters
	 * @param int $page Pagina atual
	 * @param int $limit numero de itens por pagina
	 * @return array
	 */
	public function listItems($filters, $page=0, $limit=5){
		$this->setWhereParams($filters);
		return $this->execute($page,$limit,'DESC_FABRICANTE');
	}
	
	/**
	 * Recupera as marcas pelo codigo do fabricante
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do fabricante
	 * @return array
	 */
	public function getMarcas($id){
		$this->marca->getByFabricante($id);
	}

}

