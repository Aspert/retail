<?php
class TipoPecaPracaDB  extends GenericModel {
	### START
	protected function _initialize(){
		$this->addField('ID_TIPO_PECA','int','',1,1);
		$this->addField('ID_PRACA','int','',1,1);
	}
	### END

	var $tableName = 'TB_TIPO_PECA_PRACA';

	/**
	 * Construtor 
	 * 
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @return TipoPecaPracaDB
	 */
	function __construct(){
		parent::GenericModel();
	}
	
	/**
	 * Lista os itens pelo tipo de peca e praca
	 * @author Esdras Eduardo
	 * @link http://www.247id.com.br
	 * @param integer $idTipoPeca id do tipo da peca
	 * @param integer $idPraca id da praca
	 * @return array
	 */
	public function getByTipoPecaPraca($idTipoPeca, $idPraca){
		$rs = $this->db->where('ID_TIPO_PECA', $idTipoPeca)
				->where('ID_PRACA', $idPraca)
				->get($this->tableName)
				->result_array();
				
		return $rs;
	}
	
	/**
	 * Lista os itens pelo tipo de peca e praca
	 * @author Esdras Eduardo
	 * @link http://www.247id.com.br
	 * @param integer $idTipoPeca id do tipo da peca
	 * @return array
	 */
	public function getByTipoPeca($idTipoPeca){
		$rs = $this->db->where('ID_TIPO_PECA', $idTipoPeca)
				->get($this->tableName)
				->result_array();
				
		return $rs;
	}
	
	public function deleteAll( Array $params ){
		$this->db->delete($this->tableName, $params);
	}
}

