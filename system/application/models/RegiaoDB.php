<?php
/**
 * Model que representa a tabela TB_REGIAO
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class RegiaoDB extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_REGIAO','int','',1,1);
		$this->addField('ID_CLIENTE','int','',2,0);
		$this->addField('NOME_REGIAO','string','',15,0);
	}
	### END

	var $tableName = 'TB_REGIAO';
	
	/**
	 * Construtor
	 * 
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @return CategoriaDB
	 */
	function __construct(){
		parent::GenericModel();
		$this->addValidation('ID_CLIENTE','requiredNumber','Informe o cliente');
		$this->addValidation('NOME_REGIAO','requiredString','Informe o nome');
		$this->addValidation('NOME_REGIAO','function',array($this, 'checaNomeDuplicado'));
	}
		
	/**
	 * Lista as regioes cadastradas
	 * 
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @param array $filters
	 * @param int $page Pagina atual
	 * @param int $limit numero de itens por pagina
	 * @return array
	 */
	public function listItems($filters, $page=0, $limit=5){
		$this->db->join('TB_CLIENTE C','C.ID_CLIENTE = TB_REGIAO.ID_CLIENTE');
		$this->setWhereParams($filters);
		return $this->execute($page,$limit,'NOME_REGIAO');
	}
	
	/**
	 * metodo de validacao para checar se um nome de regiao ja esta ou nao registrado para um cliente
	 * @author Hugo Ferreira da Silva
	 * @param array $data Dados do registro
	 * @return mixed True se nao tiver, string com a mensagem em caso de erro
	 */
	public function checaNomeDuplicado($data){
		$total = $this->db
			->where('NOME_REGIAO', $data['NOME_REGIAO'])
			->where('ID_CLIENTE', $data['ID_CLIENTE'])
			->where('ID_REGIAO != ', $data['ID_REGIAO'])
			->from($this->tableName)
			->count_all_results();
			
		if( $total > 0 ){
			return 'Já existe uma região com este nome para este cliente';
		}
		
		return true;
	}
	
	/**
	 * Recupera as regioes de um cliente
	 * @author Hugo Ferreira da Silva
	 * @param int $idcliente
	 * @return array
	 */
	public function getByCliente($idcliente){
		$rs = $this->db
			->where('ID_CLIENTE', $idcliente)
			->order_by('NOME_REGIAO')
			->get($this->tableName)
			->result_array();
			
		return $rs;
	}
	
	/**
	 * Recupera as regioes de um produto
	 * @author Hugo Ferreira da Silva
	 * @param int $idregiao
	 * @return array
	 */
	public function getByProduto($idproduto){
		$rs = $this->db
			->join('TB_REGIAO_PRODUTO RP','RP.ID_REGIAO = R.ID_REGIAO')
			->where('RP.ID_PRODUTO', $idproduto)
			->order_by('R.NOME_REGIAO')
			->get($this->tableName . ' R')
			->result_array();
			
		return $rs;
	}
	
	/**
	 * Recupera o nome da regiao pelo id
	 * @author Nelson Martucci
	 * @param int $idRegiao
	 * @return string nome da regiao
	 */
	public function getNomeById($idRegiao=0){
		
		$rs = $this->db
			->where('ID_REGIAO', $idRegiao)
			->get($this->tableName)
			->result_array();
			
		// Se encontrou, retorna primeira posicao
		if ( !empty($rs) && is_array($rs) ) {
			return $rs[0]['NOME_REGIAO'];
		} else {
			return '';
		}
			
	}
	
}
