<?php
class Campo_fichaDB extends GenericModel {
	### START
	protected function _initialize(){
		$this->addField('ID_CAMPO_FICHA','int','',1,1);
		$this->addField('ID_FICHA','int','',2,0);
		$this->addField('LABEL_CAMPO_FICHA','string','',15,0);
		$this->addField('VALOR_CAMPO_FICHA','blob','',14,0);
		$this->addField('PERTENCE_SUBFICHA','int','',2,0);
		$this->addField('ID_CAMPO','int','',2,0);
		$this->addField('PRINCIPAL','int','',2,0);
	}
	### END

	var $tableName = 'TB_CAMPO_FICHA'; 
	
	// retorna um campo por ficha+field
	function getCampoByField($field=null,$id_ficha){
		$sql = 'select ID_CAMPO_FICHA, ID_FICHA, LABEL_CAMPO_FICHA, VALOR_CAMPO_FICHA from TB_CAMPO_FICHA where ID_FICHA = '.$id_ficha .' AND LABEL_CAMPO_FICHA = "'.$field.'"';
		$rs = $this->db->query($sql);
		
		if($rs->num_rows()){
			return $rs->row_array();
		}
		
		return false;
	}
	// seleciona todos os registro na tabela tb_campo_ficha
	function getAllCampoFicha($id_ficha=null){
		$sql = 'select * from TB_CAMPO_FICHA where ID_FICHA = '.$id_ficha;
		$rs = $this->db->query($sql);

		if($rs->num_rows()){
			return $rs->result_array();
		}
		
		return false;
	}
	
	// insere registros na tabela tb_campo_ficha
	function saveCampoFicha($campo, $valor, $subficha, $id, $id_campo){
		//$sql = 'select ID_CAMPO_FICHA from TB_CAMPO_FICHA where ID_FICHA = '.$id.' and LABEL_CAMPO_FICHA = "'.$campo.'"';
		//$rs = $this->db->query($sql);

		//if($rs->num_rows() == 0){
			$insert = 'insert into TB_CAMPO_FICHA (ID_FICHA, LABEL_CAMPO_FICHA, VALOR_CAMPO_FICHA, ID_CAMPO, PERTENCE_SUBFICHA) values ('.$id.', "'.$campo.'", "'.mysql_escape_string($valor).'", "'.$id_campo.'", '.$subficha.')';
			$this->db->query($insert);
		//}
	}
	
	// insere registros na tabela tb_campo_ficha
	function saveCampoSubficha($id, $campo, $valor, $id_campo, $principal){
		$insert = 'insert into TB_CAMPO_FICHA (ID_FICHA, LABEL_CAMPO_FICHA, VALOR_CAMPO_FICHA, ID_CAMPO, PRINCIPAL) values ('.$id.', "'.$campo.'", "'.mysql_escape_string($valor).'", "'.$id_campo.'", '.$principal.')';
		$this->db->query($insert);
	}
	
	// deleta um registo na tabela tb_campo_ficha caso o id � v�lido
	function deleteCampoFicha($id_campo_ficha){
		$sql = 'delete from TB_CAMPO_FICHA where ID_CAMPO_FICHA = '.$id_campo_ficha;
		$this->db->query($sql);
	}
	
	// seleciona o label da tabela tb_elem_ficha
	function getLabel($id) {
		$sql = "select LABEL_CAMPO_FICHA from TB_CAMPO_FICHA where ID_CAMPO_FICHA = $id";
		$rs = $this->db->query($sql);
		
		if($rs->num_rows()){
			$arr =  $rs->result_array();
			return  $arr[0]['LABEL_CAMPO_FICHA'];
		}
		
		return false;
	}
	
	/**
	 * Recupera os campos de uma ficha 
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idFicha
	 * @return array
	 */
	public function getByFicha($idFicha, $pertenceSubficha=null){
		$this->db->where('ID_FICHA', $idFicha);
		if($pertenceSubficha === 1 || $pertenceSubficha === 0){
			$this->db->where('PERTENCE_SUBFICHA', $pertenceSubficha);
		}
		$rs = $this->db->get($this->tableName)->result_array();
		
		return $rs;
	}
	
	function getByFichaCampo($idFicha = null, $idCampo = null){
		$rs = $this->db
			->where('ID_FICHA', $idFicha)
			->where('ID_CAMPO', $idCampo)
			->get($this->tableName)
			->result_array();
			
		return $rs;
	}
	
	function updateCampoFicha($id_campo, $nome){
		$sql = "UPDATE TB_CAMPO_FICHA SET LABEL_CAMPO_FICHA = '".$nome."' where ID_CAMPO = '".$id_campo."'";
		
		
		$this->db->query($sql);
	}
	
	function updateIdCampoFicha(){
		$sql = "UPDATE TB_CAMPO_FICHA CF
					INNER JOIN TB_FICHA F ON F.ID_FICHA = CF.ID_FICHA
					INNER JOIN TB_CAMPO C ON C.ID_CLIENTE = F.ID_CLIENTE
					SET CF.ID_CAMPO = C.ID_CAMPO
					WHERE C.NOME_CAMPO = CF.LABEL_CAMPO_FICHA";
	
		$this->db->query($sql);
	}
	
	// Lista todos os campos da ficha principal que podem ser ralacionados com suas subfichas
	public function getAllCamposFichaRelacionadoSubficha($idFichaPrincipal, $idSubficha){
		$rs = $this->db
			->select($this->tableName . '.*, TB_SUBFICHA.ID_FICHA2')
			->join('TB_CAMPO', 'TB_CAMPO.ID_CAMPO = ' .$this->tableName. '.ID_CAMPO')
			->join('TB_CAMPO_RELACIONADO', $this->tableName . '.ID_CAMPO_FICHA = TB_CAMPO_RELACIONADO.ID_CAMPO_FICHA', 'LEFT')
			->join('TB_SUBFICHA', 'TB_CAMPO_RELACIONADO.ID_SUBFICHA = TB_SUBFICHA.ID_SUBFICHA AND ID_FICHA2 = ' .$idSubficha, 'LEFT')
			->where('PERTENCE_SUBFICHA', 1)
			->where('ID_FICHA', $idFichaPrincipal)
			->where('TB_CAMPO.STATUS_CAMPO', 1)
			->get($this->tableName)
			->result_array();

		return $rs;
	}
	
	// deleta um registo na tabela tb_campo_ficha caso o id � v�lido
	function deleteByIdFicha($idFicha){
		$this->db->delete($this->tableName, array('ID_FICHA' => $idFicha));
	}
}