<?php

class RelatorioDB extends GenericModel {

	/**
	 * Construtor
	 *
	 * @author Hugo Ferreira da Silva
	 * @return RelatorioDB
	 */
	public function __construct(){
		parent::GenericModel();
	}

	/**
	 * Lista todos os relatorios que o usuario tem acesso
	 * @author Hugo Ferreira da Silva
	 * @param int $idusuario
	 * @return array
	 */
	public function getRelatoriosByUsuario($idusuario){
		$rs = $this->db
			->join('TB_GRUPO G','U.ID_GRUPO = G.ID_GRUPO')
			->join('TB_PERMISSAO P','P.ID_GRUPO = G.ID_GRUPO')
			->join('TB_FUNCTION F','F.ID_FUNCTION = P.ID_FUNCTION')
			->join('TB_CONTROLLER C','C.ID_CONTROLLER = F.ID_CONTROLLER')

			->select('F.*')

			->where('U.ID_USUARIO', $idusuario)
			->where('C.CHAVE_CONTROLLER','relatorios')
			->where('F.CHAVE_FUNCTION !=','index')

			->order_by('F.DESC_FUNCTION')
			->get('TB_USUARIO U')
			->result_array();

		return $rs;

	}

	/**
	 * Recupera os dados do relatorio de jobs
	 *
	 * @author Hugo Ferreira da Silva
	 * @param array $filters
	 * @return array
	 */
	public function relatorioJobs(array $filters){
		$etapa = $this->etapa->getIdByKey('ENVIADO_AGENCIA');
		
		$this->db->select('*, J.DATA_INICIO AS DATA_INICIO, J.DATA_TERMINO AS DATA_TERMINO, F.NOME_FICHA AS NOME_FICHA, I.PAGINA_ITEM AS PAGINA_ITEM, I.ORDEM_ITEM AS ORDEM_ITEM')
			->join('TB_ETAPA ET','J.ID_ETAPA = ET.ID_ETAPA')
			->join('TB_CAMPANHA CA','CA.ID_CAMPANHA = J.ID_CAMPANHA')
			->join('TB_PRODUTO P','CA.ID_PRODUTO = P.ID_PRODUTO')
			->join('TB_CLIENTE C','C.ID_CLIENTE = P.ID_CLIENTE')
			->join('TB_AGENCIA A','A.ID_AGENCIA = J.ID_AGENCIA')
			->join('TB_EXCEL E','E.ID_JOB = J.ID_JOB')
			->join('TB_EXCEL_ITEM I','I.ID_EXCEL = E.ID_EXCEL')
			->join('TB_PRICING PR','PR.ID_EXCEL_ITEM = I.ID_EXCEL_ITEM')
			->join('TB_FICHA F','I.ID_FICHA = F.ID_FICHA')
			->join('TB_PRACA PRA','I.ID_PRACA = PRA.ID_PRACA')
			->join('TB_FABRICANTE FA','FA.ID_FABRICANTE = F.FABRICANTE_FICHA','LEFT')
			->join('TB_MARCA M','F.MARCA_FICHA = M.ID_MARCA','LEFT')
			->join('TB_CATEGORIA CAT','CAT.ID_CATEGORIA = F.ID_CATEGORIA')
			->join('TB_SUBCATEGORIA S','S.ID_SUBCATEGORIA = F.ID_SUBCATEGORIA')

			->where('I.STATUS_ITEM = 1 AND I.ID_EXCEL_VERSAO = (SELECT MAX(ID_EXCEL_VERSAO) FROM TB_EXCEL_VERSAO WHERE ID_EXCEL = E.ID_EXCEL)', NULL, FALSE)
			->where('J.ID_ETAPA', $etapa)
			;

		/////////////// INICIO CHECAGEM DATAS /////////////////////
		if( !empty($filters['DATA_INICIO']) && !empty($filters['DATA_TERMINO'])){
			$where = sprintf('((
				("%1$s" BETWEEN J.DATA_INICIO AND J.DATA_TERMINO) OR
				("%2$s" BETWEEN J.DATA_INICIO AND J.DATA_TERMINO)) OR (
				(J.DATA_INICIO BETWEEN "%1$s" AND "%2$s") OR
				(J.DATA_TERMINO BETWEEN "%1$s" AND "%2$s")))',
					format_date($filters['DATA_INICIO']),
					format_date($filters['DATA_TERMINO'])
				);

			$this->db->where($where);

		} else if(!empty($filters['DATA_INICIO'])) {
			$this->db->where('J.DATA_INICIO', format_date($filters['DATA_INICIO']));

		} else if(!empty($filters['J.DATA_TERMINO'])) {
			$this->db->where('J.DATA_TERMINO', format_date($filters['DATA_TERMINO']));

		} else {
			$this->db->where('J.DATA_INICIO BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -30 DAY) AND CURRENT_DATE');
		}
		/////////////// FIM CHECAGEM DATAS /////////////////////

		if(!empty($filters['CLIENTES'])){
			$this->db->where_in('C.ID_CLIENTE', $filters['CLIENTES']);
		}

		if(!empty($filters['PRODUTOS'])){
			$this->db->where_in('P.ID_PRODUTO', $filters['PRODUTOS']);
		}

		if(!empty($filters['AGENCIAS'])){
			$this->db->where_in('A.ID_AGENCIA', $filters['AGENCIAS']);
		}

		if(!empty($filters['ID_AGENCIA'])){
			$this->db->where('A.ID_AGENCIA', $filters['ID_AGENCIA']);
		}

		if(!empty($filters['ID_CLIENTE'])){
			$this->db->where('C.ID_CLIENTE', $filters['ID_CLIENTE']);
		}

		if(!empty($filters['ID_PRODUTO'])){
			$this->db->where('P.ID_PRODUTO', $filters['ID_PRODUTO']);
		}

		if(!empty($filters['ID_CAMPANHA'])){
			$this->db->where('CA.ID_CAMPANHA', $filters['ID_CAMPANHA']);
		}

		if(!empty($filters['TITULO_JOB'])){
			$this->db->like('J.TITULO_JOB', $filters['TITULO_JOB']);
		}

		if(!empty($filters['ID_JOB'])){
			$this->db->where('J.ID_JOB', $filters['ID_JOB']);
		}
		

		$rs = $this->db->order_by('A.DESC_AGENCIA, C.DESC_CLIENTE, P.DESC_PRODUTO, CA.DESC_CAMPANHA, J.DATA_INICIO , J.DATA_TERMINO , PRA.ID_PRACA, I.PAGINA_ITEM ASC, I.ORDEM_ITEM ASC')
			->get('TB_JOB J')
			//->order_by('I.PAGINA_ITEM ASC, I.ORDEM_ITEM ASC')
			->result_array();
		
		$rs = $this->prepararDadosJob($rs);

		return $rs;
	}

	/**
	 * Recupera os dados do relatorio de fluxo de jobs
	 *
	 * <p>Baseado principalmente no fluxo das etapas do job</p>
	 *
	 * @author Hugo Ferreira da Silva
	 * @param array $filters
	 * @return array
	 */
	public function relatorioFluxo(array $filters){
		$etapa = $this->etapa->getIdByKey('ENVIADO_AGENCIA');
		$this->db
			->select('CA.DESC_CAMPANHA, P.DESC_PRODUTO, C.DESC_CLIENTE, A.DESC_AGENCIA, J.TITULO_JOB, J.ID_JOB')
			->select('U.NOME_USUARIO, E.DESC_ETAPA, S.DESC_STATUS, EH.DESC_ETAPA AS ETAPA_ENVIADA')
			->select('H.DATA_HISTORICO, J.DATA_INICIO, J.DATA_TERMINO')
			->select('EX.DT_CADASTRO_EXCEL')
			->join('TB_CAMPANHA CA','CA.ID_CAMPANHA = J.ID_CAMPANHA')
			->join('TB_PRODUTO P','CA.ID_PRODUTO = P.ID_PRODUTO')
			->join('TB_CLIENTE C','C.ID_CLIENTE = P.ID_CLIENTE')
			->join('TB_AGENCIA A','A.ID_AGENCIA = J.ID_AGENCIA')
			->join('TB_ETAPA E','E.ID_ETAPA = J.ID_ETAPA')
			->join('TB_STATUS S','S.ID_STATUS = J.ID_STATUS')
			->join('TB_JOB_HISTORICO H','H.ID_JOB = J.ID_JOB')
			->join('TB_ETAPA EH','EH.ID_ETAPA = H.ID_ETAPA')
			->join('TB_USUARIO U','U.ID_USUARIO = H.ID_USUARIO')
			->join('TB_EXCEL EX','J.ID_JOB = EX.ID_JOB')
			//->where('J.ID_ETAPA', $etapa)
			;

		if(!empty($filters['CLIENTES'])){
			$this->db->where_in('C.ID_CLIENTE', $filters['CLIENTES']);
		}

		if(!empty($filters['AGENCIAS'])){
			$this->db->where_in('A.ID_AGENCIA', $filters['AGENCIAS']);
		}

		if(!empty($filters['PRODUTOS'])){
			$this->db->where_in('P.ID_PRODUTO', $filters['PRODUTOS']);
		}


		/////////////// INICIO CHECAGEM DATAS /////////////////////
		if( !empty($filters['DATA_INICIO']) && !empty($filters['DATA_TERMINO'])){
			$where = sprintf('((
				("%1$s" BETWEEN J.DATA_INICIO AND J.DATA_TERMINO) OR
				("%2$s" BETWEEN J.DATA_INICIO AND J.DATA_TERMINO)) OR (
				(J.DATA_INICIO BETWEEN "%1$s" AND "%2$s") OR
				(J.DATA_TERMINO BETWEEN "%1$s" AND "%2$s")))',
					format_date($filters['DATA_INICIO']),
					format_date($filters['DATA_TERMINO'])
				);

			$this->db->where($where);

		} else if(!empty($filters['DATA_INICIO'])) {
			$this->db->where('J.DATA_INICIO', format_date($filters['DATA_INICIO']));

		} else if(!empty($filters['J.DATA_TERMINO'])) {
			$this->db->where('J.DATA_TERMINO', format_date($filters['DATA_TERMINO']));

		} else {
			$this->db->where('J.DATA_INICIO BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -30 DAY) AND CURRENT_DATE');
		}
		/////////////// FIM CHECAGEM DATAS /////////////////////

		if(!empty($filters['ID_AGENCIA'])){
			$this->db->where('A.ID_AGENCIA', $filters['ID_AGENCIA']);
		}

		if(!empty($filters['ID_CLIENTE'])){
			$this->db->where('C.ID_CLIENTE', $filters['ID_CLIENTE']);
		}

		if(!empty($filters['ID_PRODUTO'])){
			$this->db->where('P.ID_PRODUTO', $filters['ID_PRODUTO']);
		}

		if(!empty($filters['ID_CAMPANHA'])){
			$this->db->where('CA.ID_CAMPANHA', $filters['ID_CAMPANHA']);
		}

		if(!empty($filters['TITULO_JOB'])){
			$this->db->like('J.TITULO_JOB', $filters['TITULO_JOB']);
		}

		if(!empty($filters['ID_JOB'])){
			$this->db->where('J.ID_JOB', $filters['ID_JOB']);
		}

		$rs = $this->db->order_by('A.DESC_AGENCIA, C.DESC_CLIENTE, P.DESC_PRODUTO, CA.DESC_CAMPANHA, J.DATA_INICIO, J.DATA_TERMINO, H.ID_JOB_HISTORICO')
			->get('TB_JOB J')
			->result_array();

		return $this->prepararDadosFluxo($rs);
	}

	/**
	 * Recupera os dados do relatorio de jobs/precos
	 *
	 * @author Juliano Polito
	 * @param array $filters
	 * @return array
	 */
	public function relatorioPricing(array $filters){
		$etapa = $this->etapa->getIdByKey('ENVIADO_AGENCIA');
		$this->db
			->join('TB_CAMPANHA CA','CA.ID_CAMPANHA = J.ID_CAMPANHA')
			->join('TB_PRODUTO P','CA.ID_PRODUTO = P.ID_PRODUTO')
			->join('TB_CLIENTE C','C.ID_CLIENTE = P.ID_CLIENTE')
			->join('TB_AGENCIA A','A.ID_AGENCIA = J.ID_AGENCIA')
			->join('TB_EXCEL E','E.ID_JOB = J.ID_JOB')
			->join('TB_EXCEL_ITEM I','I.ID_EXCEL = E.ID_EXCEL')
			->join('TB_PRACA PRA','PRA.ID_PRACA = I.ID_PRACA')
			->join('TB_PRICING PR','I.ID_EXCEL_ITEM = PR.ID_EXCEL_ITEM')
			->join('TB_FICHA F','I.ID_FICHA = F.ID_FICHA')
			->join('TB_FABRICANTE FA','FA.ID_FABRICANTE = F.FABRICANTE_FICHA','LEFT')
			->join('TB_MARCA M','F.MARCA_FICHA = M.ID_MARCA','LEFT')
			->join('TB_CATEGORIA CAT','CAT.ID_CATEGORIA = F.ID_CATEGORIA')
			->join('TB_SUBCATEGORIA S','S.ID_SUBCATEGORIA = F.ID_SUBCATEGORIA')

			->where('I.STATUS_ITEM = 1 AND I.ID_EXCEL_VERSAO = (SELECT MAX(ID_EXCEL_VERSAO) FROM TB_EXCEL_VERSAO WHERE ID_EXCEL = E.ID_EXCEL)', NULL, FALSE)
			->where('J.ID_ETAPA', $etapa);
			//->where('EPRA.ID_EXCEL_VERSAO = (SELECT MAX(ID_EXCEL_VERSAO) FROM TB_EXCEL_VERSAO WHERE ID_EXCEL = E.ID_EXCEL)', NULL, FALSE);


		if(!empty($filters['CLIENTES'])){
			$this->db->where_in('C.ID_CLIENTE', $filters['CLIENTES']);
		}

		if(!empty($filters['AGENCIAS'])){
			$this->db->where_in('A.ID_AGENCIA', $filters['AGENCIAS']);
		}

		if(!empty($filters['PRODUTOS'])){
			$this->db->where_in('P.ID_PRODUTO', $filters['PRODUTOS']);
		}

		/////////////// INICIO CHECAGEM DATAS /////////////////////
		if( !empty($filters['DATA_INICIO']) && !empty($filters['DATA_TERMINO'])){
			$where = sprintf('((
				("%1$s" BETWEEN J.DATA_INICIO AND J.DATA_TERMINO) OR
				("%2$s" BETWEEN J.DATA_INICIO AND J.DATA_TERMINO)) OR (
				(J.DATA_INICIO BETWEEN "%1$s" AND "%2$s") OR
				(J.DATA_TERMINO BETWEEN "%1$s" AND "%2$s")))',
					format_date($filters['DATA_INICIO']),
					format_date($filters['DATA_TERMINO'])
				);

			$this->db->where($where);

		} else if(!empty($filters['DATA_INICIO'])) {
			$this->db->where('J.DATA_INICIO', format_date($filters['DATA_INICIO']));

		} else if(!empty($filters['J.DATA_TERMINO'])) {
			$this->db->where('J.DATA_TERMINO', format_date($filters['DATA_TERMINO']));

		} else {
			$this->db->where('J.DATA_INICIO BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -30 DAY) AND CURRENT_DATE');
		}
		/////////////// FIM CHECAGEM DATAS /////////////////////

		if(!empty($filters['ID_AGENCIA'])){
			$this->db->where('A.ID_AGENCIA', $filters['ID_AGENCIA']);
		}

		if(!empty($filters['ID_CLIENTE'])){
			$this->db->where('C.ID_CLIENTE', $filters['ID_CLIENTE']);
		}

		if(!empty($filters['ID_PRODUTO'])){
			$this->db->where('P.ID_PRODUTO', $filters['ID_PRODUTO']);
		}

		if(!empty($filters['ID_CAMPANHA'])){
			$this->db->where('CA.ID_CAMPANHA', $filters['ID_CAMPANHA']);
		}

		if(!empty($filters['ID_PRACA'])){
			$this->db->where('PRA.ID_PRACA', $filters['ID_PRACA']);
		}

		if(!empty($filters['TITULO_JOB'])){
			$this->db->like('J.TITULO_JOB', $filters['TITULO_JOB']);
		}

		if(!empty($filters['ID_JOB'])){
			$this->db->where('J.ID_JOB', $filters['ID_JOB']);
		}

		$rs = $this->db->order_by('A.DESC_AGENCIA, C.DESC_CLIENTE, P.DESC_PRODUTO, CA.DESC_CAMPANHA, J.ID_JOB, PRA.ID_PRACA, J.DATA_INICIO, J.DATA_TERMINO')
			->get('TB_JOB J')
			->result_array();

		return $rs;
	}

	/**
	 * Recupera as informacoes de divergencias entre o espelho e o job
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $filters filtros a serem aplicados no relatorio
	 * @return array retorna os dados do relatorio
	 */
	public function divergenciasEspelhoJob(array $filters){
		
		// se nao informou a data de inicio
		if( empty($filters['DATA_INICIO']) ){
			$filters['DATA_INICIO'] = date('Y-m-d', strtotime('-30 days'));
		}

		// se nao informou a data de termino
		if( empty($filters['DATA_TERMINO']) ){
			$filters['DATA_TERMINO'] = date('Y-m-d');
		}

		// formatando as datas
		$dataInicio = format_date($filters['DATA_INICIO'], 'Y-m-d');
		$dataTermino = format_date($filters['DATA_TERMINO'], 'Y-m-d');

		// formata as datas e coloca na condicao
		$this->db
			->where('J.DATA_INICIO_PROCESSO BETWEEN "' . $dataInicio . '" AND "' . $dataTermino . '"');

		// se informou a agencia
		if( !empty($filters['ID_AGENCIA']) ){
			$this->db->where('J.ID_AGENCIA',$filters['ID_AGENCIA']);
		}

		// se informou o cliente
		if( !empty($filters['ID_CLIENTE']) ){
			$this->db->where('J.ID_CLIENTE',$filters['ID_CLIENTE']);
		}

		// se informou o titulo do job
		if( !empty($filters['TITULO_JOB']) ){
			$this->db->like('J.TITULO_JOB',$filters['TITULO_JOB']);
		}
		
		// se informou o id do job
		if( !empty($filters['ID_JOB']) ){
			$this->db->where('J.ID_JOB',$filters['ID_JOB']);
		}

		// se informou a campanha
		if( !empty($filters['ID_CAMPANHA']) ){
			$this->db->where('J.ID_CAMPANHA',$filters['ID_CAMPANHA']);
		}

		// se informou a bandeira
		if( !empty($filters['ID_PRODUTO']) ){
			$this->db->where('CA.ID_PRODUTO',$filters['ID_PRODUTO']);
		}

		// se informou a praca
		if( !empty($filters['ID_PRACA']) ){
			$this->db->where('P.ID_PRACA',$filters['ID_PRACA']);
		}

		// faz a consulta
		$rows = $this->db
			->select('J.TITULO_JOB, J.ID_JOB, J.DATA_INICIO_PROCESSO, J.DATA_INICIO, J.DATA_TERMINO')
			->select('B.DESC_PRODUTO, CA.DESC_CAMPANHA, C.DESC_CLIENTE, A.DESC_AGENCIA, P.DESC_PRACA, P.ID_PRACA')
			->select('CAT.DESC_CATEGORIA, SC.DESC_SUBCATEGORIA')
			->select('EC.QTDE_EXCEL_CATEGORIA AS QUANTIDADE')
			->select('EC.PAGINA_EXCEL_CATEGORIA AS PAGINA')
			->select('TPL.* AS LIMITE')
			->select('COUNT(DISTINCT EI.ID_EXCEL_ITEM) AS QTDE_INFORMADA')

			->from('TB_JOB J')
			->join('TB_EXCEL E','J.ID_JOB = E.ID_JOB')
			->join('TB_EXCEL_PRACA EP','EP.ID_EXCEL = E.ID_EXCEL')
			->join('TB_EXCEL_CATEGORIA EC','EC.ID_EXCEL = E.ID_EXCEL AND EC.ID_PRACA = EP.ID_PRACA AND EC.QTDE_EXCEL_CATEGORIA > 0')

			// somente itens da ultima versao
			// que estao ativos
			// que estao na mesma pagina da categoria
			// que estao nas mesmas subcategorias do job
			// retirando as fichas filhas
			->join('TB_EXCEL_ITEM EI'
				,'EI.ID_EXCEL = E.ID_EXCEL
					AND EI.SUBCATEGORIA = EC.ID_SUBCATEGORIA
					AND EI.STATUS_ITEM = 1
					AND EI.PAGINA_ITEM = EC.PAGINA_EXCEL_CATEGORIA
					AND EI.ID_EXCEL_VERSAO = (SELECT MAX(ID_EXCEL_VERSAO) FROM TB_EXCEL_VERSAO WHERE ID_EXCEL = E.ID_EXCEL
					AND (EI.ID_FICHA_PAI = 0 OR EI.ID_FICHA_PAI IS NULL))'
				,'LEFT'
			)

			->join('TB_SUBCATEGORIA SC','EC.ID_SUBCATEGORIA = SC.ID_SUBCATEGORIA')
			->join('TB_CATEGORIA CAT','CAT.ID_CATEGORIA = SC.ID_CATEGORIA')

			->join('TB_TIPO_PECA TP','TP.ID_TIPO_PECA = J.ID_TIPO_PECA')
			->join('TB_TIPO_PECA_LIMITE TPL','TP.ID_TIPO_PECA = TPL.ID_TIPO_PECA AND TPL.ID_SUBCATEGORIA = EC.ID_SUBCATEGORIA', 'LEFT')

			->join('TB_PRACA P','EP.ID_PRACA = P.ID_PRACA')
			->join('TB_CLIENTE C','C.ID_CLIENTE = J.ID_CLIENTE')
			->join('TB_AGENCIA A','A.ID_AGENCIA = J.ID_AGENCIA')
			->join('TB_CAMPANHA CA','CA.ID_CAMPANHA = J.ID_CAMPANHA')
			->join('TB_PRODUTO B','B.ID_PRODUTO = CA.ID_PRODUTO')

			// SEMPRE PEGANDO PELA ULTIMA VERSAO DO EXCEL
			->where('EC.ID_EXCEL_VERSAO = (SELECT MAX(ID_EXCEL_VERSAO) FROM TB_EXCEL_VERSAO WHERE ID_EXCEL = E.ID_EXCEL)')
			->where('EP.ID_EXCEL_VERSAO = (SELECT MAX(ID_EXCEL_VERSAO) FROM TB_EXCEL_VERSAO WHERE ID_EXCEL = E.ID_EXCEL)')

			// agrupamento
			->group_by('J.ID_JOB, EC.ID_SUBCATEGORIA, EC.PAGINA_EXCEL_CATEGORIA, EC.ID_PRACA')

			->order_by('A.DESC_AGENCIA, C.DESC_CLIENTE, B.DESC_PRODUTO, CA.DESC_CAMPANHA, J.TITULO_JOB, P.DESC_PRACA, EC.PAGINA_EXCEL_CATEGORIA, EC.ID_SUBCATEGORIA')

			// faz a consulta
			->get()
			->result_array()
			;
			
		// retorno da funcao
		$retorno = array();
		
		foreach($rows as $row){
			// vamos agrupar por job
			if( empty($retorno[$row['ID_JOB']]) ){
				$retorno[$row['ID_JOB']] = $row;
			}

			// vamos agrupar por praca
			if( empty($retorno[$row['ID_JOB']]['pracas'][$row['ID_PRACA']]) ){
				$retorno[$row['ID_JOB']]['pracas'][$row['ID_PRACA']] = $row;
			}

			// colocando item
			$retorno[$row['ID_JOB']]['pracas'][$row['ID_PRACA']]['itens'][] = $row;
		}

		return $retorno;
	}

	/**
	 * Recupera as informações do status dos usuários
	 *
	 * @author Vanessa G. de Carvalho
	 * @link http://www.247id.com.br
	 * @param array $filters filtros a serem aplicados no relatorio
	 * @return array retorna os dados do relatorio
	 */

	public function relatorioRelacaoUsuarios(array $filters){

		// se nao informou a data de inicio
		if( empty($filters['DATA_INICIO']) ){
			$filters['DATA_INICIO'] = date('Y-m-d', strtotime('-30 days'));
		}
		// se nao informou a data de termino
		if( empty($filters['DATA_TERMINO']) ){
			$filters['DATA_TERMINO'] = date('Y-m-d');
		}
		// formatando as datas
		$dataInicio = format_date($filters['DATA_INICIO'], 'Y-m-d 23:59');
		$dataTermino = format_date($filters['DATA_TERMINO'], 'Y-m-d 23:59');

		// formata as datas e coloca na condicao
		$this->db->where('U.DT_CADASTRO_USUARIO BETWEEN "' . $dataInicio . '" AND "' . $dataTermino . '"', null, false);

		// se informou a agencia
		if( !empty($filters['ID_AGENCIA']) ){
			$this->db->where('A.ID_AGENCIA',$filters['ID_AGENCIA']);
		}
		// se informou o cliente
		if( !empty($filters['ID_CLIENTE']) ){
			$this->db->where('C.ID_CLIENTE',$filters['ID_CLIENTE']);
		}
		// se informou o status
		//echo('|'.$filters['STATUS_USUARIO'].'|');
		if($filters['STATUS_USUARIO'] == '0' || $filters['STATUS_USUARIO'] == '1' ) {
			$this->db->where('U.STATUS_USUARIO', $filters['STATUS_USUARIO']);
		}

	// faz a consulta
		$this->db->select('A.DESC_AGENCIA, C.DESC_CLIENTE, G.DESC_GRUPO, U.NOME_USUARIO, U.STATUS_USUARIO'); 
		$this->db->select("DATE_FORMAT(DT_CADASTRO_USUARIO,'%d/%m/%Y %H:%i')  As DT_CADASTRO_USUARIO", false);
		$this->db->join('TB_GRUPO G', 'U.ID_GRUPO = G.ID_GRUPO');
		$this->db->join('`TB_AGENCIA` A', 'A.ID_AGENCIA = G.ID_AGENCIA AND A.STATUS_AGENCIA = 1', 'left');
		$this->db->join('`TB_CLIENTE` C', 'C.ID_CLIENTE = G.ID_CLIENTE AND C.STATUS_CLIENTE = 1', 'left');
			
		$rs = $this->db->order_by('U.NOME_USUARIO')
		->get('TB_USUARIO U')
		->result_array();
		$rs = $this->prepararDadosRelacaoUsuario($rs);

		return $rs;
	}

	/**
	 * Prepara os dados do relatorio de comparacao de pracas
	 *
	 * Este metodo retorna um array multimensional contendo
	 * os elementos (fichas) no seguinte formato, onde
	 * cada elemento e um array contendo os valores
	 * de um ExcelItem + Ficha + Categoria + Subcategoria.
	 *
	 * Caso existam elementos na mesma pagina e ordem, sempre
	 * sera considerado o ultimo elemento encontrado para
	 * aquela posicao. Esta situacao pode ocorrer se o job
	 * enviado nao passou pela paginacao e foi importado
	 * por um excel sem ter a paginacao preenchida.
	 *
	 * <code>
	 * $res = $ci->relatorio->comparacaoPracas( 3 );
	 *
	 * // formato do resultado
	 * // $res[ PAGINA ][ ORDEM ][ CODIGO_PRACA ] = $item;
	 * </code>
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idjob Codigo do job que terao as pracas comparadas
	 * @return array dados do relatorio
	 */
	public function comparativoPracas( $idjob ){
		// pracas que estao no job
		$pracas = $this->job->getPracas( $idjob );

		// resultado
		$resultado = array();

		// maior pagina
		$maiorPagina = 0;

		// armazena a maior ordem por pagina
		$maioresOrdens = array();

		// codigos das pracas
		$chavesPracas = getValoresChave($pracas, 'ID_PRACA');

		// versao do excel
		$versaoList = getValoresChave($pracas, 'ID_EXCEL_VERSAO');
		$versao = reset($versaoList);

		// codigos do excel
		$excelList = getValoresChave($pracas, 'ID_EXCEL');
		$excel = reset($excelList);

		// vamos executar duas consultas.
		// na primeira, vamos pegar o maior numero de
		// de ordens por pagina.
		// na segunda, iremos recuperar propriamente
		// os itens do job
		$paginas = $this->db
			->from( 'TB_EXCEL_ITEM I' )
			->select('I.PAGINA_ITEM, MAX(I.ORDEM_ITEM) AS ORDEM_ITEM')
			->join('TB_FICHA F','F.ID_FICHA = I.ID_FICHA')
			->join('TB_CATEGORIA C','F.ID_CATEGORIA = C.ID_CATEGORIA')
			->join('TB_SUBCATEGORIA SC','F.ID_SUBCATEGORIA = SC.ID_SUBCATEGORIA')

			->where_in('I.ID_PRACA', $chavesPracas)
			->where('I.ID_EXCEL_VERSAO', $versao)
			->where('I.ID_EXCEL', $excel)
			->where('I.STATUS_ITEM', 1)
			->where('I.PAGINA_ITEM > 0 AND I.ORDEM_ITEM > 0')
			->where('I.PAGINA_ITEM IS NOT NULL AND I.ORDEM_ITEM IS NOT NULL')

			->order_by('I.PAGINA_ITEM')
			->group_by('I.PAGINA_ITEM')
			->get()
			->result_array();

		// recuperando os maiores valores
		foreach($paginas as $item){
			$pagina = max($item['PAGINA_ITEM'], 1);
			$maiorPagina = max($pagina, $maiorPagina);

			// se a ordem for zero, ja ajusta para um
			$ordem = max($item['ORDEM_ITEM'], 1);
			$maioresOrdens[ $pagina ] = $ordem;
		}

		// pegamos os itens das pracas
		$itens = $this->db
			->from( 'TB_EXCEL_ITEM I' )
			->select('I.PAGINA_ITEM, I.ORDEM_ITEM, F.NOME_FICHA, F.COD_BARRAS_FICHA')
			->select('C.DESC_CATEGORIA, SC.DESC_SUBCATEGORIA, I.ID_PRACA')

			->join('TB_FICHA F','F.ID_FICHA = I.ID_FICHA')
			->join('TB_CATEGORIA C','F.ID_CATEGORIA = C.ID_CATEGORIA')
			->join('TB_SUBCATEGORIA SC','F.ID_SUBCATEGORIA = SC.ID_SUBCATEGORIA')

			->where_in('I.ID_PRACA', $chavesPracas)
			->where('I.ID_EXCEL_VERSAO', $versao)
			->where('I.ID_EXCEL', $excel)
			->where('I.STATUS_ITEM', 1)
			->where('I.PAGINA_ITEM > 0 AND I.ORDEM_ITEM > 0')
			->where('I.PAGINA_ITEM IS NOT NULL AND I.ORDEM_ITEM IS NOT NULL')

			->order_by('I.PAGINA_ITEM, I.ORDEM_ITEM')
			->get()
			->result_array();

		// preparando o resultado final: para cada pagina
		for($pg=1; $pg<=$maiorPagina; $pg++){

			// se existe uma maior ordem para a pagina
			// (pode ficar em branco se a pessoa nao fizer direito no excel)
			if( !empty($maioresOrdens[ $pg ]) ){

				// para a maior ordem dentro da pagina
				for($ordem=1; $ordem <= $maioresOrdens[ $pg ]; $ordem++){

					// para cada praca
					foreach( $pracas as $praca ){

						// marcamos o item de resultado
						$resultado[ $pg ][ $ordem ][ $praca['ID_PRACA'] ] = null;

					}
				}
			}
		}

		// agora que ja temos as paginas e ordens
		// equalizadas para todas as pracas
		// vamos preencher os placeholders com os
		// valores dos elementos encontrados na query
		foreach($itens as $item) {
			$pagina = max($item['PAGINA_ITEM'], 1);
			$ordem = max($item['ORDEM_ITEM'], 1);

			$resultado[ $pagina ][ $ordem ][ $item['ID_PRACA'] ] = $item;
		}

		// removendo o array de itens para
		// liberar memoria
		unset($itens);

		// retornando o resultado
		return $resultado;
	}

	/**
	 * Agrupa os dados para enviar para a controller e depois para a view
	 *
	 * @author Hugo Ferreira da Silva
	 * @param array $data
	 * @return array
	 */
	protected function prepararDadosFluxo(array $data){
		$result = array();
		foreach($data as $item){
			$group = $item['DESC_AGENCIA'].' / '.$item['DESC_CLIENTE'] . ' / '.$item['DESC_PRODUTO']
				. ' / ' . $item['DESC_CAMPANHA'] . ' <br> Job: ' . $item['ID_JOB'] . ' - ' . $item['TITULO_JOB']
				. ' - Etapa: ' . $item['DESC_ETAPA'] . ' - Aberto em: ' . format_date_to_form($item['DT_CADASTRO_EXCEL'])
				. '- Data de Inicio: ' .format_date($item['DATA_INICIO'],'d/m/Y') . ' - Data de Termino: ' . format_date($item['DATA_TERMINO'],'d/m/Y');

			$result[$group]['ID_JOB'] = $item['ID_JOB'];
			$result[$group]['TITULO_JOB'] = $item['TITULO_JOB'];
			$result[$group]['DESC_AGENCIA'] = $item['DESC_AGENCIA'];
			$result[$group]['DESC_CLIENTE'] = $item['DESC_CLIENTE'];
			$result[$group]['DESC_PRODUTO'] = $item['DESC_PRODUTO'];
			$result[$group]['DESC_CAMPANHA'] = $item['DESC_CAMPANHA'];
			$result[$group]['ITENS'][] = $item;
		}

		return $result;
	}

	/**
	 * Agrupa os dados para enviar para a controller e depois para a view
	 *
	 * @author Hugo Ferreira da Silva
	 * @param array $data
	 * @return array
	 */
	protected function prepararDadosJob(array $data){
		$result = array();
		foreach($data as $item){
			$group = $item['DESC_AGENCIA'].' / '.$item['DESC_CLIENTE'] . ' / '.$item['DESC_PRODUTO']
				. ' / ' . $item['DESC_CAMPANHA'] . ' <br> Job: ' . $item['ID_JOB'] . ' - ' . $item['TITULO_JOB']
				. ' - Etapa: ' . $item['DESC_ETAPA'] . ' - Aberto em: ' . format_date_to_form($item['DT_CADASTRO_EXCEL'])
				. '- Data de Inicio: ' .format_date($item['DATA_INICIO'],'d/m/Y') . ' - Data de Termino: ' . format_date($item['DATA_TERMINO'],'d/m/Y');		
			$result[$group]['ID_JOB'] = $item['ID_JOB'];
			$result[$group]['TITULO_JOB'] = $item['TITULO_JOB'];
			$result[$group]['DESC_AGENCIA'] = $item['DESC_AGENCIA'];
			$result[$group]['DESC_CLIENTE'] = $item['DESC_CLIENTE'];
			$result[$group]['DESC_PRODUTO'] = $item['DESC_PRODUTO'];
			$result[$group]['DESC_CAMPANHA'] = $item['DESC_CAMPANHA'];
			
			$result[$group][$item['DESC_PRACA']]['ITENS'][] = $item;		
		}

		return $result;
	}
	
	/**
	 * Agrupa os dados para enviar para a controller e depois para a view
	 *
	 * @author Vanessa G. de Carvalho
	 * @param array $data
	 * @return array
	 */
	protected function prepararDadosRelacaoUsuario(array $data){
		$result = array();
		foreach($data as $item){
			$group = $item['DESC_AGENCIA'].' / '.$item['DESC_CLIENTE'] . ' / '.$item['DESC_GRUPO'] . ' / '.$item['NOME_USUARIO']
			. ' / '.$item['STATUS_USUARIO']. ' / '.$item['DT_CADASTRO_USUARIO'];

			$result[$group]['DESC_AGENCIA'] = $item['DESC_AGENCIA'];
			$result[$group]['DESC_CLIENTE'] = $item['DESC_CLIENTE'];
			$result[$group]['DESC_GRUPO'] = $item['DESC_GRUPO'];
			$result[$group]['NOME_USUARIO'] = $item['NOME_USUARIO'];
			$result[$group]['STATUS_USUARIO'] = $item['STATUS_USUARIO'];
			$result[$group]['ITENS'][] = $item;
		}

		return $result;
	}
	
	/**
	 * Recupera os dados do relatorio de objetos pendentes
	 *
	 * @author Bruno Oliveira
	 * @param array $filters
	 * @return array
	 */
	public function relatorioObjetosPendentes(array $filters){
		
		$this->db
				->from('TB_OBJETO O')
				->select('F.ID_FICHA ,F.NOME_FICHA,F.COD_BARRAS_FICHA, O.FILE_ID, O.FILENAME, EI.PAGINA_ITEM, P.DESC_PRACA')
				->join('TB_OBJETO_FICHA OF','O.ID_OBJETO = OF.ID_OBJETO')
				->join('TB_FICHA F','F.ID_FICHA = OF.ID_FICHA')
				->join('TB_EXCEL_ITEM EI','EI.ID_FICHA = F.ID_FICHA')
				->join('TB_PRACA P','P.ID_PRACA = EI.ID_PRACA')
				->join('TB_EXCEL E','E.ID_EXCEL = EI.ID_EXCEL')
				->join('TB_JOB J','J.ID_JOB = E.ID_JOB')
				->where('EI.STATUS_ITEM','1');
		
		if(isset($filters['NOME']) || !empty($filters['NOME'])){
			$this->db->like('O.FILENAME',$filters['NOME'],'after');
		} else {
			// $this->db->where('O.FILENAME','PENDENTE.PSD');
			$this->db->like('O.FILENAME','PENDENTE');
		}
		
		if(isset($filters['ID_CLIENTE']) || !empty($filters['ID_CLIENTE'])){
			$this->db->where('J.ID_CLIENTE',$filters['ID_CLIENTE']);
		} else {
			$this->db->where('J.ID_AGENCIA',$filters['ID_AGENCIA']);
		}
		
		if(isset($filters['ID_JOB']) || !empty($filters['ID_JOB'])){
			$this->db->where('J.ID_JOB',$filters['ID_JOB']);
		}
		
		if(isset($filters['ID_PRACA']) || !empty($filters['ID_PRACA'])){
			$this->db->where('EI.ID_PRACA',$filters['ID_PRACA']);
		}
		
		$this->db->order_by('P.DESC_PRACA, EI.PAGINA_ITEM, F.ID_FICHA, O.FILENAME ASC');
		$result = $this->db->get()->result_array();
		
		return $result;
		
	}
	
	/**
	 * Recupera os dados do relatorio de comparação de produtos entre praças
	 *
	 * @author Bruno Oliveira
	 * @param array $filters
	 * @return array
	 */
	public function relatorioComparacaoProdutosPracas(array $filters){
		
		//aqui consultamos todas as praças vinculadas ao JOB
		$pracas = $this->praca->getByIdJob($filters['ID_JOB']);
		//retiramos do array a praça que foi selecionada no filtro
		foreach ($pracas as $key => $praca){
			if($praca['ID_PRACA'] == $filters['ID_PRACA']){
				$pracaFiltrada = $pracas[$key];
				unset($pracas[$key]);
			}
		}
	
		//aqui pegamos todas as praças e as quantidades totais das fichas sendo separadas por paginas
		$this->db
		->from('TB_EXCEL_ITEM EI')
		->select('ID_PRACA, PAGINA_ITEM, COUNT(*) AS QTO_TOTAL')
		->where('EI.STATUS_ITEM','1')
		->where('EI.ID_EXCEL',$filters['ID_EXCEL'])
		->where('EI.ID_PRACA != ',$pracaFiltrada['ID_PRACA'])
		->group_by('EI.ID_PRACA, EI.PAGINA_ITEM	');
		$pracasQtoTotal = $this->db->get()->result_array();
		
		$relatorio = array();
		$pracaInicio = $pracasQtoTotal[0]['ID_PRACA'];
		$totalGeral = 0;
		foreach ($pracasQtoTotal as $qtoTotal){
			$pracaFim = $qtoTotal['ID_PRACA'];
			
			if($pracaInicio == $pracaFim){
				unset($qtoTotal['ID_PRACA']);
				$totalGeral += $qtoTotal['QTO_TOTAL'];
				$relatorio[$pracaInicio]['TOTAL_PRODUTOS'] = $totalGeral;
				$relatorio[$pracaInicio]['PAGINAS'][] = $qtoTotal;
			} else {
				$totalGeral = 0;
				$pracaInicio = $pracaFim;
				$totalGeral += $qtoTotal['QTO_TOTAL'];
				$relatorio[$pracaInicio]['TOTAL_PRODUTOS'] = $totalGeral;
				unset($qtoTotal['ID_PRACA']);
				$relatorio[$pracaInicio]['PAGINAS'][] = $qtoTotal;
			}
		}
		
		
		//aqui pegamos todos os itens que se repetem por praça
		foreach ($relatorio as $idPraca => $relat){
			
			$this->db
			->from('TB_EXCEL_ITEM EI')
			->select('EI.ID_PRACA, EI.PAGINA_ITEM, COUNT(*) AS REPETIDOS')
			->where('EI.STATUS_ITEM','1')
			->where('EI.ID_EXCEL',$filters['ID_EXCEL'])
			->where('EI.ID_PRACA',$idPraca)
			->where('EI.ID_FICHA IN (SELECT ID_FICHA FROM TB_EXCEL_ITEM WHERE ID_EXCEL = '.$filters['ID_EXCEL'].' AND ID_PRACA = '.$pracaFiltrada['ID_PRACA'].' AND STATUS_ITEM = 1) ',NULL, FALSE)
			->group_by('EI.PAGINA_ITEM');
			$pracasRepitidos = $this->db->get()->result_array();
			
			$totalRepetidos = 0;
			foreach ($relat as $chaveRelatorio => $rel){
				if($chaveRelatorio == 'PAGINAS'){
					foreach ($rel as $key => $re){
						foreach ($pracasRepitidos as $repetidos){
							foreach ($pracas as $praca){
								if($praca['ID_PRACA'] == $idPraca){
									$relatorio[$idPraca]['T_DESC_PRACA'] = $praca['DESC_PRACA'];
								}
							}	
							
							if($re['PAGINA_ITEM'] == $repetidos['PAGINA_ITEM']){
								$qtoTotal = $relatorio[$idPraca]['PAGINAS'][$key]['QTO_TOTAL'];
								$totalRepetidos += $repetidos['REPETIDOS'];
								$relatorio[$idPraca]['TOTAL_REPETIDOS'] = $totalRepetidos;
								$relatorio[$idPraca]['TOTAL_PORCENTAGEM'] = calcPorcentagem($totalRepetidos,$relatorio[$idPraca]['TOTAL_PRODUTOS']);
								$relatorio[$idPraca]['PAGINAS'][$key]['REPETIDOS'] = $repetidos['REPETIDOS'];
								$relatorio[$idPraca]['PAGINAS'][$key]['PORCENTAGEM'] = calcPorcentagem($repetidos['REPETIDOS'],$qtoTotal);
								krsort($relatorio[$idPraca]);
							}
						}
					}
				}
			}
		}
		//aqui tratamos caso não exista item repetido na página
		foreach ($relatorio as $idPraca => $relat){
			foreach ($relat['PAGINAS'] as $chaveRelatorio => $rel){
				if(!isset($rel['REPETIDOS']) || $rel['REPETIDOS'] == 0){
					$relatorio[$idPraca]['PAGINAS'][$chaveRelatorio]['REPETIDOS'] = 0;
					$relatorio[$idPraca]['PAGINAS'][$chaveRelatorio]['PORCENTAGEM'] = 0;
				}				
			}
			if(!isset($relat['TOTAL_REPETIDOS'])) {
				$relatorio[$idPraca]['TOTAL_REPETIDOS'] = 0;
			}
			if(!isset($relat['TOTAL_PORCENTAGEM'])) {
				$relatorio[$idPraca]['TOTAL_PORCENTAGEM'] = 0;
			}
			
			if(!isset($relat['T_DESC_PRACA'])) {
				foreach ($pracas as $praca){
					if($praca['ID_PRACA'] == $idPraca){
						$relatorio[$idPraca]['T_DESC_PRACA'] = $praca['DESC_PRACA'];
					}
				}
			}
		}
		
		$relatorio = ordenarMaiorPorcentagemDesc($relatorio);
	
		return $relatorio;
	}
	
	public function RelatorioProdutosCampanhaJob($IDC = 0,$IDTP = 0)	{
		$Id_Campanha = $IDC;
		$Id_Tipo_Peca = $IDTP;
		
		if($Id_Tipo_Peca == 0)	{
			$this->db->select('ITEM.ORDEM_ITEM, ITEM.PAGINA_ITEM, EX.DESC_EXCEL, FIC.COD_BARRAS_FICHA,
					FIC.NOME_FICHA, ITEM.TEXTO_LEGAL, ITEM.ID_PRACA, PRI.PRECO_UNITARIO, PRI.TIPO_UNITARIO, PRI.PRECO_CAIXA,
					PRI.TIPO_CAIXA, PRI.PRECO_VISTA, PRI.PRECO_DE, PRI.PRECO_POR, PRI.ECONOMIZE, PRI.ENTRADA,
					PRI.PARCELAS, PRI.PRESTACAO, PRI.JUROS_AM, PRI.JUROS_AA, PRI.TOTAL_PRAZO, PRI.OBSERVACOES,
					ITEM.EXTRA1_ITEM, ITEM.EXTRA2_ITEM, ITEM.EXTRA3_ITEM, ITEM.EXTRA4_ITEM, ITEM.EXTRA5_ITEM, 
					CAMP.VALOR_CAMPO_FICHA, CAMP.PRINCIPAL, FIC.TIPO_FICHA, JOB.TITULO_JOB, AG.DESC_AGENCIA,
					CLI.DESC_CLIENTE, CAM.DESC_CAMPANHA, JOB.ID_JOB, PRO.DESC_PRODUTO, FIC.TIPO_FICHA, TPC.DESC_TIPO_PECA, PR.DESC_PRACA')
                    ->from('TB_EXCEL EX')
                    ->join('TB_EXCEL_ITEM ITEM','EX.ID_EXCEL = ITEM.ID_EXCEL')
                    ->join('TB_JOB JOB','EX.ID_JOB = JOB.ID_JOB')
                    ->join('TB_AGENCIA AG','JOB.ID_AGENCIA = AG.ID_AGENCIA')
                    ->join('TB_CLIENTE CLI','JOB.ID_CLIENTE = CLI.ID_CLIENTE')
                    ->join('TB_CAMPANHA CAM','CAM.ID_CAMPANHA = JOB.ID_CAMPANHA')
                    ->join('TB_PRODUTO PRO','PRO.ID_PRODUTO = CAM.ID_PRODUTO')
                    ->join('TB_PRICING PRI','PRI.ID_EXCEL_ITEM = ITEM.ID_EXCEL_ITEM')
                    ->join('TB_FICHA FIC','FIC.ID_FICHA = ITEM.ID_FICHA')
                    ->join('TB_CATEGORIA CAT','CAT.ID_CATEGORIA = FIC.ID_CATEGORIA')
                    ->join('TB_TIPO_PECA TPC','JOB.ID_TIPO_PECA = TPC.ID_TIPO_PECA')
                    ->join('TB_PRACA PR','ITEM.ID_PRACA = PR.ID_PRACA')
                    ->join('TB_CAMPO_FICHA CAMP','CAMP.ID_FICHA = FIC.ID_FICHA','left')
                    ->where('JOB.ID_CAMPANHA',$Id_Campanha)
                    ->where('JOB.ID_TIPO_PECA !=',$Id_Tipo_Peca)
                    ->where('FIC.FLAG_ACTIVE_FICHA',1)
                    ->where('ITEM.DISPONIVEL_ITEM',1)
					->order_by('PR.DESC_PRACA','ASC')
                    ->order_by('ITEM.PAGINA_ITEM','ASC')
                    ->order_by('ITEM.ORDEM_ITEM', 'ASC');
                    
		}	else	{
			$this->db->select('ITEM.ORDEM_ITEM, ITEM.PAGINA_ITEM, EX.DESC_EXCEL, FIC.COD_BARRAS_FICHA,
					FIC.NOME_FICHA, ITEM.TEXTO_LEGAL, ITEM.ID_PRACA, PRI.PRECO_UNITARIO, PRI.TIPO_UNITARIO, PRI.PRECO_CAIXA,
					PRI.TIPO_CAIXA, PRI.PRECO_VISTA, PRI.PRECO_DE, PRI.PRECO_POR, PRI.ECONOMIZE, PRI.ENTRADA,
					PRI.PARCELAS, PRI.PRESTACAO, PRI.JUROS_AM, PRI.JUROS_AA, PRI.TOTAL_PRAZO, PRI.OBSERVACOES,
					ITEM.EXTRA1_ITEM, ITEM.EXTRA2_ITEM, ITEM.EXTRA3_ITEM, ITEM.EXTRA4_ITEM, ITEM.EXTRA5_ITEM, 
					CAMP.VALOR_CAMPO_FICHA, CAMP.PRINCIPAL, FIC.TIPO_FICHA, JOB.TITULO_JOB, AG.DESC_AGENCIA,
					CLI.DESC_CLIENTE, CAM.DESC_CAMPANHA, JOB.ID_JOB, PRO.DESC_PRODUTO, FIC.TIPO_FICHA, TPC.DESC_TIPO_PECA, PR.DESC_PRACA')
                    ->from('TB_EXCEL EX')
                    ->join('TB_EXCEL_ITEM ITEM','EX.ID_EXCEL = ITEM.ID_EXCEL')
                    ->join('TB_JOB JOB','EX.ID_JOB = JOB.ID_JOB')
                    ->join('TB_AGENCIA AG','JOB.ID_AGENCIA = AG.ID_AGENCIA')
                    ->join('TB_CLIENTE CLI','JOB.ID_CLIENTE = CLI.ID_CLIENTE')
                    ->join('TB_CAMPANHA CAM','CAM.ID_CAMPANHA = JOB.ID_CAMPANHA')
                    ->join('TB_PRODUTO PRO','PRO.ID_PRODUTO = CAM.ID_PRODUTO')
                    ->join('TB_PRICING PRI','PRI.ID_EXCEL_ITEM = ITEM.ID_EXCEL_ITEM')
                    ->join('TB_FICHA FIC','FIC.ID_FICHA = ITEM.ID_FICHA')
                    ->join('TB_CATEGORIA CAT','CAT.ID_CATEGORIA = FIC.ID_CATEGORIA')
                    ->join('TB_TIPO_PECA TPC','JOB.ID_TIPO_PECA = TPC.ID_TIPO_PECA')
                    ->join('TB_PRACA PR','ITEM.ID_PRACA = PR.ID_PRACA')
                    ->join('TB_CAMPO_FICHA CAMP','CAMP.ID_FICHA = FIC.ID_FICHA','left')
                    ->where('JOB.ID_CAMPANHA',$Id_Campanha)
                    ->where('JOB.ID_TIPO_PECA',$Id_Tipo_Peca)
                    ->where('FIC.FLAG_ACTIVE_FICHA',1)
                    ->where('ITEM.DISPONIVEL_ITEM',1)
					->order_by('PR.DESC_PRACA','ASC')
                    ->order_by('ITEM.PAGINA_ITEM','ASC')
                    ->order_by('ITEM.ORDEM_ITEM', 'ASC');
                    
		}
		$retorno = $this->db->get()->result_array();
		return $retorno;
	}
	
}