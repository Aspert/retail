<?php
class PaginaDB extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_PAGINA','int','',0,1);
		$this->addField('NUMERO_PAGINA','int','',0,0);
		$this->addField('ID_PAGINACAO','int','',0,0);
		$this->addField('ID_F_C_P','int','',0,0);
		$this->addField('FLAG_DESTAQUE_PAGINA','string','',0,0);
		$this->addField('ORDEM','int','',0,0);
	}
	### END


	var $tableName = 'TB_PAGINA';
	
	function deleteByPaginacao($id_paginacao)
	{
		$sql = 'DELETE FROM TB_PAGINA WHERE ID_PAGINACAO = ' . $id_paginacao;
		
		return $this->db->query($sql);
	}
	
	public function getByPaginacao($id_paginacao)
	{
		$this->db->join('TB_F_C_P','TB_PAGINA.ID_F_C_P = TB_F_C_P.ID_F_C_P');
		$this->db->join('TB_FICHA', 'TB_FICHA.ID_FICHA = TB_F_C_P.ID_FICHA');
		$this->db->join('TB_OBJ_FICHA', '(TB_OBJ_FICHA.ID_FICHA = TB_FICHA.ID_FICHA AND TB_OBJ_FICHA.FLAG_MAIN_OBJ_FICHA = \'1\')');
		$this->db->where('ID_PAGINACAO', $id_paginacao);
		$this->db->order_by('TB_PAGINA.NUMERO_PAGINA');
		$rs = $this->db->get('TB_PAGINA');
			if ($rs->num_rows() > 0)
				return $rs->result_array();
			else
				return false;		
	}
	
	public function getByPaginacaoMidia($id_paginacao){
		$this->db->join('TB_F_C_P','TB_PAGINA.ID_F_C_P = TB_F_C_P.ID_F_C_P');
		$this->db->join('TB_PAGINACAO','TB_PAGINA.ID_PAGINACAO = TB_PAGINACAO.ID_PAGINACAO');
		$this->db->join('TB_FICHA', 'TB_FICHA.ID_FICHA = TB_F_C_P.ID_FICHA');
		$this->db->join('TB_TIPO_LAYOUT_FICHA', 'TB_TIPO_LAYOUT_FICHA.ID_FICHA = TB_F_C_P.ID_FICHA');
		$this->db->join('TB_OBJ_FICHA', '(TB_OBJ_FICHA.ID_FICHA = TB_FICHA.ID_FICHA AND TB_OBJ_FICHA.FLAG_MAIN_OBJ_FICHA = \'1\')');
		$this->db->where('TB_PAGINA.ID_PAGINACAO', $id_paginacao);
		$this->db->where('TB_TIPO_LAYOUT_FICHA.ID_MIDIA = TB_PAGINACAO.ID_MIDIA');
		$this->db->order_by('TB_PAGINA.NUMERO_PAGINA, TB_PAGINA.ORDEM');
		$rs = $this->db->get('TB_PAGINA');
			if ($rs->num_rows() > 0)
				return $rs->result_array();
			else
				return false;		
	}
	
	public function getFichasByPaginaPaginacao($id_pagina,$id_paginacao){
		$sql = <<<SQL
			SELECT EF.* ,P.*,
			       ( SELECT CONCAT(TOB.PATH_OBJ_FICHA, TOB.FILE_OBJ_FICHA)
			         FROM TB_OBJ_FICHA TOB
			         WHERE ID_OBJ_FICHA = EF.ID_OBJ_FICHA
			       ) AS PATH
			FROM
			    TB_PAGINA P,
			    TB_PAGINACAO PG,
			    TB_F_C_P FCP,
			    TB_FICHA F,
			    TB_TIPO_LAYOUT_FICHA TLF,
			    TB_ELEM_FICHA EF
			
			WHERE
			  P.ID_PAGINACAO = PG.ID_PAGINACAO
			  AND EF.ID_FICHA = TLF.ID_FICHA
			  AND PG.ID_MIDIA = TLF.ID_MIDIA
			  AND F.ID_FICHA = FCP.ID_FICHA
			  AND P.ID_F_C_P = FCP.ID_F_C_P
			  AND PG.ID_PAGINACAO = $id_paginacao
			  AND P.ID_PAGINA = $id_pagina
SQL;
		
		$rs = $this->db->query($sql);	
		
		if ( $rs->num_rows() > 0 )
			return $rs->result_array();
		else
			return false;
		
	}
	
	
}
