<?php
/**
 * Classe de modelo para o gerenciamento de clientes
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class Fornecedor_clienteDB extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_AGENCIA','int','',1,1);
		$this->addField('ID_CLIENTE','int','',1,1);
	}
	### END

	var $tableName = 'TB_FORNECEDOR_CLIENTE';
	
	/**
	 * Construtor
	 *  
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return ClienteDB
	 */
	function __construct(){
		parent::GenericModel();
	}
	
	/**
	 * Salva os clientes de uma agencia
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idagencia
	 * @param array $clientes
	 * @return void
	 */
	public function salvarClientes($idagencia, array $clientes){
		// pegmos os clientes que ja estao gravados
		$list = $this->db
			->where('ID_AGENCIA', $idagencia)
			->get($this->tableName)
			->result_array();
		
		// pegamos somente os codigos
		$codes = getValoresChave($list, 'ID_CLIENTE');
		
		// pegamos os clientes que ja existem
		foreach($codes as $code){
			// se nao estiver na lista dos selecionados
			if(!in_array($code, $agencias)){
				// removemos
				$this->db
					->where('ID_AGENCIA', $idagencia)
					->where('ID_CLIENTE', $code)
					->delete($this->tableName);
			}
		}
		
		// para cada cliente selecionado
		foreach($agencias as $code){
			// se nao estiver na lista dos que estao no banco
			if(!in_array($code, $codes)){
				// inserimos
				$data['ID_AGENCIA'] = $idagencia;
				$data['ID_CLIENTE'] = $idcliente;
				$this->db->insert($this->tableName, $data);
			}
		}
	}
	
	/**
	 * Salva os fornecedores de um cliente
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idcliente
	 * @param array $agencias
	 * @return void
	 */
	public function salvarAgencias($idcliente, array $agencias){
		// pegmos as agencias que ja estao gravadas
		$list = $this->db
			->where('ID_CLIENTE', $idcliente)
			->get($this->tableName)
			->result_array();
		
		// pegamos somente os codigos
		$codes = getValoresChave($list, 'ID_AGENCIA');
		
		// pegamos as agencias que ja existem
		foreach($codes as $code){
			// se nao estiver na lista das selecionadas
			if(!in_array($code, $agencias)){
				// removemos
				$this->db
					->where('ID_AGENCIA', $code)
					->where('ID_CLIENTE', $idcliente)
					->delete($this->tableName);
			}
		}
		
		// para cada agencia selecionada
		foreach($agencias as $code){
			// se nao estiver na lista das que estao no banco
			if(!in_array($code, $codes)){
				// inserimos
				$data['ID_AGENCIA'] = $code;
				$data['ID_CLIENTE'] = $idcliente;
				$this->db->insert($this->tableName, $data);
			}
		}
	}
	
	function updateAgenciasClientes($cliente, $agencia){
		if(is_numeric($cliente)){
			$this->db->delete($this->tableName, array("ID_CLIENTE" => $cliente));
			foreach($agencia as $idAgencia){
				$this->db->insert($this->tableName, array("ID_CLIENTE" => $cliente, "ID_AGENCIA" => $idAgencia));
			}
		}
		else if(is_numeric($agencia)){
			$this->db->delete($this->tableName, array("ID_AGENCIA" => $agencia));
			foreach($cliente as $idCliente){
				$this->db->insert($this->tableName, array("ID_CLIENTE" => $idCliente, "ID_AGENCIA" => $agencia));
			}
		}
	}
}

