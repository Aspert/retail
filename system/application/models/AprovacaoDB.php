<?php
/**
 * Classe de modelo para o gerenciamento de aprovacao de producao
 * @author Juliano Polito
 * @link http://www.247id.com.br
 */
class AprovacaoDB extends GenericModel {
	### START
	protected function _initialize(){
		$this->addField('ID_APROVACAO','int','',1,1);
		$this->addField('ID_JOB','int','',1,0);
		$this->addField('ID_USUARIO','int','',1,0);
		$this->addField('ID_STATUS','int','',1,0);
		$this->addField('ID_OLDSTATUS','int','',1,0);
		$this->addField('ENVIADO_APROVACAO','int','',1,0);
		$this->addField('OBS_APROVACAO','string','',10,0);
		$this->addField('DATA_APROVACAO','string','',10,0);
		$this->addField('DATA_SOLICITACAO','datetime','',10,0);
		$this->addField('ID_USUARIO_APROVADOR','int','',1,0);
	}
	### END

	
	var $tableName = 'TB_APROVACAO';
	
	/**
	 * Construtor
	 *  
	 * @author Juliano Polito
	 * @link http://www.247id.com.br
	 * @return CampanhaDB
	 */
	function __construct(){
		parent::GenericModel();
		// adiciona as validacoes
		//$this->addValidation('OBS_APROVACAO','requiredString','Informe o nome');
	}
	
	/**
	 * Recupera historico de aprovações por job
	 *  
	 * @author juliano.polito
	 * @param int $idJob
	 * @return array
	 */
	public function getByJob($idjob, $somenteEnviados = false){
		$this->db
				->select('*, G.ID_AGENCIA AS U_AGENCIA , G.ID_CLIENTE AS U_CLIENTE, AG.DESC_AGENCIA AS U_DESC_AGENCIA, CL.DESC_CLIENTE AS U_DESC_CLIENTE')
				->join('TB_JOB J', 'AP.ID_JOB = J.ID_JOB')
				->join('TB_EXCEL E', 'E.ID_JOB = AP.ID_JOB')
				->join('TB_STATUS S', 'S.ID_STATUS = AP.ID_STATUS')
				->join('TB_USUARIO U', 'U.ID_USUARIO = AP.ID_USUARIO')
				->join('TB_GRUPO G', 'U.ID_GRUPO = G.ID_GRUPO')
				->join('TB_AGENCIA AG', 'G.ID_AGENCIA = AG.ID_AGENCIA', 'LEFT')
				->join('TB_CLIENTE CL', 'G.ID_CLIENTE = CL.ID_CLIENTE', 'LEFT')
				->where('AP.ID_JOB', $idjob)
				->order_by('AP.ID_APROVACAO DESC');
				
			if( $somenteEnviados ){
				$this->db->where('AP.ENVIADO_APROVACAO = 1');
			}
				
		$rs = $this->db->get($this->tableName.' AP');
				
		return $rs->result_array();
	}
	
	public function saveNew($idjob, $idusuario){
		$data = array(	
						'ID_JOB' => $idjob,
						'ID_USUARIO' => $idusuario,
						'ID_STATUS' => $this->status->getIdByKey('EM_APROVACAO'),
						'ID_OLDSTATUS' => $this->status->getIdByKey('EM_APROVACAO'),
						'ENVIADO_APROVACAO' => 0,
						'OBS_APROVACAO' => '',
						'DATA_APROVACAO' => time()
					);
		return $this->save($data);
	}
	
	
	/**
	 * 
	 * Recupera o registro mais recente de aprovacao
	 * 
	 * @author juliano.polito
	 * @param int $idjob
	 * @param boolean $deagencia Se true, recupera o ultimo registro enviado pela agencia - ou seja, tem um PDF associado
	 * @return array
	 */
	public function getLastByJob($idjob, $deagencia = false, $enviado = false){
		$this->db
				->select('*, G.ID_AGENCIA AS U_AGENCIA , G.ID_CLIENTE AS U_CLIENTE')
				->join('TB_JOB J', 'AP.ID_JOB = J.ID_JOB')
				->join('TB_EXCEL E', 'E.ID_JOB = AP.ID_JOB')
				->join('TB_STATUS S', 'S.ID_STATUS = J.ID_STATUS')
				->join('TB_USUARIO U', 'U.ID_USUARIO = AP.ID_USUARIO')
				->join('TB_GRUPO G', 'U.ID_GRUPO = G.ID_GRUPO')
				->where('AP.ID_JOB', $idjob)
				->order_by('AP.ID_APROVACAO DESC');
		
		if($deagencia){
			$this->db->where('G.ID_AGENCIA IS NOT NULL');			
		}
		
		if($enviado){
			$this->db->where('AP.ENVIADO_APROVACAO = 1');			
		}else{
			$this->db->where('AP.ENVIADO_APROVACAO = 0');			
		}
		
		$rs = $this->db->get($this->tableName.' AP');
				
		return $rs->row_array();
	}
	
	/**
	 * Metodo para listar itens
	 * @author Juliano Polito
	 * @link http://www.247id.com.br
	 * @param array $data Dados para filtro
	 * @param int $pagina numero da pagina atual
	 * @param int $limit limit de itens por pagina
	 * @return array
	 */
	public function listItems($data, $pagina=0, $limit = 5){

		// se informou a agencia, pegamos os clientes da agencia
		// fazemos isso em primeiro lugar por que o codeigniter se
		// confunde com o active record.
		if( !empty($data['ID_AGENCIA']) ){
			// pega os clientes da agencia
			$list = $this->cliente->getByAgencia($data['ID_AGENCIA']);
			// lista de codigos
			$codes = array(0);
			// para cada cliente da agencia
			foreach($list as $item){
				// coloca o codigo na lista
				$codes[] = $item['ID_CLIENTE'];
			}
			// coloca a condicao
			$this->db->where_in('TB_CLIENTE.ID_CLIENTE', $codes);
		}
		
		
		if( !empty($data['PRODUTOS']) ){
			$this->db->where_in('TB_CAMPANHA.ID_PRODUTO', $data['PRODUTOS']);
		}
		
		if( !empty($data['ID_CLIENTE']) ){
			$this->db->where('TB_CLIENTE.ID_CLIENTE', $data['ID_CLIENTE']);
		}
		
		$this->db->join('TB_PRODUTO', 'TB_CAMPANHA.ID_PRODUTO = TB_PRODUTO.ID_PRODUTO')
			->join('TB_CLIENTE', 'TB_PRODUTO.ID_CLIENTE = TB_CLIENTE.ID_CLIENTE');
		
		if(is_numeric(Sessao::get('usuario'))){
			$this->db->where('TB_AGENCIA.ID_AGENCIA', $data['ID_AGENCIA']);
		}

		if(isset($data['STATUS_CAMPANHA'])){
			$this->db->where('TB_CAMPANHA.STATUS_CAMPANHA', $data['STATUS_CAMPANHA']);
		}
		
		if( !empty($data['DT_INICIO_CAMPANHA']) && !empty($data['DT_FIM_CAMPANHA']) ){
			$sql = sprintf("
				((TB_CAMPANHA.DT_INICIO_CAMPANHA BETWEEN '%1\$s' AND '%2\$s') OR
				(TB_CAMPANHA.DT_FIM_CAMPANHA BETWEEN '%1\$s' AND '%2\$s') OR
				('%1\$s' BETWEEN TB_CAMPANHA.DT_INICIO_CAMPANHA AND TB_CAMPANHA.DT_FIM_CAMPANHA) OR
				('%2\$s' BETWEEN TB_CAMPANHA.DT_INICIO_CAMPANHA AND TB_CAMPANHA.DT_FIM_CAMPANHA))",
				format_date($data['DT_INICIO_CAMPANHA']),
				format_date($data['DT_FIM_CAMPANHA'])
			);
			
			$this->db->where($sql);
			
			unset($data['DT_INICIO_CAMPANHA'], $data['DT_FIM_CAMPANHA']);
			
		} else if( !empty($data['DT_INICIO_CAMPANHA'])){
			$this->db->where('TB_CAMPANHA.DT_INICIO_CAMPANHA >= ', format_date($data['DT_INICIO_CAMPANHA']) );
			unset($data['DT_INICIO_CAMPANHA']);
			
		} else if( !empty($data['DT_FIM_CAMPANHA'])){
			$this->db->where('TB_CAMPANHA.DT_FIM_CAMPANHA <= ', format_date($data['DT_FIM_CAMPANHA']) );
			unset($data['DT_FIM_CAMPANHA']);
		}
		
			
		$this->setWhereParams($data);
		return $this->execute($pagina, $limit, 'DESC_CAMPANHA, DESC_CLIENTE, DESC_PRODUTO');
	}
	
	public function getPathPdf($idap){
		$ap = $this->getById($idap);
		$pathjob = $this->getPathJob($ap['ID_JOB']);
		
		$path = $pathjob."$idap.pdf";
		
		return $path;
	}
	
	public function getPathJob($idjob){
		$job = $this->job->getById($idjob);
		$pathCamp = $this->campanha->getPath($job['ID_CAMPANHA']);
		$pathAp = $pathCamp.$this->config->item('aprovacao_path');
		$pathJob = $pathAp.$idjob.'/';
		
		if(!is_dir($pathJob)){
			mkdir($pathJob,0777,true);
		}
	
		return $pathJob;
	}
	
	public function getArquivosAprovacao($idAprovacao, $comComentarios = true, $isArquivoCliente=0){
		$ap = $this->getById($idAprovacao);
		$arquivos = $this->db
			->join('TB_APROVACAO_COMENTARIO_ARQUIVO ACA','ACA.ID_APROVACAO_ARQUIVO = TB_APROVACAO_ARQUIVO.ID_APROVACAO_ARQUIVO')
			->where('ID_APROVACAO',$idAprovacao)
			->where('ACA.IS_ARQUIVO_CLIENTE',$isArquivoCliente)
			->get('TB_APROVACAO_ARQUIVO')
			->result_array();
		
		foreach($arquivos as &$arquivo){
			$arquivo['comentarios'] = array();
			if( $comComentarios ){
				$arquivo['comentarios'] = $this->getComentariosArquivo($arquivo['ID_APROVACAO_ARQUIVO']);
			}
		}
		
		return $arquivos;
	}
	
	public function getArquivosAprovacaoCliente($idAprovacao){
		$arquivos = $this->db
		->join('TB_APROVACAO_COMENTARIO_ARQUIVO ACA','ACA.ID_APROVACAO_ARQUIVO = TB_APROVACAO_ARQUIVO.ID_APROVACAO_ARQUIVO','LEFT')
		->where('ID_APROVACAO',$idAprovacao)
		->group_by('TB_APROVACAO_ARQUIVO.ID_APROVACAO_ARQUIVO')
		->get('TB_APROVACAO_ARQUIVO')
		->result_array();
	
		return $arquivos;
	}
	
	public function getComentariosArquivo($idArquivo){
		$comentarios = $this->db
			->select('C.*, J.TITULO_JOB, U.NOME_USUARIO, AG.DESC_AGENCIA, CG.DESC_CLIENTE, ACA.*, AR.TITULO_APROVACAO_ARQUIVO')
			->join('TB_APROVACAO_COMENTARIO_ARQUIVO ACA','ACA.ID_APROVACAO_COMENTARIO = C.ID_APROVACAO_COMENTARIO')
			->join('TB_APROVACAO_ARQUIVO AR','AR.ID_APROVACAO_ARQUIVO = ACA.ID_APROVACAO_ARQUIVO')
			->join('TB_APROVACAO AP','AP.ID_APROVACAO = AR.ID_APROVACAO')
			->join('TB_JOB J','J.ID_JOB = AP.ID_JOB')
			->join('TB_USUARIO U','U.ID_USUARIO = C.ID_USUARIO')
			->join('TB_GRUPO G','U.ID_GRUPO = G.ID_GRUPO')
			->join('TB_AGENCIA AG','AG.ID_AGENCIA = G.ID_AGENCIA','LEFT')
			->join('TB_CLIENTE CG','CG.ID_CLIENTE = G.ID_CLIENTE','LEFT')
			->where('AR.ID_APROVACAO_ARQUIVO',$idArquivo)
			->order_by('C.DATA_COMENTARIO DESC')
			->get('TB_APROVACAO_COMENTARIO C')
			->result_array();
			
		return $comentarios;
	}
	
}

