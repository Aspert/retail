<?php
/**
 * Model que representa a tabela de permissoes
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class PermissaoDB extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_PERMISSAO','int','',1,1);
		$this->addField('ID_FUNCTION','int','',1,0);
		$this->addField('ID_GRUPO','int','',1,0);
	}
	### END

	var $tableName = 'TB_PERMISSAO';
	
	/**
	 * Construtor
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return PermissaoDB
	 */
	function __construct(){
		parent::GenericModel();
		
	}
}


