<?php
/**
 * Classe de modelo para o gerenciamento de arquivos pdf para opi
 * associados aos jobs de excel
 * @author Juliano Polito
 * @link http://www.247id.com.br
 */
class ExcelopiDB extends GenericModel {
	### START
	protected function _initialize(){
		$this->addField('ID_EXCEL_OPI','int','',0,1);
		$this->addField('ID_EXCEL','int','',0,0);
		$this->addField('DATA_EXCEL_OPI','datetime','',0,0);
		$this->addField('ARQUIVO_EXCEL_OPI','string','',0,0);
		$this->addField('PROCESSADO_EXCEL_OPI','int','',0,0);
		$this->addField('FILA_OPI_DPI','string','',0,0);
		$this->addField('DATA_TERMINO_EXCEL_OPI','datetime','',0,0);
	}
	### END

	var $tableName = 'TB_EXCEL_OPI';
	
	/**
	 * Construtor
	 *  
	 * @author Juliano Polito
	 * @link http://www.247id.com.br
	 * @return CampanhaDB
	 */
	function __construct(){
		parent::GenericModel();		
	}
	
	/**
	 * Metodo para listar itens
	 * @author Juliano Polito
	 * @link http://www.247id.com.br
	 * @param array $data Dados para filtro
	 * @param int $pagina numero da pagina atual
	 * @param int $limit limit de itens por pagina
	 * @return array
	 */
	public function listItems($data, $pagina=0, $limit = 5){
		$this->setWhereParams($data);
		return $this->execute($pagina, $limit, 'DATA_EXCEL_OPI');
	}
	
	/**
	 * Registra um novo arquivo, ou atualiza caso seja o mesmo nome de um existente
	 * 
	 * @author Juliano Polito
	 * @param $id
	 * @return int Id do novo registro
	 */
	public function getNewFileID($idExcel ,$dpi ,$file){
		//$opi = $this->getByFileAndExcel($idExcel,$file);
		
//		if(!empty($opi)){
//			$opi['DATA_EXCEL_OPI'] = date('Y-m-d H:i:s');
//			$opi['FILA_OPI_DPI'] = $dpi;
//			$this->save($opi, $opi['ID_EXCEL_OPI']);
//			return $opi['ID_EXCEL_OPI'];
//		}else{
			$opi = array(
			'FILA_OPI_DPI' => $dpi,
			'DATA_EXCEL_OPI' => date('Y-m-d H:i:s'),
			'ARQUIVO_EXCEL_OPI' => $file,
			'ID_EXCEL' => $idExcel
			);
			
			return $this->save($opi);
		//}
		
	}
	
	/**
	 * Recupera um registro de opi pelo nome do arquivo e id do excel associado
	 * @author juliano.polito
	 * @param $idExcel
	 * @param $file nome do arquivo
	 * @return array
	 */
	public function getByFileAndExcel($idExcel, $file){
		$rs = $this->db->from($this->tableName)
			->where('ID_EXCEL',$idExcel)
			->where('ARQUIVO_EXCEL_OPI', $file)
			->limit(1)
			->get();
			
		return $rs->row_array();
	}
	

	/**
	 * Retorna o nome de um arquivo de opi
	 * 
	 * 
	 * @author juliano.polito
	 * @link http://www.247id.com.br
	 * @param int $idExcelOPI codigo do arquivo de opi
	 * @return string
	 */
	public function getFileName($idExcelOPI){
		
		$opi = $this->db->from($this->tableName)
			->where('ID_EXCEL_OPI',$idExcelOPI)
			->get();
		$opi = $opi->row_array();
		
		if(!empty($opi)){
			//filename para gravacao do arquivo no storage in
			
			$fileName = "%d_.pdf";
			$fileName = sprintf($fileName, $idExcelOPI);
				
			return $fileName;
		}
		
		return "";
	}
	
	/**
	 * Recupera os arquivos associados ao job
	 * 
	 * @author juliano.polito
	 * @return array
	 */
	public function getByExcel($idExcel){
		$rs = $this->db->from($this->tableName.' EOP')
			->select('*, DATE_FORMAT(EOP.DATA_EXCEL_OPI, "%d/%m/%y %H:%i:%s") AS DATA2_EXCEL_OPI,TIMEDIFF(EOP.DATA_TERMINO_EXCEL_OPI,EOP.DATA_EXCEL_OPI) AS DIF')
			->where('EOP.ID_EXCEL',$idExcel)
			->order_by('EOP.DATA_EXCEL_OPI DESC')
			->get();
			
		return $rs->result_array();
	}
	
	
}

