<?php

require_once 'r_script.php';

class XinetDB extends Model
{
	private $debug = false;
	var $rows;
	var $db_xinet;

	function __construct()
	{
		parent::Model();
	}
	
	/**
	 * Sincronia Xinet / Storage
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return string
	 */
	public function sincroniza($file,$extra = ''){
		$resposta = XinetKiwi::getInstance()->syncpreviewdir($file)->returnMessage;
		$resposta = XinetKiwi::getInstance()->katype($file,true)->returnMessage;
		$resposta = XinetKiwi::getInstance()->syncpreview($file)->returnMessage;
		return $resposta;
	}
	
	public function rebuildPreview($file, $fpo = true, $fixmime = true){
		if($fixmime){
			XinetKiwi::getInstance()->katype($file,true);
		}		
		$ret = $this->sincroniza( $file );
		
		return $ret;
	}
	
	/**
	 * Cria pastas no xinet
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $path Nome completo do diretorio a ser criado
	 * @return void
	 */
	public function createPath($path){
		//$argumentos = array(array('name' => 'caminho', 'value' => rawurlencode($path)));
		//$debug = false;
		//$scriptResult = $this->ids->call('criadiretorio',$argumentos,$debug);
		
		//cria estrutura de pasta manualmente
		$arr = explode("/", $path);
		$path = '';
		foreach($arr as $pasta) {
			$path .= $pasta.'/';
			if(!file_exists($path)){
				//cria estrutura e força o sync
				if( mkdir($path, 0777,true) ) {
					chmod($path, 0777);
					$Xinet = XinetKiwi::getInstance();
					$idsRetorno = $Xinet->syncvoltodbdir($path);
					
				}
			}
		}		
		return $idsRetorno;
	}
	
	/* Model que retorna os IDs do arquivo pelo endereço físico */
	function getFilePorPath($path, $file) {
		$this->db_xinet = $this->load->database('xinet', TRUE);
		$path = rtrim($path, '//');
		
		$path = utf8MAC($path);
		$file = str_to_xinet($file);
		$this->debug('$path '.print_r($path,true) );
		$this->debug('$file '.print_r($file,true) );
		$query = "SELECT
					f.FileID
					from file f
					inner join path p on f.PathID = p.PathID
					where p.Path = '".$path."'
					AND f.FileName = '".$file."'
					AND f.Online = 1";
		
		$rs = $this->db_xinet->query( $query )->row_array();
		$id = empty($rs['FileID']) ? 0 : $rs['FileID'];
		$this->debug('$rs '.print_r($rs,true) );
		$this->debug('$query '.print_r($query,true) );
		$this->debug('$id1 '.print_r($id,true) );
		
		return $id;
	}
	
	/**
	 * Recupera a data de alteracao de um arquivo
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $path
	 * @param string $file
	 * @return string
	 */
	function getDataAlteracao($path, $file) {
		$this->db_xinet = $this->load->database('xinet', TRUE);
		$path = rtrim($path, '//');
		
		$path = utf8MAC($path);
		$file = str_to_xinet($file);
		
		$query = 'SELECT
					f.ModifyDate
					from file f
					inner join path p on f.PathID = p.PathID
					where p.Path = "'.$path.'"
					AND f.FileName = "'.$file.'"
					AND f.online=1
					';
		
		$rs = $this->db_xinet->query( $query )->row_array();
		$data = empty($rs['ModifyDate']) ? 0 : $rs['ModifyDate'];
		
		
		return $data;
	}

	// retorna a path passando o id
	function getPathPorId($id) {
		$this->db_xinet = $this->load->database('xinet', TRUE);
		$query = "SELECT concat(path.Path, '/', file.UnixName) File, path.Path, FileID, FileName, Type FROM file
					INNER JOIN path ON file.PathID = path.PathID
					where file.FileID = '{$id}' and file.Online = 1";

			$rs = $this->db_xinet->query($query);

			if ( $rs->num_rows() > 0 ){
				return  $rs->row_array();
			}
			else
				return false;
	}
	
	function getWebData($FileID, $tabela, $spread='0'){
		$this->db_xinet = $this->load->database('xinet', TRUE);
		$query = "SELECT * FROM ".$tabela." where FileID=".$FileID." and Spread = ".$spread."  limit 1";
		$rs = $this->db_xinet->query($query);
		if ( $rs->num_rows() > 0 )
			return $rs->result_array();
		else
			return false;
	}
	
	function getTotalSpread($FileID, $tabela){
		$this->db_xinet = $this->load->database('xinet', TRUE);
		$query = "SELECT count(FileID) as 'PAGINAS' FROM ".$tabela." where FileID=".$FileID."";
		$rs = $this->db_xinet->query($query);
		if ( $rs->num_rows() > 0 )
			return $rs->result_array();
		else
			return false;
	}
	
	function getImgFilePorID( $FileID, $prefix_db='smallwebdata', $img_size='64' ){
		$this->db_xinet = $this->load->database('xinet', TRUE);
		$query = "SELECT f.FileID as fileid, f.FileName, f.Type,
	        concat('".$prefix_db."_',right(concat('000',(f.FileID % ".$img_size.")),3)) as tabela
	        FROM file f join path p on (f.PathID=p.PathID)
	        where f.FileID = '".$FileID."' ";

		$rs = $this->db_xinet->query($query);
		if ( $rs->num_rows() > 0 )
			return $rs->result_array();
		else
			return false;
	}
	
	private function debug($str) {
		$str = "---------------------------\n$str\n====\n\n";
		if($this->debug) {
			file_put_contents('system/application/models/uploadDebug.txt', $str, FILE_APPEND);
		}
	}
	
	/* Model que retorna os files de acordo com o fileids separados por virgula(,) */
	function getFilesPorId( $FileID ) {
		$this->db_xinet = $this->load->database('xinet', TRUE);
		$sql = "SELECT
					CONCAT( CONCAT(path.Path, '/' ), UnixName ) AS File,
					file.FileID, path2.PathID,
					file.Type, file.UnixName,
					file.FileName, file.Dir,
					path.Path,
					highresinfo.Width,
					highresinfo.Height,
					highresinfo.Resolution,
					highresinfo.ColorSpace,
					file.FileSize,
					file.CreateDate AS uploadDate,
					file.FileSize,
					keyword1.Field58 AS CreateDate,
					keyword1.Field72 AS CreateDateOrig,
					keyword1.Field61 AS ModifyDate,
					keyword1.Field73 AS ModifyDateOrig,
					keyword1.Field".getKeywordID('data_validade').",
					file.PathId as PathIdArquivo
				FROM file
				LEFT JOIN path ON file.PathID = path.PathID
				LEFT JOIN path AS path2 ON path2.Path = ( REPLACE( CONCAT( path.Path, '/', file.UnixName ), '//', '/' ) )
				LEFT JOIN keyword1 ON file.FileID = keyword1.FileID
				LEFT JOIN highresinfo ON file.FileID = highresinfo.FileID
				WHERE file.FileID IN( ".$FileID." )
				AND file.Online = 1 ";
	
		$rs = $this->db_xinet->query($sql);
		if ( $rs->num_rows() > 0 )
			return $rs->result_array();
	
		return false;
	
	}
	
}
