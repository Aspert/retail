<?php
/**
 * Classe de modelo para o gerenciamento de agencias
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class FilaOPIDB extends GenericModel {
	### START
	protected function _initialize(){
		$this->addField('ID_FILA_OPI','int','',1,1);
		$this->addField('FILA_OPI_DPI','int','',3,0);
	}
	### END

	var $tableName = 'TB_FILA_OPI';
	
	/**
	 * Construtor
	 *  
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return AgenciaDB
	 */
	function __construct(){
		parent::GenericModel();
		
		// adiciona as validacoes
		$this->addValidation('FILA_OPI_DPI','requiredNumber','Informe a resolução');
	}
	
	/**
	 * Metodo para listar itens
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data Dados para filtro
	 * @param int $pagina numero da pagina atual
	 * @param int $limit limit de itens por pagina
	 * @return array
	 */
	public function listItems($data, $pagina=0, $limit = 5){
		$this->setWhereParams($data);
		return $this->execute($pagina, $limit);
	}
	
	
}

