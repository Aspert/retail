<?php
class CampoRelacionadoDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_CAMPO_FICHA','int','',2,0);
		$this->addField('ID_SUBFICHA','int','',2,0);
	}
	### END

	var $tableName = 'TB_CAMPO_RELACIONADO';

	function __construct(){
		parent::GenericModel();
	}
	
	function getByFicha($idFicha){
		$sql = "SELECT * FROM TB_CAMPO_RELACIONADO CR
				INNER JOIN TB_SUBFICHA SF ON CR.ID_SUBFICHA = SF.ID_SUBFICHA
				INNER JOIN TB_CAMPO_FICHA CF ON CR.ID_CAMPO_FICHA = CF.ID_CAMPO_FICHA
				WHERE SF.ID_FICHA2 = " . $idFicha;
		
		$rs = $this->db->query($sql);
		
		return $rs->result_array();
	}
	
	function deleteBySubficha($idSubficha){
		$this->db->delete($this->tableName, array('ID_SUBFICHA' => $idSubficha));
	}
}





