<?php
class PricingDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_PRICING','int','',1,1);
		$this->addField('ID_EXCEL_ITEM','int','',1,0);
		$this->addField('PRECO_UNITARIO','real','',5,0);
		$this->addField('TIPO_UNITARIO','string','',0,0);
		$this->addField('PRECO_CAIXA','real','',0,0);
		$this->addField('TIPO_CAIXA','string','',0,0);
		$this->addField('PRECO_VISTA','real','',5,0);
		$this->addField('PRECO_DE','real','',6,0);
		$this->addField('PRECO_POR','real','',5,0);
		$this->addField('ECONOMIZE','real','',5,0);
		$this->addField('ENTRADA','real','',4,0);
		$this->addField('PARCELAS','int','',1,0);
		$this->addField('PRESTACAO','real','',5,0);
		$this->addField('JUROS_AM','real','',4,0);
		$this->addField('JUROS_AA','real','',4,0);
		$this->addField('TOTAL_PRAZO','real','',5,0);
		$this->addField('OBSERVACOES','blob','',0,0);
	}
	### END

	var $tableName = 'TB_PRICING';

	protected $moneyFields = array();

	/**
	 * Construtor
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return PricingDB
	 */
	function __construct(){
		parent::GenericModel();

		$this->moneyFields = explode(',','PRECO_UNITARIO,PRECO_CAIXA,PRECO_VISTA,PRECO_DE,PRECO_POR,ECONOMIZE,ENTRADA,PRESTACAO,TOTAL_PRAZO,JUROS_AM,JUROS_AA');
	}

	/**
	 * Recupera um item pelo codigo da ficha, praca e job
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idFicha codigo da ficha
	 * @param int $idPraca codigo da praca
	 * @param int $idJob codigo do job
	 * @return array
	 */
	public function getByFichaPracaJob($idFicha, $idPraca, $idJob){
		$rs = $this->db->where('ID_JOB', $idJob)
			->where('ID_FICHA',$idFicha)
			->where('ID_PRACA',$idPraca)
			->get($this->tableName);

		return $rs->row_array();
	}

	/**
	 * Salva um pricing enviado pelo excel
	 *
	 * <p>Salva o pricing de um item de uma praca</p>
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data dados a serem salvos
	 * @return void
	 */
	public function savePricingExcel($data){
        return $this->save($data);

	}

	/**
	 * Pega o pricing de um item
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $item
	 * @return array
	 */
	public function getByItem($item){
		$rs = $this->db->where('ID_EXCEL_ITEM', $item)
			->get($this->tableName)
			->row_array();

		return $rs;
	}

	/**
	 * Salva o pricing
	 *
	 * <p>Recebe um array multimendional de items, onde a chave
	 * e o codigo do item e o valor e um array de valores</p>
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param $itens
	 * @return void
	 */
	public function savePricing( array $itens ){
		foreach($itens as $id => $data){
			$this->save($data, $id);
		}
	}

	public function save($data, $id=null){
        $real = explode(',','ID_EXCEL_ITEM,ID_EXCEL_VERSAO,PRECO_UNITARIO,PRECO_CAIXA,PRECO_VISTA,PRECO_DE,PRECO_POR,
        ECONOMIZE,ENTRADA,PRESTACAO,TOTAL_PRAZO,OBSERVACOES,PARCELAS,TIPO_UNITARIO,TIPO_CAIXA,JUROS_AM,JUROS_AA');
        $price = array();
        $values = array(
            "PRECO_UNITARIO","PRECO_CAIXA","PRECO_VISTA","PRECO_DE",
            "PRECO_POR","ECONOMIZE","ENTRADA","PRESTACAO","TOTAL_PRAZO"
        );
        foreach($real as $r) {
            foreach ($data as $key => $value) {
                if ($key == $r) {
                    /*if ($key != "ID_EXCEL_ITEM" && $key != "ID_EXCEL_VERSAO" ) {
                        $price[$key] = valor_to_save($value);
                    } else {
                        $price[$key] = $value;
                    }*/
                    if (in_array($key,$values)) {
                        $price[$key] = valor_to_save($value);
                    } else {
                        $price[$key] = $value;
                    }
                }
            }
			/*if( isset($data[$key]) ){
                $i++;
				if( $data[$key] === '' || $data[$key] == '' ) {
					$data[$key] = NULL;
				} else {
					$data[$key] = valor_to_save($data[$key]);
				}
			}*/
		}
        parent::save($price, $id);
	}

	public function savePricingsExcel(array $lista){
		$real = $this->moneyFields;

		$sql = 'INSERT INTO '.$this->tableName.' (';
		$fields = array();
		$lines = array();

		foreach($lista as $row){
			// convertemos os campos numericos para valores para o banco
			foreach($real as $key){
				if( array_key_exists($key, $row) ){

					if( isset($row[$key]) && $row[$key] != "" ){
						$row[$key] = valor_to_save($row[$key]);
					}
					else{
						$row[$key] = '';
					}
				}
			}

			// se ainda nao preencheu os campos
			if(empty($fields)){
				// para cada item da linha
				foreach($row as $key => $value){
					// se a chave existe no mapeamento
					if( array_key_exists($key, $this->mapping)){
						// coloca a chave na lista de campos
						$fields[] = $key;
					}
				}
				// coloca a lista de campos na SQL
				$sql .= implode(', ', $fields) . ') VALUES ';
			}

			// valores a serem gravados
			$values = array();
			// para cada item da linha
			foreach($row as $key => $value){
				// se a chave existe no mapeamento
				if( array_key_exists($key, $this->mapping)){
					// coloca na lista para gravar
					$values[] = sprintf("'%s'", mysql_escape_string($value));
				}
			}
			// coloca a linha no cache
			$lines[] = '(' . implode(', ', $values) . ')';
		}
		
		// se ha linhas
		if(!empty($lines)){
			// une separando por virgula
			$sql .= implode(', ' . PHP_EOL, $lines);
			$sql = str_replace("''", "null", $sql);

			// efetua a consulta para gravar tudo de uma vez
			$this->db->query($sql);
		}
	}

	// PC-5955
	// Em 21/10/2010 - Incluido parametro $onlyRealFields ( boolean )
	// Se $onlyRealFields = true, serão avaliados somente os campos que constam em $lista e no banco de dados ($this->mapping)
	//		No Insert (Os campos não passados serao respeitados o default do banco)
	//		No Update (Serao atualizados apenas os campos passados em $lista
	// Se $onlyRealFields = false, serão considerados todos os campos do banco de dados ($this->mapping)
	
	public function atualizarLista( array $lista, $onlyRealFields=false ){
		
		$fields = array();
		$values = array();

		if ( $onlyRealFields == true && !empty($lista[0]) && is_array($lista[0]) ) {
			foreach($this->mapping as $field){
				foreach ($lista[0] as $fieldName=>$fieldValue) {
					if ($fieldName == $field->name) {
						$fields[] = $field->name;
						$fieldsImpacted[] = $field;
						break;
					}
				}
			}
		} else { 
			foreach($this->mapping as $field){
				$fields[] = $field->name;
			}
			$fieldsImpacted = $this->mapping;
		}

		foreach($lista as $item){
			$row = array();
			foreach($fieldsImpacted as $field){
				if( in_array($field->name, $this->moneyFields) && array_key_exists($field->name, $item) ){
					if($item[$field->name] != NULL ){
						$row[] = sprintf('%0.2f', valor_to_save($item[$field->name]));
					}else{
						$row[] = 'NULL';
					}
				} else {
					$row[] = !isset($item[$field->name]) || is_null($item[$field->name])
						? 'NULL'
						:  $this->db->escape( $item[$field->name] );
				}
			}
			$values[] = '(' . implode(', ', $row) . ')';
		}

		$str = 'INSERT INTO TB_PRICING ';
		$str .= '(' . implode(', ', $fields) .') VALUES ' . implode(', '.PHP_EOL, $values);
		$str .= ' ON DUPLICATE KEY UPDATE ';

		foreach($fieldsImpacted as $field){
			if( empty($field->primary) ){
				$str .= $field->name.' = VALUES(' . $field->name . '), ';
			}
		}

		$str = substr($str, 0, -2);
		$str = str_replace("''", "null", $str);
		
		$this->db->query($str);
	}
}

