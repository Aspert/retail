<?php
/**
 * Model que representa a TB_CONTROLLER
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class ControllerDB  extends GenericModel {
	### START
	protected function _initialize(){
		$this->addField('ID_CONTROLLER','int','',1,1);
		$this->addField('DESC_CONTROLLER','string','',15,0);
		$this->addField('CHAVE_CONTROLLER','string','',5,0);
	}
	### END

	var $tableName = 'TB_CONTROLLER';
	
	/**
	 * Construtor
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return ControllerDB
	 */
	function __construct(){
		parent::GenericModel();

		$this->addValidation('DESC_CONTROLLER','requiredString','Informe a descrição');
		$this->addValidation('CHAVE_CONTROLLER','requiredString','Informe a chave');
	}
	
	/**
	 * Lista as controllers cadastradas
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $filters Filtros para pesquisa
	 * @param int $page Pagina atual
	 * @param int $limit limit de registros por pagina
	 * @return array
	 */
	public function listItems($filters, $page=0, $limit=5){
		$this->setWhereParams($filters);
		return $this->execute($page,$limit,'DESC_CONTROLLER');
	}
	
	/**
	 * Recupera todas as controllers com suas functions
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return array Lista contendo todas as controller com suas functions
	 */
	public function getAllWithFunctions(){
		$list = array();
		
		$rs = $this->db->from($this->tableName)
			->order_by('DESC_CONTROLLER')
			->get();
			
		$rs_function = $this->function->getAll();
		$functions = $rs_function['data'];
		
		foreach($rs->result_array() as $item){
			$list[$item['ID_CONTROLLER']] = $item;
			$list[$item['ID_CONTROLLER']]['functions'] = array();
		}
		
		foreach($functions as $item){
			$list[$item['ID_CONTROLLER']]['functions'][] = $item;
		}
		
		return $list;
	}
}	

