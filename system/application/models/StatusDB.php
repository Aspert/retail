<?php
class StatusDB extends GenericModel {
	
	public static $BLOQUEADO = 'BLOQUEADO';
	
	### START
	protected function _initialize(){
		$this->addField('ID_STATUS','int','',1,1);
		$this->addField('CHAVE_STATUS','string','',8,0);
		$this->addField('DESC_STATUS','string','',8,0);
		$this->addField('OBS_STATUS','string','',61,0);
	}
	### END

	var $tableName = 'TB_STATUS';
	
	/**
	 * Recupera registros por mais de uma chave
	 * @author juliano.polito
	 * @param  $keys
	 * @return array
	 */
	public function getByKeys(array $keys){
		$rs = $this->db->where_in('CHAVE_STATUS',$keys)->get($this->tableName);
		
		if($rs->num_rows() == 0){
			return array();
		}
		
		return $rs->result_array();
	}
	
	/**
	 * Recupera o registro pela chave
	 * @author Hugo Silva
	 * @param string $key nome da chave
	 * @return array Array contendo os valores do registro
	 */
	public function getByKey($key){
		$rs = $this->db->where('CHAVE_STATUS',$key)->get($this->tableName);
		
		if($rs->num_rows() == 0){
			return array();
		}
		
		return $rs->row_array();
	}
	
	/**
	 * Recupera o ID do status pela chave
	 * @author Juliano Polito
	 * @param $key A chave do status
	 * @return O ID caso encontre, ou null caso não encontre
	 */
	public function getIdByKey($key){
		$row = $this->getByKey($key);		
		return $row["ID_STATUS"];
	}
}

