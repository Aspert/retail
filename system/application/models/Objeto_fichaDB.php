<?php
class Objeto_FichaDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_OBJETO','int','',0,1);
		$this->addField('ID_FICHA','int','',0,1);
		$this->addField('PRINCIPAL','int','',0,0);
	}
	### END


	var $tableName = 'TB_OBJETO_FICHA';

	/**
	 * Recupera as fichas relacionadas a um objeto pelo fileid do Xinet
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $fileid
	 * @return array
	 */
	public function getByFileId($fileid){
		$rs = $this->db
			->join('TB_OBJETO O', 'O.ID_OBJETO = OF.ID_OBJETO')
			->join('TB_FICHA F', 'F.ID_FICHA = OF.ID_FICHA')
			->where('O.FILE_ID', $fileid)
			->get($this->tableName . ' OF')
			->result_array();

		return $rs;
	}

	/**
	 * Recupera os objetos pela ficha
	 * @author Hugo Ferreira da Silva
	 * @param int $idficha codigo da ficha
	 * @return array
	 */
	public function getByFicha($idficha, $isPrincipal = false) {
		$this->db->from($this->tableName . ' OF')
			->join('TB_OBJETO O','O.ID_OBJETO = OF.ID_OBJETO')
			->join('TB_TIPO_OBJETO OT','OT.ID_TIPO_OBJETO = O.ID_TIPO_OBJETO')
			->join('TB_CATEGORIA C','C.ID_CATEGORIA = O.ID_CATEGORIA','LEFT')
			->join('TB_SUBCATEGORIA SC','SC.ID_SUBCATEGORIA = O.ID_SUBCATEGORIA','LEFT')
			->join('TB_FABRICANTE F','F.ID_FABRICANTE = O.ID_FABRICANTE','LEFT')
			->join('TB_MARCA M','M.ID_MARCA = O.ID_MARCA','LEFT')
			->where('OF.ID_FICHA',$idficha);
			
		if($isPrincipal){
			$this->db->where('OF.PRINCIPAL',1);
		}
		
		$rs = $this->db->order_by('O.FILENAME')
			->get()			
			->result_array();

		return $rs;
	}

	/**
	 * remove os relacionamentos de fichas e objetos
	 * @author Hugo Ferreira da Silva
	 * @param int $idficha
	 * @return void
	 */
	public function deleteByFicha($idficha){
		$this->db
			->where('ID_FICHA', $idficha)
			->delete($this->tableName);
	}

	/**
	 * Retorna os tipos relacionados a um excel, versao e praca
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idexcel
	 * @param int $idversao
	 * @param int $idpraca
	 * @return array
	 */
	public function getTiposByExcelVersaoPraca($idexcel, $idversao, $idpraca){
		$hashset = array();

		// primeiro, pegamos todos os itens de uma praca
		$rs = $this->db
			->where('ID_EXCEL', $idexcel)
			->where('ID_EXCEL_VERSAO', $idversao)
			->where('ID_PRACA', $idpraca)
			->get('TB_EXCEL_ITEM')
			->result_array();

		// codigos de fichas
		$codes = array();
		foreach($rs as $item){
			$codes[] = $item['ID_FICHA'];
		}

		// se encontrou fichas
		if( !empty($codes) ){
			// pegamos todos os objetos de todas as fichas
			$rs = $this->db
				->join('TB_OBJETO O','OF.ID_OBJETO = O.ID_OBJETO')
				->where_in('OF.ID_FICHA', $codes)
				->get('TB_OBJETO_FICHA OF')
				->result_array();


			foreach($rs as $item){
				$hashset[$item['FILE_ID']] = $item['ID_TIPO_OBJETO'];
			}
		}

		return $hashset;
	}

	/**
	 * recupera as datas de alteracao dos objetos para um excel, versao e praca
	 * @author Hugo Ferreira da Silva
	 * @param int $idexcel
	 * @param int $idversao
	 * @param int $idpraca
	 * @return array
	 */
	public function getDataAlteracaoByExcelVersaoPraca($idexcel, $idversao, $idpraca){
		$hashset = array();

		$rs = $this->db
			->select('O.FILE_ID, UNIX_TIMESTAMP(O.DATA_UPLOAD) AS DATA_ENVIADO')
			->join('TB_FICHA F','F.ID_FICHA = OF.ID_FICHA')
			->join('TB_OBJETO O','O.ID_OBJETO = OF.ID_OBJETO')
			->join('TB_EXCEL_ITEM I','I.ID_FICHA = F.ID_FICHA')
			->where('I.ID_EXCEL', $idexcel)
			->where('I.ID_EXCEL_VERSAO', $idversao)
			->where('I.ID_PRACA', $idpraca)
			->get('TB_OBJETO_FICHA OF')
			->result_array();

		$hashset = arraytoselect($rs,'FILE_ID', 'DATA_ENVIADO');

		return $hashset;
	}

	/**
	 * Recupera os objetos vinculados a fichas
	 *
	 * @author Hugo Ferreira da Silva
	 * @param array $codigos Codigos das fichas
	 * @return array Objetos encontrados
	 */
	public function getByFichas(array $codigos){
		if(empty($codigos)){
			$codigos[] = 0;
		}
		$rs = $this->db
			->join('TB_OBJETO O','O.ID_OBJETO = OF.ID_OBJETO')
			->join('TB_TIPO_OBJETO OT','OT.ID_TIPO_OBJETO = O.ID_TIPO_OBJETO')
			->join('TB_CATEGORIA C','C.ID_CATEGORIA = O.ID_CATEGORIA')
			->join('TB_SUBCATEGORIA SC','SC.ID_SUBCATEGORIA = O.ID_SUBCATEGORIA')
			->join('TB_FABRICANTE F','F.ID_FABRICANTE = O.ID_FABRICANTE','LEFT')
			->join('TB_MARCA M','M.ID_MARCA = O.ID_MARCA','LEFT')
			->where_in('OF.ID_FICHA', $codigos)
			->order_by('O.FILENAME')
			->get($this->tableName . ' OF')
			->result_array();

		return $rs;
	}

	/**
	 * Recupera as fichas combos associadas a uma ficha produto e objeto
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idficha Codigo da ficha produto
	 * @param int $idobjeto Codigo do objeto
	 * @return array Fichas-combo que possuem o mesmo objeto da ficha produto indicada
	 */
	public function getFichasComboAssociadas($idficha, $idobjeto){
		$rs = $this->db
			->from($this->tableName . ' OF')
			->join('TB_FICHA_COMBO FCP','FCP.ID_FICHA_COMBO = OF.ID_FICHA AND FCP.ID_FICHA_PRODUTO = ' . sprintf('%d',$idficha))
			->join('TB_FICHA F','FCP.ID_FICHA_COMBO = F.ID_FICHA')
			->where('OF.ID_OBJETO', $idobjeto)
			->get()
			->result_array();

		return $rs;
	}
	
	public function getByIdObjetoAndIdFicha($idFicha,$idObjeto){
		
		$rs = $this->db
		->from($this->tableName . ' OF')
		->where('OF.ID_OBJETO', $idObjeto)
		->where('OF.ID_FICHA', $idFicha)
		->get()
		->result_array();
		
		return $rs;
	}
	
	public function insertObjetoSubFicha($data){
		/*$this->db->where('ID_OBJETO', $data['ID_OBJETO']);
		$this->db->where('ID_FICHA', $data['ID_FICHA']);*/
		$this->db->insert($this->tableName, $data);
	}

}
