<?php
/**
 * Classe de modelo para o gerenciamento de segmentos
 * @author Juliano Polito
 * @link http://www.247id.com.br
 */
class SegmentoDB extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_SEGMENTO','int','',1,1);
		$this->addField('DESC_SEGMENTO','string','',7,0);
	}
	### END

	var $tableName = 'TB_SEGMENTO';
	
	/**
	 * Construtor
	 *  
	  * @author Juliano Polito
	 * @link http://www.247id.com.br
	 * @return ProdutoDB
	 */
	function __construct(){
		parent::GenericModel();
		
		// adiciona as validacoes
		$this->addValidation('DESC_SEGMENTO','requiredString','Informe o segmento');
	}
	
}

