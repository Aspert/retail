<?php
/**
 * Model que representa o G_A_C_P (Grupo-Agencia-Cliente-Produto)
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class GacpDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_G_A_C_P','int','',1,1);
		$this->addField('ID_GRUPO','int','',1,0);
		$this->addField('ID_CLIENTE','int','',0,0);
		$this->addField('ID_AGENCIA','int','',0,0);
		$this->addField('ID_PRODUTO','int','',0,0);
	}
	### END

	/**
	 * Nome da tabela
	 * @var string
	 */
	var $tableName = 'TB_G_A_C_P';
	
	/**
	 * Construtor
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return UsuarioDB
	 */
	function __construct(){
		parent::GenericModel();
		
	}
	
	/**
	 * Recupera um GACP.
	 * 
	 * <p>Caso nao exista, insere um novo e retorna o codigo</p>
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $grupo Codigo do grupo
	 * @param int $agencia Codigo da agencia
	 * @param int $cliente Codigo do cliente
	 * @param int $produto Codigo do produto
	 * @return int
	 * @throws Exception 
	 */
	public function getGACP($grupo,$agencia,$cliente,$produto){
		// o grupo eh o unico que nao pode ficar vazio
		if( empty($grupo) ){
			throw new Exception('O campo grupo não pode ser vazio');
		}
		
		$this->db->from('TB_G_A_C_P GA')
			->where('GA.ID_GRUPO',$grupo);
			
		$data['ID_GRUPO'] = $grupo;
			
		if(!empty($agencia)){
			$this->db->where('GA.ID_AGENCIA', $agencia);
			$data['ID_AGENCIA'] = $agencia;
		} else {
			$this->db->where('GA.ID_AGENCIA IS NULL');
		}
		
		if(!empty($cliente)){
			$this->db->where('GA.ID_CLIENTE', $cliente);
			$data['ID_CLIENTE'] = $cliente;
		} else {
			$this->db->where('GA.ID_CLIENTE IS NULL');
		}
		
		if(!empty($produto)){
			$this->db->where('GA.ID_PRODUTO', $produto);
			$data['ID_PRODUTO'] = $produto;
		} else {
			$this->db->where('GA.ID_PRODUTO IS NULL');
		}
		
		$res = $this->db->get();
		
		if($res->num_rows() > 0){
			$row = $res->row_array();
			return $row['ID_G_A_C_P'];
		}
		
		// ok, nao achamos, vamos cadastrar um novo
		$id = $this->save($data);
		
		return $id;
	}
	
	/**
	 * Lista os itens cadastrados
	 * 
	 * <p>Une tambem com as tabelas relacionadas para poder exibir melhor para o usuario</p>
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $filters Filtros para pesquisa
	 * @param int $page Pagina atual
	 * @param int $limit Numero de itens por pagina
	 * @return array
	 */
	public function listItems($filters, $page=0, $limit=5){
		$this->db->join('TB_GRUPO G','G.ID_GRUPO = TB_G_A_C_P.ID_GRUPO')
			->join('TB_AGENCIA A','A.ID_AGENCIA = TB_G_A_C_P.ID_AGENCIA','LEFT')
			->join('TB_CLIENTE C','C.ID_CLIENTE = TB_G_A_C_P.ID_CLIENTE','LEFT')
			->join('TB_PRODUTO P','P.ID_PRODUTO = TB_G_A_C_P.ID_PRODUTO','LEFT');
			
		$this->setWhereParams($filters);
		return $this->execute($page,$limit,'DESC_GRUPO, DESC_AGENCIA, DESC_CLIENTE, DESC_PRODUTO');
	}
	
	/**
	 * Retorna todas as funcoes relacionadas a um GACP
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo GACP
	 * @return array Lista contendo as funcoes relacionadas
	 */
	public function getFunctions($id){
		$rs = $this->db->from($this->tableName.' G')
			->join('TB_PERMISSAO P','P.ID_G_A_C_P = G.ID_G_A_C_P')
			->join('TB_FUNCTION F','F.ID_FUNCTION = P.ID_FUNCTION')
			->where('G.ID_G_A_C_P', $id)
			->get();
			
		return $rs->result_array();
	}
	
	/**
	 * Grava as permissoes para o GACP
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo GACP
	 * @param array $functions Lista contendo os codigos das funcoes
	 * @return void
	 */
	public function salvarPermissoes($id, array $functions){
		// primeiro, remove as permissoes do grupo que nao estao selecionadas
		$this->db->where('ID_G_A_C_P', $id)
			->where_not_in('ID_FUNCTION', $functions)
			->delete('TB_PERMISSAO');
			
		// agora vamos ver se cada uma das selecionadas 
		// ja esta no banco. se nao estiver, colocamos
		foreach($functions as $fnc){
			$total = $this->db->where('ID_G_A_C_P', $id)
				->where('ID_FUNCTION', $fnc)
				->get('TB_PERMISSAO')
				->num_rows();
				
			if($total == 0){
				$data = array(
					'ID_FUNCTION' => $fnc,
					'ID_G_A_C_P' => $id
				);
				
				$this->permissao->save($data);
			}
		}
		
	}
	
	public function getUsuarios($grupo,$agencia,$cliente,$produto){
		$id = $this->getGACP($grupo,$agencia,$cliente,$produto);
		return $this->usuario->getByGACP($id);
	}
	
}	





