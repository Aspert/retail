<?php

class ProcessoEtapaDB extends GenericModel {
	### START
	protected function _initialize(){
		$this->addField('ID_PROCESSO_ETAPA','int','',11,1);
		$this->addField('ID_PROCESSO','int','',11,0);
		$this->addField('ID_ETAPA','int','',11,0);
		$this->addField('ORDEM','int','',11,0);
		$this->addField('QTD_DIAS','real','',7,0);
		$this->addField('QTD_DIAS_ACUMULADO','real','',8,0);
		$this->addField('ID_ETAPA','int','',11,0);
		$this->addField('QTD_HORAS','string','',6,0);
		$this->addField('QTD_HORAS_ACUMULADO','string','',7,0);
		$this->addField('ID_STATUS','int','',11,0);
	}
	### END

	var $tableName = 'TB_PROCESSO_ETAPA';


	/**
	 * Remove as etapas de um processo
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idprocesso
	 * @return void
	 */
	public function removeAll($idprocesso){
		$this->db
			->where('ID_PROCESSO', $idprocesso)
			->delete($this->tableName);
	}

	/**
	 * Recupera as etapas relacionadas a um processo
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idprocesso Codigo do processo
	 * @return array lista contendo as etapas de um processo
	 */
	public function getByProcesso($idprocesso){
		$rs = $this->db
			->join('TB_ETAPA E','E.ID_ETAPA = TB_PROCESSO_ETAPA.ID_ETAPA')
			->where('ID_PROCESSO', $idprocesso)
			->order_by('ORDEM')
			->get($this->tableName)
			->result_array();

		return $rs;
	}
	
	/**
	 * Recupera o registro de um processo
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idprocesso Codigo do processo
	 * @param char $chaveEtapa Chave da Etapa
	 * @return array com o registro
	 */
	public function getByProcessoChaveEtapa($idProcesso=0, $chaveEtapa=''){
		
		$rs = $this->db
			->select('PE.*, E.*')
			->from('TB_PROCESSO_ETAPA PE')
			->join('TB_ETAPA E','E.ID_ETAPA = PE.ID_ETAPA')
			->where('PE.ID_PROCESSO', $idProcesso)
			->where('E.CHAVE_ETAPA', $chaveEtapa)
			->order_by('PE.ORDEM')
			->get()
			->row_array();
			
		return $rs;
	}
	
	/**
	 * Pegar a proxima ordem do processo
	 * 
	 * @author esdras.filho
	 * @param int $idProcesso
	 * @return int
	 */
	public function getLastOrdem( $idProcesso ){
		$rs = $this->db
			->from($this->tableName)
			->select('ORDEM')
			->where('ID_PROCESSO', $idProcesso)
			->order_by('ORDEM DESC')
			->limit(1)
			->get()
			->result_array();
		$resultado = 1;
		if( count($rs) == 1 ){
			$resultado = (int)$rs[0]['ORDEM'] + 1;
		}
		
		return $resultado;
	}
}