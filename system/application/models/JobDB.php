<?php
class JobDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_JOB','int','',1,1);
		$this->addField('ID_CAMPANHA','int','',2,0);
		$this->addField('TITULO_JOB','string','',23,0);
		$this->addField('LARGURA_JOB','real','',0,0);
		$this->addField('ALTURA_JOB','real','',0,0);
		$this->addField('DATA_INICIO_PROCESSO','datetime','',10,0);
		$this->addField('DATA_TERMINO','date','',10,0);
		$this->addField('DATA_INICIO','date','',10,0);
		$this->addField('DATA_FINALIZADO','date','',10,0);
		$this->addField('ID_CLASSIFICACAO','int','',0,0);
		$this->addField('STATUS_JOB','int','',1,0);
		$this->addField('ID_TIPO_JOB','int','',1,0);
		$this->addField('ID_ETAPA','int','',1,0);
		$this->addField('PAGINAS_JOB','int','',1,0);
		$this->addField('ID_STATUS','int','',1,0);
		$this->addField('DT_DELAY_BLOQUEIO','datetime','',0,0);

		$this->addField('ID_CLIENTE','int','',1,0);
		$this->addField('ID_AGENCIA','int','',1,0);
		$this->addField('ID_USUARIO_AUTOR','int','',1,0);
		$this->addField('ID_PROCESSO','int','',1,0);
		$this->addField('ID_PROCESSO_ETAPA','int','',11,0);
		$this->addField('ID_USUARIO_RESPONSAVEL','int','',11,0);
		
		$this->addField('ID_TIPO_PECA','int','',1,0);
		$this->addField('ID_REGIAO','int','',0,0);
		$this->addField('ALTERACOES_JOB','int','',0,0);

	}
	### END

	var $tableName = 'TB_JOB';

	/**
	 * Construtor
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return JobDB
	 */
	function __construct(){
		parent::GenericModel();

		$this->addValidation('TITULO_JOB','requiredString','Informe o título');
		$this->addValidation('TITULO_JOB','function', array($this,'checaAcentuacao'));
		//$this->addValidation('LARGURA_JOB','requiredString','Informe a largura');
		//$this->addValidation('ALTURA_JOB','requiredString','Informe a altura');
		$this->addValidation('DATA_INICIO_PROCESSO','requiredDate','Informe a data de inicio do processo');
		$this->addValidation('DATA_INICIO','requiredDate','Informe a data de inicio da validade');
		$this->addValidation('DATA_TERMINO','requiredDate','Informe a data de final da validade');
		$this->addValidation('ID_CAMPANHA','requiredNumber','Informe a campanha');
		$this->addValidation('ID_TIPO_PECA','requiredNumber','Informe o tipo de peça');
		$this->addValidation('ID_TIPO_JOB','requiredNumber','Informe o tipo de job');
		$this->addValidation('DATA_INICIO_PROCESSO','function',array($this,'checaDataInicioProcesso'));
		$this->addValidation('PAGINAS_JOB','requiredNumber','Informe o número de páginas');
		$this->addValidation('ID_REGIAO','requiredNumber','Informe a região');
	}

	/**
	 * override para ajustar os valores da largura e altura
	 * @see system/application/libraries/GenericModel::save()
	 */
	public function save($data, $id = null){
		if( !empty($data['LARGURA_JOB']) && !is_numeric($data['LARGURA_JOB']) ){
			$data['LARGURA_JOB'] = money2float($data['LARGURA_JOB']);
		}

		if( !empty($data['ALTURA_JOB']) && !is_numeric($data['ALTURA_JOB']) ){
			$data['ALTURA_JOB'] = money2float($data['ALTURA_JOB']);
		}

		return parent::save($data, $id);
	}

	public function checaDataInicioProcesso($data){
		//array com as etapas do processo
		$arrayEtapas = array();
		$etapas = array();
		
		//array com o processo utilizado
		$arrayProcesso = array();
		
		//feriados utilizados neste processo
		$feriados = array();
		
		//Tipo de Peca
		$idTipoPeca = 0;
		
		//Produto
		$idProduto = 0;
		
		//Consiste se a data inicio do processo eh valida
		if (!checkdate(	format_date($data['DATA_INICIO_PROCESSO'],'m'),
						format_date($data['DATA_INICIO_PROCESSO'],'d'),
						format_date($data['DATA_INICIO_PROCESSO'],'Y'))) {
			return 'A data de inicio do processo job é inválida';
		} else {
			$diaInicioProcesso = format_date($data['DATA_INICIO_PROCESSO'],'d');
			$mesInicioProcesso = format_date($data['DATA_INICIO_PROCESSO'],'m');
			$anoInicioProcesso = format_date($data['DATA_INICIO_PROCESSO'],'Y');
		}
		
		//Consiste se a data inicio eh valida
		if (!checkdate(	format_date($data['DATA_INICIO'],'m'),
						format_date($data['DATA_INICIO'],'d'),
						format_date($data['DATA_INICIO'],'Y'))) {
			return 'A data de inicio do job é inválida';
		} else {
			$diaInicio = format_date($data['DATA_INICIO'],'d');
			$mesInicio = format_date($data['DATA_INICIO'],'m');
			$anoInicio = format_date($data['DATA_INICIO'],'Y');
		}
		
		//Verifica se foi informado o tipo de peca
		if( !empty($data['ID_TIPO_PECA']) ) {
			$idTipoPeca = $data['ID_TIPO_PECA'];
		} else {
			return 'Informe o tipo de peça!';
		}
		
		//Verifica se veio Bandeira para busca dos feriados
		if( !empty($data['ID_PRODUTO']) ) {
			$idProduto = $data['ID_PRODUTO'];
		} else {
			return 'Informe a Bandeira!';
		}
		
		//Verifica se a data inicio do Processo eh menor que a data atual
		if ( isset($data['ID_JOB']) && is_numeric($data['ID_JOB']) ){
		}
		else{
			if ( mktime(0, 0, 0, $mesInicioProcesso, $diaInicioProcesso, $anoInicioProcesso) < mktime( 0, 0, 0, date('m'), date('d'), date('Y')) ) {
				return 'A data de inicio do processo job não pode ser menor que a data atual';
			}
		}
		
		//Verifica se data inicio do job eh menor ou igual a data inicio do processo
		if ( mktime(0, 0, 0, $mesInicio, $diaInicio, $anoInicio) <= mktime(0, 0, 0,	$mesInicioProcesso, $diaInicioProcesso, $anoInicioProcesso) ) {
			return 'A data de inicio do job não pode ser menor ou igual a data de inicio do processo';
		}
		
		//Busca Processo pelo id Tipo de Peca
		$arrayProcesso = $this->processo->getByTipoPeca($idTipoPeca);

		//Se não encontrou processo para aquele tipo de peca
		if (empty($arrayProcesso)) {
			return 'Não há processo cadastrado para o tipo de peca selecionado';
		}
		
		//Faz a consulta das etapas do processo no banco 
		$arrayEtapas = $this->db
			->select('EP.ID_PROCESSO_ETAPA, EP.ID_ETAPA, EP.QTD_HORAS, EP.ORDEM')
			->select('P.HORA_INICIO, P.HORA_FINAL, P.CONSIDERAR_DIAS_UTEIS, P.CONSIDERAR_FERIADO')
			->select('P.*')
			->select('E.DESC_ETAPA')
			->from('TB_PROCESSO P')
			->where('P.ID_PROCESSO', $arrayProcesso['ID_PROCESSO'])
			->join('TB_PROCESSO_ETAPA EP','EP.ID_PROCESSO = P.ID_PROCESSO')
			->join('TB_ETAPA E','E.ID_ETAPA = EP.ID_ETAPA')
			
			->order_by('EP.ORDEM')
			->get();
			
			
		// se encontrou etapas
		if( $arrayEtapas->num_rows() > 0 ){
			
			//array para tratamento dos dados retornados
			$etapas = $arrayEtapas->result_array();
			
			//Hora Inicio
			$horaInicial = $etapas[0]['HORA_INICIO'];
			
			//Hora final
			$horaFinal = $etapas[0]['HORA_FINAL'];
			
			//Considerar Somente dias uteis ?
			$somenteDiasUteis = $etapas[0]['CONSIDERAR_DIAS_UTEIS'];
			
			//Considerar feriados ?
			$considerarFeriados = $etapas[0]['CONSIDERAR_FERIADO'];
			
			//Verifica se a data informada eh util para o processo 
			if ( strtoupper($somenteDiasUteis) == 'S' ) {
				if ( strftime('%w', mktime( 0, 0, 0, $mesInicioProcesso, $diaInicioProcesso, $anoInicioProcesso ) ) == 6	||	//Sabado
					 strftime('%w', mktime( 0, 0, 0, $mesInicioProcesso, $diaInicioProcesso, $anoInicioProcesso ) ) == 0 ) {	//Domingo
					//Dia nao util
					return 'A data de ínicio do processo não é util!';
				}
			}
			
			//Verifica se teremos que considerar os feriados como dia de trabalho
			if ( strtoupper($considerarFeriados) == 'N' ) {
				$feriados = $this->calendario->getFeriadosByProduto($idProduto);
				if (in_array( mktime( 0, 0, 0, $mesInicioProcesso, $diaInicioProcesso, $anoInicioProcesso ), $feriados) ) {
					//Feriado
					return 'A data de ínicio do processo é feriado!';
				}
			}
		
			//Formata Data de Inicio do Processo para chamar rotina de calculo de horas uteis
			$horasMinutosPeriodoUtilInicial = explode(':', $horaInicial );
			
			//Acumulador de data/hora das etapas do processo/job
			//Inicia com a data inicio do processo
			$dataAcumulada = mktime( 	$horasMinutosPeriodoUtilInicial[0], 
										$horasMinutosPeriodoUtilInicial[1], 
										0, 
										$mesInicioProcesso,
										$diaInicioProcesso,
										$anoInicioProcesso );
							
			//Para cada Etapa
			foreach ($etapas as $etapa) {
				
				//Calcula Data Final de cada Etapa
				$dataAcumulada = horasUteis(	$dataAcumulada, 
												$etapa['QTD_HORAS'], 
												$horaInicial,
												$horaFinal,
												$somenteDiasUteis,
												$considerarFeriados,
												$feriados );
			}
		} else {
			return 'Não há etapas cadastradas para este processo';
		}
	
		//Somamos 1 minuto na data/hora acumulada para que este calcule o proximo dia util e em seguida subtraimos esse 1 minuto
		$dataAcumulada = horasUteis(	$dataAcumulada, 
										'0:01', 
										$horaInicial,
										$horaFinal,
										$somenteDiasUteis,
										$considerarFeriados,
										$feriados );
		//Subtraimos 1 minutos em segundos.
		$dataAcumulada -= 60;
														
			//Consiste data inicio recebida com a data inicio calculada
		if ( $data['DATA_INICIO'] != date('d/m/Y',$dataAcumulada) ) {
			return 'A data inicio validade é diferente da data inicio validade calculada para o processo!';
		}
			
		// se chegou ate aqui, e porque esta tudo certo
		return true;
	}

	public function checaAcentuacao($data){
		if( preg_match('@[\\\/\^\?\:|*\(\)\%\$\#\@\!\+\=\,\.\;]+@', $data['TITULO_JOB']) ){
			return 'O título do job não pode conter caracteres especiais';
		}

		return true;
	}

	/**
	 * (non-PHPdoc)
	 * @see system/application/libraries/GenericModel#getById($id)
	 */
	public function getById($id){
				
		$rs = $this->db->from($this->tableName.' J')
		
                  //Demais campos das demais tabelas
				  ->select('C.*, P.*, CL.*, A.*, S.*, E.*, TJ.*, TP.*, ET.*, EP.*, U.*')

                  //Campos da tabela JOB
				  ->select('J.ID_JOB, J.ID_CAMPANHA, J.TITULO_JOB, J.LARGURA_JOB, J.ALTURA_JOB, J.DATA_INICIO_PROCESSO, J.DATA_TERMINO, J.DATA_INICIO')
				  ->select('J.DATA_FINALIZADO, J.ID_CLASSIFICACAO, J.STATUS_JOB, J.ID_TIPO_JOB, J.ID_ETAPA, J.PAGINAS_JOB, J.ID_STATUS, J.ID_TIPO_PECA')
				  ->select('J.ID_AGENCIA, J.ID_CLIENTE, J.ID_REGIAO, J.ALTERACOES_JOB, J.ID_USUARIO_AUTOR, J.ID_PROCESSO, J.ID_PROCESSO_ETAPA')
				  ->select('J.ID_USUARIO_RESPONSAVEL, J.DT_DELAY_BLOQUEIO')
				  
				  //Campos da tabela USUARIO
                  ->select('UR.NOME_USUARIO as NOME_USUARIO_RESPONSAVEL')
                  
                  ->select('(SELECT COUNT(*) FROM TB_APROVACAO AP WHERE AP.ID_JOB = J.ID_JOB AND ENVIADO_APROVACAO = 1) AS PROVAS')
                  ->join('TB_CAMPANHA C', 'C.ID_CAMPANHA = J.ID_CAMPANHA')
                  ->join('TB_PRODUTO P', 'P.ID_PRODUTO = C.ID_PRODUTO')
                  ->join('TB_CLIENTE CL', 'CL.ID_CLIENTE = J.ID_CLIENTE')
                  ->join('TB_AGENCIA A', 'A.ID_AGENCIA = J.ID_AGENCIA')
                  ->join('TB_STATUS S', 'S.ID_STATUS = J.ID_STATUS','LEFT')
                  ->join('TB_EXCEL E', 'E.ID_JOB = J.ID_JOB','LEFT')
                  ->join('TB_TIPO_JOB TJ', 'TJ.ID_TIPO_JOB = J.ID_TIPO_JOB')
                  ->join('TB_TIPO_PECA TP', 'TP.ID_TIPO_PECA = J.ID_TIPO_PECA')
                  //->join('TB_ETAPA ET', 'ET.ID_ETAPA = J.ID_ETAPA','LEFT') - Linha retirada quando entrou o processo dinamico
                  ->join('TB_ETAPA ET', 'ET.ID_ETAPA = J.ID_ETAPA')
                  ->join('TB_PROCESSO_ETAPA EP', 'EP.ID_PROCESSO_ETAPA = J.ID_PROCESSO_ETAPA', 'LEFT')
                  ->join('TB_USUARIO U', 'U.ID_USUARIO = J.ID_USUARIO_AUTOR','LEFT')
                  ->join('TB_USUARIO UR', 'UR.ID_USUARIO = J.ID_USUARIO_RESPONSAVEL','LEFT')
                  ->where('J.ID_JOB', $id)
                  ->get();
		return $rs->row_array();
	}

	/**
	 * Lista os itens cadastrados
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $filters array com os filtros
	 * @param int $page numero da pagina
	 * @param int $limit numero de itens por pagina
	 * @return array
	 */
	public function listItems($filters, $page=0, $limit=5, $order = 'TB_JOB.ID_JOB', $direction = 'DESC',$verTodos = true){
		$this->db
			->select('*')
			->select('(SELECT DATA_HISTORICO FROM TB_JOB_HISTORICO H WHERE H.ID_JOB=TB_JOB.ID_JOB ORDER BY ID_JOB_HISTORICO DESC LIMIT 1) AS DATA_MUDANCA_ETAPA')
			->select('(SELECT COUNT(*) FROM TB_APROVACAO AP WHERE AP.ID_JOB = TB_JOB.ID_JOB AND ENVIADO_APROVACAO = 1) AS PROVAS')
			->join('TB_CAMPANHA C', 'C.ID_CAMPANHA = TB_JOB.ID_CAMPANHA')
			->join('TB_PRODUTO P', 'P.ID_PRODUTO = C.ID_PRODUTO')
			->join('TB_CLIENTE CL', 'CL.ID_CLIENTE = TB_JOB.ID_CLIENTE')
			->join('TB_AGENCIA A', 'A.ID_AGENCIA = TB_JOB.ID_AGENCIA')
			->join('TB_EXCEL E', 'E.ID_JOB = TB_JOB.ID_JOB','LEFT')
			->join('TB_STATUS S', 'S.ID_STATUS = TB_JOB.ID_STATUS','LEFT')
			->join('TB_ETAPA ET', 'ET.ID_ETAPA = TB_JOB.ID_ETAPA','LEFT')
			;
		//echo $this->db->last_query();die;
        if (!$verTodos) {
            $user = Sessao::get('usuario');
            $this->db->join('TB_JOB_USUARIOS JB', 'JB.ID_JOB = TB_JOB.ID_JOB');
            $this->db->join('TB_USUARIO U', 'U.ID_USUARIO = JB.ID_USUARIO_RESPONSAVEL','LEFT');
            $this->db->where_in('JB.ID_USUARIO_RESPONSAVEL',array($user['ID_USUARIO'],'is null',0));
        }

		if(!empty($filters['ID_AGENCIA'])){
			$this->db->where('A.ID_AGENCIA',$filters['ID_AGENCIA']);
		}

		if(!empty($filters['STATUS_CAMPANHA']) && $filters['STATUS_CAMPANHA'] == 1){
			$this->db->where('C.STATUS_CAMPANHA',$filters['STATUS_CAMPANHA'])
				->where('C.DT_FIM_CAMPANHA >= CURRENT_DATE');
		}

		if( !empty($filters['PRODUTOS']) ){
			$this->db->where_in('P.ID_PRODUTO',$filters['PRODUTOS']);
		}

		if( !empty($filters['ID_PRODUTO']) ){
			$this->db->where('P.ID_PRODUTO',$filters['ID_PRODUTO']);
		}

		if(isset($filters['ID_ETAPA']) && is_array($filters['ID_ETAPA'])){
			$this->db->where_in('TB_JOB.ID_ETAPA',$filters['ID_ETAPA']);
			unset($filters['ID_ETAPA']);
		}

		if(!empty($filters['ID_CLIENTE'])){
			$this->db->where('TB_JOB.ID_CLIENTE',$filters['ID_CLIENTE']);
		}

		if( !empty($filters['DATA_INICIO']) ){
			$filters['DATA_INICIO'] = format_date($filters['DATA_INICIO']);
		}

		if( !empty($filters['DATA_INICIO_PROCESSO']) ){
			$filters['DATA_INICIO_PROCESSO'] = format_date($filters['DATA_INICIO_PROCESSO']);
		}

		if( !empty($filters['NOT_ID_STATUS']) ) {
			$this->db->where_not_in('TB_JOB.ID_STATUS',$filters['NOT_ID_STATUS']);
			unset($filters['NOT_ID_STATUS']);
		} 
		$this->setWhereParams($filters);

		return $this->execute($page, $limit, $order, $direction);
	}


	/**
	 * Retorna a pasta de um Job
	 *
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idJob codigo do job
	 * @return string
	 */
	public function getPath($idJob){
		$rs = $this->getById($idJob);
		$path = $this->campanha->getPath($rs['ID_CAMPANHA']);
		exit;
		$path .= $rs['TITULO_JOB'] . '/';
		return $path;
	}

	/**
	 * salva o plano de midia
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param data $_POST
	 * @param int $id
	 * @return void
	 */
	public function savePlanoMidia($data, $id){
		$data['ID_ETAPA'] = $this->etapa->getIdByKey('SEL_MIDIA');
		$data['DATA_INICIO_PROCESSO'] = format_date_to_db($data['DATA_INICIO_PROCESSO']);
		$data['DATA_INICIO'] = format_date_to_db($data['DATA_INICIO']);
		$data['DATA_TERMINO'] = format_date_to_db($data['DATA_TERMINO']);

		// salva o job
		$id = $this->save($data, $id);

		// recupera o excel
		$excel = $this->excel->getByJob($id);
		//echo $this->db->last_query();
		//print_rr($excel);die;

		$idexcel = null;
		if(!empty($excel)){
			$idexcel = $excel['ID_EXCEL'];
		}

		// salva o excel
		$xls = array(
			//'NUMERO_JOB_EXCEL' => $data['NUMERO_JOB_EXCEL'],
			'NOVO_EXCEL' => 1,
			'ID_TEMPLATE' => null,
			'ID_JOB' => $id,
			'DESC_EXCEL' => $data['TITULO_JOB'],
			'OBSERVACOES_EXCEL' => $data['OBSERVACOES_EXCEL'],
			'DT_CADASTRO_EXCEL' => date('Y-m-d H:i:s'),
		);

		$idexcel = $this->excel->save($xls, $idexcel);

		// versao do excel
		$versao = $this->excelVersao->getVersaoAtual($idexcel);
		// se nao achou, cria uma nova
		if(empty($versao)){
			$versao = $this->excelVersao->getNewVersion($idexcel);

		} else {
			$versao = $versao['ID_EXCEL_VERSAO'];
		}

		// salvando as pracas
		if(!empty($data['pracas'])){
			foreach($data['pracas'] as $idpraca){
				$this->excel->saveExcelPraca($idexcel, $idpraca, $versao);
				//echo $this->db->last_query();die;
			}
		//print_rr($versao);die;

			// remove as pracas que nao estiverem selecionadas
			$this->db
				->where('ID_EXCEL_VERSAO',$versao)
				->where('ID_EXCEL',$idexcel)
				->where_not_in('ID_PRACA', $data['pracas'])
				->delete('TB_EXCEL_PRACA');

		// se nao selecionou nenhuma praca
		} else {
			// remove as pracas que nao estiverem selecionadas
			$this->db
				->where('ID_EXCEL_VERSAO',$versao)
				->where('ID_EXCEL',$idexcel)
				->delete('TB_EXCEL_PRACA');
		}

		// salvando os valores das categorias
		if(!empty($data['qtd'])){
			$this->limpaCategorias($idexcel, $versao);

			// lista de itens a serem adicionados
			$itens = array();

			// para cada quantidade encontrada
			foreach($data['qtd'] as $praca => $item){
				// se a praca foi desmarcada
				if( !empty($data['pracas']) && !in_array($praca, $data['pracas']) ){
					continue;
				}


				// para cada categoria
				foreach($item as $subcategoria => $pg){

					foreach($pg as $pagina => $qtd){
						$itens[] = sprintf('(%d, %d, %d, %d, %d, %d)',
							$idexcel, $subcategoria, $versao, $qtd, $praca, $pagina
						);
					}
				}
			}

			//print_rr($itens);die;
			// se tiver itens
			if( !empty($itens) ) {
				$str = 'INSERT INTO TB_EXCEL_CATEGORIA (ID_EXCEL, ID_SUBCATEGORIA,'.
					'ID_EXCEL_VERSAO, QTDE_EXCEL_CATEGORIA, ID_PRACA, PAGINA_EXCEL_CATEGORIA) VALUES ';

				$str .= implode(', '.PHP_EOL, $itens);

				// insere os valores
				$this->db->query($str);
			}


		}

		// retorna o numero do job
		return $id;
	}
	
	public function savePlanoMidiaReaproveitar($data, $id_antigo){
		$data['ID_ETAPA'] = $this->etapa->getIdByKey('SEL_MIDIA');
		$data['DATA_INICIO_PROCESSO'] = format_date_to_db($data['DATA_INICIO_PROCESSO']);
		$data['DATA_INICIO'] = format_date_to_db($data['DATA_INICIO']);
		$data['DATA_TERMINO'] = format_date_to_db($data['DATA_TERMINO']);
		unset($data['ID_JOB']);
	
		// salva o job
		$id = $this->save($data);
		// recupera o excel
		$excel = $this->excel->getByJob($id_antigo);
		$excel_antigo = $excel['ID_EXCEL'];
	
		// salva o excel
		$xls = array(
				//'NUMERO_JOB_EXCEL' => $data['NUMERO_JOB_EXCEL'],
				'NOVO_EXCEL' => 1,
				'ID_TEMPLATE' => null,
				'ID_JOB' => $id,
				'DESC_EXCEL' => $data['TITULO_JOB'],
				'OBSERVACOES_EXCEL' => $data['OBSERVACOES_EXCEL'],
				'DT_CADASTRO_EXCEL' => date('Y-m-d H:i:s'),
		);
		$idexcel = $this->excel->save($xls);
		
		$versao_antiga = $this->excelVersao->getVersaoAtual($excel['ID_EXCEL']);
		
		$id_versao_antiga = $versao_antiga['ID_EXCEL_VERSAO'];
	
		// versao do excel
		$versao = $this->excelVersao->getNewVersion($idexcel);
	
		$excel_item = array();
		// salvando as pracas
		if(!empty($data['pracas'])){
			foreach($data['pracas'] as $idpraca){
				$this->excel->saveExcelPraca($idexcel, $idpraca, $versao);
			}
	
			// remove as pracas que nao estiverem selecionadas
			$this->db
			->where('ID_EXCEL_VERSAO',$versao)
			->where('ID_EXCEL',$idexcel)
			->where_not_in('ID_PRACA', $data['pracas'])
			->delete('TB_EXCEL_PRACA');
	
			// se nao selecionou nenhuma praca
		} else {
			// remove as pracas que nao estiverem selecionadas
			$this->db
			->where('ID_EXCEL_VERSAO',$versao)
			->where('ID_EXCEL',$idexcel)
			->delete('TB_EXCEL_PRACA');
		}
		
		$sql = "INSERT INTO TB_EXCEL_ITEM 
					(ID_EXCEL, ID_PRACA, ID_FICHA, ID_CENARIO, CODIGO_ITEM, DESCRICAO_ITEM, STATUS_ITEM, ORDEM_ITEM, 
					PAGINA_ITEM, ID_EXCEL_VERSAO, TEXTO_LEGAL, SUBCATEGORIA, DISPONIVEL_ITEM, OBSERVACOES_ITEM, 
					EXTRA1_ITEM, EXTRA2_ITEM, EXTRA3_ITEM, EXTRA4_ITEM, EXTRA5_ITEM, ID_FICHA_PAI)
					(SELECT $idexcel, ID_PRACA, ID_FICHA, ID_CENARIO, CODIGO_ITEM, DESCRICAO_ITEM, STATUS_ITEM, ORDEM_ITEM, 
					PAGINA_ITEM, $versao, TEXTO_LEGAL, SUBCATEGORIA, DISPONIVEL_ITEM, OBSERVACOES_ITEM, 
					EXTRA1_ITEM, EXTRA2_ITEM, EXTRA3_ITEM, EXTRA4_ITEM, EXTRA5_ITEM, ID_FICHA_PAI
					FROM TB_EXCEL_ITEM WHERE ID_EXCEL = $excel_antigo)";
		
		$this->db->query($sql);
	
		$pricing = array();
		if(!empty($data['pracas'])){
			foreach($data['pracas'] as $idpraca){
				
				$excel_item_antigo = $this->excelItem->getByExcelPracaVersao($excel_antigo,$idpraca,$versao_antiga['ID_EXCEL_VERSAO']);
				$excel_item_novo = $this->excelItem->getByExcelPracaVersao($idexcel,$idpraca,$versao);
			}
			foreach ($excel_item_antigo as $key => $id_excel_antigo) {
				$pricing[] = $this->pricing->getByItem($id_excel_antigo['ID_EXCEL_ITEM']);
				
				$sql = "INSERT INTO TB_PRICING 
							(ID_EXCEL_ITEM, PRECO_UNITARIO, TIPO_UNITARIO, PRECO_CAIXA, 
							TIPO_CAIXA, PRECO_VISTA, PRECO_DE, PRECO_POR, ECONOMIZE, ENTRADA, 
							PARCELAS, PRESTACAO, JUROS_AM, JUROS_AA, TOTAL_PRAZO, OBSERVACOES)
							VALUES
							('".$excel_item_novo[$key]['ID_EXCEL_ITEM']."','".$pricing[$key]['PRECO_UNITARIO']."','".$pricing[$key]['TIPO_UNITARIO']."',
							'".$pricing[$key]['PRECO_CAIXA']."','".$pricing[$key]['TIPO_CAIXA']."','".$pricing[$key]['PRECO_VISTA']."',
							'".$pricing[$key]['PRECO_DE']."','".$pricing[$key]['PRECO_POR']."','".$pricing[$key]['ECONOMIZE']."',
							'".$pricing[$key]['ENTRADA']."','".$pricing[$key]['PARCELAS']."','".$pricing[$key]['PRESTACAO']."',
							'".$pricing[$key]['JUROS_AM']."','".$pricing[$key]['JUROS_AA']."','".$pricing[$key]['TOTAL_PRAZO']."',
							'".$pricing[$key]['OBSERVACOES']."')";
				
				$this->db->query($sql);
			}
		}
		
		// salvando os valores das categorias
		if(!empty($data['qtd'])){
			$this->limpaCategorias($idexcel, $versao);
	
			// lista de itens a serem adicionados
			$itens = array();
	
			// para cada quantidade encontrada
			foreach($data['qtd'] as $praca => $item){
				// se a praca foi desmarcada
				if( !empty($data['pracas']) && !in_array($praca, $data['pracas']) ){
					continue;
				}
	
	
				// para cada categoria
				foreach($item as $subcategoria => $pg){
	
					foreach($pg as $pagina => $qtd){
						$itens[] = sprintf('(%d, %d, %d, %d, %d, %d)',
								$idexcel, $subcategoria, $versao, $qtd, $praca, $pagina
						);
					}
				}
			}
	
			//print_rr($itens);die;
			// se tiver itens
			if( !empty($itens) ) {
				$str = 'INSERT INTO TB_EXCEL_CATEGORIA (ID_EXCEL, ID_SUBCATEGORIA,'.
						'ID_EXCEL_VERSAO, QTDE_EXCEL_CATEGORIA, ID_PRACA, PAGINA_EXCEL_CATEGORIA) VALUES ';
	
				$str .= implode(', '.PHP_EOL, $itens);
	
				// insere os valores
				$this->db->query($str);
			}
	
	
		}
	
		// retorna o numero do job
		return $id;
	}

	/**
	 * recupera os dados das pracas
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idjob Codigo do job
	 * @return array
	 */
	public function getPracas($idjob){
		$rs = $this->db->from('TB_PRACA P')
			->join('TB_EXCEL_PRACA EP','EP.ID_PRACA = P.ID_PRACA')
			->join('TB_EXCEL E','E.ID_EXCEL = EP.ID_EXCEL')
			->where('E.ID_JOB',$idjob)
			->where('EP.ID_EXCEL_VERSAO = (SELECT ID_EXCEL_VERSAO FROM TB_EXCEL_VERSAO WHERE ID_EXCEL = EP.ID_EXCEL ORDER BY ID_EXCEL_VERSAO DESC LIMIT 1)')
			->get();

		return $rs->result_array();
	}

	/**
	 * remove os valores de categorias de uma determinada versao
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idexcel
	 * @param int $versao
	 * @return void
	 */
	public function limpaCategorias($idexcel, $versao){
		$this->db
			->where('ID_EXCEL', $idexcel)
			->where('ID_EXCEL_VERSAO', $versao)
			->delete('TB_EXCEL_CATEGORIA');
	}

	/**
	 * Altera o status de um job
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idjob
	 * @param int $idstatus
	 * @return void
	 */
	public function setStatus($idjob, $idstatus){
		$this->db
			->where('ID_JOB', $idjob)
			->update('TB_JOB', array('ID_STATUS' => $idstatus));
	}

	/**
	 * Recupera o historico de aprovacao de um job
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idjob Codigo do job
	 * @return array
	 */
	public function getHistoricoAprovacao($idjob){
		$rs = $this->db
			->select('U.*, E.*, S.*, H.*, C.DESC_CLIENTE, A.DESC_AGENCIA')
			->join('TB_USUARIO U','U.ID_USUARIO = H.ID_USUARIO', 'LEFT')
			->join('TB_GRUPO G','G.ID_GRUPO = U.ID_GRUPO', 'LEFT')
			->join('TB_CLIENTE C','C.ID_CLIENTE = G.ID_CLIENTE', 'LEFT')
			->join('TB_AGENCIA A','A.ID_AGENCIA = G.ID_AGENCIA', 'LEFT')
			->join('TB_ETAPA E','E.ID_ETAPA = H.ID_ETAPA')
			->join('TB_STATUS S','S.ID_STATUS = H.ID_STATUS', 'LEFT')

			->where('H.ID_JOB', $idjob)
			->order_by('H.DATA_HISTORICO')
			->get('TB_JOB_HISTORICO H')

			->result_array();

		return $rs;
	}

	/**
	 * Recupera o historico de aprovacao de um job
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idjob Codigo do job
	 * @return array
	 */
	public function getHistoricoPraca($idjob){
		$rs = $this->db
			->select('U.*, P.*, E.*, H.DATA_HISTORICO, H.ACAO,C.DESC_CLIENTE, A.DESC_AGENCIA')
			->join('TB_USUARIO U','U.ID_USUARIO = H.ID_USUARIO', 'LEFT')
			->join('TB_GRUPO G','G.ID_GRUPO = U.ID_GRUPO', 'LEFT')
			->join('TB_CLIENTE C','C.ID_CLIENTE = G.ID_CLIENTE', 'LEFT')
			->join('TB_AGENCIA A','A.ID_AGENCIA = G.ID_AGENCIA', 'LEFT')
			->join('TB_ETAPA E','E.ID_ETAPA = H.ID_ETAPA')
			->join('TB_PRACA P','P.ID_PRACA = H.ID_PRACA', 'LEFT')

			->where('H.ID_JOB', $idjob)
			->order_by('H.DATA_HISTORICO')
			->get('TB_JOB_HISTORICO_PRACA H')

			->result_array();

		return $rs;
	}

	/**
	 * Adiciona um item no historico de aprovacao
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idjob
	 * @param int $idstatus
	 * @param int $idetapa
	 * @param string $comentario
	 * @return void
	 */
	public function addHistorico($idjob, $idstatus, $idetapa, $comentario){
		$user = Sessao::get('usuario');

		$data = array(
			'ID_USUARIO' => $user['ID_USUARIO'],
			'ID_JOB' => $idjob,
			'ID_STATUS' => $idstatus,
			'ID_ETAPA' => $idetapa,
			'COMENTARIO_JOB_APROVACAO' => $comentario,
			'DATA_JOB_APROVACAO' => date('Y-m-d H:i:s')
		);

		$this->db->insert('TB_JOB_APROVACAO', $data);
	}

	/**
	 * Adiciona um item no historico de alterações de produtos do job
	 *
	 * @author Diego Andrade
	 * @link http://www.247id.com.br
	 * @param int $idjob
	 * @param int $idpraca
	 * @param string $acao
	 * @return void
	 */
	public function addHistoricoPraca($idjob, $idpraca, $acao = ""){
		$user = Sessao::get('usuario');
		$job = $this->getNomeById($idjob);
		$idetapa = 1;
		foreach ($job as $j) {
			$idetapa = $j['ID_ETAPA'];
		}
		$data = array(
			'ID_USUARIO' => $user['ID_USUARIO'],
			'ID_JOB' => $idjob,
			'ID_PRACA' => $idpraca,
			'ID_ETAPA' => $idetapa,
			'ACAO' => $acao,
			'DATA_HISTORICO' => date('Y-m-d H:i:s')
		);

		$this->db->insert('TB_JOB_HISTORICO_PRACA', $data);
	}

	/**
	 * Recuperas todas as fichas de todas as pracas, removendo duplicados
	 *
	 * <p>Cada item traz um array chamado <pracas> para indicar
	 * em quais pracas ele esta presente</p>
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idjob Codigo do job
	 * @return array
	 */
	public function getFichasUnicas($idjob, $offset = null, $limit = null){
		$idjob = sprintf('%d', $idjob);

		$sub = '(SELECT ID_EXCEL_VERSAO
			FROM TB_EXCEL_VERSAO
			JOIN TB_EXCEL USING(ID_EXCEL)
			WHERE TB_EXCEL.ID_EXCEL = E.ID_EXCEL
			AND TB_EXCEL.ID_JOB = '.$idjob.'
			ORDER BY ID_EXCEL_VERSAO DESC LIMIT 1)';

		$pracas = $this->getPracas( $idjob );

		////////////////////////////////////////////////////////////
		// POR CONTA DO CODEIGNITER TER UM BUG NO METODO SELECT
		// (MAIS PROPRIAMENTE NO CI_DB_active_record::_merge_cache
		// VAMOS FAZER A QUERY NA MAO
		////////////////////////////////////////////////////////////
		$fields = array();
		$fields[] = 'C.DESC_CATEGORIA';
		$fields[] = 'SC.DESC_SUBCATEGORIA';
		$fields[] = 'F.ID_FICHA, F.NOME_FICHA, F.COD_BARRAS_FICHA, F.PENDENCIAS_DADOS_BASICOS';
		$fields[] = 'I.*';
		$fields[] = 'E.*';
		$fields[] = 'O.*';
		$fields[] = 'C.ID_CATEGORIA, SC.ID_SUBCATEGORIA';
		$fields[] = 'IF( SUM(IF(I.STATUS_ITEM = 1, 1, 0)) > 0, 1, 0 ) AO_MENOS_UM';

		foreach($pracas as $praca){
			$fields[] =sprintf(
					'if( sum(if(I.ID_PRACA = %1$d AND I.STATUS_ITEM = 1, 1, 0)) > 0, "1", "0") PRACA_%1$d'
					, $praca['ID_PRACA']
				);

			$fields[] = sprintf(
					'if( sum(if(
						(I.ID_PRACA = %1$d AND I.DISPONIVEL_ITEM = 1)
							OR (SELECT COUNT(*) FROM TB_EXCEL_ITEM WHERE ID_PRACA=%1$d AND ID_FICHA = I.ID_FICHA AND ID_EXCEL_VERSAO = I.ID_EXCEL_VERSAO) = 0 , 1, 0)) > 0, 1, 0) DISPONIVEL_%1$d'
					, $praca['ID_PRACA']
				);
		}

		$str_limit = '';
		if( !empty($limit) ){
			$str_limit = sprintf('LIMIT %d, %d', $offset, $limit);
		} else if( !empty($offset) ) {
			$str_limit = sprintf('LIMIT %d', $offset);
		}

		$sql = 'SELECT ' . implode(', '.PHP_EOL, $fields) . PHP_EOL
			. ' FROM TB_EXCEL_ITEM I ' . PHP_EOL
			. ' JOIN TB_EXCEL E ON E.ID_EXCEL = I.ID_EXCEL ' . PHP_EOL
			. ' JOIN TB_FICHA F ON F.ID_FICHA = I.ID_FICHA ' . PHP_EOL
			. ' LEFT JOIN TB_SUBCATEGORIA SC ON SC.ID_SUBCATEGORIA = F.ID_SUBCATEGORIA ' . PHP_EOL
			. ' LEFT JOIN TB_CATEGORIA C ON C.ID_CATEGORIA = F.ID_CATEGORIA ' . PHP_EOL
			. ' LEFT JOIN TB_OBJETO_FICHA OF ON OF.ID_FICHA = F.ID_FICHA AND OF.PRINCIPAL = 1 ' . PHP_EOL
			. ' LEFT JOIN TB_OBJETO O ON O.ID_OBJETO = OF.ID_OBJETO ' . PHP_EOL
			. ' WHERE I.ID_EXCEL_VERSAO = ' . $sub . PHP_EOL
			. ' AND (I.ID_FICHA_PAI = 0 OR isnull(I.ID_FICHA_PAI)) ' . PHP_EOL
			. ' AND E.ID_JOB = ' . sprintf('%d', $idjob) . PHP_EOL
			//. ' AND I.STATUS_ITEM = 1 ' . PHP_EOL
			. ' GROUP BY I.ID_FICHA ' . PHP_EOL
			. ' HAVING AO_MENOS_UM = 1 ' . PHP_EOL
			. ' ORDER BY I.ID_FICHA_PAI, F.NOME_FICHA ' . PHP_EOL
			. $str_limit;

		$rs = $this->db->query($sql)->result_array();

		$result = array();

		foreach($rs as $item){
			$item['INDISPONIVEL'] = array();
			$item['PRACAS'] = array();

			foreach($pracas as $praca){
				if( $item['PRACA_'.$praca['ID_PRACA']] == 1 ){
					$item['PRACAS'][] = $praca['ID_PRACA'];
				}

				if( $item['DISPONIVEL_'.$praca['ID_PRACA']] == 0 ){
					$item['INDISPONIVEL'][] = $praca['ID_PRACA'];
				}
			}

			$result[] = $item;
		}

		return $result;
	}
	
	public function getFichasUnicasSemItem($idjob){
		$idjob = sprintf('%d', $idjob);

		$sub = '(SELECT ID_EXCEL_VERSAO
			FROM TB_EXCEL_VERSAO
			JOIN TB_EXCEL USING(ID_EXCEL)
			WHERE TB_EXCEL.ID_EXCEL = E.ID_EXCEL
			AND TB_EXCEL.ID_JOB = '.$idjob.'
			ORDER BY ID_EXCEL_VERSAO DESC LIMIT 1)';

		$sql = 'SELECT COUNT(1)'
			. ' FROM TB_EXCEL_ITEM I ' . PHP_EOL
			. ' JOIN TB_EXCEL E ON E.ID_EXCEL = I.ID_EXCEL ' . PHP_EOL
			. ' JOIN TB_FICHA F ON F.ID_FICHA = I.ID_FICHA ' . PHP_EOL
			. ' LEFT JOIN TB_SUBCATEGORIA SC ON SC.ID_SUBCATEGORIA = F.ID_SUBCATEGORIA ' . PHP_EOL
			. ' LEFT JOIN TB_CATEGORIA C ON C.ID_CATEGORIA = F.ID_CATEGORIA ' . PHP_EOL
			. ' LEFT JOIN TB_OBJETO_FICHA OF ON OF.ID_FICHA = F.ID_FICHA AND OF.PRINCIPAL = 1 ' . PHP_EOL
			. ' LEFT JOIN TB_OBJETO O ON O.ID_OBJETO = OF.ID_OBJETO ' . PHP_EOL
			. ' WHERE I.ID_EXCEL_VERSAO = ' . $sub . PHP_EOL
			. ' AND (I.ID_FICHA_PAI = 0 OR isnull(I.ID_FICHA_PAI)) ' . PHP_EOL
			. ' AND E.ID_JOB = ' . sprintf('%d', $idjob) . PHP_EOL
			//. ' AND I.STATUS_ITEM = 1 ' . PHP_EOL
			. ' GROUP BY I.ID_FICHA ' . PHP_EOL
			. ' ORDER BY I.ID_FICHA_PAI, F.NOME_FICHA ' . PHP_EOL;

		$rs = $this->db->query($sql);
		$rs = $rs->num_rows();
		
		return $rs;
	}	


	/**
	 * Recuperas todas as fichas de todas as pracas, removendo duplicados
	 *
	 * <p>Cada item traz um array chamado <pracas> para indicar
	 * em quais pracas ele esta presente</p>
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idjob Codigo do job
	 * @return array
	 */
	public function getFichasUnicasFilhas($idjob, $idpai, $offset = null, $limit = null){
		$idjob = sprintf('%d', $idjob);
		$idpai = sprintf('%d', $idpai);

		$sub = '(SELECT ID_EXCEL_VERSAO
			FROM TB_EXCEL_VERSAO
			JOIN TB_EXCEL USING(ID_EXCEL)
			WHERE TB_EXCEL.ID_EXCEL = E.ID_EXCEL
			AND TB_EXCEL.ID_JOB = '.$idjob.'
			ORDER BY ID_EXCEL_VERSAO DESC LIMIT 1)';

		$pracas = $this->getPracas( $idjob );

		////////////////////////////////////////////////////////////
		// POR CONTA DO CODEIGNITER TER UM BUG NO METODO SELECT
		// (MAIS PROPRIAMENTE NO CI_DB_active_record::_merge_cache
		// VAMOS FAZER A QUERY NA MAO
		////////////////////////////////////////////////////////////
		$fields = array('C.ID_CATEGORIA, SC.ID_SUBCATEGORIA');
		$fields[] = 'C.DESC_CATEGORIA';
		$fields[] = 'SC.DESC_SUBCATEGORIA';
		$fields[] = 'F.ID_FICHA, F.NOME_FICHA, F.COD_BARRAS_FICHA,F.PENDENCIAS_DADOS_BASICOS';
		$fields[] = 'I.*';
		$fields[] = 'E.*';
		$fields[] = 'O.*';
		$fields[] = 'SF.*';
		$fields[] = 'CF.LABEL_CAMPO_FICHA, CF.VALOR_CAMPO_FICHA';
		$fields[] = 'IF( SUM(IF(I.STATUS_ITEM = 1, 1, 0)) > 0, 1, 0 ) AO_MENOS_UM';

		foreach($pracas as $praca){
			$fields[] =sprintf(
					'if( sum(if(I.ID_PRACA = %1$d AND I.STATUS_ITEM = 1, 1, 0)) > 0, "1", "0") PRACA_%1$d'
					, $praca['ID_PRACA']
				);

			$fields[] = sprintf(
					'if( sum(if(
						(I.ID_PRACA = %1$d AND I.DISPONIVEL_ITEM = 1)
							OR (SELECT COUNT(*) FROM TB_EXCEL_ITEM WHERE ID_PRACA=%1$d AND ID_FICHA = I.ID_FICHA AND ID_EXCEL_VERSAO = I.ID_EXCEL_VERSAO) = 0 , 1, 0)) > 0, 1, 0) DISPONIVEL_%1$d'
					, $praca['ID_PRACA']
				);
		}

		$str_limit = '';
		if( !empty($limit) ){
			$str_limit = sprintf('LIMIT %d, %d', $offset, $limit);
		} else if( !empty($offset) ) {
			$str_limit = sprintf('LIMIT %d', $offset);
		}

		$sql = 'SELECT ' . implode(', '.PHP_EOL, $fields) . PHP_EOL
			. ' FROM TB_EXCEL_ITEM I ' . PHP_EOL
			. ' JOIN TB_EXCEL E ON E.ID_EXCEL = I.ID_EXCEL ' . PHP_EOL
			. ' JOIN TB_FICHA F ON F.ID_FICHA = I.ID_FICHA ' . PHP_EOL
			. ' LEFT JOIN TB_SUBFICHA SF ON F.ID_FICHA = SF.ID_FICHA2 AND ID_FICHA1 = ' . $idpai . ' ' . PHP_EOL
			. ' LEFT JOIN TB_CAMPO_FICHA CF ON SF.ID_FICHA2 = CF.ID_FICHA AND CF.PRINCIPAL ' . PHP_EOL
			. ' LEFT JOIN TB_SUBCATEGORIA SC ON SC.ID_SUBCATEGORIA = F.ID_SUBCATEGORIA ' . PHP_EOL
			. ' LEFT JOIN TB_CATEGORIA C ON C.ID_CATEGORIA = F.ID_CATEGORIA ' . PHP_EOL
			. ' LEFT JOIN TB_OBJETO_FICHA OF ON OF.ID_FICHA = F.ID_FICHA AND OF.PRINCIPAL = 1 ' . PHP_EOL
			. ' LEFT JOIN TB_OBJETO O ON O.ID_OBJETO = OF.ID_OBJETO ' . PHP_EOL
			. ' WHERE I.ID_EXCEL_VERSAO = ' . $sub . PHP_EOL
			. ' AND I.ID_FICHA_PAI = ' . $idpai . PHP_EOL
			. ' AND E.ID_JOB = ' . sprintf('%d', $idjob) . PHP_EOL
			//. ' AND I.STATUS_ITEM = 1 ' . PHP_EOL
			. ' GROUP BY I.ID_FICHA ' . PHP_EOL
			. ' HAVING AO_MENOS_UM = 1 ' . PHP_EOL
			. ' ORDER BY I.ID_FICHA_PAI, F.NOME_FICHA ' . PHP_EOL
			. $str_limit;

		$rs = $this->db->query($sql)->result_array();

		$result = array();

		foreach($rs as $item){
			$item['INDISPONIVEL'] = array();
			$item['PRACAS'] = array();

			foreach($pracas as $praca){
				if( $item['PRACA_'.$praca['ID_PRACA']] == 1 ){
					$item['PRACAS'][] = $praca['ID_PRACA'];
				}

				if( $item['DISPONIVEL_'.$praca['ID_PRACA']] == 0 ){
					$item['INDISPONIVEL'][] = $praca['ID_PRACA'];
				}
			}

			$result[] = $item;
		}

		return $result;
	}
	
	
	/**
	 * Salva os itens do carrinho
	 *
	 * <p>Desabilita os itens que nao estiverem selecionados</p>
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idjob Codigo do job
	 * @param array $itens Lista de itens
	 * @return void
	 */
	public function saveCarrinho($idjob, array $itens){
		//recupera o excel
		$excel = $this->excel->getByJob($idjob);
		//recupera a versao mais atual do excel
		$versao = $this->excelVersao->getVersaoAtual($excel['ID_EXCEL']);
		//recupera as pracas do job
		$pracas = $this->getPracas($idjob);

		foreach($pracas as $prc){
			// pega o codigo da praca
			$praca = $prc['ID_PRACA'];
			
			// armazena a array pra verificar os cenarios
			$cenarios = $itens[$praca];
			
			// pega os codigos das fichas
			$list = empty($itens[ $praca ]) ? array(0) : array_keys($itens[ $praca ]);
				
			// fichas com os codigos informados
			$fichasCompletas = alteraChaveLista($this->ficha->getByListaId( $list ), 'ID_FICHA');
			
			// pega os itens da praca que estao no banco
			$itensBanco = $this->excelItem->getByVersaoPraca($versao['ID_EXCEL_VERSAO'], $praca, true);

			// fichas adicionadas para a praca
			$fichas = array(0);
			
			// filhas adicionadas para a praca
			$filhas = array(0);
			
			$subfichas = array(0);

			// coloca o ID_FICHA como chave da lista
			$itensBanco = alteraChaveLista($itensBanco,'ID_FICHA');
			
			// lista de insercao
			$insertList = array();

			// lista de atualizacao
			$updateList = array();

			// lista de insercao de pricing
			$pricingList = array();

			// para cada item
			foreach($list as $idficha){
				//pega os itens filhos da praca que estao no banco
				$itensBancoFilhas = $this->excelItem->getFilhasByVersaoPraca($versao['ID_EXCEL_VERSAO'], $praca, $idficha, true);
				// coloca o ID_FICHA como chave da lista
				$itensBancoFilhas = alteraChaveLista($itensBancoFilhas,'ID_FICHA');
				
				//pega os itens subfichas da praca que estao no banco
				$itensBancoSubfichas = $this->excelItem->getSubfichasByVersaoPraca($versao['ID_EXCEL_VERSAO'], $praca, $idficha, true);
				// coloca o ID_FICHA como chave da lista
				$itensBancoSubfichas = alteraChaveLista($itensBancoSubfichas,'ID_FICHA'); 

				// pega o cenario do excel item, caso o usuario tenha escolhido
				$idCenario = 0;
				if( array_key_exists($idficha, $cenarios) ){
					if( (is_array($cenarios[$idficha])) && isset($cenarios[$idficha]['CENARIO']) ){
						$idCenario = $cenarios[$idficha]['CENARIO'];
					}
				}

				// esta na lista de existentes no banco?
				if( array_key_exists($idficha, $itensBanco) ){
					// pega o item
					$dados_ficha = $itensBanco[$idficha];
					
					// coloca o item como ativo
					$dados_ficha['STATUS_ITEM'] = 1;
					$dados_ficha['ID_CENARIO'] = $idCenario;
					
					// coloca na lista de update
					$updateList[] = $dados_ficha;

					// vamos coloca na lista de atualizacao de pricing
					$pricingList[] = $dados_ficha;

				// se nao esta na lista de existentes no banco em itens
				} else {
					// foi encontrada nas fichas completas?
					if( array_key_exists($idficha, $fichasCompletas) ){
						// criamos uma nova ficha
						$dados_ficha = $fichasCompletas[ $idficha ];
						$dados_ficha['ID_CENARIO'] = $idCenario;
						$dados_ficha['ID_PRACA'] = $praca;
						$dados_ficha['ID_FICHA'] = $idficha;
						$dados_ficha['ID_EXCEL'] = $excel['ID_EXCEL'];
						$dados_ficha['ID_EXCEL_VERSAO'] = $versao['ID_EXCEL_VERSAO'];
						$dados_ficha['CODIGO_ITEM'] = $dados_ficha['COD_BARRAS_FICHA'];
						$dados_ficha['DESCRICAO_ITEM'] = $dados_ficha['NOME_FICHA'];
						$dados_ficha['SUBCATEGORIA'] = $dados_ficha['ID_SUBCATEGORIA'];
						$dados_ficha['PAGINA_ITEM'] = 0;
						$dados_ficha['ORDEM_ITEM'] = 0;
						
						// vamos colocar alguns dados
						$dados_ficha['DISPONIVEL_ITEM'] = 1;

						// vamos colocar alguns dados
						$dados_ficha['STATUS_ITEM'] = 1;

						// coloca na lista de inserts
						$insertList[] = $dados_ficha;

					// se nao achou, passa para a proxima
					} else {
						continue;
					}
				}
							
				// colocamos o codigo da ficha para nao zerar
				$fichas[] = $dados_ficha['ID_FICHA'];
				
				if(isset($itens['subfichas'])){
					//verifico se esta ficha tem subfichas
					if(array_key_exists($idficha, $itens['subfichas'])){
						//verifico se tem subfichas pra esta praca
						if(array_key_exists($praca, $itens['subfichas'][$idficha])){
							$subfichas = array_keys($itens['subfichas'][$idficha][$praca]);
							
							// pega os codigos das subfichas
							$listSubfichas = empty($itens['subfichas'][$idficha][ $praca ]) ? array(0) : array_keys($itens['subfichas'][$idficha][ $praca ]);

							// fichas com os codigos informados
							$subfichasCompletas = alteraChaveLista($this->ficha->getByListaId( $listSubfichas ), 'ID_FICHA');

							foreach($subfichas as $idsubficha){
								// esta na lista de existentes no banco?
								if( array_key_exists($idsubficha, $itensBancoSubfichas) ){
									// pega o item
									$dados_ficha = $itensBancoSubfichas[$idsubficha];
	
									// coloca o item como ativo
									$dados_ficha['STATUS_ITEM'] = 1;
									
									// coloca na lista de update
									$updateList[] = $dados_ficha;
				
									// vamos coloca na lista de atualizacao de pricing
									$pricingList[] = $dados_ficha;
				
								// se nao esta na lista de existentes no banco em itens
								} else {
									// foi encontrada nas fichas completas?
									if( array_key_exists($idsubficha, $subfichasCompletas) ){
										// criamos uma nova ficha
										$dados_ficha = $subfichasCompletas[ $idsubficha ];
										$dados_ficha['ID_PRACA'] = $praca;
										$dados_ficha['ID_FICHA'] = $idsubficha;
										$dados_ficha['ID_EXCEL'] = $excel['ID_EXCEL'];
										$dados_ficha['ID_EXCEL_VERSAO'] = $versao['ID_EXCEL_VERSAO'];
										$dados_ficha['CODIGO_ITEM'] = $dados_ficha['COD_BARRAS_FICHA'];
										$dados_ficha['DESCRICAO_ITEM'] = $dados_ficha['NOME_FICHA'];
										$dados_ficha['SUBCATEGORIA'] = $dados_ficha['ID_SUBCATEGORIA'];
										$dados_ficha['ID_FICHA_PAI'] = $idficha;
										$dados_ficha['PAGINA_ITEM'] = 0;
										$dados_ficha['ORDEM_ITEM'] = 0;
										
										// vamos colocar alguns dados
										$dados_ficha['DISPONIVEL_ITEM'] = 1;
				
										// vamos colocar alguns dados
										$dados_ficha['STATUS_ITEM'] = 1;
				
										// coloca na lista de inserts
										$insertList[] = $dados_ficha;
				
									// se nao achou, passa para a proxima
									} else {
										continue;
									}
								}
				
								// colocamos o codigo da ficha para nao zerar
								$subfichas[] = $dados_ficha['ID_FICHA'];
							}
						}
					}
				}

				if(isset($itens['filhas'])){
					//verifico se esta ficha tem filhos
					if(array_key_exists($idficha, $itens['filhas'])){

						//verifico se tem filhos pra esta praca
						if(array_key_exists($praca, $itens['filhas'][$idficha])){
							$fichasfilhas = array_keys($itens['filhas'][$idficha][$praca]);
							
							// pega os codigos das fichas filhas
							$listFilhas = empty($itens['filhas'][$idficha][ $praca ]) ? array(0) : array_keys($itens['filhas'][$idficha][ $praca ]);
				
							// fichas com os codigos informados
							$fichasFilhasCompletas = alteraChaveLista($this->ficha->getByListaId( $listFilhas ), 'ID_FICHA');

							foreach($fichasfilhas as $idfichafilha){
								// esta na lista de existentes no banco?
								if( array_key_exists($idfichafilha, $itensBancoFilhas) ){
									// pega o item
									$dados_ficha = $itensBancoFilhas[$idfichafilha];
	
									// coloca o item como ativo
									$dados_ficha['STATUS_ITEM'] = 1;
									
									// coloca na lista de update
									$updateList[] = $dados_ficha;
				
									// vamos coloca na lista de atualizacao de pricing
									$pricingList[] = $dados_ficha;
				
								// se nao esta na lista de existentes no banco em itens
								} else {
									// foi encontrada nas fichas completas?
									if( array_key_exists($idfichafilha, $fichasFilhasCompletas) ){
										// criamos uma nova ficha
										$dados_ficha = $fichasFilhasCompletas[ $idfichafilha ];
										$dados_ficha['ID_PRACA'] = $praca;
										$dados_ficha['ID_FICHA'] = $idfichafilha;
										$dados_ficha['ID_EXCEL'] = $excel['ID_EXCEL'];
										$dados_ficha['ID_EXCEL_VERSAO'] = $versao['ID_EXCEL_VERSAO'];
										$dados_ficha['CODIGO_ITEM'] = $dados_ficha['COD_BARRAS_FICHA'];
										$dados_ficha['DESCRICAO_ITEM'] = $dados_ficha['NOME_FICHA'];
										$dados_ficha['SUBCATEGORIA'] = $dados_ficha['ID_SUBCATEGORIA'];
										$dados_ficha['ID_FICHA_PAI'] = $idficha;
										$dados_ficha['PAGINA_ITEM'] = 0;
										$dados_ficha['ORDEM_ITEM'] = 0;
										
										// vamos colocar alguns dados
										$dados_ficha['DISPONIVEL_ITEM'] = 1;
				
										// vamos colocar alguns dados
										$dados_ficha['STATUS_ITEM'] = 1;
				
										// coloca na lista de inserts
										$insertList[] = $dados_ficha;
				
									// se nao achou, passa para a proxima
									} else {
										continue;
									}
								}
				
								// colocamos o codigo da ficha para nao zerar
								$filhas[] = $dados_ficha['ID_FICHA'];
							}

							$this->db
								->where_not_in('ID_FICHA', $fichasfilhas)
								->where('ID_EXCEL', $excel['ID_EXCEL'])
								->where('ID_FICHA_PAI', $idficha)
								->where('ID_EXCEL_VERSAO', $versao['ID_EXCEL_VERSAO'])
								->where('ID_PRACA', $praca)
								->update('TB_EXCEL_ITEM', array('STATUS_ITEM' => 0));
						}
						else{
							//caso nao venha nenhuma filha, inativa todas as filhas, caso existam
							$this->db
								->where('ID_FICHA_PAI', $idficha)
								->where('ID_EXCEL_VERSAO', $versao['ID_EXCEL_VERSAO'])
								->where('ID_PRACA', $praca)
								->update('TB_EXCEL_ITEM', array('STATUS_ITEM' => 0));
						}
					}
				}
				else{
					//caso nao venha nenhuma filha, inativa todas as filhas, caso existam
					$this->db
						->where('ID_FICHA_PAI > ', 0)
						->where('ID_EXCEL_VERSAO', $versao['ID_EXCEL_VERSAO'])
						->where('ID_PRACA', $praca)
						->update('TB_EXCEL_ITEM', array('STATUS_ITEM' => 0));
				}
			}

			// atualizando os itens
			if( !empty($updateList) ){
				$this->excelItem->atualizarLista( $updateList );
			}

			// insere a lista de pricing
			if( !empty($pricingList) ) {
				$this->pricing->atualizarLista( $pricingList, true );
			}

			// se tiver itens para inserir
			if( !empty($insertList) ){
				// inserindo lista
				$this->excelItem->atualizarLista( $insertList, true );

			}

			// agora, as fichas que nao estiverem na praca,
			// vamos marcar como inativas
			$this->db
				->where_not_in('ID_FICHA', $fichas)
				->where('ID_EXCEL', $excel['ID_EXCEL'])
				->where('ID_EXCEL_VERSAO', $versao['ID_EXCEL_VERSAO'])
				->where('ID_PRACA', $praca)
				->where('(ID_FICHA_PAI = 0 or ID_FICHA_PAI is null)')
				->update('TB_EXCEL_ITEM', array('STATUS_ITEM' => 0));

			// caso a ficha pai foi inativada, aki inativamos sua fichas filhas
			$this->db
				->where_not_in('ID_FICHA_PAI', $fichas)
				->where('ID_EXCEL', $excel['ID_EXCEL'])
				->where('ID_EXCEL_VERSAO', $versao['ID_EXCEL_VERSAO'])
				->where('ID_PRACA', $praca)
				->update('TB_EXCEL_ITEM', array('STATUS_ITEM' => 0));

			
		}
		// altera o status do job
		$this->setStatus($idjob, $this->status->getIdByKey('EM_APROVACAO'));
	}

	public function RemoveExcelItems($idExcelItem){
				$this->db->where('ID_EXCEL_ITEM', $idExcelItem)
					->delete('TB_EXCEL_ITEM');		
	}
	
	/**
	 * Salva os dados de um carrinho com os dados vindo Excel
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idjob Codigo do job
	 * @param array $data Dados do carrinho vindos do excel
	 * @return void
	 */
	public function saveCarrinhoFromExcel($idjob, array $itens, $tipoImportacao = null){
		$excel = $this->excel->getByJob($idjob);
		$versao = $this->excelVersao->getVersaoAtual($excel['ID_EXCEL']);
		$pracas = $this->getPracas($idjob);

		// se o tipo de importacao for para alterar deleta todas as filhas pois serao adicionadas novamente
		if( ($tipoImportacao == null) || ($tipoImportacao == 'substituir') ){
			foreach($itens as $key=>$value){
				$this->excelItem->deleteFilhasByVersaoPraca($versao['ID_EXCEL_VERSAO'], $key);
			}
		}

		//parara cada praca
		foreach($itens as $idPraca => $praca){
			// para cada item da praca
			foreach($praca as $row){
				// indica o excel
				$row['ID_EXCEL'] = $excel['ID_EXCEL'];
				// indica a praca
				$row['ID_PRACA'] = $idPraca;
				// indica a qual versao pertence
				$row['ID_EXCEL_VERSAO'] = $versao['ID_EXCEL_VERSAO'];
				// status do item
				$row['STATUS_ITEM'] = 1;
				// observacoes do planilha (para gravar na tabela pricing)
				$row['OBSERVACOES'] = $row['OBSERVACOES_ITEM'];
				//calcula e insere as parcelas dinamicas caso existam
				$parcelaDinamica = $this->parcela_dinamica->calculaParcelaDinamica($excel['ID_CLIENTE'], $row['PRECO_VISTA']);
				if(count($parcelaDinamica) > 0){
					$row['PRESTACAO'] = $parcelaDinamica['PRESTACAO'];
					$row['PARCELAS'] = $parcelaDinamica['PARCELAS'];
					$row['PRECO_VISTA'] = $parcelaDinamica['PRECO_VISTA'];
				}		
	
				// limpa observacoes item para nao gravar na planilha excel item
				// este campo deve ser utilizado apenas na logistica.
				unset($row['OBSERVACOES_ITEM']);
				// coloca na lista de itens temporarios
				$tempItens[] = $row;
			}

			$tempSubfichas = array();

			for($i = 0; $i <= count($tempItens) - 1; $i++){
				foreach($tempItens[$i]['SUBFICHAS'] as $s){	
					$parcelaDinamica = $this->parcela_dinamica->calculaParcelaDinamica($excel['ID_CLIENTE'], $s['PRECO_VISTA']);
					if(count($parcelaDinamica) > 0){
						$s['PRESTACAO'] = $parcelaDinamica['PRESTACAO'];
						$s['PARCELAS'] = $parcelaDinamica['PARCELAS'];
					}	
					$tempSubfichas[] = $s;
				}
				unset($tempItens[$i]['SUBFICHAS']);
			}

			if(count($tempItens) > 0 || count($tempSubfichas) > 0){			
				$this->db->where('ID_EXCEL', $excel['ID_EXCEL'])
					->where('ID_EXCEL_VERSAO', $versao['ID_EXCEL_VERSAO'])
					->where('ID_PRACA', $idPraca)
					->delete('TB_EXCEL_ITEM');		
								
				// enquanto houverem itens para serem salvos
				while( !empty($tempItens) ){
					$partial = array_splice($tempItens, 0, 300);
					$partial = $this->excelItem->saveItems($partial);
					$this->pricing->savePricingsExcel($partial);
				}
	
				while( !empty($tempSubfichas) ){
					$partial = array_splice($tempSubfichas, 0, 300);
					$partial = $this->excelItem->saveItems($partial);
					$this->pricing->savePricingsExcel($partial);
				}
			}
		}

		// muda a etapa do job
		$this->setStatus($idjob, $this->status->getIdByKey('EM_APROVACAO'));
	}

	/**
	 * salva os itens de logistica
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $id codigo do job
	 * @param array $itens lista de itens a serem gravados
	 * @return void
	 */
	public function saveLogistica($id, array $itens){
		foreach($itens as $id => $item){
			$this->excelItem->save($item, $id);
		}
	}

	/**
	 * verifica se um job tem itens indisponiveis
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idjob
	 * @return boolean
	 */
	public function hasItemIndisponivel($idjob){
		$excel = $this->excel->getByJob($idjob);
		$versao = $this->excelVersao->getVersaoAtual($excel['ID_EXCEL']);

		$sub = '(SELECT ID_EXCEL_VERSAO
			FROM TB_EXCEL_VERSAO
			JOIN TB_EXCEL USING(ID_EXCEL)
			WHERE TB_EXCEL.ID_EXCEL = E.ID_EXCEL
			AND TB_EXCEL.ID_JOB = '.$idjob.'
			ORDER BY ID_EXCEL_VERSAO DESC LIMIT 1)';

		$total = $this->db
			->from('TB_EXCEL_ITEM I')
			->join('TB_SUBCATEGORIA SC','SC.ID_SUBCATEGORIA = I.SUBCATEGORIA','LEFT')
			->join('TB_CATEGORIA C','C.ID_CATEGORIA = SC.ID_CATEGORIA','LEFT')
			->join('TB_EXCEL E','E.ID_EXCEL = I.ID_EXCEL')
			->join('TB_FICHA F','F.ID_FICHA = I.ID_FICHA')
			->join('TB_OBJETO_FICHA O','O.ID_FICHA = F.ID_FICHA AND O.PRINCIPAL = 1','LEFT')
			->join('TB_PRICING P','P.ID_EXCEL_ITEM = I.ID_EXCEL_ITEM')
			->join('TB_APROVACAO_FICHA AP','AP.ID_APROVACAO_FICHA = F.ID_APROVACAO_FICHA')

			->where('I.ID_EXCEL_VERSAO = ' . $sub)
			->where('ID_JOB', $idjob)
			->where('STATUS_ITEM', 1)
			->where('DISPONIVEL_ITEM',  0)
			->count_all_results();

		return $total > 0;
	}

	/**
	 * Altera a etapa e grava um historico de etapas
	 *
	 * @author Hugo Ferreira da Silva
	 * @author juliano.polito
	 * @author Diego Andrade
	 * @param int $idjob Codigo do job
	 * @param array $usuario Dados do usuario logado
	 * @return void
	 */
	public function mudaEtapa( $idjob=0, array $usuario,$aprovacao = 0 ){
		
		$job = array();
		$processoEtapa = array();
		$erros = array();
		$proximaEtapa = 0;
		$proximoProcessoEtapa = 0;
		$proximoStatus = 0;
		$atualProcessoEtapa = 0;
		
		//Busca informacoes do job
		$job = $this->getById($idjob);

		$arr_historico = $this->jobHistorico->getHistoricoPeca($idjob);
        $agencia = false; // job nao enviado para a agencia
        foreach ($arr_historico as $a) {
            if ($a['ID_ETAPA'] == 6) {
                $agencia = true;
            }

        }
        /*echo "<pre>";
        var_dump($arr_historico);
        exit();*/
		//Verifica se encontrou o job
		//Buscamos informacoes da proxima etapa do job
		if (!empty($job) && is_array($job)) {
			$atualProcessoEtapa = $job['ID_PROCESSO_ETAPA'];
			//Busca o Processo 
			if ( !empty($job['ID_PROCESSO'])) {
				$processoEtapa = $this->processoEtapa->getByProcesso($job['ID_PROCESSO']);
                /*echo "<pre>";
                var_dump($processoEtapa);
                exit();*/
				if (!empty($processoEtapa) && is_array($processoEtapa)) {
					$i=0;
                    // no caso de ser aprovação ou para job finalizado, não irá ficar no looping agencia/pauta
                    if ($aprovacao == 1) $agencia = false;
					//Varre o array ateh encontrar etapa atual
					while ( $i < count( $processoEtapa ) ) {
                        if ($agencia) {
                            if ( $processoEtapa[$i]['CHAVE_ETAPA'] == 'ENVIADO_AGENCIA' ) { // referente a Pauta
                                //Encontrou a etapa atual
                                if (!empty($processoEtapa[$i]['ID_PROCESSO_ETAPA'])) {
                                    //Verifica se existe etapa anterior e recupera o id
                                    $proximoProcessoEtapa = $processoEtapa[$i]['ID_PROCESSO_ETAPA'];
                                    $proximaEtapa = $processoEtapa[$i]['ID_ETAPA'];
                                    $proximoStatus = $processoEtapa[$i]['ID_STATUS'];
                                    //Estoura o acumulador para sair do while
                                    $i = count( $processoEtapa );
                                } else {
                                    $erros[] = 'Não há etapa anterior cadastrada para o processo';
                                }
                            }
                        } else {
                            if ($processoEtapa[ $i ][ 'ID_PROCESSO_ETAPA' ] == $job[ 'ID_PROCESSO_ETAPA' ]) {
                                //Encontrou a etapa atual
                                if ( !empty($processoEtapa[ $i + 1 ][ 'ID_PROCESSO_ETAPA' ])) {
                                    //Verifica se existe proxima etapa e recupera o id
                                    $proximoProcessoEtapa = $processoEtapa[ $i + 1 ][ 'ID_PROCESSO_ETAPA' ];
                                    $proximaEtapa         = $processoEtapa[ $i + 1 ][ 'ID_ETAPA' ];
                                    $proximoStatus        = $processoEtapa[ $i + 1 ][ 'ID_STATUS' ];
                                    //Estoura o acumulador para sair do while
                                    $i = count($processoEtapa);
                                } else {
                                    $erros[] = 'Não há próxima etapa cadastrada para o processo';
                                }
                            }
                        }
						$i++;
					}
				} else {
					$erros[] = 'Processo/Etapas do Processo não encontrado';
				}
			} else {
				$erros[] = 'Não há processo associado ao Job';
			}
		} else {
			$erros[] = 'Job não encontrado';
		}
			
		
		//Verifica se encontrou proxima etapa
		if ( $proximoProcessoEtapa > 0 && $proximaEtapa > 0 && $proximoStatus > 0 ) {
			
			$data = array(
				'ID_ETAPA' => $proximaEtapa,
				'ID_JOB' => $idjob,
				'DATA_HISTORICO' => date('Y-m-d H:i:s'),
				'ID_USUARIO' => $usuario['ID_USUARIO'],
				'ID_PROCESSO_ETAPA' => $proximoProcessoEtapa,
				'ID_STATUS' => $proximoStatus
			);
	
			//Recupera informacoes da proxima etapa
			$etapa = $this->etapa->getById($proximaEtapa);
			
			//Verifica se encontrou a etapa
			if (!empty($etapa) && is_array($etapa)) {
				
				//Recupera informacoes do proximo status
				$status = $this->status->getById($proximoStatus);
				
				//Verifica se encontrou o status
				if (!empty($status) && is_array($status)) {
					
					$this->email->sendEmailRegraProcesso(
						'mudanca_etapa',
						$atualProcessoEtapa,
						'Job enviado para a etapa ' . $etapa['DESC_ETAPA'],
						'ROOT/templates/mudanca_etapa',
						array(
							'job'=>$job,
							'etapa'=>$etapa,
							'status'=>$status
						),
                        $idjob
						);
			
					//Salva historico
					$this->db->insert('TB_JOB_HISTORICO', $data);
	
					//caso o job seja finalizado, gravamos a data
					if($etapa['CHAVE_ETAPA'] == 'FINALIZADO'){
						$data['DATA_FINALIZADO'] = date('Y-m-d H:i:s');
					}
		
					//Salva os dados do job
					unset($data['ID_JOB'], $data['ID_USUARIO']);
					
					//zera o delay de bloqueio do job sempre que muda etapa
					$data['DT_DELAY_BLOQUEIO'] = '0000-00-00 00:00:00';
					$this->save($data, $idjob);
					
					//Atualiza sessão
					Sessao::set('nova_etapa', $etapa);
					
				} else {
					$erros[] = 'Status do processo não encontrado';					
				}
			} else {
				$erros[] = 'Etapa do processo não encontrada';
			}
		}
		return $erros;
	}

	/**
	 * Altera a etapa e a faz voltar para um processo anterior e grava um historico de etapas
	 *
	 * @author Diego Andrade
	 * @param int $idjob Codigo do job
	 * @param array $usuario Dados do usuario logado
	 * @return void
	 */
	public function voltaEtapa( $idjob=0, array $usuario )
    {
		$job = array();
		$processoEtapa = array();
		$erros = array();
		$etapaAnterior = 0;
		$processoEtapaAnterior = 0;
		$statusAnterior = 0;
		$atualProcessoEtapa = 0;

		//Busca informacoes do job
		$job = $this->getById($idjob);
		//Verifica se encontrou o job
		//Buscamos informacoes da proxima etapa do job
		if (!empty($job) && is_array($job)) {
			$atualProcessoEtapa = $job['ID_PROCESSO_ETAPA'];
			//Busca o Processo
			if ( !empty($job['ID_PROCESSO'])) {
				$processoEtapa = $this->processoEtapa->getByProcesso($job['ID_PROCESSO']);
				if (!empty($processoEtapa) && is_array($processoEtapa)) {
					$i=0;
					//Varre o array ateh encontrar etapa atual
					while ( $i < count( $processoEtapa ) ) {
						if ( $processoEtapa[$i]['ID_PROCESSO_ETAPA'] == $job['ID_PROCESSO_ETAPA'] ) {
							//Encontrou a etapa atual
							if (!empty($processoEtapa[$i-1]['ID_PROCESSO_ETAPA'])) {
								//Verifica se existe etapa anterior e recupera o id
								$processoEtapaAnterior = $processoEtapa[$i-1]['ID_PROCESSO_ETAPA'];
								$etapaAnterior = $processoEtapa[$i-1]['ID_ETAPA'];
								$statusAnterior = $processoEtapa[$i-1]['ID_STATUS'];
								//Estoura o acumulador para sair do while
								$i = count( $processoEtapa );
							} else {
								$erros[] = 'Não há etapa anterior cadastrada para o processo';
							}
						}
						$i++;
					}
				} else {
					$erros[] = 'Processo/Etapas do Processo não encontrado';
				}
			} else {
				$erros[] = 'Não há processo associado ao Job';
			}
		} else {
			$erros[] = 'Job não encontrado';
		}


		//Verifica se encontrou proxima etapa
		if ( $processoEtapaAnterior > 0 && $etapaAnterior > 0 && $statusAnterior > 0 ) {

			$data = array(
				'ID_ETAPA' => $etapaAnterior,
				'ID_JOB' => $idjob,
				'DATA_HISTORICO' => date('Y-m-d H:i:s'),
				'ID_USUARIO' => $usuario['ID_USUARIO'],
				'ID_PROCESSO_ETAPA' => $processoEtapaAnterior,
				'ID_STATUS' => $statusAnterior
			);

			//Recupera informacoes da proxima etapa
			$etapa = $this->etapa->getById($etapaAnterior);

			//Verifica se encontrou a etapa
			if (!empty($etapa) && is_array($etapa)) {

				//Recupera informacoes do status anterior
				$status = $this->status->getById($statusAnterior);

				//Verifica se encontrou o status
				if (!empty($status) && is_array($status)) {

					$this->email->sendEmailRegraProcesso(
						'voltar_etapa',
						$atualProcessoEtapa,
						'Job voltou para a etapa ' . $etapa['DESC_ETAPA'],
						'ROOT/templates/voltar_etapa',
						array(
							'job'=>$job,
							'etapa'=>$etapa,
							'status'=>$status
						),
						$idjob
					);

					//Salva historico
					$this->db->insert('TB_JOB_HISTORICO', $data);

					//caso o job seja finalizado, gravamos a data
					if($etapa['CHAVE_ETAPA'] == 'FINALIZADO'){
						$data['DATA_FINALIZADO'] = date('Y-m-d H:i:s');
					}

					//Salva os dados do job
					unset($data['ID_JOB'], $data['ID_USUARIO']);

					//zera o delay de bloqueio do job sempre que muda etapa
					$data['DT_DELAY_BLOQUEIO'] = '0000-00-00 00:00:00';
					$this->save($data, $idjob);

					//Atualiza sessão
					Sessao::set('nova_etapa', $etapa);

				} else {
					$erros[] = 'Status do processo não encontrado';
				}
			} else {
				$erros[] = 'Etapa do processo não encontrada';
			}
		}
		return $erros;
	}

	/**
	 * Solicitando correcoes e faz alternar entre agencia e pauta
	 *
	 * @author Diego Andrade
	 * @param int $idjob Codigo do job
	 * @param array $usuario Dados do usuario logado
	 * @return void
	 */
	public function correcoes( $idjob = 0, array $usuario )
	{
		$job = array();
		$processoEtapa = array();
		$erros = array();
		$etapaAnterior = 0;
		$processoEtapaAnterior = 0;
		$statusAnterior = 0;
		$atualProcessoEtapa = 0;

		//Busca informacoes do job
		$job = $this->getById($idjob);
		/*echo "<pre>";
		var_dump($job);
		exit;*/
		//Verifica se encontrou o job
		//Buscamos informacoes da proxima etapa do job
        if (!empty($job) && is_array($job)) {
            $atualProcessoEtapa = $job['ID_PROCESSO_ETAPA'];
            //Busca o Processo
            if ( !empty($job['ID_PROCESSO'])) {
                $processoEtapa = $this->processoEtapa->getByProcesso($job['ID_PROCESSO']);
                /*echo "<pre>";
                var_dump($processoEtapa);
                exit();*/
                if (!empty($processoEtapa) && is_array($processoEtapa)) {
                    $i=0;
                    //Varre o array ateh encontrar etapa atual
                    while ( $i < count( $processoEtapa ) ) {
                        if ( $processoEtapa[$i]['CHAVE_ETAPA'] == 'CHECKLIST' ) { // referente a Pauta
                            //Encontrou a etapa atual
                            if (!empty($processoEtapa[$i]['ID_PROCESSO_ETAPA'])) {
                                //Verifica se existe etapa anterior e recupera o id
                                $processoEtapaAnterior = $processoEtapa[$i]['ID_PROCESSO_ETAPA'];
                                $etapaAnterior = $processoEtapa[$i]['ID_ETAPA'];
                                $statusAnterior = $processoEtapa[$i]['ID_STATUS'];
                                //Estoura o acumulador para sair do while
                                $i = count( $processoEtapa );
                            } else {
                                $erros[] = 'Não há etapa anterior cadastrada para o processo';
                            }
                        }
                        $i++;
                    }
                } else {
                    $erros[] = 'Processo/Etapas do Processo não encontrado';
                }
            } else {
                $erros[] = 'Não há processo associado ao Job';
            }
        } else {
            $erros[] = 'Job não encontrado';
        }
        //Verifica se encontrou proxima etapa
		if ( $processoEtapaAnterior > 0 && $etapaAnterior > 0 && $statusAnterior > 0 ) {
			$data = array(
				'ID_ETAPA' => $etapaAnterior,
				'ID_JOB' => $idjob,
				'DATA_HISTORICO' => date('Y-m-d H:i:s'),
				'ID_USUARIO' => $usuario['ID_USUARIO'],
				'ID_PROCESSO_ETAPA' => $processoEtapaAnterior,
				'ID_STATUS' => $statusAnterior
			);

			//Recupera informacoes da proxima etapa
			$etapa = $this->etapa->getById($etapaAnterior);

			//Verifica se encontrou a etapa
			if (!empty($etapa) && is_array($etapa)) {

				//Recupera informacoes do status anterior
				$status = $this->status->getById($statusAnterior);

				//Verifica se encontrou o status
				if (!empty($status) && is_array($status)) {

					$this->email->sendEmailRegraProcesso(
						'mudanca_etapa',
						$atualProcessoEtapa,
						'Job voltou para a etapa ' . $etapa['DESC_ETAPA'],
						'ROOT/templates/mudanca_etapa',
						array(
							'job'=>$job,
							'etapa'=>$etapa,
							'status'=>$status
						),
						$idjob
					);

					//Salva historico
					$this->db->insert('TB_JOB_HISTORICO', $data);

					//caso o job seja finalizado, gravamos a data
					if($etapa['CHAVE_ETAPA'] == 'FINALIZADO'){
						$data['DATA_FINALIZADO'] = date('Y-m-d H:i:s');
					}

					//Salva os dados do job
					unset($data['ID_JOB'], $data['ID_USUARIO']);

					//zera o delay de bloqueio do job sempre que muda etapa
					$data['DT_DELAY_BLOQUEIO'] = '0000-00-00 00:00:00';
					$this->save($data, $idjob);

					//Atualiza sessão
					Sessao::set('nova_etapa', $etapa);

				} else {
					$erros[] = 'Status do processo não encontrado';
				}
			} else {
				$erros[] = 'Etapa do processo não encontrada';
			}
		}
		return $erros;
	}

	/**
	 * Recupera as categorias que foram escolhidas em um plano de midia
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idjob Codigo do job relacionado
	 * @param boolean $agruparSubcategorias Agrupa as subcategorias ou nao
	 * @return array
	 */
	public function getPlanoMidia($idjob, $agruparSubcategorias = false){
		$idjob = sprintf('%d', $idjob);

		$sub = 'SELECT ID_EXCEL_VERSAO
			FROM TB_EXCEL_VERSAO
			JOIN TB_EXCEL USING(ID_EXCEL)
			WHERE TB_EXCEL.ID_EXCEL = EC.ID_EXCEL
			AND TB_EXCEL.ID_JOB = '.$idjob.'
			ORDER BY ID_EXCEL_VERSAO DESC LIMIT 1';

		if( $agruparSubcategorias ){
			$this->db
				->select('SUM(EC.QTDE_EXCEL_CATEGORIA) AS QTDE_EXCEL_CATEGORIA')
				->group_by('P.ID_PRACA, SC.ID_SUBCATEGORIA');
		} else {
			$this->db
				->select('EC.QTDE_EXCEL_CATEGORIA');
		}

		$rs = $this->db
			->select('C.*, SC.*, P.DESC_PRACA, P.ID_PRACA, EC.PAGINA_EXCEL_CATEGORIA')
			->join('TB_SUBCATEGORIA SC','SC.ID_CATEGORIA = C.ID_CATEGORIA')
			->join('TB_EXCEL_CATEGORIA EC','EC.ID_SUBCATEGORIA = SC.ID_SUBCATEGORIA AND ('.$sub.') = EC.ID_EXCEL_VERSAO')
			->join('TB_EXCEL E','E.ID_EXCEL = EC.ID_EXCEL AND E.ID_JOB = '.$idjob)
			->join('TB_PRACA P','P.ID_PRACA = EC.ID_PRACA')

			->where('EC.QTDE_EXCEL_CATEGORIA IS NOT NULL AND EC.QTDE_EXCEL_CATEGORIA > 0')
			->order_by('C.DESC_CATEGORIA ASC, SC.DESC_SUBCATEGORIA')
			->get('TB_CATEGORIA C')
			->result_array();

		//pre($this->db->last_query());

		$result = array();
		foreach($rs as $item){
			if( !isset($result[$item['ID_PRACA']]) ){
				$result[$item['ID_PRACA']] = $item;
				$result[$item['ID_PRACA']]['categorias'] = array();
			}
			if( !isset($result[$item['ID_PRACA']]['categorias'][$item['ID_CATEGORIA']]) ){
				$result[$item['ID_PRACA']]['categorias'][$item['ID_CATEGORIA']] = $item;
			}
			$result[$item['ID_PRACA']]['categorias'][$item['ID_CATEGORIA']]['subcategorias'][] = $item;
		}

		return $result;
	}

	/**
	 * Verifica se o job indicado possui fichas ativas
	 *
	 * Se ao menos um esta disponivel, retorna true. Do contrario false.
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idjob Numero do job
	 * @return boolean True se ao menos um produto esta ativo, do contrario false
	 */
	public function hasItensAtivos($idjob){
		$excel = $this->excel->getByJob($idjob);

		$sub = '(SELECT ID_EXCEL_VERSAO
			FROM TB_EXCEL_VERSAO
			JOIN TB_EXCEL USING(ID_EXCEL)
			WHERE TB_EXCEL.ID_EXCEL = E.ID_EXCEL
			AND TB_EXCEL.ID_JOB = '.$idjob.'
			ORDER BY ID_EXCEL_VERSAO DESC LIMIT 1)';

		$total = $this->db
			->from('TB_EXCEL_ITEM I')
			->join('TB_SUBCATEGORIA SC','SC.ID_SUBCATEGORIA = I.SUBCATEGORIA','LEFT')
			->join('TB_CATEGORIA C','C.ID_CATEGORIA = SC.ID_CATEGORIA','LEFT')
			->join('TB_EXCEL E','E.ID_EXCEL = I.ID_EXCEL')
			->join('TB_FICHA F','F.ID_FICHA = I.ID_FICHA')
			->join('TB_OBJETO_FICHA O','O.ID_FICHA = F.ID_FICHA AND O.PRINCIPAL = 1','LEFT')
			->join('TB_PRICING P','P.ID_EXCEL_ITEM = I.ID_EXCEL_ITEM')
			->join('TB_APROVACAO_FICHA AP','AP.ID_APROVACAO_FICHA = F.ID_APROVACAO_FICHA')

			->where('I.ID_EXCEL_VERSAO = ' . $sub)
			->where('ID_JOB', $idjob)
			->where('STATUS_ITEM', 1)
			->where('DISPONIVEL_ITEM',  1)
			->count_all_results();

		return $total > 0;
	}


	/**
	 * Checa se as categorias do plano de marketing foram preenchidas na paginacao
	 *
	 * Faz uma consulta por praca, pegando as categorias que tiveram
	 * informacoes definidas no plano de marketing. Une tambem com os
	 * os itens paginados no modulo de paginacao.
	 *
	 * O resultado da consulta, traz os dados de cada categoria, por praca, indicando
	 * quantos elementos deveriam ser colocados naquela pagina para aquela categoria,
	 * e quantos realmente foram colocados, ex:
	 *
	 * <code>
	 * CATEGORIA    SUBCATEGORIA    PAGINA    QTDE_ESPERADA     QTDE_ENCONTRADA
	 * Hard 1       Cosmeticos      1         4                 3
	 * Hard 2       Pereciveis      2         2                 2
	 * </code>
	 *
	 * Se o parametro <code>$todos</code> for informado como true, retorna todos os elementos,
	 * e nao somente os que nao foram completados
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idjob Numero do job
	 * @param boolean $todos Indica se e para trazer todos os elementos, independente se esta completo ou nao
	 * @return array Itens que nao foram completados
	 */
	public function checarPaginacao($idjob, $todos = false){
		$excel    = $this->excel->getByJob($idjob);
		$versao   = $this->excelVersao->getVersaoAtual($excel['ID_EXCEL']);
		$pracas   = $this->praca->getByExcel($excel['ID_EXCEL']);
		$idversao = sprintf('%d', $versao['ID_EXCEL_VERSAO']);
		$result   = array();

		foreach($pracas as $praca){
			// se nao for para trazer todos
			if( !$todos ){
				// indica que so e para trazer o que nao bater
				$this->db->having('TOTAL != EC.QTDE_EXCEL_CATEGORIA');
			}

			// efetua a consulta
			$rs = $this->db
				->from('TB_EXCEL_CATEGORIA EC')
				->join('TB_EXCEL_ITEM I','I.DISPONIVEL_ITEM = 1 AND I.STATUS_ITEM = 1 AND I.ID_EXCEL = EC.ID_EXCEL AND EC.PAGINA_EXCEL_CATEGORIA = I.PAGINA_ITEM AND I.ID_EXCEL_VERSAO = '. $idversao.' AND I.ID_PRACA = '.$praca['ID_PRACA'],'LEFT')
				->join('TB_FICHA F','F.ID_FICHA = I.ID_FICHA AND F.ID_SUBCATEGORIA = EC.ID_SUBCATEGORIA','LEFT')
				->join('TB_SUBCATEGORIA SC','SC.ID_SUBCATEGORIA = EC.ID_SUBCATEGORIA')
				->join('TB_CATEGORIA C','C.ID_CATEGORIA = SC.ID_CATEGORIA')
				->select('C.DESC_CATEGORIA, SC.DESC_SUBCATEGORIA')
				->select('EC.PAGINA_EXCEL_CATEGORIA, EC.QTDE_EXCEL_CATEGORIA')
				->select('COUNT(F.ID_FICHA) AS TOTAL')
				->where('EC.ID_EXCEL_VERSAO',$idversao)
				->where('EC.ID_PRACA',$praca['ID_PRACA'])
				->where('EC.QTDE_EXCEL_CATEGORIA > 0')
				->groupBy('EC.ID_SUBCATEGORIA, EC.PAGINA_EXCEL_CATEGORIA')
				->order_by('EC.PAGINA_EXCEL_CATEGORIA, C.DESC_CATEGORIA,SC.DESC_SUBCATEGORIA')
				->get();

			// se encontrou alguma coisa
			if( $rs->num_rows() > 0 ){
				// coloca os itens na praca
				$praca['ITENS'] = $rs->result_array();
				// coloca a praca nos itens de resultado
				$result[] = $praca;
			}
		}

		// retorna o resultado
		return $result;
	}

	/**
	 * Salva o plano de midia conforme dados da importacao
	 *
	 * Recupera os dados obtidos na planilha para atualizar o plano de marketing.
	 * O parametro <code>$subcategorias</code> recebe um array contendo pracas, subcategorias, paginas e
	 * quantos produtos vao. Exemplo:
	 *
	 * <code>
	 * $subcategorias[ID_PRACA][ID_SUBCATEGORIA][PAGINA] = $numeroDeItens;
	 * </code>
	 *
	 * Desta forma, podemos iterar pelos resultados para poder gravar o novo
	 * plano de marketing conforme a importacao da planilha.
	 *
	 * Vamos utilizar multi-insert, para agilizar o processo.
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idjob Codigo do Job
	 * @param int $versao Versao da importacao
	 * @param array $subcategorias Subcategorias com a quantidade de itens
	 * @return void
	 */
	public function savePlanoMidiaByImportacao($idjob, $versao, array $subcategorias){
		// cache de linhas a serem inseridas
		$linhas = array();
		// pega os dados do excel pelo job
		$xls = $this->excel->getByJob($idjob);
		// pega o id de excel
		$idexcel = $xls['ID_EXCEL'];

		// grava a maior pagina
		$maiorPagina = 0;

		// para cada praca
		foreach( $subcategorias as $praca => $subcats ){
			// para cada subcategoria
			foreach( $subcats as $idsubcat => $paginas ){
				// para cada pagina
				foreach( $paginas as $pagina => $qtdProdutos ){
					// armazena os valores
					$valores = array();
					$valores[] = sprintf('%d', $idexcel);
					$valores[] = sprintf('%d', $idsubcat);
					$valores[] = sprintf('%d', $versao);
					$valores[] = sprintf('%d', $qtdProdutos);
					$valores[] = sprintf('%d', $praca);
					$valores[] = sprintf('%d', $pagina);

					// colocamos os valores concatenados no cache de linhas
					// a serem inseridas
					$linhas[] = '(' . implode(', ', $valores) . ')';

					// se a pagina atual e maior que a pagina anterior
					if( $pagina > $maiorPagina ){
						// grava a pagina
						$maiorPagina = $pagina;
					}
				}
			}
		}

		// enquanto houverem valores a serem inseridos
		while( !empty($linhas) ){
			// remove uma porcao do cache
			$portion = array_splice($linhas, 0, 400);
			// monta a SQL
			$sql = 'INSERT INTO TB_EXCEL_CATEGORIA (ID_EXCEL, ID_SUBCATEGORIA, ID_EXCEL_VERSAO, QTDE_EXCEL_CATEGORIA, ID_PRACA, PAGINA_EXCEL_CATEGORIA) VALUES ';
			$sql .= implode(', ', $portion);

			// executa a query
			$this->db->query($sql);
		}

		// atualiza a pagina do plano de marketing
		$job = $this->job->getById($idjob);
		$job['PAGINAS_JOB'] = $maiorPagina;
		$this->save($job, $idjob);
	}

	/**
	 * Recupera os dados do cliente a partir do codigo do job
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idjob Codigo do job
	 * @return array Dados do cliente
	 */
	public function getCliente($idjob){
		$rs = $this->db
			->select('C.*')
			->from('TB_CLIENTE C')
			->join('TB_JOB J','C.ID_CLIENTE = J.ID_CLIENTE')
			->where('J.ID_JOB', $idjob)
			->get()
			->row_array();

		return $rs;
	}

	/**
	 * conta quantas revisoes tem um job
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idjob codigo do job
	 * @return int numero de revisoes de um job
	 */
	public function getNumeroProvas($idjob){
		$rs = $this->db
			->from('TB_APROVACAO AP')
			->where('ID_JOB',$idjob)
			->where('ENVIADO_APROVACAO', 1)
			->count_all_results();

		return $rs;
	}

	/**
	 * Recupera o historico de etapas
	 *
	 * Lista todas as etapas por qual o job
	 * solicitado passou, colocando a data
	 * em que ele entrou e saiu da etapa
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idjob Codigo do job desejado
	 * @return array
	 */
	public function getHistoricoEtapas($idjob){
		$sql = 'SELECT
			  E.DESC_ETAPA AS PROXIMA_ETAPA,
			  E.COR_ETAPA,
			  E.CHAVE_ETAPA,
			  U.NOME_USUARIO,
			  H1.DATA_HISTORICO AS DATA_INICIO_ETAPA,
			  IF(
			    E.CHAVE_ETAPA = "FINALIZADO", H1.DATA_HISTORICO,
				  IFNULL(
				      (SELECT DATA_HISTORICO FROM TB_JOB_HISTORICO WHERE ID_JOB = H1.ID_JOB AND DATA_HISTORICO > H1.DATA_HISTORICO ORDER BY DATA_HISTORICO ASC LIMIT 1)
			    	  ,CURRENT_TIMESTAMP
			  	)
			  ) AS DATA_FIM_ETAPA

			FROM
			  TB_ETAPA E
			  JOIN TB_JOB_HISTORICO H1 ON E.ID_ETAPA = H1.ID_ETAPA
			  JOIN TB_USUARIO U ON H1.ID_USUARIO = U.ID_USUARIO

			  WHERE H1.ID_JOB = %d

			ORDER BY H1.DATA_HISTORICO';

		$sql = sprintf($sql, $idjob);

		$rs = $this->db
			->query($sql)
			->result_array();

		return $rs;
	}
	
	/**
	 * Busca Data Inicio e Data Final de cada etapa do job(processo)
	 * idJob = Id do Job a ser calculado
	 * $dataInicioProcesso = Se foi informado, utilizar esta data para o calculo. Sera utilizado na alteracao de datas/etapa da pauta
	 * @author Nelson Martucci
	 * @param unknown_type $idJob
	 * @return array()
	 */
	
	public function getDataEtapasJob( $idJob=0, $dataInicioProcesso='1970-01-01' ) {

		//Array para retornar das etapas do job
		$etapasJob = array();
		
		//Array com os Feriados do JOB
		$feriados = array();
		
		//Busca o job
		$rsJob = $this->db		
					->select('DATA_INICIO_PROCESSO, ID_PROCESSO')
					->from('TB_JOB')
					->where('ID_JOB', $idJob)
					->get();
					
		// se encontrou o Job
		if( $rsJob->num_rows() > 0 ){
			
			//Monta Array de Retorno com a data de inicio do processo
			$etapasJob['JOB'] = $rsJob->row_array();
			
			//Se foi informado $dataInicioProcesso, quer dizer que eh um calculo esporadico. Utilizar esta
			if ( $dataInicioProcesso != '1970-01-01') {
				$etapasJob['JOB']['DATA_INICIO_PROCESSO'] = $dataInicioProcesso;
			}
			
			//Faz a consulta das etapas do processo no banco 
			$rsEtapas = $this->db
				->select('EP.ID_PROCESSO_ETAPA, EP.ID_ETAPA, EP.QTD_HORAS, EP.ORDEM, EP.ID_STATUS')
				->select('P.HORA_INICIO, P.HORA_FINAL, P.CONSIDERAR_DIAS_UTEIS, P.CONSIDERAR_FERIADO')
				->select('P.*')
				->select('E.DESC_ETAPA, E.COR_ETAPA, E.CHAVE_ETAPA')
				->from('TB_PROCESSO P')
				->where('P.ID_PROCESSO', $etapasJob['JOB']['ID_PROCESSO'])
				->join('TB_PROCESSO_ETAPA EP','EP.ID_PROCESSO = P.ID_PROCESSO')
				->join('TB_ETAPA E','E.ID_ETAPA = EP.ID_ETAPA')
				
				->order_by('EP.ORDEM')
				->get();
				
			// se encontrou etapas
			if( $rsEtapas->num_rows() > 0 ){
				
				//array para tratamento dos dados retornados
				$etapas = $rsEtapas->result_array();
				
				//Hora Inicio
				$etapasJob['PROCESSO']['HORA_INICIO'] = $etapas[0]['HORA_INICIO'];
				
				//Hora final
				$etapasJob['PROCESSO']['HORA_FINAL'] = $etapas[0]['HORA_FINAL'];
				
				//Considerar Somente dias uteis ?
				$etapasJob['PROCESSO']['CONSIDERAR_DIAS_UTEIS'] = $etapas[0]['CONSIDERAR_DIAS_UTEIS'];
				
				//Considerar feriados ?
				$etapasJob['PROCESSO']['CONSIDERAR_FERIADO'] = $etapas[0]['CONSIDERAR_FERIADO'];
				
				//Buscar os feriados do job.
				if ( strtoupper($etapasJob['PROCESSO']['CONSIDERAR_FERIADO']) == 'N') {
					$feriados = $this->calendario->getFeriadosByJob($idJob);
				}
		
				//Formata Data de Inicio do Processo para chamar rotina de calculo de horas uteis
				$horasMinutosPeriodoUtilInicial = explode(':', $etapasJob['PROCESSO']['HORA_INICIO'] );
				$etapasJob['JOB']['DATA_INICIO_PROCESSO'] = mktime( 	$horasMinutosPeriodoUtilInicial[0], 
																		$horasMinutosPeriodoUtilInicial[1], 
																		0, 
																		format_date($etapasJob['JOB']['DATA_INICIO_PROCESSO'],'m'),
																		format_date($etapasJob['JOB']['DATA_INICIO_PROCESSO'],'d'),
																		format_date($etapasJob['JOB']['DATA_INICIO_PROCESSO'],'Y') );
				
				//Acumulador de data/hora das etapas do processo/job
				//Inicia com a data inicio do processo
				$dataAcumulada = $etapasJob['JOB']['DATA_INICIO_PROCESSO'];
				
				//Para cada Etapa
				foreach ($etapas as $etapa) {
					
					//Monta array auxiliar de retorno
					$etapaCalculada = array();
					
					$etapaCalculada['ID_PROCESSO_ETAPA'] 	= $etapa['ID_PROCESSO_ETAPA'];
					$etapaCalculada['ID_ETAPA'] 			= $etapa['ID_ETAPA'];
					$etapaCalculada['ID_STATUS'] 			= $etapa['ID_STATUS'];
					$etapaCalculada['QTD_HORAS'] 			= $etapa['QTD_HORAS'];
					$etapaCalculada['ORDEM'] 				= $etapa['ORDEM'];
					$etapaCalculada['DESC_ETAPA'] 			= $etapa['DESC_ETAPA'];
					$etapaCalculada['COR_ETAPA'] 			= $etapa['COR_ETAPA'];
					$etapaCalculada['CHAVE_ETAPA'] 			= $etapa['CHAVE_ETAPA'];
					
					$etapaCalculada['DATA_INICIO_ETAPA'] = $dataAcumulada;
					
					//Calcula Data Final de cada Etapa
					$etapaCalculada['DATA_FINAL_ETAPA'] = horasUteis(	$dataAcumulada, 
																		$etapa['QTD_HORAS'], 
																		$etapasJob['PROCESSO']['HORA_INICIO'],
																		$etapasJob['PROCESSO']['HORA_FINAL'],
																		$etapasJob['PROCESSO']['CONSIDERAR_DIAS_UTEIS'],
																		$etapasJob['PROCESSO']['CONSIDERAR_FERIADO'],
																		$feriados );
																
					//Aux
					$etapaCalculada['DATA_INICIO_ETAPA_AUX'] = date('H:i:s d/m/Y', $etapaCalculada['DATA_INICIO_ETAPA']);
					$etapaCalculada['DATA_FINAL_ETAPA_AUX'] = date('H:i:s d/m/Y', $etapaCalculada['DATA_FINAL_ETAPA']);
					
					//Guarda Data Final do Processo
					$etapasJob['JOB']['DATA_FINAL_PROCESSO'] = $etapaCalculada['DATA_FINAL_ETAPA'];
					
					$etapasJob['ETAPA'][] = $etapaCalculada;
					
					//Montar DATA_INICIO_ETAPA da proxima etapa
					//A Data de inicio de cada etapa eh sempre a data final da etapa anterior. Excecao quando a hora final eh a hora final do periodo util.
					//Ex: Hora final da etapa = 17:00 e Hora final do periodo = 17:00
					//Para resolver este problema, somamos 1 minuto na data/hora acumulada para que este calcule o proximo dia util e em seguida subtraimos esse 1 minuto
					$dataAcumulada = horasUteis(	$etapaCalculada['DATA_FINAL_ETAPA'], 
													'0:01', 
													$etapasJob['PROCESSO']['HORA_INICIO'],
													$etapasJob['PROCESSO']['HORA_FINAL'],
													$etapasJob['PROCESSO']['CONSIDERAR_DIAS_UTEIS'],
													$etapasJob['PROCESSO']['CONSIDERAR_FERIADO'],
													$feriados );
													
					//Subtraimos 1 minutos em segundos.
					$dataAcumulada -= 60;												
				}
				
				//A ultima $dataAcumulada eh a DATA_INICIO do Job
				$etapasJob['JOB']['DATA_INICIO'] = $dataAcumulada;
			}
		}
	
		// retorna o resultado
		return $etapasJob;
			
	}
	/**
	 * Retorna os jobs de um determinado produto
	 * @author Nelson Martucci
	 * @param unknown_type $idProduto
	 * @return unknown_type
	 */
	public function getJobsByProduto( $idProduto=0 ) {

		//Array com os jobs
		$arrayJobs = array();
		
		$rs = $this->db
			->select('J.ID_JOB, J.TITULO_JOB')
			->from('TB_JOB J')
			->join('TB_CAMPANHA C','C.ID_CAMPANHA = J.ID_CAMPANHA AND C.ID_PRODUTO = ' . $idProduto )
			->where('J.DATA_FINALIZADO IS NULL')
			->where('J.DATA_INICIO_PROCESSO <= CURRENT_DATE')
			->where('J.DATA_INICIO >= CURRENT_DATE')
			->where('J.STATUS_JOB = 1')
			->get();

		// se encontrou alguma coisa
		if( $rs->num_rows() > 0 ){
			
			$arrayJobs = $rs->result_array();
		}

		// retorna o resultado
		return $arrayJobs;
		
	}
	/**
	 * Retorna os jobs utilizados pelo calendario
	 * @author Nelson Martucci
	 * @param unknown_type $idCalendario
	 * @return unknown_type
	 */
	public function getJobsByCalendario( $idCalendario=0 ) {
		
		//Array com os jobs
		$arrayJobs = array();
		
		$rs = $this->db
			->select('J.ID_JOB, J.TITULO_JOB')
			->from('TB_JOB J')
			->join('TB_CAMPANHA C','C.ID_CAMPANHA = J.ID_CAMPANHA')
			->join('TB_PRODUTO P','P.ID_PRODUTO = C.ID_PRODUTO AND P.ID_CALENDARIO = ' . $idCalendario )
			->where('J.DATA_FINALIZADO IS NULL')
			->where('J.DATA_INICIO_PROCESSO <= CURRENT_DATE')
			->where('J.DATA_INICIO >= CURRENT_DATE')
			->where('J.STATUS_JOB = 1')
			->get();

		// se encontrou alguma coisa
		if( $rs->num_rows() > 0 ){
			
			$arrayJobs = $rs->result_array();
		}

		// retorna o resultado
		return $arrayJobs;
		
	}
	
	/**
	 * Atualiza Data Inicio e Data Fim do Job de acordo com recalculo automatico
	 * @author Nelson Martucci
	 * @param unknown_type $idJob
	 * @return unknown_type
	 */
	public function atualizaDatasJob( $idJob=0 ) {
	
		$job = array ();
		$etapasJob = array ();
		
		$diferencaEmDias = 0;
		$diferencaEmSegundos = 0;
		
		//Busca Job
		$job = $this->getById( $idJob );
		
		//Se encontrou
		if ( !empty($job) && is_array($job)) {
			//Recalcula data final do job
			$etapasJob = $this->getDataEtapasJob( $idJob );
			
			//Se encontrou
			if ( !empty($job) && is_array($job)) {
				
				//A nova DATA_INICIO do Job eh a data final do processo calculada
				$novaDataInicio = date('Y-m-d',$etapasJob['JOB']['DATA_INICIO']);
				
				//A diferença entre as nova data inicio e a data inicio antiga, deve ser aplicada também na data Termino
				//Para isso vamos transformar tudo em segundos para calculo
				$antigaDataInicioEmSegundos = mktime( 	0,
														0,
														0,
														format_date($job['DATA_INICIO'],'m'),
														format_date($job['DATA_INICIO'],'d'),
														format_date($job['DATA_INICIO'],'Y') ); 
				
				$novaDataInicioEmSegundos = mktime( 0,
													0,
													0,
													date('m',$etapasJob['JOB']['DATA_INICIO']),
													date('d',$etapasJob['JOB']['DATA_INICIO']),
													date('Y',$etapasJob['JOB']['DATA_INICIO']) );
													
				//Calcular a diferença em segundos
				if ( $novaDataInicioEmSegundos > $antigaDataInicioEmSegundos ) {
					$diferencaEmSegundos = $novaDataInicioEmSegundos - $antigaDataInicioEmSegundos;

					//Vamos transformar em dias
					$diferencaEmDias = $diferencaEmSegundos / 3600 / 24;
																
					//Calcular Nova data Termino
					$novaDataTermino = date('Y-m-d',mktime(	0,
															0,
															0,
															format_date($job['DATA_TERMINO'],'m'),
															format_date($job['DATA_TERMINO'],'d') + $diferencaEmDias,
															format_date($job['DATA_TERMINO'],'Y')
													)
										); 
				} else {
					$diferencaEmSegundos = $antigaDataInicioEmSegundos - $novaDataInicioEmSegundos;
					
					//Vamos transformar em dias
					$diferencaEmDias = $diferencaEmSegundos / 3600 / 24;
																
					//Calcular Nova data Termino
					$novaDataTermino = date('Y-m-d',mktime(	0,
															0,
															0,
															format_date($job['DATA_TERMINO'],'m'),
															format_date($job['DATA_TERMINO'],'d') - $diferencaEmDias,
															format_date($job['DATA_TERMINO'],'Y')
													)
										); 
				}
				
				//Atualiza o Banco de Dados
				$this->db
					->where('ID_JOB', $idJob)
					->update('TB_JOB', array('DATA_INICIO' => $novaDataInicio, 'DATA_TERMINO' => $novaDataTermino));
					
				return true;
				
			}
		}
	}

	/**
	 * Recupera os limites de subcategorias, maximo e minimo
	 * 
	 * @author Sidnei Tertuliano Junior
	 * @link http://www.247id.com.br
	 * @param int $idExcel
	 * @param int $idPraca
	 * @param int $idSubcategoria
	 * @return array 
	 */
	public function getLimitesEspelho($idExcel, $idPraca, $idSubcategoria){
		$rs = $this->db->select('(sum(EC.QTDE_EXCEL_CATEGORIA) + LIMITE_MAXIMO) as MAXIMO, (sum(EC.QTDE_EXCEL_CATEGORIA) - LIMITE_MINIMO) as MINIMO')
				->join('TB_EXCEL E', 'EC.ID_EXCEL = E.ID_EXCEL')
				->join('TB_JOB J', 'E.ID_JOB = J.ID_JOB')
				->join('TB_TIPO_PECA_LIMITE TPL', 'J.ID_TIPO_PECA = TPL.ID_TIPO_PECA and EC.ID_SUBCATEGORIA = TPL.ID_SUBCATEGORIA')
				->where('EC.ID_EXCEL', $idExcel)
				->where('EC.ID_PRACA', $idPraca)
				->where('EC.ID_SUBCATEGORIA', $idSubcategoria)
				->group_by('EC.ID_SUBCATEGORIA');

		return $this->db->get('TB_EXCEL_CATEGORIA EC')->result_array();
	}
	/**
	 * Retorna os jobs em andamento/ativos
	 * @author Nelson Martucci
	 * @return array de jobs
	 */
	public function getJobsAtivos() {

		//Array com os jobs
		$arrayJobs = array();
		
		//Retira jobs bloqueados
		$arrayStatusDenied = array( $this->status->getIdByKey('BLOQUEADO') );
		
		//Retira jobs finalizados
		$arrayEtapaDenied = array( $this->etapa->getIdByKey('FINALIZADO'));
		
		$rs = $this->db
			->select('J.*, A.DESC_AGENCIA, CL.DESC_CLIENTE, P.DESC_PRODUTO, C.DESC_CAMPANHA, S.DESC_STATUS, ET.DESC_ETAPA, PE.ORDEM')
			->select('UR.NOME_USUARIO as NOME_USUARIO_RESPONSAVEL')
			->join('TB_CAMPANHA C', 'C.ID_CAMPANHA = J.ID_CAMPANHA')
			->join('TB_PRODUTO P', 'P.ID_PRODUTO = C.ID_PRODUTO')
			->join('TB_CLIENTE CL', 'CL.ID_CLIENTE = J.ID_CLIENTE')
			->join('TB_AGENCIA A', 'A.ID_AGENCIA = J.ID_AGENCIA')
			->join('TB_STATUS S', 'S.ID_STATUS = J.ID_STATUS')
			->join('TB_ETAPA ET', 'ET.ID_ETAPA = J.ID_ETAPA')
			->join('TB_PROCESSO_ETAPA PE', 'PE.ID_PROCESSO_ETAPA = J.ID_PROCESSO_ETAPA')
			->join('TB_USUARIO UR', 'UR.ID_USUARIO = J.ID_USUARIO_RESPONSAVEL','LEFT')
			->from('TB_JOB J')
			->where('J.DATA_INICIO_PROCESSO <= CURRENT_DATE')
			->where('J.DATA_INICIO >= CURRENT_DATE')
			->where('J.DATA_FINALIZADO IS NULL')
			->where('J.STATUS_JOB = 1')
			->where('J.ID_PROCESSO_ETAPA IS NOT NULL')
			->where_not_in('J.ID_STATUS', $arrayStatusDenied )
			->where_not_in('J.ID_ETAPA', $arrayEtapaDenied )
			->get();

		// se encontrou alguma coisa
		if( $rs->num_rows() > 0 ){
			
			$arrayJobs = $rs->result_array();
		}

		// retorna o resultado
		return $arrayJobs;
		
	}
	
	/**
	 * Valida os itens que vem do carrinho com o espelho
	 * - verifica se exite limites cadastrados para o tipo de pecas e subcategorias dos itens
	 * 
	 * @author Sidnei Tertuliano Junior
	 * @param int $id
	 * @return void
	 */
	public function validaItensEspelho($idJob){
		// armazena as iformacoes do job
		$job = $this->job->getById($idJob);
		
		// armazena as pracas do job
		$pracas = $this->job->getPracas($idJob);
		
		// armazena a id do excel
		$idExcel = $job['ID_EXCEL'];
		
		// array de erros
		$erros = array();		

		// faz um for nas pracas do job
		foreach($pracas as $p){
			// agrupa as subcategorias e pega os totais de registros de cada categoria contido na excel item
			$itensTotalSubcategorias = $this->excelItem->getTotalSubcategoriasByExcelPraca($idExcel, $p['ID_PRACA']);
			
			// for nas categorias
			foreach($itensTotalSubcategorias as $i){
				// armazena a id subcategoria
				$idSubcategoria = $i['ID_SUBCATEGORIA'];
				// armazena o total referente aquela subcategoria
				$total = $i['TOTAL'];
				
				// pega o limite do espelho q foi cadastrado no plano de marketing e adiciona e subtrai oque tem no tipo de peça
				$espelho = $this->job->getLimitesEspelho($idExcel, $p['ID_PRACA'], $idSubcategoria);

				// pega todas informacoes da subcategoria
				$subcategoria = $this->subcategoria->getById($idSubcategoria);

				// se ouver espelho, ou seja o limite maximo e minimo de itens iremos validar pra ver se eta dentro do range so memso
				if(isset($espelho[0]['MAXIMO']) && isset($espelho[0]['MINIMO'])){
					// armazena limite maximo permitido
					$maximo = $espelho[0]['MAXIMO'];
					// armazena limite minimo permitido
					$minimo = $espelho[0]['MINIMO'];
					
					// se total de itens contidos no job ultrapassa o limite maximo, coloca a msg na array de erros
					if($total > $maximo){						
						$msgErro = "Existem '" .$total. "' itens da subcategoria '" .$subcategoria['DESC_CATEGORIA']. " - " .$subcategoria['DESC_SUBCATEGORIA']. "' ";
						$msgErro .= "e o limite maximo permitido é de '" .$maximo. "' itens!";
						
						$erros[$p['DESC_PRACA']][$idSubcategoria] = $msgErro;
					}
					
					// se total de itens contidos no job nao alcancar a quantidade minima, coloca a msg na array de erros
					if($total < $minimo){
						$msgErro = "Existem '" .$total. "' itens da subcategoria '" .$subcategoria['DESC_CATEGORIA']. " - " .$subcategoria['DESC_SUBCATEGORIA']. "' ";
						$msgErro .= "e o limite minimo é de '" .$minimo. "' itens!";
						
						$erros[$p['DESC_PRACA']][$idSubcategoria] = $msgErro;
					}
				}
				else{
					// se nao existir o limite minomo e maximo, coloca a msg na array de erros
//					$erros[$p['DESC_PRACA']][$idSubcategoria] = "Subcategoria '" .$subcategoria['DESC_CATEGORIA']. " - " .$subcategoria['DESC_SUBCATEGORIA']. "' nao possui limite Minimo e Maximo!";
					$erros[$p['DESC_PRACA']][$idSubcategoria] = 'Não há limite parametrizado para a categoria ' .$subcategoria['DESC_CATEGORIA']. ' e subcategoria ' .$subcategoria['DESC_SUBCATEGORIA']. ' para este tipo de peça';
					$erros['ERRO_LIMITE_NOT_FOUND'] = true;
				}
			}
		}
		return $erros;
	}
	
	/**
	 * Ordena os dados de um array (planilha) pela base
	 * 
	 * @author Sidnei Tertuliano Junior
	 * @param int $id
	 * @param array $pracaBase
	 * * @param array $ddos
	 * @return array
	 */
	// funcao que ordena os dados de um array (planilha) pela base
	public function ordenaPracaBase($idJob, $pracaBase, $dados){
		// dados do excel
		$excel = $this->excel->getByJob($idJob);

		// contador para incrementar caso o produto nao exista na base
		$contador = null;	
		
		// pagina do produto atual do loop
		$paginaAtual = null;
		
		// pagina do produto anterior do loop
		$paginaAnterior = null;
		
		// array q armazenara a pagina e as ordens de produtos que existem na praca base (pagina=>ordem)
		$paginaOrdem = array();

		// loop para oordenar as fichas, neste trecho ordenamos apenas as que existem na base
		for($d = 0; $d <= count($dados)-1; $d++){
			$produto = $this->excelItem->getByExcelCodigoPracaPagina($excel['ID_EXCEL'], $dados[$d]['PRODUTO'], $pracaBase['ID_PRACA'], $dados[$d]['PAGINA_ITEM'], true, false);

			if(count($produto) > 0){
				$dados[$d]['ORDEM_ITEM'] = $produto[0]['ORDEM_ITEM'];

				// loop para oordenar as subfichas caso existam
				if(isset($dados[$d]['SUBFICHAS']) && count($dados[$d]['SUBFICHAS']) > 0){
					foreach($dados[$d]['SUBFICHAS'] as $key=>$value){
						$dados[$d]['SUBFICHAS'][$key]['ORDEM_ITEM'] = $produto[0]['ORDEM_ITEM'];
					}			
				}

				$paginaOrdem[$produto[0]['PAGINA_ITEM']][] = $produto[0]['ORDEM_ITEM'];
			}
		}

		// loop para oordenar as fichas, neste trecho ordenamos apenas as que nao existem na base
		for($d = 0; $d <= count($dados)-1; $d++){
			$produto = $this->excelItem->getByExcelCodigoPracaPagina($excel['ID_EXCEL'], $dados[$d]['PRODUTO'], $pracaBase['ID_PRACA'], $dados[$d]['PAGINA_ITEM'], true, false);

			if(count($produto) == 0){
				$paginaAtual = $dados[$d]['PAGINA_ITEM'];

				if(($contador == null) || ($paginaAtual != $paginaAnterior) ){
					if(isset($paginaOrdem[$paginaAtual])){
//						$contador = max($paginaOrdem[$paginaAtual]);
						$contador = $this->excelItem->getTotalOrdemByExcelPracaPagina($excel['ID_EXCEL'], $pracaBase['ID_PRACA'], $paginaAtual);
						$contador++;
					}
					else{
						$contador = $this->excelItem->getTotalOrdemByExcelPracaPagina($excel['ID_EXCEL'], $pracaBase['ID_PRACA'], $dados[$d]['PAGINA_ITEM']);
						if(is_numeric($contador)){
							$contador++;
						}
						else{
							$contador = 1;
						}
					}
				}

				$dados[$d]['ORDEM_ITEM'] = $contador;
				
				// loop para oordenar as subfichas caso existam
				if(isset($dados[$d]['SUBFICHAS']) && count($dados[$d]['SUBFICHAS']) > 0){
					foreach($dados[$d]['SUBFICHAS'] as $key=>$value){
						$dados[$d]['SUBFICHAS'][$key]['ORDEM_ITEM'] = $contador;
					}			
				}

				$paginaAnterior = $paginaAtual;
				
				$contador++;
			}
		}
    	return $dados;
	}
	
	/**
	 * Recupera os jobs ATIVOS que estão disponiveis para o cliente/agência
	 *
	 * @author Bruno de Oliveira
	 * @param array $filters
	 * @return array
	 */
	public function getByIdClienteOrIdAgencia(array $filters){
		$this->db
				->from($this->tableName.' J')
				->select('J.ID_JOB,J.TITULO_JOB')
				->join('TB_CAMPANHA C', 'C.ID_CAMPANHA = J.ID_CAMPANHA')
				->where('J.STATUS_JOB','1')
				->where('C.STATUS_CAMPANHA','1');
		
		if(isset($filters['ID_CLIENTE']) || !empty($filters['ID_CLIENTE'])){
			$this->db->where('J.ID_CLIENTE',$filters['ID_CLIENTE']);
		} else {
			$this->db->where('J.ID_AGENCIA',$filters['ID_AGENCIA']);
		}

		$result = $this->db->get()->result_array();
		
		return $result;
	}
	
	/**
	 * Recupera os jobs ATIVOS que estão disponiveis para o cliente/agência
	 *
	 * @author Bruno de Oliveira
	 * @param array $filters
	 * @return array
	 */
	public function getNomeById($idJob){
		$this->db
		->from($this->tableName.' J')
		->select('J.ID_JOB,J.TITULO_JOB,J.ID_ETAPA')
		->where('J.ID_JOB',$idJob);
	
		$result = $this->db->get()->result_array();
	
		return $result;
	}

	/**
	 * Recupera os jobs diferentes do passado
     * Com o mesmo número de páginas e status em pauta
	 * E com a campanha ativa
     *
	 * @author Diego Andrade
	 * @param int $id
	 * @return array
	 */
	public function getJobsToCopy($id,$numeroPaginas){
        $user = Sessao::get('usuario');
        $this->db
			->from($this->tableName.' J')
			->select('*')
            ->join('TB_CAMPANHA C', 'C.ID_CAMPANHA = J.ID_CAMPANHA')
			->where('J.ID_JOB <> '.$id)
			->where('J.ID_CLIENTE',$user['ID_CLIENTE'])
            ->where('J.PAGINAS_JOB',$numeroPaginas)
            ->where('J.ID_ETAPA = 5')
            //->where('J.STATUS_JOB','1')
            ->where('C.STATUS_CAMPANHA','1');

		$result = $this->db->get()->result_array();

		return $result;

}

public function setEtapa($idjob, $idetapa){
		$this->db
			->where('ID_JOB', $idjob)
			->update('TB_JOB', array('ID_ETAPA' => $idetapa,'DATA_FINALIZADO' => date('Y-m-d')));
	}
	
}
