<?php
/**
 * Classe de modelo para o gerenciamento de aprovacao de producao
 * @author Juliano Polito
 * @link http://www.247id.com.br
 */
class AprovacaoArquivoDB extends GenericModel {
	### START
	protected function _initialize(){
		$this->addField('ID_APROVACAO_ARQUIVO','int','',1,1);
		$this->addField('ID_APROVACAO','int','',1,0);
		$this->addField('ID_USUARIO','int','',1,0);
		$this->addField('TITULO_APROVACAO_ARQUIVO','string','',10,0);
		$this->addField('DATA_CADASTRO','string','',10,0);
	}
	### END

	
	var $tableName = 'TB_APROVACAO_ARQUIVO';
	
	/**
	 * Construtor
	 *  
	 * @author Juliano Polito
	 * @link http://www.247id.com.br
	 * @return CampanhaDB
	 */
	function __construct(){
		parent::GenericModel();
		// adiciona as validacoes
		//$this->addValidation('OBS_APROVACAO','requiredString','Informe o nome');
	}
	
	/**
	 * Recupera o path do arquivo 
	 * 
	 * @author juliano.polito
	 * @param int $idarquivo
	 * @return string
	 */
	public function getPathArquivo($idarquivo,$IsArquivoCliente=false){
		
		$arquivo = $this->aprovacaoArquivo->getById($idarquivo);
		$ap = $this->aprovacao->getById($arquivo['ID_APROVACAO']);
		
		$pathJob = $this->getPathJob($ap['ID_JOB']);
		$pathJob = $pathJob.$ap['ID_APROVACAO'].'/';
		
		if(!is_dir($pathJob)){
			mkdir($pathJob,0777,true);
			chmod($pathJob, 0777);
		}
		
		if($IsArquivoCliente == '1')
			$path = $pathJob.'ARQUIVOS_CLIENTE/'.$arquivo['TITULO_APROVACAO_ARQUIVO'];
		else
			$path = $pathJob.$idarquivo.'.pdf';
		
		return $path;
	}
	
	public function getPathJob($idjob){
		$job = $this->job->getById($idjob);
		$pathCamp = $this->campanha->getPath($job['ID_CAMPANHA']);
		$pathAp = $pathCamp.$this->config->item('aprovacao_path');
		$pathJob = $pathAp.$idjob.'/';
		
		
		if(!is_dir($pathJob)){
			mkdir($pathJob,0777,true);
			chmod($pathJob, 0777);
		}
		
		return $pathJob;
	}
	
	public function getByAprovacao($idAprovacao){
		$ap = $this->getById($idAprovacao);
		$arquivos = $this->db
					->join('TB_APROVACAO_COMENTARIO_ARQUIVO ACA', 'ACA.ID_APROVACAO_ARQUIVO = TB_APROVACAO_ARQUIVO.ID_APROVACAO_ARQUIVO')
					->where('ID_APROVACAO',$idAprovacao)
					->where('ACA.IS_ARQUIVO_CLIENTE',0)
					->get('TB_APROVACAO_ARQUIVO')->result_array();
		
		foreach($arquivos as $key => $arquivo){
			$arquivos[$key]['comentarios'] = $this->aprovacaoComentario->getByArquivo($arquivo['ID_APROVACAO_ARQUIVO']);
		}
		
		return $arquivos;
	}
	
	public function getcomentariosByAprovacao($idAprovacao){
		$ap = $this->getById($idAprovacao);
		$arquivos = $this->db
		->join('TB_APROVACAO_COMENTARIO_ARQUIVO ACA', 'ACA.ID_APROVACAO_ARQUIVO = TB_APROVACAO_ARQUIVO.ID_APROVACAO_ARQUIVO')
		->join('TB_APROVACAO AP','AP.ID_APROVACAO = TB_APROVACAO_ARQUIVO.ID_APROVACAO')
		->join('TB_APROVACAO_COMENTARIO AC','ACA.ID_APROVACAO_COMENTARIO = AC.ID_APROVACAO_COMENTARIO','LEFT')
		->join('TB_JOB J','J.ID_JOB = AP.ID_JOB')
		->join('TB_USUARIO U','U.ID_USUARIO = AC.ID_USUARIO')
		->join('TB_GRUPO G','U.ID_GRUPO = G.ID_GRUPO')
		->join('TB_AGENCIA AG','AG.ID_AGENCIA = G.ID_AGENCIA','LEFT')
		->join('TB_CLIENTE CG','CG.ID_CLIENTE = G.ID_CLIENTE','LEFT')
		->where('TB_APROVACAO_ARQUIVO.ID_APROVACAO',$idAprovacao)
		->where('ACA.IS_ARQUIVO_CLIENTE',0)
		->get('TB_APROVACAO_ARQUIVO')->result_array();
	
		foreach($arquivos as $key => $arquivo){
			$arquivos[$key]['comentarios'] = $this->aprovacaoComentario->getByArquivo($arquivo['ID_APROVACAO_ARQUIVO']);
		}
	
		return $arquivos;
	}
}

