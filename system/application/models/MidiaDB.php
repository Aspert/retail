<?php
class MidiaDB extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_MIDIA','int','',1,1);
		$this->addField('DESC_MIDIA','string','',15,0);
		$this->addField('FLAG_ACTIVE_MIDIA','integer','',1,0);
	}
	### END

	var $tableName = 'TB_MIDIA';
	var $statusField = 'FLAG_ACTIVE_MIDIA';
	
	function getByLike($dados = null, $pagina=0, $limit=null)
	{
		
		$sql = 	' SELECT * FROM TB_MIDIA ' .								 			 
			 	' WHERE TB_MIDIA.FLAG_ACTIVE_MIDIA = 1';
				
		if( isset($dados['DESC_MIDIA']) && $dados['DESC_MIDIA']!= null)
			$sql.=" AND DESC_MIDIA LIKE '%".$dados['DESC_MIDIA']."%'";
				
		return $this->executeQuery($sql,$pagina, $limit, 'DESC_MIDIA', 'DESC');
	}
	
}

