<?php
class GrupoDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_GRUPO','int','',1,1);
		$this->addField('DESC_GRUPO','string','',5,0);
		$this->addField('CHAVE_GRUPO','string','',5,0);
		$this->addField('ID_AGENCIA','int','',0,0);
		$this->addField('ID_CLIENTE','int','',0,0);
		$this->addField('STATUS_GRUPO','int','',1,0);
	}
	### END

	var $tableName = 'TB_GRUPO';
	var $statusField = 'STATUS_GRUPO';

	/**
	 * Construtor 
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return GrupoDB
	 */
	function __construct(){
		parent::GenericModel();
		
		$this->addValidation('DESC_GRUPO','requiredString','Informe o nome');
		//Chave deixa de ser obrigatoria pois deve ser setada por nos. 
		//O pessoal do suporte nao sabe criar essa chave
		$this->addValidation('CHAVE_GRUPO','requiredString','Informe a chave interna');
		$this->addValidation('ID_GRUPO','function', array($this, 'checaAgenciaCliente'));
	}
	
	/**
	 * Checa se selecionou uma agencia ou cliente
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param array $data
	 * @return mixed
	 */
	public function checaAgenciaCliente($data){
		if( empty($data['ID_AGENCIA']) && empty($data['ID_CLIENTE']) ){
			return 'Informe uma agência ou cliente';
		}
		return true;
	}
	
	public function getById($id){
		$rs = $this->db
			->join('TB_CLIENTE C','C.ID_CLIENTE = G.ID_CLIENTE','LEFT')
			->join('TB_AGENCIA A','A.ID_AGENCIA = G.ID_AGENCIA','LEFT')
			->where('ID_GRUPO',$id)
			->get($this->tableName . ' G')
			->row_array();
			
		return $rs;
	}
	
	/**
	 * Override para poder salvar os produtos e categorias relacionados
	 * 
	 * @see system/application/libraries/GenericModel#save($data, $id)
	 */
	public function save($data, $id = null){
		$id = parent::save($data, $id);
		
		// removendo os produtos
		$this->db
			->where('ID_GRUPO', $id)
			->delete('TB_GRUPO_PRODUTO');
			
		// removendo as categorias
		$this->db
			->where('ID_GRUPO', $id)
			->delete('TB_GRUPO_CATEGORIA');
			
		// reatribuindo os produtos
		if( !empty($data['produtos']) ){
			foreach($data['produtos'] as $iditem){
				$item = array();
				$item['ID_GRUPO'] = $id;
				$item['ID_PRODUTO'] = $iditem;
				$this->db->insert('TB_GRUPO_PRODUTO', $item);
			}
		}
		
		// reatribuindo as categorias
		if( !empty($data['categorias']) ){
			foreach($data['categorias'] as $iditem){
				$item = array();
				$item['ID_GRUPO'] = $id;
				$item['ID_CATEGORIA'] = $iditem;
				$this->db->insert('TB_GRUPO_CATEGORIA', $item);
			}
		}
		
	}
	
	/**
	 * Lista os itens cadastrados
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $filters array com os filtros
	 * @param int $page numero da pagina
	 * @param int $limit numero de itens por pagina
	 * @return array
	 */
	public function listItems($filters, $page=0, $limit=5){
		$this->db
			->join('TB_CLIENTE C','TB_GRUPO.ID_CLIENTE = C.ID_CLIENTE','LEFT')
			->join('TB_AGENCIA A','TB_GRUPO.ID_AGENCIA = A.ID_AGENCIA','LEFT');
		
		$this->setWhereParams($filters,true);
		return $this->execute($page, $limit, 'DESC_GRUPO');
	}
	
	/**
	 * Recupera o registro pela chave
	 * @author Juliano Polito
	 * @param string $key nome da chave
	 * @return array Array contendo os valores do registro
	 */
	public function getByKey($key){
		$rs = $this->db->where('CHAVE_GROUP',$key)->get($this->tableName);
		
		if($rs->num_rows() == 0){
			return array();
		}
		
		return $rs->row_array();
	}
	
	/**
	 * Recupera o ID do status pela chave
	 * @author Juliano Polito
	 * @param $key A chave do status
	 * @return O ID caso encontre, ou null caso nÃ£o encontre
	 */
	public function getIdByKey($key){
		$row = $this->getByKey($key);		
		return $row["ID_GROUP"];
	}
	
	/**
	 * Seleciona dados do Grupo Pelo ID
	 * @author Fábio Liossi
	 * @param $id codigo do Grupo
	 * @return
	 */
	public function getGrupo($id){
		$sql = "SELECT * FROM TB_GRUPO WHERE ID_GRUPO = $id";
		$result = $this->db->query($sql);	
		return $result->row_array();
	}
	
	
	
	
	/**
	 * Salva lista de Categorias do Grupo
	 * @author Fábio Liossi
	 * @param $key A chave do grupo, $arrCat lista de id das Categorias
	 * @return
	 */
	public function insertCategorias($key, $arrCat){
		$sql = "DELETE FROM TB_GRUPO_CATEGORIA WHERE ID_GRUPO = $key";	
		$rs = $this->db->query($sql);	

		
		foreach($arrCat as $cat){
			$sql = "INSERT INTO TB_GRUPO_CATEGORIA VALUES ($key, $cat);";
			$rs = $this->db->query($sql);			
		}	
	}
	

	/**
	 * Recupera os grupos pelo codigo do cliente
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idcliente
	 * @return array
	 */
	public function getByCliente($idcliente){
		$res = $this->db
			->where('ID_CLIENTE', $idcliente)
			->where('STATUS_GRUPO', 1)
			->get($this->tableName)
			->result_array();
			
		return $res;
	}
	
	/**
	 * Recupera os grupos pelo coidgo da agencia
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idagencia
	 * @return array
	 */
	public function getByAgencia($idagencia){
		$res = $this->db
			->where('ID_AGENCIA', $idagencia)
			->where('STATUS_GRUPO', 1)
			->get($this->tableName)
			->result_array();
			
		return $res;
	}
	
	/**
	 * Recupera as permissoes de um grupo
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idgrupo
	 * @return array
	 */
	public function getPermissoes($idgrupo){
		$rs = $this->db
			->join('TB_PERMISSAO P','P.ID_GRUPO = G.ID_GRUPO')
			->join('TB_FUNCTION F','F.ID_FUNCTION = P.ID_FUNCTION')
			->where('P.ID_GRUPO', $idgrupo)
			->order_by('DESC_FUNCTION')
			->get($this->tableName . ' G')
			->result_array();
			
		return $rs;
	}

	/**
	 * Recupera uma permissão do um grupo
	 *
	 * @author Diego Andrade
	 * @param int $idgrupo
	 * @return array
	 */
	public function getPermission($idGrupo,$function){
		$rs = $this->db
			->join('TB_PERMISSAO P','P.ID_GRUPO = G.ID_GRUPO')
			->join('TB_FUNCTION F','F.ID_FUNCTION = P.ID_FUNCTION')
			->where('P.ID_GRUPO', $idGrupo)
			->where('F.CHAVE_FUNCTION', $function)
			->order_by('DESC_FUNCTION')
			->get($this->tableName . ' G')
			->result_array();

		return $rs;
	}
	
	/**
	 * Grava as permissoes para o grupo
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do grupo
	 * @param array $functions Lista contendo os codigos das funcoes
	 * @return void
	 */
	public function salvarPermissoes($id, array $functions){
		// primeiro, remove as permissoes do grupo que nao estao selecionadas
		$this->db->where('ID_GRUPO', $id)
			->where_not_in('ID_FUNCTION', $functions)
			->delete('TB_PERMISSAO');
			
		// agora vamos ver se cada uma das selecionadas 
		// ja esta no banco. se nao estiver, colocamos
		foreach($functions as $fnc){
			$total = $this->db->where('ID_GRUPO', $id)
				->where('ID_FUNCTION', $fnc)
				->get('TB_PERMISSAO')
				->num_rows();
				
			if($total == 0){
				$data = array(
					'ID_FUNCTION' => $fnc,
					'ID_GRUPO' => $id
				);
				
				$this->permissao->save($data);
			}
		}
		
	}
	
}

