<?php
/**
 * Model que representa a tabela TB_CATEGORIA
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */

class CategoriaDB extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_CATEGORIA','int','',1,1);
		$this->addField('DESC_CATEGORIA','string','',6,0);
		$this->addField('CODIGO_CATEGORIA','string','',1,0);
		$this->addField('ID_CLIENTE','int','',1,0);
		$this->addField('FLAG_ACTIVE_CATEGORIA','int',1,1,0);
	}
	### END

	var $tableName = 'TB_CATEGORIA';

	/**
	 * Construtor
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return CategoriaDB
	 */
	function __construct(){
		parent::GenericModel();

		$this->addValidation('ID_CLIENTE','requiredNumber','Informe o cliente');
		$this->addValidation('DESC_CATEGORIA','requiredString','Informe o nome');
		$this->addValidation('DESC_CATEGORIA','function',array($this,'checaNomeDuplicado'));
		$this->addValidation('CODIGO_CATEGORIA','requiredString','Informe o código');
		$this->addValidation('CODIGO_CATEGORIA','function',array($this,'checaCodigoDuplicado'));

	}

	/**
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param $id
	 * @return unknown_type
	 */
	public function getDescricao($id){
		$rs = $this->db->where('ID_CATEGORIA', $id)
			->get($this->tableName)
			->row_array();

		return empty($rs['DESC_CATEGORIA']) ? '' : $rs['DESC_CATEGORIA'];
	}

	/**
	 * Lista as categorias cadastradas
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $filters
	 * @param int $page Pagina atual
	 * @param int $limit numero de itens por pagina
	 * @return array
	 */
	public function listItems($filters, $page=0, $limit=5){
		$this->db->join('TB_CLIENTE C','C.ID_CLIENTE = TB_CATEGORIA.ID_CLIENTE','LEFT');
		if(isset($filters['FLAG_ACTIVE_CATEGORIA']) && $filters['FLAG_ACTIVE_CATEGORIA'] === '0'){
			$this->db->where('FLAG_ACTIVE_CATEGORIA', 0);
		}
		$this->setWhereParams($filters);
		return $this->execute($page,$limit,'FLAG_ACTIVE_CATEGORIA DESC, DESC_CLIENTE,DESC_CATEGORIA');
	}

	/**
	 * Recupera as subcategorias
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idCategoria Codigo da categoria
	 * @return array
	 */
	public function getSubcategorias($idCategoria){
		return $this->subcategoria->getByCategoria($idCategoria);
	}

	/**
	 * Recupera as categorias de um cliente
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idCliente Codigo do cliente
	 * @return array
	 */
	public function getByCliente($idCliente){
		$rs = $this->db
			->where('ID_CLIENTE', $idCliente)
			->where('FLAG_ACTIVE_CATEGORIA', 1)
			->order_by('DESC_CATEGORIA')
			->get($this->tableName);

		return $rs->result_array();
	}

	/**
	 * Recupera as categorias pelo codigo do produto
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idProduto codigo do produto
	 * @return array
	 */
	public function getByProduto($idProduto){
		$rs = $this->db->where('ID_PRODUTO', $idProduto)
			->where('FLAG_ACTIVE_CATEGORIA', 1)
			->order_by('DESC_CATEGORIA')
			->get($this->tableName);

		return $rs->result_array();
	}

	public function checaCodigoDuplicado($data){
		$total = $this->db->where('ID_CLIENTE',$data['ID_CLIENTE'])
			->where('CODIGO_CATEGORIA',$data['CODIGO_CATEGORIA'])
			->where('ID_CATEGORIA !=', $data['ID_CATEGORIA'])
			->get($this->tableName)
			->num_rows();

		if($total > 0){
			return 'Já existe uma categoria com este codigo para este cliente';
		}

		return true;
	}

	public function checaNomeDuplicado($data){
		$total = $this->db->where('ID_CLIENTE',$data['ID_CLIENTE'])
			->where('DESC_CATEGORIA',$data['DESC_CATEGORIA'])
			->where('ID_CATEGORIA !=', $data['ID_CATEGORIA'])
			->get($this->tableName)
			->num_rows();

		if($total > 0){
			return 'Já existe uma categoria com este nome para este cliente';
		}

		return true;
	}

	/**
	 * recupera categorias
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param $lista
	 * @return array
	 */
	public function getCategorias(array $lista){
		$result = array();
		$rs = $this->db->where_in('ID_CATEGORIA', $lista)
			->where('FLAG_ACTIVE_CATEGORIA', 1)
			->order_by('DESC_CATEGORIA')
			->get($this->tableName);

		foreach($rs->result_array() as $item){
			$result[$item['ID_CATEGORIA']] = $item['DESC_CATEGORIA'];
		}

		return $result;
	}

	/***********************************************************/
	/******************** FUNCOES ANTIGAS **********************/
	/***********************************************************/

	function getAllSub($id)
	{
		$this->db->order_by('DESC_SUBCATEGORIA', 'ASC');
		$this->db->where('ID_CATEGORIA', $id)
			 ->where('FLAG_ACTIVE_SUBCATEGORIA', 1)
			 ->where('FLAG_ACTIVE_CATEGORIA', 1);
		$rs = $this->db->get('TB_SUBCATEGORIA');

		if ( $rs->num_rows() > 0 )
			return $rs->result_array();
		else
			return array();
	}
	function getAllByUser($id)
	{
		return $this->db->getwhere('TB_USER_CATEGORIA',array('ID_USER'=>$id))->result_array();
	}
	function getByLike($dados = null, $pagina=0, $limit=null)
	{

		$sql = 	' SELECT * FROM TB_CATEGORIA ' .
			 	' WHERE 1 = 1';

		if( isset($dados['DESC_CATEGORIA']) && $dados['DESC_CATEGORIA']!= null)
			$sql.=" AND DESC_CATEGORIA LIKE '%".$dados['DESC_CATEGORIA']."%'";

		return $this->executeQuery($sql,$pagina, $limit, 'DESC_CATEGORIA', 'ASC');
	}
	function getAllByUserAndCarrinhoInCategoria($id_carrinho)
	{
		if($this->session->userdata['usuario_session']['ID_GROUP']==$this->config->item('grupo_categoria'))
		{
			/**
			 * Caso o usuario tenha alguma categoria na tb_user_categoria, ent�o exibe apenas essas categorias
			 */
			 $data = $this->db->getwhere('TB_USER_CATEGORIA',array('ID_USER'=>$this->session->userdata['usuario_session']['ID_USER']));
			 if($data->num_rows()>0)
			 {
			 	$categorias = 'SELECT ID_CATEGORIA FROM TB_USER_CATEGORIA WHERE ID_USER = '.$this->session->userdata['usuario_session']['ID_USER'];
			 }else{
			 	$categorias = 'SELECT ID_CATEGORIA FROM TB_CATEGORIA WHERE FLAG_ACTIVE_CATEGORIA  = 1';
			 }

		}else{
			$categorias = 'SELECT ID_CATEGORIA FROM TB_CATEGORIA';
		}

		$sql = ' 	SELECT *
					FROM TB_CATEGORIA
					where ID_CATEGORIA IN (
	                        SELECT ID_CATEGORIA
	                        FROM TB_QTD_CATEGORIA_PAGINACAO
	                        JOIN TB_CARRINHO ON TB_CARRINHO.ID_PAGINACAO = TB_QTD_CATEGORIA_PAGINACAO.ID_PAGINACAO
	                        WHERE TB_CARRINHO.ID_CARRINHO = '.$id_carrinho.'
	                        AND TB_QTD_CATEGORIA_PAGINACAO.ID_CATEGORIA IN
                            (
                                '.$categorias.'
                            )
                       )';

        $recordset = $this->db->query($sql);
        return $recordset->result_array();
	}


	function getAllByUserInCategoria($id_user)
	{

//		if($this->session->userdata[USUARIO]['ID_GROUP']==$this->config->item('grupo_categoria'))
//		{
//
//			/**
//			 * Caso o usuario tenha alguma categoria na tb_user_categoria, ent�o exibe apenas essas categorias
//			 */
//			 $data = $this->db->getwhere('TB_USER_CATEGORIA',array('ID_USER'=>$this->session->userdata['usuario_session']['ID_USER']));
//			 if($data->num_rows()>0)
//			 {
//			 	$sql = ' SELECT * FROM '.$this->tableName.' ' .
//			 		   ' INNER JOIN TB_USER_CATEGORIA ON TB_USER_CATEGORIA.ID_CATEGORIA = '.$this->tableName.'.ID_CATEGORIA' .
//			 		   ' WHERE TB_USER_CATEGORIA.ID_USER = '.$this->session->userdata['usuario_session']['ID_USER'];
//			 	$data = $this->db->query($sql);
//			 	return $data->result_array();
//			 }else{
//			 	/**
//			 	 * Significa que o usuario nao tem nenhum categoria associado, ent�o, poder� ver todas as categorias
//			 	 */
//				$data = $this->db->get($this->tableName);
//				return $data->result_array();
//			 }
//		}else{
			$data = $this->db->get($this->tableName);
			return $data->result_array();
		//}
	}

	function getSelecionado($id_usuario)
	{
		$sql = ' SELECT * ' .
			   ' FROM TB_CATEGORIA ' .
			   ' WHERE ID_CATEGORIA ' .
			   '	IN(' .
			   '		SELECT ID_CATEGORIA ' .
			   '		FROM TB_USER_CATEGORIA ' .
			   '		WHERE ID_USER = '.$id_usuario.' ' .
			   		') ' .
			   ' AND FLAG_ACTIVE_CATEGORIA = 1'.
			   ' ORDER BY DESC_CATEGORIA ASC';

		$rs = $this->db->query($sql);

		if ( $rs->num_rows() > 0 )
			return $rs->result_array();
		else
			return false;
	}
	function getDisponivel($id_usuario)
	{
		$sql = 	' SELECT * FROM TB_CATEGORIA ' .
				'  WHERE ID_CATEGORIA NOT IN ' .
				' (' .
				'   SELECT TB_USER_CATEGORIA.ID_CATEGORIA ' .
				'   FROM TB_USER_CATEGORIA ' .
				'   WHERE ID_USER = '. $id_usuario .
				' ) ' .
  			    ' AND FLAG_ACTIVE_CATEGORIA = 1'.
				' ORDER BY TB_CATEGORIA.DESC_CATEGORIA ASC ';


		$rs = $this->db->query($sql);

		if ( $rs->num_rows() > 0 )
			return $rs->result_array();
		else
			return false;
	}

	function getByDesc($desc){
		$this->db->like('DESC_CATEGORIA', $desc)
				 ->where('FLAG_ACTIVE_CATEGORIA', 1);
		$data = $this->db->get($this->tableName);
		if($data->num_rows()>=1)
			return $data->row_array();
		return false;
	}

	/**
	 * recupera as categorias / subcategorias por praca para um plano de midia
	 *
	 * <p>Sempre recupera os valores da ultima versao de excel</p>
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idcliente
	 * @param int $idjob
	 * @param int $idpraca
	 * @return array
	 */
	public function getForPlanoMidia($idcliente, $idjob, $idpraca, $idusuario){
		$idpraca = sprintf('%d', $idpraca);
		$idjob = sprintf('%d', $idjob);
		$idusuario = sprintf('%d', $idusuario);
		$user = $this->usuario->getById($idusuario);

		$sub = 'SELECT ID_EXCEL_VERSAO
			FROM TB_EXCEL_VERSAO
			JOIN TB_EXCEL USING(ID_EXCEL)
			WHERE TB_EXCEL.ID_EXCEL = EC.ID_EXCEL
			AND TB_EXCEL.ID_JOB = '.$idjob.'
			ORDER BY ID_EXCEL_VERSAO DESC LIMIT 1';

		// se for cliente
		if( !empty($user['ID_CLIENTE']) ){
			// se tiver categorias associadas ao grupo
			if( count($this->getByGrupo($user['ID_GRUPO'])) > 0 ){
				// vamos filtrar somente as categorias que ele tem acesso
				$this->db
					->join('TB_GRUPO_CATEGORIA GC','GC.ID_CATEGORIA = C.ID_CATEGORIA')
					->where('GC.ID_GRUPO', $user['ID_GRUPO']);
			}
		}

		$rs = $this->db
			->select('*, SC.ID_SUBCATEGORIA')
			->join('TB_SUBCATEGORIA SC','SC.ID_CATEGORIA = C.ID_CATEGORIA')
			->join('TB_EXCEL_CATEGORIA EC','EC.ID_SUBCATEGORIA = SC.ID_SUBCATEGORIA AND ('.$sub.') = EC.ID_EXCEL_VERSAO AND EC.ID_PRACA = '.$idpraca,'LEFT')
			->join('TB_EXCEL E','E.ID_EXCEL = EC.ID_EXCEL AND E.ID_JOB = '.$idjob,'LEFT')

			->where('C.ID_CLIENTE', $idcliente)
			->where('C.FLAG_ACTIVE_CATEGORIA', 1)
			->where('SC.FLAG_ACTIVE_SUBCATEGORIA', 1)
			->order_by('C.DESC_CATEGORIA ASC, SC.DESC_SUBCATEGORIA ASC')
			->get($this->tableName.' C')
			->result_array();

		return $rs;
	}

	/**
	 * Recupera as categorias que foram escolhidas em um plano de midia
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idjob Codigo do job relacionado
	 * @return array
	 */
	public function getByPlanoMidia($idjob){
		$idjob = sprintf('%d', $idjob);

		$sub = 'SELECT ID_EXCEL_VERSAO
			FROM TB_EXCEL_VERSAO
			JOIN TB_EXCEL USING(ID_EXCEL)
			WHERE TB_EXCEL.ID_EXCEL = EC.ID_EXCEL
			AND TB_EXCEL.ID_JOB = '.$idjob.'
			ORDER BY ID_EXCEL_VERSAO DESC LIMIT 1';

		$rs = $this->db
			->select('C.*')
			->join('TB_SUBCATEGORIA SC','SC.ID_CATEGORIA = C.ID_CATEGORIA')
			->join('TB_EXCEL_CATEGORIA EC','EC.ID_SUBCATEGORIA = SC.ID_SUBCATEGORIA AND ('.$sub.') = EC.ID_EXCEL_VERSAO')
			->join('TB_EXCEL E','E.ID_EXCEL = EC.ID_EXCEL AND E.ID_JOB = '.$idjob)

			//->where('C.ID_CLIENTE', $idcliente)
			
			->where('C.FLAG_ACTIVE_CATEGORIA', 1)
			->where('SC.FLAG_ACTIVE_SUBCATEGORIA', 1)
			->group_by('C.ID_CATEGORIA')
			->order_by('C.DESC_CATEGORIA ASC')
			->get($this->tableName.' C')
			->result_array();

		return $rs;
	}

	/**
	 * Recupera as categorias por usuario e cliente
	 *
	 * @author Hugo Ferreira da Silva
	 * @param int $idusuario
	 * @param int $idcliente
	 * @return array
	 */
	public function getByUsuarioCliente($idusuario, $idcliente){
		$rs = $this->db->join('TB_GRUPO_CATEGORIA TGC', 'TGC.ID_CATEGORIA = C.ID_CATEGORIA')
			->join('TB_GRUPO G','G.ID_GRUPO = TGC.ID_GRUPO')
			->join('TB_USUARIO U','G.ID_GRUPO = U.ID_GRUPO')
			->select('C.*')
			->where('C.FLAG_ACTIVE_CATEGORIA', 1)
			->where('U.ID_USUARIO', $idusuario)
			->where('G.ID_CLIENTE', $idcliente)
			->order_by('DESC_CATEGORIA')
			->get($this->tableName . ' C');
			
		if( $rs->num_rows() == 0 ){
			return $this->getByCliente($idcliente);
		}

		return $rs->result_array();
	}

	/**
	 * Recupera as categorias por ID de grupo
	 *
	 * @author Fabio Liossi
	 * @link http://www.247id.com.br
	 * @param int $idGrupo Codigo do grupo
	 * @return array
	 */
	public function getByGrupo($idGrupo){
		$rs = $this->db->join('TB_GRUPO_CATEGORIA TGC', 'TGC.ID_CATEGORIA = TB_CATEGORIA.ID_CATEGORIA')
			->where('TGC.ID_GRUPO', $idGrupo)
			->where('FLAG_ACTIVE_CATEGORIA', 1)
			->order_by('DESC_CATEGORIA')
			->get($this->tableName);
		return $rs->result_array();
	}

}
