<?php
/**
 * Classe de modelo para o gerenciamento de clientes
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class JobUsuarioDB extends GenericModel
{
	### START
	protected function _initialize()
	{
		$this->addField('ID_TB_JOB_USUARIOS','int','',1,1);
		$this->addField('ID_JOB','int','',1,1);
		$this->addField('ID_USUARIO_RESPONSAVEL','int','',1,1);
	}
	### END

	var $tableName = 'TB_JOB_USUARIOS';

	/**
	 * Construtor
	 *
	 * @author Diego Andrade
	 * @link http://www.247id.com.br
	 * @return JobUsuarioDB
	 */
	function __construct(){
		parent::GenericModel();
	}

	/**
	 * Salva os Usuários Responsáveis do JOB
	 *
	 * @author Diego Andrade
	 * @param int $idJob
	 * @param string/array $usuarios
	 * @return void
	 */
	public function salvaJobUsuario($idJob,$usuarios)
	{
		if ((int) $idJob > 0) {
			$this->deleteJobUsuario($idJob);
			if (is_array($usuarios)) {
				foreach ($usuarios as $usuario) {
					if (trim($usuario) != "") {
						$data = array(
							"ID_JOB" => $idJob,
							"ID_USUARIO_RESPONSAVEL" => $usuario,
						);
						$this->db->insert($this->tableName, $data);
					}
				}
			} elseif (trim($usuarios) != "") {
                $data = array(
                    "ID_JOB" => $idJob,
                    "ID_USUARIO_RESPONSAVEL" => $usuarios,
                );
                $this->db->insert($this->tableName, $data);
            } #ENDIF
		}
	}

	/**
	 * Deleta os Usuarios Responsáveis do JOB
	 *
	 * @author Diego Andrade
	 * @param int $idJob
	 * @return void
	 */
	public function deleteJobUsuario($idJob)
	{
		$this->db
			->where('ID_JOB',$idJob)
			->delete($this->tableName);
	}

	/**
	 * Verifica se o usuário tem permissão de ver o JOB
	 *
	 * @author Diego Andrade
	 * @param array $listas
	 * @param boolean $verTodos
	 * @return array
	 */
	public function viewJobUsuario(array $listas, $verTodos = false)
	{
		if ($verTodos) return $listas;
		$user = Sessao::get('usuario');
		// pegamos todos os usuarios desse Job
		foreach ($listas as $key => $value) {
			$permissions = $this->db
				->select('ID_USUARIO_RESPONSAVEL')
				->where('ID_JOB', $value['ID_JOB'])
				->where_not_in('ID_USUARIO_RESPONSAVEL',array('is null',0))
				->get($this->tableName)
				->result_array();
			if (count($permissions) <= 0) continue;
			foreach ($permissions as $permission) {
				if ($permission['ID_USUARIO_RESPONSAVEL'] != $user['ID_USUARIO']) unset($listas[$key]);
			}

		}
		return $listas;
	}

	/**
	 * Recupera os usuários responsáveis do Job
	 *
	 * @author Diego Andrade
	 * @param int $idJob
	 * @return array
	 */
	public function getUsers($idJob)
	{
		// pegamos os usuários responsáveis
		$list = $this->db->from($this->tableName.' JB')
            ->select('JB.*,U.*')
            ->join('TB_USUARIO U','U.ID_USUARIO = JB.ID_USUARIO_RESPONSAVEL AND U.STATUS_USUARIO = 1')
			->where('JB.ID_JOB', $idJob)
			->get()
			->result_array();

        return $list;
	}

	function updateAgenciasClientes($cliente, $agencia){
		if(is_numeric($cliente)){
			$this->db->delete($this->tableName, array("ID_CLIENTE" => $cliente));
			foreach($agencia as $idAgencia){
				$this->db->insert($this->tableName, array("ID_CLIENTE" => $cliente, "ID_AGENCIA" => $idAgencia));
			}
		}
		else if(is_numeric($agencia)){
			$this->db->delete($this->tableName, array("ID_AGENCIA" => $agencia));
			foreach($cliente as $idCliente){
				$this->db->insert($this->tableName, array("ID_CLIENTE" => $idCliente, "ID_AGENCIA" => $agencia));
			}
		}
	}
}

