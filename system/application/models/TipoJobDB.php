<?php
class TipoJobDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_TIPO_JOB','int','',1,1);
		$this->addField('DESC_TIPO_JOB','string','',9,0);
	}
	### END

	var $tableName = 'TB_TIPO_JOB';

	/**
	 * Construtor 
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return TipoJobDB
	 */
	function __construct(){
		parent::GenericModel();
		
		
	}
	
	/**
	 * Lista os itens cadastrados
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $filters array com os filtros
	 * @param int $page numero da pagina
	 * @param int $limit numero de itens por pagina
	 * @return array
	 */
	public function listItems($filters, $page=0, $limit=5){
		$this->setWhereParams($filters);
		return $this->execute($page, $limit, 'DESC_TIPO_JOB');
	}
	
	
}

