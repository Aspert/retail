<?php
class Obj_fichaDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_OBJ_FICHA','int','',2,1);
		$this->addField('ID_FICHA','int','',1,0);
		$this->addField('FILEID_OBJ_FICHA','int','',1,0);
		$this->addField('FILE_OBJ_FICHA','string','',10,0);
		$this->addField('PATH_OBJ_FICHA','string','',32,0);
		$this->addField('FILEID_OBJ_FICHA','int','',1,0);
		$this->addField('FLAG_MAIN_OBJ_FICHA','string','',1,0);
		$this->addField('ID_TIPO_OBJETO','int','',1,0);
	}
	### END


	var $tableName = 'TB_OBJ_FICHA';
	
	function getByID($id)
	{
		$this->db->where('ID_OBJ_FICHA', $id);
		$this->db->join('TB_TIPO_OBJETO','TB_TIPO_OBJETO.ID_TIPO_OBJETO = TB_OBJ_FICHA.ID_TIPO_OBJETO','LEFT');
		$rs = $this->db->get($this->tableName);
			if ($rs->num_rows() == 1)
				return $rs->row_array();
			else
				return null;		
	}
	
	/**
	 * Retorna quais fichas o objeto pertence
	 * @author juliano.polito
	 * @param $path
	 * @param $file
	 * @return array
	 */
	function getByObjeto($path,$file){
		$rs = $this->db->distinct()
						->join('TB_FICHA F','F.ID_FICHA = OF.ID_FICHA','INNER')
						->join('TB_CATEGORIA C','C.ID_CATEGORIA = F.ID_CATEGORIA','INNER')
						->join('TB_SUBCATEGORIA SC','SC.ID_SUBCATEGORIA = F.ID_SUBCATEGORIA','INNER')
						->join('TB_APROVACAO_FICHA AF','AF.ID_APROVACAO_FICHA = F.ID_APROVACAO_FICHA','INNER')
						->where('OF.FILE_OBJ_FICHA', $file)
						->where('OF.PATH_OBJ_FICHA', $path)
						->get($this->tableName.' OF');
		return $rs->result_array();
	}
	
	/**
	 * Retorna true se o objeto esta associado a alguma ficha
	 * @author juliano.polito
	 * @param $path
	 * @param $file
	 * @return bool
	 */
	function objetoExists($path,$file){
		$count = $this->db->from($this->tableName)
						->where('FILE_OBJ_FICHA', $file)
						->where('PATH_OBJ_FICHA', $path)
						->count_all_results();
		return ($count >= 1);
	}
	
	function getByFicha($id)
	{
		$this->db->where('ID_FICHA', $id);
		$rs = $this->db->get($this->tableName);
		return $rs->result_array();
		
	}
	
	function getFileIDSByFichas(array $ids){
		//para casos em que não há fichas cadastradas
		$ids[] = 0;
		$this->db->select('OF.FILEID_OBJ_FICHA')
				 ->join('TB_FICHA F','F.ID_FICHA = OF.ID_FICHA')
				 ->where_in('OF.ID_FICHA', $ids);
		$rs = $this->db->get($this->tableName.' OF')->result_array();
		if(!empty($rs)){
			return (array) array_unique(array_values(arraytoselect($rs,'FILEID_OBJ_FICHA','FILEID_OBJ_FICHA')));
		}else{
			return array();
		}
	}
	
	function getAllByFicha($id)
	{
		$this->db->where('ID_FICHA', $id);		
		$rs = $this->db->get($this->tableName)->result_array();
		
		foreach($rs as $key => $item){
			$data = $this->general->getByFileNameAndPath( $item['FILE_OBJ_FICHA'], utf8MAC($item['PATH_OBJ_FICHA']) );
			if(!empty($data)){
				$item['TIPO'] = $this->tipo_objeto->getDescricao($data['TIPO']);
			} else {
				$item['TIPO'] = '';
			}
			$rs[$key] = $item;
		}
		
		return $rs;
	}
	
	function deleteByFicha($ficha)
	{
		
		if ( strlen(trim($ficha)) > 0 )
		{
			$this->db->where('ID_FICHA', $ficha);
		
			return $this->db->delete($this->tableName);
		}
		
		return false;
	}
	
	function export_relatorio($filtro = null){
		$this->load->dbutil();

		$strCVS = "";

		$strSQL = "";
		$strSQL .= " SELECT ";
		$strSQL .= " 	NOME_FICHA AS Ficha ";
		$strSQL .= " FROM ";
		$strSQL .= " 	TB_OBJ_FICHA ";
		$strSQL .= " INNER JOIN ";
		$strSQL .= " 	TB_FICHA ON TB_FICHA.ID_FICHA = TB_OBJ_FICHA.ID_FICHA ";

		if(isset($filtro) && is_array($filtro)){
			$count = 1;
			foreach($filtro as $index => $value){
				$index_temp = split(":", $index);
				$index = trim($index_temp[0]);
				$comparer_str = trim($index_temp[1]);
				$formate_str = trim($index_temp[2]);

				switch($comparer_str){
					case "major":
						$comparer = ">";
						break;
					case "majorequal":
						$comparer = ">=";
						break;
					case "minor":
						$comparer = "<";
						break;
					case "minorequal":
						$comparer = "<=";
						break;
					default:
						$comparer = "=";
						break;
				}

				switch($formate_str){
					case "date":
						$value = format_date_to_db($value, 2);
						break;
					default:
						break;
				}

				$strSQL .= ($count == 1) ? " WHERE " : " AND ";
				$strSQL .= " 	(". $index ." ". $comparer ." '". $value ."') ";

				$count++;
			}
		}

		$obj_recordset = $this->db->query($strSQL);
		if($obj_recordset && $obj_recordset->num_rows > 0){
			$strCVS .= $this->dbutil->csv_from_result($obj_recordset, ";", "\r\n");
		}
		else{
			$strCVS .= "Nenhuma ficha encontrada"."\n";
		}

		return $strCVS;
	}
	
	/**
	 * Pega o nome do tipo por path
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $str
	 * @return string 
	 */
	public function getTipoByFile($str){
		$parts = explode('/', $str);
		$file = array_pop($parts);
		$path = implode('/', $parts).'/';
		
		$fileid = $this->xinet->getFilePorPath($path, $file);
		
		if(!empty($fileid)){
			$keys = $this->general->getKeywordsByFileID($fileid);
			$tipo = $this->tipo_objeto->getById($keys['TIPO']);
			
			if(!empty($tipo)){
				return $tipo['DESC_TIPO_OBJETO'];
			}
		}
		
		return 'Imagem';
	}
	
	/**
	 * Retorna todos os objetos cadastrados por path/file
	 * 
	 * @author juliano.polito
	 * @param $file
	 * @param $path
	 * @return void
	 */
	public function getByPath($path,$file){
		$rs = $this->db->from($this->tableName . ' OF' )
			->where('OF.FILE_OBJ_FICHA',$file)
			->where('OF.PATH_OBJ_FICHA',$path)
			->get();
		return $rs->result_array();
	}
	
	/**
	 * recupera um caminho completo de arquivo
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id
	 * @return string
	 */
	public function getPathPorId($id){
		$rs = $this->getByID($id);
		$path = $rs['PATH_OBJ_FICHA'];
		if(substr($path, strlen($path)-1) == '/'){
			$path = substr($path, 0, strlen($path)-1);
		}
		
		
		return $path .'/'. $rs['FILE_OBJ_FICHA'];
	}
	
	/**
	 * Recupera as informacoes adicionais do objeto
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $item
	 * @return void
	 */
	public function getInfo(&$item){
		$path = $item['PATH_OBJ_FICHA'];
		$xinet = (array) $this->general->getByFileNameAndPath($item['FILE_OBJ_FICHA'], $path);
		
		$item = array_merge($item,$xinet);
		
		if(!empty($item['SUBCATEGORIA'])){
			$item['SUBCATEGORIA'] = $this->subcategoria->getDescricao($item['SUBCATEGORIA']);
		}
		
		if(!empty($item['CATEGORIA'])){
			$item['CATEGORIA'] = $this->categoria->getDescricao($item['CATEGORIA']);
		}
		
		if(isset($item['TIPO'])){
			$item['TIPO'] = $this->tipo_objeto->getDescricao($item['TIPO']);
		}
		
		if(isset($item['MARCA'])){
			$item['MARCA'] = $this->marca->getDescricao($item['MARCA']);
		}
	}
	
	/**
	 * Retorna os tipos relacionados a um excel, versao e praca
	 * @author Hugo Ferreira da Silva
	 * @param int $idexcel
	 * @param int $idversao
	 * @param int $idpraca
	 * @return array
	 */
	public function getTiposByExcelVersaoPraca($idexcel, $idversao, $idpraca){
		$hashset = array();
		
		// primeiro, pegamos todos os itens de uma praca
		$rs = $this->db
			->where('ID_EXCEL', $idexcel)
			->where('ID_EXCEL_VERSAO', $idversao)
			->where('ID_PRACA', $idpraca)
			->get('TB_EXCEL_ITEM')
			->result_array();
			
		// codigos de fichas
		$codes = array();
		foreach($rs as $item){
			$codes[] = $item['ID_FICHA'];
		}
		
		// se encontrou fichas
		if( !empty($codes) ){
			// pegamos todos os objetos de todas as fichas
			$rs = $this->db
				->where_in('ID_FICHA', $codes)
				->get('TB_OBJ_FICHA')
				->result_array();
				
			$codes = array();
			foreach($rs as $item){
				$codes[] = $item['FILEID_OBJ_FICHA'];
			}
			
			// se encontrou file id's para usar no xinet
			if( !empty($codes) ){
				// consultamos o xinet
				$list = $this->general->getIdTipoByCodeList($codes);
				
				foreach($list as $item){
					$hashset[$item['ID']] = $item['TIPO'];
				}
			}
		}
		
		return $hashset;
	}
	
	/**
	 * recupera as datas de alteracao dos objetos para um excel, versao e praca
	 * @author Hugo Ferreira da Silva
	 * @param int $idexcel
	 * @param int $idversao
	 * @param int $idpraca
	 * @return array
	 */
	public function getDataAlteracaoByExcelVersaoPraca($idexcel, $idversao, $idpraca){
		$hashset = array();
		
		// primeiro, pegamos todos os itens de uma praca
		$rs = $this->db
			->where('ID_EXCEL', $idexcel)
			->where('ID_EXCEL_VERSAO', $idversao)
			->where('ID_PRACA', $idpraca)
			->get('TB_EXCEL_ITEM')
			->result_array();
			
		// codigos de fichas
		$codes = array();
		foreach($rs as $item){
			$codes[] = $item['ID_FICHA'];
		}
		
		// se encontrou fichas
		if( !empty($codes) ){
			// pegamos todos os objetos de todas as fichas
			$rs = $this->db
				->where_in('ID_FICHA', $codes)
				->get('TB_OBJ_FICHA')
				->result_array();
				
			$codes = array();
			foreach($rs as $item){
				$codes[] = $item['FILEID_OBJ_FICHA'];
			}
			
			// se encontrou file id's para usar no xinet
			if( !empty($codes) ){
				// consultamos o xinet
				$list = $this->general->getDataAlteracaoByCodeList($codes);
				$hashset = arraytoselect($list,'FileID', 'ModifyDate');
			}
		}
		
		return $hashset;
	}
	
}
