<?php
class PaginacaoDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_PAGINACAO','int','',1,1);
		$this->addField('ID_USER','int','',2,0);
		$this->addField('ID_MIDIA','int','',1,0);
		$this->addField('ID_VEICULO','int','',2,0);
		$this->addField('ID_SITUACAO_PAGINACAO','int','',1,0);
		$this->addField('ID_FORMATO','int','',2,0);
		$this->addField('ID_PRACA','int','',1,0);
		$this->addField('ID_CLASSIFICACAO','int','',1,0);
		$this->addField('ID_TIPO_PAGINACAO','int','',1,0);
		$this->addField('ID_ETAPA','int','',1,0);
		$this->addField('ID_STATUS','int','',1,0);
		$this->addField('DESC_PAGINACAO','string','',11,0);
		$this->addField('BRIFING_PAGINACAO','blob','',0,0);
		$this->addField('NUMERO_PAGINACAO','int','',1,0);
		$this->addField('QTD_SKU_PAGINACAO','int','',1,0);
		$this->addField('OBS_PAGINACAO','blob','',0,0);
		$this->addField('OBS_PREPAGINACAO','blob','',9,0);
		$this->addField('DT_INSERT_PAGINACAO','timestamp','',19,0);
		$this->addField('DT_VEICULACAO_PAGINACAO','timestamp','',19,0);
		$this->addField('FLAG_SITUACAO_PAGINACAO','string','',1,0);
		$this->addField('FLAG_ACTIVE_PAGINACAO','string','',1,0);
		$this->addField('FLAG_ATIVO','int','',1,0);
		$this->addField('FLAG_PRICING_COMPLETO','int','',1,0);
		$this->addField('NUM_DOWNLOAD','int','',1,0);
	}
	### END

	var $tableName = 'TB_PAGINACAO';
	
	public static $arrayTraducao = array("COLOR_ELEM_FICHA"=>"color",
									"X_ELEM_FICHA"=>"x",
									"Y_ELEM_FICHA"=>"y",
									"W_ELEM_FICHA"=>"largura",
									"H_ELEM_FICHA"=>"altura",
									"R_ELEM_FICHA"=>"rotacao",
									"FONT_FAMILY_ELEM_FICHA"=>"fonte",
									"FONT_SIZE_ELEM_FICHA"=>"fonte_tamanho",
									"INDEX_ELEM_FICHA"=>"index",
									"LINK"=>"link",
									"CONTEUDO"=>"conteudo",
									"FONTE_COR"=>"fonte_cor"
									);
	
	function getById($id=null){
		
		if($id==null){
			return null;
		}
		if ( isset($this->statusField) ){
			$this->db->where($this->statusField, MODEL_STATUS_ATIVO);
		}
		
		$this->db->join('TB_SITUACAO_PAGINACAO', 'TB_SITUACAO_PAGINACAO.ID_SITUACAO_PAGINACAO = TB_PAGINACAO.ID_SITUACAO_PAGINACAO');
		$this->db->join('TB_TIPO_PAGINACAO', 'TB_TIPO_PAGINACAO.ID_TIPO_PAGINACAO = TB_PAGINACAO.ID_TIPO_PAGINACAO','LEFT');
		$this->db->join('TB_PRACA', 'TB_PRACA.ID_PRACA = TB_PAGINACAO.ID_PRACA','LEFT');
		$this->db->join('TB_MIDIA', 'TB_MIDIA.ID_MIDIA = TB_PAGINACAO.ID_MIDIA','LEFT');
		$this->db->join('TB_CLASSIFICACAO', 'TB_CLASSIFICACAO.ID_CLASSIFICACAO = TB_PAGINACAO.ID_CLASSIFICACAO','LEFT');
		$this->db->join('TB_VEICULO', 'TB_VEICULO.ID_VEICULO = TB_PAGINACAO.ID_VEICULO','LEFT');
		$this->db->join('TB_FORMATO', 'TB_FORMATO.ID_FORMATO = TB_PAGINACAO.ID_FORMATO','LEFT');
		$this->db->where($this->primaryKey, $id);
		$rs = $this->db->get($this->tableName);

		if ( $rs->num_rows() == 1 ){
			return $rs->row_array();
		}else{
			return null;
		}
	}
	/*
	 * Metodo especifico para o modulo inicio 
	 */	 
	function getAllByUser($user)
	{
		$sql = 	' SELECT * ' .
				' FROM TB_PAGINACAO ' .
				' JOIN TB_SITUACAO_PAGINACAO ON ' .
				' ( TB_PAGINACAO.ID_SITUACAO_PAGINACAO = TB_SITUACAO_PAGINACAO.ID_SITUACAO_PAGINACAO ) ' .
				' WHERE ID_USER = ' . addslashes($user) . ' ' .
				' AND TB_PAGINACAO.FLAG_ACTIVE_PAGINACAO = 1' .
				' ORDER BY TB_PAGINACAO.DT_INSERT_PAGINACAO DESC ' .
				' LIMIT 10 ';
		
		$rs = $this->db->query($sql);
		
		if ( $rs->num_rows() > 0 )
			return $rs->result_array();
		else
			return false;
	}
	/*
	 * Metodo especifico para o modulo inicio 
	 */
	function getAllByStatus($aprovacao)
	{
		$sql = 	' SELECT * ' .
				' FROM TB_PAGINACAO ' .
				' INNER JOIN TB_CARRINHO ON (TB_CARRINHO.ID_PAGINACAO =TB_PAGINACAO.ID_PAGINACAO)' .
				' JOIN TB_SITUACAO_PAGINACAO ON (TB_PAGINACAO.ID_SITUACAO_PAGINACAO = TB_SITUACAO_PAGINACAO.ID_SITUACAO_PAGINACAO) ' .
				' WHERE TB_PAGINACAO.FLAG_ACTIVE_PAGINACAO = ' . addslashes($aprovacao) . 
				' AND TB_CARRINHO.FLAG_APROVACAO_PRESIDENCIA = 1 ' .
				' AND TB_CARRINHO.FLAG_APROVACAO_ELENCO = 1 ' .
				' AND TB_CARRINHO.FLAG_FULL_CARRINHO = 1 ' .				
				' ORDER BY TB_PAGINACAO.DT_INSERT_PAGINACAO DESC ' .
				' LIMIT 10 ';
		
		$rs = $this->db->query($sql);
		
		if ( $rs->num_rows() > 0 )
			return $rs->result_array();
		else
			return false;
	}
	
	function issetCarrinhoInPaginacao($idpaginacao)
	{
		$sql = 'SELECT COUNT(*) AS TOTAL FROM TB_CARRINHO WHERE ID_PAGINACAO = '.$idpaginacao;
		$row = $this->db->query($sql)->row_array();
		if($row['TOTAL']==0)
		{
			return false;
		}
		return true;			
	}
	
	function getQtdByPaginaAndPaginacao($idcategoria, $idpaginacao)
	{
		$sql = ' SELECT * ' .
			   ' FROM TB_QTD_CATEGORIA_PAGINACAO ' .
			   ' WHERE ID_PAGINACAO = "'.$idpaginacao.'"' .
			   ' AND ID_CATEGORIA = "'.$idcategoria.'"';		
		$rs = $this->db->query($sql);
		
		if ( $rs->num_rows() > 0 )
			return $rs->result_array();
		else
			return false;
	}
	
	function getByLike($dados = null, $pagina=0, $limit=null)
	{
		if ((isset($dados['DT_INSERT_PAGINACAO'])) && $dados['DT_INSERT_PAGINACAO']!=null)
			$dados['DT_INSERT_PAGINACAO'] = format_date_to_db($dados['DT_INSERT_PAGINACAO'], 2);
		
		if ((isset($dados['DT_INSERT_PAGINACAO2'])) && $dados['DT_INSERT_PAGINACAO2']!=null)	
			$dados['DT_INSERT_PAGINACAO2'] = format_date_to_db($dados['DT_INSERT_PAGINACAO2'], 2);
			
			$sql = 	' SELECT * FROM TB_PAGINACAO ' .					
				 	' LEFT JOIN TB_MIDIA ON (TB_MIDIA.ID_MIDIA =TB_PAGINACAO.ID_MIDIA)' .
				 	' LEFT JOIN TB_VEICULO ON (TB_VEICULO.ID_VEICULO =TB_PAGINACAO.ID_VEICULO)' .
				 	' LEFT JOIN TB_PRACA ON (TB_PRACA.ID_PRACA =TB_PAGINACAO.ID_PRACA)' .
				 	' LEFT JOIN TB_TIPO_PAGINACAO ON (TB_TIPO_PAGINACAO.ID_TIPO_PAGINACAO =TB_PAGINACAO.ID_TIPO_PAGINACAO)' .
				 	' JOIN TB_SITUACAO_PAGINACAO ON (TB_SITUACAO_PAGINACAO.ID_SITUACAO_PAGINACAO =TB_PAGINACAO.ID_SITUACAO_PAGINACAO)' .
				 	' INNER JOIN TB_CARRINHO ON (TB_CARRINHO.ID_PAGINACAO =TB_PAGINACAO.ID_PAGINACAO)' .
				 	' LEFT JOIN TB_FORMATO ON (TB_FORMATO.ID_FORMATO =TB_PAGINACAO.ID_FORMATO)' .
				 	' LEFT JOIN TB_APROVACAO_PAGINACAO ON TB_APROVACAO_PAGINACAO.ID_APROVACAO_PAGINACAO = TB_PAGINACAO.FLAG_ACTIVE_PAGINACAO' .				 					 	
				 	' WHERE ' .
				 	' TB_CARRINHO.FLAG_APROVACAO_ELENCO = 1	'.
				 	' AND TB_CARRINHO.FLAG_FULL_CARRINHO = 1	'.
				 	' AND TB_CARRINHO.FLAG_APROVACAO_PRESIDENCIA = 1	'; 
				 	
				if($dados['tipo_pesquisa'] == 'simples')
				{
					
					if (isset($dados['DESC_PAGINACAO']) && $dados['DESC_PAGINACAO']!=null)
						$sql.=" AND DESC_PAGINACAO LIKE '%" . addslashes($dados['DESC_PAGINACAO']) . "%' ";					
					
					if(isset($dados['FLAG_ACTIVE_PAGINACAO'])&&$dados['FLAG_ACTIVE_PAGINACAO']!='')					
						$sql.=' AND TB_PAGINACAO.FLAG_ACTIVE_PAGINACAO = '.$dados['FLAG_ACTIVE_PAGINACAO'];
					
				}
				else
				{
					if(isset($dados['FLAG_SITUACAO_PAGINACAO'])&&$dados['FLAG_SITUACAO_PAGINACAO']!=null)
						$sql.=" AND FLAG_SITUACAO_PAGINACAO = ".$dados['FLAG_SITUACAO_PAGINACAO'];
					
					if ($dados['ID_PAGINACAO']!=null)
						$sql.=" AND TB_PAGINACAO.ID_PAGINACAO = '".$dados['ID_PAGINACAO']."'";
					
					if ($dados['DESC_PAGINACAO']!=null)
						$sql.=" AND DESC_PAGINACAO LIKE '%".$dados['DESC_PAGINACAO']."%'";
						
					if ($dados['DT_INSERT_PAGINACAO']!=null)
						$sql.=" AND DT_INSERT_PAGINACAO BETWEEN '".$dados['DT_INSERT_PAGINACAO']."' AND '".$dados['DT_INSERT_PAGINACAO2']."'";
						
					if ($dados['ID_MIDIA']!=null && $dados['ID_MIDIA']!=0)
						$sql.=" AND TB_PAGINACAO.ID_MIDIA = ".$dados['ID_MIDIA'];
											
					if(isset($dados['FLAG_ACTIVE_PAGINACAO'])&&$dados['FLAG_ACTIVE_PAGINACAO']!='')
						$sql.=" AND TB_PAGINACAO.FLAG_ACTIVE_PAGINACAO = ".$dados['FLAG_ACTIVE_PAGINACAO'];
											
					(verifica($dados['ID_TIPO_PAGINACAO']))?$sql.=" AND TB_PAGINACAO.ID_TIPO_PAGINACAO = ".$dados['ID_TIPO_PAGINACAO']:'';
						
					if(isset($dados['SITUACAO']))
					{
							$sql.=" AND TB_PAGINACAO.FLAG_ACTIVE_PAGINACAO IN (";
							foreach($dados['SITUACAO'] as $id)
							{
								
								$sql.= $id . ",";
							}
							$sql.=")";
						
						$sql = str_replace(',)',')',$sql);
					}						
			}
			return $this->executeQuery($sql,$pagina, $limit, 'DT_INSERT_PAGINACAO', 'DESC');			
	}
	function getByPaginaAndPagina($id,$pagina)
	{
		$this->db->select(	' *, TB_PAGINA.ORDEM, ' .							
							' (' .
							' 	SELECT NAME_USER FROM TB_HISTORICO_FICHA ' .
							' 	JOIN TB_USER ON TB_USER.ID_USER = TB_HISTORICO_FICHA.ID_USER' .							
							' 	WHERE TB_HISTORICO_FICHA.ID_FICHA = TB_FICHA.ID_FICHA' .
							' 	ORDER BY DT_INSERT_HISTORICO DESC' .
							' 	LIMIT 1' .
							' ) AS USER_APROVACAO ,' .
							' (' .
							'	SELECT TB_OBJ_FICHA.THUMB_OBJ_FICHA' .
							'	FROM TB_OBJ_FICHA ' .
							'	WHERE TB_OBJ_FICHA.ID_FICHA = TB_FICHA.ID_FICHA' .
							'   AND TB_OBJ_FICHA.FLAG_MAIN_OBJ_FICHA =1' .
							'   LIMIT 1' .
							' ) AS THUMB_OBJ_FICHA'
						 );
		$this->db->join('TB_PAGINA', 'TB_PAGINA.ID_PAGINACAO = TB_PAGINACAO.ID_PAGINACAO');		
		$this->db->join('TB_F_C_P', 'TB_PAGINA.ID_F_C_P = TB_F_C_P.ID_F_C_P');
		$this->db->join('TB_FICHA', 'TB_FICHA.ID_FICHA = TB_F_C_P.ID_FICHA');						 
				
		
		$this->db->where('TB_PAGINACAO.ID_PAGINACAO', $id);				
		$this->db->where('TB_PAGINA.NUMERO_PAGINA', $pagina)
			->order_by('NUMERO_PAGINA ASC, TB_PAGINA.ORDEM ASC');			
		
		$rs = $this->db->get('TB_PAGINACAO');

		if ($rs->num_rows() > 0)
			return $rs->result_array();
		else
			return false;
	}
	function getAllFicha($id)
	{
		$sql = 	' SELECT TB_FICHA.COD_FICHA, ' .
				' TB_FICHA.NOME_FICHA,TB_FICHA.ID_FICHA, ' .
				' TB_FICHA.PV_ENC_FICHA,' .
				' TB_CARRINHO.DESC_CARRINHO, ' .				
				' TB_F_C_P.ID_F_C_P,' .
				' TB_PAGINA.NUMERO_PAGINA, ' .
				' TB_PRICING.ID_PRICING, ' .
				' TB_PAGINACAO.ID_PAGINACAO,' .
				' TB_OBJ_FICHA.THUMB_OBJ_FICHA' .
				' FROM TB_PAGINACAO' .
				' LEFT JOIN TB_PAGINA ON TB_PAGINA.ID_PAGINACAO = TB_PAGINACAO.ID_PAGINACAO' .
				' JOIN TB_F_C_P ON TB_F_C_P.ID_F_C_P = TB_PAGINA.ID_F_C_P' .
				' JOIN  TB_FICHA ON TB_FICHA.ID_FICHA = TB_F_C_P.ID_FICHA' .
				' JOIN TB_OBJ_FICHA ON TB_OBJ_FICHA.ID_FICHA = TB_FICHA.ID_FICHA AND TB_OBJ_FICHA.FLAG_MAIN_OBJ_FICHA' .
				' LEFT JOIN TB_PRICING ON TB_PRICING.ID_F_C_P = TB_F_C_P.ID_F_C_P' .
				' LEFT JOIN TB_CARRINHO ON TB_CARRINHO.ID_CARRINHO = TB_F_C_P.ID_CARRINHO' .				
				' WHERE TB_PAGINACAO.ID_PAGINACAO ='.$id;
		
		$rs = $this->db->query($sql);
		
		if ($rs->num_rows() > 0)
			return $rs->result_array();
		else
			return false;
	}
	
	function export($id)
	{
		$this->load->dbutil();
		
		$data = $this->getById($id);
		
		$saida = 'Descricao, '.$data['DESC_PAGINACAO'];
		$saida.= "\r\n";
		$saida.= 'Tipo, '.$data['DESC_TIPO_PAGINACAO'];
		$saida.= "\r\n";
		$saida.= 'Pra�a, '.$data['DESC_PRACA'];
		$saida.= "\r\n";
		$saida.= 'M�dia, '.$data['DESC_MIDIA'];
		$saida.= "\r\n";
		$saida.= 'Veiculo, '.$data['DESC_VEICULO'];
		$saida.= "\r\n";
		$saida.= 'Formato, '.$data['DESC_FORMATO'];
		$saida.= "\r\n";
		$saida.= 'N�mero de p�ginas, '.$data['NUMERO_PAGINACAO'];
		$saida.= "\r\n";
		$saida.= 'Briefing,'.$data['BRIFING_PAGINACAO'];
		$saida.= "\r\n";
		$res = $this->paginacao->getAllFicha($id);
		
		
		$saida.= "\r\n";
		
		$saida.= 'P�gina,Carrinho,Ficha,PV,Pricing';
		$saida.= "\r\n";
		if(is_array($res))
		{
			foreach($res as $item)
			{
				$saida.=$item['NUMERO_PAGINA'].',';
				$saida.=$item['DESC_CARRINHO'].',';
				$saida.=$item['NOME_FICHA'].',';
				$saida.=$item['PV_ENC_FICHA'].',';				
				$saida.= "\r\n";
			}			
		}
	
		return $saida;						
	}
	function setFlagAprovacaoForAgencia($idpaginacao)
	{
		$this->db->where('ID_PAGINACAO',$idpaginacao);
		$this->db->update('TB_PAGINACAO',array('FLAG_ACTIVE_PAGINACAO'=>2));
		return;
	}
	function flow($id)
	{

		$this->db->join('TB_MIDIA', 'TB_MIDIA.ID_MIDIA=TB_PAGINACAO.ID_MIDIA', 'left');
		$this->db->join('TB_PRACA', 'TB_PRACA.ID_PRACA=TB_PAGINACAO.ID_PRACA', 'left');
		$this->db->join('TB_VEICULO', 'TB_VEICULO.ID_VEICULO=TB_PAGINACAO.ID_VEICULO', 'left');
		$this->db->join('TB_FORMATO', 'TB_FORMATO.ID_FORMATO=TB_PAGINACAO.ID_FORMATO', 'left');
		$this->db->join('TB_TIPO_PAGINACAO', 'TB_TIPO_PAGINACAO.ID_TIPO_PAGINACAO=TB_PAGINACAO.ID_TIPO_PAGINACAO', 'left');
		$this->db->where('ID_PAGINACAO', $id);
		$query= $this->db->get('TB_PAGINACAO');

		$row = $query->row();
		
		$dados = array(		
		   'ID_JOB' => $id,
           'CAMPANHA_JOB' => $row->DESC_PAGINACAO,
           'ID_TIPO_JOB' => 2,
           'ID_CLIENTE' => 1,
           'ID_AGENCIA' => 1,
           'ATENDIMENTO_JOB' => 'daniel.machado@247id.com.br',
           'FLAG_CRIADO_JOB' => '0',
		   'BRIEFING_JOB' => $row->BRIFING_PAGINACAO,		
		   'TIPO_PAG_JOB' => $row->DESC_TIPO_PAGINACAO,
		   'PRACA_JOB' => $row->DESC_PRACA,
		   'MIDIA_JOB' => $row->DESC_MIDIA,
		   'VEICULO_JOB' => $row->DESC_VEICULO,
 	       'FORMATO_JOB' => $row->DESC_FORMATO,
   		   'NUMERO_PAG_JOB' => $row->NUMERO_PAGINACAO,
		   'USUARIO_ABERTURA' => $this->session->userdata['usuario_session']['LOGIN_USER'],
		   'GRUPO_ABERTURA' => 'x_dm9rj_grp_pontofrio',
		   'DT_VEICULACAO_JOB' => $row->DT_VEICULACAO_PAGINACAO
        );
		
		$rs= $this->db->get_where('TB_JOB', array('ID_JOB' => $id));
		
		if ($rs->num_rows() > 0){
            $this->db->where('ID_JOB', $id);
			$this->db->update('TB_JOB', $dados);
		} else {
			$this->db->insert('TB_JOB', $dados);	
		}
	}
	function getAllSemCategoria()
	{
		$sql = 	' SELECT * FROM TB_PAGINACAO' .
				' WHERE TB_PAGINACAO.ID_PAGINACAO NOT IN ' .
				'( ' .
				'	SELECT ID_PAGINACAO ' .
				'	FROM TB_QTD_CATEGORIA_PAGINACAO' .
				')' .
				' AND FLAG_SITUACAO_PAGINACAO = 1' .
				' AND FLAG_ACTIVE_PAGINACAO = 1' .
				' ORDER BY DT_INSERT_PAGINACAO ASC' .
				' LIMIT 5 ';
				
		$rs = $this->db->query($sql);
		
		if ($rs->num_rows() > 0)
			return $rs->result_array();
		else
			return false;
	}
	function getAllSemAprovacaoPresidencia()
	{
		$sql = 	' SELECT * FROM TB_PAGINACAO' .
				' INNER JOIN TB_CARRINHO ON TB_CARRINHO.ID_PAGINACAO = TB_PAGINACAO.ID_PAGINACAO' .
				' WHERE TB_PAGINACAO.FLAG_ACTIVE_PAGINACAO = 1' .
				' AND TB_PAGINACAO.FLAG_SITUACAO_PAGINACAO = 1' .
				' AND TB_CARRINHO.FLAG_ACTIVE_CARRINHO = 1' .
				' AND TB_CARRINHO.FLAG_APROVACAO_ELENCO = 1' .
				' AND TB_CARRINHO.FLAG_APROVACAO_PRESIDENCIA = 0';
				
		$rs = $this->db->query($sql);
		
		if ($rs->num_rows() > 0)
			return $rs->result_array();
		else
			return false;
	}
	function getByIdEmail($id)
	{
		$this->db->select(	'TB_PAGINACAO.DESC_PAGINACAO AS Descricao, ' .
							'TB_TIPO_PAGINACAO.DESC_TIPO_PAGINACAO AS Tipo, ' .
							'TB_PRACA.DESC_PRACA AS Praca, ' .
							'TB_MIDIA.DESC_MIDIA AS Midia, ' .
							'TB_VEICULO.DESC_VEICULO AS Veiculo, ' .
							'TB_PAGINACAO.NUMERO_PAGINACAO AS Paginas, ' .
							'TB_PAGINACAO.OBS_PAGINACAO AS Observacao, ' .							
							'TB_SITUACAO_PAGINACAO.DESC_SITUACAO_PAGINACAO AS Situacao');
							
		$this->db->join('TB_MIDIA', 'TB_MIDIA.ID_MIDIA=TB_PAGINACAO.ID_MIDIA','LEFT');
		$this->db->join('TB_SITUACAO_PAGINACAO', 'TB_SITUACAO_PAGINACAO.ID_SITUACAO_PAGINACAO=TB_PAGINACAO.ID_SITUACAO_PAGINACAO','LEFT');
		$this->db->join('TB_TIPO_PAGINACAO', 'TB_TIPO_PAGINACAO.ID_TIPO_PAGINACAO=TB_PAGINACAO.ID_TIPO_PAGINACAO','LEFT');
		$this->db->join('TB_PRACA', 'TB_PRACA.ID_PRACA=TB_PAGINACAO.ID_PRACA','LEFT');
		$this->db->join('TB_VEICULO', 'TB_VEICULO.ID_VEICULO=TB_PAGINACAO.ID_VEICULO','LEFT');						
		$this->db->where('ID_PAGINACAO', $id);			
		if($rs = $this->db->get('TB_PAGINACAO'))
				return $rs->row_array();
			else
				return false;
	}
	
	function setSituacaoPaginacao($id,$status)
	{
		$this->db->where('ID_PAGINACAO',$id);
		$this->db->update($this->tableName,array('ID_SITUACAO_PAGINACAO'=>$status));
		return;
	}
	
	/**
	 * Altera o id_status de uma paginacao
	 * @param $id
	 * @param $status
	 * @return boolean Retorna true em caso de sucesso na atualização, false em caso de falha
	 */
	function setStatus($id,$status){
		$this->db->where($this->primaryKey,$id);
		return $this->db->update($this->tableName,array('ID_STATUS'=>$status));
	}
	
	/**
	 * Altera o id_etapa de uma paginacao
	 * @param $id
	 * @param $status
	 * @return boolean Retorna true em caso de sucesso na atualização, false em caso de falha
	 */
	function setEtapa($id,$etapa){
		$this->db->where($this->primaryKey,$id);
		return $this->db->update($this->tableName,array('ID_ETAPA'=>$etapa));
	}
	
	function getByIdCarrinho($id=null)
	{
		if($id==null)
			return null;
		if ( isset($this->statusField) )
			$this->db->where($this->statusField, MODEL_STATUS_ATIVO);

		$this->db->join('TB_CARRINHO', 'TB_CARRINHO.ID_PAGINACAO = TB_PAGINACAO.ID_PAGINACAO','LEFT');
		$this->db->join('TB_SITUACAO_PAGINACAO', 'TB_SITUACAO_PAGINACAO.ID_SITUACAO_PAGINACAO = TB_PAGINACAO.ID_SITUACAO_PAGINACAO','LEFT');
		$this->db->join('TB_TIPO_PAGINACAO', 'TB_TIPO_PAGINACAO.ID_TIPO_PAGINACAO = TB_PAGINACAO.ID_TIPO_PAGINACAO','LEFT');
		$this->db->join('TB_CLASSIFICACAO', 'TB_CLASSIFICACAO.ID_CLASSIFICACAO = TB_PAGINACAO.ID_CLASSIFICACAO','LEFT');
		$this->db->join('TB_PRACA', 'TB_PRACA.ID_PRACA = TB_PAGINACAO.ID_PRACA','LEFT');
		$this->db->join('TB_MIDIA', 'TB_MIDIA.ID_MIDIA = TB_PAGINACAO.ID_MIDIA','LEFT');
		$this->db->join('TB_VEICULO', 'TB_VEICULO.ID_VEICULO = TB_PAGINACAO.ID_VEICULO','LEFT');
		$this->db->join('TB_FORMATO', 'TB_FORMATO.ID_FORMATO = TB_PAGINACAO.ID_FORMATO','LEFT');
		$this->db->where('TB_CARRINHO.ID_CARRINHO', $id);
		$rs = $this->db->get($this->tableName);
		
		if ( $rs->num_rows() == 1 )
			return $rs->row_array();
		else
			return null;
	}
	/**
	 * Incrementa o campo NUM_DOWNLOAD de uma paginacao
	 * @param $id
	 */
	function incrementaDownload($id){
		$sql = "UPDATE 
					TB_PAGINACAO 
				SET 
					NUM_DOWNLOAD = NUM_DOWNLOAD+1 
				WHERE 
					ID_PAGINACAO = %d";
		$sql = sprintf($sql, $id);
		$this->db->query($sql);
	}
	
	/**
	 * Zera o campo download de uma paginacao
	 * @param $id
	 */
	function zeraDownload($id){
		$sql = "UPDATE 
					TB_PAGINACAO 
				SET 
					NUM_DOWNLOAD = 0 
				WHERE 
					ID_PAGINACAO = %d";
		$sql = sprintf($sql, $id);
		$this->db->query($sql);
	}
	
	/**
	 * Salva os itens de paginacao
	 * 
	 * <p>Usado na controller de paginacao,
	 * para definir a pagina e ordem dos itens</p>
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $itens Lista de itens a serem gravados
	 * @return void
	 */
	public function savePaginacao(array $itens){
		if(!empty($itens)){
			foreach($itens as $id => $item){
				$this->excelItem->save($item, $id);
				
				$ei = $this->excelItem->getById($id);
				$subficha = $this->excelItem->getFilhasByVersaoPraca($ei['ID_EXCEL'], $ei['ID_PRACA'], $ei['ID_FICHA']);
				
				foreach($subficha as $sf){
					$this->excelItem->save($item, $sf['ID_EXCEL_ITEM']);	
				}
			}
		}
		
	}
}

