<?php
class SubfichaDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_SUBFICHA','int','',1,1);
		$this->addField('ID_FICHA1','int','',2,0);
		$this->addField('ID_FICHA2','int','',2,0);
	}
	### END

	var $tableName = 'TB_SUBFICHA';

	function __construct(){
		parent::GenericModel();
	}
	
	//function getById($id = null){
	//}
	
	function getByFichaPrincipal($idFichaPrincipal = null){
		$rs = $this->db
				->select('O.FILE_ID,SF.*, F.*, CF.ID_CAMPO_FICHA, CF.LABEL_CAMPO_FICHA, CF.VALOR_CAMPO_FICHA, CF.PRINCIPAL, U.NOME_USUARIO')
				->join('TB_FICHA F', 'F.ID_FICHA = SF.ID_FICHA2')
				->join('TB_CAMPO_FICHA CF', 'F.ID_FICHA = CF.ID_FICHA AND CF.PRINCIPAL = 1', 'LEFT')
				->join('TB_USUARIO U', 'U.ID_USUARIO = F.ID_USUARIO')
				->join('TB_OBJETO_FICHA OB', 'OB.ID_FICHA = SF.ID_FICHA2 AND OB.PRINCIPAL = 1', 'LEFT')
				->join('TB_OBJETO O', 'O.ID_OBJETO = OB.ID_OBJETO', 'LEFT')
				->where('SF.ID_FICHA1', $idFichaPrincipal)
				->get($this->tableName . ' SF');
			//echo $this->db->last_query();die;
				return $rs->result_array();
	}
	
	function desvincular($idFicha1, $idFicha2){
		$this->db->delete($this->tableName, array('ID_FICHA1' => $idFicha1, 'ID_FICHA2' => $idFicha2));
	}
	
	function vincular($idFicha1, $idFicha2){
		$rs = $this->db
			->where('ID_FICHA1', $idFicha1)
			->where('ID_FICHA2', $idFicha2)
			->get($this->tableName)
			->result_array();
			
		if(count($rs) <= 0){
			$this->db->insert($this->tableName, array('ID_FICHA1' => $idFicha1, 'ID_FICHA2' => $idFicha2));
			return $this->db->insert_id();
		}
		return 0;
	}
	
	function getByFichas($idFicha1, $idFicha2){
		$rs = $this->db
				->join('TB_FICHA F', 'F.ID_FICHA = SF.ID_FICHA2')
				->where('SF.ID_FICHA1', $idFicha1)
				->where('SF.ID_FICHA2', $idFicha2)
				->get($this->tableName . ' SF');
				
		return $rs->result_array();
	}
	
	function getIdFichaPrincipal($idFicha1, $idFicha2){
		$sql = "SELECT * FROM TB_SUBFICHA 
			WHERE (ID_FICHA1 = '" .$idFicha1. "' AND ID_FICHA2 = " .$idFicha2. ") OR 
			(ID_FICHA1 = " .$idFicha2. " AND ID_FICHA2 = " .$idFicha1. ")
			limit 1;";
		$rs = $this->db->query($sql)->row_array();
		
		if(count($rs) > 0){
			return $rs['ID_FICHA1'];
		}
		else
		{
			return null;
		}
	}
	
	//GAMBI PARRA CCZ QNDO UMA SUBFICHA VIER SOZINHA NO TEMPLATE PUCHAR UMA DESCRICAO DE ALGUMA FICHA PAI
	function getNomeFichaPrincipal($idSubficha){
		$sql = "SELECT NOME_FICHA FROM TB_SUBFICHA S
				INNER JOIN TB_FICHA F ON F.ID_FICHA = S.ID_FICHA1
				WHERE S.ID_FICHA2 = '" .$idSubficha. "' 
				ORDER BY F.ID_FICHA DESC LIMIT 1;";
		
		$rs = $this->db->query($sql)->row_array();
		return $rs;
	}
	
	function getByClienteCodigo($cliente, $codigo, $status=false){
		$sql = "SELECT * FROM TB_FICHA 
				WHERE COD_BARRAS_FICHA = '" .$codigo. "' AND ID_CLIENTE = '" .$cliente. "' ";
		
		if($status == true){
			$sql = $sql . "AND FLAG_ACTIVE_FICHA = '" .$status. "'"; 
		}
		
		$rs = $this->db->query($sql)->row_array();
		return $rs;
	} 
}




