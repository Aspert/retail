<?php
/**
 * 
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class ExcelVersaoDB extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_EXCEL_VERSAO','int','',1,1);
		$this->addField('ID_EXCEL','int','',1,0);
		$this->addField('DATA_CADASTRO','datetime','',19,0);
	}
	### END

	var $tableName = 'TB_EXCEL_VERSAO';
	
	/**
	 * Recupera o ID de uma nova versao de excel
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo da importacao de excel que vai ter uma nova versao
	 * @return int codigo da versao
	 */
	public function getNewVersion($id){
		$data = array(
			'DATA_CADASTRO' => date('Y-m-d H:i:s'),
			'ID_EXCEL' => $id
		);
		
		//Atualiza a data do excel
		$this->excel->save(array('DT_CADASTRO_EXCEL' => date('Y-m-d H:i:s')), $id);
		
		return $this->save($data);
	}

	/**
	 * Recupera os dados de uma versao anterior de excel
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idVersaoAtual Codigo da versao atual
	 * @return array
	 */
	public function getVersaoAnterior($idVersaoAtual){
		$atual = $this->getById($idVersaoAtual);
		
		$rs = $this->db->join('TB_EXCEL E','E.ID_EXCEL=V.ID_EXCEL')
			->where('V.ID_EXCEL_VERSAO !=', $atual['ID_EXCEL_VERSAO'])
			->where('V.ID_EXCEL', $atual['ID_EXCEL'])
			->order_by('V.DATA_CADASTRO DESC')
			->limit(1)
			->get($this->tableName.' V')
			->row_array();
			
		return $rs;
	}
	
	/**
	 * Recupera os dados da versao atual do excel
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idExcel
	 * @return array
	 */
	public function getVersaoAtual($idExcel){
		$atual = $this->getById($idExcel);
		
		$rs = $this->db->join('TB_EXCEL E','E.ID_EXCEL=V.ID_EXCEL')
			->where('V.ID_EXCEL', $idExcel)
			->order_by('V.DATA_CADASTRO DESC')
			->limit(1)
			->get($this->tableName.' V')
			->row_array();
			
		return $rs;
	}
	
	/**
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param $idExcel
	 * @return unknown_type
	 */
	public function getLastDifferences($idExcel){
		
		$atual = $this->getVersaoAtual($idExcel);
		$anterior = $this->getVersaoAnterior($atual['ID_EXCEL_VERSAO']);
		
		
		$idanterior = empty($anterior['ID_EXCEL_VERSAO']) ? -1 : $anterior['ID_EXCEL_VERSAO'];
		
		return $this->getDifferences($idanterior, $atual['ID_EXCEL_VERSAO']);
		
	}
	
	/**
	 * Compara duas versoes
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $version1
	 * @param int $version2
	 * @return array
	 */
	public function getDifferences($version1, $version2){
		$lista = array();
		$data1 = $this->excelVersao->getById($version1);
		$pracas = $this->excel->getPracas($data1['ID_EXCEL'], $version1);
		$data = array();
		
		$removidos = 0;
		$atualizados = 0;
		$adicionados = 0;
		
		////////////////////////////////////////////////////////////////////
		// fazendo da versao anterior
		////////////////////////////////////////////////////////////////////
		foreach($pracas as $praca){
			$itens = $this->excelItem->getByExcelPracaVersao($data1['ID_EXCEL'], $praca['ID_PRACA'], $version1,true);
			
			$data[$praca['DESC_PRACA']] = array(
				'REMOVIDA' => 1,
				'NOVA' => 0
			);
			
			foreach($itens as $item){
				$res = array();
				$precos = $this->pricing->getByItem($item['ID_EXCEL_ITEM']);
				
				foreach($item as $key => $val){
					$res[$key]['antigo'] = $val;
					$res[$key]['novo'] = $val;
				}
				
				foreach($precos as $key => $val){
					$res[$key]['antigo'] = $val;
					$res[$key]['novo'] = $val;
				}
				
				// marca sempre o antigo como removido
				$res['REMOVIDO'] = 1;
				$res['ATUALIZADO'] = 0;
				$res['ADICIONADO'] = 0;
				
				$data[$praca['DESC_PRACA']]['ITENS'][$item['PAGINA_ITEM']][$item['ORDEM_ITEM']] = $res;
			}
		}
		
		////////////////////////////////////////////////////////////////////
		// fazendo da versao posterior
		////////////////////////////////////////////////////////////////////
		$data2 = $this->excelVersao->getById($version2);
		$pracas = $this->excel->getPracas($data2['ID_EXCEL'], $version2);
		
		foreach($pracas as $praca){
			$itens = $this->excelItem->getByExcelPracaVersao($data2['ID_EXCEL'], $praca['ID_PRACA'], $version2,true);
			
			// se nao existe uma praca anterior
			if(empty($data[$praca['DESC_PRACA']])){
				// criamos
				$data[$praca['DESC_PRACA']] = array(
					'REMOVIDA' => 0,
					'NOVA' => 1
				);
			
			// marcamos que nao eh nova
			} else {
				$data[$praca['DESC_PRACA']]['NOVA'] = 0;
				$data[$praca['DESC_PRACA']]['REMOVIDA'] = 0;
			}
			
			// referencia da praca atual
			$pracaData = &$data[$praca['DESC_PRACA']];
			
			// pegamos os itens da praca
			foreach($itens as $item){
				$novo = false;
				// se nao existe o item anterior
				if(empty($pracaData['ITENS'][$item['PAGINA_ITEM']][$item['ORDEM_ITEM']])){
					// criamos um novo
					$res = array(
						'REMOVIDO' => 0,
						'ATUALIZADO' => 0,
						'ADICIONADO' => 1
					);
					$novo = true;
					
				// do contrario pegamos o anterior
				} else {
					$res = $pracaData['ITENS'][$item['PAGINA_ITEM']][$item['ORDEM_ITEM']];
					$res['REMOVIDO'] = 0;
				}
				
				$precos = $this->pricing->getByItem($item['ID_EXCEL_ITEM']);
				
				foreach($item as $key => $val){
					$res[$key]['novo'] = $val;
					if($novo){
						$res[$key]['antigo'] = $val;
					}
				}
				
				foreach($precos as $key => $val){
					$res[$key]['novo'] = $val;
					if($novo){
						$res[$key]['antigo'] = $val;
					}
				}
				
				unset($res['ID_EXCEL_ITEM'], $res['ID_EXCEL_VERSAO'], $res['ID_PRICING']);
				
				// agora vamos ver se algum dos valores foi alterado
				foreach($res as $key => $resItem){
					if($resItem['novo'] != $resItem['antigo']){
						$res['ATUALIZADO'] = 1;
						break;
					}
				}
				
				$pracaData['ITENS'][$item['PAGINA_ITEM']][$item['ORDEM_ITEM']] = $res;
			}
		}
		
		// contando remocoes, adicoes, atualizacoes
		foreach($data as $praca){
			foreach($praca['ITENS'] as $pagina => $node){
				foreach($node as $ordem => $item){
					
					if($item['REMOVIDO'] == 1) $removidos++;
					if($item['ADICIONADO'] == 1) $adicionados++;
					if($item['ATUALIZADO'] == 1) $atualizados++;
					
				}
			}
		}
		
		// prepara o retorno
		$retorno = array(
			'DATA_1' => $data1['DATA_CADASTRO'],
			'DATA_2' => $data2['DATA_CADASTRO'],
			'REMOVIDOS' => $removidos,
			'ATUALIZADOS' => $atualizados,
			'ADICIONADOS' => $adicionados,
			'PRACAS' => $data
		);
		
		return $retorno;
	}
}


