<?php
class Ficha_historicoDB extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_HISTORICO_FICHA','int','',1,1);
		$this->addField('ID_USUARIO','int','',2,0);
		$this->addField('ID_FICHA','int','',1,0);
		$this->addField('NEW_STATUS','int','',0,0);
		$this->addField('DT_INSERT_HISTORICO','timestamp','',19,0);
		$this->addField('COMENT_HISTORICO_FICHA','blob','',2,0);
	}
	### END

	var $tableName = 'TB_HISTORICO_FICHA';
	
	
	function __construct(){
		parent::GenericModel();
		
	}
	
	/**
	 * Recupera o ultimo registro de historico de uma ficha com join TB_USUARIO e TB_APROVACAO
	 * @author juliano.polito
	 * @param $idFicha
	 * @return unknown_type
	 */
	function getLastByFicha($idFicha){
		$rs = $this->db->from($this->tableName.' HF')
			->join('TB_USUARIO U','HF.ID_USUARIO = U.ID_USUARIO')
			->join('TB_APROVACAO_FICHA AF','HF.NEW_STATUS = AF.ID_APROVACAO_FICHA')
			->where('ID_FICHA',$idFicha)
			->order_by('DT_INSERT_HISTORICO DESC')
			->limit(1)
			->get();
			
		return $rs->row_array();
	}
	
	
}

