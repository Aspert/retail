<?php
/**
 * Classe de modelo para o gerenciamento de clientes
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class ClienteDB extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_CLIENTE','int','',1,1);
		$this->addField('DESC_CLIENTE','string','',7,0);
		$this->addField('LOGO_CLIENTE','string','',0,0);
		$this->addField('STATUS_CLIENTE','int','',1,0);
		$this->addField('FEED_CLIENTE','string','',0,0);
		$this->addField('CHAVE_STORAGE_CLIENTE','string','',0,0);
		$this->addField('VALOR_CODIGO_BARRAS','int','',1,0);
		$this->addField('CLASSE_IMPORTADOR','string','',1,0);
		$this->addField('ID_TRANSFORMADOR','int','',1,0);
		$this->addField('QTD_DIAS_INATIVACAO_FICHA','int','',4,0);
		$this->addField('SUBCATEGORIA_LOTE','int','',1,0);
	}
	### END

	var $tableName = 'TB_CLIENTE';
	var $statusField = 'STATUS_CLIENTE';
	
	/**
	 * Construtor
	 *  
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return ClienteDB
	 */
	function __construct(){
		parent::GenericModel();
		
		// adiciona as validacoes
		$this->addValidation('DESC_CLIENTE','requiredString','Informe o nome');
		$this->addValidation('DESC_CLIENTE','function', array($this, 'checaNomeDuplicado'));
		$this->addValidation('CHAVE_STORAGE_CLIENTE','function', array($this, 'checaChaveStorage'));
		$this->addValidation('STATUS_CLIENTE','requiredNumber','Informe o status');
	}
	
	public function getById($id){
		$rs = $this->db->where('ID_CLIENTE',$id)
			->get($this->tableName.' C')
			->row_array();
			
		return $rs;
	}
	
	/**
	 * Lista os clientes cadastrados
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param data $filters filtros para pesquisa
	 * @param int $page numero da pagina para iniciar os registros
	 * @param int $limit numero registros por pagina
	 * @return array 
	 */
	public function listItems($filters, $page=0, $limit=5, $field='DESC_CLIENTE', $direction='ASC'){
		$this->setWhereParams($filters);
		$this->db->order_by("DESC_CLIENTE");

		return $this->execute($page,$limit,$field,$direction);
	}

	function getAllAgenciasByIdCliente($id=null){
		$res = null;
		if($id != null){
			$sql = "select A.ID_AGENCIA, DESC_AGENCIA, IF(ID_CLIENTE IS NULL, 0, 1) as SELECIONADO from TB_AGENCIA A ";
			$sql .= "left join TB_FORNECEDOR_CLIENTE FC on FC.ID_AGENCIA = A.ID_AGENCIA and FC.ID_CLIENTE = " . $id;
			$sql .= " and A.STATUS_AGENCIA = 1 ";
			$this->db->order_by("DESC_AGENCIA");
			$res = $this->db->query($sql)->result_array();
		}
		else{
			$this->db->where('STATUS_AGENCIA', 1);
			$this->db->order_by("DESC_AGENCIA");
			$res = $this->db->get("TB_AGENCIA")->result_array();
		}
		return $res;
	}

	function getAll(){
		$this->db->where('TB_CLIENTE.STATUS_CLIENTE', 1);
		$this->db->order_by("DESC_CLIENTE");
		$res = $this->db->get("TB_CLIENTE")->result_array();
		return $res;
	}

	/**
	 * Retorna a lista de clientes pelo id da agencia
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idAgencia Codigo da agencia
	 * @return array
	 */
	public function getByAgencia($idAgencia){

		$this->db->join("TB_FORNECEDOR_CLIENTE FC", "FC.ID_CLIENTE = C.ID_CLIENTE")
			->join("TB_AGENCIA A", "A.ID_AGENCIA = FC.ID_AGENCIA");

		$res = $this->db->where('A.ID_AGENCIA', $idAgencia)
			->where('C.STATUS_CLIENTE', 1)
			->where('A.STATUS_AGENCIA', 1)
			->order_by('C.DESC_CLIENTE')
			->get($this->tableName. " C");

		return $res->result_array();
	}
	
	/**
	 * Recupera itens filtrando a partir de sessao
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $session Dados da sessao
	 * @param array $filters Demais dados para filtro
	 * @return array
	 */
	public function filterBySession($session, $filters=array()){
		$this->db->from($this->tableName);
		
		if(!empty($session['ID_AGENCIA'])){
			$this->db->where('ID_AGENCIA', $session['ID_AGENCIA']);
		}
		
		if(!empty($session['ID_CLIENTE'])){
			$this->db->where('ID_CLIENTE', $session['ID_CLIENTE']);
		}
		
		$this->db->order_by('DESC_CLIENTE');
		$this->setWhereParams($filters);
		
		return $this->db->get()->result_array();
	}
	
	/**
	 * Checa nome duplicado de cliente
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data
	 * @return mixed
	 */
	public function checaNomeDuplicado($data){
		$idcliente = empty($data['ID_CLIENTE']) ? 0 : sprintf('%d',$data['ID_CLIENTE']);
		
		$rs = $this->db->where('DESC_CLIENTE',$data['DESC_CLIENTE'])
			->where('ID_CLIENTE != ', $idcliente)
			->get($this->tableName);

		if($rs->num_rows() > 0){
			return 'Já existe um cliente com este nome nesta agência';
		}
		
		return true;
	}
	
	/**
	 * Verifica se a chave do storage ja nao pertecen a outro cliente
	 * @author Hugo Ferreira da Silva
	 * @param array $data
	 * @return mixed
	 */
	public function checaChaveStorage($data){
		if( !empty($data['CHAVE_STORAGE_CLIENTE']) ){
			$idcliente = empty($data['ID_CLIENTE']) ? 0 : sprintf('%d',$data['ID_CLIENTE']);
			
			$rs = $this->db->where('CHAVE_STORAGE_CLIENTE',$data['CHAVE_STORAGE_CLIENTE'])
				->where('ID_CLIENTE != ', $idcliente)
				->get($this->tableName);
	
			if($rs->num_rows() > 0){
				return 'Já existe uma chave de storage com este nome';
			}
		}
		
		return true;
	}
	
	/**
	 * Retorna a pasta de um cliente
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idCliente codigo do cliente
	 * @return string
	 */
	public function getPath($idCliente){
		$cl = $this->getById($idCliente);
		$path = $this->config->item('caminho_storage') . '/CLIENTES/';
		$path .= $cl['CHAVE_STORAGE_CLIENTE'] . '/RETAIL/';
			
		return $path;
	}
	
	/**
	 * Recupera um novo codigo de barras
	 * 
	 * Usado principalmente para o cadastro de fichas combo
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idcliente codigo do cliente
	 * @return string
	 */
	public function getNovoCodigoBarras($idcliente){
		$data = $this->getById($idcliente);
		$vlr = $data['VALOR_CODIGO_BARRAS'];
		
		$data['VALOR_CODIGO_BARRAS']++;
		$this->save($data, $idcliente);
		
		return 'CB' . sprintf('%011d', $vlr);
	}
	
	/**
	 * Recupera os clientes ativos
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param array $filters filtros adicionais
	 * @return array
	 */
	public function getAtivos(array $filters = array()){
		
		$filters['STATUS_CLIENTE'] = 1;
		$this->setWhereParams($filters);
		
		return $this->db
			->order_by('DESC_CLIENTE')
			->get($this->tableName)
			->result_array();
	}
}

