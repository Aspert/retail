<?php
/**
 * Classe de modelo para o gerenciamento de comentarios dos arquivos em aprovação
 * @author Bruno de Oliveira
 * @link http://www.247id.com.br
 */
class AprovacaoComentarioArquivoDB extends GenericModel {
	### START
	protected function _initialize(){
		$this->addField('ID_APROVACAO_COMENTARIO','int','',1,1);
		$this->addField('ID_APROVACAO_COMENTARIO_ARQUIVO','int','',1,1);
		$this->addField('ID_APROVACAO_ARQUIVO','int','',1,1);
		$this->addField('IS_ARQUIVO_CLIENTE','int','',1,0);
	}
	### END

	var $tableName = 'TB_APROVACAO_COMENTARIO_ARQUIVO';
	
	/**
	 * Construtor
	 *  
	 * @author Bruno de Oliveira
	 * @link http://www.247id.com.br
	 * @return AprovacaoComentarioArquivoDB
	 */
	function __construct(){
		parent::GenericModel();
		// adiciona as validacoes
		//$this->addValidation('OBS_APROVACAO','requiredString','Informe o nome');
	}
	
	/**
	 * Metodo para buscar os comentários feitos pelo perfil cliente
	 * @author Bruno de Oliveira
	 * @link http://www.247id.com.br
	 * @param int $IdComentario id do comentário
	 * @param int $IdArquivo id do arquivo
	 * @return array
	 */
	public function getComentarioArquivoByArquivoID($IdComentario,$IdArquivo){
		$comentarios = $this->db
		->join('TB_APROVACAO_COMENTARIO AC', 'AC.ID_APROVACAO_COMENTARIO = TB_APROVACAO_COMENTARIO_ARQUIVO.ID_APROVACAO_COMENTARIO')
		->join('TB_APROVACAO_ARQUIVO AA', 'AA.ID_APROVACAO_ARQUIVO= TB_APROVACAO_COMENTARIO_ARQUIVO.ID_APROVACAO_ARQUIVO')
		->where('TB_APROVACAO_COMENTARIO_ARQUIVO.ID_APROVACAO_COMENTARIO',$IdComentario)
		->where('TB_APROVACAO_COMENTARIO_ARQUIVO.IS_ARQUIVO_CLIENTE',1)
		->get($this->tableName)->result_array();
		
		return $comentarios;
	}
	
}