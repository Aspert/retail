<?php
class ObjetoDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_OBJETO','int','',0,1);
		$this->addField('ID_TIPO_OBJETO','int','',0,0);
		$this->addField('ID_CLIENTE','int','',0,0);
		$this->addField('ID_CATEGORIA','int','',0,0);
		$this->addField('ID_SUBCATEGORIA','int','',0,0);
		$this->addField('FILE_ID','int','',0,0);
		$this->addField('PATH','blob','',0,0);
		$this->addField('FILENAME','string','',0,0);
		$this->addField('ID_FABRICANTE','int','',0,0);
		$this->addField('ID_MARCA','int','',0,0);
		$this->addField('DATA_UPLOAD','datetime','',0,0);
		$this->addField('KEYWORDS','blob','',0,0);
		$this->addField('ORIGEM','string','',0,0);
		$this->addField('CODIGO','string','',0,0);
	}
	### END


	var $tableName = 'TB_OBJETO';

	public function save($data, $id = null){
		if( array_key_exists('KEYWORDS', $data) && !is_null($data['KEYWORDS']) ){
			$data['KEYWORDS'] = utf8_encode($data['KEYWORDS']);
		}
		parent::save($data,$id);
	}

	/**
	 * Lista os itens
	 *
	 * 23/07/2010 - o campo HAS_FICHA conta somente fichas ativas
	 *
	 * @author Hugo Ferreira da Silva
	 * @param array $filters
	 * @param int $page
	 * @param int $limit
	 * @param string $oder
	 * @param string $direction
	 * @return array
	 */
	public function listItems(array $filters, $page=0, $limit=5, $order = '', $direction='ASC'){
		// se informou a agencia
		if( !empty($filters['ID_AGENCIA']) ){
			// pega os clientes da agencia
			$clientes = $this->cliente->getByAgencia($filters['ID_AGENCIA']);
			// lista de codigos de clientes
			$codes = array(0);
			// para cada cliente
			foreach($clientes as $item){
				// coloca o codigo na lista
				$codes[] = $item['ID_CLIENTE'];
			}
			// filtra pelos codigos dos clientes
			$this->db->where_in('TB_OBJETO.ID_CLIENTE', $codes);
		}

		$this->db
			->join('TB_CLIENTE CL','CL.ID_CLIENTE = TB_OBJETO.ID_CLIENTE')
			->join('TB_CATEGORIA C','C.ID_CATEGORIA = TB_OBJETO.ID_CATEGORIA','LEFT')
			->join('TB_SUBCATEGORIA SC','SC.ID_SUBCATEGORIA = TB_OBJETO.ID_SUBCATEGORIA','LEFT')
			->join('TB_FABRICANTE F','F.ID_FABRICANTE = TB_OBJETO.ID_FABRICANTE','LEFT')
			->join('TB_MARCA M','M.ID_MARCA = TB_OBJETO.ID_MARCA','LEFT')
			->join('TB_TIPO_OBJETO T','T.ID_TIPO_OBJETO = TB_OBJETO.ID_TIPO_OBJETO')
			->select('*, IF((SELECT COUNT(*) FROM TB_FICHA F JOIN TB_OBJETO_FICHA OF WHERE F.ID_FICHA=OF.ID_FICHA AND OF.ID_OBJETO=TB_OBJETO.ID_OBJETO AND F.FLAG_ACTIVE_FICHA = 1) > 0, 1, 0) AS HAS_FICHA',FALSE);
		
		//puxa somente fichas de categorias e sub ativas
		$this->db->where("(C.FLAG_ACTIVE_CATEGORIA = 1 OR TB_OBJETO.ID_CATEGORIA IS NULL)");
		$this->db->where("(SC.FLAG_ACTIVE_SUBCATEGORIA = 1 OR TB_OBJETO.ID_SUBCATEGORIA IS NULL)");
			
		if( !empty($filters['PERIODO_INICIAL']) && !empty($filters['PERIODO_FINAL']) ){
			$this->db->where(
				sprintf('DATA_UPLOAD BETWEEN "%s" AND "%s"',
					format_date_to_db($filters['PERIODO_INICIAL'],2),
					format_date_to_db($filters['PERIODO_FINAL'],3)
				)
			);

		} else if( !empty($filters['PERIODO_INICIAL']) ){
			$this->db->where( 'DATA_UPLOAD >= ', format_date_to_db($filters['PERIODO_INICIAL'],2) );

		} else if( !empty($filters['PERIODO_FINAL']) ){
			$this->db->where( 'DATA_UPLOAD <= ', format_date_to_db($filters['PERIODO_FINAL'],3) );
		}

		// 5154 - pesquisa like por nome de arquivo
		// 5240 - troca do OR do like para AND
		if( !empty($filters['FILENAME']) ){
			$parts = explode(' ', $filters['FILENAME']);
			unset($filters['FILENAME']);
			$like = array();
			foreach($parts as $item){
				$item = trim($item);
				if( $item !== '' ){
					$like[] = " TB_OBJETO.FILENAME LIKE '%" . addslashes($item) . "%' ";
				}
			}
			if( !empty($like) ){
				$this->db->where('(' . implode(' AND ', $like) . ')');
			}
		}

		// 5154 - pesquisa like por keywords
		// 5240 - troca do OR do like para AND
		if( !empty($filters['KEYWORDS']) ){
			$parts = explode(' ', $filters['KEYWORDS']);
			unset($filters['KEYWORDS']);
			$like = array();
			foreach($parts as $item){
				$item = trim($item);
				if( $item !== '' ){
					$like[] = " TB_OBJETO.KEYWORDS LIKE '%" . addslashes($item) . "%' ";
				}
			}
			if( !empty($like) ){
				$this->db->where('(' . implode(' AND ', $like) . ')');
			}
		}

		if( !empty($filters['CATEGORIAS']) ){
			$this->db->where("(TB_OBJETO.ID_CATEGORIA IN (".mysql_real_escape_string(implode(',',$filters['CATEGORIAS'])).") OR TB_OBJETO.ID_CATEGORIA IS NULL)");
		}

		$this->setWhereParams($filters);
		$res = $this->execute($page, $limit, $order, $direction);
		
		//pre($this->db->last_query());
		
		return $res;
	}

	/**
	 * Retorna todos os objetos cadastrados por path/file
	 *
	 * @author juliano.polito
	 * @param $file
	 * @param $path
	 * @return void
	 */
	public function getByPath($path,$file){
		$rs = $this->db
			->from($this->tableName . ' O' )
			->where('O.FILENAME',$file)
			->where('O.PATH',$path)
			->get();
		return $rs->result_array();
	}
	
	public function getByPathLike($path,$file){
		// Sidnei Tertuliano Junior
		// GLPI 10360 - Objetos\Envio de objeto - N�o valida se existe um arquivo no sistema
		// Foi retirada a query em ACTIVE DB pois em nomes de arquivo como por exmplo "01 2937 2_BotticelliTannat.jpg"
		// o CodeIgniter estava transformando em "01 `2937` 2_BotticelliTannat", ou seja, colocava um escape caso houvesse um numero
		// separado por espacos no meio da string
		$query = "SELECT O.*,TB_TIPO_OBJETO.*, CAST(O.FILENAME AS BINARY) AS FILENAMEBIN FROM " . $this->tableName . ' O ';
		$query .= "JOIN TB_TIPO_OBJETO ON TB_TIPO_OBJETO.ID_TIPO_OBJETO = O.ID_TIPO_OBJETO ";
		$query .= "WHERE O.PATH LIKE '%" .mysql_real_escape_string($path). "%' ";
		$query .= "HAVING FILENAMEBIN = '" .mysql_real_escape_string(utf8_decode($file)). "' ";
		$rs = $this->db->query( $query );
		
		return $rs->result_array();
	}
	
	/**
	 * Recupera um objeto por fileID
	 * @author Hugo Ferreira da Silva
	 * @param int/array $fileid
	 * @return array
	 */
	public function getByFileId($fileid){
		if(!is_array($fileid)){
			$rs = $this->db
				->where('FILE_ID', $fileid)
				->get($this->tableName)
				->row_array();
		}else{
			$rs = $this->db
				->join('TB_CATEGORIA C','C.ID_CATEGORIA = TB_OBJETO.ID_CATEGORIA','LEFT')
				->join('TB_SUBCATEGORIA SC','SC.ID_SUBCATEGORIA = TB_OBJETO.ID_SUBCATEGORIA','LEFT')
				->join('TB_TIPO_OBJETO TOB','TOB.ID_TIPO_OBJETO = TB_OBJETO.ID_TIPO_OBJETO','LEFT')
				->where_in('FILE_ID', $fileid)
				->get($this->tableName)
				->result_array();			
		}
		return $rs;
	}

	/**
	 * recupera um objeto pelo codigo
	 *
	 * @author Hugo Ferreira da Silva
	 * @param string $codigo
	 * @return array
	 */
	public function getByCodigo($codigo){
		$rs = $this->db
			->where('CODIGO', $codigo)
			->get($this->tableName)
			->row_array();

		return $rs;
	}

	/**
	 * Recupera as fichas que estao relacionadas a um objeto pelo FileID
	 *
	 * Somente fichas ativas
	 *
	 * @param int $fileid
	 * @author Hugo Ferreira da Silva
	 * @return array
	 */
	public function getFichasByFileId($fileid){
		$rs = $this->db->distinct()
						->join('TB_OBJETO_FICHA OF','OF.ID_FICHA = F.ID_FICHA','INNER')
						->join($this->tableName.' O','O.ID_OBJETO = OF.ID_OBJETO')
						->join('TB_CATEGORIA C','C.ID_CATEGORIA = F.ID_CATEGORIA','INNER')
						->join('TB_SUBCATEGORIA SC','SC.ID_SUBCATEGORIA = F.ID_SUBCATEGORIA','INNER')
						->join('TB_APROVACAO_FICHA AF','AF.ID_APROVACAO_FICHA = F.ID_APROVACAO_FICHA','INNER')
						->where('O.FILE_ID', $fileid)
						->where('F.FLAG_ACTIVE_FICHA', 1)
						->get('TB_FICHA F');
		return $rs->result_array();
	}

	/**
	 * Recupera fichas de multiplos objetos
	 * 
	 * @author juliano.polito
	 * @param array $fileids
	 * @return array
	 */
	public function getFichasByFileIds($fileids){
		$rs = $this->db->distinct()
						->join('TB_OBJETO_FICHA OF','OF.ID_FICHA = F.ID_FICHA','INNER')
						->join($this->tableName.' O','O.ID_OBJETO = OF.ID_OBJETO')
						->join('TB_CATEGORIA C','C.ID_CATEGORIA = F.ID_CATEGORIA','INNER')
						->join('TB_SUBCATEGORIA SC','SC.ID_SUBCATEGORIA = F.ID_SUBCATEGORIA','INNER')
						->join('TB_APROVACAO_FICHA AF','AF.ID_APROVACAO_FICHA = F.ID_APROVACAO_FICHA','INNER')
						->where_in('O.FILE_ID', $fileids)
						->where('F.FLAG_ACTIVE_FICHA', 1)
						->get('TB_FICHA F');
		return $rs->result_array();
	}
	
	/**
	 * recupera um objeto pela id
	 *
	 * @author Sidnei Tertuliano Junior
	 * @param string $id
	 * @return array
	 */
	public function getById($id){
		$rs = $this->db
			->select('O.ID_OBJETO, O.ID_TIPO_OBJETO, O.ID_CLIENTE, O.ID_CATEGORIA, O.ID_SUBCATEGORIA, O.FILE_ID') 
			->select('O.PATH, O.FILENAME, O.ID_FABRICANTE, O.ID_MARCA, O.DATA_UPLOAD, O.KEYWORDS, O.ORIGEM, O.CODIGO')			
			->select('C.DESC_CATEGORIA, SC.DESC_SUBCATEGORIA, OT.DESC_TIPO_OBJETO, M.DESC_MARCA')
			->join('TB_TIPO_OBJETO OT','OT.ID_TIPO_OBJETO = O.ID_TIPO_OBJETO')
			->join('TB_CATEGORIA C','C.ID_CATEGORIA = O.ID_CATEGORIA','LEFT')
			->join('TB_SUBCATEGORIA SC','SC.ID_SUBCATEGORIA = O.ID_SUBCATEGORIA','LEFT')
			->join('TB_FABRICANTE F','F.ID_FABRICANTE = O.ID_FABRICANTE','LEFT')
			->join('TB_MARCA M','M.ID_MARCA = O.ID_MARCA','LEFT')
			->where('ID_OBJETO', $id)
			->get($this->tableName . ' O')
			->row_array();

		return $rs;
	}	
}

