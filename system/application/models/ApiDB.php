<?php
/**
 * Classe de modelo para gerenciamento de api
 * @author Gabriel Silva
 * 06 de Junho de 2018
 */
 
 class	ApiDB extends GenericModel	{
 protected function _initialize(){
		$this->addField('ID_API','int','',1,1);
		$this->addField('MODULO_API','string','',23,0);
		$this->addField('CHAVE_API','string','',23,0);
		$this->addField('ACESSO_API','string','',23,0);
		$this->addField('CLIENTE_API','int','',1,0);
		$this->addField('STATUS_API','int','',1,0);
	}
	### END

	/**
	 * Nome da tabela
	 * @var string
	 */
	var $tableName = 'TB_API';
	var $statusField = 'STATUS_API';
	
	/**
	 * Construtor
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return UsuarioDB
	 */
	function __construct(){
		parent::GenericModel();
		
		$this->addValidation('MODULO_API','requiredString','Selecione o Módulo');
		$this->addValidation('CHAVE_API','requiredString','Informe a Chave');
		$this->addValidation('ACESSO_API','requiredString','Informe o Código de Acesso');
		$this->addValidation('CLIENTE_API','requiredString','Selecione o Cliente');
	}
	
 	public function listItems($filters,$page=0,$limit=5){
		
		$this->db
			->join('TB_CLIENTE CL','CL.ID_CLIENTE = TB_API.CLIENTE_API')
			->join('TB_CONTROLLER CO','CO.ID_CONTROLLER = TB_API.MODULO_API','LEFT')
			;
			
		if( !empty($filters['ID_AGENCIA']) ){
			$this->db->where('G.ID_AGENCIA', $filters['ID_AGENCIA']);
		}
		
		if( !empty($filters['ID_GRUPO']) ){
			$this->db->where('G.ID_GRUPO', $filters['ID_GRUPO']);
		}
		
		if( !empty($filters['ID_CLIENTE']) ){
			$this->db->where('G.ID_CLIENTE', $filters['ID_CLIENTE']);
		}
		
		if( isset($filters['STATUS_USUARIO']) && $filters['STATUS_USUARIO'] != '' ){
			$this->db->where('STATUS_USUARIO', $filters['STATUS_USUARIO']);
			unset($filters['STATUS_USUARIO']);
		}
		
		$this->setWhereParams($filters);
		return $this->execute($page,$limit, 'ID_API ASC, DESC_AGENCIA ASC, DESC_CLIENTE ASC, NOME_USUARIO');
	}

 	
 }