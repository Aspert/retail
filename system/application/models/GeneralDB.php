<?php
/**
 * Classe que faz consultas no Xinet
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class GeneralDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_OBJ_FICHA','int','',2,1);
		$this->addField('ID_FICHA','int','',1,0);
		$this->addField('FILE_OBJ_FICHA','string','',10,0);
		$this->addField('PATH_OBJ_FICHA','string','',32,0);
		$this->addField('THUMB_OBJ_FICHA','string','',87,0);
		$this->addField('FLAG_MAIN_OBJ_FICHA','string','',1,0);
		$this->addField('ID_TIPO_OBJETO','int','',1,0);
	}
	### END

	function __construct(){
		parent::GenericModel();
		$this->db_xinet = $this->load->database('xinet', TRUE);
	}
	
	/**
	 * nome da tabela
	 * @var string
	 */
	var $tableName = 'TB_OBJ_FICHA';
	
	/**
	 * mapeamento para encontrar os nomes dos campos para consultas
	 * @var array
	 */
	private $_fields = array(
    	'NOME'         => 'FileName',
    	'CODIGO'       => 'Field295',
    	'MARCA'        => 'Field176',
    	'FABRICANTE'   => 'Field244',
    	'DATA_CRIACAO' => 'Field216',
    	'ORIGEM'       => 'Field205',
    	'TIPO'         => 'Field177',
    	'CATEGORIA'    => 'Field174',
    	'SUBCATEGORIA' => 'Field175',
    	'KEYWORDS'     => 'Field7',
		'AGENCIA'      => 'Field217',
		'CLIENTE'      => 'Field180'
    );
	
    /**
     * Monta condicoes WHERE para aplicar em SQL pelos nomes dos campos
     * 
     * <p>Os campos nao sao os mesmos da tabela. Toda inteligencia
     * esta dentro do metodo </p>
     * @author Hugo Ferreira da Silva
     * @link http://www.247id.com.br
     * @param array $attributes
     * @return string Condicoes montadas 
     */
    function mountWhereByAttributes($attributes)
    {
    	$sql = "";
    	
    	// converte os dados para UTF-8
    	foreach ( $attributes as $key=>$value ){
    		$attributes[$key] = str_to_xinet($value);
    	}
    	
    	if ( isset($attributes['PATH']) && $attributes['PATH'] != '' )
    	{
    		$path = explode("/", trim($attributes['PATH']));
    		$sql.= " AND p.Path LIKE '%" . $path[count($path) - 2] . "' ";
    	}
    		
    	if ( isset($attributes['FILE']) && trim($attributes['FILE']) != '' ){
    		$sql.= " AND f.".$this->field('nome') ." = '" . trim($attributes['FILE']) . "' ";
    	}
    	
    	if ( !empty($attributes['NOME']) ){
    		
    		// montagem do like
			$like = "";
			// separa as palavras
			$lista = explode(" ", str_xinet($attributes['NOME']));
			// para cada palavra na lista
			foreach ($lista  as $word ){
				// tira espacos em excesso
				$word = trim($word);
				
				// se nao estiver vazio
				if(!empty($word)){
					// coloca a condicao no like
					$like .= ( ($like=="") ? "" :" AND " ) . " f." . $this->field('nome') . " LIKE '%" . $word . "%' ";
				}
			} 
			// se tiver alguma coisa no like
			if(!empty($like)){
				// adiciona ao sql
				$sql.= " AND ( " . $like . " ) ";
			}
    		
			//$sql.= " AND f.".$this->field('nome') ." LIKE '%" . $attributes['NOME'] . "%' ";
    	}
			
		if ( isset($attributes['CODIGO']) && $attributes['CODIGO'] != '' )
			//$sql.= " AND (f.".$this->field('nome') ." REGEXP '" . $attributes['CODIGO'] . "\\\.[a-zA-Z0-9]{3}$' OR f.".$this->field('NOME') ." = '" . $attributes['CODIGO'] . "') ";
			$sql .= " AND k1." . $this->field('codigo') . " like '%" . $attributes['CODIGO'] . "%' ";
			
		if ( isset($attributes['MARCA']) && $attributes['MARCA'] != '' ){
			$sql.= " AND k1.".$this->field('marca') ." = '" . $attributes['MARCA'] . "' ";
		}
		
		if ( isset($attributes['FABRICANTE']) && $attributes['FABRICANTE'] != '' ){
			$sql.= " AND k1.".$this->field('fabricante') ." = '" . $attributes['FABRICANTE'] . "' ";
		}
		
		if ( isset($attributes['ID_FABRICANTE']) && $attributes['ID_FABRICANTE'] != '' ){
			$sql.= " AND k1.".$this->field('fabricante') ." = '" . $attributes['ID_FABRICANTE'] . "' ";
		}
			
		// Caso venha somente a data inicial
		if(!empty($attributes['PERIODO_INICIAL']) && empty($attributes['PERIODO_FINAL'])) {
			$sql.= " AND f.CreateDate >= UNIX_TIMESTAMP('" . $attributes['PERIODO_INICIAL'] . "') "; 
			//$sql.= " AND k1.".$this->field('data_criacao') ." >= '" . $attributes['PERIODO_INICIAL'] . "' ";
			
		// caso so venha a data final
		} else if(empty($attributes['PERIODO_INICIAL']) && !empty($attributes['PERIODO_FINAL'])) {
			$sql.= " AND f.CreateDate <= UNIX_TIMESTAMP('" . $attributes['PERIODO_FINAL'] . "') "; 
			//$sql.= " AND k1.".$this->field('data_criacao') ." <= '" . $attributes['PERIODO_FINAL'] . "' ";

		// se informou as duas
		} else if(!empty($attributes['PERIODO_FINAL']) && !empty($attributes['PERIODO_INICIAL'])) {
			$sql.= " AND f.CreateDate BETWEEN UNIX_TIMESTAMP('" . $attributes['PERIODO_INICIAL'] . "') AND UNIX_TIMESTAMP('" . $attributes['PERIODO_FINAL'] . "') "; 
			//$sql.= " AND k1.".$this->field('data_criacao') ." BETWEEN '" . $attributes['PERIODO_INICIAL'] . "' AND '" . $attributes['PERIODO_FINAL'] . "' ";
		}
			
		if ( isset($attributes['ID_TIPO_OBJETO']) && $attributes['ID_TIPO_OBJETO'] != '' )
			$sql.= " AND k1.".$this->field('tipo') ." = '" . $attributes['ID_TIPO_OBJETO'] . "' ";
		
		if(empty($attributes['NO_CATEGORIA'])){
			if ( isset($attributes['CATEGORIA']) && $attributes['CATEGORIA'] != '' ) {
				$sql.= " AND k1.".$this->field('categoria') ." = '" . $attributes['CATEGORIA'] . "' ";
			}
			if ( isset($attributes['SUBCATEGORIA']) && $attributes['SUBCATEGORIA'] != '' ){
				$sql.= " AND k1.".$this->field('subcategoria') ." = '" . $attributes['SUBCATEGORIA'] . "' ";
	    	}	
		}else{
			$sql.= " AND k1.".$this->field('categoria') ." = '' ";
		}
		
		if ( !empty($attributes['ORIGEM'])) {
			$sql.= " AND k1.".$this->field('origem') ." like '%" . $attributes['ORIGEM'] . "%' ";
		}
			
		
    	// se informou as keywords
		if ( !empty($attributes['KEYWORDS']) ) {
			// montagem do like
			$like = "";
			// separa as palavras
			$lista = explode(" ", str_xinet($attributes['KEYWORDS']));
			// para cada palavra na lista
			foreach ($lista  as $keyword ){
				// tira espacos em excesso
				$keyword = trim($keyword);
				// se nao estiver vazio
				if(!empty($keyword)){
					// coloca a condicao no like
					$like = ( ($like=="") ? "" :" OR " ) . " k1." . $this->field('keywords') . " LIKE '%" . $keyword . "%' ";
				}
			} 
			// se tiver alguma coisa no like
			if(!empty($like)){
				// adiciona ao sql
				$sql.= " AND ( " . $like . " ) ";
			}
		}
		
		$sql .= " AND f.Online = 1";
				
		return $sql;
    }
    
    /**
     * Recupera as keywords de um arquivo no Xinet
     * @param $id
     * @return array
     */
    function getKeywordsByFileID($id){
    	$sql = "
    		SELECT 
		    	".$this->field('nome')." AS NOME,
		    	".$this->field('codigo')." AS CODIGO,
		    	".$this->field('marca')." AS MARCA,
		    	".$this->field('fabricante')." AS FABRICANTE,
		    	".$this->field('data_criacao')." AS DATA_CRIACAO,
		    	".$this->field('origem')." AS ORIGEM,
		    	".$this->field('tipo')." AS TIPO,
		    	".$this->field('categoria')." AS CATEGORIA,
		    	".$this->field('subcategoria')." AS SUBCATEGORIA,
		    	".$this->field('keywords')." AS KEYWORDS,
		    	".$this->field('agencia')." AS AGENCIA,
		    	".$this->field('cliente')." AS CLIENTE
    		FROM 
    			webnative.file f 
    			LEFT JOIN webnative.keyword1 k1 ON k1.FileID = f.FileID
			WHERE 
				f.FileID = %d";
    	$sql = sprintf($sql,$id);
    	
    	$data = $this->db_xinet->query($sql);
    	
    	if($data){
			if($data->num_rows()>0){
				$rs = $data->result_array();
				
	    		foreach ( $rs as $row ) {
	    			foreach ( $row as $key=>$value ){
	    				if ( !is_numeric($key) ){
	    					$linha[$key] = $value;
	    				}
	    			}
					$linha['CODIGO'] = ereg_replace("\.[a-zA-Z0-9]+", "", $linha['CODIGO']);
					$linha['CATEGORIA'] = ($linha['CATEGORIA']);
					$linha['SUBCATEGORIA'] = ($linha['SUBCATEGORIA']);
					
					$linha['CODIGO'] = ($linha['CODIGO']);
					$linha['KEYWORDS'] = utf8_encode($linha['KEYWORDS']);
					$linha['NOME'] = utf8_decode(str_xinet($linha['NOME']));
	    		}	    		
	    		return $linha;
			}			
    		return array();		
		}    	
    	return array();	
    	
    }
    
    /**
     * Salva keywords
     * 
     * @author juliano.polito
     * @param int $fileID
     * @param array $keywords
     * @return void
     */
    function saveFileKeywords2($fileID, $keywords){
    	$sql = sprintf("select * from webnative.keyword1 where FileID = %d", $fileID);
    	$rs = $this->db_xinet->query($sql);
    	
    	$kw = array();
    	
    	foreach($keywords as $k=>$v){
    		try{
    			$kw[$this->field($k)] = $v;
    		}catch (Exception $e){
    			//caso não encontre algum field
    			//nenhuma acao é necessaria
    		}
    	}
    	
    	if(empty($kw)){
    		return;
    	}
    	
    	//carrega o activerecord
    	$db_xinet = $this->load->database('xinet', TRUE);
    	
    	//se existe atualiza
    	if($rs->num_rows() == 1){
			$db_xinet->where('FileID', $fileID);
			$db_xinet->update('keyword1', $kw);
    	}else{
	    	//se nao existe insere
    		$kw['FileID'] = $fileID;
			$db_xinet->insert('keyword1', $kw);
    	}
    }
    
    
    function saveFileKeywords($fileID, $keywords){
    	// primeiro, checamos se existe
    	$sql = sprintf("select * from webnative.keyword1 where FileID = %d", $fileID);
    	$rs = $this->db_xinet->query($sql);
    	
    	if($rs->num_rows() == 1){
	    	//MARCA,CATEGORIA,SUBCATEGORIA,KEYWORDS
	    	$sql = "
	    	UPDATE webnative.keyword1 k1
	    	SET 
	    		".$this->field('AGENCIA')." = '%s',
	    		".$this->field('CLIENTE')." = '%s',
	    		".$this->field('MARCA')." = '%s',
	    		".$this->field('FABRICANTE')." = '%s',
	    		".$this->field('CATEGORIA')." = '%s',
	    		".$this->field('SUBCATEGORIA')." = '%s',
	    		".$this->field('KEYWORDS')." = '%s',
	    		".$this->field('TIPO')." = '%s',
	    		".$this->field('CODIGO')." = '%s'
	    	WHERE
	    		k1.FileID = %d
	    	";
	    	$sql = sprintf($sql, 
	    						 '',
	    						 mysql_escape_string($keywords['CLIENTE']),
	    						 mysql_escape_string($keywords['MARCA']),
	    						 mysql_escape_string($keywords['FABRICANTE']),
	    						 mysql_escape_string($keywords['CATEGORIA']),
	    						 mysql_escape_string($keywords['SUBCATEGORIA']),
	    						 mysql_escape_string($keywords['KEYWORDS']),
	    						 mysql_escape_string($keywords['TIPO']),
	    						 mysql_escape_string($keywords['CODIGO']),
	    						 mysql_escape_string($fileID));
    	} else {
	    	$sql = "
	    	INSERT INTO webnative.keyword1
	    		(FileID,
	    		".$this->field('AGENCIA').",
	    		".$this->field('CLIENTE').",
	    		".$this->field('MARCA').",
	    		".$this->field('FABRICANTE').",
	    		".$this->field('CATEGORIA').",
	    		".$this->field('SUBCATEGORIA').",
	    		".$this->field('KEYWORDS').",
	    		".$this->field('TIPO').",
	    		".$this->field('CODIGO').")
	    	VALUES
	    		(%d,'%s','%s','%s','%s','%s','%s','%s','%s','%s')
	    	";
	    	$sql = sprintf($sql, $fileID,
	    						 mysql_escape_string($keywords['AGENCIA']),
	    						 mysql_escape_string($keywords['CLIENTE']),
	    						 mysql_escape_string($keywords['MARCA']),
	    						 mysql_escape_string($keywords['FABRICANTE']),
	    						 mysql_escape_string($keywords['CATEGORIA']),
	    						 mysql_escape_string($keywords['SUBCATEGORIA']),
	    						 mysql_escape_string($keywords['KEYWORDS']),
	    						 mysql_escape_string($keywords['TIPO']),
	    						 mysql_escape_string($keywords['CODIGO']),
	    						 mysql_escape_string($fileID));
    		
    	}
    	
    	//LogUtil::log('keywords.sql', $sql);
    	
    	// $sql = utf8_decode($sql);
    	$this->db_xinet->query($sql);
    }
    

	public function getImgData($FileID=0, $spread=0, $img_size='big'){
		if($FileID != null ){
			$out = "";

			if($img_size == 'big'){
				$img_size = '256';
				$prefix_db = 'bigwebdata';
			}else{
				$img_size = '64';
				$prefix_db = 'smallwebdata';
			}

			$file = $this->xinet->getImgFilePorID($FileID, $prefix_db, $img_size);

			$tabela = ( isset($file[0]['tabela']) && $file[0]['tabela'] == $prefix_db."_000" ) ? $prefix_db : $file[0]['tabela'];

			if( $webdata = $this->xinet->getWebData( $FileID, $tabela, $spread ) ){
				foreach($webdata as $data){
					$out .= $data['Data'];
				}
			}else{
				$file[0]['FileName'] = 'nopreview.gif';
				$out = file_get_contents('img/nopreview.gif');
			}
		}else{
			$file[0]['FileName'] = 'nopreview.gif';
			$out = file_get_contents('img/nopreview.gif');
		}

		return array( 'FileName' => $file[0]['FileName'], 'Data' => $out );
	}
    
    /**
     * Recupera os dados pelo nome do arquivo e caminho
     * 
     * @author Hugo Ferreira da Silva
     * @link http://www.247id.com.br
     * @param string $FILE
     * @param string $PATH
     * @return array
     */
    function getByFileNameAndPath($FILE, $PATH)
    {
    	$CI =& get_instance();
    	
    	$sql = " SELECT " .
    	" f.".$this->field('nome')." as NOME, " . 
    	" k1.".$this->field('codigo')." as CODIGO, " . 
    	" k1.".$this->field('marca')." AS MARCA, " .
    	//" k1.".$this->field('data_cricao')." AS DATA_CRIACAO, " .
    	" k1.".$this->field('origem')." AS ORIGEM, " .
	    " k1.".$this->field('tipo')." AS TIPO, " .
	    " k1.".$this->field('categoria')." AS CATEGORIA, " .
	    " k1.".$this->field('subcategoria')." AS SUBCATEGORIA, " .
	    " k1.".$this->field('keywords')." AS KEYWORDS, " .
    	" f.FileName as FILE, " .
    	" from_unixtime(f.CreateDate) AS DATA_CRIACAO,  " .
	    " concat(p.Path) as PATH, " . 
	    " f.FileID " .

		" FROM file f " . 
		" JOIN path p ON f.PathID = p.PathID " . 
		" JOIN keyword1 k1 ON f.FileID = k1.FileID " .
		
		" WHERE " .
		" f.Dir = 0  AND f.Online = 1" .
	    " AND p.Path = '".$PATH."' ";
	    
    	$sql.= $this->mountWhereByAttributes(array('FILE'=>$FILE));
    	
    	$data = $this->db_xinet->query($sql);
		
		if($data)
		{
			if($data->num_rows()>0)
			{
				$rs = $data->result_array();
				
	    		foreach ( $rs as $row ) {
	    			foreach ( $row as $key=>$value ){
	    				if ( !is_numeric($key) ){
	    					$linha[$key] = $value;
	    				}
	    			}
	    					
	    			$linha['THUMB'] = $CI->config->item('xinet_url') . str_replace('%2F','/', urlencode($linha['PATH'].'/'.$linha['FILE']));
					$linha['CODIGO'] = ereg_replace("\.[a-zA-Z0-9]+", "", $linha['CODIGO']);
					$linha['CATEGORIA'] = htmlentities($linha['CATEGORIA']);
					$linha['SUBCATEGORIA'] = htmlentities($linha['SUBCATEGORIA']);
					
					$linha['CODIGO'] = utf8_decode($linha['CODIGO']);
					$linha['NOME'] = str_xinet($linha['NOME']);
					$linha['FILE'] = str_xinet($linha['FILE']);
	    		}	    		
	    		return $linha;					
			}			
    		return false;		
		}    	
    	return false;	
    }

    /**
     * faz uma contagem do total de objetos encontrados a partir dos atributos
     * 
     * @author Hugo Ferreira da Silva
     * @link http://www.247id.com.br
     * @param array $attributes
     * @return int 
     */
    function getTotalObjectsByAttributes($attributes)
    {
    	$sql = " SELECT COUNT(*) AS TOTAL ".
    	" FROM file f " .
    	" JOIN path p ON f.PathID = p.PathID " .
    	" JOIN keyword1 k1 ON f.FileID = k1.FileID " .
    	
		" WHERE" .
		" f.Dir = 0 AND f.Online = 1" .
		" AND p.Path LIKE '".$this->config->item('caminho_storage')."%' ";
    	
    	$sql.= $this->mountWhereByAttributes($attributes);
			
		/*$result=$this->adodb->Execute($sql);
		
		return $result->fields['TOTAL'];*/
		$row = $this->db_xinet->query( $sql )->row_array();
		return $row['TOTAL'];
    }
    
    
    /**
     * Recupera objetos a partir de condicoes (atributos, como eles chamam)
     * 
     * @author Hugo Ferreira da Silva
     * @link http://www.247id.com.br
     * @param $attributes
     * @param $pagina
     * @param $limit
     * @return unknown_type
     */
    function getByLike($attributes, $pagina=0, $limit=5, $path = null, $order = null, $orderDirection = 'ASC')
    {
    	
    	if ((isset($attributes['PERIODO_INICIAL'])) && $attributes['PERIODO_INICIAL']!=null){
			$attributes['PERIODO_INICIAL'] = format_date_to_db($attributes['PERIODO_INICIAL'], 2);
    	}
			
		if ((isset($attributes['PERIODO_FINAL'])) && $attributes['PERIODO_FINAL']!=null){
			$attributes['PERIODO_FINAL'] = format_date_to_db($attributes['PERIODO_FINAL'], 3);
		}
				
		
		if(is_null($path)){
			$path = $this->config->item('caminho_storage');
		}
		
		$path = utf8MAC($path);
			
    	$sql = " SELECT " .
    	" f.".$this->field('nome')." as NOME, " . 
    	" f.FileID as FILE_ID, " . 
    	" k1.".$this->field('codigo')." as CODIGO, " . 
    	" k1.".$this->field('marca')." AS MARCA, " .
    	" k1.".$this->field('FABRICANTE')." AS FABRICANTE, " .
    	//" k1.".$this->field('data_criacao')." AS DATA_CRIACAO, " .
    	" k1.".$this->field('origem')." AS ORIGEM, " .
	    " k1.".$this->field('tipo')." AS TIPO, " .
	    " k1.".$this->field('categoria')." AS CATEGORIA, " .
	    " k1.".$this->field('subcategoria')." AS SUBCATEGORIA, " .
	    " k1.".$this->field('keywords')." AS KEYWORDS, " .
	    " from_unixtime(f.CreateDate) AS DATA_CRIACAO,  " .
    	" f.FileName as FILE, " .
	    " concat(p.Path) as PATH ";
	    
	    $from = " FROM file f " . 
		" JOIN path p ON f.PathID = p.PathID " . 
		" LEFT JOIN keyword1 k1 ON f.FileID = k1.FileID " ;
		
		$where = " WHERE " .
		" f.Dir = 0 AND f.Online = 1" .
	    " AND p.Path LIKE '".$path."%' ";
	    
	    $where.= $this->mountWhereByAttributes($attributes); 		
		
	    if(!empty($order)){
	    	$order = ' ORDER BY '.mysql_escape_string($order);
	    	if(!empty($orderDirection)){
	    		$order .= ' '.mysql_escape_string($orderDirection);
	    	}
	    }
	    
		$sql .= $from . $where;
		$sql .= $order;
				
		$row = $this->db_xinet->query('SELECT count(*) AS total '. utf8_decode($from . $where) )->row_array();
		
		$total = $row['total'];
		
		
		// seta o numero de paginas
		$nPaginas = null;
		if ( $total != null && $limit != null )
		{
			$nPaginas = ceil($total / $limit);
		}		
			
		//$sql.= " LIMIT " . ($limit*$pagina) . ", " . $limit;		
		$sql.= " LIMIT " . ($pagina) . ", " . $limit;	
		
		$sql = utf8_decode($sql);
		
		$data = $this->db_xinet->query($sql);		

		if($data)
		{
			$rs = $data->result_array();
			
			$retorno = array();
			
			foreach ( $rs as $row )
			{
				foreach ( $row as $key=>$field )
				{
					if ( !is_numeric($key) )
					{
						$linha[$key] = $field;
					}
				}
				
				$path = $this->xinet->getPathPorId($linha['FILE_ID']);
				$linha['THUMB'] = base_url().'img.php?img_path=' . $path['File'];
				$linha['CODIGO'] = ereg_replace("\.[a-zA-Z0-9]+", "", $linha['CODIGO']);
				$linha['CATEGORIA'] = htmlentities($linha['CATEGORIA']);
				$linha['SUBCATEGORIA'] = htmlentities($linha['SUBCATEGORIA']);

				$linha['CODIGO'] = str_xinet($linha['CODIGO']);
				$linha['NOME'] = str_xinet($linha['NOME']);
				$linha['FILE'] = str_xinet($linha['FILE']);
				
				$retorno[] = $linha;
			}			
			
			return array(
				'data'=>$retorno,
				'total'=>$total,
				'pagina'=>$pagina,
				'nPaginas'=>$nPaginas,
			);
		}		
		
		return null;
    }
	
/**
     * Recupera objetos a partir de condicoes (atributos, como eles chamam) e/ou um range de fileids
     * 
     * @author juliano.polito
     * @link http://www.247id.com.br
     * @param $attributes
     * @param array $range
     * @param $pagina
     * @param $limit
     * @return unknown_type
     */
    function getByLikeAndRange($attributes,array $range=array(), $pagina=0, $limit=5, $path = null, $order = null, $orderDirection = 'ASC')
    {
    	
    	if ((isset($attributes['PERIODO_INICIAL'])) && $attributes['PERIODO_INICIAL']!=null){
			$attributes['PERIODO_INICIAL'] = format_date_to_db($attributes['PERIODO_INICIAL'], 2);
    	}
			
		if ((isset($attributes['PERIODO_FINAL'])) && $attributes['PERIODO_FINAL']!=null){
			$attributes['PERIODO_FINAL'] = format_date_to_db($attributes['PERIODO_FINAL'], 3);
		}
		
		if(is_null($path)){
			$path = $this->config->item('caminho_storage');
		}
		
		$path = utf8MAC($path);
			
    	$sql = " SELECT " .
    	" f.".$this->field('nome')." as NOME, " . 
    	" f.FileID as FILE_ID, " . 
    	" k1.".$this->field('codigo')." as CODIGO, " . 
    	" k1.".$this->field('marca')." AS MARCA, " .
    	" k1.".$this->field('FABRICANTE')." AS FABRICANTE, " .
    	//" k1.".$this->field('data_criacao')." AS DATA_CRIACAO, " .
    	" k1.".$this->field('origem')." AS ORIGEM, " .
	    " k1.".$this->field('tipo')." AS TIPO, " .
	    " k1.".$this->field('categoria')." AS CATEGORIA, " .
	    " k1.".$this->field('subcategoria')." AS SUBCATEGORIA, " .
	    " k1.".$this->field('keywords')." AS KEYWORDS, " .
	    " from_unixtime(f.CreateDate) AS DATA_CRIACAO,  " .
    	" f.FileName as FILE, " .
	    " concat(p.Path) as PATH ";
	    
	    $from = " FROM file f " . 
		" JOIN path p ON f.PathID = p.PathID " . 
		" LEFT JOIN keyword1 k1 ON f.FileID = k1.FileID " ;
		
		$where = " WHERE " .
		" f.Dir = 0 AND f.Online = 1" .
	    " AND p.Path LIKE '".$path."%' ";
	    
	    $where.= $this->mountWhereByAttributes($attributes);
	    
	    if(!empty($range)){
	    	$where.= ' AND f.FileID IN ('.implode(',',$range).') ';
	    }
	    if(!empty($order)){
	    	$order = ' ORDER BY '.mysql_escape_string($order);
	    	if(!empty($orderDirection)){
	    		$order .= ' '.mysql_escape_string($orderDirection);
	    	}
	    }
	    
		$sql .= $from . $where;
		$sql .= $order;
		
		$row = $this->db_xinet->query('SELECT count(*) AS total '. utf8_decode($from . $where) )->row_array();
		
		$total = $row['total'];
		
		
		// seta o numero de paginas
		$nPaginas = null;
		if ( $total != null && $limit != null )
		{
			$nPaginas = ceil($total / $limit);
		}		
			
		//$sql.= " LIMIT " . ($limit*$pagina) . ", " . $limit;		
		$sql.= " LIMIT " . ($pagina) . ", " . $limit;	
		
		$sql = utf8_decode($sql);
		
		$data = $this->db_xinet->query($sql);		

		if($data)
		{
			$rs = $data->result_array();
			
			$retorno = array();
			
			foreach ( $rs as $row )
			{
				foreach ( $row as $key=>$field )
				{
					if ( !is_numeric($key) )
					{
						$linha[$key] = $field;
					}
				}
				
				$path = $this->xinet->getPathPorId($linha['FILE_ID']);
				$linha['THUMB'] = base_url().'img.php?img_path=' . $path['File'];
				$linha['CODIGO'] = ereg_replace("\.[a-zA-Z0-9]+", "", $linha['CODIGO']);
				$linha['CATEGORIA'] = htmlentities($linha['CATEGORIA']);
				$linha['SUBCATEGORIA'] = htmlentities($linha['SUBCATEGORIA']);

				$linha['CODIGO'] = str_xinet($linha['CODIGO']);
				$linha['NOME'] = str_xinet($linha['NOME']);
				$linha['FILE'] = str_xinet($linha['FILE']);
				
				$retorno[] = $linha;
			}			
			
			return array(
				'data'=>$retorno,
				'total'=>$total,
				'pagina'=>$pagina,
				'nPaginas'=>$nPaginas,
			);
		}		
		
		return null;
    }
    
    
    /**
     * Recupera objetos pelo SKU
     * 
     * @author Hugo Ferreira da Silva
     * @link http://www.247id.com.br
     * @param string $sku
     * @return array
     */
    function getObjectBySku($sku){
    	$query="SELECT 
			    f.FileName as FILE, 
			    concat(p.Path,'/') as PATH
			FROM file f 
			    join path p on (f.PathID=p.PathID) 
			    join keyword1 k1 on (f.FileID = k1.FileID)
			    where f.Dir =0  AND f.Online = 1
			    and p.Path like '".$this->config->item('caminho_storage')."%' 
			    and k1." . $this->field('codigo') . " = '".$sku."' limit 20";
			    
    	$data = $this->db_xinet->query($query);
    	
    	if($data->num_rows()>0)
    	{
    		$result = $data->result_array();
    		
	    	//concat('http://banco.247id.com.br/PORTAL/GETIMAGE.php?-f+archiveFormat+-small+',p.Path,'/',f.FileName) as THUMB
	    	$count=0;
	    	if($result)
	    	{
		    	foreach ($result as $row){
		    		
		    		$field[$count]['PATH']=utf8_encode($row[1]);
		    		$field[$count]['FILE']=$row[0];
		    		$field[$count]['THUMB']="http://banco.247id.com.br/PORTAL/GETIMAGE.php?-f+archiveFormat+-small+".str_replace('%2F','/', urlencode($row[1].$row[0]));
		    		$count++;
		    	}
		    	return $field;    		
	    	}
    		
    	}
    	return false;
    }
    
    /**
     * Recupera objetos por atributos
     * 
     * @author Hugo Ferreira da Silva
     * @link http://www.247id.com.br
     * @param string $sku codigo do produto
     * @param string $categoria
     * @param string $subcategoria
     * @param string $marca
     * @return array
     */
    function getObjectByAttrib($sku,$categoria='',$subcategoria='',$marca=''){
    	
    	$query="SELECT 
			    f.FileName as FILE, 
			    concat(p.Path,'/') as PATH, 
			    concat('http://banco.247id.com.br/PORTAL/GETIMAGE.php?-f+archiveFormat+-small+',p.Path,'/',f.FileName) as THUMB,
			    k1." . $this->field('categoria') . " as CATEGORIA,
			    k1." . $this->field('subcategoria') . " as SUBCATEGORIA 
			FROM file f 
			    join path p on (f.PathID=p.PathID) 
				join keyword1 k1 on (f.FileID=k1.FileID)
			    
				where f.Dir =0  AND f.Online = 1
			    and p.Path like '".$this->config->item('caminho_storage')."%' 
			    and k1." . $this->field('codigo') . " like '".$sku."%' ";
    			if ($categoria!=''){
			    	$query.="and k1." . $this->field('categoria') . "= '".$categoria."' ";
    			}
    			if ($subcategoria!=''){
			    	$query.="and k1." . $this->field('subcategoria') . "= '".$subcategoria."' ";
    			}
        		if ($marca!=''){
			    	$query.="and k1." . $this->field('marca') . "= '".$marca."' ";
    			}
			    $query.="limit 10";
			    
		$data = $this->db_xinet->query($query);		
		
		if($data)
		{
			if($data->num_rows()>0)
			{
				return $rs = $data->result_array();
			}		
			return array();	
		}	    	
    	return array();
    	
    }
    
    /**
     * recupera o nome real de um campo para o Xinet por uma chave
     *  
     * @author Hugo Ferreira da Silva
     * @link http://www.247id.com.br
     * @param string $key Nome da chave
     * @return string nome do campo
     */
    public function field($key){
    	$key = strtoupper($key);
    	
    	if(empty($this->_fields[$key])){
    		throw new Exception('Nenhum campo encontrado com a chave "'.$key.'"');
    	}
    	
    	return $this->_fields[$key];
    }

    /**
     * Recupera o codigo dos tipos relacionados a objetos
     * 
     * @author Hugo Ferreira da Silva
     * @param array $codes codigos dos objetos
     * @return array lista contendo codigos das categorias e codigos dos tipos
     */
 	public function getIdTipoByCodeList(array $codes){
    	$list = array();
    	
    	if( !empty($codes) ){
	    	$sql = "SELECT FileID as ID, " . $this->field('TIPO') ." as TIPO FROM keyword1 
	    		WHERE FileID IN (" . implode(', ', $codes) . ")"; 
	    	
	    	$list = $this->db_xinet->query($sql)->result_array();
    	}
    	
    	return $list;
    }
    
    /**
     * recupera uma lista contendo o codigo do objeto e a data de alteracao
     * @author Hugo Ferreira da Silva
     * @param array $codes lista de codigos de objetos
     * @return array
     */
    public function getDataAlteracaoByCodeList(array $codes){
    	$list = array();
    	
    	if( !empty($codes) ){
	    	$sql = "SELECT
	    				f.FileID,
						f.ModifyDate
					from file f
						where 
						f.FileID in (" . implode(', ', $codes) . ")
						AND f.online=1"; 
	    	
	    	$list = $this->db_xinet->query($sql)->result_array();
    	}
    	
    	return $list;
    }
    
}
