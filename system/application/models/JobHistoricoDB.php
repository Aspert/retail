<?php
/**
 * Classe de modelo para historico do job
 * @author esdras.filho
 * @link http://www.247id.com.br
 */
class JobHistoricoDB extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_JOB_HISTORICO','int','',11,1);
		$this->addField('ID_JOB','int','',11,0);
		$this->addField('ID_ETAPA','int','',11,0);
		$this->addField('DATA_HISTORICO','timestamp','',11,0);
		$this->addField('ID_USUARIO','int','',11,0);
		$this->addField('ID_PROCESSO_ETAPA','int','',11,0);
		$this->addField('ID_STATUS','int','',11,0);
	}
	### END

	var $tableName = 'TB_JOB_HISTORICO';
	
	/**
	 * Construtor
	 *  
	 * @author esdras.filho
	 * @link http://www.247id.com.br
	 * @return ItemMenuDB
	 */
	public function __construct(){
		parent::GenericModel();
	}
	
	/**
	 * Pega os historico do job
	 * 
	 * @author esdras.filho
	 * @param int $idJob
	 * @param int $limit
	 * @param int $order
	 */
	public function getHistoricoPeca( $idJob, $limit = 0, $order = ''){
		$this->db->from($this->tableName)
			->where('ID_JOB', $idJob);
			if( $limit > 0 ){
				$this->db->limit($limit);
			}
		
			if( $order != '' ){
				$this->db->order_by($order);
			}
		$resultado = $this->db->get()->result_array();
		return $resultado;
	}
	
	/**
	 * Adicionar um historico
	 * 
	 * @author esdras.filho
	 * @param $idJob
	 * @param $idEtapa
	 * @param $idUsuario
	 * @param $idProcesso
	 * @param $idStatus
	 */
	public function addHistorico( $idJob, $idEtapa, $idUsuario, $idProcesso, $idStatus ){
		$data = Array();
		$data['ID_JOB'] = $idJob;
		$data['ID_ETAPA'] = $idEtapa;
		$data['ID_USUARIO'] = $idUsuario;
		$data['ID_PROCESSO_ETAPA'] = $idProcesso;
		$data['ID_STATUS'] = $idStatus;
		$data['DATA_HISTORICO'] = date('Y-m-d H:i:s');
		$this->save($data);
	}
}

