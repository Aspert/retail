<?php

class ProcessoDB extends GenericModel {
	### START
	protected function _initialize(){
		$this->addField('ID_PROCESSO','int','',11,1);
		$this->addField('ID_TIPO_PECA','int','',11,0);
		$this->addField('STATUS_PROCESSO','int','',1,0);
		$this->addField('DATA_CADASTRO','timestamp','',19,0);
		$this->addField('HORA_INICIO','string','',5,0);
		$this->addField('HORA_FINAL','string','',5,0);
		$this->addField('CONSIDERAR_DIAS_UTEIS','string','',1,0);
		$this->addField('CONSIDERAR_FERIADO','string','',1,0);
	}
	### END

	var $tableName = 'TB_PROCESSO';

	/**
	 * Construtor
	 * @author Hugo Ferreira da Silva
	 * @return ProcessoDB
	 */
	public function __construct(){
		parent::GenericModel();

		$this->addValidation('ID_TIPO_PECA','requiredString','Por favor, selecionar o tipo de peça');
		$this->addValidation('ID_TIPO_PECA','function',array($this,'validaUnico'));
		$this->addValidation('HORA_INICIO','function',array($this,'validaHora'));
		$this->addValidation('STATUS_PROCESSO','function',array($this,'validaEtapas'));
	}
	
	/**
	 * Override na funcao de salvar para ja salvar as etapas
	 *
	 * @see system/application/libraries/GenericModel::save()
	 */
	public function save($data, $id = null){
		$id = parent::save($data,$id);

		if( !empty($data['etapas']) ){
			$qtdSegundosAcumulados = 0;
			$qtdDiasAcumulados = 0;
			
			// transforma hora inicio em segundos
			$horaInicio = explode(':', $data['HORA_INICIO']);
			$horaInicioSegundos = segundos($horaInicio[0], $horaInicio[1]);
			
			// transforma hora fim em segundos
			$horaFinal = explode(':', $data['HORA_FINAL']);
			$horaFinalSegundos = segundos($horaFinal[0], $horaFinal[1]);
			
			// clacula a diferenca de hora inicial e final
			$segundosDiferenca = ($horaFinalSegundos-$horaInicioSegundos);

			foreach($data['etapas'] as $i => $idetapa){		
				// pega a duracao em segundos de uma etapa	
				$qtdHoras = explode(':', $data['qtd_horas'][$i]);
				$segundos = segundos($qtdHoras[0], $qtdHoras[1]);
				
				// clacula a quantidade de dias que dura uma etapa
				$qtdDias = number_format(($segundos / $segundosDiferenca), 1);
								
				// calcula os dias acumulados
				$qtdDiasAcumulados += $qtdDias;
				
				// clacula a quantidade de segundos acumulados
				$qtdSegundosAcumulados += $segundos;
							
				
				$item['ID_PROCESSO'] = $id;
				$item['ID_ETAPA'] = $idetapa;
				$item['QTD_DIAS'] = $qtdDias;
				$item['QTD_DIAS_ACUMULADO'] = $qtdDiasAcumulados;
				$item['QTD_HORAS'] = sgundosParaHoras($segundos);
				$item['QTD_HORAS_ACUMULADO'] = sgundosParaHoras($qtdSegundosAcumulados);
				$item['ID_STATUS'] = $data['id_status'][$i];
				$item['ORDEM'] = $i+1;

				if( isset($data['id_processo_etapas'][$i]) ){
					$this->processoEtapa->save($item, $data['id_processo_etapas'][$i]);
				} else {
					$this->processoEtapa->save($item);
				}
			}
		}
	}

	/**
	 * Override para poder fazer os joins necessarios
	 *
	 * @see system/application/libraries/GenericModel::getById()
	 */
	public function getById($id){
		$rs = $this->db
			->from($this->tableName . ' P')
			->join('TB_TIPO_PECA TP','TP.ID_TIPO_PECA = P.ID_TIPO_PECA')
			->join('TB_CLIENTE C','C.ID_CLIENTE = TP.ID_CLIENTE')
			->where('P.ID_PROCESSO', $id)
			->get()
			->row_array();

		return $rs;
	}

	/**
	 * Verifica se existe outro controle de processo para o mesmo tipo de peca
	 *
	 * @author Hugo Ferreira da Silva
	 * @param array $data Dados do cadastro
	 * @return mixed Boolean true se passar, string se der falha
	 */
	public function validaUnico($data){
		$total = $this->db
			->where('ID_PROCESSO != ', sprintf('%d',$data['ID_PROCESSO']))
			->where('ID_TIPO_PECA',$data['ID_TIPO_PECA'])
			->where('STATUS_PROCESSO',1)
			->count_all_results($this->tableName);

		if( $total > 0 ){
			return 'Já existe um processo ativo para este tipo de peça';
		}

		return true;
	}

	/**
	 * Valida a hora inicial e final
	 *
	 * @author Sdinei Tertuliano Junior
	 * @param array $data Dados do cadastro
	 * @return mixed Boolean true se passar, string se der falha
	 */
	public function validaHora($data){
		$erros = array();
		$inicio = explode(':', $data['HORA_INICIO']);
		$fim = explode(':', $data['HORA_FINAL']);

		// valida hora inicial
		if(count($inicio) == 2){
			if(($inicio[0] < 0) || ($inicio[0] > 23)){
				$erros[] = 'Hora de início deve ser entre 00:00 e 23:00';
			}
			else if($inicio[1] < 0 || $inicio[1] > 59){
				$erros[] = 'Hora de início inválida';
			}
			else if($inicio[1]%5 != 0){
				$erros[] = 'A hora início está com horas quebradas. Só é permitido intervalo de 5 minutos';
			}
		}
		else{
			$erros[] = 'Hora de início inválida';
		}
		
		// valida hora final
		if(count($fim) == 2){
			if(($fim[0] < 0) && ($fim[0] > 23)){
				$erros[] = 'Hora de fim deve ser entre 00:00 e 23:00';
			}
			else if($fim[1] < 0 || $fim[1] > 59 ){
				$erros[] = 'Hora de fim inválida';
			}
			else if($fim[1]%5 != 0){
				$erros[] = 'A hora final está com horas quebradas. Só é permitido intervalo de 5 minutos';
			}
		}
		else{
			$erros[] = 'Hora de fim inválida';
		}
		
		// valida hora final eh maior q hora inicial
		if((count($inicio) == 2) && (count($fim) == 2)){
			if(segundos($inicio[0], $inicio[1]) >= segundos($fim[0], $fim[1])){
				$erros[] = 'A hora inicio é maior que a hora fim. Por favor, informe um período de horas valido!';
			}
		}
			
		if( !empty($erros) ){
			return implode('<br>', $erros);
		}

		return true;
	}
	
	/**
	 * Valida as etapas selecionadas
	 *
	 * @author Hugo Ferreira da Silva
	 * @param array $data Dados do cadastro
	 * @return mixed Boolean true se passar, string se der falha
	 */
	public function validaEtapas($data){
		if( empty($data['etapas']) ){
			return 'Por favor, informe ao menos uma etapa';
		}

		$erros = array();
		
		$idEtapa = $this->etapa->getIdByKey('SEL_MIDIA');
		
		if($data['etapas'][0] != $idEtapa){
			$erros[] = 'A primeira etapa deve ser Plano de Marketing';
		}
		
		$segundosEtapas = 0;

		// valida as quantidades de hora de cada etapa
		foreach($data['etapas'] as $i => $idetapa){
			$horasEtapa = explode(':', $data['qtd_horas'][$i]);
			
			if(count($horasEtapa) == 2){
				if(($horasEtapa[0] < 0)){
					$erros[] = 'A etapa "'.$data['descricoes'][$i].'" está com a quantidade de horas inválida';
				}
				else if($horasEtapa[1] < 0 || $horasEtapa[1] > 59){
					$erros[] = 'A etapa "'.$data['descricoes'][$i].'" está com a quantidade de horas inválida';
				}
				else if($horasEtapa[1]%5 != 0){
					$erros[] = 'A etapa "'.$data['descricoes'][$i].'" está com intervalo diferente de 5 minutos. Por favor, informe apenas valores com intervalo de 5 minutos.';
				}
				else{
					$segundosEtapas += segundos($horasEtapa[0], $horasEtapa[1]);
				}
			}
			else{
				$erros[] = 'A etapa "'.$data['descricoes'][$i].'" está com a quantidade de horas inválida';
			}
		}
				
		// valida as horas da etapa para ver se se encaixam perfeitamente com  a hora de processo
		if($segundosEtapas > 0){
			$inicio = explode(':', $data['HORA_INICIO']);
			$fim = explode(':', $data['HORA_FINAL']);
			$segundosInicioFim = (segundos($fim[0], $fim[1]) - segundos($inicio[0], $inicio[1]));
			
			if($segundosEtapas%$segundosInicioFim != 0){
				$erros[] = 'A quantidade de horas informadas no processo não corresponde a quantidade de horas correta para elaboração do job. 
							A soma da quantidade de horas das etapas deve ser valores múltiplos da quantidade de horas diárias (' . 
							$data['HORA_INICIO'] . '/' . $data['HORA_FINAL'] . ').';
			}
		}
		

		if( !empty($erros) ){
			return implode('<br>', $erros);
		}

		return true;
	}
	
	
	function execute_processo($pagina=null, $limit=null, $order_by=null, $order=null)
	{
		if ( $order_by != null )
			$this->db->order_by($order_by, ( ($order!=null) ?$order :'ASC' ) );
		
		// pega a quantidade de registros na tabela ($total)
		$total = $this->db->get($this->tableName)->num_rows();

		// se informado a pagina, setar o limit default
		if ( $pagina !== null )
		{
			$limit = ($limit!=null) ?$limit :$this->limitDefault;

			//$this->db->limit($limit, $pagina*$limit);
			$this->db->limit($limit, $pagina);
		}

		// seta o numero de paginas
		$nPaginas = null;
		if ( $total != null && $limit != null )
		{
			$nPaginas = ceil($total / $limit);
		}

		$rs = $this->db->get($this->tableName);

		if ( $rs->num_rows() > 0 )
		{
			return array(
				'data'=>$rs->result_array(),
				'total'=>$total,
				'pagina'=>$pagina,
				'nPaginas'=>$nPaginas,
			);
		}
		else
			return array(
				'data'=>array(),
				'total'=>0,
				'pagina'=>0,
				'nPaginas'=>0
			);
	}

	/**
	 * Lista os processos cadastrados
	 *
	 * @author Hugo Ferreira da Silva
	 * @param array $filters
	 * @param int $page
	 * @param int $limit
	 * @param string $field
	 * @param string $direction
	 * @return array
	 */
	public function listItems($filters, $page=0, $limit=5, $field='DESC_CLIENTE', $direction='ASC'){

		// PEGA O TOTAL DE REGISTROS ---------------------------------------------------------------
		if( !empty($filters['DESC_TIPO_PECA']) ){
			$this->db->like('TP.DESC_TIPO_PECA',$filters['DESC_TIPO_PECA']);
		}

		if( !empty($filters['ID_CLIENTE']) ){
			$this->db->where('C.ID_CLIENTE',$filters['ID_CLIENTE']);
		}

		if( !empty($filters['CLIENTES']) ){
			$this->db->where_in('C.ID_CLIENTE',$filters['CLIENTES']);
		}

		$this->db
			->select('C.DESC_CLIENTE, TP.DESC_TIPO_PECA, TB_PROCESSO.*')
			->select('COUNT(DISTINCT J.ID_JOB) AS QTD_JOB')
			->join('TB_JOB J','J.ID_PROCESSO = TB_PROCESSO.ID_PROCESSO','LEFT')
			->join('TB_TIPO_PECA TP','TP.ID_TIPO_PECA = TB_PROCESSO.ID_TIPO_PECA')
			->join('TB_CLIENTE C','C.ID_CLIENTE = TP.ID_CLIENTE')
			->group_by('TB_PROCESSO.ID_PROCESSO');
			
		$total = $this->db->get($this->tableName)->num_rows();
		// ------------------------------------------------------------------------------------------
		
		if( !empty($filters['DESC_TIPO_PECA']) ){
			$this->db->like('TP.DESC_TIPO_PECA',$filters['DESC_TIPO_PECA']);
		}

		if( !empty($filters['ID_CLIENTE']) ){
			$this->db->where('C.ID_CLIENTE',$filters['ID_CLIENTE']);
		}

		if( !empty($filters['CLIENTES']) ){
			$this->db->where_in('C.ID_CLIENTE',$filters['CLIENTES']);
		}
		
		if( !empty($filters['ID_PROCESSO']) ){
			$this->db->where('TB_PROCESSO.ID_PROCESSO',$filters['ID_PROCESSO']);
		}

		$this->db
			->select('C.DESC_CLIENTE, TP.DESC_TIPO_PECA, TB_PROCESSO.*')
			->select('COUNT(DISTINCT J.ID_JOB) AS QTD_JOB')
			->join('TB_JOB J','J.ID_PROCESSO = TB_PROCESSO.ID_PROCESSO','LEFT')
			->join('TB_TIPO_PECA TP','TP.ID_TIPO_PECA = TB_PROCESSO.ID_TIPO_PECA')
			->join('TB_CLIENTE C','C.ID_CLIENTE = TP.ID_CLIENTE')
			->group_by('TB_PROCESSO.ID_PROCESSO');
			
			
			if ( $direction != null ){
				$this->db->order_by($field, $direction);
			}
						
			// se informado a pagina, setar o limit default
			if ( $page !== null )
			{
				$limit = ($limit!=null) ?$limit :$this->limitDefault;
	
				//$this->db->limit($limit, $pagina*$limit);
				$this->db->limit($limit, $page);
			}
			
		$rs = $this->db->get($this->tableName);

		// seta o numero de paginas
		$nPaginas = null;
		if ( $total != null && $limit != null )
		{
			$nPaginas = ceil($total / $limit);
		}
		
		if ( $rs->num_rows() > 0 )
		{
			$result = array(
				'data'=>$rs->result_array(),
				'total'=>$total,
				'pagina'=>$page,
				'nPaginas'=>$nPaginas,
			);
		}
		else{
			$result =  array(
				'data'=>array(),
				'total'=>0,
				'pagina'=>0,
				'nPaginas'=>0
			);
		}

		return $result;
	}

	/**
	 * Recupera a quantidade de dias de um processo pelo tipo de peca
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idTipoPeca codigo do tipo de peca
	 * @throws Excpetion Quando nao houver processo para o tipo de peca informado
	 * @return int quantidade de dias do processo
	 */
	public function getQtdDiasByTipoPeca( $idTipoPeca ){
		$rs = $this->db
			->from('TB_PROCESSO P')
			->join('TB_PROCESSO_ETAPA E','E.ID_PROCESSO = P.ID_PROCESSO')
			->select('SUM(QTD_DIAS) AS TOTAL_DIAS')
			->where('P.ID_TIPO_PECA', $idTipoPeca)
			->where('P.STATUS_PROCESSO', 1)
			->get()
			->row_array();

		if( empty($rs['TOTAL_DIAS']) ){
			throw new Exception('Processo não encontrado para o tipo de peça escolhida');
		}

		return $rs['TOTAL_DIAS'];
	}

	/**
	 * Recupera a quantidade de dias de um processo pelo codigo do processo
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idProcesso codigo do processo
	 * @return int quantidade de dias do processo
	 */
	public function getQtdDias( $idProcesso ){
		$rs = $this->db
			->from('TB_PROCESSO P')
			->join('TB_PROCESSO_ETAPA E','E.ID_PROCESSO = P.ID_PROCESSO')
			->select('SUM(QTD_DIAS) AS TOTAL_DIAS')
			->where('P.ID_PROCESSO', $idProcesso)
			->where('P.STATUS_PROCESSO', 1)
			->get()
			->row_array();

		return $rs['TOTAL_DIAS'];
	}

	/**
	 * Recupera as etapas de um job contendo as datas finais e inicias de cada uma
	 *
	 * Este metodo visa recuperar cada etapa, com sua data de inicio e fim,
	 * segundo a programacao do cadastro do job
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idjob Codigo do job
	 * @return array Lista de etapas com as datas inicias e finais de cada etapa
	 */
	public function getEtapasComDatas($idjob){
		$sql = 'SELECT
			  J.ID_JOB,
			  J.TITULO_JOB,
			  J.DATA_INICIO_PROCESSO,
			  J.DATA_INICIO,
			  J.DATA_TERMINO,
			  C.ID_CLIENTE,
			  C.DESC_CLIENTE,
			  A.ID_AGENCIA,
			  A.DESC_AGENCIA,
			  CA.ID_CAMPANHA,
			  CA.DESC_CAMPANHA,
			  PR.ID_PRODUTO,
			  PR.DESC_PRODUTO,
			  U.NOME_USUARIO,
			  E.QTD_HORAS,

			  EA.COR_ETAPA,
			  EA.DESC_ETAPA AS  PROXIMA_ETAPA,

			  DATE_ADD(J.DATA_INICIO_PROCESSO, INTERVAL IFNULL(PE.QTD_DIAS_ETAPA,0) DAY) AS DATA_INICIO_ETAPA,
			  DATE_ADD(J.DATA_INICIO_PROCESSO, INTERVAL E.QTD_DIAS_ETAPA-1 DAY) AS DATA_FIM_ETAPA,
			  DATE_ADD(DATE_ADD(J.DATA_INICIO_PROCESSO, INTERVAL E.QTD_DIAS_ETAPA DAY), INTERVAL -E.QTD_HORAS HOUR) AS DATA_ENVIA_EMAIL


			FROM TB_PROCESSO P
			JOIN TB_PROCESSO_ETAPA E ON P.ID_PROCESSO = E.ID_PROCESSO
			LEFT JOIN TB_PROCESSO_ETAPA PE ON PE.ID_PROCESSO = P.ID_PROCESSO AND PE.ORDEM = E.ORDEM - 1
			JOIN TB_JOB J ON P.ID_PROCESSO = J.ID_PROCESSO
			JOIN TB_ETAPA EA ON EA.ID_ETAPA = E.ID_ETAPA
			JOIN TB_CLIENTE C ON C.ID_CLIENTE = J.ID_CLIENTE
			JOIN TB_AGENCIA A ON A.ID_AGENCIA = J.ID_AGENCIA
			LEFT JOIN TB_USUARIO U ON U.ID_USUARIO = J.ID_USUARIO_AUTOR
			JOIN TB_CAMPANHA CA ON CA.ID_CAMPANHA = J.ID_CAMPANHA
			JOIN TB_PRODUTO PR ON PR.ID_PRODUTO = CA.ID_PRODUTO

			WHERE J.ID_JOB = %d

			ORDER BY ID_CLIENTE, DATA_INICIO';

		// troca o valor
		$sql = sprintf($sql, $idjob);

		// efetua a consulta e pega o resultado
		$rs = $this->db
			->query($sql)
			->result_array();

		// retorna o resultado
		return $rs;
	}

	/**
	 * Retorna um processo pelo tipo de peca
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idTipoPeca Codigo do tipo de peca
	 * @return array dados do processo
	 */
	public function getByTipoPeca( $idTipoPeca ){
		$rs = $this->db
			->where('ID_TIPO_PECA', $idTipoPeca)
			->where('STATUS_PROCESSO',1)
			->get($this->tableName)
			->row_array();

		return $rs;
	}
}