<?php
class CarrinhoDB  extends GenericModel{
	### START
	protected function _initialize(){
		$this->addField('ID_CARRINHO','int','',1,1);
		$this->addField('ID_PAGINACAO','int','',1,0);
		$this->addField('ID_USER','int','',2,0);
		$this->addField('DESC_CARRINHO','string','',11,0);
		$this->addField('RESPONSAVEL_CARRINHO','string','',11,0);
		$this->addField('COMENTARIO_CARRINHO','blob','',9,0);
		$this->addField('DT_INSERT_CARRINHO','timestamp','',19,0);
		$this->addField('DT_EXPIRA_CARRINHO','timestamp','',19,0);
		$this->addField('FLAG_SITUACAO_CARRINHO','string','',1,0);
		$this->addField('FLAG_ACTIVE_CARRINHO','string','',1,0);
		$this->addField('FLAG_FULL_CARRINHO','string','',1,0);
		$this->addField('FLAG_APROVACAO_ELENCO','string','',1,0);
		$this->addField('FLAG_APROVACAO_PRESIDENCIA','string','',1,0);
		$this->addField('FLAG_CONFIG_CARRINHO','string','',1,0);
	}
	### END


	var $tableName = 'TB_CARRINHO';  	
	
	function getAllByUser($user)
	{
		$this->db->where('ID_USER', $user);
		$this->db->where('FLAG_SITUACAO_CARRINHO', 1);
		
		$this->db->order_by('DT_INSERT_CARRINHO', 'DESC');
		
		$this->db->limit(10);
		
		$rs = $this->db->get($this->tableName);
		
		if ( $rs->num_rows() > 0 )
			return $rs->result_array();
		else
			return false;
	}			
	
	function getByLike($dados = null, $pagina=0,$limit=null)
	{
		if ((isset($dados['DT_INSERT_CARRINHO'])) && $dados['DT_INSERT_CARRINHO']!=null)
			$dados['DT_INSERT_CARRINHO'] = format_date_to_db($dados['DT_INSERT_CARRINHO'], 2);
		
		if ((isset($dados['DT_INSERT_CARRINHO2'])) && $dados['DT_INSERT_CARRINHO2']!=null)	
			$dados['DT_INSERT_CARRINHO2'] = format_date_to_db($dados['DT_INSERT_CARRINHO2'], 2);		
		
		/**
		 * Rotina para filtrar carrinhos onde a 
		 * configuracao seja igual a categoria do usuario ( caso ele possua uma)
		 */
		 
		if($this->session->userdata[USUARIO]['ID_GROUP']==$this->config->item('grupo_categoria'))
		{
			$data = $this->db->query('SELECT ID_CATEGORIA FROM TB_USER_CATEGORIA WHERE ID_USER = "'.$this->session->userdata[USUARIO]['ID_USER'].'"');
			if($data->num_rows()>0)
			{
				$lista_categorias = 'SELECT ID_CATEGORIA FROM TB_USER_CATEGORIA WHERE ID_USER = "'.$this->session->userdata[USUARIO]['ID_USER'].'"';
			}else{
				$lista_categorias = 'SELECT ID_CATEGORIA FROM TB_CATEGORIA';
			}
			
			$rs = $this->db->query($lista_categorias);
			
			$categorias = $rs->result_array();
			
			foreach($categorias as $item)
			{
				$categ[] = $item['ID_CATEGORIA'];
			}
			$sql = ' SELECT * FROM '.$this->tableName. ' ' .
				   ' JOIN TB_PAGINACAO ON TB_PAGINACAO.ID_PAGINACAO = TB_CARRINHO.ID_PAGINACAO' .
				   ' JOIN TB_QTD_CATEGORIA_PAGINACAO ON TB_QTD_CATEGORIA_PAGINACAO.ID_PAGINACAO = TB_PAGINACAO.ID_PAGINACAO' .
				   ' WHERE TB_QTD_CATEGORIA_PAGINACAO.ID_CATEGORIA IN('.join(',',$categ).') ';
		}else{
			$sql = 'SELECT * FROM '.$this->tableName.' JOIN TB_PAGINACAO ON TB_PAGINACAO.ID_PAGINACAO = '.$this->tableName.'.ID_PAGINACAO WHERE 1 = 1';			
		}
		
		if($dados['tipo_pesquisa'] == 'simples')
		{
			if ((isset($dados['DESC_CARRINHO'])) && $dados['DESC_CARRINHO']!=null){
				$sql.=" AND DESC_CARRINHO LIKE '%". $dados['DESC_CARRINHO']."%'";
			}				
			$sql.= 	" AND FLAG_SITUACAO_CARRINHO =1 AND FLAG_FULL_CARRINHO = 0" ;					
		}
		 
		elseif($dados['tipo_pesquisa'] == 'simples_checklist')
		{
			if ((isset($dados['DESC_CARRINHO'])) && $dados['DESC_CARRINHO']!=null){
				$sql.=" AND DESC_CARRINHO LIKE '%". $dados['DESC_CARRINHO']."%'";
			}				
			
			$sql.= 	" AND FLAG_SITUACAO_CARRINHO =1 AND FLAG_FULL_CARRINHO = 1 AND FLAG_APROVACAO_ELENCO = 0" ;
			
			
			// Rotina para ver se todas as fichas foram aprovadas
			$LISTA_INATIVOS = 'SELECT DISTINCT TB_CARRINHO.ID_CARRINHO FROM TB_F_C_P 
								JOIN TB_CARRINHO ON TB_CARRINHO.ID_CARRINHO = TB_F_C_P.ID_CARRINHO
								JOIN TB_FICHA ON TB_FICHA.ID_FICHA = TB_F_C_P.ID_FICHA
								LEFT JOIN TB_PRICING ON TB_PRICING.ID_F_C_P = TB_F_C_P.ID_F_C_P
								WHERE (TB_PRICING.SITUACAO_PRICING <> 1 or TB_PRICING.SITUACAO_PRICING IS NULL)';
			$rs_retail = $this->db->query($LISTA_INATIVOS);
			
			if($rs_retail->num_rows()>0)
			{
				$data_carrinho = $rs_retail->result_array();
				if(count($data_carrinho)>0)
				{
					$valores = '(';
					foreach($data_carrinho as $item)
					{
						$valores.= $item['ID_CARRINHO'].',';	
					}				
					$valores.= ')';
					$valores = str_replace(',)',')',$valores);
					
					$sql.=' AND ID_CARRINHO NOT IN '.$valores;							 				
				}				
			}
								
		}
		
		else
		{
			if(isset($dados['FLAG_SITUACAO_CARRINHO']))
			{
				$sql.=" AND FLAG_SITUACAO_CARRINHO = ".$dados['FLAG_SITUACAO_CARRINHO'];	
			}else{
				$sql.=" AND FLAG_SITUACAO_CARRINHO = 1";
			}
			
					
			if (isset($dados['ID_CARRINHO'])&& $dados['ID_CARRINHO']!=null)
				$sql.=" AND ID_CARRINHO = '".$dados['ID_CARRINHO']."'";
				
			if (isset($dados['DESC_CARRINHO'])&& $dados['DESC_CARRINHO']!=null)
				$sql.=" AND DESC_CARRINHO LIKE '%".$dados['DESC_CARRINHO']."%'";
				
			if (isset($dados['RESPONSAVEL_CARRINHO'])&& $dados['RESPONSAVEL_CARRINHO']!=null)
				$sql.=" AND RESPONSAVEL_CARRINHO LIKE '%".$dados['RESPONSAVEL_CARRINHO']."%'";				
						
			if (isset($dados['DT_INSERT_CARRINHO'])&& $dados['DT_INSERT_CARRINHO']!=null)
				$sql.=" AND DT_INSERT_CARRINHO BETWEEN '".$dados['DT_INSERT_CARRINHO']."' AND '".$dados['DT_INSERT_CARRINHO2']."'";
				
			if (isset($dados['SITUACAO'])&& $dados['SITUACAO']=='selecao')
				$sql.=" AND FLAG_FULL_CARRINHO = 0" ;
				
			if (isset($dados['SITUACAO'])&& $dados['SITUACAO']=='completo')
				$sql.=" AND FLAG_FULL_CARRINHO = 1 AND FLAG_APROVACAO_ELENCO = 0 AND FLAG_APROVACAO_PRESIDENCIA = 0" ;
				
			// Rotina para ver se todas as fichas foram aprovadas
			$LISTA_INATIVOS = 'SELECT DISTINCT TB_CARRINHO.ID_CARRINHO FROM TB_F_C_P 
								JOIN TB_CARRINHO ON TB_CARRINHO.ID_CARRINHO = TB_F_C_P.ID_CARRINHO
								JOIN TB_FICHA ON TB_FICHA.ID_FICHA = TB_F_C_P.ID_FICHA
								LEFT JOIN TB_PRICING ON TB_PRICING.ID_F_C_P = TB_F_C_P.ID_F_C_P
								WHERE (TB_PRICING.SITUACAO_PRICING <> 1 or TB_PRICING.SITUACAO_PRICING IS NULL)';
			$rs_retail = $this->db->query($LISTA_INATIVOS);
			
			if($rs_retail->num_rows()>0)
			{
				$data_carrinho = $rs_retail->result_array();
				if(count($data_carrinho)>0)
				{
					$valores = '(';
					foreach($data_carrinho as $item)
					{
						$valores.= $item['ID_CARRINHO'].',';	
					}				
					$valores.= ')';
					$valores = str_replace(',)',')',$valores);
					
					$sql.=' AND ID_CARRINHO NOT IN '.$valores;							 				
				}				
			}
				

			if (isset($dados['SITUACAO'])&& $dados['SITUACAO']=='elenco_aprovado')
				$sql.=" AND FLAG_FULL_CARRINHO = 1 AND FLAG_APROVACAO_ELENCO = 1 AND FLAG_APROVACAO_PRESIDENCIA = 0" ;	
						
			if (isset($dados['SITUACAO'])&& $dados['SITUACAO']=='presidencia')
				$sql.=" AND FLAG_APROVACAO_PRESIDENCIA = 1" ;			
					
			if (isset($dados['SITUACAO'])&& $dados['SITUACAO']=='presidencia_reprovado')
				$sql.=" AND FLAG_APROVACAO_PRESIDENCIA = 3" ;			
							
		}
		
		
		return $this->executeQuery($sql,$pagina,$limit,'DT_INSERT_CARRINHO','DESC');
		
	}
	function export_relatorio($filtro = null){
		$this->load->dbutil();

		$strCVS = "";

		$strSQL = "";
		$strSQL .= " SELECT ";
		$strSQL .= " 	DESC_CARRINHO AS Descricao, ";
		$strSQL .= " 	RESPONSAVEL_CARRINHO AS Responsavel, ";
		$strSQL .= " 	COMENTARIO_CARRINHO AS Comentario, ";		
		$strSQL .= " 	NAME_USER AS Usuario ";
		$strSQL .= " FROM ";
		$strSQL .= " 	TB_CARRINHO ";
		$strSQL .= " INNER JOIN ";
		$strSQL .= " 	TB_USER ON TB_USER.ID_USER = TB_CARRINHO.ID_USER ";

		if(isset($filtro) && is_array($filtro)){
			$count = 1;
			foreach($filtro as $index => $value){
				$index_temp = split(":", $index);
				$index = trim($index_temp[0]);
				$comparer_str = trim($index_temp[1]);
				$formate_str = trim($index_temp[2]);

				switch($comparer_str){
					case "major":
						$comparer = ">";
						break;
					case "majorequal":
						$comparer = ">=";
						break;
					case "minor":
						$comparer = "<";
						break;
					case "minorequal":
						$comparer = "<=";
						break;
					default:
						$comparer = "=";
						break;
				}

				switch($formate_str){
					case "date":
						$value = format_date_to_db($value, 2);
						break;
					default:
						break;
				}

				$strSQL .= ($count == 1) ? " WHERE " : " AND ";
				$strSQL .= " 	(". $index ." ". $comparer ." '". $value ."') ";

				$count++;
			}
		}

		$obj_recordset = $this->db->query($strSQL);
		if($obj_recordset && $obj_recordset->num_rows > 0){
			$strCVS .= $this->dbutil->csv_from_result($obj_recordset, ";", "\r\n");
		}
		else{
			$strCVS .= "Nenhuma ficha encontrada"."\n";
		}

		return $strCVS;
	}
	function getByIdEmail($id)
	{
		$this->db->select(	'TB_CARRINHO.DESC_CARRINHO AS Descricao, ' .														
							'TB_CARRINHO.COMENTARIO_CARRINHO AS Responsavel, ' .							
							'DATE_FORMAT(TB_CARRINHO.DT_EXPIRA_CARRINHO , "%d/%m/%Y") AS Validade');
																		
		$this->db->where('ID_CARRINHO', $id);			
		if($rs = $this->db->get('TB_CARRINHO'))
			return $rs->row_array();
		else
			return false;
	}
	function setFiltro($id_carrinho,$id_status,$mode)
	{
		$this->db->where('ID_CARRINHO',$id_carrinho);
		$this->db->update($this->tableName,array('FLAG_APROVACAO_'.$mode=>$id_status));
		return;
	}
	function setFull($id_carrinho,$id_status)
	{
		$this->db->where('ID_CARRINHO',$id_carrinho);
		$this->db->update($this->tableName,array('FLAG_FULL_CARRINHO'=>$id_status));
		return;		
	}
	function setFlag_Config_Carrinho($id_carrinho,$id_status)
	{
		$this->db->where('ID_CARRINHO',$id_carrinho);
		$this->db->update($this->tableName,array('FLAG_CONFIG_CARRINHO'=>$id_status));
		return;		
		
	}
	function getPaginacaoByCarrinho($id)
	{
		$this->db->select('ID_PAGINACAO');
		$this->db->from($this->tableName);
		$this->db->where('ID_CARRINHO',$id);
		
		$data = $this->db->get();
		
		if($data->num_rows()>0)
			return $data->row_array();
		return false;	
		
	}
	function getCarrinhoByPaginacao($id)
	{
		$this->db->select('ID_CARRINHO');
		$this->db->from($this->tableName);
		$this->db->where('ID_PAGINACAO',$id);
		
		$data = $this->db->get();
		
		if($data->num_rows()>0)
			return $data->row_array();
		return false;	
		
	}
	function isCompleted($id_carrinho)
	{
		$sql = ' SELECT TB_CARRINHO.ID_PAGINACAO, TB_CARRINHO.ID_CARRINHO FROM TB_CARRINHO ' .
			   ' INNER JOIN TB_PAGINACAO ON TB_PAGINACAO.ID_PAGINACAO = TB_CARRINHO.ID_PAGINACAO' .
			   ' WHERE TB_CARRINHO.FLAG_ACTIVE_CARRINHO = 1' .
			   ' AND TB_PAGINACAO.FLAG_ATIVO = 1 ' .
			   ' AND TB_CARRINHO.ID_CARRINHO = "'.$id_carrinho.'"' ;
		$data = $this->db->query($sql);
		if($data->num_rows()==0)
		{
			return false;
		}
		$carrinhos = $data->result_array();	   
		
		$retorno = null;
		
		$result = null;
		
		$bloqueados = null;
		
		$permitidos = null;
		
		foreach($carrinhos as $item)
		{	
			//	pegando as configura��es de cada um dos carrinhos
			$getconfig =  ' SELECT * FROM TB_QTD_CATEGORIA_PAGINACAO ' .
					      ' WHERE ID_PAGINACAO = "'.$item['ID_PAGINACAO'].'"';
			$recordset = $this->db->query($getconfig);
			if($recordset->num_rows()>0)
			{
				$config = $recordset->result_array();
				
				foreach($config as $chave)
				{ 	
					$retorno['ID_CATEGORIA'] = $chave['ID_CATEGORIA'];
					$retorno['QTD_Q_C_P'] = $chave['QTD_Q_C_P'];	
				}				
			}	
			
			// agora que ja tenho a configuracao, verifico a quantidade atual do carrinho
			$getatual = 'SELECT TB_FICHA.ID_CATEGORIA , COUNT(*) AS QTD_Q_C_P  FROM TB_FICHA 
						 INNER JOIN TB_F_C_P ON TB_F_C_P.ID_FICHA = TB_FICHA.ID_FICHA
						 WHERE TB_F_C_P.ID_CARRINHO = '.$item['ID_CARRINHO'].'
						 GROUP BY TB_FICHA.ID_CATEGORIA';
			
			$linhas = $this->db->query($getatual);
			
			if($linhas->num_rows()==0)
			{	
				$bloqueados[] = $item['ID_CARRINHO'];
			}else{
				$dados = $linhas->result_array();
				
				
				foreach($dados as $value)
				{
					if($value['ID_CATEGORIA']==$retorno['ID_CATEGORIA'])
					{
						if($value['QTD_Q_C_P']==$retorno['QTD_Q_C_P'])
						{
							$permitidos[] = $item['ID_CARRINHO'];
						}else{
							$bloqueados[] = $item['ID_CARRINHO'];
						}
					}						
				}
					
			}				
				      		
		}
		 
		if(is_array($permitidos))
		{			
			if(in_array($id_carrinho,$permitidos))
			{
				return true;
			}else{
				return false;
			}
		}
		return false;
				   
	}		
	/*function getCarrinhosCompletos()
	{
		$sql = ' SELECT TB_CARRINHO.ID_PAGINACAO, TB_CARRINHO.ID_CARRINHO FROM TB_CARRINHO ' .
			   ' INNER JOIN TB_PAGINACAO ON TB_PAGINACAO.ID_PAGINACAO = TB_CARRINHO.ID_PAGINACAO' .
			   ' WHERE TB_CARRINHO.FLAG_ACTIVE_CARRINHO = 1' .
			   ' AND TB_CARRINHO.FLAG_SITUACAO_CARRINHO = 1 ' ;
		$data = $this->db->query($sql);
		if($data->num_rows()==0)
		{
			return false;
		}		   
	   
		$carrinhos = $data->result_array();	   
		
		$retorno = null;
		
		$result = null;
		
		$bloqueados = null;
		
		$permitidos = null;
		
		foreach($carrinhos as $item)
		{	
			//	pegando as configura��es de cada um dos carrinhos
			$getconfig =  ' SELECT * FROM TB_QTD_CATEGORIA_PAGINACAO ' .
					      ' WHERE ID_PAGINACAO = "'.$item['ID_PAGINACAO'].'"';
			$recordset = $this->db->query($getconfig);
			if($recordset->num_rows()>0)
			{
				$config = $recordset->result_array();
				
				foreach($config as $chave)
				{ 	
					$retorno['ID_CATEGORIA'] = $chave['ID_CATEGORIA'];
					$retorno['QTD_Q_C_P'] = $chave['QTD_Q_C_P'];	
				}				
			}	
			
			// agora que ja tenho a configuracao, verifico a quantidade atual do carrinho
			$getatual = 'SELECT TB_FICHA.ID_CATEGORIA , COUNT(*) AS QTD_Q_C_P  FROM TB_FICHA 
						 INNER JOIN TB_F_C_P ON TB_F_C_P.ID_FICHA = TB_FICHA.ID_FICHA
						 WHERE TB_F_C_P.ID_CARRINHO = '.$item['ID_CARRINHO'].'
						 GROUP BY TB_FICHA.ID_CATEGORIA';
			
			$linhas = $this->db->query($getatual);
			
			if($linhas->num_rows()==0)
			{	
				$bloqueados[] = $item['ID_CARRINHO'];
			}else{
				$dados = $linhas->result_array();
				foreach($dados as $value)
				{
					if($value['ID_CATEGORIA']==$retorno['ID_CATEGORIA'])
					{
						if($value['QTD_Q_C_P']==$retorno['QTD_Q_C_P'])
						{
							$permitidos[] = $item['ID_CARRINHO'];
						}else{
							$bloqueados[] = $item['ID_CARRINHO'];
						}
					}						
				}
					
			}				
				      		
		} 			   
		return array('permitidos'=>$permitidos,'bloqueados'=>$bloqueados);
		#return array('permitidos'=>$bloqueados,'bloqueados'=>$permitidos);
			   
			   
	}*/
	function getCarrinhosCompletos()
	{
		$sql = ' SELECT TB_CARRINHO.ID_PAGINACAO, TB_CARRINHO.ID_CARRINHO FROM TB_CARRINHO ' .
			   ' INNER JOIN TB_PAGINACAO ON TB_PAGINACAO.ID_PAGINACAO = TB_CARRINHO.ID_PAGINACAO' .
			   ' WHERE TB_CARRINHO.FLAG_ACTIVE_CARRINHO = 1' .
			   ' AND TB_CARRINHO.FLAG_SITUACAO_CARRINHO = 1 ' ;
		$data = $this->db->query($sql);
		if($data->num_rows()==0)
		{
			return false;
		}	
		
		$bloqueados = null;
		
		$permitidos = null;
		
		$sql = 'SELECT ID_CARRINHO FROM TB_CARRINHO WHERE FLAG_CONFIG_CARRINHO = 1';
		$data = $this->db->query($sql);	
		
		if($data->num_rows()>0)
		{
			foreach($data->result_array() as $item)
			{
				$permitidos[] = $item['ID_CARRINHO'];
			}
		}   

		$sql_2 = 'SELECT ID_CARRINHO FROM TB_CARRINHO WHERE FLAG_CONFIG_CARRINHO = 0';
		$data_2 = $this->db->query($sql_2);	
		
		if($data_2->num_rows()>0)
		{
			foreach($data_2->result_array() as $item)
			{
				$bloqueados[] = $item['ID_CARRINHO'];
			}
		}   
	
		return array('permitidos'=>$permitidos,'bloqueados'=>$bloqueados);
		
	}	
	function getAllEmSelecao()
	{
		$sql = 	' SELECT * FROM TB_CARRINHO' .
				' WHERE FLAG_FULL_CARRINHO = 0' .
				' AND FLAG_SITUACAO_CARRINHO = 1' .
				' ORDER BY DT_INSERT_CARRINHO DESC' .
				' LIMIT 5';
		
		$rs = $this->db->query($sql);
		
		if ( $rs->num_rows() > 0 )
			return $rs->result_array();
		else
			return false;
	}
	function getByPaginacao($id=null)
	{
		if($id != null)
		{
			$this->db->where('ID_PAGINACAO', $id);			
			if($rs = $this->db->get('TB_CARRINHO'))
				return $rs->row_array();
			else
				return false;
		}
		return false;		
	}
}	

