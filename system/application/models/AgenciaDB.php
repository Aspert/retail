<?php
/**
 * Classe de modelo para o gerenciamento de agencias
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class AgenciaDB extends GenericModel {
	### START
	protected function _initialize(){
		$this->addField('ID_AGENCIA','int','',1,1);
		$this->addField('DESC_AGENCIA','string','',10,0);
		$this->addField('LOGO_AGENCIA','string','',0,0);
		$this->addField('STATUS_AGENCIA','int','',1,0);
		$this->addField('FEED_AGENCIA','string','',0,0);
		$this->addField('CHAVE_STORAGE_AGENCIA','string','',0,0);
	}
	### END

	var $tableName = 'TB_AGENCIA';
	var $statusField = 'STATUS_AGENCIA';
	
	/**
	 * Construtor
	 *  
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return AgenciaDB
	 */
	function __construct(){
		parent::GenericModel();
		
		// adiciona as validacoes
		$this->addValidation('DESC_AGENCIA','requiredString','Informe o nome');
		$this->addValidation('DESC_AGENCIA','requiredUnique','Já existe uma agência com este nome');
		$this->addValidation('STATUS_AGENCIA','requiredNumber','Informe o status');
		$this->addValidation('CHAVE_STORAGE_AGENCIA','function', array($this, 'checaChaveStorage'));
	}
	
	/**
	 * Metodo para listar itens
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data Dados para filtro
	 * @param int $pagina numero da pagina atual
	 * @param int $limit limit de itens por pagina
	 * @return array
	 */
	public function listItems($data, $pagina=0, $limit = 5){
		$this->setWhereParams($data,true);
		return $this->execute($pagina, $limit);
	}

	public function getAllClientesByIdAgencia($id=null){
		$res = null;
		if($id != null){
			$sql = "select C.ID_CLIENTE, DESC_CLIENTE, IF(ID_AGENCIA IS NULL, 0, 1) as SELECIONADO from TB_CLIENTE C ";
			$sql .= "left join TB_FORNECEDOR_CLIENTE FC on FC.ID_CLIENTE = C.ID_CLIENTE and FC.ID_AGENCIA = " . $id;
			$res = $this->db->query($sql)->result_array();
		}
		else{
			$this->db->order_by("DESC_CLIENTE");
			$res = $this->db->get("TB_CLIENTE")->result_array();
		}
		return $res;
	}
	
	/**
	 * Retorna a pasta de uma agencia
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idAgencia codigo da agencia
	 * @return string
	 */
	public function getPath($idAgencia){
		$rs = $this->getById($idAgencia);
			
		$path = $this->config->item('caminho_storage') . '/AGENCIAS/'
			 . $rs['CHAVE_STORAGE_AGENCIA'] . '/RETAIL/';
			
		return $path;
	}
	
	/**
	 * Verifica se a chave do storage ja nao pertecen a outra agencia
	 * @author Hugo Ferreira da Silva
	 * @param array $data
	 * @return mixed
	 */
	public function checaChaveStorage($data){
		if( !empty($data['CHAVE_STORAGE_AGENCIA']) ){
			$id = empty($data['ID_AGENCIA']) ? 0 : sprintf('%d',$data['ID_AGENCIA']);
			
			$rs = $this->db->where('CHAVE_STORAGE_AGENCIA',$data['CHAVE_STORAGE_AGENCIA'])
				->where('ID_AGENCIA != ', $id)
				->get($this->tableName);
	
			if($rs->num_rows() > 0){
				return 'Já existe uma chave de storage com este nome';
			}
		}
		
		return true;
	}
	
	/**
	 * recupera as agencias por codigo de cliente
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idcliente
	 * @return array
	 */
	public function getByCliente($idcliente){
		$rs = $this->db
			->join('TB_FORNECEDOR_CLIENTE F','F.ID_AGENCIA = A.ID_AGENCIA')
			->where('F.ID_CLIENTE', $idcliente)
			->order_by('A.DESC_AGENCIA')
			->get($this->tableName . ' A')
			->result_array();
			
		return $rs;
	}
	
	
	/**
	 * Recupera os clientes ativos
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param array $filters filtros adicionais
	 * @return array
	 */
	public function getAtivos(array $filters = array()){
		
		$filters['STATUS_AGENCIA'] = 1;
		$this->setWhereParams($filters);
		
		return $this->db
			->order_by('DESC_AGENCIA')
			->get($this->tableName)
			->result_array();
	}
}

