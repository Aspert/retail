<?php
/**
 * Classe de modelo para historico de alteracoes de campanha
 * @author Hugo Silva
 * @link http://www.247id.com.br
 */
class Campanha_historicoDB extends GenericModel {
	### START
	protected function _initialize(){
		$this->addField('ID_CAMPANHA_HISTORICO','int','',1,1);
		$this->addField('ID_CAMPANHA','int','',1,0);
		$this->addField('ID_USUARIO','int','',1,0);
		$this->addField('DATA_ALTERACAO','datetime','',10,0);
		$this->addField('ALTERACOES','string','',1,0);
	}
	### END

	var $tableName = 'TB_CAMPANHA_HISTORICO';
	
	/**
	 * nomes para poder trocar as chaves do formulario por valores mais faceis para o usuario
	 * @var array
	 */
	private $nomes = array(
		'DESC_CAMPANHA' => 'Campanha',
		'DT_INICIO_CAMPANHA' => 'Data Inicio',
		'DT_FIM_CAMPANHA' => 'Data Termino',
		'STATUS_CAMPANHA' => 'Status'
	);
	
	/**
	 * Construtor
	 *  
	 * @author Hugo Silva
	 * @link http://www.247id.com.br
	 * @return Campanha_historicoDB
	 */
	function __construct(){
		parent::GenericModel();
		
	}

	/**
	 * Faz um log de alteracoes da campanha
	 * 
	 * Recupera a campanha que esta sendo gravada.
	 * Se a campanha nao possui historico, grava o primeiro com todos os dados.
	 * Se tiver alteracoes, pega a versao anterior, faz um comparativo e grava
	 * somente as diferencas.
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idcampanha
	 * @param int $idusuario
	 * @param array $dados
	 * @return void
	 */
	public function gravarAlteracoes($idcampanha, $idusuario, array $dados){
		// pega a lista de alteracoes
		$list = $this->getAlteracoes($idcampanha);
		// dados para insercao do historico
		$data = array(
			'ID_CAMPANHA' => $idcampanha,
			'ID_USUARIO' => $idusuario,
			'DATA_ALTERACAO' => date('Y-m-d H:i:s')
		);
		
		// nao precisamos mostrar ou gravar a chave de storage
		unset($dados['CHAVE_STORAGE_CAMPANHA']);
		// novos dados (com os indices normalizados)
		$novo = array();
		// para cada dado da campanha enviado
		foreach($dados as $key => $val){
			// se estiver no array de nomes
			if( isset($this->nomes[$key]) ){
				// coloca o array com a chave nova 
				$novo[$this->nomes[$key]] = $val;
			}
		}
		
		// indica que as alteracoes sao os mesmo dados enviados pelo usuario
		$data['ALTERACOES'] = serialize($novo);
		// salva o log no banco
		$this->save($data,null);
	}
	
	/**
	 * Recupera todas as alteracoes que uma ficha sofreu
	 * 
	 * Faz a consulta na tabela de historico de alteracoes de campanha
	 * e retorna todos os itens que foram alterados.
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idcampanha
	 * @return array
	 */
	public function getAlteracoes($idcampanha){
		$rs = $this->db
			->select('U.*, H.*, C.DESC_CLIENTE')
			->join('TB_USUARIO U','U.ID_USUARIO = H.ID_USUARIO')
			->join('TB_GRUPO G','G.ID_GRUPO = U.ID_GRUPO')
			->join('TB_CLIENTE C','C.ID_CLIENTE = G.ID_CLIENTE', 'LEFT')
			->where('H.ID_CAMPANHA',$idcampanha)
			->order_by('H.DATA_ALTERACAO ASC')
			->get($this->tableName . ' H')
			->result_array();
			
		return $rs;
	}
	
}

