<?php

require_once dirname(__FILE__) .'/phpmailer/class.phpmailer.php';

class MY_Email  {

    private $email;

    /**
     * Construtor
     *
     * @author Hugo Ferreira da Silva
     * @link http://www.247id.com.br
     * @return MY_Email
     */
    function __construct(){

    }

    function init(){
        $this->email = new PHPMailer();
        $this->email->IsSMTP();
        $this->email->Hostname = "mail.uoldiveosaas.com.br";
        $this->email->Host = "mail.uoldiveosaas.com.br";
        $this->email->SMTPAuth = true;
        $this->email->Username = "retail@aspert.com.br";
        $this->email->SMTPSecure = "tls";
        $this->email->Port = 587;
        $this->email->Password = "247.Admin";
        $this->email->WordWrap = 50;
        $this->email->IsHTML();
        //$this->IsMail();
        $this->email->From = 'retail@aspert.com.br';
        $this->email->FromName = '[ Retail ]';
        $this->email->Subject = utf8_decode('[ RETAIL ] E-mail do Sistema');

    }

    /**
     * Envia o e-mail seguindo as regras de e-mail
     *
     * @author Hugo Ferreira da Silva
     * @link http://www.247id.com.br
     * @param mixed $to array ou string de email que vai receber o email
     * @param string $tpl template a ser usado
     * @param mixed $dados Dados extras para serem processados
     * @return void
     */
    public function sendEmail($to, $tpl, $dados){
        $ci = &get_instance();

        $this->init();

        $result = $ci->load->view($tpl, $dados, true);
        $result = fromUTF8($result);

        $tags = array('td','th','div','body');
        $result = preg_replace('@<('.implode('|', $tags).')@i', '<$1 style="font-family:Verdana; font-size: 11px;" ', $result);

        if( is_string($to) ){
            $to = array($to);
        }

        foreach($to as $item){
            $this->email->AddAddress($item);
        }

        $this->email->Body = $result;
        $this->email->Send();

    }

    /**
     * Envia um email para debug
     *
     * @author Hugo Ferreira da Silva
     * @link http://www.247id.com.br
     * @param string $txt texto a ser enviado
     * @return void
     */
    public function sendDebugEmail($txt){
        $this->init();
        $lista = array(
            //'hugo.silva@247id.com.br',
            '247ID-Implantacao@247id.com.br',
            //'rodrigo.belini@247id.com.br',
            //'juliano.polito@247id.com.br',
            'sidnei.tertuliano@247id.com.br',
            //'givaldo.silva@247id.com.br',
            //'daniel.giesbrecht@247id.com.br'
        );

        foreach($lista as $item){
            $this->email->AddAddress($item);
        }

        $this->email->IsHTML(false);
        $this->email->Body = $_SERVER['SERVER_ADDR'].PHP_EOL.$txt;
        $this->email->Send();
    }

    /**
     * Envia um email para usuarios das regras de email associadas a chave
     *
     * Este metodo adiciona chaves extras ao array:
     * __usuario - array contando todos os dados do usuario que recebera o email
     * __logado - array com dados do usuario logado
     *
     * @author juliano.polito
     * @param $chave chave da regra para envio
     * @param $tpl template a ser usado
     * @param $dados Dados para usar no template
     * @return array Um array contendo erros que podem ter acontecido no envio. Array vazio se nao houver erros.
     */
    public function sendEmailRegra($chave ,$titulo , $tpl, $dados, $agencia, $cliente, $attachmentFile = null){
        $ci = &get_instance();
        //$ci = new MY_Controller();
        $this->init();

        //titulo do email
        $this->email->Subject = utf8_decode($titulo);

        //regras para a controller e function sendo executadas
        $regra = $ci->regraEmail->getByChave($chave);

        //se nao encontrar regra paramos o processo e retorna false
        if(empty($regra)){
            return false;
        }

        //recupera usuarios da regra
        $usuarios = $ci->regraUsuario->getByRegraEmailAgenciaCliente($regra['ID_REGRA_EMAIL'], $agencia, $cliente);

        //adiciona dados do usuario logado ao array de dados
        $user = Sessao::get('usuario');
        $dados['__logado'] = $user;

        //envia email pro usuario logado caso esteja configurado
        //e ele nao faca parte do grupo
        if($regra['FLAG_ENVIA_LOGADO'] == 1){
            $found = false;
            foreach ($usuarios as $usuario) {
                if($usuario['ID_USUARIO'] == $user['ID_USUARIO']){
                    $found = true;
                    break;
                }
            }

            if( !$found && !empty($user) ){
                //adiciona usuario logado na lista
                $usuarios[] = $user;
            }
        }
        //se nao encontrar nenhum usuario paramos o processo e retorna false
        if(empty($usuarios)){
            return false;
        }

        //array que guarda usuarios que deram erro
        $erros = array();

        if(!empty($attachmentFile)){
            $this->email->AddAttachment($attachmentFile,'relatorio.xls');
        }
        //adiciona os enderecos de email
        foreach ($usuarios as $usuario) {
            //limpa emails
            $this->email->ClearAddresses();

            $dados['__usuario'] = $usuario;

            $result = $ci->load->view($tpl, $dados, true);
            $result = fromUTF8($result);

            $tags = array('td','th','div','body');
            //$result = preg_replace('@<('.implode('|', $tags).')@i', '<$1 style="font-family:Verdana; font-size: 11px;" ', $result);
            preg_match_all('@<('.implode('|', $tags).')(.*?)>@i', $result, $reg);

            for($i=0; $i<count($reg[0]); $i++){
                if( stripos($reg[2][$i], 'style="') === false ){
                    $tag = '<' . $reg[1][$i].' style="font-family:Verdana; font-size: 11px;" '.$reg[2][$i].'>';
                    $result = str_replace($reg[0][$i], $tag, $result);
                }
            }

            $this->email->AddAddress($usuario['EMAIL_USUARIO'],$usuario['NOME_USUARIO']);

            $this->email->Body = $result;

            $ok = $this->email->Send();

            if(!$ok){
                $erros[] = $usuario;
            }

        }

        return $erros;

    }

    /**
     * Portado o email da util helper
     *
     * @author Hugo Ferreira da Silva
     * @param unknown_type $data
     * @param unknown_type $usuarios
     * @return unknown_type
     */
    function emailPadrao($data, $usuarios){
        $CI =& get_instance();

        $this->init();
        $template =  $CI->load->view('ROOT/templates/email_padrao', null, true);
        $template = str_replace('#BASEURL#', base_url() , $template);
        $template = str_replace('#MSG#', utf8_decode($data['texto']) , $template);
        $template = str_replace('#URL#', $data['url'] , $template);

        if(isset($usuarios)) {
            foreach($usuarios as $u) {
                $msg = preg_replace('@\{(\w+)\}@e','$u["$1"]',$template);
                $assunto= $data['titulo'];
                $this->email->ClearAddresses();
                $this->email->AddAddress($u['EMAIL_USUARIO']);
                //$this->email->AddAddress("bruno.oliveira@247id.com.br");
                $this->email->Body = $template;
                $this->email->Send();
            }
        }
    }

    /**
     * Envia um email para usuarios das regras de email de processo associadas a chave
     *
     * Este metodo adiciona chaves extras ao array:
     * __usuario - array contando todos os dados do usuario que recebera o email
     * __logado - array com dados do usuario logado
     *
     * @author esdras.filho
     * @param $chave chave da regra para envio de processo
     * @param $tpl template a ser usado
     * @param $dados Dados para usar no template
     * @param $idJob id para buscar a permissão do grupo
     * @return array Um array contendo erros que podem ter acontecido no envio. Array vazio se nao houver erros.
     */
    public function sendEmailRegraProcesso($chave, $idProcessoEtapa, $titulo , $tpl, $dados, $idJob=0){

        $ci = &get_instance();
        $this->init();
        //titulo do email
        $this->email->Subject = utf8_decode($titulo);

        //regras para a controller e function sendo executadas
        $regra = $ci->regraEmailProcessoEtapa->getByChave($chave);


        //se nao encontrar regra paramos o processo e retorna false
        if(empty($regra)){
            return false;
        }
        $usersMail = array();
        $usuarios = array();
        
        if ((int)$idJob > 0) {
            $usuarios = $ci->jobUsuario->getUsers($idJob);
            $usersMail = $ci->regraEmailProcessoEtapaUsuario->getByRegraEmail($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA'], $idProcessoEtapa);
            
            if(!empty($usersMail) && count($usersMail) > 0) {
                
                $idGrupo = 0;
                $permission = false;
                
                foreach($usersMail as $user)    {
                    $idGrupo = $ci->grupo->getById($user['ID_GRUPO']);
                    $grupoPermissions = $ci->grupo->getPermission($idGrupo['ID_GRUPO'], 'ver_todos');
                    
                    if(!empty($grupoPermissions) && count($grupoPermissions) > 0)   {
                        $usuarios[] = $user;
                    }
                    
                }
                
            }
            
        }
        
        // fim do código idjob
        
        if (count($usuarios) <= 0) {
            //recupera usuarios da regra Porém somente do mesmo grupo
            /*$_SESSION['MailGrupoId'] =*/
//            $usuarios = $ci->regraEmailProcessoEtapaUsuario->getByRegraEmail($regra['ID_REGRA_EMAIL_PROCESSO_ETAPA'], $idProcessoEtapa);
        }

        //adiciona dados do usuario logado ao array de dados
        $user = Sessao::get('usuario');
        $dados['__logado'] = $user;

        //envia email pro usuario logado caso esteja configurado
        //e ele nao faca parte do grupo
        if($regra['FLAG_ENVIA_LOGADO'] == 1){
            $found = false;
            foreach ($usuarios as $usuario) {
                if($usuario['ID_USUARIO'] == $user['ID_USUARIO']){
                    $found = true;
                    break;
                }
            }

            if( !$found && !empty($user) ){
                //adiciona usuario logado na lista
                $usuarios[] = $user;
            }
        }

        /*echo "<pre>";
        var_dump($usuarios);
        exit();*/

        //se nao encontrar nenhum usuario paramos o processo e retorna false
        if(empty($usuarios)){
            return false;
        }

        //array que guarda usuarios que deram erro
        $erros = array();


        //adiciona os enderecos de email
        foreach ($usuarios as $usuario) {
            //limpa emails
            $this->email->ClearAddresses();

            $dados['__usuario'] = $usuario;

            $result = $ci->load->view($tpl, $dados, true);
            $result = fromUTF8($result);

            $tags = array('td','th','div','body');

            preg_match_all('@<('.implode('|', $tags).')(.*?)>@i', $result, $reg);

            for($i=0; $i<count($reg[0]); $i++){
                if( stripos($reg[2][$i], 'style="') === false ){
                    $tag = '<' . $reg[1][$i].' style="font-family:Verdana; font-size: 11px;" '.$reg[2][$i].'>';
                    $result = str_replace($reg[0][$i], $tag, $result);
                }
            }

            $this->email->AddAddress($usuario['EMAIL_USUARIO'],$usuario['NOME_USUARIO']);

            $this->email->Body = $result;

            $ok = $this->email->Send();

            if(!$ok){
                $erros[] = $usuario;
            }

        }

        return $erros;

    }
}

