<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Ldap {
	
	private $server;
	private $user;
	private $password;
	private $dn;
	private $filter;
	private $attributes;

    function __construct() {
		$this->setServer("dc24713.idigital.inet");
		$this->setUser("sw24705.srvc@idigital.inet");
		$this->setPassword("n#zo2koi");
		$this->setDN("dc=idigital,dc=inet");
    }
    
    function setDN($dn){
        $this->dn = $dn;
    }
    
    function getDN(){
        return $this->dn;
    }
    
    function setServer($server){
        $this->server = $server;
    }
    function getServer(){
        return $this->server;
    }
    
    function setUser($user){
        $this->user = $user;
    }
    
    function getUser(){
        return $this->user;
    }
    
    function setPassword($password){
        $this->password = $password;
    }
    
    function getPassword(){
        return $this->password;
    }
    
    function setFilter($filter){
        $this->filter = $filter;
    }
    
    function getFilter(){
        return $this->filter;
    }
    
    function setAttributes($attributes){
        $this->attributes = $attributes;
    }
    
    function getAttributes(){
        return $this->attributes;
    }
    
    function connect(){
        $ldapconn = ldap_connect($this->getServer()) or die("Could not connect to AD.");
        ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0);
        ldap_set_option($ldapconn, LDAP_OPT_SIZELIMIT, 10);

        return $ldapconn;
    }
    
    function bind($ldapconn){
        $ldapbind = ldap_bind($ldapconn, $this->getUser(), $this->getPassword()) or die("Could not bind to AD!");
        return $ldapbind;
    }
    
    function unbind($ldapconn){
        ldap_unbind($ldapconn);
    }
    
    function checaUser($usuario,$password){
        $ldapbind = ldap_bind($this->connect(), $usuario, $password);
        return $ldapbind;
    }
    
    function getPager($usuario){
        $ldapconn = $this->connect();
        $ldapbind = $this->bind($ldapconn);
        //$this->setFilter("sAMAccountName=daniel.giesbrecht");
        $this->setFilter("sAMAccountName=".$usuario);     
        $this->setAttributes( array("Pager") );
        $ldapresult = ldap_search($ldapconn, $this->getDN(), $this->getFilter(), $this->getAttributes(), 0 , 1, 30) or die("Could not search to AD!");

        $entries = ldap_get_entries($ldapconn, $ldapresult);

        $ldapbind = $this->unbind($ldapconn);

        return $entries[0]['pager'][0];
    }
    
    function getDisplayNameAndEmailLdap($username){
        $ldapconn = $this->connect();
        $ldapbind = $this->bind($ldapconn);

        $this->setFilter("sAMAccountName=".$username);
        $this->setAttributes( array("displayName", "userPrincipalName", "memberOf", "sAMAccountName", "Mail") );

        $ldapresult = ldap_search($ldapconn, $this->getDN(), $this->getFilter(), $this->getAttributes(), 0 , 1, 30) or die("Could not search to AD!");
        $entries = ldap_get_entries($ldapconn, $ldapresult);

        $ldapbind = $this->unbind($ldapconn);

        return $entries;
    }
}
