<?php
define('MODEL_STATUS_DELETADO', '0');
define('MODEL_STATUS_ATIVO', '1');

class GenericModel extends Model
{
	var $noStatus = true;
	var $prefixField = '';
	var $primaryKey;

	var $mapping = array();
	var $_validation = array();
	var $_validtionTypes = array('requiredString','requiredInt','requiredFloat','requiredCpf','requiredCnpj','function','requiredNumber','requiredDate','requiredUnique');

	function GenericModel()
	{
		parent::Model();
		$this->_initialize();

		$className = get_class($this);

		if ( $className != 'GenericModel' )
		{
			$this->limitDefault = 5;
		}
	}

	function getById($id=null)
	{
		if($id==null)
			return null;
		if ( isset($this->statusField) )
			$this->db->where($this->statusField, MODEL_STATUS_ATIVO);

		$this->db->where($this->primaryKey, $id);

		$rs = $this->db->get($this->tableName);

		if ( $rs->num_rows() == 1 )
			return $rs->row_array();
		else
			return null;
	}

	function getAll($pagina=null, $limit=null, $order_by=null, $order=null)
	{
		if ( isset($this->statusField) )
			$this->db->where($this->statusField, MODEL_STATUS_ATIVO);

		return $this->execute($pagina, $limit, $order_by, $order);
	}

	function executeQuery($sql,$pagina=null, $limit=null, $order_by=null, $order=null)
	{
		$found = explode("FROM",strtoupper($sql));

		$limit = ($limit!=null) ?$limit :$this->limitDefault;


		$tamanho = count($found)-1;

		$sql_count = ' SELECT COUNT(*) AS total FROM ' . $found[$tamanho];

		$row = $this->db->query($sql_count)->row_array();

		$total = $row['total'];

		// seta o numero de paginas
		$nPaginas = null;
		if ( $total != null && $limit != null )
		{
			$nPaginas = ceil($total / $limit);
		}

		if ( $order_by != null )
		{
			$sql.= ' ORDER BY '.$order_by.' '.( ($order!=null) ?$order :'ASC') ;
		}

		if ( $pagina == null )
		{
			$pagina = 'inicio';
		}

		if ( $pagina!= '' )
		{
			if($pagina=='inicio')
			{
				$pagina = 0;
			}

			//$res = $pagina*$limit;
			$res = $pagina;
			$sql.=' LIMIT '.$res.' , '.$limit;
		}

		$rs = $this->db->query($sql);

		if ( $rs->num_rows() > 0 )
		{
			return array(
				'data'=>$rs->result_array(),
				'total'=>$total,
				'pagina'=>$pagina,
				'nPaginas'=>$nPaginas,
			);
		}
		else
			return array(
				'data'=>array(),
				'total'=>0,
				'pagina'=>0,
				'nPaginas'=>0
			);


	}

	function execute($pagina=null, $limit=null, $order_by=null, $order=null)
	{
		// pega a quantidade de registros na tabela ($total)
		$total = $this->db->count_all_results($this->tableName, false);

		// se informado a pagina, setar o limit default
		if ( $pagina !== null )
		{
			$limit = ($limit!=null) ?$limit :$this->limitDefault;

			//$this->db->limit($limit, $pagina*$limit);
			$this->db->limit($limit, $pagina);
		}

		// seta o numero de paginas
		$nPaginas = null;
		if ( $total != null && $limit != null )
		{
			$nPaginas = ceil($total / $limit);
		}

		if ( $order_by != null )
			$this->db->order_by($order_by, ( ($order!=null) ?$order :'ASC' ) );

		$rs = $this->db->get($this->tableName);

		if ( $rs->num_rows() > 0 )
		{
			return array(
				'data'=>$rs->result_array(),
				'total'=>$total,
				'pagina'=>$pagina,
				'nPaginas'=>$nPaginas,
			);
		}
		else
			return array(
				'data'=>array(),
				'total'=>0,
				'pagina'=>0,
				'nPaginas'=>0
			);
	}

	function save($data, $id=null){

		// vamos remover todos os indices que nao fazem parte da tabela
		if(is_array($data)){
			foreach($data as $key => $val){
				if(!array_key_exists($key,$this->mapping)){
					unset($data[$key]);
				}
			}
		}

		if ( $id == null || strlen(trim($id)) == 0 )
		{
			if ( isset($this->statusField) )
				$data[$this->statusField] = MODEL_STATUS_ATIVO;

			if ( $this->db->insert($this->tableName, $data) )
				return $this->db->insert_id();

		} else {

			$this->db->where($this->primaryKey, $id);

			if ( $this->db->update($this->tableName, $data) ){
				return $id;
			}
		}

		return false;
	}

	function delete($id)
	{
		return $this->deleteFisico($id);
	}

	function deleteLogico($id)
	{
		if ( isset($this->statusField) )
			$this->db->where($this->statusField, MODEL_STATUS_ATIVO);

		$this->db->where($this->primaryKey, $id);

		$this->db->limit(1);

		return $this->db->update($this->tableName, array($this->statusField=>MODEL_STATUS_DELETADO));
	}

	function deleteFisico($id)
	{
		if ( isset($this->statusField) )
			$this->db->where($this->statusField, MODEL_STATUS_DELETADO);

		$this->db->where($this->primaryKey, $id);

		$this->db->limit(1);

		return $this->db->delete($this->tableName);
	}

	/**
	 * Metodo de inicializacao
	 *
	 * <p>Deve ser sobrescrito na classe filha</p>
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	protected function _initialize(){

	}

	/**
	 * Adiciona um campo no mapeamento
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $name Nome do campo
	 * @param string $type Tipo do campo
	 * @param string $default Valor padrao do campo
	 * @param int $length Comprimento do campo
	 * @param int $primary 1 quando chave primaria
	 * @return void
	 */
	protected function addField($name,$type,$default,$length,$primary){
		$obj = new stdClass();
		$obj->name = $name;
		$obj->type = $type;
		$obj->default = $default;
		$obj->max_length = $length;
		$obj->primary_key = $primary;

		if($primary == 1){
			$this->primaryKey = $name;
		}

		$this->fields[] = $obj;
		$this->mapping[$name] = $obj;
	}

	/**
	 * Seta clausulas where
	 *
	 * @author Hugo Silva
	 * @param array $params
	 * @return void
	 */
	public function setWhereParams(array $params, $useEmptyValues=false){
		foreach($params as $key => $value){
			if(array_key_exists($key, $this->mapping) && ((!$useEmptyValues && !empty($value)) || $useEmptyValues)){
				switch($this->mapping[$key]->type){
					case 'int':
						$this->db->where($this->tableName . '.' . $key,$value);
					break;

					case 'blob':
					case 'string':
						$this->db->like($this->tableName . '.' . $key,$value);
					break;

					case 'timestamp':
					case 'datetime':
						$this->db->where($this->tableName . '.' . $key, date('Y-m-d H:i:s', strtotime($value)));
					break;

					case 'date':
						$this->db->where($this->tableName . '.' . $key, date('Y-m-d', strtotime($value)));
					break;

					case 'float':
					case 'real':
						$this->db->where($this->tableName . '.' . $key, (float)sprintf('%f', $value));
					break;

				}
			}
		}
	}

	/**
	 * Limpa as validacoes
	 * @author Hugo Silva
	 * @return void
	 */
	public function clearValidation(){
		$this->_validation = array();
	}

	/**
	 * Adiciona uma validacao a um campo
	 *
	 * @author Hugo Silva
	 * @param string $field
	 * @param string $type
	 * @param string $msg
	 * @param int    $min
	 * @param int    $max
	 * @return void
	 */
	public function addValidation($field, $type, $msg, $min=null, $max=null){
		if(!array_key_exists($field,$this->mapping)){
			die('O campo "'.$field.'" não existe para ser adicionado na validação ('.get_class($this).')');
		}

		if(!in_array($type,$this->_validtionTypes)){
			die('O tipo de validação "'.$type.'" não existe');
		}

		$this->_validation[] = array(
			'field' => $field,
			'type' => $type,
			'msg' => $msg,
			'min' => $min,
			'max' => $max
		);
	}

	/**
	 * Metodo para validacao dos campos antes de insercao ou atualizacao
	 * @author Hugo Silva
	 * @return array
	 */
	public function validate($data){
		$erros = array();

		foreach($this->_validation as $item){
			if(array_key_exists($item['field'], $erros)){
				continue;
			}

			$msg = $item['msg'];

			switch($item['type']){
				case 'requiredString':
					if(empty($data[$item['field']])){
						$erros[$item['field']] = $msg;
						continue;
					}

					if(!is_null($item['min']) && strlen($data[$item['field']]) < $item['min']){
						$erros[$item['field']] = $msg;
						continue;
					}

					if(!is_null($item['max']) && strlen($data[$item['field']]) > $item['max']){
						$erros[$item['field']] = $msg;
						continue;
					}

				break;

				case 'requiredNumber':
					if(!isset($data[$item['field']]) || $data[$item['field']] == '' || !is_numeric($data[$item['field']])){
						$erros[$item['field']] = $msg;
						continue;
					}

					if(!is_null($item['min']) && $data[$item['field']] < $item['min']){
						$erros[$item['field']] = $msg;
						continue;
					}

					if(!is_null($item['max']) && $data[$item['field']] > $item['max']){
						$erros[$item['field']] = $msg;
						continue;
					}
				break;

				case 'requiredInt':
					if($data[$item['field']] == '' || !is_int($data[$item['field']])){
						$erros[$item['field']] = $msg;
						continue;
					}

					if(!is_null($item['min']) && $data[$item['field']] < $item['min']){
						$erros[$item['field']] = $msg;
						continue;
					}

					if(!is_null($item['max']) && $data[$item['field']] > $item['max']){
						$erros[$item['field']] = $msg;
						continue;
					}
				break;

				case 'requiredFloat':
					if($data[$item['field']] == '' || !is_float($data[$item['field']])){
						$erros[$item['field']] = $msg;
						continue;
					}

					if(!is_null($item['min']) && $data[$item['field']] < $item['min']){
						$erros[$item['field']] = $msg;
						continue;
					}

					if(!is_null($item['max']) && $data[$item['field']] > $item['max']){
						$erros[$item['field']] = $msg;
						continue;
					}
				break;

				case 'requiredDate':
					if(empty($data[$item['field']])){
						$erros[$item['field']] = $msg;
						continue;
					}

					$val = $data[$item['field']];

					if( ! preg_match('@^(\d{2}\/\d{2}\/\d{4}|\d{4}-\d{2}\-d{2})$@', $val) ) {
						$erros[$item['field']] = $msg;
						continue;
					}
				break;

				case 'requiredCpf':

				break;

				case 'requiredCnpj':
				break;

				case 'requiredUnique':
					$tbl = $this->tableName;
					$rs = $this->db->where($item['field'], $data[$item['field']])
						//->where($this->primaryKey . ' !=', $this->{$this->primaryKey})
						->where($this->primaryKey . ' !=', sprintf('%d', $data[$this->primaryKey]) )
						->get($this->tableName);

					if($rs->num_rows() > 0){
						$erros[$item['field']] = $msg;
						continue;
					}
				break;

				case 'function':
					$rs = call_user_func_array($item['msg'], array($data));
					if($rs !== true){
						$erros[$item['field']] = $rs;
					}
				break;
			}
		}

		return $erros;
	}
}
