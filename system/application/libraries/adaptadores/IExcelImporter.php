<?php

interface IExcelImporter {
	
	/**
	 * Metodo para importacao de excel
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param string $filename Caminho completo do arquivo
	 * @param ITransformer $transformer transformador de dados
	 * @return RetailWorkbook
	 */
	function import($filename, ITransform $transformer);
	
	/**
	 * Valida se o arquivo informado em import e valido
	 * @author Nelson Martucci
	 * @return bool
	 */
	function validate();
}