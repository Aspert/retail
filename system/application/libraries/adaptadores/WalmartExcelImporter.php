<?php

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'IExcelImporter.php';
require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'RetailWorkbook.php';
require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'ExcelReader.php';

class WalmartExcelImporter implements IExcelImporter {

	/**
	 * leitor de arquivos xlsx
	 * @var ExcelReader
	 */
	protected $leitor;

	/**
	 * Metodo para importacao de excel
	 *
	 * @author Hugo Ferreira da Silva
	 * @param string $filename Caminho completo do arquivo
	 * @return RetailWorkbook
	 */
	public function import($filename, ITransform $transformer){

		//Array com os dados da planilha
		$dadosPracaProduto = array();

		//Verifica parametro de nome de arquivo
		if (empty($filename) || !is_file($filename) ) {
			//Dispara Exceção
			throw new Exception('Informar nome de Planilha Excel 2007 para processamento!');
		}

		//Leitura da planilha solicitada
		$dadosPracaProduto = $this->lerPlanilha($filename);

		//Gera objeto Retail Workbook e retorna
		$wb = $this->geraRetailWorkbook($dadosPracaProduto);

		// aplica o transformador
		$rs = $transformer->execute( $wb );
		
		return $wb;

	}

	/**
	 * Verifica se planilha eh compativel com adaptador
	 * (non-PHPdoc)
	 * @see system/application/libraries/adaptadores/IExcelImporter#validate()
	 */
	public function validate() {

		//Faz referencia para utilizacao do leitor da planilha
		$planilhaExcel = $this->leitor;

		//Array com as celulas a serem validadas para planilha Walmart
		$configuracaoValidarPlanilha = array(	'E15' => 'PÁGINA N.º',
												'F15' => 'POSIÇÃO ITEM NA PÁGINA',
												'H15' => 'EAN ITEM PAI');

		//Indice da Planilha do Arquivo (xlsx) que sera lido (Somente a primeira - Indice 0 (zero))
		$iPlanilha = 0;

		//variavel auxiliar de retorno
		$retorno = true;

		//Percorre array de configuracao
		foreach ($configuracaoValidarPlanilha as $celula => $valor) {

			//recupera valor da celula a ser validada
			$valCelulaPlanilha = $planilhaExcel->getCellData($iPlanilha, $celula);

			//Compara valor recuperado da planilha com o valor que deveria ser
			if ( (empty($valCelulaPlanilha)) || (empty($valor)) || ($valor != $valCelulaPlanilha) ) {
				$retorno = false;
				break;
			}
		}

		return $retorno;
	}

	/**
	 * Efetua Leitura da planilha solicitada (Percorre a planilha enquanto coluna EAN_ITEM_PAI (Codigo de Barras) tiver conteudo)
	 * @author Nelson Martucci
	 * @param $filename: Nome do arquivo Excel 2007 a ser carregado para um array
	 * @return array(): Por Praça x Produto
	 */
	private function lerPlanilha($filename) {

		define('COLUNA_EAN_ITEM_PAI', 'H');
		define('HIFEN', '-');
		define('LINHA_NOME_PRACA', 16);
		define('LINHA_INICIO_PLANILHA_LEITURA', 17);

		//Leitor Excel
		$objArquivoExcel = null;

		//Linha percorrida da planilha
		$linha = 0;

		//Indice da Planilha do Arquivo (xlsx) que sera lido (Somente a primeira - Indice 0 (zero))
		$iPlanilha = 0;

		//Conteudo da Celula Item Pai (Codigo de Barras)
		$val_EAN_ITEM_PAI_Lin = '';

		//Conteudo da Celula Encarte Tabloide (Utilizada para todas as regioes)
		$val_ENCARTE_TABLOIDE_lin = '';

		//Conteudo da Celula Valor Preco Venda Tabloide (Utilizada para todas as regioes)
		$val_PRECO_VENDA_TABLOIDE_lin = '';

		//Informacoes das celulas da planilha de leitura
		$configPlanilhaLeitura = array();

		//1a Regiao
		$item = array();
		$item['colunas_validacao'] = array('AE','AF');
		$item['pracas'] = array('AH',
								'AI',
								'AJ',
								'AK',
								'AL',
								'AM',
								'AN',
								'AO',
								'AP',
								'AQ',
								'AR',
								'AS',
								'AT',
								'AU',
								'AV',
								'AW',
								'AX',
								'AY',
								'AZ',
								'BA',
								'BB',
								'BC',
								'BD',
								'BE',
								'BF',
								'BG',
								'BH',
								'BI',
								'BJ',
								'BK');

		$item['descricao'] = 'D';
		$item['pagina'] = 'E';
		$item['ordem'] = 'F';
		$item['ean_item_pai'] = 'H';
		$item['preco_venda'] = 'U';
		$item['prazo_pagto'] = 'V';
		$item['valor_parcela'] = 'W';
		$item['observacoes'] = 'CW';
		$item['texto_legal'] = 'CX';
		$item['extra_1'] = 'CY';
		$item['extra_2'] = 'CZ';
		$item['extra_3'] = 'DA';
		$item['extra_4'] = 'DB';
		$item['extra_5'] = 'DC';
		$configPlanilhaLeitura[] = $item;

		//2a Regiao
		$item = array();
		$item['colunas_validacao'] = array('BL','BM');
		$item['pracas'] = array('BO',
								'BP',
								'BQ',
								'BR',
								'BS',
								'BT',
								'BU',
								'BV',
								'BW',
								'BX',
								'BY',
								'BZ',
								'CA');
		$item['descricao'] = 'D';
		$item['pagina'] = 'E';
		$item['ordem'] = 'F';
		$item['ean_item_pai'] = 'H';
		$item['preco_venda'] = 'U';
		$item['prazo_pagto'] = 'V';
		$item['valor_parcela'] = 'W';
		$item['observacoes'] = 'CW';
		$item['texto_legal'] = 'CX';
		$item['extra_1'] = 'CY';
		$item['extra_2'] = 'CZ';
		$item['extra_3'] = 'DA';
		$item['extra_4'] = 'DB';
		$item['extra_5'] = 'DC';
		$configPlanilhaLeitura[] = $item;

		//3a Regiao
		$item = array();
		$item['colunas_validacao'] = array('CB','CC');
		$item['pracas'] = array('CE',
								'CF',
								'CG',
								'CH',
								'CI',
								'CJ',
								'CK',
								'CL',
								'CM',
								'CN',
								'CO',
								'CP',
								'CQ',
								'CR',
								'CS',
								'CT',
								'CU',
								'CV');
		$item['descricao'] = 'D';
		$item['pagina'] = 'E';
		$item['ordem'] = 'F';
		$item['ean_item_pai'] = 'H';
		$item['preco_venda'] = 'U';
		$item['prazo_pagto'] = 'V';
		$item['valor_parcela'] = 'W';
		$item['observacoes'] = 'CW';
		$item['texto_legal'] = 'CX';
		$item['extra_1'] = 'CY';
		$item['extra_2'] = 'CZ';
		$item['extra_3'] = 'DA';
		$item['extra_4'] = 'DB';
		$item['extra_5'] = 'DC';
		$configPlanilhaLeitura[] = $item;

		//Array com os dados da planilha
		$dadosPracaProduto = array();

		//Array com os dados de cada produto (cada linha da planilha)
		$produto = array();

		// cria o leitor de excel
		$this->leitor = new ExcelReader($filename);

		//fazemos uma referencia local para failitar
		$objArquivoExcel = $this->leitor;

		//Abre Arquivo Excel
		try {
			$objArquivoExcel->open();
		} catch (Exception $e) {
			throw $e;
		}

		//Ira ler somente a primeira planilha do arquivo
		try {
			$objArquivoExcel->getSheetData($iPlanilha);
		} catch (Exception $e) {
			throw $e;
		}

		//Validar se planilha compativel com adaptador
		if (!$this->validate()) {
			throw new Exception('Planilha incompativel para o adaptador informado!', ExcelImporterFactory::ARQUIVO_INVALIDO);
		}

		//Inicio Linha a percorrer
		$linha = LINHA_INICIO_PLANILHA_LEITURA;

		//Conteudo da celula EAN_ITEM_PAI (Codigo de barras)
		$val_EAN_ITEM_PAI_Lin = $objArquivoExcel->getCellData($iPlanilha, (COLUNA_EAN_ITEM_PAI . $linha) );

		//Enquanto tiver valor na Coluna EAN ITEM PAI
		while ( !empty($val_EAN_ITEM_PAI_Lin) ) {

			//Regioes
			foreach ($configPlanilhaLeitura as $item) {

				//Array com os dados de cada produto (cada linha da planilha)
				$produto = array();

				//Conteudo da celula (ENCARTE TABLOIDE)
				$val_ENCARTE_TABLOIDE_lin = $objArquivoExcel->getCellData($iPlanilha, ($item['colunas_validacao'][0] . $linha) );

				//Conteudo da celula (PRECO VENDA TABLOIDE)
				$val_PRECO_VENDA_TABLOIDE_lin = $objArquivoExcel->getCellData($iPlanilha, ($item['colunas_validacao'][1] . $linha) );

				//Verifica se é nulo
				if (empty($val_PRECO_VENDA_TABLOIDE_lin)) {
					$val_PRECO_VENDA_TABLOIDE_lin = '';
				}

				//Consiste se a linha (produto) sera relacionado para importacao
				//( Se Coluna (Encarte Tabloide) for igual a S   E  Coluna (Preco Venda Tabloide) for diferente de EXCLUIR )
				if ( !empty($val_ENCARTE_TABLOIDE_lin) && strtoupper(trim($val_ENCARTE_TABLOIDE_lin)) == 'S' ) {
					if ( strtoupper(trim($val_PRECO_VENDA_TABLOIDE_lin)) != 'EXCLUIR' ) {

						//Recupera os valores necessarios de cada linha (produto da regiao x praca)
						$produto['descricao'] 		= $objArquivoExcel->getCellData($iPlanilha, ($item['descricao'] 	. $linha) );
						$produto['pagina'] 			= $objArquivoExcel->getCellData($iPlanilha, ($item['pagina'] 		. $linha) );
						$produto['ordem'] 			= $objArquivoExcel->getCellData($iPlanilha, ($item['ordem'] 		. $linha) );
						$produto['ean_item_pai'] 	= $objArquivoExcel->getCellData($iPlanilha, ($item['ean_item_pai'] 	. $linha) );
						$produto['preco_venda'] 	= $objArquivoExcel->getCellData($iPlanilha, ($item['preco_venda'] 	. $linha) );
						$produto['prazo_pagto'] 	= $objArquivoExcel->getCellData($iPlanilha, ($item['prazo_pagto'] 	. $linha) );
						$produto['valor_parcela'] 	= $objArquivoExcel->getCellData($iPlanilha, ($item['valor_parcela']	. $linha) );
						$produto['observacoes'] 	= $objArquivoExcel->getCellData($iPlanilha, ($item['observacoes'] 	. $linha) );
						$produto['texto_legal'] 	= $objArquivoExcel->getCellData($iPlanilha, ($item['texto_legal'] 	. $linha) );
						$produto['extra_1'] 		= $objArquivoExcel->getCellData($iPlanilha, ($item['extra_1'] 		. $linha) );
						$produto['extra_2'] 		= $objArquivoExcel->getCellData($iPlanilha, ($item['extra_2'] 		. $linha) );
						$produto['extra_3'] 		= $objArquivoExcel->getCellData($iPlanilha, ($item['extra_3'] 		. $linha) );
						$produto['extra_4'] 		= $objArquivoExcel->getCellData($iPlanilha, ($item['extra_4'] 		. $linha) );
						$produto['extra_5'] 		= $objArquivoExcel->getCellData($iPlanilha, ($item['extra_5'] 		. $linha) );
						
						// faz o tratamento no PRECO DE (Glpi 7212)
						if($produto['preco_venda'] == HIFEN){
							$produto['preco_venda'] = '0,00';
						} else if($produto['preco_venda'] === ''){
							$produto['preco_venda'] = NULL;
						}
						
						// faz o tratamento no VALOR DA PARCELA (Glpi 7212)
						if($produto['valor_parcela'] == HIFEN){
							$produto['valor_parcela'] = '0,00';
						} else if($produto['valor_parcela'] === ''){
							$produto['valor_parcela'] = NULL;
						}
						
						// faz o tratamento no PRESTACAO (Glpi 7212)
						if($produto['prazo_pagto'] == HIFEN){
							$produto['prazo_pagto'] = '0,00';
						} else if($produto['prazo_pagto'] === ''){
							$produto['prazo_pagto'] = NULL;
						}
						

						//Valor a vista de Cada Praca
						foreach ($item['pracas'] as $praca) {

							//Consiste o valor da praca
							$valor = $objArquivoExcel->getCellData($iPlanilha, ($praca . $linha) );
//							ob_start();
//							var_dump($valor);
//							$vv = ob_get_flush();
//							ob_clean();
//							file_put_contents('plan.txt', $praca.'-'.$linha.':'.$vv.PHP_EOL, FILE_APPEND);
							//AH-17:-
							//AI-17:100
							//AJ-17:-

							//Preco de cada praca
							//Se a coluna do preco da praca estiver preenchido
							//se a coluna estiver preenchida com um zero na planilha, o excel exibe um hifen
							//porém chega zero aqui e cai no empty, por iss verificamos o '0' com strict
							if( ($valor === '' || $valor === '0' || !empty($valor))) {
								// Se na coluna de valor estiver escrito NAO, ignora produto para aquela praca
								//if ( (mb_strtoupper(trim($valor)) != 'NÃO') && (mb_strtoupper(trim($valor)) != 'NAO') ) {
								if( ($valor != 'NÃO') && ($valor != 'NAO') && ($valor != 'nao') && ($valor != 'não') && ($valor != 'Não') && ($valor != 'Nao') ){
									//Se tiver um HIFEN no preco
									if ( trim($valor) == HIFEN ) {
										$produto['COLUNA_A_VISTA'] = '0,00';
									} else if($valor === ''){
										$produto['COLUNA_A_VISTA'] = NULL;
									} else {
										$produto['COLUNA_A_VISTA'] = $valor;
									}

									//Nome da Praca + Codigo EAN ITEM PAI
									$nomePraca = $objArquivoExcel->getCellData($iPlanilha, ($praca . LINHA_NOME_PRACA) );
									$dadosPracaProduto[$nomePraca][$val_EAN_ITEM_PAI_Lin] = $produto;
								}
							}
						}
					}
				}
			}

			//Incrementa linha e recupera proximo valor da coluna EAN_ITEM_PAI
			$linha++;

			//Conteudo da celula EAN_ITEM_PAI (Codigo de barras)
			$val_EAN_ITEM_PAI_Lin = $objArquivoExcel->getCellData($iPlanilha, (COLUNA_EAN_ITEM_PAI . $linha) );
		}

		//Liberando da memoria os dados da primeira planilha (unica que foi lida)
		try {
			$objArquivoExcel->disposeSheetData(0);
		} catch (Exception $e) {
			throw $e;
		}

		//Fechando arquivo excel
		try {
			$objArquivoExcel->close();
		} catch (Exception $e) {
			throw $e;
		}
		
//		ob_start();
//		var_dump($dadosPracaProduto);
//		$vv = ob_get_flush();
//		ob_clean();
//		file_put_contents('$dadosPracaProduto.txt', $vv, FILE_APPEND);
		
		return $dadosPracaProduto;
	}

	/**
	 * Gera Objeto RetailWorkbook a partir do array recebido ($dadosPracaProduto)
	 * @author Nelson Martucci
	 * @param $dadosPracaProduto: array contendo os valores de cada praca x produto
	 * @return RetailWorkbook
	 */

	private function geraRetailWorkbook($dadosPracaProduto){

		define('LINHA_INICIO_PLANILHA_GRAVACAO', 5);

		//Create RetailWorkbook.
		$workbook = new RetailWorkbook();

		//Informacoes das sheets/celulas a serem do objeto RetailWorkbook
		$configPlanilhaGravacao = array();

		//Auxiliar para calculo de coluna calculada
		$valorEconomize = 0;

		$configPlanilhaGravacao['pagina'] = 'B';
		$configPlanilhaGravacao['ordem'] = 'A';
		$configPlanilhaGravacao['codigo_barras'] = 'D';
		$configPlanilhaGravacao['descricao'] = 'E';
		$configPlanilhaGravacao['a_vista_de'] = 'L';
		$configPlanilhaGravacao['financiado_parcela'] = 'P';
		$configPlanilhaGravacao['financiado_prestacao'] = 'Q';
		$configPlanilhaGravacao['a_vista_a_vista'] = 'K';
		$configPlanilhaGravacao['a_vista_por'] = 'M';
		$configPlanilhaGravacao['a_vista_economize'] = 'N';
		$configPlanilhaGravacao['observacoes'] = 'U';
		$configPlanilhaGravacao['texto_legal'] = 'F';
		$configPlanilhaGravacao['extra_1'] = 'V';
		$configPlanilhaGravacao['extra_2'] = 'W';
		$configPlanilhaGravacao['extra_3'] = 'X';
		$configPlanilhaGravacao['extra_4'] = 'Y';
		$configPlanilhaGravacao['extra_5'] = 'Z';
		$configPlanilhaGravacao['ultima_coluna_dados'] = 'Z';

		// Varre Array com os dados da planilha de leitura e separa por pracas
		foreach ($dadosPracaProduto as $nomePraca => $itensPracaProduto) {

			// Cria uma sheet para cada praca dentro do workbook principal
			$sheet = $workbook->createSheet($nomePraca);

			//Para cada praça, uma sheet diferente
			//Cabecalho de cada Sheet
			$sheet->setCellData('A1', 'Codigo do Job');
		    $sheet->setCellData('B1', '');
		    $sheet->setCellData('A2', 'Texto Legal:');
		    $sheet->setCellData('G3', 'ATACAREJO');
		    $sheet->setCellData('K3', 'A VISTA');
		    $sheet->setCellData('O3', 'FINANCIADO');
		    $sheet->setCellData('U3', 'OBSERVACOES');
		    $sheet->setCellData('V3', 'EXTRA1');
		    $sheet->setCellData('W3', 'EXTRA2');
		    $sheet->setCellData('X3', 'EXTRA3');
		    $sheet->setCellData('Y3', 'EXTRA4');
		    $sheet->setCellData('Z3', 'EXTRA5');
		    $sheet->setCellData('A4', 'ORDEM');
		    $sheet->setCellData('B4', 'PAGINA');
		    $sheet->setCellData('C4', 'SUBCATEGORIA');
		    $sheet->setCellData('D4', 'CODIGO DE BARRAS');
		    $sheet->setCellData('E4', 'DESCRICAO');
		    $sheet->setCellData('F4', 'TEXTO LEGAL');
		    $sheet->setCellData('G4', 'PRECO UN.');
		    $sheet->setCellData('H4', 'TIPO UN.');
		    $sheet->setCellData('I4', 'PRECO CX.');
		    $sheet->setCellData('J4', 'TIPO CX.');
		    $sheet->setCellData('K4', 'A VISTA');
		    $sheet->setCellData('L4', 'DE');
		    $sheet->setCellData('M4', 'POR');
		    $sheet->setCellData('N4', 'ECONOMIZE');
		    $sheet->setCellData('O4', 'ENTRADA');
		    $sheet->setCellData('P4', 'PARCELAS');
		    $sheet->setCellData('Q4', 'PRESTAÇÃO');
		    $sheet->setCellData('R4', 'JUROS AM');
		    $sheet->setCellData('S4', 'JUROS AA');
		    $sheet->setCellData('T4', 'TOTAL PRAZO');

			//Inicia gravacao na linha 5
			$linha = LINHA_INICIO_PLANILHA_GRAVACAO;

			//Separa por Produto
			foreach ($itensPracaProduto as $nomeProduto => $itensProduto) {

				//Cada produto em uma linha
				$sheet->setCellData(($configPlanilhaGravacao['pagina'] 					. $linha), $itensProduto['pagina']);
				$sheet->setCellData(($configPlanilhaGravacao['ordem'] 					. $linha), $itensProduto['ordem']);
				$sheet->setCellData(($configPlanilhaGravacao['codigo_barras']			. $linha), $itensProduto['ean_item_pai']);
				$sheet->setCellData(($configPlanilhaGravacao['descricao']				. $linha), $itensProduto['descricao']);
				$sheet->setCellData(($configPlanilhaGravacao['a_vista_de'] 				. $linha), $itensProduto['preco_venda']);
				$sheet->setCellData(($configPlanilhaGravacao['financiado_parcela'] 		. $linha), $itensProduto['prazo_pagto']);
				$sheet->setCellData(($configPlanilhaGravacao['financiado_prestacao'] 	. $linha), $itensProduto['valor_parcela']);
				$sheet->setCellData(($configPlanilhaGravacao['a_vista_a_vista'] 		. $linha), $itensProduto['COLUNA_A_VISTA']);
				$sheet->setCellData(($configPlanilhaGravacao['observacoes'] 			. $linha), $itensProduto['observacoes']);
				$sheet->setCellData(($configPlanilhaGravacao['texto_legal'] 			. $linha), $itensProduto['texto_legal']);
				$sheet->setCellData(($configPlanilhaGravacao['extra_1'] 				. $linha), $itensProduto['extra_1']);
				$sheet->setCellData(($configPlanilhaGravacao['extra_2'] 				. $linha), $itensProduto['extra_2']);
				$sheet->setCellData(($configPlanilhaGravacao['extra_3'] 				. $linha), $itensProduto['extra_3']);
				$sheet->setCellData(($configPlanilhaGravacao['extra_4'] 				. $linha), $itensProduto['extra_4']);
				$sheet->setCellData(($configPlanilhaGravacao['extra_5'] 				. $linha), $itensProduto['extra_5']);

				//Celulas a serem calculadas que nao vieram na planilha de entrada/leitura
				$sheet->setCellData(($configPlanilhaGravacao['a_vista_por'] 			. $linha), $itensProduto['COLUNA_A_VISTA']);

				//Somente fara o calculo se as duas colunas forem numericas
				$valorEconomize = 0;
				if ( !empty($itensProduto['preco_venda']) && !empty($itensProduto['COLUNA_A_VISTA']) ) {
					if ( is_numeric($itensProduto['preco_venda']) && is_numeric($itensProduto['COLUNA_A_VISTA']) ) {
						$valorEconomize = $itensProduto['preco_venda'] - $itensProduto['COLUNA_A_VISTA'];
					}
				}

				$sheet->setCellData(($configPlanilhaGravacao['a_vista_economize'] 		. $linha), $valorEconomize);

				//Incrementa linha na mesma planilha
				$linha++;
			}

			//Ultima linha de dados
			$sheet->setMaxRow($linha--);

			//Ultima Coluna de dados
			$sheet->setMaxColumn($configPlanilhaGravacao['ultima_coluna_dados']);

		}

		//Retorna
		return $workbook;
	}
}
