<?php

require_once 'system/application/libraries/transformadores/ExcelTransform.php';

abstract class ExcelImporterFactory {

	/**
	 * Constante que indica que o arquivo e invalido
	 *
	 * Utilizado para excecoes
	 *
	 * @var unknown_type
	 */
	const ARQUIVO_INVALIDO = 0x01;

	/**
	 * Recupera um importador de Excel pelo nome
	 *
	 * Serve para facilitar a recupera de importadores
	 * em runtime.
	 *
	 * Passamos o sufixo do importador, que geralmente
	 * sera o nome do cliente. Exemplo:
	 *
	 * <code>
	 * $xlsImpoter = ExcelImporterFactory::getImporter('Walmart');
	 * $workbook = $xlsImpoter->import($filename);
	 * </code>
	 *
	 * @author Hugo Ferreira da Silva
	 * @param string $importerName Nome do importador
	 * @return IExcelImporter
	 * @throws Exception Quando nao encontrar o importar conforme o nome
	 */
	public static function getImporter($importerName){

		$dir = dirname(__FILE__);
		$classname = $importerName . 'ExcelImporter';
		$filename = $dir . DIRECTORY_SEPARATOR . $classname . '.php';

		if( !file_exists($filename) ){
			throw new Exception('Arquivo "'.$filename.'" nao encontrado');
		}

		require_once $filename;

		if( !class_exists($classname) ){
			throw new Exception('Classe "'.$classname.'" nao encontrada');
		}

		$ref = new ReflectionClass( $classname );
		$instance = $ref->newInstance();

		return $instance;
	}
}