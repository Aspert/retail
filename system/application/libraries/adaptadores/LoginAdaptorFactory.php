<?php

/**
 * Classe para gerenciamento de autenticacao padrao
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 */
class LoginAdaptorFactory {

	private static $instance;

	/**
	 * Construtor privado
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return LoginAdaptorFactory
	 */
	private function __construct(){

	}

	/**
	 * Recupera a instancia da fabrica de logins
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return LoginAdaptorFactory
	 */
	public static function getInstance(){
		if( is_null(self::$instance) ){
			self::$instance = new LoginAdaptorFactory();
		}

		return self::$instance;
	}

	/**
	 * Carrega um adaptador de login
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $type
	 * @return ILoginAdaptor
	 */
	public function load($type){
		$type .= 'Login';
		$dir = dirname(__FILE__) . '/';
		$filename = $dir . $type . '.php';

		if( file_exists($filename) ){
			require_once $filename;

			if( !class_exists($type) ){
				throw new Exception('Classe "'. $type . '" nao encontrada', -1);
			}

			$cls = new ReflectionClass($type);
			$inst = $cls->newInstance();

			return $inst;
		}

		throw new Exception('Arquivo "'. $filename. '" nao encontrado', -1);
	}

}