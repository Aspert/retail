<?php

/**
 * Interface para criacao de adaptadores de login
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 */
interface ILoginAdaptor {
	/**
	 * Efetua o login no sistema
	 *
	 * @param string $login
	 * @param string $senha
	 * @param string $coordenada
	 * @param string $numCoordenada
	 * @return boolean
	 */
	function login($login, $senha, $coordenada, $numCoordenada);

	/**
	 * Efetua a saida do sistema
	 * @return void;
	 */
	function logout();

	/**
	 * Troca a senha do usuario
	 *
	 * @param string $login
	 * @param string $senhaAntiga
	 * @param string $senhaNova
	 * @return boolean
	 */
	function trocarSenha($login, $senhaAntiga, $senhaNova);

	/**
	 * Recupera a quantidade de dias que faltam para a senha expirar
	 * @param string $login
	 * @return int
	 */
	function diasParaExpiracao($login);

	/**
	 * Recupera as politicas de troca de senha
	 *
	 * Dados / regras para troca de senha do usuario
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $login
	 * @return array
	 */
	function getPoliticaTrocaSenha($login);

	/**
	 * Retorna as mensagens para exibicao para o usuario sobre troca de senhas
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $login
	 * @return array
	 */
	function getMensagensTrocaSenha($login);

	/**
	 * Cria uma senha e reinicia o registro do usuario
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param $login
	 * @return string nova senha gerada
	 */
	function resetarSenha($login);

}