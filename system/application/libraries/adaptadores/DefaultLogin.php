<?php

require_once dirname(__FILE__) . '/ILoginAdaptor.php';

/**
 * Classe para gerenciamento de autenticacao padrao
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 */
class DefaultLogin implements ILoginAdaptor {

	private $wsURI;
	private $wsEndpoint;

	/**
	 * Instancia do WebService
	 *
	 * @var SoapClient
	 */
	private $service;

	/**
	 * Construtor
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 */
	public function __construct(){

		$ci = &get_instance();

		$this->setWsURI( $ci->config->item('loginWebServiceURI') );
		$this->setWsEndpoint( $ci->config->item('loginWebServiceEndpoint') );
	}

	/**
	 * Efetua o login no sistema
	 *
	 * @param string $login
	 * @param string $senha
	 * @param string $coordenada
	 * @param string $numCoordenada
	 * @return boolean
	 */
	public function login($login, $senha, $coordenada, $numCoordenada){
		/*require_once __DIR__.'/../../../../controleusuarios/system/application/models/UsuarioModel.php';
		require_once __DIR__.'/../../../../controleusuarios/system/application/controllers/service.php';
		$service = new Service();
		var_dump($service->login($login, $senha, $coordenada, $numCoordenada));
		exit();*/
		try {
			$teste = $this->getService();
			$teste->login($login, $senha, $coordenada, $numCoordenada);
		}catch (Exception $e){
			//var_dump($e);
			exit($e->getMessage().'123');
		}
        return $this->getService()->login($login, $senha, $coordenada, $numCoordenada);
	}

	/**
	 * Efetua a saida do sistema
	 * @return void
	 */
	public function logout(){

	}

	/**
	 * Troca a senha do usuario
	 *
	 * @param string $login
	 * @param string $senhaAntiga
	 * @param string $senhaNova
	 * @return boolean
	 */
	public function trocarSenha($login, $senhaAntiga, $senhaNova){
		return $this->getService()->changePassword($login, $senhaAntiga, $senhaNova);
	}

	/**
	 * Recupera a quantidade de dias que faltam para a senha expirar
	 *
	 * @param string $login
	 * @return int
	 */
	public function diasParaExpiracao($login){
		return $this->getService()->daysToExpirePassword($login);
	}

	/**
	 * Recupera as politicas de troca de senha
	 *
	 * Dados / regras para troca de senha do usuario
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $login
	 * @return array
	 */
	public function getPoliticaTrocaSenha($login){
		return $this->getService()->getPoliticaSenhaByLogin($login);
	}

	/**
	 * Retorna as mensagens para exibicao para o usuario sobre troca de senhas
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $login
	 * @return array
	 */
	public function getMensagensTrocaSenha($login){
		return $this->getService()->getRulesMessagesByLogin($login);
	}

	/**
	 * Cria uma senha e reinicia o registro do usuario
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param $login
	 * @return string nova senha gerada
	 */
	public function resetarSenha($login){
		return $this->getService()->resetPassword($login);
	}

	/**
	 * recupera a URI do webservice
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return string URI do webservice
	 */
	public function getWsURI()
	{
	    return $this->wsURI;
	}

	/**
	 * Altera a URI
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $wsURI
	 */
	public function setWsURI($wsURI)
	{
	    $this->wsURI = $wsURI;
	}

	/**
	 * recupera o endpoint
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return string o endpoint
	 */
	public function getWsEndpoint()
	{
	    return $this->wsEndpoint;
	}

	/**
	 * recupera a URI do webservice
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 */
	public function setWsEndpoint($wsEndpoint)
	{
	    $this->wsEndpoint = $wsEndpoint;
	}

	/**
	 * Retorna o objeto SoapClient
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return SoapClient objeto de webservice
	 */
	public function getService()
	{
		if( is_null($this->service) ){
			$config = array(
                'location' => $this->getWsEndpoint(),
                'uri'      => $this->getWsURI()
                /*'location' => 'http://localhost:8082/controleusuarios/index.php/service/index',
                'uri'      => 'http://localhost:8082/controleusuarios/index.php/service/index',*/
			);
			/*exit(var_dump($config));*/
			try {
				$this->service = new SoapClient(null, $config);
			}catch (Exception $e){
				exit($e->getMessage());
			}
			/*var_dump($this->service->__getFunctions());
			exit('Passou');*/
			/*var_dump($this->service);
			exit('Passou');*/
		}
	    return $this->service;
	}
}