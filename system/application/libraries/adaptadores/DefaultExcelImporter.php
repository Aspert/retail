<?php
/**
 * Classe de adaptador padrao de importacao de Excel
 *
 * Este adaptador simplesmente le o conteudo da planilha
 * padrao usada no sistema e retorna os valores utilizando
 * as classes auxiliares RetailWorkbook e RetailSheet
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 *
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'IExcelImporter.php';
require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'RetailWorkbook.php';
require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'ExcelReader.php';


class DefaultExcelImporter implements IExcelImporter {

	/**
	 * Leitor de excel
	 *
	 * @var ExcelReader
	 */
	protected $leitor = null;

	/**
	 * Metodo para importacao de excel
	 *
	 * @author Hugo Ferreira da Silva
	 * @param string $filename Caminho completo do arquivo
	 * @return RetailWorkbook
	 */
	public function import($filename, ITransform $transformer){
		// cria o leitor de excel
		$this->leitor = new ExcelReader($filename);
		$this->leitor->open();

		if( $this->validate() == false ){
			throw new Exception('O arquivo de Excel informado não é válido para este adaptador', ExcelImporterFactory::ARQUIVO_INVALIDO);
		}

		// referencia local para facilitar
		$reader = $this->leitor;

		$wb = new RetailWorkbook();

		// pega o numero total de planilhas
		$totalSheets = $reader->getSheetCount();

		for($i=0; $i<$totalSheets; $i++){
			$name = $reader->getSheetName( $i );
			$reader->getSheetData($i);

			$maxcol = $reader->getMaxCol($i);
			$maxrow = $reader->getMaxRow($i);

			$sheet = $wb->createSheet($name);
			$sheet->setMaxColumn($maxcol);
			$sheet->setMaxRow($maxrow);

			for($col=0; $col<=$maxcol; $col++){
				for($row=1; $row<=$maxrow; $row++){
					$coord = ExcelReader::stringFromColumnIndex($col) . $row;
					$sheet->setCellData( $coord, $reader->getCellData($i, $coord) );
				}
			}

			$reader->disposeSheetData($i);
		}

		$transformer->execute( $wb );

		return $wb;
	}

	/**
	 * Valida os dados do excel antes de importar
	 *
	 * Faz uma pequena verificacao para ver se os
	 * dados da planilha estao condizentes com o que espera
	 * este adaptador.
	 *
	 * @see system/application/libraries/adaptadores/IExcelImporter::validate()
	 * @return boolean
	 */
	public function validate(){
		// vamos verificar em todas as planilhas
		// se os cabecalhos estao certos
		$cols = array(
			'A4' => 'ORDEM',
			'B4' => 'PAGINA',
			'D4' => 'CODIGO DE BARRAS',
		);

		for( $plan = 0; $plan < $this->leitor->getSheetCount(); $plan ++ ){

			$this->leitor->getSheetData( $plan );

			foreach($cols as $key => $expected){
				$found = $this->leitor->getCellData($plan, $key);
				$found = fromUTF8($found);
				$found = mb_strtoupper($found, 'UTF-8');

				if( $expected != $found ){
					return false;
				}
			}
		}

		return true;
	}

}