<?
class MensagemUtil{
	
	/**
	 *
	 * @var MY_Controller
	 */
	private static $CI;
	
	/**
	 * 
	 * @param $chave
	 * @param $keys
	 * @return Mensagem
	 */
	public static function getMensagem($chave,array $keys){
		$CI =& get_instance();
		$msgArray = $CI->emailMensagem->getByChave($chave);
		if(isset($msgArray)){
			return self::replaceKeys($msgArray, $keys);
		}
		return new Mensagem();
	}
	
	/**
	 * Substitui as chaves pelos valores na mensagem e titulo
	 * @param $keys
	 * @return Mensagem
	 */
	private static function replaceKeys(array $msgArray, array $keys){
		$k = array_keys($keys);
		
		foreach ($k as $key => &$value) {
			$k[$key] = '{'.$value.'}';
		}
		
		$v = array_values($keys);
		
		$msg = new Mensagem();
		$msg->mensagem = str_replace($k, $v, $msgArray['MENSAGEM']);		
		$msg->titulo = str_replace($k, $v, $msgArray['TITULO']);
		
		return $msg;
	}
}


class Mensagem{
	public $mensagem;
	public $titulo;
}