<?php
/**
 * Classe para representar uma pasta de trabalho de Excel
 * 
 * Principalmente usada na importacao de planilhas.
 * Como podem haver varios tipos de arquivos a serem importados,
 * foi padronizado que todos os os adaptadores de importacao
 * retornarao dois objetos, RetailWorkbook e RetailSheet.
 * 
 * Esta classe serve somente para armazenamento e 
 * recuperacao de representacao de planilhas de forma mais 
 * organizada. Ela nao tem operacoes avancadas de 
 * tratamento de arquivos de Excel
 * 
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 * @package libraries
 *
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'RetailSheet.php';

class RetailWorkbook {
	/**
	 * Lista de planilhas da pasta de trabalho
	 * @var unknown_type
	 */
	private $sheets = array();
	
	/**
	 * Construtor
	 * 
	 * @author Hugo Ferreira da Silva
	 * @return RetailWorkbook
	 */
	public function __construct(){
		
	}
	
	/**
	 * Adiciona uma planilha a pasta de trabalho
	 * 
	 * @see RetailWorkbook::createSheet
	 * @author Hugo Ferreira da Silva
	 * @param RetailSheet $sheet
	 * @return void
	 */
	public function addSheet(RetailSheet $sheet){
		if( $this->getSheetByName($sheet->getSheetName()) != null ){
			throw new Exception('Voce nao pode ter duas planilhas com o mesmo nome em uma pasta de trabalho');
		}
		
		$this->sheets[] = $sheet;
	}
	
	/**
	 * Remove uma planilha da pasta de trabalho
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param RetailSheet $sheet
	 * @return void
	 */
	public function removeSheet(RetailSheet $sheet){
		$new = array();
		foreach( $this->sheets as $item ){
			if( $sheet != $item ){
				$new[] = $item;
			}
		}
		
		$this->sheets[] = $new;
	}
	
	/**
	 * Recupera uma planilha por nome
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param string $sheetName Nome da planilha desejada
	 * @return RetailSheet Objeto representando a planilha
	 */
	public function getSheetByName($sheetName){
		$result = null;
		
		foreach( $this->sheets as $item ){
			if( $sheetName == $item->getSheetName() ){
				$result = $item;
				break;
			}
		}
		
		return $result;
	}
	
	/**
	 * Recupera uma planilha pelo indice
	 * 
	 * <code>
	 * $sheet = $workbook->getSheetByIndex( 0 );
	 * </code>
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $sheetIndex
	 * @return RetailSheet
	 */
	public function getSheetByIndex($sheetIndex){
		$sheet = null;
		if( array_key_exists($sheetIndex, $this->sheets) ){
			$sheet = $this->sheets[ $sheetIndex ];
		}
		
		return $sheet;
	}
	
	/**
	 * Recupera todas as planilhas do workbook
	 * 
	 * @author Hugo Ferreira da Silva
	 * @return array Array contendo objetos do tipo RetailSheet
	 */
	public function getSheets(){
		return $this->sheets;
	}
	
	/**
	 * Cria uma planilha e ja adiciona ao Workbook
	 * 
	 * <code>
	 * $sheet = $workbook->createSheet('Planilha 1');
	 * 
	 * // depois podemos recuperar por
	 * $sheetRef = $workbook->getSheetByName('Planilha 1');
	 * </code>
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param string $sheetName Nome da planilha 
	 * @return RetailSheet Planilha criada
	 */
	public function createSheet( $sheetName ){
		$sheet = new RetailSheet( $sheetName );
		$this->addSheet( $sheet );
		return $sheet;
	}
	
	
}