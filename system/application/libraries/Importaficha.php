<?php
class Importaficha{
	


	//	 acesse web service da compra facil
	public static function compraFacil($referencia){
		$wsCompraFacil = 'http://biblia.comprafacil.com.br/cfWsManager/ReferenciaProdutoServiceBean';

		$client = new SoapClient($wsCompraFacil . '?wsdl');
		
		$function = 'buscaProdutoPorReferencia';
		 
		$arguments= array($function => array(
		                        'refcatalogo'   => $referencia
		                ));
		                
		$options = array('location' => $wsCompraFacil);
		 
		$result = $client->__soapCall($function, $arguments, $options);
		 
		return objectToArray($result);
	}
	
	//	 acesse web service da compra facil
	public static function hermes($referencia){
		$wsCompraFacil = 'http://biblia.comprafacil.com.br/cfWsManager/ReferenciaProdutoServiceBean';

		$client = new SoapClient($wsCompraFacil . '?wsdl');
		
		$function = 'consultaReferenciaHermes';
		 
		$arguments= array($function => array(
		                        'refcatalogo'   => $referencia
		                ));
		                
		$options = array('location' => $wsCompraFacil);
		 
		$result = $client->__soapCall($function, $arguments, $options);

		return objectToArray($result);
	}
	
	// trata o texto para retirar a MARCA
	public static function retornaMarca($texto){
		$texto = quebraLinha($texto);
		$pos1 = strpos($texto, 'Marca:');
		$pos2 = strpos($texto, '<br>', $pos1);

		if($pos1 && $pos2){
			$pos1 = ($pos1 + 6);
			$texto = substr($texto, $pos1, $pos2-$pos1);
			return trim($texto);
		}
		else{
			return false;
		}
	}
	
	// trata o texto para retirar o MODELO
	public static function retornaModelo($texto){
		$texto = quebraLinha($texto);
		$pos1 = strpos($texto, 'Modelo:');
		$pos2 = strpos($texto, '<br>', $pos1);

		if($pos1 && $pos2){
			$pos1 = ($pos1 + 7);
			$texto = substr($texto, $pos1, $pos2-$pos1);
			return trim($texto);
		}
		else{
			return false;
		}
	}
	
	// trata o texto para retirar o COR
	public static function retornaCor($texto){
		$texto = quebraLinha($texto);
		$pos1 = strpos($texto, 'Cor:');
		$pos2 = strpos($texto, '<br>', $pos1);

		if($pos1 && $pos2){
			$pos1 = ($pos1 + 4);
			$texto = substr($texto, $pos1, $pos2-$pos1);
			return trim($texto);
		}
		else{
			return false;
		}
	}
}