<?php
/**
 * Classes para acesso aos serviços do XinetKiwi
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 * @package XinetKiwi

 */

/**
 * Classe para gerenciar requisicoes ao servico XinetKiwi
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 * @package XinetKiwi

 */
class XinetKiwi {

	/**
	 * nome do usuario
	 *
	 * @var string
	 */
	protected $username;
	/**
	 * URL de origem da requisicao
	 *
	 * @var string
	 */
	protected $referer;
	/**
	 * Sistema solicitante
	 *
	 * @var string
	 */
	protected $system;

	/**
	 * Endereco do XinetKiwi
	 *
	 * @var string
	 */
	protected $xinetAddress = '';
	/**
	 * Porta de acesso ao XinetKiwi
	 *
	 * @var string
	 */
	protected $xinetPort = '';

	/**
	 * Instancia do XinetKiwi
	 *
	 * @var XinetKiwi
	 */
	private static $instance;


	/**
	 * Construtor
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return XinetKiwi
	 */
	protected function __construct(){
		$scheme = 'http://';
		if( !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ){
			$scheme = 'https://';
		}

		$this->setReferer( $scheme . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );
	}

	################### INICIO GETTERS / SETTERS ######################

	/**
	 * Recupera o nome do usuario que esta realizando a requisicao
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return string
	 */
	public function getUsername()
	{
	    return $this->username;
	}

	/**
	 * Altera o nome do usuario que esta realizando a requisicao
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $username
	 * @return void
	 */
	public function setUsername($username)
	{
	    $this->username = $username;
	}

	/**
	 * Recupera o endereco de origem da requisicao
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return string
	 */
	public function getReferer()
	{
	    return $this->referer;
	}

	/**
	 * Altera o endereco de origem da requisicao
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $referer
	 * @return void
	 */
	public function setReferer($referer)
	{
	    $this->referer = $referer;
	}

	/**
	 * Recupera o nome do sistema
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return string
	 */
	public function getSystem()
	{
	    return $this->system;
	}

	/**
	 * altera o nome do sistema
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $system
	 * @return void
	 */
	public function setSystem($system)
	{
	    $this->system = $system;
	}

	/**
	 * Recupera o endereco de acesso ao xinetkiwi
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return string
	 */
	public function getXinetAddress()
	{
	    return $this->xinetAddress;
	}

	/**
	 * Altera o endereco de acesso ao xinetkiwi
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $xinetAddress
	 * @return void
	 */
	public function setXinetAddress($xinetAddress)
	{
	    $this->xinetAddress = $xinetAddress;
	}

	/**
	 * Recupera a porta de acesso ao xinetkiwi
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return string
	 */
	public function getXinetPort()
	{
	    return $this->xinetPort;
	}

	/**
	 * Altera a porta de conexao com o xinetkiwi
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $xinetPort
	 * @return void
	 */
	public function setXinetPort($xinetPort)
	{
	    $this->xinetPort = $xinetPort;
	}

	################### FIM GETTERS / SETTERS ######################

	/**
	 * Retorna a instancia do XinetKiwi
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return XinetKiwi
	 */
	public static function getInstance(){
		if( is_null(self::$instance) ){
			self::$instance = new XinetKiwi();
		}

		return self::$instance;
	}

	/**
	 * Comando dummy para teste
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return XinetKiwiResponse Resposta do xinetkiwi
	 */
	public function dummy(){
		$command = new XinetKiwiCommand('dummy');
		return $this->execute($command);
	}

	/**
	 * Sincroniza um arquivo
	 *
	 * Este comando é utilizado para sincronizar o arquivo
	 * que está em storage monitorado pelo Xinet com o
	 * banco de dados Webnative.
	 *
	 * Após usar este utilitário, o registro do arquivo
	 * estará disponível no banco de dados para posterior
	 * manipulação pela aplicação solicitante.
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $path arquivo a ser sincronizado
	 * @return XinetKiwiResponse resposta do xinetkiwi
	 */
	public function syncvoltodb($path){
		$command = new XinetKiwiCommand('syncvoltodb');
		$command->addParameter(new XinetKiwiParameter('path', $path));

		return $this->execute( $command );
	}

	/**
	 * Altera as permissões de um arquivo ou pasta.
	 *
	 * A permissão deve ser no formato octal,
	 * exemplo: 0755, 0777
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string  $path Caminho do arquivo ou pasta que terá suas permissões alteradas
	 * @param string  $mode Permissão a ser aplicada
	 * @param boolean $recursive Indica se as permissões devem ser aplicadas recursivamente (para subpastas e arquivos)
	 * @return XinetKiwiResponse resposta do xinetkiwi
	 */
	public function chmod($path, $mode, $recursive = true){
		$command = new XinetKiwiCommand('chmod');
		$command->addParameter(new XinetKiwiParameter('path', $path));
		$command->addParameter(new XinetKiwiParameter('mode', $mode));
		$command->addParameter(new XinetKiwiParameter('recursive', $recursive));

		return $this->execute($command);
	}

	/**
	 * Sincroniza um diretorio
	 *
	 * Este comando é utilizado para sincronizar o arquivo
	 * que está em storage monitorado pelo Xinet com o
	 * banco de dados Webnative.
	 *
	 * Após usar este utilitário, o registro do arquivo
	 * estará disponível no banco de dados para posterior
	 * manipulação pela aplicação solicitante.
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $path pasta a ser sincronizada
	 * @return XinetKiwiResponse resposta do xinetkiwi
	 */
	public function syncvoltodbdir($path){
		$command = new XinetKiwiCommand('syncvoltodbdir');
		$command->addParameter(new XinetKiwiParameter('path', $path));

		return $this->execute( $command );
	}

	/**
	 * Gera preview e FPO de arquivos
	 *
	 * Comando utilizado para gerar os registros/arquivos
	 * de preview e também arquivos/registros utilizados
	 * no FPO (For Preview Only). Os arquivos FPO são
	 * utilizados em PDF's para aprovação e também
	 * na diagramação dos arquivos pelo InDesign
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $path arquivo para gerar preview/fpo
	 * @param boolean $web gera ou nao o preview
	 * @param boolean $fpo gera ou nao o fpo
	 * @return XinetKiwiResponse resposta do xinetkiwi
	 */
	public function dtrebuild($path, $web = true, $fpo = true){
		$command = new XinetKiwiCommand('dtrebuild');
		$command->addParameter(new XinetKiwiParameter('path', $path));
		$command->addParameter(new XinetKiwiParameter('fpo', $fpo));
		$command->addParameter(new XinetKiwiParameter('web', $web));

		return $this->execute( $command );
	}

	/**
	 * Executa o sync e o dtrebuild
	 *
	 * Atalho para efetuar simultaneamente os comandos syncvoltodb e dtrebuild.
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $path arquivo para efetuar as operacoes
	 * @return XinetKiwiResponse resposta do xinetkiwi
	 */
	public function syncpreview( $path ){
		$command = new XinetKiwiCommand('syncpreview');
		$command->addParameter(new XinetKiwiParameter('path', $path));

		return $this->execute( $command );
	}

	/**
	 * Executa o sync e o dtrebuild em uma pasta
	 *
	 * Atalho para efetuar simultaneamente os comandos syncvoltodb e dtrebuild.
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $path pasta para efetuar as operacoes
	 * @return XinetKiwiResponse resposta do xinetkiwi
	 */
	public function syncpreviewdir( $path ){
		$command = new XinetKiwiCommand('syncpreviewdir');
		$command->addParameter(new XinetKiwiParameter('path', $path));

		return $this->execute( $command );
	}

	/**
	 * Altera os metadados do arquivo, tanto no banco de dados quanto fisicamente
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $path Caminho do arquivo
	 * @param string $type Valores de type aceitos pelo Xinet (MacType válido)
	 * @param string $creator Valores de creator (aplicação que criou o arquivo)
	 * @return XinetKiwiResponse resposta do xinetkiwi
	 */
	public function kats($path, $type, $creator){
		$command = new XinetKiwiCommand('kats');
		$command->addParameter(new XinetKiwiParameter('path', $path));
		$command->addParameter(new XinetKiwiParameter('type', $type));
		$command->addParameter(new XinetKiwiParameter('creator', $creator));

		return $this->execute( $command );
	}
	
	/**
	 * Retorna o type/creator que o Xinet escolheria para um arquivo 
	 * ou arquivos de uma pasta.
	 * Pode também ser usado para corrigir o type/creator desses arquivos
	 * passando o parâmetro fix=true. Isso altera o type/creator para 
	 * o mesmo escolhido pelo Xinet.
	 *
	 * @author Juliano Polito
	 * @link http://www.247id.com.br
	 * @param string $path Caminho do arquivo
	 * @param boolean $fix True para reparar o type creator do arquivo, false para retornar o tipo.
	 * @return XinetKiwiResponse resposta do xinetkiwi
	 */
	public function katype($path, $fix){
		$command = new XinetKiwiCommand('katype');
		$command->addParameter(new XinetKiwiParameter('path', $path));
		$command->addParameter(new XinetKiwiParameter('fix', $fix));

		return $this->execute( $command );
	}
	
	/**
	 * Retorna informações sobre o arquivo desejado
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $path Caminho do arquivo que deseja recuperar informações
	 * @return XinetKiwiResponse resposta do xinetkiwi
	 */
	public function imagetoxxinf( $path ){
		$command = new XinetKiwiCommand('imagetoxxinf');
		$command->addParameter(new XinetKiwiParameter('path', $path));

		return $this->execute( $command );
	}

	/**
	 * Copia um arquivo
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $origem Caminho de origem
	 * @param string $destino Caminho de destino
	 * @return XinetKiwiResponse resposta do xinetkiwi
	 */
	public function kscp( $origem, $destino ){
		$command = new XinetKiwiCommand('kscp');
		$command->addParameter(new XinetKiwiParameter('origem', $origem));
		$command->addParameter(new XinetKiwiParameter('destino', $destino));

		return $this->execute( $command );
	}

	/**
	 * Move ou renomeia um arquivo
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $origem Caminho de origem
	 * @param string $destino Caminho de destino
	 * @return XinetKiwiResponse resposta do xinetkiwi
	 */
	public function ksmv( $origem, $destino ){
		$command = new XinetKiwiCommand('ksmv');
		$command->addParameter(new XinetKiwiParameter('origem', $origem));
		$command->addParameter(new XinetKiwiParameter('destino', $destino));

		return $this->execute( $command );
	}

	/**
	 * Remove um arquivo
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $path Caminho do arquivo a ser removido
	 * @return XinetKiwiResponse resposta do xinetkiwi
	 */
	public function ksrm( $path ){
		$command = new XinetKiwiCommand('ksrm');
		$command->addParameter(new XinetKiwiParameter('path', $path));

		return $this->execute( $command );
	}

	/**
	 * Remove um diretório
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $path Caminho do diretório a ser removido
	 * @return XinetKiwiResponse resposta do xinetkiwi
	 */
	public function ksrmdir( $path ){
		$command = new XinetKiwiCommand('ksrmdir');
		$command->addParameter(new XinetKiwiParameter('path', $path));

		return $this->execute( $command );
	}

	/**
	 * Converte uma imagem para outro formato e dpi utilizando image magick
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $origem Caminho do arquivo de origem
	 * @param string $destino Caminho do arquivo de destino
	 * @param int $dpi Resolução para o arquivo de destino
	 * @return XinetKiwiResponse resposta do xinetkiwi
	 */
	public function convertDPI($origem, $destino, $dpi){
		$command = new XinetKiwiCommand('convertDPI');
		$command->addParameter(new XinetKiwiParameter('origem', $origem));
		$command->addParameter(new XinetKiwiParameter('destino', $destino));
		$command->addParameter(new XinetKiwiParameter('dpi', $dpi));

		return $this->execute( $command );
	}

	/**
	 * Cria um preview de audio em formato mp3
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $origem Caminho do arquivo de origem
	 * @param string $destino Caminho do arquivo de destino do preview
	 * @return XinetKiwiResponse resposta do xinetkiwi
	 */
	public function ffmpegAudio($origem, $destino){
		$command = new XinetKiwiCommand('ffmpegAudio');
		$command->addParameter(new XinetKiwiParameter('origem', $origem));
		$command->addParameter(new XinetKiwiParameter('destino', $destino));

		return $this->execute( $command );
	}

	/**
	 * Cria um preview de video no formato flv utilizando o ffmpeg
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $origem Caminho do arquivo de origem
	 * @param string $destino Caminho do arquivo de destino do preview
	 * @param string $size Tamanho do video no formato [WIDTH]x[HEIGHT] Ex: 320x240
	 * @return XinetKiwiResponse resposta do xinetkiwi
	 */
	public function ffmpegVideo($origem, $destino, $size){
		$command = new XinetKiwiCommand('ffmpegVideo');
		$command->addParameter(new XinetKiwiParameter('origem', $origem));
		$command->addParameter(new XinetKiwiParameter('destino', $destino));
		$command->addParameter(new XinetKiwiParameter('size', $size));

		return $this->execute( $command );
	}

	/**
	 * Utiliza o rip do xinet para gerar arquivos de preview (PDF?)
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $origem Arquivo original
	 * @param string $destino Arquivo de destino
	 * @param string $formato Equivale ao argumento -sDEVICE (Ex: jpgcmyk)
	 * @param int $dpi Resolução do arquivo. Equivale ao argumento -r.
	 * @return XinetKiwiResponse resposta do xinetkiwi
	 */
	public function ripDPI($origem, $destino, $formato, $dpi){
		$command = new XinetKiwiCommand('ripDPI');
		$command->addParameter(new XinetKiwiParameter('origem', $origem));
		$command->addParameter(new XinetKiwiParameter('destino', $destino));
		$command->addParameter(new XinetKiwiParameter('formato', $formato));
		$command->addParameter(new XinetKiwiParameter('dpi', $dpi));

		return $this->execute( $command );
	}

	/**
	 * Cria um novo diretorio utilizando o mkdir do OS, e efetua o sync (syncvoltodb) e atribui permissoes 777
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $path
	 * @return XinetKiwiResponse resposta do xinetkiwi
	 */
	public function mkdirsync( $path ){
		$command = new XinetKiwiCommand('mkdirsync');
		$command->addParameter(new XinetKiwiParameter('path', $path));

		return $this->execute( $command );
	}

	/**
	 * Efetua uma requisição ao servidor XinetKiwi
	 *
	 * A requisicao é sempre feita em Raw Format.
	 * É enviado uma string no formato JSON para o
	 * servidor. Esta mensagem deverá sempre ser
	 * em uma única linha, seguida de \r\n\r\n.
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param XinetKiwiCommand $command comando a ser executado
	 * @return XinetKiwiResponse Resposta gerada pelo XinetKiwi
	 */
	protected function execute(XinetKiwiCommand $command){
		// ajustamos o restante dos parametros
		$command->addParameter(new XinetKiwiParameter('username', $this->getUsername()));
		$command->addParameter(new XinetKiwiParameter('referer', $this->getReferer()));
		$command->addParameter(new XinetKiwiParameter('system', $this->getSystem()));

		// criamos a string json
		$JSONMsg = $command->getJSON();
		$JSONMsg .= "\r\n\r\n";

		// abrimos a conexao
		$fs = fsockopen($this->xinetAddress, $this->xinetPort, $errno, $errstr, 60);
		
		// enviamos o json
		fwrite($fs, $JSONMsg);

		// var para armazenar a resposta
		$resp = '';

		// enquando houver resposta
		try{
			while( !feof($fs) ){
				$resp .= fgets($fs, 1024);
			}
		}
		catch(Exception $e){
		}

		// fechamos a conexao
		fclose($fs);

		// tiramos espacos em branco desnecessarios
		$resp = trim($resp);

		// objeto json
		$resposta = null;

		$json = json_decode($resp);

		if( is_null($json) ){
			$json = new stdClass();
			$json->returnMessage = $resp;
			$json->errorCode = -1;
		}

		// resposta do xinetkiwi
		$resposta = new XinetKiwiResponse( $json );

		// retornamos a resposta enviada pelo xinet kiwi
		return $resposta;
	}

}

/**
 * Classe que representa a resposta do XinetKiwi
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 * @package XinetKiwi

 */
class XinetKiwiResponse {

	/**
	 * Mensagem de retorno da requisicao
	 *
	 * @var string
	 */
	public $returnMessage;
	/**
	 * Codigo de retorno da requisicao
	 *
	 * @var int
	 */
	public $errorCode;

	/**
	 * Objeto JSON retornado
	 *
	 * @var stdClass
	 */
	private $_response;
	/**
	 * Chaves que o objeto contem
	 *
	 * @var array
	 */
	private $_keys;

	/**
	 * Construtor
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param stdClass $response resposta do XinetKiwi
	 * @return XinetKiwiResponse
	 */
	public function __construct(stdClass $response){
		$this->_response = $response;

		$properties = get_object_vars($response);
		$keys = array_keys($properties);

		$this->_keys = $keys;

		if( in_array('returnMessage', $keys) ){
			$this->returnMessage = $response->returnMessage;
		}

		if( in_array('errorCode', $keys) ){
			$this->errorCode = $response->errorCode;
		}
	}

	/**
	 * Getter implicito para propriedades do objeto
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $name nome da propriedade
	 * @return mixed valor da propriedade
	 */
	public function __get($name){
		if( in_array($name, $this->_keys) ){
			return $this->_response->$name;
		}

		return null;
	}

	/**
	 * Retorna uma representacao em string do objeto
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return string
	 */
	public function __toString(){
		return json_encode( $this );
	}

}

/**
 * Classe que representa um parametro a ser enviado para o xinet
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 * @package XinetKiwi

 */
class XinetKiwiParameter {

	/**
	 * Nome do parametro
	 *
	 * @var string
	 */
	public $name;
	/**
	 * Valor do parametro
	 *
	 * Marcado como mixed por que pode ser enviado qualquer tipo
	 * de valor (array, objeto, string, inteiro, etc)
	 *
	 * @var mixed
	 */
	public $value;

	/**
	 * Construtor
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $name Nome do parametro
	 * @param mixed $value Valor do parametro
	 * @return XinetKiwiParameter
	 */
	public function __construct($name, $value){
		$this->name = $name;
		$this->value = $value;
	}

}

/**
 * Classe que representa um comando que sera efetuado
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 * @package XinetKiwi

 */
class XinetKiwiCommand {
	/**
	 * Parametros que serao enviados juntamente com o comando
	 *
	 * @var array
	 */
	protected $parameters;
	/**
	 * Nome do comando a ser executado
	 *
	 * @var string
	 */
	protected $name;

	/**
	 * Construtor
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $name Nome do comando
	 * @return XinetKiwiCommand
	 */
	public function __construct($name){
		$this->name = $name;
		$this->parameters = array();
	}

	/**
	 * Recupera o nome do comando
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return string
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * Adiciona um parametro ao comando
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param XinetKiwiParameter $parameter
	 * @return void
	 */
	public function addParameter(XinetKiwiParameter $parameter){
		$this->parameters[] = $parameter;
	}

	/**
	 * Remove um parametro do comando
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param XinetKiwiParameter $parameter
	 * @return void
	 */
	public function removeParameter(XinetKiwiParameter $parameter){
		$pos = array_search($parameter, $this->parameters);
		array_splice($this->parameters, $pos);
	}

	/**
	 * Remove um parametro pelo nome
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $parameterName Nome do parametro
	 * @return void
	 */
	public function removeParameterByName($parameterName){
		$newParameters = array();

		foreach( $this->parameters as $param ){
			if( $parameterName != $param->name ){
				$newParameters[] = $param;
			}
		}

		$this->parameters = $newParameters;
	}

	/**
	 * Recupera os parametros do comando
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return array
	 */
	public function getParameters(){
		return $this->parameters;
	}

	/**
	 * Retorna uma representacao JSON do comando
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return string
	 */
	public function getJSON(){
		$obj = new stdClass();

		$obj->action = $this->name;

		foreach($this->getParameters() as $parameter){
			$obj->{ $parameter->name } = $parameter->value;
		}

		return json_encode($obj);
	}
}