<?php
/**
 * Classe para gerar os graficos relacionados ao processo
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 */
class GraficoProcesso {

	/**
	 * lista de etapas
	 *
	 * @var array
	 */
	private $etapas = array();
	/**
	 * largura da imagem
	 *
	 * @var int
	 */
	private $largura = 830;
	/**
	 * Altura da imagem
	 *
	 * @var int
	 */
	private $altura = 300;
	/**
	 * altura da etapa no grafico
	 *
	 * @var float
	 */
	private $alturaEtapa = null;
	/**
	 * largura do dia no grafico
	 *
	 * @var float
	 */
	private $larguraDia = null;
	/**
	 * tamanho da margem
	 *
	 * @var int
	 */
	private $margemEsquerda = 140;
	/**
	 * tamanho da margem direita
	 *
	 * @var int
	 */
	private $margemDireita = 1;
	/**
	 * tamanho da margem superior
	 *
	 * @var int
	 */
	private $margemSuperior = 1;
	/**
	 * tamanho da margem inferior
	 *
	 * @var int
	 */
	private $margemInferior = 50;
	/**
	 * cores alocadas para a imagem
	 *
	 * @var array
	 */
	private $cores = array();
	/**
	 * imagem
	 *
	 * @var resource
	 */
	private $img = null;
	/**
	 * numero de dias do cronograma
	 *
	 * @var int
	 */
	private $dias = 0;

	/**
	 * Indice de colunas por data
	 *
	 * Como as datas podem vir mescladas
	 * e ate mesmo datas iguais, temos que fazer um
	 * indice de colunas, para sabermos
	 * por data, em qual coluna a etapa comeca.
	 *
	 * O indice da coluna sera a data no
	 * formato do banco, exemplo:
	 *
	 * <code>
	 * $this->colunas['2010-08-01'] = 31;
	 * </code>
	 *
	 * Ela grava somente a data de inicio, e nao de fim!
	 * A quantidade de colunas que sera ocupada
	 * fica a cargo do metodo que desenha
	 * a etapa no grafico.
	 *
	 * @var array
	 */
	private $colunas = array();

	/**
	 * posicoes das etapas
	 *
	 * @var array
	 */
	private $posicoes = array();

	/**
	 * Construtor
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return GraficoProcesso
	 */
	public function __construct($width = 830, $height = 350, $larguraDia = null, $alturaEtapa = null){
		$this->setLargura($width);
		$this->setAltura($height);

		$this->setLarguraDia($larguraDia);
		$this->setAlturaEtapa($alturaEtapa);
	}

	/**
	 * Adiciona uma etapa para gerar o grafico
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param GraficoProcessoEtapa $etapa
	 * @return void
	 */
	public function addEtapa(GraficoProcessoEtapa $etapa){
		$this->etapas[] = $etapa;

		if( !array_key_exists($etapa->getNome(), $this->posicoes) ){
			$this->posicoes[$etapa->getNome()] = count($this->posicoes);
		}
	}

	/**
	 * Remove uma etapa
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param GraficoProcessoEtapa $etapa
	 * @return void
	 */
	public function removeEtapa(GraficoProcessoEtapa $etapa){
		$nova = array();
		foreach($this->etapas as $item){
			if( $item != $etapa ) {
				$nova[] = $item;
			}
		}
	}

	public function removeAllEtapas(){
		$this->etapas = array();
	}


	public function getLargura()
	{
	    return $this->largura;
	}

	public function setLargura($largura)
	{
	    $this->largura = $largura;
	}

	public function getAltura()
	{
		$altura = count($this->etapas) * $this->getAlturaEtapa() + $this->getMargemInferior() + $this->getMargemSuperior();
	    return $this->altura;
	}

	public function setAltura($altura)
	{
	    $this->altura = $altura;
	}

	public function getAlturaEtapa()
	{
	    return $this->alturaEtapa;
	}

	protected function setAlturaEtapa($alturaEtapa)
	{
	    $this->alturaEtapa = $alturaEtapa;
	}

	public function getLarguraDia()
	{
	    return $this->larguraDia;
	}

	protected function setLarguraDia($larguraDia)
	{
	    $this->larguraDia = $larguraDia;
	}

	public function getMargemEsquerda()
	{
	    return $this->margemEsquerda;
	}

	public function setMargemEsquerda($margemEsquerda)
	{
	    $this->margemEsquerda = $margemEsquerda;
	}

	public function getMargemDireita()
	{
	    return $this->margemDireita;
	}

	public function setMargemDireita($margemDireita)
	{
	    $this->margemDireita = $margemDireita;
	}

	public function getMargemSuperior()
	{
	    return $this->margemSuperior;
	}

	public function setMargemSuperior($margemSuperior)
	{
	    $this->margemSuperior = $margemSuperior;
	}

	public function getMargemInferior()
	{
	    return $this->margemInferior;
	}

	public function setMargemInferior($margemInferior)
	{
	    $this->margemInferior = $margemInferior;
	}

	protected function setColor($label, $color){
		$arr = $this->hex2rgb($color);
		$this->cores[$label] = imagecolorallocate($this->img, $arr['red'], $arr['green'], $arr['blue']);
	}

	/**
	 * Recupera uma cor alocada
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $label
	 * @return int
	 */
	protected function getColor($label){
		if( array_key_exists($label, $this->cores) ){
			return $this->cores[$label];
		}

		return null;
	}

	/**
	 * metodo principal para gerar o grafico de cronograma
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	public function cronograma_gerar(){
		$last = end($this->etapas);
		$first = reset($this->etapas);

		$qtdDias = ((strtotime($last->getDataFim()) - strtotime($first->getDataInicio())) / (3600 * 24))+1;
		$etapas = array();

		foreach($this->etapas as $etapa){
			$etapas[] = $etapa->getNome();
		}

		$etapas = array_unique($etapas);

		// vamos checar se o usuario ja informou uma largura para o dia
		// e uma altura para etapa.
		// se ele fez isso, vamos alterar o tamanho da imagem
		// antes de iniciar o recurso
		if( !empty($this->larguraDia) ){
			$largura = ($this->getLarguraDia() * $qtdDias)
				+ $this->getMargemDireita()
				+ $this->getMargemEsquerda();

			$this->setLargura($largura);
		}

		if( !empty($this->alturaEtapa) ){
			$altura = ($this->getAlturaEtapa() * count($etapas))
				+ $this->getMargemInferior()
				+ $this->getMargemSuperior();

			$this->setAltura($altura);
		}

		// image handler
		$this->img = imagecreatetruecolor($this->getLargura(), $this->getAltura());
		// aloca algumas cores
		$this->setColor('branco', 'FFFFFF');
		$this->setColor('cinza', '999999');
		$this->setColor('cinzaClaro', 'CCCCCC');
		$this->setColor('preto', '000000');
		$this->setColor('cinzaBgDia', 'EFEFEF');

		$this->setColor('verde',    '006738');
		$this->setColor('amarelo',  'FFF9AE');
		$this->setColor('vermelho', 'BE1E2D');

		// se nao tiver etapas
		if( count($this->etapas) == 0 ){
			imagefill($this->img, 0, 0, $this->getColor('branco'));
			imagestring(
				$this->img
				, 2
				, 10
				, $this->getAltura() / 2 - imagefontheight(2) / 2
				, 'Nao ha etapas para gerar o grafico. Se estiver alterando um job, salve-o e tente novamente.'
				, $this->getColor('preto')
			);

		} else {
			// desenha as etapas
			$this->cronograma_desenhaBase();
			$this->cronograma_desenhaEtapas();
			$this->cronograma_desenhaGrade();
		}

		// enviando a imagem
		header("Content-Type: image/gif");
		imagegif($this->img,'',90);
	}

	/**
	 * Desenha a base do grafico
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return
	 */
	protected function cronograma_desenhaBase(){
		// fundo branco
		imagefill($this->img, 0,0,$this->getColor('branco'));

		// recuperando as datas
		$ultima = end($this->etapas);
		$primeira = reset($this->etapas);

		$dataInicial = strtotime($primeira->getDataInicio());
		$dataFinal = strtotime($ultima->getDataFim());
		$dias = ($dataFinal - $dataInicial) / 24 / 3600;
		$this->dias = $dias+1;

		if( empty($this->larguraDia) ){
			// seta a largura de cada dia
			$this->setLarguraDia(($this->getLargura() - $this->getMargemEsquerda() - $this->getMargemDireita()) / $this->dias);
		}

		if( empty($this->alturaEtapa) ){
			// seta a altura de cada etapa
			$this->setAlturaEtapa( ($this->getAltura() - $this->getMargemInferior() - $this->getMargemSuperior()) / count($this->posicoes) );
		}

		// desenha a faixa do dia de hoje, se for o caso
		$hoje = strtotime(date('Y-m-d'));
		// cada coluna e um dia
		for($col = 0; $col <= $this->dias; $col++){
			$x = $this->getMargemEsquerda() + $col * $this->getLarguraDia();
			$y = $this->getMargemSuperior();
			$altura = $this->getAlturaEtapa() * (count($this->etapas)) + $this->getMargemSuperior();

			$dataTeste = $dataInicial + ($col * 24 * 3600);

			if( $dataTeste == $hoje && $col < $this->dias ){
				imagefilledrectangle($this->img
					, $x
					, $y
					, $x + $this->getLarguraDia()
					, $altura
					, $this->getColor('cinzaBgDia')
				);
			}

			// vamos criar o indice de colunas
			$this->colunas[date('Y-m-d', $dataTeste)] = $col;
		}
	}

	/**
	 * Desenhamos as etapas
	 *
	 * Colocamos os nomes das etapas e tambem
	 * desenhamos as linhas coloridas que
	 * representam uma etapa
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	protected function cronograma_desenhaEtapas(){
		// tamanho da fonte
		$fontSize = 2;
		$charSize = 6;

		$lastcol = 0;

		$hoje = strtotime(date('Y-m-d'));

		// criando os labels das etapas
		foreach($this->posicoes as $nome => $i) {
			//$etapa = $this->getEtapaByPosicao($i);

			// desenhando o label
			$x = 0;
			$y = $this->getMargemSuperior() + $i * $this->getAlturaEtapa();
			$y += $this->getAlturaEtapa() / 2 - (($fontSize*$charSize)/2);

			$color = $this->getColor('cinza');

			for($j=0; $j<count($this->etapas); $j++) {
				$et = $this->getEtapa($j);
				if( $et->getNome() == $nome ){
					if( strtotime($et->getDataInicio()) <= $hoje && strtotime($et->getDataFim()) >= $hoje ){
						$color = $this->getColor('preto');
					}
				}
			}

			imagestring(
				$this->img
				, $fontSize
				, $x
				, $y
				, $nome
				, $color
			);
		}

		// colocando as barras
		for($i=0; $i<count($this->etapas); $i++){
			$etapa = $this->getEtapa($i);

			// pegando coluna inicial pelo dia do cronograma
			$qtdCol = (strtotime($etapa->getDataFim()) - strtotime($etapa->getDataInicio()))/24/3600 + 1;
			$currCol = $this->colunas[ date('Y-m-d', strtotime($etapa->getDataInicio())) ];

			//echo $etapa->getNome() . ': ' . $qtdCol . '<br>';

			// coordenadas
			$x = ($currCol * $this->getLarguraDia()) + $this->getMargemEsquerda();
			$largura = $x + $this->getLarguraDia() * $qtdCol;

			$y = $this->getMargemSuperior() + ($this->posicoes[$etapa->getNome()] * $this->getAlturaEtapa());
			$altura = $y + $this->getAlturaEtapa();

			// alocando as cores
			$this->setColor($etapa->getCor(), $etapa->getCor());

			// gerando
			imagefilledrectangle(
				$this->img
				, $x
				, $y
				, $largura
				, $altura
				, $this->getColor($etapa->getCor())
			);

		}
		//exit;
	}

	/**
	 * Desenhamos grade superior / pontilhada
	 *
	 * Tambem desenhamos os numeros dos dias no rodape
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return void
	 */
	protected function cronograma_desenhaGrade(){
		// alterando o estilo da linha
		imagesetstyle($this->img
			, array(
				$this->getColor('cinzaClaro'),
				$this->getColor('cinzaClaro'),
				$this->getColor('cinzaClaro'),
				$this->getColor('cinzaClaro'),
				IMG_COLOR_TRANSPARENT,
				IMG_COLOR_TRANSPARENT,
				IMG_COLOR_TRANSPARENT,
				IMG_COLOR_TRANSPARENT,
				IMG_COLOR_TRANSPARENT,
			)
		);

		// pega a primeira e ultima etapa
		$first = $this->getEtapa(0);
		$last = $this->getEtapa( count($this->etapas) - 1 );
		$inicio = strtotime($first->getDataInicio());
		$fim = strtotime($last->getDataFim());

		$months = array();

		for($dia=0; $dia < $this->dias; $dia++){
			$time = $inicio + ($dia * 24 * 3600);
			// desenhando o fundo.
			$y = $this->getAltura() - $this->getMargemInferior();
			$y2 = $y + $this->getMargemInferior();
			$x = $this->getMargemEsquerda()
				+ ($dia * $this->getLarguraDia());

			$x2 = $x + $this->getLarguraDia();

			// se o tipo de grafico for atual
			if( $first->getTipo() == Processo::TIPO_ATUAL ){
				$color = $this->getColor('verde');
			} else {
				$color = $this->getColor('cinza');
			}

			imagefilledrectangle($this->img, $x, $y, $x2, $y2, $color);

			// configuracoes para escrever o dia
			$date = date('d', $time);
			$y = $this->getAltura() - $this->getMargemInferior() + 5;
			$x = $this->getMargemEsquerda()
				+ ($dia * $this->getLarguraDia())
				+ ($this->getLarguraDia() / 2)
				- 6;

			// string do dia
			imagestring(
				$this->img
				, 2
				, $x
				, $y
				, $date
				, $this->getColor('branco')
			);

			// pegando o mes
			$currmonth = date('m-Y', $time);
			// se nao existe, cria
			if( !array_key_exists($currmonth, $months) ){
				$months[$currmonth] = 1;
			} else {
				$months[$currmonth]++;
			}
		}

		// espaco para o mes
		$espacoMes = 28;

		// barra dos meses
		imagefilledrectangle($this->img
			, $this->getMargemEsquerda()
			, $this->getAltura() - $espacoMes
			, $this->getLargura() - $this->getMargemDireita()
			, $this->getAltura()
			, $this->getColor('cinzaBgDia')
		);

		// muda o locale
		setlocale(LC_TIME, "pt_BR", "pt_BR.iso-8859-1", "pt_BR.utf-8", "portuguese");

		// coluna inicial
		$col = 0;
		// ok, agora ja temos os meses, vamos colocar abaixo dos dias
		foreach($months as $key => $qtd){
			// serapa o mes do ano
			list($month, $year) = explode('-', $key);
			// posicao em y
			$y = $this->getAltura() - $espacoMes;
			// posicao inicial em x
			$x = $col * $this->getLarguraDia() + $this->getMargemEsquerda();
			// texto
			$texto = '';
			// largura dos dias do mes
			$largura = $qtd * $this->getLarguraDia();

			// espaco minimo para escrever o mes
			if( $largura >= 29 ){
				// colocamos a abreviacao
				$texto = strtoupper(strftime("%b", mktime(0,0,0,$month,1,$year)));
				// pega a largura do texto
				$width = imagefontwidth(2) * strlen($texto);
				// escreve o mes
				imagestring($this->img
					, 2
					, $x + ($largura/2) - ($width/2)
					, $y
					, $texto
					, $this->getColor('cinza')
				);

				// agora vamos para o ano
				$texto = strftime("%Y", mktime(0,0,0,$month,1,$year));
				// pega a largura do texto
				$width = imagefontwidth(2) * strlen($texto);
				// escreve o mes
				imagestring($this->img
					, 2
					, $x + ($largura/2) - ($width/2)
					, $y + 11
					, $texto
					, $this->getColor('cinza')
				);
			}

			// desenha a borda direita
			imageline($this->img
				, $x + $largura
				, $this->getAltura() - $this->getMargemInferior()
				, $x + $largura
				, $this->getAltura()
				, IMG_COLOR_STYLED
			);

			// incrementamos a coluna atual
			$col += $qtd;
		}

		///////////////////////////////////////////////////////////////////
		//////////////////// POR ULTIMO A GRADE ///////////////////////////
		///////////////////////////////////////////////////////////////////

		// vamos desenhas primeiro as linhas verticais
		for($col=0; $col <= $this->dias; $col++){
			$x = $this->getMargemEsquerda() + ($col * $this->getLarguraDia());
			$y = $this->getMargemSuperior();
			$altura = $this->getAltura() - $this->getMargemInferior();

			imageline(
				$this->img
				, $x
				, $y
				, $x
				, $altura
				, IMG_COLOR_STYLED
			);
		}

		// desenhando as linhas horizontais
		for($row = 0; $row <= count($this->posicoes); $row++) {
			$x = 0;
			$x2 = $this->getLargura() - $this->getMargemDireita();
			$y = $this->getMargemSuperior() + ($row * $this->getAlturaEtapa());

			imageline(
				$this->img
				, $x
				, $y
				, $x2
				, $y
				, IMG_COLOR_STYLED
			);
		}
	}

	/**
	 * Recupera uma etapa do processo
	 *
	 * Recupera pela posicao
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idx posicao da etapa
	 * @return GraficoProcessoEtapa
	 */
	protected function getEtapa($idx){
		return $this->etapas[$idx];
	}

	/**
	 * Converte hexadecimal para decimal
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $hexStr
	 * @return array
	 */
	protected function hex2rgb($hexStr)
	{
		$hexStr = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr); // retiramos o que nao for hexadecimal
	    $rgbArray = array();

	    // se tiver 6 caracteres
	    if (strlen($hexStr) == 6) {
	        $colorVal = hexdec($hexStr);
	        $rgbArray['red'] = 0xFF & ($colorVal >> 0x10);
	        $rgbArray['green'] = 0xFF & ($colorVal >> 0x8);
	        $rgbArray['blue'] = 0xFF & $colorVal;

	    // se tiver somente 3
	    } elseif (strlen($hexStr) == 3) {
	        $rgbArray['red'] = hexdec(str_repeat(substr($hexStr, 0, 1), 2));
	        $rgbArray['green'] = hexdec(str_repeat(substr($hexStr, 1, 1), 2));
	        $rgbArray['blue'] = hexdec(str_repeat(substr($hexStr, 2, 1), 2));

	    } else {
	        return false; //cor invalida
	    }
	    return $rgbArray;
	}
}

/**
 * Classe que representa uma etapa no processo
 *
 * Cada elemento de etapa compora o grafico
 * do cronograma.
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 */
class GraficoProcessoEtapa {
	/**
	 * data inicial da etapa
	 *
	 * @var string
	 */
	private $dataInicio;
	/**
	 * data final da etapa
	 *
	 * @var string
	 */
	private $dataFim;
	/**
	 * cor da etapa
	 *
	 * @var string
	 */
	private $cor;
	/**
	 * Nome da etapa
	 *
	 * @var string
	 */
	private $nome;
	/**
	 * tipo da etapa
	 *
	 * @var string
	 */
	private $tipo;

	/**
	 * Construtor
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $dataInicio
	 * @param string $dataFim
	 * @param string $cor
	 * @param string $nome
	 * @param string $tipo
	 * @return GraficoProcessoEtapa
	 */
	public function __construct($dataInicio, $dataFim, $cor, $nome, $tipo){
		$this->setCor($cor);
		$this->setDataInicio($dataInicio);
		$this->setDataFim($dataFim);
		$this->setNome($nome);

		// esse nao pode mudar
		$this->tipo = $tipo;
	}

	/**
	 * Recupera a data inicio da etapa
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return string
	 */
	public function getDataInicio()
	{
	    return $this->dataInicio;
	}

	/**
	 * Altera a data inicio da etapa
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $dataInicio
	 * @return void
	 */
	public function setDataInicio($dataInicio)
	{
	    $this->dataInicio = $dataInicio;
	}

	/**
	 * Recupera a data fim da etapa
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return string
	 */
	public function getDataFim()
	{
	    return $this->dataFim;
	}

	/**
	 * Altera a data final da etapa
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $dataFim
	 * @return void
	 */
	public function setDataFim($dataFim)
	{
	    $this->dataFim = $dataFim;
	}

	/**
	 * Retorna a cor da etapa
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return string
	 */
	public function getCor()
	{
	    return $this->cor;
	}
	/**
	 * Altera a cor da etapa
	 *
	 * Nao pode conter o '#'
	 * Formato Hexadecimal
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $cor
	 * @return void
	 */
	public function setCor($cor)
	{
	    $this->cor = $cor;
	}
	/**
	 * recupera o nome da etapa
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return string
	 */
	public function getNome()
	{
	    return utf8_decode($this->nome);
	}
	/**
	 * altera o nome da etapa
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $nome
	 * @return void
	 */
	public function setNome($nome)
	{
	    $this->nome = $nome;
	}
	/**
	 * retorna o tipo de grafico
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return string
	 */
	public function getTipo()
	{
	    return $this->tipo;
	}
}