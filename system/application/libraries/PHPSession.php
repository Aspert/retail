<?php

	
 	class PHPSession 
 	{
 		var $userdata;
 		
 		/**
 		 * Construtor da Classe de sess�o
 		 *
 		 * @return PHPSession
 		 */
 		function PHPSession()
 		{
 			$this->start();
 		}
 		
 		function start()
 		{
 			session_start();
 			
 			$this->userdata = $_SESSION;
 		}
 		
 		/**
 		 * Fun��o que elimina a sess�o
 		 * @return null
 		 */ 		
 		function end()
 		{
 			if(count($_SESSION)>0){	unset($_SESSION);} 			
 		}
 		 
 		/**
 		 * Fun��o que seta os dados na sess�o
 		 * @param Chave  - pode receber uma string ou um array ( definido que usaremos
 		 * apenas array de uma unica dimens�o, para n�o sobrecarregar a sess�o )
 		 *
 		 */
 		function set_userdata($chave=null,$valor=null)
 		 {		
 		 	try {
 		 		
 		 		if($chave=='')
 		 			throw new Exception('Valor n�o encontrado');
 		 			
	 		 	switch (gettype($chave)) {
	 		 		case "array":
						$vet = $chave; 	
	 		 			break;
	 		 	
	 		 		default:
	 		 			$vet[$chave] = $valor;  
	 		 			break;
	 		 	} 		 			
	 		 	
	 		 	if(!$this->check_session())
	 		 		throw new Exception('Sess�o n�o foi iniciada');
	 		 	
	 		 	foreach ($vet as $key=>$value) 
	 		 	{
	 		 		$_SESSION[$key] = $value;	 		 		
	 		 	}
	 		 	
	 		 	$this->userdata = $_SESSION;
 		 		
 		 	}
 		 	catch (Exception $e)
 		 	{
 		 		return $e->getMessage();
 		 	} 		 	
 		 	
 		 }
 		 
 		 /**
 		  * Fun��o que recebe a chave procurada como parametro, 
 		  * verificando se a mesma existe caso n�o exista retorna null
 		  *
 		  * @param string $chave
 		  * @return string
 		  */
 		 function userdata($chave)
 		 {
 		 	try {
 		 		if ($chave=="")
 		 			throw new Exception('Valor n�o encontrado');
 		 		
 		 		if(substr_count("[",$chave)>0)
 		 		{
 		 			foreach ($_SESSION as $key=>$value) 
 		 			{
 		 				if($_SESSION[$key]==$chave)
 		 				{
 		 					return $value;
 		 				}
 		 			} 		 			
 		 		}else{
 		 			if(isset($_SESSION[$chave]))
 		 				return $_SESSION[$chave];
 		 			return false;
 		 		}
 		 			
 		 	}
 		 	catch (Exception $e)
 		 	{
 		 		return $e->getMessage();
 		 	}
 		 	
 		 }
 		 
 		 function remove($chave)
 		 {
 		 	switch (gettype($chave)) {
 		 		case "array":
					$vet = $chave; 	
 		 			break;
 		 	
 		 		default:
 		 			$vet[$chave] = $chave;  
 		 			break;
 		 	} 		 			 		 	
 		 	
 		 	$this->userdata[$chave] = '';
 		 	$this->set_userdata[$chave];
 		 	
 		 }
 		 
 		 
 		 
 		 /**
 		  * Fun��o recursiva que verifica se existe uma chave em um array
 		  * @author 
 		  * @param unknown_type $needle
 		  * @param unknown_type $haystack
 		  * @return unknown
 		  * @package http://br.php.net/function.array-key-exists
 		  */
		 function array_key_exists_r($needle, $haystack)
		 {
		    $result = array_key_exists($needle, $haystack);
		    if ($result)
		        return $result;
		    foreach ($haystack as $v)
		    {
		        if (is_array($v) || is_object($v))
		            $result = array_key_exists_r($needle, $v);
		        if ($result)
		        return $result;
		    }
		    return $result;
		 } 		 
 		 
 		 function check_session()
 		 {
 		 	return (!isset($_SESSION))?false:true; 		 	
 		 }
 		 
 	}
 	
 	
?>