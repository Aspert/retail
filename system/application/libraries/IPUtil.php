<?php
/**
 * Classe utilitaria para validar IPs
 * 
 * @author juliano.polito
 */
class IPUtil{
	
	//Pattern usado hoje pelos clientes 247
	//e pelo localhost. Alterar o pattern de acordo com alteracoes
	//da estrutura de rede
	//Hoje o pattern bate tudo que comeca com 172.247.x.x(ips externos) ou 127.0.0.1(localhost)
	public static $ip247Pattern = "/^(172\.247|127)/";
	
	public function __construct(){
		
	}
	
	/**
	 * Verifica se o ip atraves do qual o usuario esta acessando o sistema 
	 * pertence a 247 (UTILIZA REMOTE_ADDR)
	 * Lanca uma excecao caso de erro na comparacao 
	 * @author juliano.polito
	 * @param ip IP que sera verifica contra o pattern da 247
	 * @return bool true caso seja da 247, false caso não seja
	 */
	public static function is247($ip = ''){
		//caso ip seja vazio usa o endereco atual
		if($ip === ''){
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		
		//faz a comparacao contra o pattern
		$match = preg_match(self::$ip247Pattern,$_SERVER['REMOTE_ADDR']);
		
		//retorno false indica erro no parse
		if($match === false){
			throw new Exception('Nao foi possivel fazer a comparacao.');
		}
		
		//preg_match retorna 0 ou 1
		if($match === 0){
			//0 em caso nao bater pattern
			return false;
		}else if ($match === 1){
			//1 em caso de bater pattern
			return true;
		}
		
	}
	
	
	
	
}
?>