<?php
class ApplicationContext
{
	function ApplicationContext()
	{
		$CI =& get_instance();
		
		// Setando charset default
		ini_set('default_charset', $CI->config->item('charset'));
		
		// Iniciando a vari�vel data 
		$CI->data = null;
		
		// Carregando a Model do Codeigniter e a GenericModel
		$CI->load->library('Model');
		$CI->load->library('GenericModel');
		
		// carregando as models da pasta model (models referente as tabelas do banco de dados)
		$dh  = opendir(APPPATH . 'models/');
		while (false !== ( $file = readdir( $dh ) ))
		{
			if ( !in_array($file, array('.', '..', 'CVS', 'index.html')) && preg_match("/((.*)DB)\.php$/", $file, $match) )
			{
				$modelName = $match[1];
				$modelInstance = strtolower(substr($match[2], 0, 1)) . substr($match[2], 1, strlen($match[2]) - 1);
				
				$CI->load->model($modelName, $modelInstance);
			}
		}
	}
}
?>