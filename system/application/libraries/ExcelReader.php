<?php

/**
 * Classe para leitura de arquivos XLSX
 * 
 * Esta classe foi criada para poder agilizar a leitura de arquivos
 * XLSX. 
 * 
 * Ela ignora completamente a parte de formatacao do arquivo e pega 
 * somente o conteudo. Nas celulas onde contem funcoes, ela ignora as funcoes
 * e pega somente o valor contido na parte de valores ja calculados (o XLSX guarda a formula
 * e o valor calculado).
 *
 * @todo Ler datas corretamente
 * 
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 *
 */
class ExcelReader {
	
	/**
	 * Zip Handler para manipular o arquivo Excel
	 * @var ZipArchive
	 */
	private $zip = null;
	
	/**
	 * Nome do arquivo que sera analisado
	 * @var string
	 */
	private $file;
	
	/**
	 * Planilhas do arquivo
	 * @var array
	 */
	private $sheets = array();
	
	/**
	 * Dados das planilhas
	 * @var array
	 */
	private $sheetData = array();
	
	/**
	 * Lista de strings utilizadas
	 * @var array
	 */
	private $sharedStrings = array();
	/**
	 * Armazena as colunas maximas das planilhas
	 * @var array
	 */
	private $maxCol = array();
	/**
	 * Armazena as linhas maximas das planilhas
	 * @var array
	 */
	private $maxRow = array();
	
	/**
	 * Construtor privado para trabalharmos com singleton
	 * @author Hugo Ferreira da Silva
	 * @return ExceleEader
	 */
	public function __construct($pFile=''){
		if(!empty($pFile)){
			$this->setFile($pFile);
		}
	}

	/**
	 * Altera o arquivo que ser usado
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param string $pValue Nome do arquivo
	 * @return void
	 * @throws ExcelReaderException
	 */
	public function setFile($pValue){
		$this->verifyXLSX($pValue);
		$this->file = $pValue;
	}
	
	/**
	 * Recupera o arquivo que esta sendo usado
	 * @author Hugo Ferreira da Silva
	 * @return string Arquivo sendo usado
	 */
	public function getFile(){
		return $this->file;
	}
	
	/**
	 * Abre o arquivo para iniciar o processamento
	 *  
	 * @author Hugo Ferreira da Silva
	 * @param string $pFile Nome do arquivo
	 * @return void
	 * @throws ExcelReaderExcpetion
	 */
	public function open($pFile = ''){
		if( $pFile != '' ){
			$this->setFile($pFile);
		}
		
		$this->zip = new ZipArchive();
		$this->zip->open($this->getFile());
		
		$this->parseWorkbook();
		$this->parseSharedStrings();
	}
	
	/**
	 * Recupera os nomes das planilhas existentes no arquivo de excel
	 * @author Hugo Ferreira da Silva
	 * @return array Nomes das planilhas em UTF-8
	 */
	public function getSheetNames(){
		return $this->sheets;
	}
	
	/**
	 * Recupera o numero de planilhas de uma pasta de trabalho
	 * @author Hugo Ferreira da Silva
	 * @return int
	 */
	public function getSheetCount(){
		return count($this->sheets);
	}
	
	/**
	 * Recupera o nome de uma planilha pelo indice
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $idx
	 * @return string
	 */
	public function getSheetName($idx){
		return $this->sheets[$idx];
	}
	
	/**
	 * Recupera a maior coluna de uma planilha
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $pSheetIndex Indice da planilha
	 * @return int Numero da maior coluna
	 */
	public function getMaxCol($pSheetIndex){
		if(!isset($this->maxCol[$pSheetIndex])){
			throw new ExcelReaderException('Indice nao encontrado: '.$pSheetIndex.' (Voce ja recuperou os dados da planilha?)',0,$this);
		}
		return $this->maxCol[$pSheetIndex];
	}
	
	/**
	 * Recupera a maior linha de uma planilha
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $pSheetIndex Indice da planilha
	 * @return int Numero da maior linha 
	 */
	public function getMaxRow($pSheetIndex){
		if(!isset($this->maxRow[$pSheetIndex])){
			throw new ExcelReaderException('Indice nao encontrado: '.$pSheetIndex.' (Voce ja recuperou os dados da planilha?)',0,$this);
		}
		return $this->maxRow[$pSheetIndex];
	}
	
	/**
	 * Recupera o indice da planilha pelo nome
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param string $pSheet Nome da planilha desejada
	 * @return int Indice da planilha
	 */
	public function getSheetIndex($pSheet){
		foreach($this->sheets as $idx => $name){
			if( $name == $pSheet ){
				return $idx;
			}
		}
		throw new ExcelReader('Nenhuma planilha encontrada com o nome "'.$pSheet.'"', ExcelReaderException::NO_SUCH_SHEET, $this);
	}
	
	
	/**
	 * Recupera os dados de uma planilha
	 * 
	 * Se a planilha nao teve seus dados recuperados,
	 * entao vamos recupera-los. Do contrario, somente
	 * devolvemos os dados previamente carregados.
	 * 
	 * Se o indice informado nao existe, dispara uma excecao
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $pSheetIndex Indice da planilha
	 * @return array Dados da planilha
	 * @throws ExcelReaderException
	 */
	public function getSheetData($pSheetIndex){
		
		if( !isset($this->sheets[$pSheetIndex]) ){
			throw new ExcelReaderException('Indice de planilha inexistente: '.$pSheetIndex, ExcelReaderException::NO_SUCH_SHEET, $this);
		}
		
		if( !isset($this->sheetData[$pSheetIndex]) ){
			$this->sheetData[$pSheetIndex] = array();
			$sheet = &$this->sheetData[$pSheetIndex];
			
			$sheetXML = 'xl/worksheets/sheet'.($pSheetIndex+1).'.xml';
			$xml = simplexml_load_string( $this->getData($sheetXML) );
			
			// pegando a dimensao da planilha
			$dimension = (string)$xml->dimension->attributes()->ref;
			
			@list($begin, $end) = explode(':', $dimension);
			@list($startCol, $statRow) = $this->coordToColumnRow($begin);
			@list($endCol, $endRow) = $this->coordToColumnRow($end);
			
			$this->maxCol[$pSheetIndex] = $endCol;
			$this->maxRow[$pSheetIndex] = $endRow;
			
			foreach($xml->sheetData->row as $row){
				foreach($row->c as $col){
					if( (string) $col->attributes()->t == 's'){
						if( (string)$col->v != '' ){
							$sheet[(string)$col->attributes()->r] = (string)$this->sharedStrings[ (int)$col->v ];
						} else {
							$sheet[(string)$col->attributes()->r] = '';
						}
					} else {
						$sheet[(string)$col->attributes()->r] = (string)$col->v;
					}
				}
			}
		}
		
		return $this->sheetData[$pSheetIndex];
	}
	
	/**
	 * Retorna o valor de uma celula especifica
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $pSheetIndex Indice da planilha
	 * @param string $pCell Coordenada da celula (A1, B612)
	 * @return mixed Valor da cleula
	 */
	public function getCellData($pSheetIndex, $pCell){
		if( isset($this->sheetData[$pSheetIndex][$pCell]) ){
			return $this->sheetData[$pSheetIndex][$pCell];
		}
		
		return null;
	}
	
	/**
	 * Recupera o valor de uma celula pelo indice da coluna e numero da linha
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $pSheetIndex Indice da planilha
	 * @param int $pColumn Indice da coluna
	 * @param int $pRow Numero da linha
	 * @return mixed
	 */
	public function getCellDataByColumnRow($pSheetIndex, $pColumn, $pRow){
		$coord = $this->columnRowToCoord($pColumn, $pRow);
		return $this->getCellData($pSheetIndex, $coord);
	}
	
	/**
	 * Libera os dados de uma determinada planilha
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $pSheetIndex Indice da planilha
	 * @return void
	 */
	public function disposeSheetData($pSheetIndex){
		unset($this->sheetData[$pSheetIndex]);
		unset($this->maxRow[$pSheetIndex]);
		unset($this->maxCol[$pSheetIndex]);
	}
	
	/**
	 * Fecha o arquivo ZIP
	 * 
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	public function close(){
		$this->zip->close();
	}
	
	/**
	 * Recupera as informacoes importantes da pasta de trabalho
	 * 
	 * Por enquanto, somente recuperamos os nomes das planilhas
	 * que existem no excel.
	 * 
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	protected function parseWorkbook(){
		$xml = simplexml_load_string( $this->getData('xl/workbook.xml') );
		
		$i=0;
		foreach($xml->sheets->sheet as $sheet){
			$this->sheets[ $i++ ] = (string)$sheet->attributes()->name;
		}
	}
	
	/**
	 * Faz a analise do arquivo de strings
	 * 
	 * No Excel, as strings sao armazenadas em um XML diferente, 
	 * para que elas possam ser reutilizadas em outras planilhas.
	 * 
	 * As planilhas fazem referencia a este XML atraves do indice
	 * da string.
	 * 
	 * @author Hugo Ferreira da Silva
	 * @return void
	 */
	protected function parseSharedStrings(){
		$xml = simplexml_load_string( $this->getData('xl/sharedStrings.xml') );
		
		$count = 0;
		foreach($xml->si as $item){
			$this->sharedStrings[$count++] = (string)$item->t;
		}
	}
	
	
	
	/**
	 * Certifica que o arquivo que esta sendo usado e um XLSX valido
	 * 
	 * Primeiramente tentamos abrir como um ZIP.
	 * Todo arquivo XLSX e um arquivo ZIP.
	 * 
	 * Se conseguirmos abrir, procuramos os arquivos de definicao de pasta de trabalho (workbook)
	 * e o arquivo de strings compartilhadas (sharedStrings).
	 * 
	 * Qualquer problema encontrado, uma excecao e disparada
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param string $filename Nome do arquivo
	 * @return void
	 * @throws ExcelReaderException
	 */
	protected function verifyXLSX($pFilename){
		$zip = new ZipArchive();
		
		if( $zip->open($pFilename) === true ){
			if( $zip->locateName('xl/workbook.xml') === false ){
				$zip->close();
				throw new ExcelReaderException('A definicao de workbook nao foi encontrado no arquivo '.$pFilename, ExcelReaderException::WORKBOOK_ERROR, $this);
			}
			
			if( $zip->locateName('xl/sharedStrings.xml') === false ){
				$zip->close();
				throw new ExcelReaderException('O arquivo de strings nao foi encontrado no arquivo '.$pFilename, ExcelReaderException::SI_ERROR, $this);
			}
			
			$zip->close();
			
		} else {
			throw new ExcelReaderException('Nao foi possivel abrir como zip o arquivo '.$pFilename, ExcelReaderException::ZIP_ERROR, $this);
		}
	}
	
	/**
	 * Transforma a coordenada em indice numerico (coluna e linha)
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param string $pCoord Coordenada da celula (A1, B4)
	 * @return array Indice da coluna e numero da linha
	 */
	protected function coordToColumnRow($pCoord){
		$col = 0;
		$row = 0;
		
		if( preg_match('@^([A-Z]+)([0-9]+)$@',$pCoord,$reg) ){
			$col = self::columnIndexFromString($reg[1]);
			$row = $reg[2];
		}
		
		return array($col, $row);
	}
	
	/**
	 * Recupera o indice da coluna e linha em coordenada do excel (A1, B4)
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $pCol Indice da coluna
	 * @param int $pRow Numero da linha
	 * @return string Coordenada da celula
	 */
	protected function columnRowToCoord($pCol, $pRow){
		$colStr = self::stringFromColumnIndex($pCol);
	   	return $colStr . $pRow;
	}
	
	/**
	 * Recupera o indice da coluna
	 * 
	 * @author CodePlex / PHPExcel
	 * @param string $pString Nome da coluna (A, C, Z)
	 * @return int indice da coluna 
	 */
	public static function columnIndexFromString($pString = 'A') {
		$pString = strtoupper($pString);

		$strLen = strlen($pString);
		if ($strLen == 1) {
			return (ord($pString{0}) - 65);
		} elseif ($strLen == 2) {
			return $result = ((1 + (ord($pString{0}) - 65)) * 26) + (ord($pString{1}) - 65);
		} elseif ($strLen == 3) {
			return ((1 + (ord($pString{0}) - 65)) * 676) + ((1 + (ord($pString{1}) - 65)) * 26) + (ord($pString{2}) - 65);
		} else {
			throw new Exception("Column string index can not be " . ($strLen != 0 ? "longer than 3 characters" : "empty") . ".");
		}
	}

	/**
	 * Recupera o nome da coluna pelo indice
	 * 
	 * @author CodePlex / PHPExcel
	 * @param int $pColumnIndex Indice da coluna
	 * @return string Representacao em string da coluna
	 */
	public static function stringFromColumnIndex($pColumnIndex = 0)
	{
		// Determine column string
		if ($pColumnIndex < 26) {
			return chr(65 + $pColumnIndex);
		}
	   	return self::stringFromColumnIndex((int)($pColumnIndex / 26) -1).chr(65 + $pColumnIndex%26) ;
	}
	
	/**
	 * Recupera os dados do ZIP a partir de uma determinada URI
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param string $uri Endereco do arquivo que se deseja abrir
	 * @return string Conteudo do arquivo
	 * @throws ExcelReaderException
	 */
	protected function getData($uri){
		$index = $this->zip->locateName( $uri );
		
		if( $index === false ){
			throw new ExcelReaderException('URI nao encontrada: '.$uri, ExcelReaderException::BAD_URI, $this);
		}
		
		$data = $this->zip->getFromIndex( $index );
		
		return $data;
	}
	
}

/**
 * Classe de excecao para ser utilizada no ExcelReader
 * 
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 *
 */
class ExcelReaderException extends Exception {
	const ZIP_ERROR           = 0x1;
	const WORKBOOK_ERROR      = 0x2;
	const SI_ERROR            = 0x3;
	const NO_SUCH_SHEET       = 0x4;
	const SHEET_XML_NOT_FOUND = 0x5;
	const BAD_URI             = 0x6;
	
	/**
	 * Objeto de leitura de excel
	 * @var ExcelReader
	 */
	public $reader;
	
	/**
	 * Construtor
	 * @author Hugo Ferreira da Silva
	 * @param string      $pMsg    Mensagem da excecao 
	 * @param int         $pCode   Codigo da excecao
	 * @param ExcelReader $pReader Objeto de leitura
	 * @return ExcelReaderException
	 */
	public function __construct($pMsg, $pCode, ExcelReader $pReader){
		$this->reader = $pReader;
		parent::__construct($pMsg, $pCode);
	}
}


