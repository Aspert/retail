<?php
/**
 * Classe para representar uma planilha de Excel
 * 
 * Principalmente usada na importacao de planilhas.
 * Como podem haver varios tipos de arquivos a serem importados,
 * foi padronizado que todos os os adaptadores de importacao
 * retornarao dois objetos, RetailWorkbook e RetailSheet.
 * 
 * Esta classe serve somente para armazenamento e 
 * recuperacao dos dados de uma planilha de forma mais 
 * organizada. Ela nao tem operacoes avancadas de 
 * tratamento de arquivos de Excel
 * 
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 * @package libraries
 *
 */


class RetailSheet {
	/**
	 * Linha maxima preenchida
	 * @var int
	 */
	private $maxRow;
	/**
	 * Coluna maxima preenchida
	 * @var int
	 */
	private $maxColumn;
	/**
	 * Nome da planilha
	 * @var string
	 */
	private $sheetName;
	/**
	 * Dados da planilha
	 * 
	 * Os dados sao preenchidos com o metodo setCellData
	 * 
	 * @see RetailSheet::setCellData
	 * @var array
	 */
	private $data = array();
	
	/**
	 * Construtor
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param string $sheetName Nome da planilha
	 * @return RetailSheet
	 */
	public function __construct($sheetName){
		$this->setSheetName( $sheetName );
	}
	
	/**
	 * Altera o nome da planilha
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param string $sheetName Nome da planilha
	 * @return void
	 */
	public function setSheetName($sheetName){
		$this->sheetName = $sheetName;
	}
	
	/**
	 * Retorna o nome da planilha
	 * 
	 * @author Hugo Ferreira da Silva
	 * @return string Nome da planilha
	 */
	public function getSheetName(){
		return $this->sheetName;
	}
	
	/**
	 * Altera a maior linha com dados preenchidos
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $maxRow maior linha com dados preenchidos
	 * @return void
	 */
	public function setMaxRow( $maxRow ){
		$this->maxRow = $maxRow;
	}
	
	/**
	 * Recupera a maior linha com dados preenchidos
	 * @author Hugo Ferreira da Silva
	 * @return int maior linha com dados preenchidos
	 */
	public function getMaxRow(){
		return $this->maxRow;
	}
	
	/**
	 * Altera a maior coluna com valor preenchido
	 * @author Hugo Ferreira da Silva
	 * @param int $maxColumn maior coluna com valor preenchido
	 * @return void
	 */
	public function setMaxColumn( $maxColumn ){
		$this->maxColumn = $maxColumn;
	}
	
	/**
	 * Maior coluna com valor preenchido
	 * @author Hugo Ferreira da Silva
	 * @return int maior coluna com valor preenchido
	 */
	public function getMaxColumn(){
		return $this->maxColumn;
	}
	
	/**
	 * Altera a o valor de uma celula
	 * 
	 * O valor da celula deve sempre estar associado
	 * a coordenada da mesma, exemplo:
	 * 
	 * <code>
	 * $sheet = new RetailSheet('planilha');
	 * $sheet->setCellData('A1', 'aqui vai o valor');
	 * </code>
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param string $cellCoord Coordenada da celula
	 * @param mixed $value Valor da celula
	 * @return void
	 */
	public function setCellData($cellCoord, $value){
		$this->data[ $cellCoord ] = $value;
	}
	
	/**
	 * Recupera o valor de uma celula
	 * 
	 * Para recuperar o valor da celula, eh necessario informar
	 * a coordenada da mesma, exemplo:
	 * 
	 * <code>
	 * $sheet = $workbook->getSheetByName('planilha');
	 * $sheet->getCellData('A1');
	 * </code>
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param string $cellCoord Coordenada da celula
	 * @return mixed valor da celula
	 */
	public function getCellData($cellCoord){
		$value = null;
		if( array_key_exists($cellCoord, $this->data) ){
			$value = $this->data[ $cellCoord ];
		}
		
		return $value;
	}
	
	/**
	 * Recupera o valor de uma celula pela linha e coluna
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param int $pColumn Indice da coluna (a partir de 0)
	 * @param int $pRow Numero da linha (a partir de 1)
	 * @return mixed valor da celula
	 */
	public function getCellDataByColumnRow($pColumn, $pRow){
		$coord = self::stringFromColumnIndex($pColumn) . $pRow;
		return $this->getCellData($coord);
	}
	
	/**
	 * Recupera o indice da coluna
	 * 
	 * @author CodePlex / PHPExcel
	 * @param string $pString Nome da coluna (A, C, Z)
	 * @return int indice da coluna 
	 */
	public static function columnIndexFromString($pString = 'A') {
		$pString = strtoupper($pString);

		$strLen = strlen($pString);
		if ($strLen == 1) {
			return (ord($pString{0}) - 65);
		} elseif ($strLen == 2) {
			return $result = ((1 + (ord($pString{0}) - 65)) * 26) + (ord($pString{1}) - 65);
		} elseif ($strLen == 3) {
			return ((1 + (ord($pString{0}) - 65)) * 676) + ((1 + (ord($pString{1}) - 65)) * 26) + (ord($pString{2}) - 65);
		} else {
			throw new Exception("Column string index can not be " . ($strLen != 0 ? "longer than 3 characters" : "empty") . ".");
		}
	}

	/**
	 * Recupera o nome da coluna pelo indice
	 * 
	 * @author CodePlex / PHPExcel
	 * @param int $pColumnIndex Indice da coluna
	 * @return string Representacao em string da coluna
	 */
	public static function stringFromColumnIndex($pColumnIndex = 0)
	{
		// Determine column string
		if ($pColumnIndex < 26) {
			return chr(65 + $pColumnIndex);
		}
	   	return self::stringFromColumnIndex((int)($pColumnIndex / 26) -1).chr(65 + $pColumnIndex%26) ;
	}
	
}