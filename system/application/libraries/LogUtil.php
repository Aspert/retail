<?php
class LogUtil{

	public static function log($filepath, $msg){
		$msg = date('d/m/Y H:i:s - ') . $msg . PHP_EOL;
		file_put_contents($filepath, $msg, FILE_APPEND);
	}

}
?>