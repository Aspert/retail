<?php
class XmlUtil{
	
	/**
	 * Retorna um objeto xml a partir de um array
	 * @param $arr array contendo os dados
	 * @param $utf8 boolean default true
	 * @param $first
	 * @return String
	 */
	public static function array2xml( $arr, $utf8 = true, $first = true ){
		$xml = array();
	    $stack = array();
	
	    foreach( $arr as $key => $item ){
	        if( is_numeric($key) ){
	            $key = 'item';
	        }
	        
	        $xml[] = sprintf('<%s>', $key);
	        
	        if( is_array($item) ) {
                $str = self::array2xml($item, $utf8, false);
	        } else {
	            $str = sprintf('<![CDATA[%s]]>', $utf8 ? utf8_encode($item) : $item);
	        }
	        
	        $xml[] = $str;
	        $xml[] = sprintf('</%s>', $key);
	    }
	    
	    if( $first ) {
	        return '<data>' . implode(PHP_EOL, $xml) . '</data>';
	    } else {
	        return implode(PHP_EOL, $xml);
	    }
	}
}
?>