<?php
class MY_Loader extends CI_Loader
{
	function MY_Loader()
	{
		parent::CI_Loader();
	}
	
	function xml($model)
	{
		$CI =& get_instance();
		
		$CI->load->library('XmlBuilder');
		
		$CI->xmlbuilder->getSource($model);
	}
	
	/**
	 * Override do metodo model() da classe CI_Loader  
	 */
	function model($model, $name = '', $db_conn = FALSE)
	{
		if (is_array($model))
		{
			foreach($model as $babe)
			{
				$this->model($babe);	
			}
			return;
		}

		if ($model == '')
		{
			return;
		}
	
		// Is the model in a sub-folder? If so, parse out the filename and path.
		if (strpos($model, '/') === FALSE)
		{
			$path = '';
		}
		else
		{
			$x = explode('/', $model);
			$model = end($x);			
			unset($x[count($x)-1]);
			$path = implode('/', $x).'/';
		}
	
		if ($name == '')
		{
			$name = $model;
		}
		
		if (in_array($name, $this->_ci_models, TRUE))
		{
			return;
		}
		
		$CI =& get_instance();
		if (isset($CI->$name))
		{
			//show_error('The model name you are loading is the name of a resource that is already being used: '.$name);
		}
		
		/**
		 * Essa linha foi comentada para a model que for carregada dentro 
		 * da pasta APPPATH . 'models/' manter a formatação
		 */
		//$model = strtolower($model);
	
		if ( ! file_exists(APPPATH.'models/'.$path.$model.EXT))
		{
			show_error('Unable to locate the model you have specified: '.$model);
		}
				
		if ($db_conn !== FALSE AND ! class_exists('CI_DB'))
		{
			if ($db_conn === TRUE)
				$db_conn = '';
		
			$CI->load->database($db_conn, FALSE, TRUE);
		}
	
		if ( ! class_exists('Model'))
		{
			load_class('Model', FALSE);
		}

		require_once(APPPATH.'models/'.$path.$model.EXT);

		$model = ucfirst($model);
				
		$CI->$name = new $model();
		$CI->$name->_assign_libraries();
		
		$this->_ci_models[] = $name;
	}
}
