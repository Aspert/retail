<?php

require_once("r_script.php");

class IDS {

	/**
	 * Chama um script de indesign 
	 * 
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $script
	 * @param array $argumentos
	 * @param boolean $debug
	 * @return array 
	 */
	public function call($script, $argumentos, $debug=false){
		$ci = &get_instance();
		
		//Enderecos do server
		$ids_ip = $ci->config->item("indesign_ip");
		$ids_port = ":".$ci->config->item("indesign_port");
		$ids_uri = $ci->config->item("indesign_uri");
		
		//pasta dos scripts
		$ids_scriptFolder = $ci->config->item("indesign_scripts");
		
		//recupera o retorno da execucao do script 
		$scriptResult = phpRunScript($ids_uri, $ids_ip.$ids_port, $ids_scriptFolder . $script . '.jsx', null, 'javascript', $argumentos, $debug);
		
		return $scriptResult;
	}

	/**
	 * Agenda um processo para o IDS kiwi
	 * 
	 * @author Hugo Ferreira da Silva
	 * @param string $sistema Nome identificador do sistema
	 * @param string $script Nome do script InDesign sem extensao
	 * @param array $argumentos Lista de argumentos para o script
	 * @param string $urlCallback URL para callback do IDS Kiwi
	 * @return int numero do processo gerado
	 */
	public function kiwi($sistema, $script, $argumentos, $urlCallback){
		$opt = array();
		$opt['location'] = IDS_KIWI_ENDPOINT;
		$opt['uri'] = IDS_KIWI_URI;
		
		$origem = $_SERVER['REQUEST_URI'];
		
		$ci = &get_instance();
		
		// caminho dos scripts
		$idsPath = $ci->config->item('indesign_scripts');
		$argumentos[] = array('name' => 'IDS_PATH_SCRIPTS', 'value' => $idsPath);
		
		$client = new SoapClient(null, $opt);
		$pid = $client->register($sistema, $script, $argumentos, $origem, $urlCallback);
		
		return $pid;
	}
}
