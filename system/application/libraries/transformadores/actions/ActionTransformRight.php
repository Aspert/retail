<?php


require_once dirname(__FILE__) . '/AbstractActionTransform.php';

class ActionTransformRight extends AbstractActionTransform {

	public function __construct(){
		$this->name = 'Right';
		$this->description = 'Retorna o numero indicado de caracteres à direita';
	}

	/**
	 * Executa uma acao em um transformador
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $value Valor a ser transformado
	 * @param array $args Argumentos a serem aplicados na chamada
	 * @return string valor transformado
	 */
	public function execute($value){
		list( $count ) = $this->parameters;
		return substr($value, 0, - $count);
	}

	/**
	 * Retorna o numero de argumentos que este transformador precisa
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return int numero de argumentos necessarios
	 */
	public function getParamCount(){
		return 1;
	}

}