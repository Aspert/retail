<?php

/**
 * Interface para acoes que serao executadas por transformadores
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 */
interface IActionTransform {

	/**
	 * Recupera o nome da acao
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return string nome da acao
	 */
	function getName();

	/**
	 * Recupera a descricao da acao
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return string
	 */
	function getDescription();

	/**
	 * Executa uma acao em um transformador
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $value Valor a ser transformado
	 * @return string valor transformado
	 */
	function execute($value);

	/**
	 * Retorna o numero de argumentos que este transformador precisa
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return int numero de argumentos necessarios
	 */
	function getParamCount();

	/**
	 * Altera os parametros que serao usados
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $parameters
	 * @return void
	 */
	function setParameters(array $parameters);

}