<?php


require_once dirname(__FILE__) . '/AbstractActionTransform.php';

class ActionTransformLeft extends AbstractActionTransform {

	public function __construct(){
		$this->name = 'Left';
		$this->description = 'Retorna o numero informado de caracters à esquerda';
	}

	/**
	 * Executa uma acao em um transformador
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $value Valor a ser transformado
	 * @param array $args Argumentos a serem aplicados na chamada
	 * @return string valor transformado
	 */
	public function execute($value){
		list( $count ) = $this->parameters;
		return substr($value, 0, $count);
	}

	/**
	 * Retorna o numero de argumentos que este transformador precisa
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return int numero de argumentos necessarios
	 */
	public function getParamCount(){
		return 1;
	}

}