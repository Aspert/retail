<?php


require_once dirname(__FILE__) . '/AbstractActionTransform.php';

class ActionTransformUppercase extends AbstractActionTransform {

	public function __construct(){
		$this->name = 'Uppercase';
		$this->description = 'Transforma os valores em maiúsculo';
	}

	/**
	 * Executa uma acao em um transformador
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $value Valor a ser transformado
	 * @param array $args Argumentos a serem aplicados na chamada
	 * @return string valor transformado
	 */
	public function execute($value){
		return mb_strtoupper($value);
	}

	/**
	 * Retorna o numero de argumentos que este transformador precisa
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return int numero de argumentos necessarios
	 */
	public function getParamCount(){
		return 0;
	}

}