<?php


require_once dirname(__FILE__) . '/AbstractActionTransform.php';

class ActionTransformPadright extends AbstractActionTransform {

	public function __construct(){
		$this->name = 'Pad Right';
		$this->description = 'Adiciona caracteres à direita do valor.
			O primeiro parâmetro é a string a ser adicionada.
			O segundo é o número de casas a serem adicionadas.';
	}

	/**
	 * Executa uma acao em um transformador
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $value Valor a ser transformado
	 * @param array $args Argumentos a serem aplicados na chamada
	 * @return string valor transformado
	 */
	public function execute($value){
		list($str, $qtd) = $this->parameters;
		return str_pad($value, $qtd, $str, STR_PAD_RIGHT);
	}

	/**
	 * Retorna o numero de argumentos que este transformador precisa
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return int numero de argumentos necessarios
	 */
	public function getParamCount(){
		return 2;
	}

}