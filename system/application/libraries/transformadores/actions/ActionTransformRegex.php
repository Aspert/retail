<?php


require_once dirname(__FILE__) . '/AbstractActionTransform.php';

class ActionTransformRegex extends AbstractActionTransform {

	public function __construct(){
		$this->name = 'Regex';
		$this->description = 'Aplica uma expressão regular ao valor informado.
		O primeiro parâmetro é a RegEx a ser aplicada.
		O segundo é RegEx de substituição.';
	}

	/**
	 * Executa uma acao em um transformador
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $value Valor a ser transformado
	 * @param array $args Argumentos a serem aplicados na chamada
	 * @return string valor transformado
	 */
	public function execute($value){
		list( $exp, $repl ) = $this->parameters;

		$value = preg_replace($exp, $repl, $value);

		return $value;
	}

	/**
	 * Retorna o numero de argumentos que este transformador precisa
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return int numero de argumentos necessarios
	 */
	public function getParamCount(){
		return 2;
	}

}