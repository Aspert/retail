<?php


require_once dirname(__FILE__) . '/AbstractActionTransform.php';

class ActionTransformSubstring extends AbstractActionTransform {

	public function __construct(){
		$this->name = 'Substring';
		$this->description = 'Retorna uma parte de uma string';
	}

	/**
	 * Executa uma acao em um transformador
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $value Valor a ser transformado
	 * @param array $args Argumentos a serem aplicados na chamada
	 * @return string valor transformado
	 */
	public function execute($value){
		list( $start, $length ) = $this->parameters;
		return substr($value, $start, $length);
	}

	/**
	 * Retorna o numero de argumentos que este transformador precisa
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return int numero de argumentos necessarios
	 */
	public function getParamCount(){
		return 2;
	}

}