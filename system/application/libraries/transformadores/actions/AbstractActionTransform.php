<?php

require_once dirname(__FILE__) . '/IActionTransform.php';

abstract class AbstractActionTransform implements IActionTransform {

	protected $name = 'Abstract';
	protected $description = 'Descricao';
	protected $parameters = array();

	/**
	 * Recupera o nome da acao
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return string nome da acao
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * Recupera a descricao da acao
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return string
	 */
	public function getDescription(){
		return $this->description;
	}

	/**
	 * Executa uma acao em um transformador
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $value Valor a ser transformado
	 * @param array $args Argumentos a serem aplicados na chamada
	 * @return string valor transformado
	 */
	public function execute($value){
		throw new Exception('Must be implemented');
	}

	/**
	 * Retorna o numero de argumentos que este transformador precisa
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @return int numero de argumentos necessarios
	 */
	public function getParamCount(){
		throw new Exception('Must be implemented');
	}

	/**
	 * Altera os parametros que serao usados
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $parameters
	 * @return void
	 */
	public function setParameters(array $parameters){

		if( count($parameters) != $this->getParamCount() ){
			throw new Exception(
				sprintf('Numero de parametros incorreto. Esperado %d, e voce informou %d'
					, $this->getParamCount()
					, count($parameters)
				)
			);
		}

		$this->parameters = $parameters;
	}

}