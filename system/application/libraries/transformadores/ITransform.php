<?php

/**
 * Interface para implementacao de transformadores
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 */
interface ITransform {

	/**
	 * Executa o transformador
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param mixed $data Dados recebidos para tratamento pelo transformador
	 * @return mixed Mesma estrutura de $data, porem com os valores transformados
	 */
	function execute( $data );

	/**
	 * Adiciona uma acao para ser executada
	 *
	 * Toda acao devera ser vinculada a um campo
	 * ou coluna especifica, por isso o primeiro
	 * argumento e para identificacao deste campo.
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $fieldname nome do campo
	 * @param IActionTransform $action Acao a ser executada
	 * @param array $parameters Parametros para serem usados com a acao
	 * @return mixed valor transformado
	 */
	function addAction($fieldname, IActionTransform $action, array $parameters = array());

	/**
	 * Remove um transformador de um campo
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $fieldname Nome do campo
	 * @param IActionTransform $action
	 * @return void
	 */
	function removeAction($fieldname, IActionTransform $action);
}