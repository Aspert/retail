<?php

require_once dirname(__FILE__) . '/actions/IActionTransform.php';
require_once dirname(__FILE__) . '/ITransform.php';

/**
 * Interface para implementacao de transformadores
 *
 * @author Hugo Ferreira da Silva
 * @link http://www.247id.com.br
 */
class ExcelTransform implements ITransform {

	protected $actions = array();

	// mapeamento de colunas
	protected $map = array(
			'ORDEM'         => 'A',
			'PAGINA'        => 'B',
			'SUBCATEGORIA'  => 'C',
			'CODIGO_BARRAS' => 'D',
			'DESCRICAO'     => 'E',
			'TEXTO_LEGAL'   => 'F',
			'PRECO_UN'      => 'G',
			'TIPO_UN'       => 'H',
			'PRECO_CX'      => 'I',
			'TIPO_CX'       => 'J',
			'PRECO_VISTA'   => 'K',
			'PRECO_DE'      => 'L',
			'PRECO_POR'     => 'M',
			'ECONOMIZE'     => 'N',
			'ENTRADA'       => 'O',
			'PARCELAS'      => 'P',
			'PRESTACAO'     => 'Q',
			'JUROS_AM'      => 'R',
			'JUROS_AA'      => 'S',
			'TOTAL_PRAZO'   => 'T',
			'OBSERVACOES'   => 'U',
			'EXTRA1'        => 'V',
			'EXTRA2'        => 'W',
			'EXTRA3'        => 'X',
			'EXTRA4'        => 'Y',
			'EXTRA5'        => 'Z',
		);

	/**
	 * Executa o transformador
	 *
	 * Este transformador em especifico esta ligado a
	 * planilha que e usada como padrao do sistema.
	 *
	 * Os dados que serao alterados devem primeiro
	 * ter passado pelo transformador apropriado para
	 * o cliente que esta usando o sistema.
	 *
	 * O parametro que vai ser passado para este transformador
	 * sera uma instancia de RetailWorkbook.
	 *
	 * @see IExcelImporter
	 * @see WalmartExcelImporter
	 * @see DefaultExcelImporter
	 * @see RetailSheet
	 * @see RetailWorkbook
	 * @see IActionTransform
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param mixed $data Dados recebidos para tratamento pelo transformador
	 * @return mixed Mesma estrutura de $data, porem com os valores transformados
	 */
	public function execute( $data ){
		// se nao for uma instancia de RetailWorkbook
		if( !($data instanceof RetailWorkbook) ){
			throw new Exception('Objeto invalido');
		}

		// se nao tivermos acoes, saimos da funcao
		if( empty($this->actions) ){
			return $data;
		}

		// vamos varrer todas as planilhas
		$list = $data->getSheets();

		// para cada planilha
		foreach($list as $plan){

			// para cada linha
			for( $line=5, $max = $plan->getMaxRow(); $line <= $max; $line++ ){

				// para cada campo adicionado
				foreach( $this->actions as $name => $actionList ){

					// seta a coordenada
					$coord = $this->map[ $name ] . $line;

					// pega o valor
					$value = $plan->getCellData( $coord );

					// para cada acao do campo
					foreach($actionList as $action){
						$value = $action->execute( $value );
					}

					// seta o valor novamente na planilha
					$plan->setCellData( $coord, $value );
				}

			}

		}

	}

	/**
	 * Adiciona uma acao para ser executada
	 *
	 * Toda acao devera ser vinculada a um campo
	 * ou coluna especifica, por isso o primeiro
	 * argumento e para identificacao deste campo.
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $fieldname nome do campo
	 * @param IActionTransform $action Acao a ser executada
	 * @param array $parameters Parametros para serem usados com a acao
	 * @return mixed valor transformado
	 */
	public function addAction($fieldname, IActionTransform $action, array $parameters = array()){
		if( !array_key_exists($fieldname, $this->map) ){
			throw new Exception('Campo inexistente: ' . $fieldname);
		}

		if( !empty($parameters) ){
			$action->setParameters($parameters);
		}

		$this->actions[ $fieldname ][] = $action;
	}

	/**
	 * Remove um transformador de um campo
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $fieldname Nome do campo
	 * @param IActionTransform $action
	 * @return void
	 */
	public function removeAction($fieldname, IActionTransform $action){
		if( !array_key_exists($fieldname, $this->map) ){
			throw new Exception('Campo inexistente: ' . $fieldname);
		}

		$nova = array();

		foreach($this->actions as $fname => $itens){
			$pode = true;

			if( $fname == $fieldname ){
				foreach($itens as $actionItem){
					if( $actionItem == $action ){
						$pode = false;
						break;
					}
				}
			}

			if( $pode ){
				$nova[$fname][] = $actionItem;
			}
		}

		$this->actions = $nova;
	}

	/**
	 * Carrega as acoes do banco de dados
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $id Codigo do conjunto de acoes que deve ser carregado
	 * @return void
	 */
	public function loadRules( $id ){
		$ci = &get_instance();

		$list = $ci->transformador->getActions( $id );

		foreach($list as $item){

			if( !class_exists($item['ACAO']) ){
				$filename = dirname(__FILE__) . '/actions/' . $item['ACAO'] . '.php';

				if( file_exists($filename) ){
					require_once $filename;
				}
			}

			$class = new ReflectionClass($item['ACAO']);

			$this->addAction($item['CAMPO'], $class->newInstance(), $item['PARAMETROS']);

		}

	}

	/**
	 * Carrega as regras baseadas nos dados do job
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idjob codigo do job
	 * @return ITransform
	 */
	public static function loadForJob( $idjob ){
		$ci = &get_instance();
		$tr = new ExcelTransform();

		$job = $ci->job->getById($idjob);

		return self::loadForClienteOrProduto($job['ID_CLIENTE'], $job['ID_PRODUTO']);
	}

	/**
	 * Carrega as regras baseadas ou em cliente, ou em produto
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param int $idcliente Codigo do cliente
	 * @param int $idproduto Codigo do Produto
	 * @return ITransform
	 */
	public static function loadForClienteOrProduto($idcliente = null, $idproduto = null){
		$ci = &get_instance();
		$tr = new ExcelTransform();

		$prod = $ci->produto->getById($idproduto);
		$cli = $ci->cliente->getById($idcliente);

		if( !empty($prod['ID_TRANSFORMADOR']) ){
			$tr->loadRules($prod['ID_TRANSFORMADOR']);

		} else {
			$tr->loadRules($cli['ID_TRANSFORMADOR']);

		}

		return $tr;
	}

}