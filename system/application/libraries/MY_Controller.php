<?php
class MY_Controller extends Controller
{

	public $data;

	/**
	 *
	 * @var AcaoDB
	 */
	public $acao;
	
	/**
	 *
	 * @var EncarteDB
	 */
	public $encarte;

	/**
	 *
	 * @var AnunDB
	 */
	public $anun;
	/**
	 *
	 * @var AnunItemDB
	 */
	public $anunItem;
	/**
	 *
	 * @var AnunPracaDB
	 */
	public $anunPraca;
	/**
	 *
	 * @var AprovacaoDB
	 */
	public $aprovacao;
	/**
	 *
	 * @var AprovacaoArquivoDB
	 */
	public $aprovacaoArquivo;
	/**
	 *
	 * @var AprovacaoComentarioDB
	 */
	public $aprovacaoComentario;
	/**
	 *
	 * @var AprovacaoComentarioArquivoDB
	 */
	public $aprovacaoComentarioArquivo;	

	/**
	 *
	 * @var AcaoemailDB
	 */
	public $acaoemail;

	/**
	 *
	 * @var Acao_emailDB
	 */
	public $acao_email;

	/**
	 *
	 * @var Aprovacao_fichaDB
	 */
	public $aprovacao_ficha;

	/**
	 *
	 * @var Aprovacao_paginacaoDB
	 */
	public $aprovacao_paginacao;

	/**
	 *
	 * @var Campo_fichaDB
	 */
	public $campo_ficha;

	/**
	 *
	 * @var CarrinhoDB
	 */
	public $carrinho;

	/**
	 *
	 * @var CategoriaDB
	 */
	public $categoria;

	/**
	 *
	 * @var Categoria_paginacaoDB
	 */
	public $categoria_paginacao;

	/**
	 *
	 * @var CentimetragemDB
	 */
	public $centimetragem;

	/**
	 *
	 * @var ClassificacaoDB
	 */
	public $classificacao;

	/**
	 *
	 * @var ColunaDB
	 */
	public $coluna;

	/**
	 *
	 * @var C_uDB
	 */
	public $c_u;

	/**
	 *
	 * @var Elemento_fichaDB
	 */
	public $elemento_ficha;

	/**
	 *
	 * @var EtapaDB
	 */
	public $etapa;

	/**
	 *
	 * @var Expectativa_pricingDB
	 */
	public $expectativa_pricing;

	/**
	 *
	 * @var FabricanteDB
	 */
	public $fabricante;

	/**
	 *
	 * @var FcpDB
	 */
	public $fcp;

	/**
	 *
	 * @var FichaDB
	 */
	public $ficha;

	/**
	 *
	 * @var Ficha_historicoDB
	 */
	public $ficha_historico;

	/**
	 *
	 * @var FormatoDB
	 */
	public $formato;

	/**
	 *
	 * @var FunctionsDB
	 */
	public $functions;

	/**
	 *
	 * @var GeneralDB
	 */
	public $general;

	/**
	 *
	 * @var HistoricoDB
	 */
	public $historico;

	/**
	 *
	 * @var historico_fichaDB
	 */
	public $historico_ficha;

	/**
	 *
	 * @var ImpostoDB
	 */
	public $imposto;

	/**
	 *
	 * @var JuroDB
	 */
	public $juro;

	/**
	 *
	 * @var LogDB
	 */
	public $log;

	/**
	 *
	 * @var MarcaDB
	 */
	public $marca;

	/**
	 *
	 * @var MethodDB
	 */
	public $method;

	/**
	 *
	 * @var MidiaDB
	 */
	public $midia;

	/**
	 *
	 * @var ModuleDB
	 */
	public $module;

	/**
	 *
	 * @var Obj_fichaDB
	 */
	public $obj_ficha;
	
	/**
	 *
	 * @var Objeto_FichaDB
	 */
	public $objeto_ficha;
	
	/**
	 *
	 * @var ObjetoDB
	 */
	
	public $objeto;

	/**
	 *
	 * @var PaginacaoDB
	 */
	public $paginacao;

	/**
	 *
	 * @var PaginaDB
	 */
	public $pagina;

	/**
	 *
	 * @var Pedido_fichaDB
	 */
	public $pedido_ficha;

	/**
	 *
	 * @var Perm_typeDB
	 */
	public $perm_type;

	/**
	 *
	 * @var PracaDB
	 */
	public $praca;

	/**
	 *
	 * @var Praca_veiculoDB
	 */
	public $praca_veiculo;

	/**
	 *
	 * @var PrepaginacaoDB
	 */
	public $prepaginacao;

	/**
	 *
	 * @var PricingDB
	 */
	public $pricing;

	/**
	 *
	 * @var PromocaoDB
	 */
	public $promocao;

	/**
	 *
	 * @var SelecaoDB
	 */
	public $selecao;

	/**
	 *
	 * @var Situacao_paginacaoDB
	 */
	public $situacao_paginacao;

	/**
	 *
	 * @var StatusDB
	 */
	public $status;

	/**
	 *
	 * @var SubcategoriaDB
	 */
	public $subcategoria;

	/**
	 *
	 * @var Tipo_objetoDB
	 */
	public $tipo_objeto;

	/**
	 *
	 * @var Tipo_paginacaoDB
	 */
	public $tipo_paginacao;

	/**
	 *
	 * @var Tipo_planoDB
	 */
	public $tipo_plano;

	/**
	 *
	 * @var UsuarioDB
	 */
	public $usuario;

	/**
	 *
	 * @var VeiculoDB
	 */
	public $veiculo;

	/**
	 *
	 * @var WebsalDB
	 */
	public $websal;

	/**
	 *
	 * @var XinetDB
	 */
	public $xinet;
	/**
	 *
	 * @var CampoDB
	 */
	public $campo;
	/**
	 *
	 * @var AgenciaDB
	 */
	public $agencia;
	/**
	 *
	 * @var ClienteDB
	 */
	public $cliente;
	/**
	 *
	 * @var ProdutoDB
	 */
	public $produto;
	/**
	 *
	 * @var CampanhaDB
	 */
	public $campanha;

	/**
	 *
	 * @var GrupoDB
	 */
	public $grupo;

	/**
	 *
	 * @var GacpDB
	 */
	public $gacp;
	/**
	 *
	 * @var ControllerDB
	 */
	public $controller;
	/**
	 *
	 * @var FunctionDB
	 */
	public $function;
	/**
	 *
	 * @var PermissaoDB
	 */
	public $permissao;
	/**
	 *
	 * @var ItemMenuDB
	 */
	public $itemMenu;
	/**
	 *
	 * @var TipoPecaDB
	 */
	public $tipoPeca;
	/**
	 *
	 * @var TipoPecaLimiteDB
	 */
	public $tipoPecaLimite;
	/**
	 *
	 * @var TemplateDB
	 */
	public $template;
	/**
	 *
	 * @var EmailMensagemDB
	 */
	public $emailMensagem;
	/**
	 *
	 * @var ExcelDB
	 */
	public $excel;
	/**
	 *
	 * @var ExcelVersaoDB
	 */
	public $excelVersao;
	/**
	 *
	 * @var SegmentoDB
	 */
	public $segmento;
	/**
	 *
	 * @var TipoJobDB
	 */
	public $tipoJob;
	/**
	 *
	 * @var RegraEmailDB
	 */
	public $regraEmail;

	/**
	 *
	 * @var RegraUsuarioDB
	 */
	public $regraUsuario;
	
	/**
	 *
	 * @var RegraEmailProcessoEtapaUsuarioDB
	 */
	public $regraEmailProcessoEtapaUsuario;

	/**
	 *
	 * @var RegraEmailProcessoEtapaDB
	 */
	public $regraEmailProcessoEtapa;
	
	/**
	 *
	 * @var ExcelopiDB
	 */
	public $excelopi;

	/**
	 *
	 * @var MY_Email
	 */
	public $email;

	/**
	 *
	 * @var RelatorioDB
	 */
	public $relatorio;

	/**
	 *
	 * @var JobDB
	 */
	public $job;
	
	/**
	 *
	 * @var JobHistoricoDB
	 */
	public $jobHistorico;
	
	/**
	 *
	 * @var ProcessoDB
	 */
	public $processo;
	/**
	 *
	 * @var ProcessoEtapaDB
	 */
	public $processoEtapa;

	/**
	 *
	 *
	 * @var TransformadorDB
	 */
	public $transformador;
	/**
	 *
	 *
	 * @var TransformadorAcaoDB
	 */
	public $transformadorAcao;
	/**
	 *
	 *
	 * @var RegiaoDB
	 */
	public $regiao;

	/**
	 *
	 *
	 * @var CalendarioDB
	 */
	public $calendario;
	
	/**
	 *
	 * @var IDS
	 */
	public $ids;

	/**
	 *
	 *
	 * @var TipoPecaPracaDB
	 */
	public $tipoPecaPraca;
	
	/**
	 *
	 *
	 * @var CenarioDB
	 */
	public $cenario;
	
	/**
	 *
	 * @var SubfichaDB
	 */
	public $subficha;
	
	/**
	 *
	 * @var CampoRelacionadoDB
	 */
	public $campoRelacionado;

	/**
	 *
	 * @var JobUsuarioDB
	 */
	public $jobUsuario;

	/**
	 * Indica o caminho base dos templates da controller atual
	 * @var string
	 */
	protected $_templatesBasePath;


	function __construct(){
		parent::Controller();
	}

	/**
	 * Exibe o template na tela
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $tpl Nome do template a ser usado
	 * @param boolean $return Indica se eh para retornar o conteudo ou exibir na tela
	 * @return mixed
	 */
	protected function display($tpl, $return=false) {
		return $this->load->view($this->_templatesBasePath . $tpl, $this->data, $return);
	}

	/**
	 * Exibe uma janela de erro
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $msg
	 * @return void
	 */
	protected function showError($msg){
		$this->data['msg'] = $msg;
		$this->load->view('ROOT/layout/erro', $this->data);
	}

	/**
	 * Verifica se o usuario logado pode alterar algum registro
	 *
	 * <p>Este metodo deve ser especializado nas classes que herdam
	 * MY_Controller</p>
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data Dados do registro
	 * @return void
	 */
	protected function checaPermissaoAlterar($data){

	}

	/**
	 * Adiciona uma variavel em $data
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param string $var
	 * @param mixed $value
	 * @return void
	 */
	protected function assign($var, $value){
		$this->data[$var] = $value;
	}

	/**
	 * Adiciona os valores da sessao
	 *
	 * @author Hugo Ferreira da Silva
	 * @link http://www.247id.com.br
	 * @param array $data
	 * @return void
	 */
	protected function setSessionData( &$data ){
		$user = Sessao::get('usuario');
		foreach($user as $key => $val){
			if( $val != '' ){
				$data[ $key ] = $val;
			}
		}
	}
}
