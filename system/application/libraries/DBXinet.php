<?php
class DBXinet extends CI_Base
{
	function DBXinet()
	{
		require(APPPATH . 'config/database' . EXT);
		
		$CI =& get_instance();
		
		$config = $db2["xinet"];
		
		$dsn = $config['dbdriver'].'://' . $config['username'] . ':' . $config['password'] . '@' . $config['hostname'] . '/' . $config['database'].'?char_set=latin1&dbcollat=latin1_general_ci';
		
		$CI->db_xinet = $CI->load->database($dsn, true);
	}
}
