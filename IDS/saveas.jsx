var origemfile = new File(origem);
var objDocumento = app.open(File(origemfile));

temp= origem.replace(".indd",".xpto.indd")
tempfile = new File(temp);

app.documents.item(0).close(SaveOptions.YES, tempfile);

if (tempfile.exists) {
	origemfile.remove();
	tempfile.rename(tempfile.name.replace(".xpto.indd",".indd"))
	return 1;
} else {
	return 0;
}