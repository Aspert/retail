var pathPdf = decodeURIComponent(app.scriptArgs.get("save_pdf").replace("+", " "));
var pathImagens = decodeURIComponent(app.scriptArgs.get("path_files").replace("+", " "));
var imagens = decodeURIComponent(app.scriptArgs.get("files").replace("+", " "));
var pid = app.scriptArgs.get("pid");
var pathLog = decodeURIComponent(app.scriptArgs.get("path_log").replace("+", " "));

function logPID(dirPath, pid, status, message){	
	if(status == 0){
		status = "fail";
	}else{
		status = "ok";
	}

	status = status.toLowerCase();
	
	var logFile = new File(dirPath + "/"+pid+"."+status);
	try{
		logFile.open("w");
		if(message){
			logFile.write(message);
		}
	}catch(e){
		throw e;
	}finally{
		if(logFile){
			logFile.close();
		}
	}
}

//var pathImagens = "/C/Documents and Settings/sidnei.tertuliano/Meus documentos/Minhas imagens/teste/";
//var imagens = "01.jpg,02.jpg,03.jpg,04.jpg";

var objDocumento = app.documents.add();
objDocumento.documentPreferences.facingPages = false;

objDocumento.viewPreferences.horizontalMeasurementUnits = MeasurementUnits.points;
objDocumento.viewPreferences.verticalMeasurementUnits = MeasurementUnits.points;

var arrImagens = imagens.split(",");

try{
	for(var i = 0; i <= arrImagens.length - 1; i++){
		if(i > 0){
			var objPage = objDocumento.pages.add()
		}
		else{
			var objPage = objDocumento.pages.item(0);
		}

		var objRetangulo = objPage.rectangles.add();
		objRetangulo.geometricBounds = [0, 0, objDocumento.documentPreferences.pageHeight, objDocumento.documentPreferences.pageWidth];
		var objFile = new File(pathImagens + arrImagens[i]);

		if(objFile.exists){
			objRetangulo.place(objFile);
			objRetangulo.fit(1667591779);
			objRetangulo.fit(1668247152);
		}
	}

	objDocumento.exportFile(ExportFormat.pdfType, new File(pathPdf), app.pdfExportPresets.item("[Smallest File Size]"));
	logPID(pathLog, pid, 1);
}
catch(e){
	logPID(pathLog, pid, 0, e.description);
}
finally{
	app.documents.item(0).close(SaveOptions.NO);
}