﻿function right(str, n){
	if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}

origem = decodeURIComponent(app.scriptArgs.get("origem"));
destino = decodeURIComponent(app.scriptArgs.get("destino"));
jpgTemp = "/Library/WebServer/Documents/IDS/temp/" + Math.round((Math.random()*9)+1) + ".jpg";
pdfTemp = "/Library/WebServer/Documents/IDS/temp/" + Math.round((Math.random()*9)+1) + ".pdf";
var retorno = new Array();

var objArquivoOrigem = new File(origem);

if(right(origem, 4) == "indd"){
	// o argumento passado é de um arquivo indd

	if(objArquivoOrigem.exists){
		// o arquivo existe

		if(objArquivoOrigem.copy(destino)){
			// a cópia foi feita com sucesso
			var objDocumento = app.open(File(destino));

			// SAVE AS para diminuir o tamanho do arquivo
			temp = destino.replace(".indd",".xpto.indd")
			tempfile = new File(temp);
			app.documents.item(0).close(SaveOptions.YES, tempfile);

			if (tempfile.exists) {
				// arquivo 'save as' existe

				// a cópia deu certo, apago o original no webserver
				objArquivoOrigem.remove();

				// apago o arquivo de destino com o nome original, antes de mudar o nome do novo arquivo
				var objArquivoDestino = new File(destino);
				objArquivoDestino.remove();

				// mudo o nome do arquivo para ficar igual ao nome original
				tempfile.rename(tempfile.name.replace(".xpto.indd",".indd"));
				objDocumento = app.open(File(tempfile));

				// crio arquivo JPG
				// configuro parâmetros do arquivo
				app.jpegExportPreferences.jpegQuality = JPEGOptionsQuality.maximum;
				app.jpegExportPreferences.resolution = 150;
				//app.jpegExportPreferences.jpegExportRange = 1785742657 ;
				//app.jpegExportPreferences.pageString = '1';
				jpgTemp = new File(destino.replace(".indd", ".jpg"));
				// gravo arquivo
				objDocumento.exportFile(1246775072, jpgTemp);
				// coloco extensão correta
				//jpgTemp.copy(destino.replace(".indd", ".jpg"));
				// apago arquivo temporário
				//jpgTemp.remove();

				// crio arquivo PDF
				// configuro parâmetros do arquivo
				//myPDFExportPreset = app.pdfExportPresets.item("[Smallest File Size]");
				//objDocumento.exportFile(ExportFormat.pdfType, new File(pdfTemp), myPDFExportPreset);
				//pdfTemp = new File(pdfTemp);
				// gravo arquivo
				//objDocumento.exportFile(ExportFormat.pdfType, pdfTemp, myPDFExportPreset);
				// coloco extensão correta
				//pdfTemp.copy(destino.replace(".indd", ".pdf"));
				// apago arquivo temporário
				//pdfTemp.remove();

				for(i = 0; i <= objDocumento.links.length - 1; i++){
					if(objDocumento.links.item(i).status != "1282237028"){
						retorno[i] = objDocumento.links.item(i).filePath;
					}
				}

				app.documents.item(0).close();
			}else{
				retorno[0] = "O arquivo 'saved as' nao foi encontrado";
			}
		}else{
			retorno[0] = "O arquivo nao foi copiado";
		}
	}else{
		retorno[0] = "O arquivo nao existe";
	}
}else{
	retorno[0] = "O arquivo de origem nao é InDesign";
}

retorno;
