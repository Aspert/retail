﻿function right(str, n){
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}

origem = decodeURIComponent(app.scriptArgs.get("origem"));
jpgTemp = "/Library/WebServer/Documents/IDS/temp/" + Math.round((Math.random()*100000)+1) + ".jpg";
pdfTemp = "/Library/WebServer/Documents/IDS/temp/" + Math.round((Math.random()*100000)+1) + ".pdf";
var retorno = new Array();

var objArquivoOrigem = new File(origem);

if(objArquivoOrigem.exists){
	if(right(objArquivoOrigem.name, 4) == "indd"){
		
		try{
			//gera objetos file
			jpgTemp = new File(jpgTemp);
			pdfTemp = new File(pdfTemp);
			
			var objDocumento = app.open(objArquivoOrigem);
			//preferencias de exportacao de JPEG
			app.jpegExportPreferences.jpegQuality = JPEGOptionsQuality.maximum;
			app.jpegExportPreferences.resolution = 150;
			app.jpegExportPreferences.jpegExportRange = 1785742674;
			app.jpegExportPreferences.pageString = '1';
			
			//gera arquivo jpeg de preview
			objDocumento.exportFile(1246775072, jpgTemp);
			jpgTemp.copy(origem.replace(".indd", ".jpg"));
			
			//export pdf preset
			myPDFExportPreset = app.pdfExportPresets.item("[Smallest File Size]");
			//gera preview em PDF
			objDocumento.exportFile(ExportFormat.pdfType, new File(pdfTemp), myPDFExportPreset);
			
			objDocumento.exportFile(ExportFormat.pdfType, pdfTemp, myPDFExportPreset);
			pdfTemp.copy(origem.replace(".indd", ".pdf"));
			
		}catch(e){
			retorno[0] = e.description;
		}finally{
			//remove os arquivos temporarios
			if(jpgTemp.exists()){
				jpgTemp.remove();
			}
			if(pdfTemp.exists()){
				pdfTemp.remove();
			}
			//fecha documento
			app.documents.item(0).close();
		}
	}else{
		retorno[0] = "Arquivo não possui extensao indd";
	}
}
else{
	retorno[0] = "O arquivo de origem nao existe";
}
 
retorno;