//FUNCAO PARA DELETAR OS ELEMENTOS DENTRO DE UM NOH XML
function deletaElementosXml(objXml){
	try{objXml.texts.item(0).parentTextFrames[0].remove();}
	catch(e){}
	try{objXml.remove();}
	catch(e){}
}

var arrLinksInternos = new Array();
var arrLinksExternos = new Array();
var arrLinksBaixar = new Array();
var arr = new Array();

if(app.documents.length > 0){
	app.scriptPreferences.userInteractionLevel = UserInteractionLevels.NEVER_INTERACT;
	var folder = new Folder(pluginPath);
	folder.create();

	if(folder.exists){
		try{jobinfo = replaceAll(app.activeDocument.xmlElements.item(0).xmlElements.item("jobinfo").markupTag.name, " ", "_");}
		catch(e){jobinfo = ""}
		try{idJob = app.activeDocument.xmlElements.item(0).xmlElements.item("jobinfo").xmlAttributes.item("id_job").value;}
		catch(e){idJob = ""}
		try{idPraca = app.activeDocument.xmlElements.item(0).xmlElements.item("jobinfo").xmlAttributes.item("id_praca").value;}
		catch(e){idPraca = ""}
		try{idExcel = app.activeDocument.xmlElements.item(0).xmlElements.item("jobinfo").xmlAttributes.item("id_excel").value;}
		catch(e){idExcel = ""}
		try{servidor = app.activeDocument.xmlElements.item(0).xmlElements.item("jobinfo").xmlAttributes.item("servidor").value;}
		catch(e){servidor = ""}
		try{paginaInicio = parseInt(app.activeDocument.xmlElements.item(0).xmlElements.item("jobinfo").xmlAttributes.item("pagina_inicio").value);}
		catch(e){paginaInicio = 0}
		try{paginaFim = parseInt(app.activeDocument.xmlElements.item(0).xmlElements.item("jobinfo").xmlAttributes.item("pagina_fim").value);}
		catch(e){paginaFim = 0}

		if((jobinfo != "") && (idJob != "") && (idPraca != "") && (idExcel != "") && (servidor != "")){
			var folder = new Folder(pluginPath + barra + idJob + "_" + idPraca);
			deletaDiretorio(folder) ;
			folder.create();
			
			if(folder.exists){
				folder = new Folder(pluginPath + barra + idJob + "_" + idPraca + barra + "img");
				deletaDiretorio(folder);
				folder.create();
				
				if(folder.exists){
					//VARRE ELEMENTOS DENTRO DO XML DO INDESIGN
					for(var i = 0; i <= app.activeDocument.xmlElements.item(0).xmlElements.length - 1; i++){
						//SE NO FOR DO TIPO CENARIO
						if(app.activeDocument.xmlElements.item(0).xmlElements.item(i).markupTag.name == "cenario"){
							
							//VARRE ELEMENTOS DO OBJETO FICHA
							for(var w = 0; w <= app.activeDocument.xmlElements.item(0).xmlElements.item(i).xmlElements.length - 1; w++){
								tipo = app.activeDocument.xmlElements.item(0).xmlElements.item(i).xmlElements.item(w).xmlContent.constructor.name;
								tipo = tipo.toUpperCase();
								
								//VERIFICA SE TIPO EPS, IMAGEM OU RETANGULO
								if((tipo == "EPS") || (tipo == "IMAGE") || (tipo == "RECTANGLE")){
									//RETORNA PATH DO ARQUIVO
									linkPath = decodeURIComponent(app.activeDocument.xmlElements.item(0).xmlElements.item(i).xmlElements.item(w).xmlAttributes.item("href").value);
									linkPath = linkPath.replace("file:///Volumes", "");
									
									try{
										//RETORNA ATRIBUDO DE DATA
										var linkData = app.activeDocument.xmlElements.item(0).xmlElements.item(i).xmlElements.item(w).xmlAttributes.item("data_alteracao").value;
									} catch(e){
										 linkData = new Date().getTime();
									}

									//ADICIONA NA ARRAY DE LINKS INTERNOS CASO O PATH SEJA DO BANCO DE TEMPLATES
									var _linkPath = linkPath.toUpperCase();
									if(_linkPath.indexOf("BANCO DE TEMPLATES/TEMPLATE_") == -1){
										arrLinksInternos.push(linkPath + "###" + linkData);
									}
								}
							}

						}
					}

					//RETORNA O XML DE FICHAS E CENARIOS
					if(File.fs == "Windows"){
						var batText = "@echo off\n";
						batText += "echo Atualizando XML\n";
						batText += "@java -jar " + pluginPath + barra + "plugin.jar \"atualiza_fichas\" \"" + pluginPath + "\" \"" + servidor + "\" \"" + idJob + "\" \"" + idPraca + "\" \"" + idExcel + "\" \"" + paginaInicio.toString() + "\" \"" + paginaFim.toString() + "\"";
						var fileExec = new File(pluginPath + idJob + "_" + idPraca + barra + "xml.bat");
						fileExec.open("w");
						fileExec.write(batText);
						fileExec.close();
						fileExec.execute();
					}
					else{
						var shText = "java -jar " + pluginPath + barra + "plugin.jar \"atualiza_fichas\" \"" + pluginPath + "\" \"" + servidor + "\" \"" + idJob + "\" \"" + idPraca + "\" \"" +idExcel + "\" \"" + paginaInicio.toString() +  "\" \"" + paginaFim.toString() + "\"; ";
						var shScript = pluginPath + idJob + "_" + idPraca + barra + "xml.sh";
						var fileExec = new File(shScript);
						fileExec.open("w+x");
						fileExec.write(shText);
						fileExec.close();
						var appleScript = "do shell script \"sh '" + shScript + "' \"\n ";
						app.doScript(appleScript, ScriptLanguage.APPLESCRIPT_LANGUAGE);
					}

					//AGUARDA FINALIZACAO DAS TAREFAS DO JAVA. CASO O ARQUIVO local.pid SEJA CRIADO, FORAM FINALIZADA AS TAREFAS
					do{
						var pidFile = new File(pluginPath + barra + idJob + "_" + idPraca + barra + "local.pid");
						var errFile = new File(pluginPath + barra + idJob + "_" + idPraca + barra + "local.xml.err");
						var errControle = new File(pluginPath + barra + "controle.err");

						if(errFile.exists){
							alert("Nao foi possivel importar o XML externo");
							break;
						}
						errFile.close();

						if(errControle.exists){
							alert("Nao foi possivel setar ambiente");
							break;
						}
						errControle.close();
					}
					while(!pidFile.exists);

					fileExec.remove();
					pidFile.remove();

					//IMPORTA XML EXTERNO VINDO DO JAVA
					try{
						var objXmlRootExterno = importaXml(pluginPath + barra + idJob + "_" + idPraca + barra + "local.xml");

						if(objXmlRootExterno.xpath("//jobinfo")  == ""){
							alert("XML invalido!");
						}
					}
					catch(e){
						alert("XML invalido!");
					}
					
					if(!verificaCompatibilidadePaginas(app.activeDocument, objXmlRootExterno)){
						alert("XML contem mais paginas doque o Documento InDesign!");
					}

					//ABRE COMO ARQUIVO XML VINDO DO JAVA EM MODO LEITURA
					file = new File(pluginPath + barra + idJob + "_" + idPraca + barra + "local.xml");
					file.open("r");
					var stringXml = file.read();
					file.close();
					file.remove();

					//RETORNA OBJETOS NAO QUAL O ATRIBUTO ACAO SEJA VAZIO
					objXmlCenariosExternos = objXmlRootExterno.xpath("//cenario");

					//VARRE OBJETO COM CENARIOS VAZIAS
					for(var i = 0; i <= objXmlCenariosExternos.length() - 1; i++){
						//VARRE ELEMENTOS DO OBJETO
						for(var y = 0; y <= objXmlCenariosExternos[i].elements().length() - 1; y++){
							//RETORNA PATH
							linkPath = objXmlCenariosExternos[i].elements()[y].attribute("link").toString();
							linkData = objXmlCenariosExternos[i].elements()[y].attribute("data_alteracao").toString();
							//CASO O ARQUIVO TENHA PATH CORRETO
							if(linkPath != ""){
								//VERIFICA SE � DIFERENTE DE BANCO DE TEMPLATES E ADICIONA NA ARRAY DE L INKS EXTERNOS
								var _linkPath = linkPath.toUpperCase();
								if(_linkPath.indexOf("BANCO DE TEMPLATES/TEMPLATE_") == -1){
									arrLinksExternos.push(linkPath + "###" + linkData);
								}
							}
						}
					}

					//REMOVE REPETIDOS
					arrLinksExternos = removeRepetidosArray(arrLinksExternos);

					//FAZ COMPARACAO ENTRE LINK INTERNO E EXTERNO. CASO O OBJETO EXISTA NO LINK INTERNO E N�O NO EXTERNO, BAIXA VIA JAVA
					var arquivosDownload = pluginPath + idJob + "_" + idPraca + barra + "arquivos.txt" ;
					var fileArquivos = new File(arquivosDownload);
					fileArquivos.open("w");
					
					for(var i = 0; i <= arrLinksExternos.length - 1; i++){
						var arr = arrLinksExternos[i].split("###");
						if(arr.length == 2){
							linkPath = arr[0];
							linkData = arr[1];
							
							//RETORNA A PATH DO ARQUIVO E REMOVE FPO
							filePath = getPathArquivo(linkPath);
							filePath = filePath.replace("_FPO", "");
							
							//RETORNA NOME DE ARQUIVO
							fileName = getNomeArquivo(linkPath);
							
							//REMOVE EXTENSAO DO ARQUIVO
							fileNameEps = removeExtensao(fileName) + ".eps";
							fileNameErr = removeExtensao(fileName) + ".err";
							
							var fileError = new File(pluginPath + barra + idJob + "_" + idPraca + barra + "img" + barra + fileNameErr);
							fileError.remove();
							
							fileArquivos.writeln(filePath + "/" + fileName);
						}
					}

					fileArquivos.close();

					if(File.fs == "Windows"){
						var fileExec = new File(pluginPath + idJob + "_" + idPraca + barra + "arquivos.bat");
						fileExec.open("w");
						fileExec.write("@java -jar " + pluginPath + barra + "plugin.jar \"baixa_fpo\" \"" + pluginPath + "\" \"" + servidor + "\" \"" + arquivosDownload + "\" \"" + idJob + "\" \"" + idPraca + "\" \"" + idExcel + "\" \"true\" \n");
						fileExec.close();
						
						if(fileExec.exists){
							fileExec.execute();
						}
						else{
							alert("Nao foi possivel efetuar downloads");
						}
					}
					else{
						var shText = "java -jar " + pluginPath + barra + "plugin.jar \"baixa_fpo\" \"" + pluginPath + "\" \"" + servidor + "\" \"" + arquivosDownload+ "\" \"" +idJob + "\" \"" +idPraca + "\" \"" + idExcel + "\" \"true\";";
						var shScript = pluginPath + idJob + "_" + idPraca + barra + "arquivos.sh";
						var fileExec = new File(shScript);
						fileExec.open("w+x");
						fileExec.write(shText);
						fileExec.close();
						
						if(fileExec.exists){
							var appleScript = "do shell script \"sh '" + shScript + "' \"\n ";
							app.doScript(appleScript, ScriptLanguage.APPLESCRIPT_LANGUAGE);
						} else{
							alert("Nao foi possivel efetuar downloads");
						}
					}

					for(var i = 0; i <= arrLinksExternos.length - 1; i++){
						//CONTROLE DE COMPARACAO
						var boo = false;
						
						//ARRAY DE LINK INTERNO FAZENDO COMPARACAO COM EXTERNO
						for(var y = 0; y <= arrLinksInternos.length - 1; y++){
							if(arrLinksExternos[i] == arrLinksInternos[y]){
								boo = true;
								break;
							}
						}
						

						//ARRAY DE LINKS INTERNOS
						var arr = arrLinksExternos[i].split("###");
						
						//CASO O NOME DO ARQUIVO TENHA ### E DATA
						if(arr.length == 2){
							linkPath = arr[0];
							linkData = arr[1];
							
							fileName = getNomeArquivo(linkPath);
							fileNameEps = removeExtensao(fileName) + ".eps";
							fileNameErr = removeExtensao(fileName) + ".err";
							
							do{
								var file = new File(pluginPath + barra + idJob + "_" + idPraca + barra + "img" + barra + fileNameEps);
								var fileError = new File(pluginPath + barra + idJob + "_" + idPraca + barra + "img" + barra + fileNameErr);
								var fileErrorTotal = new File(pluginPath + barra + idJob + "_" + idPraca + barra + "erro_total.err");

								if(fileError.exists){
									alert("Arquivo: " +  fileNameEps + " nao foi baixado");
									break;
								}
								if(fileErrorTotal.exists){
									alert("Nao foi possivel baixar arquivos");
									break;
								}
							} while(!file.exists);

							if(file.exists){
								//REPLACE DE ARQUIVO EXTERNO PARA INTERNO
								var path1 = replaceAll(getPathArquivo(linkPath) + barra + getNomeArquivo(linkPath), "\\", "/");
								var path2 = replaceAll(pluginPath + barra + idJob + "_" + idPraca + barra + "img" + barra + fileNameEps, "\\", "/");
								var stringXml = replaceAll(stringXml, path1, path2);
							} else{
								alert("Arquivo: " + pluginPath + barra + idJob + "_" + idPraca + barra + "img" + barra + fileNameEps + " nao existe");
							}
						}
					}

					fileExec.remove();
					fileArquivos.remove();

					//CRIA XML FINAL PARA O SCRIPT update.jsx FAZER AS MUDAN�AS (gorgonza)
					var fileXml = new File(pluginPath + barra + idJob + "_" + idPraca + barra + "3_.xml");
						fileXml.open("w", "0777","0777");

					try{
						for(wz = 0; wz < stringXml.length; wz++){
							fileXml.write(stringXml[wz]);
						}
					}catch(e){
						alert("Nao foi possivel salvar XML local com as alteracoes");
					}
					fileXml.close();

					if(app.documents.length > 0){
						//ABRE O DOCUMENTO DO INDESIGN ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
						var objDocumento = app.activeDocument;
						//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

						//MUDA AS UNIDADES DE MEDIDA DA PAGINA PARA POINTS --------------------------------------------------------------------------------------------------------------------------------------------------
						objDocumento.viewPreferences.horizontalMeasurementUnits = MeasurementUnits.points;
						objDocumento.viewPreferences.verticalMeasurementUnits = MeasurementUnits.points;
						//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

						//DEFINE O PRIMEIRO ELEMENTO DO XML O ROOT ----------------------------------------------------------------------------------------------------------------------------------------------------------
						var objXmlRootInterno = objDocumento.xmlElements.item(0);
						//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

						//IMPORTA O XML EXTERNO -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
						var objXmlRootExterno = importaXml(pluginPath + "/" + idJob + "_" + idPraca + "/3_.xml");
						fileXml.remove();
						//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

						//ATUALIZA A VERSAO DENTRO DO JOBINFO ---------------------------------------------------------------------------------------------------------------------------------------------------------------
						objXmlRootInterno.xmlElements.item("jobinfo").xmlAttributes.item("versao").value = objXmlRootExterno.xpath("//jobinfo").attribute("versao").toString();
						//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
						
						///DEFINE O CAMINHO ONDE IRAO SER SALVAS AS IMAGENS LOCAIS (strPathImagensLocais)  ------------------------------------------------------------------------------------------------------------------
						var strPathImagensLocais = pluginPath + "\\" + objXmlRootInterno.xmlElements.item("jobinfo").xmlAttributes.item("id_job").value + "_" + objXmlRootInterno.xmlElements.item("jobinfo").xmlAttributes.item("id_praca").value + "\\img\\";
						//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

						//DELETA AS CENARIOS INUTILIZADAS -------------------------------------------------------------------------------------------------------------------------------------------------------------------
						for(i = objXmlRootInterno.xmlElements.length - 1; i >= 0; i--){
							if(objXmlRootInterno.xmlElements.item(i).markupTag.name == "cenario"){
								var cenarioAtivo = false;
								if(objXmlRootInterno.xmlElements.item(i).xmlElements.length > 0){
									for(y = objXmlRootInterno.xmlElements.item(i).xmlElements.length - 1; y >= 0; y--){
										if(objXmlRootInterno.xmlElements.item(i).xmlElements.item(y).xmlContent.constructor.name != "Text"){
											cenarioAtivo = true;
										}
									}
								}

								if(cenarioAtivo == false){
									objXmlRootInterno.xmlElements.item(i).xmlElements.everyItem().remove();
									objXmlRootInterno.xmlElements.item(i).remove();
								}
							}
						}
						//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

						//ALTERANDO CONTEUDOS DAS FICHAS---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
						var cenariosExternos = objXmlRootExterno.xpath("//cenario");

						for(i = cenariosExternos.length() - 1; i >= 0; i--){
							var idCenarioExterno = cenariosExternos[i].attribute("id");

							for(y = objXmlRootInterno.xmlElements.length - 1; y >= 0; y--){
								if(objXmlRootInterno.xmlElements.item(y).markupTag.name == "cenario"){
									var idCenarioInterno = objXmlRootInterno.xmlElements.item(y).xmlAttributes.item("id").value;
									if(idCenarioExterno == idCenarioInterno){
										var rectangleCenario = objXmlRootInterno.xmlElements.item(y).xmlElements.item("imagem_p").xmlContent.parent;
										if(rectangleCenario.constructor.name == "Rectangle"){
											try{
												rectangleCenario.place( File(cenariosExternos[i].child("imagem_p").attribute("link")) );
												rectangleCenario.fit(1667591779);
												rectangleCenario.fit(1668247152);
												rectangleCenario.epss.item(0).itemLink.unlink();
											}
											catch(e){}
										}
									}
								}
							}

						}
						//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

						//DELETANDO CENARIOS VAZIOS ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
						for(y = objXmlRootInterno.xmlElements.length - 1; y >= 0; y--){
							if(objXmlRootInterno.xmlElements.item(y).markupTag.name == "cenario"){
								var idCenarioInterno = objXmlRootInterno.xmlElements.item(y).xmlAttributes.item("id").value;
								var cenariosExternos = objXmlRootExterno.xpath("//cenario[@id='" + idCenarioInterno + "']");

								if( cenariosExternos.toXMLString() == "" ){
									for(var h = objXmlRootInterno.xmlElements.item(y).xmlElements.length - 1; h >= 0; h--){
										deletaElementosXml(objXmlRootInterno.xmlElements.item(y).xmlElements.item(h));
									}
									deletaElementosXml(objXmlRootInterno.xmlElements.item(y));
								}
							}
						}
						//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

						//ADICIONAR NOVAS CENARIOS FORA DA PAGINA -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
						var adicionar = new Array();
						var contents = "";

						var cenariosExternos = objXmlRootExterno.xpath("//cenario");
						var intContador = 0;
						var alturaCenario = 200;
						var larguraCenario = 200;
						var espaco = 5;
						
						for(var i = cenariosExternos.length() - 1; i >= 0; i--){
							if(cenariosExternos[i].name() == "cenario"){
								var booCenarioEncontrado = false;
								var idCenarioExterno = cenariosExternos[i].attribute("id").toString();
		
								for(y = objXmlRootInterno.xmlElements.length - 1; y >= 0; y--){
									if(objXmlRootInterno.xmlElements.item(y).markupTag.name == "cenario"){
										var idCenarioInterno = objXmlRootInterno.xmlElements.item(y).xmlAttributes.item("id").value;
										if( idCenarioExterno == idCenarioInterno ){
											booCenarioEncontrado = true;
											break;
										}
									}
								}

								if( booCenarioEncontrado ){
									continue;
								}
								else{
									if(intContador == 0){
										var y1 = 0;
										var x1 = 0;
										var y2 = (y1 + alturaCenario);
										var x2 = (x1 + larguraCenario);
									}
									else{
										var y1 = 0;
										var x1 = (larguraCenario * intContador) + espaco;
										var y2 = (y1 + alturaCenario);
										var x2 = (larguraCenario * intContador) + espaco + larguraCenario;
									}

									var idCenario = cenariosExternos[i].attribute("id").toString();
									var nomeCenario = cenariosExternos[i].attribute("nome").toString();
									var codigoCenario = cenariosExternos[i].attribute("codigo").toString();
									var link = cenariosExternos[i].child("imagem_p").attribute("link").toString()

									var objCenario = objDocumento.rectangles.add();
									objCenario.geometricBounds = [y1, x1, y2, x2];
									objCenario.place(File(link));
									try{objCenario.epss.item(0).itemLink.unlink();}catch(e){}
									objCenario.fit(1667591779);
									objCenario.fit(1668247152);

									var xmlCenario = objXmlRootInterno.xmlElements.add("cenario");
									xmlCenario.xmlAttributes.add("id", idCenario);
									xmlCenario.xmlAttributes.add("nome", nomeCenario);
									xmlCenario.xmlAttributes.add("codigo", codigoCenario);
									
									var xmlImagemCenario = xmlCenario.xmlElements.add("imagem_p");
									xmlImagemCenario.xmlAttributes.add("link", link);
									xmlImagemCenario.xmlAttributes.add("id_cenario", idCenario);
									
									xmlImagemCenario.markup(objCenario);
									
									intContador++;
								}
							}
						}
						//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
					}
				}
				else{
					alert("Diretorio nao existe '" + pluginPath + idJob + "_" + idPraca + "\\img");
				}
			}
			else{
				alert("Diretorio nao existe '" + pluginPath + barra + "job" + barra + idJob + "_" + idPraca + "'");
			}
		}
		else{
			alert("Informacoes corrompidas na tag 'jobinfo'");
		}
	}
	else{
		alert("Diretorio '" + pluginPath + "' nao encontrado");
	}
}
else{
	alert("Nenhum documento aberto");
}