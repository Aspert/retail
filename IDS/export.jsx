// Alexandre de Silva Cunha
// alemac@mac.com
// 27/03/09 - 10:30 - v1
#include "glue code.jsx"
#include "functions.jsx"


var preview = app.scriptArgs.get("preview"); // para saber se coloca ou nao a marca d'agua no pdf
var exportacomo = app.scriptArgs.get("exportAs"); // formato para exportacao do arquivo gerado pelo indd: jpg, pdf, indd
var iddinamizacao = app.scriptArgs.get("iddinamizacao"); // c�digo da dinamizacao
var pagina = app.scriptArgs.get("pagina"); // pagina
var paginas = app.scriptArgs.get("paginas"); // total de paginas do arquivo
var registro = app.scriptArgs.get("registro"); // registros

var codigo = iddinamizacao + "_" + pagina + "_" + registro;
var site = app.scriptArgs.get("site");

baseIndd = app.scriptArgs.get("caminho");

baseXml =  "/" + site + "/vod/files/xml/" + codigo + ".xml";
objDocument = app.open(File(baseIndd));

expotFileIndd = "/" + site + "/vod/files/preview/" + codigo + ".indd";
expotFileJpg = "/" + site + "/vod/files/preview/" + codigo +  ".jpg";
expotFilePdf = "/" + site + "/vod/files/preview/" + codigo + ".pdf";

try{
	objDocument.zeroPoint = [0, 0];

	for(ii = 0; ii <= 30; ii++){
		for(gg = 0; gg <= objDocument.groups.length -1; gg++){
			objDocument.groups.item(gg).ungroup();
		}
	}

	// DEFINE AS UNIDADES DE MEDIDA A SER USADA EM POINTS (PIXELS)
	objDocument.viewPreferences.horizontalMeasurementUnits = MeasurementUnits.points;
	objDocument.viewPreferences.verticalMeasurementUnits = MeasurementUnits.points;

	// IMPORTA O XML RECEBIDO COMO PARAMETRO
	objDocument.importXML(File(baseXml));

	for(rr = 0; rr <= objDocument.rectangles.length - 1; rr++){
		object1 = objDocument.rectangles.item(rr);
		objXmlElement = object1.associatedXMLElement;

		if(objXmlElement){
			object1.fit(1668247152); // amplia
			object1.fit(1667591779); // centraliza
		}
	}

	if(paginas>1){
		exportacomo="pdf";
	}

	if(exportacomo == "pdf"){
		if(preview=="true"){
			marcadagua(app.scriptArgs.get("marca"));
			//myPDFExportPreset = app.pdfExportPresets.item("[Smallest File Size]");
			myPDFExportPreset = app.pdfExportPresets.item("[PDF/X-1a:2001]");
		} else {
			myPDFExportPreset = app.pdfExportPresets.item("[High Quality Print]");
		}
		objDocument.exportFile(ExportFormat.pdfType, new File(expotFilePdf), myPDFExportPreset);
	} else if(exportacomo == "jpg"){
		marcadagua(app.scriptArgs.get("marca"));
		app.jpegExportPreferences.jpegQuality = JPEGOptionsQuality.maximum;
		app.jpegExportPreferences.resolution = 150;
		objDocument.exportFile(1246775072, new File(expotFileJpg));
		//objDocument.save(new File(expotFileIndd));
	} else if(exportacomo == "indd"){
		objDocument.save(new File(expotFileIndd));
	}

	app.documents.item(0).close();

	exportacomo;

	//File(filetemp).remove();
}

catch(e){
	// FECHA O ARQUIVO INDD //
	app.documents.item(0).close();
	//File(filetemp).remove();
	"ERRO: " + e.description;
}