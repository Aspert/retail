//RECEBE OS PARAMETROS --------------------------------------------------------------------------------------------------------------------------------------------------
var baseIndd = decodeURIComponent(app.scriptArgs.get("destino"));
var pid = app.scriptArgs.get("pid");
var pathLog = decodeURIComponent(app.scriptArgs.get("path_log").replace("+", " "));
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

//FUNCAO RIGHT IGUAL A DO VB --------------------------------------------------------------------------------------------------------------------------------------------
function right(str, n){
	if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

//CRIA UM ARQUIVO DE LOGPID PARA IDENTIFICAR O RESULTADO DO PROCESSAMENTO PARA ------------------------------------------------------------------------------------------
function logPID(dirPath, pid, status, message){	
	if(status == 0){
		status = "fail";
	}else{
		status = "ok";
	}

	status = status.toLowerCase();
	
	var logFile = new File(dirPath + "/"+pid+"."+status);
	try{
		logFile.open("w");
		if(message){
			logFile.write(message);
		}
	}catch(e){
		throw e;
	}finally{
		if(logFile){
			logFile.close();
		}
	}
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

var pdfTemp = "/Library/WebServer/Documents/IDS/temp/" + Math.round((Math.random()*9)+1) + ".pdf";

var objArquivo = new File(baseIndd);

if(right(baseIndd, 4) == "indd"){
	if(objArquivo.exists){
		try{
			var objDocumento = app.open(objArquivo);

			pdfExportPreset = app.pdfExportPresets.item("[Smallest File Size]");
			objDocumento.exportFile(ExportFormat.pdfType, new File(baseIndd.replace(".indd", ".pdf")), pdfExportPreset);

			app.documents.item(0).close();

			logPID(pathLog, pid, 1);
		}
		catch(e){
			app.documents.item(0).close();
			logPID(pathLog, pid, 0, e.description);
		}
	}
	else{
		logPID(pathLog, pid, 0, "O arquivo indd nao existe");
	}
}
else{
	logPID(pathLog, pid, 0, "Parametro passado nao e um arquivo InDesign");
}