var Util = new Class({
	vazio: function() {},

	options: {
		base_url:'',
		site_url:'',
		suffix_url:'',
		method:'get'
	},


	mouseover: function(obj)
      {
            obj.bgColor='#191919';
            obj.style.cursor='pointer';
      },

	mouseout: function(obj)
      {
            obj.bgColor='#111111';
            obj.style.cursor='pointer';
      },

	call: function(uri, data, response, method)
	{
		var site_url = this.options.site_url;

		if (site_url.substr(site_url.length-1, 1) != '/' )
			site_url+= '/';

		var url = site_url + 'json/' + uri + this.options.suffix_url;

		response = (response!=null) ?response :this.vazio;
		method = (method!=null) ?method :this.options.method;

		new Json.Remote(url, {
			'onComplete': response,
			'method': method
		}).send(data);
		/*new Json.Remote(url, {
			'onComplete': function(){alert('onComplete')},
			'onSuccess': function(){alert('onSuccess')},
			'method': method
		}).send(data);*/
	},


	pesquisaCompleta: function()
	{
		$('pesquisa_completo').style.display='block';
		$('pesquisa_simples').style.display='none';

	},

	pesquisaSimples: function()
	{
		$('pesquisa_completo').style.display='none';
		$('pesquisa_simples').style.display='block';
	},

	img_size: function(id)
	{
		var img = $(id);

		if(img){
			var largura = img.width;
			var altura = img.height;
			if (altura>largura) {
				img.height = 60;
			} else {
				img.width = 60;
			}
		}
	},

	confirmBox: function (msg,go,submit,email)
	{
		if(email)
			$('EMAIL').value = 'envia';
		else
			$('EMAIL').value = '';

		if (confirm(msg))
		{
			if (go=='submit')
				document.forms[submit].submit();
			 else
				location.href=go;
		}
	},

	confirmBoxId: function (msg,go,form)
	{

		if (confirm(msg))
		{
			if (go=='submit')
				$(form).submit();
			 else
				location.href=go;
		}
	},

	doPaginacao: function(url, totalPagina, paginaAtual, nIntervalo)
	{
		if ( totalPagina <= 1) return "";

		nIntervalo = (nIntervalo!=null) ?nIntervalo :20;

		if ( totalPagina < nIntervalo)
		{
			_inicio = 0;
			_final = totalPagina;
		}
		else
		{
			_inicio = paginaAtual - (Math.floor(nIntervalo/2));
			_final = _inicio + nIntervalo;

			if ( _inicio < 0 )
			{
				_inicio = 0;
				_final = nIntervalo;
			}
			if ( _final > totalPagina )
			{
				_final = totalPagina;
				_inicio = totalPagina-nIntervalo;
			}
		}

		var saida = '';

		saida+= '<table border="0"><tr><td align="right" width="130">';

		if ( paginaAtual != 0 )
			saida+= url.replace('#PAGINA#', '0').replace('#LABEL#', 'primeiro') + ' | ' + url.replace('#PAGINA#', (paginaAtual-1)).replace('#LABEL#', 'anterior');
		else
			saida+= 'primeiro | anterior';

		saida+= '</td><td>';
		for ( i = _inicio; i < _final; i++ )
			saida+= ( (paginaAtual==i) ?"<b>" + (i+1) + "</b> " :url.replace('#PAGINA#', i).replace('#LABEL#', (i+1)) ) + ' ';
		saida+= '</td><td align="left" width="130">';

		if ( paginaAtual != totalPagina-1 )
			saida+= url.replace('#PAGINA#', (parseInt(paginaAtual)+1)).replace('#LABEL#', 'pr&oacute;ximo') + ' | ' + url.replace('#PAGINA#', (totalPagina-1)).replace('#LABEL#', '&uacute;ltimo');
		else
			saida+= 'pr&oacute;ximo | &uacute;ltimo';

		saida+= '</td></tr></table>';

		return saida;
	},

 	newimg_size: function(id)
 	{
		var largura = id.width;
		var altura = id.height;
		if (altura>largura){
			id.height = 60;
		} else {
			id.width = 60;
		}
 	},

	soNums: function(e)
    {
        if (document.all){var evt=event.keyCode;} // caso seja IE
        else{var evt = e.charCode;}    // do contrrio deve ser Mozilla
        var valid_chars = '0123456789';    // criando a lista de teclas permitidas
        var chr= String.fromCharCode(evt);    // pegando a tecla digitada
        if (valid_chars.indexOf(chr)>-1 ){return true;}    // se a tecla estiver na lista de permisso permite-a
        // para permitir teclas como <BACKSPACE> adicionamos uma permisso para
        // cdigos de tecla menores que 09 por exemplo (geralmente uso menores que 20)
        if (valid_chars.indexOf(chr)>-1 || evt < 9){return true;}    // se a tecla estiver na lista de permisso permite-a
        return false;    // do contrrio nega
    },

    formatCurrency: function(o, n, dig, dec)
    {

		o.c = !isNaN(n) ? Math.abs(n) : 2;
		o.dec = typeof dec != "string" ? "," : dec, o.dig = typeof dig != "string" ? "." : dig;
		new Util().addEvent(o, "keypress", function(e){
			if(e.key > 47 && e.key < 58){
				var o, s, l = (s = ((o = this).value.replace(/^0+/g, "") + String.fromCharCode(e.key)).replace(/\D/g, "")).length, n;
				if(o.maxLength + 1 && l >= o.maxLength) return false;
				l <= (n = o.c) && (s = new Array(n - l + 2).join("0") + s);
				for(var i = (l = (s = s.split("")).length) - n; (i -= 3) > 0; s[i - 1] += o.dig);
				n && n < l && (s[l - ++n] += o.dec);
				o.value = s.join("");
			}
			e.key > 30 && e.preventDefault();
		});
    },

     addEvent: function(o, e, f, s)
    {
		var r = o[r = "_" + (e = "on" + e)] = o[r] || (o[e] ? [[o[e], o]] : []), a, c, d;
		r[r.length] = [f, s || o], o[e] = function(e)
		{
			try{
				(e = e || event).preventDefault || (e.preventDefault = function(){e.returnValue = false;});
				e.stopPropagation || (e.stopPropagation = function(){e.cancelBubble = true;});
				e.target || (e.target = e.srcElement || null);
				e.key = (e.which + 1 || e.keyCode + 1) - 1 || 0;
			}catch(f){}
			for(d = 1, f = r.length; f; r[--f] && (a = r[f][0], o = r[f][1], a.call ? c = a.call(o, e) : (o._ = a, c = o._(e), o._ = null), d &= c !== false));
			return e = null, !!d;
	        }
    },
   formatarmoeda: function(num){

		var erro = 0;
		valor = !isNaN(num) ? Math.abs(num) : erro++;

		if(num<=0)
		{
			erro++;
		}

		stringnum = num.toString();

		size = stringnum.length;
		if(size<=2)
		{
			return stringnum;
		}else{
			digito = stringnum.substr(size-2,size);
			resto = stringnum.substr(0,size-2);

			for (var i = 0; i < Math.floor((resto.length-(1+i))/3); i++)
			{
				resto = resto.substring(0,resto.length-(4*i+3))+"."+resto.substring(resto.length-(4*i+3));
			}
			total = resto+","+digito;

		}

		if(erro>0)
		{
			return "falso";
		}else{
			return total;
		}
   },

   movimento: function(elemento, direcao)
   {
        var sel = document.getElementById(elemento);
        var len, i;
        if (!sel) {
            return;
        }
        if (direcao == 'passar' && arguments[2] == undefined) {
            return;
        } else if (direcao == 'passar') {
            var sel_pai = document.getElementById(arguments[2]);
            var selecionados = new Array();
            if (!sel_pai) {
                return;
            }
            len = sel_pai.options.length;
            for (i = 0; i < len; i++) {
                if (sel_pai.options[i].selected) {
                    sel.options[sel.options.length] = new Option(sel_pai.options[i].text, sel_pai.options[i].value);
                    selecionados.push(i);
                }
            }
            len = selecionados.length;
            for (i = len-1; i >= 0; i--) {
                sel_pai.options[selecionados[i]] = null;
            }
        } else if (direcao == 'cima' || direcao == 'baixo') {
            var selecionado = sel.selectedIndex;
            var comparacao = direcao == 'cima' ? selecionado - 1 : selecionado;
            var opts_values = new Array();
            var opts_texts = new Array();
            var tam = sel.options.length;
            var i;
            if (selecionado == -1) {
                return;
            }
            if (direcao == 'cima' && selecionado == 0) {
                return;
            }
            if (direcao == 'baixo' && selecionado == tam - 1) {
                return;
            }
            selecionado = direcao == 'cima' ? selecionado - 1 : selecionado + 1;
            for (i = 0; i < sel.options.length; i++) {
                if (i == comparacao) {
                    opts_values.push(sel.options[i+1].value);
                    opts_texts.push(sel.options[i+1].text);
                    sel.options[i + 1] = null;
                }
                opts_values.push(sel.options[i].value);
                opts_texts.push(sel.options[i].text);
            }
            for (i = 0; i < tam; i++) {
                sel.options[i] = new Option(opts_texts[i], opts_values[i]);
            }
            sel.selectedIndex = selecionado;
        }
	},

	selectMult: function(sel_pai,frm)
	{
	    len = document.getElementById(sel_pai).options.length;

	    if(len==0){
	    	alert("Campo Obrigatorio");
	    	return false;
	    }

	    for (i = 0; i < len; i++) {
	        document.getElementById(sel_pai).options[i].selected="true";
	        //alert(document.getElementById(sel_pai).options[i].value);
	    }
	    document.getElementById("frm").submit();
	},
	displayColuna: function(id)
    {
    	if(id == 1)
    		$('coluna').style.display = '';
    	else
    	{
    		$('coluna').style.display = 'none';
    		$('DESC_COLUNA').value = '';
    	}
    },

    typeAll: function()
    {
		if($('sel_all').value == '')
		{
			var categorias = $$('.qtd_categoria');
			for ( var i = 0; i < categorias.length; i++ )
				categorias[i].value = 1;
				$('sel_all').value = 1;
		}
		else
		{
			var categorias = $$('.qtd_categoria');
			for ( var i = 0; i < categorias.length; i++ )
				categorias[i].value = '';
				$('sel_all').value = '';
		}

    },
    alteraDisplay: function(id, visible)
    {
    	$(id).style.display = visible;
    },
    redirectPermissao: function (grupo)
    {
 		window.location.href = new Util().options.site_url + '/permissao/form/'+ grupo;
    }
});

function fazLocation()
{
	window.location=new Util().options.site_url+'checklist';
}

function clearSelect(el, numItemsLeft){
	var itens = $(el).getElements('option');
	var total = itens.length;
	for(var i=total-1; i>=numItemsLeft; i--){
		$(itens[i]).remove();
	}
}

function clearSelectGroup(el, numItemsLeft){
	var itens = $(el).getElements('optgroup');
	var total = itens.length;
	for(var i=total-1; i>=numItemsLeft; i--){
		$(itens[i]).remove();
	}
}
function montaOptions(el, lista, chave, valor, select,filtra = false){
	clearSelect(el, 1);
	var valorAtt = 0;
	if(filtra){
		if(lista.length == 1){
			$each(lista, function(item){
				var opt = document.createElement('option');
				el.options.add(opt);
				opt.text = item[valor];
				opt.value = item[chave];
				opt.selected = true;
				valorAtt = item[chave];
			});
			
			return valorAtt;
		}else{
			$each(lista, function(item){
				var opt = document.createElement('option');
				el.options.add(opt);
				opt.text = item[valor];
				opt.value = item[chave];
		
				if(select == item[chave]){
					opt.selected = true;
				}
			});
			return valorAtt;
		}
	}else{
		$each(lista, function(item){
			var opt = document.createElement('option');
			el.options.add(opt);
			opt.text = item[valor];
			opt.value = item[chave];
	
			if(select == item[chave]){
				opt.selected = true;
			}
		});
		return valorAtt;
	}
	
}

function montaOptionsAjax(el, url, data, chave, label, select,filtra = false){
	var valor = 0;
	new Ajax(url, {
		method: 'post',
		async: false,
		onComplete: function(json){
			var lista = eval(json);
			montaOptions(el, lista, chave, label, select,filtra);
			
		}

	}).request(data);

}

function montaOptionsGroup(el, lista, chave, valor, sub, subLabel, blank){
	clearSelect(el, 1);
	clearSelectGroup(el, 1);
	
	if (blank) {
		var optblank = document.createElement('option');
		optblank.text = '';
		optblank.value = '';
		el.options.add(optblank);
	}
	
	$each(lista, function(grupo){
		
		var optGroup = document.createElement('optgroup');
		optGroup.label = grupo[subLabel];
		
		$each(grupo[sub], function(item){
			var opt = document.createElement('option');
			opt.text = item[valor];
			opt.value = item[chave];
			optGroup.appendChild(opt);
		});
		
		el.appendChild(optGroup);
	});
}

function montaOptionsGroupAjax(el, url, data, chave, label, sub, subLabel, blank){
	new Ajax(url, {
		method: 'post',
		onComplete: function(json){
		var lista = eval(json);
		montaOptionsGroup(el, lista, chave, label, sub, subLabel, blank);
	}
	}).request(data);
}

function montaCheckbox(el, lista, name,chaveLabel,chaveValue,checkField){
	$j.each(lista, function(){
		var lb = $j('<label></label>');
		lb.text(this[chaveLabel]);
		var chk = $j('<input type="checkbox" />');
		chk.attr('name',name);
		chk.attr('value',this[chaveValue]);
		if(this[checkField]){
			chk.attr('checked',true);
		}

		el.append(chk);
		el.append(lb);
		el.append('<br />');

	});
}

function montaCheckboxAjax(url, data, el, name, chaveLabel, chaveValue){
	$.post(url, data,
		   function(json,status){
				var lista = eval(json);
				montaCheckbox(el, lista, name, chaveLabel, chaveValue);
				alert('rolou');
		   }, "json");
}

function printSelection(node){
	var content=node.innerHTML
	var pwin=window.open('','print_content','width=10,height=10');

	pwin.document.open();
	pwin.document.write('<html><body onload="window.print()">'+content+'</body></html>');
	pwin.document.close();

	setTimeout(function(){pwin.close();},1000);
}


/**
 * classe para carregamento sequencial e por blocos
 */
function StackLoader(url, params){
	// parametros para enviar via post
	this.params           = params;
	// url de requisicao
	this.url              = url;
	// container que vai receber o conteudo da primeira vez que carregar
	this.firstContainer   = null;
	// container que vai receber o conteudo das outras vezes
	this.defaultContainer = null;
	// limit de registros por ciclo
	this.limit            = 30;
	// registro inicial
	this.offset           = 0;

	// indica se o processo foi iniciado
	this._started         = false;
	// indica se o processo esta parado
	this._stopped         = false;
	// checa se esta completo
	this._completed       = false;
}

StackLoader.prototype = {
	// inicia o processo de carregamento
	start:function(){
		this._load();
	},

	// carrega de um ponto especifico
	load: function(offset, limit){
		this._stopped = false;
		this._started = false;
		this._completed = false;
		this.offset = offset;
		this.limit = limit;
		this.loadNext();
	},

	// carrega o um bloco
	loadNext : function(){
		var _old = this.onCompleteBlock;
		this.onCompleteBlock = function(loader, container, html){
			_old.call(null, loader, container, html);
			loader.onComplete.call(loader, loader);
			loader.onCompleteBlock = _old;
			return false;
		}

		this._load();
	},

	isComplete : function(){
		return this._completed;
	},

	// metodo interno de carregamento do conteudo
	_load: function(){
		// se estiver parado
		if( this._stopped ){
			this._completed = true;
			// sai da rotina
			return;
		}

		if( this.onStart(this) == false ){
			return;
		}

		// dados que vao para o post
		var params = {};
		// para cada dado padrao
		for(var str in this.params){
			// adiciona na lista que vai para o post
			params[str] = this.params[str];
		}
		// adiciona o limite
		params.limit  = this.limit;
		// adiciona o offset
		params.offset = this.offset;

		// referencia a este objeto
		var ref = this;
		// envia por post usando jquery
		$j.post(this.url, params, function(html){
			// se retornar uma string vazia
			if( html == '' ){
				// terminou o carregamento
				ref._completed = true;
				ref.onComplete(ref);
				return;
			}

			// aumenta o offset
			ref.offset += ref.limit;

			// indica se e para carregar o proximo bloco
			var continuar = true;
			// se ainda nao iniciou (nao houve nenhum carregamento anterior)
			if( !ref._started ){
				// se o container inicial nao for null
				if( ref.firstContainer != null ){
					// adiciona o conteudo a ele
					$j(html).appendTo( ref.firstContainer );
				}
				// indica que ja iniciou
				ref._started = true;
				// marca a flag de continuar conforme a resposta do callback
				continuar = ref.onCompleteBlock(ref, $j(ref.firstContainer), html);

			// se nao e a primeira vez que esta carregando
			} else {
				// se o container padrao nao e nulo
				if( ref.defaultContainer != null ){
					// carrega o conteudo no container padrao
					$j(html).appendTo(ref.defaultContainer);
				}
				// marca a flag de continuar conforme a resposta do callback
				continuar = ref.onCompleteBlock(ref, $j(ref.defaultContainer), html);
			}

			// se for para continuar
			if( continuar ){
				// chama o carregamento novamente
				ref._load();
			}
		});
	},

	// callback
	onStart: function(loader){ return true; },
	onCompleteBlock: function(loader, container, html){return true;},
	onComplete: function(loader){}
}

function configuraPauta(field, direction){
	var options =
		{
			columns:[{col:'.numeroJob',label:'TB_JOB.ID_JOB'},
				{col:'.codigoJob',label:'B.ID_JOB'},
				{col:'.tituloJob',label:'TITULO_JOB'},
				{col:'.inicioProcessoJob',label:'DATA_INICIO_PROCESSO'},
				{col:'.inicioJob',label:'DATA_INICIO'},
				{col:'.terminoJob',label:'DATA_TERMINO'},
				{col:'.agenciaJob',label:'DESC_AGENCIA'},
				{col:'.clienteJob',label:'DESC_CLIENTE'},
				{col:'.bandeiraJob',label:'DESC_PRODUTO'},
				{col:'.campanhaJob',label:'DESC_CAMPANHA'},
				{col:'.mudancaJob',label:'DATA_MUDANCA_ETAPA'},
				{col:'.situacaoJob',label:'DESC_STATUS'},
				{col:'.tipopecaJob',label:'DESC_TIPO_PECA'},
				{col:'.tipoJob',label:'DESC_TIPO_JOB'},
				{col:'.etapaJob',label:'DESC_ETAPA'}
			],
		form:'form',
		orderDirection: direction,
		selectedField: field
		}

	$j('.tableMidia').sortgrid(options);
}


// carrega os tipos de peca de um cliente
// e adiciona as mascaras no objeto de mascaras
function carregaTiposPeca(idcliente, callback){
	// limpamos as mascaras anteriores
	mascaras = {};

	// tira a mascara do campo
	$j('#TITULO_JOB').unmask();

	// faz a consulta
	$j.post(util.options.site_url + 'json/admin/getTipoPecaCliente', 'id='+idcliente, function(json){

		// recarregamos as mascaras
		$j.each(json, function(){
			// colocamos o tipo na lista de mascaras
			mascaras[this.ID_TIPO_PECA] = {mascara: this.MASCARA, tip: this.TEXTO_EXPLICATIVO};
		});

		// se o usuario definiu um callback
		if( callback ){

			// chama o callback
			callback.apply(this, [json]);
		}

	},'json');
}

// muda a mascara do campo do titulo do job
function mudaMascara( idTipoPeca ){
	// tira a mascara do campo
	$j('#TITULO_JOB').unmask();

	// tira o texto explicativo
	$j('.TEXTO_EXPLICATIVO').html('');

	if( idTipoPeca != '' && mascaras.hasOwnProperty(idTipoPeca) && mascaras[ idTipoPeca ].mascara != '' ){
		$j('#TITULO_JOB').mask( mascaras[ idTipoPeca ].mascara );
		$j('.TEXTO_EXPLICATIVO').html( mascaras[ idTipoPeca ].tip );
	}
}

function adicionarMascaras(){
	$j.mask.addPlaceholder("*","(\\s|[A-Za-z0-9])");
	$j.mask.addPlaceholder("#","[0-9]");
	$j.mask.removePlaceholder('a');
}

// operacoes padrao para qualquer tela
// inicializadas quando terminar de iniciar os elementos DOM
jQuery(function($){
	var criado = false;
	var div = null;
	var msg = '';
	$('.information').mouseover(function(){
		div = criado ? $('#information_div_tooltip') : $('<div id="information_div_tooltip"></div>').appendTo(document.body);
		criado = true;

		div.html( $(this).attr('alt') )
			.css('left', $(this).offset().left )
			.css('top', $(this).offset().top + $(this).height() )
			.show();
			;

		$(this).attr('alt','');

		return false;

	}).mouseout(function(){
		$(this).attr('alt', div.html());
		div.hide();

		return false;
	});
});


function visualizarProcesso(idprocesso){
	removeDivModal();
	getDivModal()
		.html('Carregando')
		.dialog({
			modal: true,
			title: 'Visualizar Processo',
			width: 700,
			height: 500
		}).dialog('open');

	$j.post(util.options.site_url + 'json/processo/detalhes/' + idprocesso, null, function(html){
		getDivModal().html( html );
	});
}

String.prototype.substitute = function( list ){
	var m;
	var i=0;
	var ref = this;
	while( (m = ref.match(/\{(\w+)\}/)) ){
		if( list[ m[1] ] ){
			ref = ref.replace(m[0], list[ m[1] ]);

		} else {
			ref = ref.replace(m[0], '');

		}
	}

	return ref;
};

function toggleImgFilha(td){
	var img = td.find('img');
	if(img.size() > 0){
		var src = img.attr('src');
		if(src.indexOf('close') >= 0){
			img.attr('src',src.replace('close','open'));
		}else{
			img.attr('src',src.replace('open','close'));
		}
	}
}

function checkDate(digData) {
    var bissexto = 0;
    var data = digData;
    var tam = data.length;
    if (tam == 10) {
        var dia = data.substr(0, 2)
        var mes = data.substr(3, 2)
        var ano = data.substr(6, 4)
        if (ano > 1902 && ano < 2100 && dia > 0 && dia <= 31 && mes > 0 && mes <= 12) {
            switch (mes) {
                case '01':
                case '03':
                case '05':
                case '07':
                case '08':
                case '10':
                case '12':
                    if (dia <= 31) {
                        return true;
                    }
                    break

                case '04':
                case '06':
                case '09':
                case '11':
                    if (dia <= 30) {
                        return true;
                    }
                    break
                case '02':
                    /* Validando ano Bissexto / fevereiro / dia */
                    if ((ano % 4 == 0) || (ano % 100 == 0) || (ano % 400 == 0)) {
                        bissexto = 1;
                    }
                    if ((bissexto == 1) && (dia <= 29)) {
                        return true;
                    }
                    if ((bissexto != 1) && (dia <= 28)) {
                        return true;
                    }
                    break
            }
        }
    }
    alert("A Data " + data + " é inválida!");
    return false;
}

function refresh(){
	location.reload();
}