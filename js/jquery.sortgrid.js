/*
 * SortGrid - cria sistema de sort para tabela estaticas + php
 * Version: 0.5 (10/09/2009)
 * Copyright (c) 2009 Juliano Polito - julianopolito@gmail.com
 * Licensed under the MIT License: http://en.wikipedia.org/wiki/MIT_License
 * Requires: jQuery v1.3+
*/

(function($){
	$.fn.sortgrid = function(settings){	
	
		$.fn.sortgrid.options = {field:'ORDER',
								directionField:'ORDER_DIRECTION',
								orderDirection: 'ASC'
								};
		
		var option = $.extend($.fn.sortgrid.options, settings);
		$.fn.sortgrid.options = option;
		
		for (var i = 0; i < option.columns.length; i++){
			
			var col = option.columns[i];
			var colObject = this.find(col.col);
			
			if(colObject.length > 0){
				//associa os objetos a coluna atual
				colObject.data('col',col);
				//var link = $("<a href='javascript:undefined'></a>");
				if(col.label == option.selectedField){
					//colObject.css('background-color','#FFFF00');
					//link.addClass('sortgrid_selectedLink');	
					colObject.wrapInner("<b></b>");
					if(option.orderDirection.toUpperCase() == 'ASC'){
						colObject.addClass('sortgrid_selectedHeaderUp');
					}else if(option.orderDirection.toUpperCase() == 'DESC'){
						colObject.addClass('sortgrid_selectedHeaderDown');
					}
					
					// colocamos o campo hidden para ordenacao
					var inputField = $j('#'+option.field);	
					if(inputField.length == 0){
						inputField = $j('<input type="hidden" />');
						inputField.attr('id', option.field);
						$(option.form).append(inputField);
					}
					inputField.attr('name', option.field);
					inputField.val(col.label);
					
					// ordenacao escolhida
					var inputDir = $j('#'+option.directionField);
					if(inputDir.length == 0){
						inputDir = $j('<input type="hidden" />');
						inputDir.attr('id',option.directionField);
						$(option.form).append(inputDir);
					}
					inputDir.attr('name',option.directionField);
					inputDir.val(option.orderDirection.toUpperCase());
					
				}else{
					//link.addClass('sortgrid_link');
					colObject.addClass('sortgrid_normalHeader');
				}//end if
				
				//Link para a coluna
				colObject
					//.wrapInner(link)
					.click(
						function(){
							var option = $.fn.sortgrid.options;
							var col = $j(this).data('col');
							var form = $j(option.form);
							
							var inputField = $j('#'+option.field);	
							if(inputField.length == 0){
								inputField = $j('<input type="hidden" />');
								inputField.attr('id', option.field);
								form.append(inputField);
							}
							inputField.attr('name', option.field);
							inputField.val(col.label);
							
							var inputDir = $j('#'+option.directionField);
							if(inputDir.length == 0){
								inputDir = $j('<input type="hidden" />');
								inputDir.attr('id',option.directionField);
								form.append(inputDir);
							}
							inputDir.attr('name',option.directionField);
							if(col.label == option.selectedField){
								if(option.orderDirection.toUpperCase() == 'ASC'){
									option.orderDirection = 'DESC';
								}else if(option.orderDirection.toUpperCase() == 'DESC'){
									option.orderDirection = 'ASC';
								}
							}else{
								option.orderDirection = 'ASC';
							}
							inputDir.val(option.orderDirection);
							
							form.submit();
						}//end function
					);//end click
			}//end if
		}//end for
	};//end sortgrid
	
	function debug($obj) {
		if (window.console && window.console.log){
			for (var i in $obj){
				window.console.log(i+':'+$obj[i]);
			}
		}else{
			var msg = '';
			for (var i in $obj){
				msg += i + ':' + $obj[i] + '\n';
			}
		}
	};
	
})(jQuery);