var Ficha = new Class({
	
	formPesquisa: function(response)
	{			
		
		var categorias = response['categorias'];
		
		var option = '';
		
		for ( var i = 0; i < categorias.length ; i++ )
		{										
			option += '<option value="' + categorias[i]['ID_CATEGORIA'] + '">'; 
			option += categorias[i]['DESC_CATEGORIA'] ;
			option += '</option>' ;					
		}
		
		var content = ''+
			'<div style="width:800px;height:500px;overflow:auto;margin:5px;"><div class="titulo_view">Pesquisa de Ficha</div><br/><br />' +
			'<div class="titulo_form">Tipo de ficha:</div>' +
			'<select class="inputs" id="TIPO_COMBO_FICHA" >' +				
				'<option  value="1">Produto</option>' +
				'<option  value="2">Promo&#231;&#227;o</option>' + 	
			'</select><br />' +			
			'<div class="titulo_form">Nome:</div><input type="text" class="inputs" id="NOME_FICHA" /><br/>' +
			'<div class="titulo_form">C&oacute;digo Produto:</div><input type="text" class="inputs" id="COD_FICHA" /><br/>' +
			'<div class="titulo_form">Categoria:</div>' +
			'<select class="inputs" id="ID_CATEGORIA" onChange="new Categoria().getAllSub(new Categoria().responseSub , this.value, \'_ID_SUBCATEGORIA\');">' +				
				'<option selected="selected" value="">[Selecione]</option>' + 				
					option + 												
			'</select><br />' +
			'<div class="titulo_form">Subcategoria:</div>' +
			'<select class="inputs" id="_ID_SUBCATEGORIA">'+
				'<option selected="selected" value="">[Selecione]</option>' + 				
			'</select> <br/>' +						
			'<a href="javascript:new Util().vazio()" onClick="new Ficha().pesquisar()"><img src="'+ new Util().options.base_url +'img/lupa.gif"  alt="Confimar"  title="Pesquisar" class="pesquisa_add" border="0" /></a> ' +
			'<a href="javascript:new Util().vazio()" onClick="closeMessage();selecaoFichas = []"><img src="'+ new Util().options.base_url +'img/cancelar.gif" border="0"/></a>' +
			'<br style="clear:both">' +
			'<div class="titulo_view">Sele&ccedil;&atilde;o de fichas</div>' +
			'<div id="resultado"></div></div>';	
		
		displayStaticMessage(content);
	},

		
	
	/* Essa funcao chama o controlle CARRINHO para pesquisa  */
	pesquisar: function(pagina)
	{				
		var inputs = $$('.inputs');
		
		var carrinho_id = $('carrinho_id').value;		
		
		var content = '{ID_CARRINHO:'+carrinho_id+',';
		
		for ( var i = 0; i < inputs.length; i++ )
			content+= ( (i==0) ?'' :',' ) + '\'' + inputs[i].id + '\':' + '\'' + inputs[i].value + '\'';		
			
		if(pagina != null)
			content+= ',\'pagina\':\'' + pagina + '\'';
		
		content+= '}';
				
		eval('var data = ' + content);
		
		new Util().call('ficha/pesquisar', data, this.responsePesquisar, 'post');		
	},
	
	/* Monta a tabela com o resultado da pesquisa no modal */
	responsePesquisar: function(response)
	{		
		var fichas = response['fichas'];
		var totalPagina = response['totalPagina'];
		var pagina = response['pagina'];
		var linhas = '';
		
		if(fichas)
		{
		for( var i= 0; i < fichas.length ; i++ )
		{													
			linhas+='<tr>';	
			linhas+='<td align="center"><input type="checkbox" class="selecao" value="' + fichas[i]['ID_FICHA'] +' " ></td>';
			linhas+='<td align="center"><img src="'+fichas[i]['THUMB_OBJ_FICHA']+'" border="0" onload="new Util().newimg_size(\'id'+i+'\');" id="id'+i+'" name="id'+i+'" /></td>';			
			linhas+='<td>' + fichas[i]['COD_FICHA'] + ' - '+ fichas[i]['NOME_FICHA'] + '</td>';
			linhas+='<td>' + fichas[i]['DESC_CATEGORIA'] + '</td>';
			linhas+='<td>' + fichas[i]['PV_ENC_FICHA'] + '</td>';							
			linhas+='</tr>';
		}
		
		var content = '' +							
			'<table>' +			
				'<tr>' +
					'<th>Selecionar</th>' +
					'<th>Produto</th>' +					
					'<th>Ficha</th>' +
					'<th>Categoria</th>' +
					'<th>PV</th>' +											
					'<th>FLAG</th>' +											
				'</tr>' +
				linhas +
			'</table>' +
			'<br/>' +
			'<span class="paginacao" id="paginacao"></span>' +
			'<img src="'+ new Util().options.base_url +'img/check.gif" class="pesquisa" alt="Confirmar"  title="Confirmar" onClick="new Ficha().getSelected();"/>' +			
		'';			
		$('resultado').innerHTML = content;
		
		$('paginacao').innerHTML = new Util().doPaginacao('<a href="javascript:new Util().vazio()" onClick="new Ficha().pesquisar(#PAGINA#)">#LABEL#</a>', totalPagina, pagina);
			
		}else{
			$('resultado').innerHTML = 'Nenhum resultado encontrado';
		}
		
		$each($$(".selecao"), function (o){
			//registra um clickhandler para os checkboxes
			//adicionando a ficha a um array selecaoFichas
			o.onclick = function(e){
				selectFicha(e);
			};
			
			//seleciona os checkboxes que estao no array
			o.checked = indexOf(selecaoFichas,o.value) != null;
			
		});
		
		
		
	},
	
	/* Monta um array de ficha selecionada para montar a lista no formulario de insercao */	
	getSelected: function()
	{		
		
		//fazemos um getBySearch passando as fichas selecionadas em multiplas paginas que agora estao
		//no array selecaoFichas
		new Util().call('ficha/getBySearch', {'fichas':selecaoFichas}, this.resultSearch);
		//limpa a selecao apos envio
		selecaoFichas = [];
	},
	resultSearch:function(response)
	{
		var selecao = $$('.fcp');
		var setado = 0;
		
		var fichas = response['fichas'];
		for( var i= 0; i < fichas.length ; i++ )
		{
			
			 if($('cat_'+fichas[i]['ID_CATEGORIA']))
			 {
				 if($('cat_'+fichas[i]['ID_CATEGORIA']).value==0)
				 {
				 	alert('Ficha ja foi completada para esta categoria');
				 	setado++;
				 }			 	
			 }
			for ( var j = 0; j < selecao.length; j++ )
			{				
				if(selecao[j].value==fichas[i]['ID_FICHA'])
				{
					alert('Ficha ja foi selecionada');
					setado++;			
				}								 					 
			}
			
			if(setado==0)
			{
				
				if($('cat_'+fichas[i]['ID_CATEGORIA']).value>0)
				{					
					var oldvalue = $('cat_'+fichas[i]['ID_CATEGORIA']).value;
					$('cat_'+fichas[i]['ID_CATEGORIA']).value = oldvalue-1; 
				}
				
				var tr = document.createElement('TR');
				
				var td0 = document.createElement('TD');
				td0.align = "center";
				td0.id = 'ficha_' + fichas[i]['ID_FICHA'];
				var prefix_field = 'FICHAS[' + fichas[i]['ID_FICHA'] + ']';
				td0.innerHTML = '<input type="hidden" class="fcp" value="'+fichas[i]['ID_FICHA']+'" />'+
								'<input type="hidden"  name="' + prefix_field + '[FICHA][ID_FICHA]" value="'+ fichas[i]['ID_FICHA']+ '" class="teste" />' +
								'<input type="hidden" name="' + prefix_field + '[F_C_P][ID_F_C_P]" value="" />' +
								'<input type="hidden" name="' + prefix_field + '[IMG][ID_IMG]" value="'+ fichas[i]['THUMB_OBJ_FICHA']+'" />' +
								'<img height="50" src="'+ fichas[i]['THUMB_OBJ_FICHA']+'" border="0"/>' +'';
				tr.appendChild(td0);
				
				var base_url = new Util().options.base_url;
				var td1 = document.createElement('TD');
				td1.align = "center";
				td1.innerHTML = '<a href="javascript:new Util().vazio()" onClick="new Ficha().detalheModal('+ fichas[i]['ID_FICHA'] +','+fichas[i]['TIPO_COMBO_FICHA']+')"><img src="' + base_url + 'img/visualizar.gif" border="0"/></a>';
				tr.appendChild(td1);			
	
				var td2 = document.createElement('TD');
				td2.align = "center";
				td2.innerHTML = '<a href="javascript:new Util().vazio()" onClick="new Ficha().deletecontent(this,'+fichas[i]['TIPO_COMBO_FICHA']+','+fichas[i]['ID_CATEGORIA']+')"><img src="'+ base_url + 'img/excluir.gif" border="0"/></a>';
				tr.appendChild(td2);
				
				var tdnew = document.createElement('TD');
				tdnew.align = "center";
				tdnew.innerHTML = '<a id="pricing_'+fichas[i]['ID_FICHA']+'" href="javascript:new Util().vazio()" onclick="javascript:new Pricing().formPricing('+"''"+','+fichas[i]['ID_FICHA']+','+$('carrinho_id').value+');"><img alt="Editar" title="Editar" src="'+ base_url + 'img/edit_pricing.gif" border="0"/></a>';
				tr.appendChild(tdnew);
	
				var td3 = document.createElement('TD');
				td3.align = "center";
				td3.innerHTML = fichas[i]['COD_FICHA'];
				tr.appendChild(td3);
		
				var td4 = document.createElement('TD');
				td4.innerHTML = fichas[i]['TITULO_ENC_FICHA'];
				tr.appendChild(td4);
				
				
				var td5 = document.createElement('TD');
				
				td5.innerHTML = '<input type="text" name="'+prefix_field+'[F_C_P][DESC_FICHA]" value="" />';
				tr.appendChild(td5); 
				
				var td6 = document.createElement('TD');
				td6.innerHTML = fichas[i]['DESC_CATEGORIA'];
				tr.appendChild(td6);
				
				$('lista_fichas').appendChild(tr);					
			}			
		}
		
		if(setado==0)
		{
			closeMessage();
		}
	},
	
	/* Monta a lista de objeto - resultado da pesquisa no Form  */
	mountListObjetos: function (response)
	{				
		
		var fichas = response['fichas'];
		var has_pricing = response['has_pricing_all'];
		
		
		for( var i= 0; i < fichas.length ; i++ )
		{																										
			var tr = document.createElement('TR');
			
			var td0 = document.createElement('TD');
			td0.align = "center";
			td0.id = 'ficha_' + fichas[i]['ID_FICHA'];
			
			var prefix_field = 'FICHAS[' + fichas[i]['ID_FICHA'] + ']';
			td0.innerHTML = '' +
				'<input type="hidden" name="' + prefix_field + '[FICHA][ID_FICHA]" value="'+ fichas[i]['ID_FICHA']+ '" />' +
				'<input type="hidden" name="' + prefix_field + '[F_C_P][ID_F_C_P]" value="" />' +
				'<input type="hidden" name="' + prefix_field + '[PRICING][ID_PRICING]" value="" />' +
			'<input type="hidden" name="' + prefix_field + '[IMG][ID_IMG]" value="'+ fichas[i]['THUMB_OBJ_FICHA']+'" />' +
				'<img height="50" src="'+ fichas[i]['THUMB_OBJ_FICHA']+'" border="0"/>' +
			'';
			tr.appendChild(td0);
			
			var base_url = new Util().options.base_url;
			
			var td1 = document.createElement('TD');
			td1.align = "center";
			td1.innerHTML = '<a href="javascript:new Util().vazio()" onClick="new Fichas().detalhe1('+ fichas[i]['ID_FICHA'] +')"><img src="' + base_url + 'img/visualizar.gif" border="0"/></a>';
			tr.appendChild(td1);
			
			var td2 = document.createElement('TD');
			td2.align = "center";
			td2.innerHTML = '<a href="javascript:new Util().vazio()" onClick="$(this.parentNode.parentNode).remove();"><img src="'+ base_url + 'img/excluir.gif" border="0"/></a>';
			tr.appendChild(td2);
			
			var td3 = document.createElement('TD');
			td3.innerHTML = '<input type="text"  style="border:none;"  name="' + prefix_field + '[NOME_FICHA]" value="'+fichas[i]['COD_FICHA'] +' - '+ fichas[i]['NOME_FICHA']+'">';
			tr.appendChild(td3);
	
			var td4 = document.createElement('TD');
			td4.innerHTML = '<select name="' + prefix_field + '[TIPO_FICHA]">'+
			'<option value="0">[Selecione]</option>'+
			'<option value="1">Compre</option>'+
			'<option value="2">Ganhe</option>'+
			'</select>';
			tr.appendChild(td4);
			
			
			$('lista_fichas').childNodes[0].appendChild(tr);
		}
	},
	
	detalhe: function(id_ficha)
	{				
		ficha = {'ID_FICHA':id_ficha};
		
		new Util().call('ficha/getByIdAndObjeto', ficha, this.mountDetalhe);
		
	},
		
	detalheModal: function(id_ficha,tipo)
	{
		ficha = {'ID_FICHA':id_ficha};
		if(tipo==1)
		{
			new Util().call('ficha/getByObjeto', ficha, this.mountDetalhe);
		}
		else
		{
			new Util().call('ficha/getByPromocao', ficha, this.mountDetalhePromocao);			
		}
	},
	mountDetalhePromocao: function(response)
	{		
	
		var fichas = response['fichas'];
		var ficha = response['ficha'];
		
		var linha = '';
		for( var i= 0; i < fichas.length ; i++ )
		{				
			alert('alooooooooooooooo');
			linha+='<tr>';		
			linha+='<td align="center"><img src="'+fichas[i]['THUMB_OBJ_FICHA']+'" border="0" onload="new Util().img_size(\'id'+i+'\');" id="id'+i+'" name="id'+i+'" /></td>';
			linha+='<td>'+ fichas[i]['FILE_OBJ_FICHA']+ '</td>';
			linha+='<td>'+ fichas[i]['FILE_OBJ_FICHA'] + '</td>';		
			linha+='<td>'+ fichas[i]['TIPO']+'</td>';
			linha+='</tr>';					
		}				
		
		content='';
		content='<div style="overflow:auto; height:500px;width:800px;margin:5px;">';
		content+='<div style="float:right;><a href="javascript:new Util().vazio()" onClick="closeMessage();"><img src="'+ new Util().options.base_url +'img/cancelar.gif"  alt="Sair"  title="Sair" style=" margin-right:5px;" border="0" /></a></div><br style="clear:both;"/>';
		content+='<div class="titulo_view">Detalhes Ficha da promocao</div>';
		content+='<div class="titulo_form">C&oacute;digo Produto:</div>'+ ficha['COD_FICHA']+'<br/><br/>';
		content+='<div class="titulo_form">Nome:</div>'+ ficha['NOME_FICHA']+' <br/><br/>';	
		content+='<div class="titulo_form">Data Cria&ccedil;&atilde;o:</div>'+ ficha['DT_INSERT_FICHA']+'<br/><br/>';
		//content+='<div class="titulo_form">Data Expira&ccedil;&atilde;o:</div>'+ ficha['DT_EXPIRA_FICHA']+'<br/><br/>';
		content+='<br/>';
		content+='<br/>';
		content+='<div class="titulo_view">Fichas da Ficha</div>';
		content+='<div id="lista" >';
		
		content+='<table border="0">';
		content+='<tr>';		
		content+='<th>Produto</th>';
		content+='<th>Nome</th>';
		content+='<th>C&oacute;digo Produto</th>';		
		content+='<th>Tipo</th>';		
		content+='</tr>';		
		content+=linha;
		content+='</tr>';
		content+='</table>';
		content+='</div>';
		
		displayStaticMessage(content);
	},
	deletecontent: function(obj,idficha,idcategoria)
	{
		if($('cat_'+idcategoria))
		{
			var oldvalue = $('cat_'+idcategoria).value;
			$('cat_'+idcategoria).value = oldvalue+1;			
		}
		$(obj.parentNode.parentNode).remove();
	},
	
	mountDetalhe: function(response)
	{		
		var objetos = response['objetos'];
		
		var ficha = response['ficha'];
		
		var linha = '';
		if(objetos){
			for( var i= 0; i < objetos.length ; i++ )
			{														
				linha+='<tr>';		
				linha+='<td align="center"><img src="'+objetos[i]['THUMB_OBJ_FICHA']+'" border="0" onload="new Util().img_size(\'id'+i+'\');" id="id'+i+'" name="id'+i+'" /></td>';
				linha+='<td>'+ objetos[i]['FILE_OBJ_FICHA']+ '</td>';
				linha+='<td>'+ objetos[i]['FILE_OBJ_FICHA'] + '</td>';		
				linha+='<td>'+ objetos[i]['TIPO']+'</td>';
				linha+='</tr>';					
			}				
		}		
		content='';
		content='<div style="overflow:auto; height:500px;width:800px;margin:5px;">';
		content+='<div style="float:right;><a href="javascript:new Util().vazio()" onClick="closeMessage();"><img src="'+ new Util().options.base_url +'img/cancelar.gif"  alt="Sair"  title="Sair" style=" margin-right:5px;" border="0" /></a></div><br style="clear:both;"/>';
		content+='<div class="titulo_view">Detalhes Ficha &nbsp;</div><br /><br />';		
		content+='<div class="titulo_form">Código Produto:</div>'+ ficha['COD_FICHA']+'<br/><br/>';
		content+='<div class="titulo_form">Nome:</div>'+ ficha['NOME_FICHA']+' <br/><br/>';						
		content+='<div class="titulo_form">Data Cria&ccedil;&atilde;o:</div>'+ ficha['DT_INSERT_FICHA']+'<br/><br/>';
		if(ficha['DESC_FABRICANTE'])
			content+='<div class="titulo_form">Fabricante:</div>'+ ficha['DESC_FABRICANTE']+'<br/><br/>';		
		if(ficha['DESC_MARCA'])
			content+='<div class="titulo_form">Marca:</div>'+ ficha['DESC_MARCA']+'<br/><br/>';		
		if(ficha['DESC_MODELO'])
			content+='<div class="titulo_form">Modelo:</div>'+ ficha['MODELO_FICHA']+'<br/><br/>';
		content+='<div class="titulo_form">Categoria:</div>'+ ficha['DESC_CATEGORIA']+'<br/><br/>';
		content+='<div class="titulo_form">SubCategoria:</div>'+ ficha['DESC_SUBCATEGORIA']+'<br/><br/>';
		content+='<div class="titulo_form">Aprovado por:</div>'+ ((ficha['USER_APROVACAO'] != null) ? ficha['USER_APROVACAO'] : '') +'<br/><br/>';
		
		content+='<br/>';
		content+='<br/>';
		content+='<div class="titulo_view">Objetos da Ficha</div>';
		content+='<div id="lista" >';
		
		content+='<table border="0">';
		content+='<tr>';		
		content+='<th>Produto</th>';
		content+='<th>Nome do arquivo</th>';
		content+='<th>C&oacute;digo Produto</th>';		
		content+='<th>Tipo</th>';		
		content+='</tr>';		
		content+=linha;
		content+='</tr>';
		content+='</table>';
		content+='</div>';
		
		displayStaticMessage(content);
	},
	
	getIdFicha: function(id,email)
	{
		
		var teste = $$('.teste');	
		
		var str = '';
		
		for ( var i = 0; i < teste.length; i++ )
		{
			str+= (str=='' ?'' :',') + teste[i].value;
		}
		
		eval('var data = {"EMAIL":' + email + ',"produtos":[' + str + '], "id_carrinho":'+ id +' }');
		
		new Util().call('ficha/getAll', data, this.responseGetIdFicha);
		
	},
	
	responseGetIdFicha: function (response)
	{	
		carrinhos = response['carrinhos'];
		EMAIL = response['EMAIL'];
		
		config = response['config'];
		
		var mensagem = 'O(s) Código(s) de Produto(s) ';		
		//A(s) SKU(s) (n�mero SKU), se repete no carrinho �x� (nome do carrinho)
	
		if(carrinhos.length)
		{
			
			for(i=0;i<carrinhos.length;i++)
			{
				mensagem+=carrinhos[i][0]['COD_FICHA']+',';	
			}
			mensagem+=' se repete(m) no(s) carrinho(s) ';
			
			for(i=0;i<carrinhos.length;i++)
			{
				mensagem+=carrinhos[i][0]['DESC_CARRINHO']+', ';	
			}
			mensagem+='.';
			var m = mensagem.replace(", .",".")
			if(confirm(m))
			{				
				new Ficha().valida_carrinho('Salvar carrinho?', EMAIL);				
			}
		}else{
			new Ficha().valida_carrinho('Salvar carrinho?', EMAIL);
		}
		
	},

	formPesquisaImport: function(response)
	{			
		
		var categorias = response['categorias'];
		
		var option = '';
		
		for ( var i = 0; i < categorias.length ; i++ )
		{										
			option += '<option value="' + categorias[i]['ID_CATEGORIA'] + '">'; 
			option += categorias[i]['DESC_CATEGORIA'] ;
			option += '</option>' ;					
		}
		
		var content = ''+
			'<div style="width:800px;height:500px;overflow:auto;margin:5px;"><div class="titulo_view">Pesquisa de Ficha</div>' +
			'<div class="titulo_form">Tipo de ficha:</div>' +
			'<select class="inputs" id="TIPO_COMBO_FICHA" >' +				
				'<option  value="1">Produto</option>' +
				'<option  value="2">Promo&#231;&#227;o</option>' + 	
			'</select><br />' +
			'<div class="titulo_form">Nome:</div><input type="text" class="inputs" id="NOME_FICHA" size="35"/><br/>' +
			'<div class="titulo_form">Descri&ccedil;&atilde;o do Carrinho:</div><input type="text" class="inputs" id="DESC_CARRINHO"  size="35"/><br/>' +
			'<div class="titulo_form">C&oacute;digo Produto:</div><input type="text" class="inputs" id="COD_FICHA" /><br/>' +
			'<div class="titulo_form">Categoria:</div>' +
			'<select class="inputs" id="ID_CATEGORIA" onChange="new Categoria().getAllSub(new Categoria().responseSub , this.value, \'_ID_SUBCATEGORIA\');">' +				
				'<option selected="selected" value="">[Selecione]</option>' + 				
					option + 												
			'</select><br />' +
			'<div class="titulo_form">Subcategoria:</div>' +
			'<select class="inputs" id="_ID_SUBCATEGORIA">'+
				'<option selected="selected" value="">[Selecione]</option>' + 				
			'</select> <br/>' +						
			
			'<a href="javascript:new Util().vazio()" onClick="new Ficha().pesquisarFichaPricing()"><img src="'+ new Util().options.base_url +'img/lupa.gif"  alt="Confimar"  title="Pesquisar" class="pesquisa_add" border="0" /></a> ' +
			'<a href="javascript:new Util().vazio()" onClick="closeMessage()"><img src="'+ new Util().options.base_url +'img/cancelar.gif" border="0"/></a>' +
			'<div class="titulo_view">Sele&ccedil;&atilde;o de fichas</div>' +
			'<div id="resultado"></div></div>';	
		
		displayStaticMessage(content);
	},
	pesquisarFichaPricing: function(pagina)
	{		
		var inputs = $$('.inputs');
		var content = '{';
		
		for ( var i = 0; i < inputs.length; i++ )
			content+= ( (i==0) ?'' :',' ) + '\'' + inputs[i].id + '\':' + '\'' + inputs[i].value + '\'';		
			
		if(pagina != null)
			content+= ',\'pagina\':\'' + pagina + '\'';
		
		content+= '}';
		eval('var data = ' + content);
		
		new Util().call('ficha/pesquisarFichaPricing', data, this.responsePesquisarFichaPricing, 'post');		
	},
	
	responsePesquisarFichaPricing: function(response)
	{		
		var selecao = $$('.fcp');
		var setado = 0;
		
		var fichas = response['fichas'];
		var totalPagina = response['totalPagina'];
		var pagina = response['pagina'];
		var linhas = '';
		
		if(fichas)
		{
		for( var i= 0; i < fichas.length ; i++ )
		{																
			if(fichas[i]['TIPO_COMBO_FICHA']=='1')
			{
				linhas+='<tr>';	
				linhas+='<td align="center"><input type="checkbox" class="selecao" value="' + fichas[i]['ID_FICHA'] +' " ></td>';
				linhas+='<td align="center"><img src="'+fichas[i]['THUMB_OBJ_FICHA']+'" border="0" onload="new Util().img_size(\'id'+i+'\');" id="id'+i+'" name="id'+i+'" /></td>';			
				linhas+='<td>' + fichas[i]['COD_FICHA'] + ' - '+ fichas[i]['NOME_FICHA'] + '</td>';
				//linhas+='<td>' + fichas[i]['PV_FICHA'] + '</td>';
				linhas+='<td>' + fichas[i]['PV_ENC_FICHA'] + '</td>';
								
				if(fichas[i]['ID_PRICING']!=null)
					linhas+='<td align="center">Sim</td>';
				else 
					linhas+='<td align="center">N&atilde;o</td>';		
							 								
				linhas+='<td>' +  fichas[i]['DESC_CARRINHO'] + '</td>';
				linhas+='<td>' +  fichas[i]['COMENTARIO_CARRINHO'] + '</td>';
				linhas+='</tr>';
			}
			if(fichas[i]['TIPO_COMBO_FICHA']=='2')
			{
				linhas+='<tr>';	
				linhas+='<td align="center"><input type="checkbox" class="selecao" value="' + fichas[i]['ID_FICHA'] +' " ></td>';			
				linhas+='<td>' + fichas[i]['COD_FICHA'] + ' - '+ fichas[i]['NOME_FICHA'] + '</td>';
				//linhas+='<td>' + fichas[i]['PV_FICHA'] + '</td>';
				linhas+='<td>' + fichas[i]['PV_ENC_FICHA'] + '</td>';
				
				if(fichas[i]['ID_PRICING']!=null)
					linhas+='<td align="center">Sim</td>';
				else 
					linhas+='<td align="center">N&atilde;o</td>';
					
				linhas+='<td>' +  fichas[i]['DESC_CARRINHO'] + '</td>';				
				linhas+='</tr>';
			}			
		}
			
		}
		
		var a=0;
		if(fichas)
		{
			if(fichas[a]['TIPO_COMBO_FICHA']=='1')
			{		
				var content = '' +							
					'<table>' +			
						'<tr>' +
							'<th>Selecionar</th>' +			
							'<th>Objeto</th>' +
							'<th>Ficha</th>' +
							'<th>PV</th>' +		
							'<th>Pricing</th>' +		
							'<th>Carrinho</th>' +
							'<th>Observa&ccedil;&atilde;o </th>' +													
						'</tr>' +
						linhas +
					'</table>' +
					'<br/>' +
					'<span class="paginacao" id="paginacao"></span>' +
					'<img src="'+ new Util().options.base_url +'img/check.gif" class="pesquisa" alt="Confirmar"  title="Confirmar" onClick="if(confirm(\'Tem certeza que deseja importar Fichas?\'))new Ficha().getSelected();"/>' +			
				'';	
			}			
			if(fichas[a]['TIPO_COMBO_FICHA']=='2')
			{					
				var content = '' +							
					'<table>' +			
						'<tr>' +
							'<th>Selecionar</th>' +			
							'<th>Ficha</th>' +
							'<th>PV</th>' +													
						'</tr>' +
						linhas +
					'</table>' +
					'<br/>' +
					'<span class="paginacao" id="paginacao"></span>' +
					'<img src="'+ new Util().options.base_url +'img/check.gif" class="pesquisa" alt="Confirmar"  title="Confirmar" onClick="new Ficha().getSelected();"/>' +			
				'';	
			}
		}
			
		if(fichas)
		{
			$('resultado').innerHTML = content;		
		
			$('paginacao').innerHTML = new Util().doPaginacao('<a href="javascript:new Util().vazio()" onClick="new Ficha().pesquisarFichaPricing(#PAGINA#)">#LABEL#</a>', totalPagina, pagina);
		}
		else
			$('resultado').innerHTML = "Nenhum resultado";
		
	},
	
	valida_carrinho: function(msg, EMAIL)
	{
		
		var erro = 0;
		
		document.getElementById("mensagem").innerHTML = "";
		
		if(confirm(msg))
		{
			if($('desc_carrinho').value=='')
			{
				document.getElementById("mensagem").innerHTML+=" &nbsp;&nbsp;Campo Descricao Obrigatorio <br>";
				erro++;	
			}
			if($('responsavel').value=='')
			{
				document.getElementById("mensagem").innerHTML+=" &nbsp;&nbsp;Campo Responsavel Obrigatorio <br>";
				erro++;	
			}
			/*
			if($('validade').value=='')
			{
				document.getElementById("mensagem").innerHTML+=" &nbsp;&nbsp;Campo Data de Validade Obrigatorio <br>";
				erro++;	
			}
			*/
			
			
			if(erro==0)
			{			
				if(EMAIL)		
					$('EMAIL').value = 'envia';
				else	
					$('EMAIL').value = '';
			
				document.frm.submit();
				document.getElementById("mensagem").innerHTML = '';			
			}		
	}
	
}
	
});

Ficha.modelo = null;
Ficha.pesquisar = function(data, pg){
	var url = new Util().options.site_url + 
		'json/ficha/pesquisarObjetos';
	new Ajax(url,{
		onComplete: function(html){
			$('resultados').innerHTML = html;
			$j('#resultados .jqzoom').fancybox();
			$j('#fancy_overlay').css('z-index',100005);
			$j('#fancy_outer').css('z-index',100006);
		}
	}).request(data);
}

Ficha.pesquisaPorPaginacao = function(pg){
	$('pagina').value = pg;
	this.pesquisar($('frmPesquisa').toQueryString(), pg);
}

Ficha.desabilitaLinha = function(idficha){
	$each($('ficha_' + idficha).getChildren(), function(el){
		if(!$(el).hasClass('editar')){
			$(el).setStyle('opacity', .3);
		}
	});
	
	var line = $('ficha_'+idficha);
	line.getElement('.ativo').value = 0;
	
	$each($$('#ficha_'+idficha+' a'), function(el){
		if(!$(el).hasClass('editarLink')){
			el.onclick = null;
			el.onmousedown = null;
			el.onmouseover = null;
			el.onmouseup = null;
		}
	});
}

Ficha.habilitaLinha = function(idficha, data){
	var ficha = $('ficha_'+idficha);
	
	if(data){
		ficha.getElement('.detalhe').onclick = function(){
			new Ficha().detalheModal(data.ID_FICHA, data.TIPO_COMBO_FICHA);
		}
		
		ficha.getElement('.editarLink').onclick = function(){
			displayMessage(util.options.site_url + 'carrinho/form_pesquisa/'+$('carrinho_id').value+'/'+data.ID_FICHA);
		}
		
		ficha.getElement('.desabilitar').onclick = function(){
			Ficha.desabilitaLinha(data.ID_FICHA);
		}
		
		if(ficha.getElement('.pricing')){
			ficha.getElement('.pricing').id = 'pricing_'+idficha;
			ficha.getElement('.pricing').onclick = function(){
				new Pricing().formPricing(data.ID_F_C_P ? data.ID_F_C_P : '', idficha, $('carrinho_id').value);
			}
		}
	}
	
	var line = $('ficha_'+idficha);
	line.getElement('.ativo').value = 1;
	
	$each(ficha.getChildren(), function(el){
		$(el).setStyle('opacity', 1);
	});
}

Ficha.trocaFicha = function(idantiga, idnova){
	if(idantiga != idnova && $('ficha_'+idnova)){
		alert('Esta ficha já está no carrinho');
		return;
	}
	
	var url = new Util().options.site_url+'json/ficha/getFichaJson/'+idnova;
	
	new Ajax(url, {
		method: 'post',
		onComplete: function(resp){
			var data = eval('['+resp+'];');
			// categoria antiga 
			var catAntiga = $('ficha_'+idantiga).className.replace('categoria_','');
			// categoria nova
			var catNova = data[0].ID_CATEGORIA;
			// se nao forem as mesmas
			if(catAntiga != catNova){
				// limites das categorias
				var limiteCatAntiga = $('cat_'+catAntiga).value;
				var limiteCatNova = $('cat_'+catNova).value;
				// quantidade de elementos na categoria nova
				var qtdJaColocados = $$('.categoria_'+catNova).length;
				// se a quantidade existente igual ou maior que o limite da categoria
				if(qtdJaColocados >= limiteCatNova){
					// informamos que nao pode colocar
					alert('Esta categoria já está totalmente preenchida');
					return;
				}
			}
			
			Ficha.adicionaLinha(data[0], $('ficha_'+idantiga));
			closeMessage();
		}
	}).request();
	
}

/**
 * Adiciona linhas na tabela de elementos
 */
Ficha.adicionaLinha = function(data, line){
	if(this.modelo == null){
		alert('Modelo ainda nao definido');
		return;
	}
	
	var util = new Util();
	var nova = false;
	
	if(!line){
		var line = this.modelo.clone();
		nova = true;
	}
	
	// atribuindo os dados na linha
	line.className = 'categoria_' + data.ID_CATEGORIA;
	line.getElement('.thumb').src = data.THUMB_OBJ_FICHA;
	line.getElement('.codficha').innerHTML = data.COD_FICHA;
	line.getElement('.descricao').innerHTML = data.TITULO_ENC_FICHA ? data.TITULO_ENC_FICHA : '';
	line.getElement('.categoria').innerHTML = data.DESC_CATEGORIA;
	line.getElement('.observacao').value = data.DESC_FICHA ? data.DESC_FICHA : '';
	line.getElement('.idficha').value = data.ID_FICHA;
	line.getElement('.thumbobj').value = data.THUMB_OBJ_FICHA;
	line.getElement('.fcp').value = data.ID_F_C_P ? data.ID_F_C_P : '';
	line.getElement('.container').id = 'container_'+data.ID_FICHA;
	line.getElement('.ordem').value = data.ORDEM ? data.ORDEM : line.getElement('.ordem').value;
	line.getElement('.ativo').value = data.ATIVO;
	
	// ajustando os nomes para funcionar com o PHP feito pela Savoir
	line.getElement('.idficha').name = 'FICHAS['+data.ID_FICHA+'][FICHA][ID_FICHA]';
	line.getElement('.ordem').name = 'FICHAS['+data.ID_FICHA+'][F_C_P][ORDEM]';
	line.getElement('.ativo').name = 'FICHAS['+data.ID_FICHA+'][F_C_P][ATIVO]';
	line.getElement('.observacao').name = 'FICHAS['+data.ID_FICHA+'][F_C_P][DESC_FICHA]';
	line.getElement('.fcp').name = 'FICHAS['+data.ID_FICHA+'][F_C_P][ID_F_C_P]';
	line.getElement('.thumbobj').name = 'FICHAS['+data.ID_FICHA+'][IMG][ID_IMG]';
	
	// alterando o id da linha
	line.id = 'obj_'+data.ID_FICHA;
	
	if(nova){
		// adiciona a linha na tabela
		$('lista_fichas').adopt(line);
	}
	
	// habilita a linha
	this.habilitaLinha(data.ID_FICHA, data);
};


Ficha.adicionarLinhaObjeto = function(data){
	if(this.modelo == null){
		alert('Modelo ainda nao definido');
		return;
	}
	if($j('#divModalPesquisaObjeto').length == 1) {
		this.modelo = modelo_AdicionarObjetoSubFicha;
	}

	var util = new Util();
	var nova = false;
	
	if(!line){
		var line = this.modelo.clone();
		nova = true;
	}
	// atribuindo os dados na linha

	line.getElement('.imgThumb').onload = function(){
		this.id = 'id' + Math.ceil(Math.random() * (new Date().getTime()));
		this.name = this.id;
		new Util().img_size(this.id);
	}

	var jq = line.getElement('.jqzoom');
	jq.href = base_url+'img.php?img_id='+data.FILE_ID+"&img_size=big&rand=" + (Math.ceil(Math.random() * (new Date().getTime())))+"&a.jpg";
	line.getElement('.imgThumb').src = base_url+'img.php?img_id='+data.FILE_ID+"&rand=" + (Math.ceil(Math.random() * (new Date().getTime())))+"&a.jpg";
	line.getElement('.inputFile').value = data.FILENAME;
	line.getElement('.inputPath').value = data.PATH;
	line.getElement('.inputFileid').value = data.FILE_ID;
	if($j('#divModalPesquisaObjeto').length == 1) {
		line.getElement('.objSubFicha').id = data.ID_OBJETO;
		line.getElement('.objSubFicha').value = 0;
		line.getElement('.objSubFicha').setAttribute('fileID',data.FILE_ID);
	} else{
		line.getElement('.objFicha').value = data.FILE_ID;
	}
	
	// demais dados da ficha 
	line.getElement('.inputIdObjeto2').value = data.ID_OBJETO;
	if($j('#divModalPesquisaObjeto').length == 1) {
		line.getElement('.inputPrincipal').value = data.ID_OBJETO;
		line.getElement('.inputIdSubFicha').value = data.ID_FICHA;
	}
	line.getElement('.inputCodigo').value = data.CODIGO;
	if($j('#divModalPesquisaObjeto').length != 1) {
		line.getElement('.inputMarca').value = data.DESC_MARCA;
		line.getElement('.inputDataCriacao').value = data.DATA_UPLOAD;
		line.getElement('.inputOrigem').value = data.ORIGEM;
	}
	line.getElement('.inputTipo').value = data.DESC_TIPO_OBJETO;
	line.getElement('.inputCategoria').value = data.DESC_CATEGORIA;
	line.getElement('.inputSubcategoria').value = data.DESC_SUBCATEGORIA;
	if($j('#divModalPesquisaObjeto').length != 1) {
		line.getElement('.inputKeywords').value = data.KEYWORDS;
	}
	line.getElement('.inputTipoObjeto').value = data.DESC_TIPO_OBJETO;
	
	line.getElement('.file').innerHTML = data.FILENAME;
	line.getElement('.codigo').innerHTML = data.CODIGO;
	if($j('#divModalPesquisaObjeto').length != 1) {
		line.getElement('.marca').innerHTML = data.DESC_MARCA;
		line.getElement('.modified_date').innerHTML = dateFormat(data.DATA_UPLOAD, 'dd/mm/yyyy HH:MM:ss');
		line.getElement('.origem').innerHTML = data.ORIGEM;
	}
	line.getElement('.tipo').innerHTML = data.DESC_TIPO_OBJETO;
	line.getElement('.categoria').innerHTML = (data.DESC_CATEGORIA) ? data.DESC_CATEGORIA : "";
	line.getElement('.subcategoria').innerHTML = (data.DESC_SUBCATEGORIA) ? data.DESC_SUBCATEGORIA : "";
	if($j('#divModalPesquisaObjeto').length != 1) {
		line.getElement('.keywords').innerHTML = data.KEYWORDS;
	}
	
	// alterando o id da linha
	line.id = data.FILE_ID;
	
	// var id = 100 + '' + Math.ceil(new Date().getTime() * Math.random());
	line.getElements('input[type="hidden"]').each(function(el){
		el.name = el.name.replace('{holder}', line.id);
	});
	
	if(nova){
		// adiciona a linha na tabela
		if($j('#divModalPesquisaObjeto').length == 1) {
			$j(".lista_objetosSubFicha").css('display','');
			$('lista_objetosSubFicha').adopt(line);
		} else {
			$('lista_objetos').adopt(line);
		}
	}
	
	$j(jq).fancybox();
	
};

Ficha.fila = [];
Ficha.adicionarNovasFichas = function(){
	$each($$('.selecao'), function(el){
		if(el.checked){
			Ficha.fila.push(el);
		}
	});
	this._proccessQueue();
}

Ficha.adicionarNovosObjetos = function(){
	//alert('entrou');
   $j('.selecaoObjeto:checked').each(
		  function(index,El){
			 var chk = $j(this);
			 var data = chk.next('.selecaoObjeto_t').val();
			 var json = eval('['+data+']')[0];
			 var exists = false;
			 
			 var idObjeto = $j(this).attr('idObjeto');
			 //alteração na validação pois estava deixando adicionar o mesmo objeto varias vezes
			 $j('.selecaoObjeto:checked').each(function(){
				 
				if($j(this).val() == json["THUMB"]) {
				   exists = true;
				}
				$j('.inputIdObjeto2').each(function(){
					if(idObjeto == $j(this).val()){
						exists = true;
					} 
				});
			 });
			 if(!exists) {
				 if($j('.inputIdObjeto2').length == 0){
					 $j('#objetosSubFicha').show();
				 }
				 Ficha.adicionarLinhaObjeto(json);
			 }
		  }
	);
	updateRadioPrincipal();
}

Ficha._proccessQueue = function(){
	if(this.fila.length > 0){
		var el=this.fila.shift();
		
		if($('ficha_'+el.value)){
			alert('A ficha "'+el.alt+'" já está no carrinho.');
			Ficha._proccessQueue();
			
		} else {
			
			var url = new Util().options.site_url+'json/ficha/getFichaJson/'+el.value;
			new Ajax(url, {
				method:'post',
				onComplete: function(json){
					var data = eval('['+json+']');
					var ficha = data[0];
					// checa a quantidade de itens na categoria
					var qtd = $$('.categoria_'+ficha.ID_CATEGORIA).length;
					
					if(qtd >= $('cat_'+ficha.ID_CATEGORIA).value){
						alert('A categoria "'+ficha.DESC_CATEGORIA+'" está completa');
					} else {
						Ficha.adicionaLinha(ficha);
					}
					Ficha._proccessQueue();
				}
			}).request();
		}
	} else {
		this.startSort();
		closeMessage();
	}
}

Ficha.startSort = function(){
	if(this.sortInstance){
		this.sortInstance.detach();
	}
	this.sortInstance = new Sortables('lista_fichas',{
		handles: '.handler',
		onComplete: function(el){
			var i = 1;
			$each($('lista_fichas').getChildren(), function(row){
				$(row).getElement('.ordem').value = i++;
			})
		}
	});
}

Ficha.HistoricoJob = function(id) {
	displayMessagewithparameter(util.options.site_url+'json/ficha/historico_job/' + id, 800, 600);
}

Ficha.removerObjeto = function( el ){
	// pegando a linha do elemento
	var line = $j(el).closest('tr');

	// pega o input com o codigo do objeto
	var ipt = line.find('.inputIdObjeto2');
	
	// se for uma ficha combo, pode remover direto
	if( $j('#TIPO_FICHA').val() == 'COMBO' ){
		line.remove();

	// se nao
	} else {
		// coloca a mensagem de que esta validando
		$j('<div>Validando...</div>').appendTo( $j(el).parent() );
		
		var data = {
			ID_FICHA: $j('#ID_FICHA').val(),
			ID_OBJETO: ipt.val()
		};
		
		$j.post(util.options.site_url+'json/ficha/checar_exclusao_objeto', data, function(combos){
			// remove a div de validacao
			$j(el).parent().find('div').remove();
			
			// se estiver associado a alguma ficha-combo
			if( combos.length > 0 ){
				// nao pode remoover
				var msg = 'Este objeto esta ligado nas fichas-combo: \n' +
					combos.join('\n') +
					'\n\nDesassocie primeiro das fichas-combo';

				alert( msg );
				
			} else {
				// ok, pode remover
				line.remove();
				
				if($j('.inputIdObjeto2').length == 0){
					$j('#objetosSubFicha').hide();
				}
			}
		},'json');
		
	}
}

var selecaoFichas = new Array();

function selectFicha(e){
	var check = e.target;
	var idx = indexOf(selecaoFichas, check.value);

	if(check.checked){
		selecaoFichas.push(check.value);
	}else if(idx != null){
		selecaoFichas.splice(idx,1);
	}
}

function indexOf(array,value){
	for (var i = 0; i < array.length; i++){
		if(array[i] == value){
			return i;
		}
	}
	return null;
}
/****************************************************************************/
/*************************** FICHAS COMBO ***********************************/
/****************************************************************************/
Ficha.Combo = new Object();
Ficha.Combo.modelo = null;
Ficha.Combo.containerFichas = null;
Ficha.Combo.rowID = 0;

$j(function(){
	if( $j('#modelo_ficha_combo').length > 0 ){
		Ficha.Combo.modelo = $j('#modelo_ficha_combo').clone();
		Ficha.Combo.containerFichas = $j('#modelo_ficha_combo').parent();
		$j('#modelo_ficha_combo').remove();
	}
});

Ficha.Combo.buscarFichas = function(){
	// abre a modal
	getDivModal().dialog({
		width: '90%',
		height: $j(window).height() - 100,
		title: 'Buscar Fichas',
		modal: true,
		close: removeDivModal,
		autoOpen: false
	}).dialog('open');
	
	// indica que esta carregando
	getDivModal().html('Carregando...');
	
	// carrega o conteudo da modal
	$j.post(util.options.site_url+'json/ficha/busca_fichas_combo', $j('form').serialize(), function(html){
		getDivModal().html( html );
	});
}

Ficha.Combo.adicionar = function(){
	// itens selecionados
	var sel = $j('.item:checked');
	// se nao tiver nenhum
	if( sel.length == 0 ){
		alert('Você deve selecionar ao menos uma ficha');
		
	} else {
		var lista = [];
		sel.each(function(){
			lista.push( 'id[]='+this.value );
		});
		
		var data = lista.join('&');
		
		// o retorno desta chamada retorna uma lista de fichas
		// cada ficha com seus objetos
		$j.post(util.options.site_url+'json/ficha/busca_fichas_combo/adicionar', data, function(json){
			$j(json.fichas).each(function(){
				Ficha.Combo.adicionarFicha( this );
			});
			
			// para cada objeto
			$j(json.objetos).each(function(){
				Ficha.Combo.adicionarObjeto( this );
				
			});
			
			// fecha a modal
			getDivModal().dialog('close');
			
		},'json');
	}
}

Ficha.Combo.adicionarObjeto = function(data){
	// pesquisa para ver se existe
	var ipt = $j('.inputIdObjeto[value="'+data.ID_OBJETO+'"]');
	// se nao encontrar
	if( ipt.length == 0 ){
		// adiciona
		Ficha.adicionarLinhaObjeto( data );
		ipt = $j('.inputIdObjeto[value="'+data.ID_OBJETO+'"]');
	} 
	
	// pega os elementos
	var td = ipt.closest('td');
	var tr = ipt.closest('tr');
	var id = data.FILE_ID;
	
	// se ainda nao colocou o codigo da ficha
	if( td.find('.codigos_objetos[value="'+data.ID_FICHA+'"]').length == 0 ){
		// adiciona o codigo da ficha
		$j('<input class="codigos_objetos" type="hidden" name="objetos['+id+'][fichas][]" value="'+data.ID_FICHA+'" />').appendTo(td);
	}
}

Ficha.Combo.adicionarFicha = function(data){
	if( $j('.fichaCombo[value="'+data.ID_FICHA+'"]').length > 0 ){
		return false;
	}
	
	var line = this.modelo.clone();
	line.attr('id','');
	
	data.ROW_ID = this.rowID++;
	data.DT_INSERT_FICHA = dateFormat(data.DT_INSERT_FICHA, 'dd/mm/yyyy HH:MM');
	
	var el = $j('<div></div>');
	$j(line).appendTo(el);
	
	var html = el.html();
	for(var name in data){
		html = html.replace(new RegExp('\{'+name+'\}','g'), data[ name ]);
	}
	
	$j(html).appendTo(this.containerFichas);
}

Ficha.Combo.removerFicha = function(idficha){
	var ipt = $j('.codigos_objetos[value="'+idficha+'"]');
	
	ipt.each(function(){
		var td = $j(this).parent();
		$j(this).remove();
		
		if( td.find('.codigos_objetos').length == 0 ){
			td.closest('tr').remove();
		}
	});
	
	$j('.fichaCombo[value="'+idficha+'"]').closest('tr').remove();
}

Ficha.Combo.efetuarPesquisa = function(pagina){
	// altera a mensagem
	$j('#resultadoBusca').html( 'Pesquisando' );
	// pagina inicial (offset)
	var offset = pagina || 0;
	// serializa os dados do formulario
	var data = $j('#busca_fichas').serialize();
	// pega o cliente
	data += '&ID_CLIENTE='+$j('#ID_CLIENTE').val();
	data += '&pagina='+offset;
	
	// efetua a consulta
	$j.post(util.options.site_url+'json/ficha/busca_fichas_combo/pesquisar', data, function(html){
		$j('#resultadoBusca').html( html );
	});
}

Ficha.Combo.atualizarObjetos = function(){
	var lista = [];
	$j('.fichaCombo').each(function(){
		lista.push( 'id[]=' + this.value );
	});
	
	if( lista.length > 0 ){
		$j.post(util.options.site_url+'json/ficha/atualizar_objetos_combo', lista.join('&'), function(json){
			$j.each(json, function(){
				Ficha.Combo.adicionarObjeto( this );
			});
			
			alert('Objetos atualizados');
		},'json');
		
	} else {
		alert('Nenhuma ficha selecionada para atualizar os objetos');
	}
}

Ficha.Combo.atualizarAssociacoes = function( msgContainer ){
	var codigos = [];
	$j('.atualizarFichas:checked').each(function(){
		codigos.push('combos[]=' + this.value);
	});
	
	if( codigos.length == 0 ){
		alert('Nenhum ficha selecionada para ser atualiza');
		
	} else {
		$j('<div id="_msg">Atualizando ... </div>').appendTo( msgContainer );
		
		var data = $j('form').serialize() + '&' + 
			'id=' + $j('#ID_FICHA').val() + '&' + 
			codigos.join('&');
		
		$j.post(util.options.site_url+'json/ficha/atualizar_associacoes', data, function(html){
			msgContainer.find('#_msg').remove();
			alert('Atualização concluída');
		});
	}
}

/****************************************************************************/
/************************* FICHAS PAI ***************************************/
/****************************************************************************/
Ficha.Pai = new Object();
Ficha.Pai.modelo = null;
Ficha.Pai.containerFichas = null;
Ficha.Pai.rowID = 0;

$j(function(){
	if( $j('#modelo_ficha_pai').length > 0 ){
		Ficha.Pai.modelo = $j('#modelo_ficha_pai').clone();
		Ficha.Pai.containerFichas = $j('#modelo_ficha_pai').parent();
		$j('#modelo_ficha_pai').remove();
	}
});

Ficha.Pai.buscarFichas = function(){
	// abre a modal
	getDivModal().dialog({
		width: '90%',
		height: $j(window).height() - 100,
		title: 'Buscar Fichas Pai',
		modal: true,
		close: removeDivModal,
		autoOpen: false
	}).dialog('open');
	
	// indica que esta carregando
	getDivModal().html('Carregando...');
	
	// carrega o conteudo da modal
	$j.post(util.options.site_url+'json/ficha/busca_fichas_pai', $j('form').serialize(), function(html){
		getDivModal().html( html );
	});
}

Ficha.Pai.efetuarPesquisa = function(pagina){
	// altera a mensagem
	$j('#resultadoBusca').html( 'Pesquisando' );
	// pagina inicial (offset)
	var offset = pagina || 0;
	// serializa os dados do formulario
	var data = $j('#busca_fichas').serialize();
	// pega o cliente
	data += '&ID_CLIENTE='+$j('#ID_CLIENTE').val();
	data += '&pagina='+offset;

	// efetua a consulta
	$j.post(util.options.site_url+'json/ficha/busca_fichas_pai/pesquisar', data, function(html){
		$j('#resultadoBusca').html( html );
	});
}

Ficha.Pai.adicionar = function(){
	// itens selecionados
	var sel = $j('.item:checked');
	// se nao tiver nenhum
	if( sel.length == 0 ){
		alert('Você deve selecionar ao menos uma ficha');
	} else {
		var lista = [];
		sel.each(function(){
			lista.push( 'id[]='+this.value );
		});
		
		var data = lista.join('&');

		// o retorno desta chamada retorna uma lista de fichas
		// cada ficha com seus objetos
		$j.post(util.options.site_url+'json/ficha/busca_fichas_pai/adicionar', data, function(json){
			$j(json.fichas).each(function(){
				Ficha.Pai.adicionarFicha( this );
			});
			
			// fecha a modal
			getDivModal().dialog('close');
			
		},'json');
	}
}

Ficha.Pai.adicionarFicha = function(data){
	if( $j('.fichaPai[value="'+data.ID_FICHA+'"]').length > 0 ){
		return false;
	}

	var line = this.modelo.clone();
	line.attr('id','');
	
	data.ROW_ID = this.rowID++;
	data.DT_INSERT_FICHA = dateFormat(data.DT_INSERT_FICHA, 'dd/mm/yyyy HH:MM');
	
	var el = $j('<div></div>');
	$j(line).appendTo(el);
	
	var html = el.html();
	for(var name in data){
		html = html.replace(new RegExp('\{'+name+'\}','g'), data[ name ]);
	}
	
	$j(html).appendTo(this.containerFichas);
}

Ficha.Pai.adicionarObjeto = function(data){
	// pesquisa para ver se existe
	var ipt = $j('.inputIdObjeto[value="'+data.ID_OBJETO+'"]');
	// se nao encontrar
	if( ipt.length == 0 ){
		// adiciona
		Ficha.adicionarLinhaObjeto( data );
		ipt = $j('.inputIdObjeto[value="'+data.ID_OBJETO+'"]');
	} 
	
	// pega os elementos
	var td = ipt.closest('td');
	var tr = ipt.closest('tr');
	var id = data.FILE_ID;
	
	// se ainda nao colocou o codigo da ficha
	if( td.find('.codigos_objetos[value="'+data.ID_FICHA+'"]').length == 0 ){
		// adiciona o codigo da ficha
		$j('<input class="codigos_objetos" type="hidden" name="objetos['+id+'][fichas][]" value="'+data.ID_FICHA+'" />').appendTo(td);
	}
}

Ficha.Pai.removerFicha = function(idficha){
	var ipt = $j('.codigos_objetos[value="'+idficha+'"]');
	
	ipt.each(function(){
		var td = $j(this).parent();
		$j(this).remove();
		
		if( td.find('.codigos_objetos').length == 0 ){
			td.closest('tr').remove();
		}
	});
	
	$j('.fichaPai[value="'+idficha+'"]').closest('tr').remove();
}

/****************************************************************************/
/************************* FICHAS FILHA *************************************/
/****************************************************************************/
Ficha.Filha = new Object();
Ficha.Filha.modelo = null;
Ficha.Filha.containerFichas = null;
Ficha.Filha.rowID = 0;

$j(function(){
	if( $j('#modelo_ficha_filha').length > 0 ){
		Ficha.Filha.modelo = $j('#modelo_ficha_filha').clone();
		Ficha.Filha.containerFichas = $j('#modelo_ficha_filha').parent();
		$j('#modelo_ficha_filha').remove();
	}
});

Ficha.Filha.buscarFichas = function(){
	// abre a modal
	getDivModal().dialog({
		width: '90%',
		height: $j(window).height() - 100,
		title: 'Buscar Fichas Filha',
		modal: true,
		close: removeDivModal,
		autoOpen: false
	}).dialog('open');
	
	// indica que esta carregando
	getDivModal().html('Carregando...');
	
	// carrega o conteudo da modal
	$j.post(util.options.site_url+'json/ficha/busca_fichas_filha', $j('form').serialize(), function(html){
		getDivModal().html( html );
	});
}

Ficha.Filha.efetuarPesquisa = function(pagina){
	// altera a mensagem
	$j('#resultadoBusca').html( 'Pesquisando' );
	// pagina inicial (offset)
	var offset = pagina || 0;
	// serializa os dados do formulario
	var data = $j('#busca_fichas').serialize();
	// pega o cliente
	data += '&ID_CLIENTE='+$j('#ID_CLIENTE').val();
	data += '&pagina='+offset;

	// efetua a consulta
	$j.post(util.options.site_url+'json/ficha/busca_fichas_filha/pesquisar', data, function(html){
		$j('#resultadoBusca').html( html );
	});
}

Ficha.Filha.adicionar = function(){
	// itens selecionados
	var sel = $j('.item:checked');
	// se nao tiver nenhum
	if( sel.length == 0 ){
		alert('Você deve selecionar ao menos uma ficha');
	} else {
		var lista = [];
		sel.each(function(){
			lista.push( 'id[]='+this.value );
		});
		
		var data = lista.join('&');

		// o retorno desta chamada retorna uma lista de fichas
		// cada ficha com seus objetos
		$j.post(util.options.site_url+'json/ficha/busca_fichas_filha/adicionar', data, function(json){
			$j(json.fichas).each(function(){
				Ficha.Filha.adicionarFicha( this );
			});
			
			// fecha a modal
			getDivModal().dialog('close');
			
		},'json');
	}
}

Ficha.Filha.adicionarFicha = function(data){
	if( $j('.fichaFilha[value="'+data.ID_FICHA+'"]').length > 0 ){
		return false;
	}

	var line = this.modelo.clone();
	line.attr('id','');
	
	data.ROW_ID = this.rowID++;
	data.DT_INSERT_FICHA = dateFormat(data.DT_INSERT_FICHA, 'dd/mm/yyyy HH:MM');
	
	var el = $j('<div></div>');
	$j(line).appendTo(el);
	
	var html = el.html();
	for(var name in data){
		html = html.replace(new RegExp('\{'+name+'\}','g'), data[ name ]);
	}
	
	$j(html).appendTo(this.containerFichas);
}

Ficha.Filha.adicionarObjeto = function(data){
	// pesquisa para ver se existe
	var ipt = $j('.inputIdObjeto[value="'+data.ID_OBJETO+'"]');
	// se nao encontrar
	if( ipt.length == 0 ){
		// adiciona
		Ficha.adicionarLinhaObjeto( data );
		ipt = $j('.inputIdObjeto[value="'+data.ID_OBJETO+'"]');
	} 
	
	// pega os elementos
	var td = ipt.closest('td');
	var tr = ipt.closest('tr');
	var id = data.FILE_ID;
	
	// se ainda nao colocou o codigo da ficha
	if( td.find('.codigos_objetos[value="'+data.ID_FICHA+'"]').length == 0 ){
		// adiciona o codigo da ficha
		$j('<input class="codigos_objetos" type="hidden" name="objetos['+id+'][fichas][]" value="'+data.ID_FICHA+'" />').appendTo(td);
	}
}

Ficha.Filha.removerFicha = function(idficha){
	var ipt = $j('.codigos_objetos[value="'+idficha+'"]');
	
	ipt.each(function(){
		var td = $j(this).parent();
		$j(this).remove();
		
		if( td.find('.codigos_objetos').length == 0 ){
			td.closest('tr').remove();
		}
	});
	
	$j('.fichaFilha[value="'+idficha+'"]').closest('tr').remove();
}

/****************************************************************************/
/********************** FICHAS FILHA PAUTA **********************************/
/****************************************************************************/
Ficha.Filha.Pauta = new Object();
Ficha.Filha.Pauta.modelo = null;
Ficha.Filha.Pauta.containerFichas = null;
Ficha.Filha.Pauta.rowID = 0;

$j(function(){
	if( $j('#modelo_ficha_pauta_filha').length > 0 ){
		Ficha.Filha.Pauta.modelo = $j('#modelo_ficha_pauta_filha').clone();
		Ficha.Filha.Pauta.containerFichas = $j('#modelo_ficha_pauta_filha').parent();
		$j('#modelo_ficha_pauta_filha').remove();
	}
	if( $j('#modelo_ficha_pauta_subficha').length > 0 ){
		Ficha.Subficha.Pauta.modelo = $j('#modelo_ficha_pauta_subficha').clone();
		Ficha.Subficha.Pauta.containerFichas = $j('#modelo_ficha_pauta_subficha').parent();
		$j('#modelo_ficha_pauta_subficha').remove();
	}
});

Ficha.Filha.Pauta.efetuarPesquisa = function(idFichaPai, trAlvo, td){
	// efetua a consulta
	$j.post(util.options.site_url+'json/ficha/busca_fichas_filha_pauta/' + idFichaPai, '', function(json){
		
		if($j('._filha'+idFichaPai).size() > 0){
			$j('._filha'+idFichaPai).remove();
		}else{
			$j(json.fichasFilhas).each(function(){
				var el = $j('<div></div>');
				el.append(Ficha.Filha.Pauta.modelo.clone());
				var html = unescape(el.html());
	
				for(var name in this){
					html = html.replace(new RegExp('\{'+name+'\}','g'), this[ name ]);
				}
				//html = html.replace(new RegExp('\{' + 'FILE_ID' +'\}','gm'), 'aaa');
				var ht = $j(html);
				ht.addClass('_filha'+idFichaPai);
				trAlvo.after(ht);
				
				$j(".jqzoom").fancybox();
				
			});
		}
		/*
		// fecha a modal
		getDivModal().dialog('close');*/
		
	},'json');
}

/****************************************************************************/
/*************************** SUBFICHAS **************************************/
/****************************************************************************/
Ficha.Subficha = new Object();
Ficha.Subficha.modelo = null;
Ficha.Subficha.container = null;

Ficha.Subficha.desvincular = function(obj, idFicha1, idFicha2){
	if(confirm("Deseja realmente excluir esta Subficha?")){
		$j.post(
			util.options.site_url+'json/subficha/desvincular', 
			{idFicha1 : idFicha1, idFicha2 : idFicha2}, 
			function(data){
				if(data == 'ok'){
					removeLinha(obj);
				}
			}
		);
	}
}



Ficha.Subficha.efetuarPesquisa = function(pagina){
	// altera a mensagem
	$j('#resultadoBusca').html( 'Pesquisando' );
	// pagina inicial (offset)
	var offset = pagina || 0;
	// serializa os dados do formulario
	var data = $j('#busca_fichas').serialize();
	// pega o cliente
	data += '&ID_CLIENTE='+$j('#ID_CLIENTE').val();
	data += '&pagina='+offset;

	// efetua a consulta
	$j.post(util.options.site_url+'json/ficha/busca_subfichas/pesquisar', data, function(html){
		$j('#resultadoBusca').html( html );
	});
}

Ficha.Subficha.adicionar = function(data){
	alert($j("#container_subficha").parseHTML());
	
	var line = $j('#linha_modelo_subficha').clone();

	$j(this.container).append(line);
}

/********************** FICHAS FILHA PAUTA **********************************/
/****************************************************************************/
Ficha.Subficha.Pauta = new Object();
Ficha.Subficha.Pauta.modelo = null;
Ficha.Subficha.Pauta.containerFichas = null;
Ficha.Subficha.Pauta.rowID = 0;

$j(function(){
	if( $j('#modelo_ficha_pauta_filha').length > 0 ){
		Ficha.Filha.Pauta.modelo = $j('#modelo_ficha_pauta_filha').clone();
		Ficha.Filha.Pauta.containerFichas = $j('#modelo_ficha_pauta_filha').parent();
		$j('#modelo_ficha_pauta_filha').remove();
	}
});

Ficha.Subficha.Pauta.efetuarPesquisa = function(idFicha, trAlvo, td){
	// efetua a consulta
	$j.post(util.options.site_url+'json/subficha/busca_subfichas_pauta/' + idFicha, '', function(json){
		
		if($j('._filha'+idFicha).size() > 0){
			$j('._filha'+idFicha).remove();
		}else{
			$j(json.subfichas).each(function(){
				
				var el = $j('<div></div>');
				el.append(Ficha.Subficha.Pauta.modelo.clone());
				var html = unescape(el.html());
	
				for(var name in this){
					if(this[ name ] == null){
						html = html.replace(new RegExp('\{'+name+'\}','g'), '');
					}
					{
						html = html.replace(new RegExp('\{'+name+'\}','g'), this[ name ]);
					}
				}
				//html = html.replace(new RegExp('\{' + 'FILE_ID' +'\}','gm'), 'aaa');
				var ht = $j(html);
				ht.addClass('_filha'+idFicha);
				trAlvo.after(ht);
				
				$j(".jqzoom").fancybox();

				//alteração feita para que seja exibido o objeto caso exista algum finculado à subficha
				if(this['FILE_ID'] == null || this['FILE_ID'] == 0){
					$j('#objetoSubfichaPauta_'+this["ID_FICHA"]).hide();
					$j('#setaSubficha_'+this["ID_FICHA"]).show();
				} else {
					$j('#objetoSubfichaPauta_'+this['ID_FICHA']).show();
					$j('#setaSubficha_'+this['ID_FICHA']).hide();
				}
				
			});
		}
		/*
		// fecha a modal
		getDivModal().dialog('close');*/
		
	},'json');
}

/****************************************************************************/

