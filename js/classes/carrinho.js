var Carrinho = new Class({
	/**
	 * Popup de aprovacao e reprovacao 
	 * Alterado para poder carregar o conteudo certo de uma view,
	 * facilitando alteracoes.
	 * @author Hugo Silva
	 * @param id_carrinho     Codigo do carrinho a ser aprovado/reprovado
	 * @param tipo_aprovacao  Indica se o carrinho vai ser aprovado (APROVADO) ou reprovado (REPROVADO)
	 * @return void
	 */
	popupAprovacao: function(id_carrinho, tipo_aprovacao){
		// instancia a classe util
		var util = new Util();
		// cria o objeto de ajax passando a URL...
		new Ajax(util.options.site_url + 'json/carrinho/popupAprovacao', {
			// .. modo de envio ..
			method: 'post',
			// .. dados ..
			data: {id_carrinho: id_carrinho, tipo_aprovacao: tipo_aprovacao},
			// .. quando terminar a requisicao
			onComplete: function(result){
				// exibe o resultado na tela
				displayStaticMessage(result);
			}
		// efetua a requisicao
		}).request();
	},
	
	/**
	 * Envia a solicitacao via Ajax no formato JSON de aprovacao/reprovacao.
	 * Os demais dados sao recuperados do formulario.
	 * 
	 * @author Hugo Silva
	 * @param id_carrinho Codigo do carrinho que esta sendo aprovado/reprovado
	 * @return void
	 */
	gravarAprovacao: function(id_carrinho){
		// se nao informou um comentario
		if($('comentario_historico').value == ''){
			alert('Informe o comentário para ' + ($('status_key').value == 'APROVADO' ? 'aprovação' : 'reprovação')+'!');
			return;
		}
		
		// esconde os botoes
		$('botoes').setStyle('display','none');
		
		// dados que serao enviados
		var dados = $('formAprovacao').toQueryString()+'&id_carrinho='+id_carrinho;
		
		// instancia a classe util
		var util = new Util();
		// organiza os dados que serao enviados
		var data = {
			'status_log': $('status').value,
			'desc_log':$('comentario_historico').value,
			'modulo': $('modulo').value ,
			'id_carrinho': $('id_carrinho').value,
			'etapa': ($('etapa') != null ? $('etapa').value : '')
		};
		
		// envia a solicitacao
		util.call('carrinho/salvarAprovacao', data, function(response){
			alert('Dados gravados com sucesso');
			closeMessage();
			fazLocation();
		});
		
	},
	
	/*********************************************************************/
	
	aprovacao: function(modulo,id_carrinho,mode,id_status)
	{
		new Util().call('carrinho/getLogByCarrinho',{'id_carrinho': id_carrinho,'mode':mode,'id_status':id_status ,'modulo': modulo}, this.mountAprovacao);
	},
	mountAprovacao: function(response)
	{
		var Status = response['status'];
		
		var option = '<option value="">Selecione</option>';
		
		var varlabel;
		
		for ( var i = 0; i < Status.length ; i++ )
		{										
			if(Status[i]['ID_STATUS']==response['sel_status'])
			{
				varlabel = Status[i]['DESC_STATUS']; 
				//option += '<option selected="selected" value="' + Status[i]['ID_STATUS'] + '">';
			}else{
				//option += '<option value="' + Status[i]['ID_STATUS'] + '">';	
			}
			 
			option += Status[i]['DESC_STATUS'] ;
			option += '</option>' ;					
		}		
		
		var historico = response['historico'];
		
		if(historico.length>0)
		{
			conteudohistorico = 
			'	<tr>' +
			'		<th width="25%">Autor</th>' +
			'		<th>Status</th>' +
			'		<th>Data</th>' +
			'		<th width="40%">Comentario</th>' + 			
			'</tr>';
			
			for(var j = 0;j<historico.length;j++)
			{
				conteudohistorico+= '<tr bgcolor="#111111" onmouseout="bgColor=\'#111111\'" onmouseover="bgColor=\'#191919\'">' +
						'<td>'+historico[j]['NAME_USER']+'</td>' +
						'<td>'+historico[j]['STATUS_LOG']+'</td>' +
						'<td>'+historico[j]['DT_INSERT_LOG']+'</td>' +
						'<td>'+historico[j]['DESC_LOG']+'</td>' +
						'</tr>';	
			}

		}else{
			conteudohistorico = '<tr><td></td></tr>';
		}
		
		var content = ''+
			'<div style="width:800px;height:500px;overflow:auto;margin:5px;"><div class="titulo_view">Historico de Carrinhos</div>' +			
			'<br /><div style="width:790px;height:250px;overflow:auto;margin:5px;">' +
			'<input type="hidden" name="" value="'+response['id_carrinho']+'" id="id_carrinho" />' +
			'<input type="hidden" name="" value="'+response['modulo']+'" id="modulo" />' +
			'<input type="hidden" name="" value="'+response['mode']+'" id="mode" />' +
			
			'<table border="0" width="650">' +
			conteudohistorico +
			'' +
			'</table>' +
			'' +
			'</div>' +
			'<br /><div class="titulo_form">Status:<span class="obrigatorio">*</span></div><br />' +
			'<input type="hidden" name="status_historico" id="status_historico" value="'+response['sel_status']+'" />' +
			varlabel+			
			'<br /><div class="titulo_form">Comentario:<span class="obrigatorio">*</span></div>' +
			'<textarea name="comentario_historico" id="comentario_historico" rows="6" cols="50"></textarea>'+
			'<br /><br /><a href="javascript:new Util().vazio();" onclick="javascript:new Carrinho().salvarHistorico()" title="Salvar"><img src="'+ new Util().options.base_url +'img/salvar.gif" style="margin-left:180px;" class="btn" alt="Salvar" /></a>'+
			'&nbsp;&nbsp;<a href="javascript:new Util().vazio()" onClick="closeMessage()"><img src="'+ new Util().options.base_url +'img/cancelar.gif"  alt="Fechar"  title="Fechar" border="0" /></a> ' +
			'<div id="resultado"></div></div>';	
		
		displayStaticMessage(content);
		
	},
	getHistorico: function(id)
	{
		if(id!=null)
		{
			new Util().call('carrinho/gethistorico',{'carrinho':id},this.mountgetHistorico);
		}
	},
	mountgetHistorico: function(response)
	{
		if(response)
		{
			var content = '';
			
			content+='<div class="titulo_view">Historico<a href="javascript:new Util().vazio()" onClick="closeMessage()"><img src="'+ new Util().options.base_url +'img/cancelar.gif"   style="float:right;" alt="Fechar"  title="Fechar" border="0" /></a></div><br style="clear:both;" />' +
					'<table border="1" width="100%" >' +
					'<tr>' +
					'<th>Autor</th>' +
					'<th>Status</th>' +
					'<th>Data</th>' +
					'<th>Motivo</th>' +
					'</tr>';
					
			for(i=0;i<response['historicos'].length;i++)
			{
				content+='<tr>' +
						'<td>'+response['historicos'][i]['NAME_USER']+'</td>' +
						'<td>'+response['historicos'][i]['STATUS_LOG']+'</td>' +
						'<td>'+response['historicos'][i]['DT_INSERT_LOG']+'</td>' +
						'<td>'+response['historicos'][i]['DESC_LOG']+'</td>' +
						'</tr>';						
			}		
			
			content+='</table>';
			
			displayStaticMessagewithparameter(content,500,300);	
		}else{
			alert("Registro sem historico cadastrado");
		}
	},
	salvarHistorico: function()
	{
		var erro = 0;
		if($('status_historico').value=="")
		{
			alert('Por favor selecione o status');
			erro++;
		}
		if($('comentario_historico').value=="")
		{
			alert('Por favor informe o seu comentario');
			erro++;
		}
		if(erro==0)
		{
			//alert($('status_historico').value+'-'+$('comentario_historico').value+'-'+$('mode').value+"-"+$('modulo').value+"-"+$('id_carrinho').value );
			var util = new Util();
			var data = {
				'status_log': $('status_historico').value,
				'desc_log':$('comentario_historico').value,
				'mode':$('mode').value,
				'modulo': $('modulo').value ,
				'id_carrinho': $('id_carrinho').value
			};
			
			util.call('carrinho/saveHistory', data, function(response){
				closeMessage();
				
				setTimeout("fazLocation()", 2000);
			});
			/*new Util().call('carrinho/saveHistory',data, new Util().vazio());*/				
		}		
		
	},
	/*Detalhe do carrinho*/
	detalhe: function(id_carrinho)
	{				
		carrinho = {'ID_CARRINHO':id_carrinho};
		
		new Util().call('carrinho/getById', carrinho, this.mountDetalhe);
		
	},
	
	mountDetalhe: function(response)
	{		
		var fichas = response['fichas'];		
		var carrinho = response['carrinho'];
		
		var linha = '';
		
		if(fichas !=null)
		{		
			for( var i= 0; i < fichas.length ; i++ )
			{														
				linha+='<tr>';		
				linha+='<td align="center"><img height="50" src="'+ fichas[i]['THUMB_OBJ_FICHA'] +'" border="0"/></td>';
				linha+='<td>'+ fichas[i]['NOME_FICHA']+ '</td>';
				linha+='<td>'+ fichas[i]['COD_FICHA'] + '</td>';		
				//linha+='<td>'+ fichas[i]['MARCA_FICHA']+'</td>';
				linha+='<td>'+ fichas[i]['DESC_MARCA']+'</td>';
				linha+='<td>'+ fichas[i]['MODELO_FICHA']+'</td>';
				linha+='<td>'+ fichas[i]['PV_FICHA']+'</td>';
				linha+='</tr>';					
			}				
		}	
		content='';
		content='<div style="overflow:auto; height:500px;width:800px;margin:5px;">';
		content+='<div style="float:right;><a href="javascript:new Util().vazio()" onClick="closeMessage();"><img src="'+ new Util().options.base_url +'img/cancelar.gif"  alt="Confimar"  title="Pesquisar" style=" margin-right:5px;" border="0" /></a></div><br style="clear:both;"/>';
		content+='<div class="titulo_view">Detalhes Carrinho</div><br />';		
		content+='<div class="titulo_form">Descri&ccedil;&atilde;o:</div>'+ carrinho['DESC_CARRINHO']+' <br/><br/>';			
		content+='<div class="titulo_form">Data Cria&ccedil;&atilde;o:</div>'+ carrinho['DT_INSERT_CARRINHO']+'<br/><br/>';
		content+='<div class="titulo_form">Respons&aacute;vel:</div>'+ carrinho['RESPONSAVEL_CARRINHO']+'<br/><br/>';
		content+='<div class="titulo_form">Coment&aacute;rio:</div>'+ carrinho['COMENTARIO_CARRINHO']+'<br/><br/>';
		
		content+='<br/>';
		content+='<br/>';
		content+='<div class="titulo_view">Fichas do Carrinho</div>';
		content+='<div id="lista" >';
		
		content+='<table border="0">';
		content+='<tr>';		
		content+='<th>Produto</th>';
		content+='<th>Nome</th>';
		content+='<th>C&oacute;digo</th>';		
		content+='<th>Marca</th>';
		content+='<th>Modelo</th>';
		content+='<th>PV</th>';		
		content+='</tr>';		
		content+=linha;
		content+='</tr>';
		content+='</table>';
		content+='</div>';
		
		displayStaticMessage(content);
	}	
});	