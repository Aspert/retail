var Cenario = new Class({
});

Cenario.modelo = null;

Cenario.pesquisar = function(data, pg){
	var url = new Util().options.site_url + 
		'json/cenario/pesquisarObjetos';

	new Ajax(url,{
		onComplete: function(html){
			$('resultados').innerHTML = html;
			$j('#resultados .jqzoom').fancybox();
			$j('#fancy_overlay').css('z-index',100005);
			$j('#fancy_outer').css('z-index',100006);
		}
	}).request(data);
}

Cenario.adicionarNovosObjetos = function(){
	if ( $j('#objeto_padrao').size() == 0 ){
		$each($$('.selecaoObjeto'), function(el){
			if(el.checked){
				var json = eval('['+el.value+']')[0];
				var exists = false;
				$each($$('.objSelecionados'), function(el2){
					if($(el2).value == json["THUMB"]){
						exists = true;
					}
				});
				if(!exists){
					Cenario.adicionarLinhaObjeto(json);
					closeMessage();
				}
			}
		});
	}
	else{
		alert('Somente é possível associar um objeto ao Cenário. Por favor, selecione apenas um objeto.')
	}
}

Cenario.adicionarLinhaObjeto = function(data){
	
	if($j('#objeto_padrao').size() > 0){
		$j('#objeto_padrao').remove();
	}

	if(this.modelo == null){
		alert('Modelo ainda nao definido');
		return;
	}
	
	var util = new Util();
	var nova = false;
	
	if(!line){
		var line = this.modelo.clone();
		nova = true;
	}
	// atribuindo os dados na linha

	line.getElement('.imgThumb').onload = function(){
		this.id = 'id' + Math.ceil(Math.random() * (new Date().getTime()));
		this.name = this.id;
		new Util().img_size(this.id);
	}

	var jq = line.getElement('.jqzoom');
	jq.href = base_url+'img.php?img_id='+data.FILE_ID+"&img_size=big&rand=" + (Math.ceil(Math.random() * (new Date().getTime())))+"&a.jpg";
	line.getElement('.imgThumb').src = base_url+'img.php?img_id='+data.FILE_ID+"&rand=" + (Math.ceil(Math.random() * (new Date().getTime())))+"&a.jpg";
	
	// demais dados do cenario 
	line.getElement('.inputIdObjeto').value = data.ID_OBJETO;
	
	line.getElement('.file').innerHTML = data.FILENAME;
	line.getElement('.codigo').innerHTML = data.CODIGO;
	line.getElement('.marca').innerHTML = data.DESC_MARCA;
	line.getElement('.modified_date').innerHTML = dateFormat(data.DATA_UPLOAD, 'dd/mm/yyyy HH:MM:ss');
	line.getElement('.tipo').innerHTML = data.DESC_TIPO_OBJETO;
	line.getElement('.categoria').innerHTML = (data.DESC_CATEGORIA) ? data.DESC_CATEGORIA : "";
	line.getElement('.subcategoria').innerHTML = (data.DESC_SUBCATEGORIA) ? data.DESC_SUBCATEGORIA : "";
	line.getElement('.keywords').innerHTML = data.KEYWORDS;
	
	// alterando o id da linha
	line.id = data.FILE_ID;
	
	// var id = 100 + '' + Math.ceil(new Date().getTime() * Math.random());
	line.getElements('input[type="hidden"]').each(function(el){
		el.name = el.name.replace('{holder}', line.id);
	});
	
	if(nova){
		// adiciona a linha na tabela
		$j(line).attr('id', 'objeto_padrao');
		$('lista_objetos').adopt(line);
	}
	
	$j(jq).fancybox();
};

Cenario.pesquisaPorPaginacao = function(pg){
	$('pagina').value = pg;
	this.pesquisar($('frmPesquisa').toQueryString(), pg);
}

Cenario.removerObjeto = function( el ){
	// pegando a linha do elemento
	var line = $j(el).closest('tr');
	// removendo a linha
	line.remove();
}