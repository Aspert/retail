var Categoria = new Class({
	
	/*busca todas as categorias*/
	getAll: function(response)
	{				
		new Util().call('categoria/getAll', null, response);					
	},
		
	/*busca todas as subcategorias*/
	getAllSub: function(response, id, combo, value)
	{						
		combo = (combo!=null) ?combo :'ID_SUBCATEGORIA';
		value = (value!=null) ?value :'ID_SUBCATEGORIA';
		
		new Util().call('categoria/getAllSub', {'id':id, 'combo': combo, 'value': value}, response, 'post');
	},
	/*busca todas as subcategorias*/
	getSubByCategoria: function(response, id, combo, value)
	{						
		combo = (combo!=null) ?combo :'ID_SUBCATEGORIA';
		value = (value!=null) ?value :'ID_SUBCATEGORIA';
		
		new Util().call('categoria/getSubByCategoria', {'id':id, 'combo': combo, 'value': value}, response, 'post');
	},
	/*resposta da subcategoria */
	responseSub: function(response)
	{
		
		var subCategorias =  response['data'];
		
		var option = document.createElement('OPTION');
		option.innerHTML = '[Selecione]';
		option.value = '';
		
		$(response['combo']).empty().appendChild(option);
		
		for ( var i = 0; i < subCategorias.length ; i++ )
		{																  
			var option = document.createElement('OPTION');
			option.value = subCategorias[i][response['value']];
			option.innerHTML = subCategorias[i]['DESC_SUBCATEGORIA'];

			$(response['combo']).appendChild(option);			
		}
	},
	reponseGetAllByUsuario: function(response)
	{				
		var selecionados = response['selecionados'];
		var option = document.createElement('OPTION');
		$('categoria_selecionados').empty();
		for ( var i = 0; i < selecionados.length ; i++ )
		{																  
			var option = document.createElement('OPTION');
			option.value = selecionados[i]['ID_CATEGORIA'];
			option.innerHTML = selecionados[i]['DESC_CATEGORIA'];

			$('categoria_selecionados').appendChild(option);
			
					
		}
		
		var disponiveis = response['disponiveis'];
		var option1 = document.createElement('OPTION');
		$('categoria_disponiveis').empty();
		for ( var i = 0; i < disponiveis.length ; i++ )
		{																  
			var option1 = document.createElement('OPTION');
			option1.value = disponiveis[i]['ID_CATEGORIA'];
			option1.innerHTML = disponiveis[i]['DESC_CATEGORIA'];

			$('categoria_disponiveis').appendChild(option1);
			
					
		}
		
		
		//alert(response['selecionados']);					
	}
					
});