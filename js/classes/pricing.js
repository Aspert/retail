var Pricing = new Class({
	
	calcValue: function(id)
	{				
		var valorde  = $('PRECO_DE_PRICING_'+id).value;
		var valorpor = $('PRECO_POR_PRICING_'+id).value;		
		
		valorde = valorde.replace('.','');
		valorde = valorde.replace(',','');
		valorpor = valorpor.replace('.',''); 
    	valorpor = valorpor.replace(',','');  
    	
    	valorde = parseInt(valorde);
    	valorpor = parseInt(valorpor);
    	
    	var valor = valorde-valorpor;
    	
    	var resultado = new Util().formatarmoeda(valor);
    	
    	if(resultado=="falso")
    	{
    		alert("Valor nao pode ser calculado");
    	}else{
    		$('ECONOMIZE_PRICING_'+id).value = resultado;	
    	}
	},
	atualizaMuMua: function(id)
	{
		
		var fator = $('imposto').value;
		if(fator == '')
		{
			if(confirm(unescape('Essa ficha nao contem uma tabela de fator, deseja continuar ?')))
			{
				$('MU_PRICING_'+id).value = '0,00';
				$('MUA_PRICING_'+id).value= '0,00';
						
			}	
		}
		else
		{
			//CUSTO_PRICING
			var custo = $('CUSTO_PRICING_'+id).value;			
			custo = custo.replace(/\./g,'');
			custo = custo.replace(/\,/g,'.');
			
			if($('PRECO_POR_PRICING_'+id).value != '')
			{
				//PRECO_AVISTA_PRICING			
				var preco = $('PRECO_POR_PRICING_'+id).value;
				preco = preco.replace(/\./g,'');
				preco = preco.replace(/\,/g,'.');
			}
			else
			{
				alert('Necessario preencher o valor por');								
			}	
			//PRECO_AVISTA_PRICING			
			var recomposicao = $('RECOMPOSICAO_PRICING_'+id).value;
			recomposicao = recomposicao.replace(/\./g,'');
			recomposicao = recomposicao.replace(/\,/g,'.');
			
			//CALCULO MU - (preco*fator-custo)/(preco*fator)
			var mu = ((preco*fator-custo)/(preco*fator))*100;			
			mu = new String(Math.round( mu * Math.pow( 10 , 2 ) ) / Math.pow( 10 , 2 ));
			
			//MU PARA VIEW 
			mu = mu.replace(/\./g,',');
			$('MU_PRICING_'+id).value = mu;		
			
			//MUA - (((preco*fator)-(custo-recomposicao))/(preco*fator)
			
			var mua = (((preco*fator)-(custo-recomposicao))/(preco*fator))*100;			
			mua = new String(Math.round( mua * Math.pow( 10 , 2 ) ) / Math.pow( 10 , 2 ));
			
			//MUA PARA VIEW 
			mua = mua.replace(/\./g,',');
			$('MUA_PRICING_'+id).value = mua;	
						
		}			  
	},
	
	getByParcela: function(id)
	{
		new Util().call('juro/getByParcela',{'parcela': $('PARCELAS_PRICING_'+id).value, 'tipo_plano': $('ID_TIPO_PLANO_'+id).value,'indice':id}, new Pricing().responsegetByParcela);	
	},
	
	responsegetByParcela: function(response)
	{
		var juros = response['juro'];
		var tipo_plano = response['tipo_plano'];
		var id = response['indice'];
		
		
		if(tipo_plano != 1 && tipo_plano != '')
		{						
			//PRECO_AVISTA_PRICING			
			var preco = $('PRECO_AVISTA_PRICING_'+id).value;
			preco = preco.replace(/\./g,'');
			preco = preco.replace(/\,/g,'.');
			
			//ENTRADA_PRICING
			if($('FLAG_ENTRADA_PRICING_'+id).checked)				
			{
				var entrada = $('ENTRADA_PRICING_'+id).value;			
				entrada = entrada.replace(/\./g,'');
				entrada = entrada.replace(/\,/g,'.');				
			}
			else
			{			
				var entrada = 0;	
			}
			
			//PARCELA_PRICING			
			var parcela = $('PARCELAS_PRICING_'+id).value;			
			juro_am = juros['TAXA_JURO'];
				
			if(juros != false && juro_am != 0)
			{
				fator = juros['FATOR_JURO'];
				
				var dif = preco-entrada;
				
				fator = parseFloat(fator);
				
				//PRESTACAO  -  (fator*(preco-entrada))				
				//var pretacao = fator*(preco-entrada);
				var pretacao = fator*dif;				
				
				pretacao = new String(Math.round( pretacao * Math.pow( 10 , 2 ) ) / Math.pow( 10 , 2 ));				
				
				var p= pretacao;
				
				//PRESTACAO PARA VIEW 
				pretacao = pretacao.replace(/\./g,',');
				
				$('PRESTACAO_PRICING_'+id).value = pretacao;		
								
				
				//JUROS_AA_PRICING  - ((juro_am+1)^12)-1
				juro_aa = ((Math.pow(((juro_am/100)+1),12))-1)*100 ;
				
				//JUROS_AA_PRICING PARA VIEW 
				juro_aa = new String(Math.round( juro_aa * Math.pow( 10 , 2 ) ) / Math.pow( 10 , 2 ));
				juro_aa = juro_aa.replace(/\./g,',');
				$('JUROS_AA_PRICING_'+id).value = juro_aa;				
				
				//JUROS_AA_PRICING PARA VIEW
				$('CET_PRICING_'+id).value = juro_aa;
				
				//JUROS_AM_PRICING PARA VIEW
				juro_am = juro_am.replace(/\./g,',');
				$('JUROS_AM_PRICING_'+id).value = juro_am;
			}
			else
			{
				if(confirm('Nao contem uma tabela de fator para  essa quantidade \nde parcela e tipo de pagamento.\n Deseja continuar ? '))
				{
					//PRESTACAO  -  (preco-entrada)/parcela				
					var pretacao = (preco-entrada)/parcela;
					pretacao = new String(Math.round( pretacao * Math.pow( 10 , 2 ) ) / Math.pow( 10 , 2 ));
					var p= pretacao;
					//PRESTACAO PARA VIEW 
					pretacao = pretacao.replace(/\./g,',');
					$('PRESTACAO_PRICING_'+id).value = pretacao;					
					$('JUROS_AM_PRICING_'+id).value = '0,00';
					$('JUROS_AA_PRICING_'+id).value = '0,00';
					$('CET_PRICING_'+id).value = '0,00';
				}
			}
			entrada = parseFloat(entrada);
			var total = (p*parcela)+entrada;
			total = new String(Math.round( total * Math.pow( 10 , 2 ) ) / Math.pow( 10 , 2 ));						
			total = total.replace(/\./g,',');			
			$('TOTAL_PRICING_'+id).value = total;	
		}		
	},
	responseDetalhe: function(response)
	{		
		pricing = response['pricing'];
		
		var content = '';

		content='<div style="overflow:auto; height:500px;width:800px;margin:5px;">';
		content+='<div style="float:right;><a href="javascript:new Util().vazio()" onClick="closeMessage();"><img src="'+ new Util().options.base_url +'img/cancelar.gif"  alt="Sair"  title="Sair" style=" margin-right:5px;" border="0" /></a></div><br style="clear:both;"/>';
		
		if(pricing)
		{
			for(i=0;i<pricing.length;i++)
			{
				
				content+='<div class="titulo_view">Pricing'+pricing[i]['ID_PRICING']+'</div><br/>';
				content+='<div class="titulo_form">Pre&ccedil;o &agrave Vista:</div>' + ( (pricing)? pricing[i]['PRECO_AVISTA_PRICING'] : "" )+ '<br/><br/>';
				content+='<div class="titulo_form">Pre&ccedil;o de:</div>' + ( (pricing)? pricing[i]['PRECO_DE_PRICING'] : "" ) + '<br/><br/>';
				content+='<div class="titulo_form">Pre&ccedil;o por:</div>' + ( (pricing)? pricing[i]['PRECO_POR_PRICING'] : "" ) + '<br/><br/>';
				content+='<div class="titulo_form">Economize:</div>' + ( (pricing)? pricing[i]['ECONOMIZE_PRICING'] : "" ) + '<br/><br/>';
				content+='<div class="titulo_form">Custo Comercial:</div>' + ( (pricing)? pricing[i]['CUSTO_PRICING'] : "" ) + '<br/><br/>';
				content+='<div class="titulo_form">Valor de recomposi&ccedil;&atilde;o:</div>' + ( (pricing)? pricing[i]['RECOMPOSICAO_PRICING'] : "" ) + '<br/><br/>';
				content+='<div class="titulo_view">Plano</div>';
				content+='<br/>';
				content+='<div class="titulo_form">MU:</div>' + ((pricing) ? pricing[i]['MU_PRICING'] : '') +'<br/><br/>';
				content+='<div class="titulo_form">MUA:</div>' + ((pricing) ? pricing[i]['MUA_PRICING'] : '') +'<br/><br/>';		
				content+='<div class="titulo_form">Tipo:</div>' + ((pricing) ? pricing[i]['DESC_TIPO_PLANO'] : '') +'<br/><br/>';		
				content+='<div class="titulo_form">Entrada:</div>' + ( (pricing && pricing[i]['FLAG_ENTRADA_PRICING'] == '1') ?'Com entrada' :(pricing['FLAG_ENTRADA_PRICING'] == undefined) ? '' : 'Sem entrada' ) + '<br/><br/>'; 	
				content+='<div class="titulo_form">Valor da Entrada:</div>' + ( (pricing)? pricing[i]['ENTRADA_PRICING'] : "" ) + '<br/><br/>';
				content+='<div class="titulo_form">Parcelas:</div>' + ( (pricing)? pricing[i]['PARCELAS_PRICING'] : "" ) + '<br/><br/>';
				content+='<div class="titulo_form">Presta&ccedil;&atilde;o:</div>' + ( (pricing)? pricing[i]['PRESTACAO_PRICING'] : "" ) + '<br/><br/>';
				content+='<div class="titulo_form">JurosAM:</div>' + ( (pricing)? pricing[i]['JUROS_AM_PRICING'] : "" ) + '<br/><br/>';
				content+='<div class="titulo_form">JurosAA:</div>' + ( (pricing)? pricing[i]['JUROS_AA_PRICING'] : "" ) + '<br/><br/>';
				content+='<div class="titulo_form">CET:</div>' + ( (pricing)? pricing[i]['CET_PRICING'] : "" ) + '<br/><br/>';
				content+='<div class="titulo_form">Total:</div>' + ( (pricing)? pricing[i]['TOTAL_PRICING'] : "" ) + '<br/><br/>';				
				content+='<div class="titulo_form">Situa&ccedil;&atilde;o:</div>'+ ( (pricing && pricing[i]['SITUACAO_PRICING'] == 1)? 'Aprovado' : (pricing['SITUACAO_PRICING'] ==undefined) ?'' : 'Reprovado' ) ;				
				content+='<br /><br />';
			}			
		}
		
		
		content+='</div>';
		
		displayStaticMessage(content);
	},
	addForm: function (response)
	{					
		var planos = response['planos'];			
		var options = '<option value"">[Selecione]</option>';
		
		for(var i= 0; i < planos.length ; i++ )
		{													
			
			options+='<option value="' + planos[i]['ID_TIPO_PLANO'] + '"> ' + planos[i]['DESC_TIPO_PLANO'] + '</option>';			
		}
		
		
		var indice = 'novo_' + $('add_form').value;
		
		var content = '';
		content+=	'<br/>' +
					'<div class="titulo_view">Pricing</div>' +
					'<br/>' +
					'<br/>' +
					'<div class="titulo_form">Pre&ccedil;o &agrave; Vista:</div>' +
					'<input name="PRICING[' + indice + '][PRECO_AVISTA_PRICING]" id="PRECO_AVISTA_PRICING_'+ indice + '" value="">' +					
					'<br/>' +
					'<div class="titulo_form">Pre&ccedil;o De:</div>' +
					'<input name="PRICING[' + indice + '][PRECO_DE_PRICING]" id="PRECO_DE_PRICING_'+ indice + '" value="" />' +
					
					'<br/>' +
					'<div class="titulo_form">Pre&ccedil;o por:</div>' +
					'<input name="PRICING[' + indice + '][PRECO_POR_PRICING]" id="PRECO_POR_PRICING_'+ indice + '" value=""/>' +
					'<br/>' +
					'<div class="titulo_form">Economize:</div>' +
					'<input type="text" name="PRICING[' + indice + '][ECONOMIZE_PRICING]" id="ECONOMIZE_PRICING_'+ indice + '" value=""/>' +
					'<br/>' +
					'<div class="titulo_form">Custo comercial:</div>' +
					'<input type="text" name="PRICING[' + indice + '][CUSTO_PRICING]" id="CUSTO_PRICING_'+ indice + '" value="" size="20" />' +
					'<br/>' +
					'<div class="titulo_form">Valor de Recomposi&ccedil;&atilde;o:</div>' +
					'<input type="text" name="PRICING[' + indice + '][RECOMPOSICAO_PRICING]" id="RECOMPOSICAO_PRICING_'+ indice + '" value="" onBlur="new Pricing().atualizaMuMua(\''+ indice +'\')"/>' +
					'<span style="color:#f00;font-weight:bold;" >' +
					'	<a  style="color:#f00;font-weight:bold;" href="javascript:new Util().vazio();" onclick="new Pricing().calcValue(\''+ indice +'\');">' +
					'		C&aacute;lculo do Economize ' +
					'	</a>' +
					'</span>' +
					'<br/>' +
					'<div class="titulo_view">Plano</div>' +
					'<br/><br/>' +
					'<div class="titulo_form">MU:</div>' +
					'<input type="text" name="PRICING[' + indice + '][MU_PRICING]" id="MU_PRICING_'+ indice + '" value=""/>' +
					'<span style="color:#f00;font-weight:bold;" >' +
					'	<a  style="color:#f00;font-weight:bold;" href="javascript:new Util().vazio();" onclick="new Pricing().atualizaMuMua(\''+ indice +'\');">' +
					'		C&aacute;lculo MU e MUA ' +
					'	</a>' +
					'</span>' +
					'<br/>' +
					'<div class="titulo_form">MUA:</div>' +
					'<input type="text" name="PRICING[' + indice + '][MUA_PRICING]" id="MUA_PRICING_'+ indice + '" value=""/>' +
					'<br/>' +
					'<div class="titulo_form">Tipo:</div>' +
					'<select name="PRICING[' + indice + '][ID_TIPO_PLANO]" id="ID_TIPO_PLANO_'+ indice + '"> ' +
					options +
					'</select>' +
					'<br />' +
					'<div class="titulo_form">Entrada:</div>' +
					'<input name="PRICING[' + indice + '][FLAG_ENTRADA_PRICING]" id="FLAG_ENTRADA_PRICING_'+ indice + '" type="checkbox" value="1" />' +
					'<br />' +
					'<div class="titulo_form">Valor da Entrada:</div>' +
					'<input type="text" name="PRICING[' + indice + '][ENTRADA_PRICING]" id="ENTRADA_PRICING_'+ indice + '" value=""/>' +					
					'<br />' +
					'<div class="titulo_form">Parcelas:</div>' +
					'<input type="text" name="PRICING[' + indice + '][PARCELAS_PRICING]" id="PARCELAS_PRICING_'+ indice + '" size="10" value=""/>' +
					'<br />' +
					'<div class="titulo_form">Presta&ccedil;&atilde;o:</div>' +
					'<input type="text" name="PRICING[' + indice + '][PRESTACAO_PRICING]" id="PRESTACAO_PRICING_'+ indice + '" value=""/>' +
					'<span style="color:#f00;font-weight:bold;" >' +
					'	<a  style="color:#f00;font-weight:bold;" href="javascript:new Util().vazio();" onclick="javascript:new Pricing().getByParcela(\''+ indice +'\');">' +
					'		C&aacute;lculo presta&ccedil;&atilde;o' +
					'	</a>' +
					'</span>' +
					'<br />' +
					'<div class="titulo_form">JurosAM:</div>' +
					'<input type="text" name="PRICING[' + indice + '][JUROS_AM_PRICING]" id="JUROS_AM_PRICING_'+ indice + '" value=""/>' +
					'<br />' +
					'<div class="titulo_form">JurosAA:</div>' +
					'<input type="text" name="PRICING[' + indice + '][JUROS_AA_PRICING]" id="JUROS_AA_PRICING_'+ indice + '" value=""/>' +
					'<br />' +
					'<div class="titulo_form">CET:</div>' +
					'<input type="text" name="PRICING[' + indice + '][CET_PRICING]" id="CET_PRICING_'+ indice + '" value=""/>' +
					'<br />' +
					'<div class="titulo_form">Total:</div>' +
					'<input type="text" name="PRICING[' + indice + '][TOTAL_PRICING]" id="TOTAL_PRICING_'+ indice + '" value=""/>' +
					'<br />' +
					'<div class="titulo_form">Situa&ccedil;&atilde;o:</div>' +
					'<input type="radio" name="PRICING[' + indice + '][SITUACAO_PRICING]" value="1"/> Aprovado&nbsp ' +
					'<input type="radio" name="PRICING[' + indice + '][SITUACAO_PRICING]" value="2"/>Reprovado<br/>' +
					'<img src="'+ new Util().options.base_url +'img/excluir.gif" value="Remover" onClick="new Pricing().remover(this,\'' + indice+'\')" border="0" /> <br/><br/>';
					
			$('add_form').value = 1 + parseInt($('add_form').value); 
						
			var DIV = document.createElement('DIV');
			DIV.innerHTML = content;
			DIV.id = "pricing_"+indice;
			$('div_form').appendChild(DIV);
						
			new Util().formatCurrency($('PRECO_AVISTA_PRICING_'+ indice), 2, ".", ",");											
			new Util().formatCurrency($('PRECO_DE_PRICING_'+ indice), 2, ".", ",");
			new Util().formatCurrency($('PRECO_POR_PRICING_'+ indice), 2, ".", ",");
			new Util().formatCurrency($('ECONOMIZE_PRICING_'+ indice), 2, ".", ",");
			new Util().formatCurrency($('CUSTO_PRICING_'+ indice), 2, ".", ",");
			new Util().formatCurrency($('RECOMPOSICAO_PRICING_'+ indice), 2, ".", ",");
			new Util().formatCurrency($('MU_PRICING_'+ indice), 2, ".", ",");
			new Util().formatCurrency($('MUA_PRICING_'+ indice), 2, ".", ",");
			new Util().formatCurrency($('ENTRADA_PRICING_'+ indice), 2, ".", ",");			
			new Util().formatCurrency($('PRESTACAO_PRICING_'+ indice), 2, ".", ",");
			new Util().formatCurrency($('JUROS_AM_PRICING_'+ indice), 2, ".", ",");
			new Util().formatCurrency($('JUROS_AA_PRICING_'+ indice), 2, ".", ",");
			new Util().formatCurrency($('CET_PRICING_'+ indice), 2, ".", ",");
			new Util().formatCurrency($('TOTAL_PRICING_'+ indice), 2, ".", ",");
	},	
	formPricing: function(id_f_c_p,id_ficha,id_carrinho)
	{		
		var data = {'ID_FICHA':id_ficha,'ID_F_C_P': id_f_c_p,'ID_CARRINHO':id_carrinho};		
		
		new Util().call('pricing/getPricingByFcp', data, this.responseFormPricing);		
	},
	responseFormPricing: function(response)
	{
		var tipos = response['tipos_plano'];
		var carrinho = response['carrinho'];
		var ID_FICHA = response['ficha'];
		var ID_F_C_P = response['ID_F_C_P'];
		var ID_PRICING = response['ID_PRICING'];
		
		var imposto = response['imposto'];
		
		var ficha = response['ficha'];
		
		var praca = response['pracas'];
		
		var pricing = null;
		
		if ( response['pricing'] )
			pricing = response['pricing'];
		
		var base_url = new Util().options.base_url;
		
		var content = '';
		content+='<div style="width:780px;height:450px;overflow:auto;margin:5px;"><div class="titulo_view">Carrinho</div><br /><br />';
		
		content+='' +
		'<table border="0">' +
			'<tr>' +
				'<th width="150">Produto</th>' +
				'<th width="150">Carrinho</th>' +
				'<th width="150">Ficha</th>' +
				'<th width="150">Categoria</th>' +
				'<th width="150">SubCategoria</th>' +
				'<th width="150">Fabricante</th>' +
				'<th width="150">Marca</th>' +
			'</tr>' +
			'<tr>' +
				'<td align="center" width="150"><img src="' + ficha['THUMB_OBJ_FICHA'] + '" height="50" border="0"/></td>' +
				'<td align="center" width="150">' + ( (carrinho['DESC_CARRINHO']!=null) ?carrinho['DESC_CARRINHO'] :"" ) + '</td>' +
				'<td width="150">' + ( (ficha['NOME_FICHA']!=null) ?ficha['COD_FICHA'] + ' - ' + ficha['NOME_FICHA'] :"" ) + '</td>' +
				'<td width="150">' + ( (ficha['DESC_CATEGORIA']!=null) ?ficha['DESC_CATEGORIA'] :"" ) + '</td>' +
				'<td width="150">' + ( (ficha['DESC_SUBCATEGORIA']!=null) ?ficha['DESC_SUBCATEGORIA'] :"" ) + '</td>' +
				'<td width="150">' + ( (ficha['DESC_FABRICANTE']!=null) ?ficha['DESC_FABRICANTE'] :"" ) + '</td>' +
				'<td width="150">' + ( (ficha['DESC_MARCA']!=null) ?ficha['DESC_MARCA'] :"" ) + '</td>' +
			'</tr>' +
		'</table><br /><br /><br />';
		
		if(praca)
		{
			all_praca = new Array();
			
			var expectativa = response['expectativa'];			
			
			content+='<div class="titulo_view">Expectativa Venda </div><br />';			
			for(y=0;y<praca.length;y++)
			{
				var valor = '';
				
				all_praca[y] = praca[y]['ID_PRACA'];
				if(expectativa)
				{
					
					for(p=0;p<expectativa.length;p++)
					{
						if(expectativa[p]['ID_PRACA']==praca[y]['ID_PRACA']&&expectativa[p]['QTD_Q_E_P'])
						{
							valor = expectativa[p]['QTD_Q_E_P'];														
						}
					}
				}
				content+='<div class="titulo_form">'+praca[y]['DESC_PRACA']+'</div><input id="ID_PRACA_'+praca[y]['ID_PRACA']+'_'+ficha['ID_FICHA']+'" onkeypress="return new Util().soNums(event);" size="6" maxlength="5" type="text" name="" value="'+ valor +'" /><br />';
			}
			content+='<br /><br />';
			
		}
		
		
		
		if(pricing)
		{
			all_pricing = new Array();
			 
			for(i=0;i<pricing.length;i++)
			{
				all_pricing[i] = pricing[i]['ID_PRICING'];	
				
				var option = '<option value="">[Selecione]</option>';				
				for ( var j= 0; j < tipos.length ; j++ )
				{
					if($('ID_TIPO_PLANO_HD_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']))
					{
						if($('ID_TIPO_PLANO_HD_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']).value==tipos[j]['ID_TIPO_PLANO'])
						{
							option+='<option selected="selected" value="'+tipos[j]['ID_TIPO_PLANO']+'">'+tipos[j]['DESC_TIPO_PLANO']+'</option>';
						}
					}else{
						if(pricing[i]['ID_TIPO_PLANO']==tipos[j]['ID_TIPO_PLANO'])
						{
							option+='<option selected="selected" value="'+tipos[j]['ID_TIPO_PLANO']+'">'+tipos[j]['DESC_TIPO_PLANO']+'</option>';	
						}else{
							option+='<option value="'+tipos[j]['ID_TIPO_PLANO']+'">'+tipos[j]['DESC_TIPO_PLANO']+'</option>';
						}					
					}
				}
				
				if(pricing[i]['SITUACAO_PRICING']==""&&pricing[i]['SITUACAO_PRICING']==0)
				{
					var ativo = '';
					var inativo = 'checked="checked"';
				}else{
					var ativo = 'checked="checked"';
					var inativo = '';
				}
				// Ver como carregar esse cara
				 
				content+='<div class="titulo_view">Pricing '+ ((pricing[i]['ID_PRICING'] !=0 ) ? pricing[i]['ID_PRICING'] : '' )+'</div><br />' +
						'<input type="hidden" name="fcp_'+pricing[i]['ID_PRICING']+'" value="'+pricing[i]['ID_F_C_P']+'" />' +
						'<input type="hidden" name="" id="imposto" value="'+imposto['FATOR_IMPOSTO']+'" /><br />' +
						//'<input type="hidden" name="" id="ID_PRICING_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']+'" value="'+(($('ID_PRICING_HD_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA'])) ? $('ID_PRICING_HD_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']).value : pricing[i]['ID_PRICING'])+'" /><br />' +
						//'<div class="titulo_form">Quantidade:</div><input type="text" id="QTD_PRICING_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']+'" value="'+(($('QTD_PRICING_HD_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA'])) ? $('QTD_PRICING_HD_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']).value : pricing[i]['QTD_PRICING'])+'" /><br/>' +
						'<div class="titulo_form">Pre&ccedil;o &agrave Vista:</div><input type="text" id="PRECO_AVISTA_PRICING_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']+'" class="decimal" value="'+(($('PRECO_AVISTA_PRICING_HD_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA'])) ? $('PRECO_AVISTA_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA']).value : pricing[i]['PRECO_AVISTA_PRICING'])+'" /><br/>' +
						'<div class="titulo_form">Pre&ccedil;o de:</div><input type="text" id="PRECO_DE_PRICING_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']+'" class="decimal" value="'+(($('PRECO_DE_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA'])) ? $('PRECO_DE_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA']).value : pricing[i]['PRECO_DE_PRICING'])+'" /><br/>' +
						'<div class="titulo_form">Pre&ccedil;o por:</div><input id="PRECO_POR_PRICING_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']+'" type="text" class="decimal" value="'+(($('PRECO_POR_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA'])) ? $('PRECO_POR_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA']).value : pricing[i]['PRECO_POR_PRICING'])+'" />'+
						'<span style="color:#f00;font-weight:bold;" ><a style="color:#f00;font-weight:bold;" href="javascript:new Util().vazio();" onclick="new Pricing().calcValue(\''+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']+'\');">C&aacute;lculo do Economize </a></span><br/>'+
						'<div class="titulo_form">Economize:</div><input type="text" id="ECONOMIZE_PRICING_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']+'" class="decimal" value="'+(($('ECONOMIZE_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA'])) ? $('ECONOMIZE_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA']).value : pricing[i]['ECONOMIZE_PRICING'])+'" /><br/>' +
						'<div class="titulo_form">Custo Comercial:</div><input type="text" id="CUSTO_PRICING_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']+'" class="decimal" value="'+(($('CUSTO_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA'])) ? $('CUSTO_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA']).value : pricing[i]['CUSTO_PRICING'])+'" /><br/>' +
						'<div class="titulo_form">Valor de Recomposi&ccedil;&atilde;o:</div><input type="text" class="decimal" id="RECOMPOSICAO_PRICING_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']+'" value="'+(($('RECOMPOSICAO_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA'])) ? $('RECOMPOSICAO_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA']).value : pricing[i]['RECOMPOSICAO_PRICING'])+'" /><br/>' +
						'<div class="titulo_view">Plano</div><br/><br/>' +
						'<div class="titulo_form">MU:</div><input type="text" id="MU_PRICING_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']+'" class="decimal" value="'+(($('MU_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA'])) ? $('MU_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA']).value : pricing[i]['MU_PRICING'])+'" />' +
						'<span style="color:#f00;font-weight:bold;" ><a  style="color:#f00;font-weight:bold;" href="javascript:new Util().vazio();" onclick="new Pricing().atualizaMuMua(\''+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']+'\');">C&aacute;lculo MU e MUA </a></span><br/>'+
						'<div class="titulo_form">MUA:</div><input type="text" id="MUA_PRICING_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']+'" class="decimal" value="'+(($('MUA_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA'])) ? $('MUA_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA']).value : pricing[i]['MUA_PRICING'])+'" /><br/>'+
						'<div class="titulo_form">Tipo:</div><select id="ID_TIPO_PLANO_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']+'">'+option+'</select><br/>' +
						'<div class="titulo_form">Entrada:</div><input type="checkbox" ' + ( $('FLAG_ENTRADA_PRICING_'+ID_FICHA) && $('FLAG_ENTRADA_PRICING_'+ID_FICHA).value == '1' ? 'checked="checked" ' : ( (pricing != null && pricing['FLAG_ENTRADA_PRICING'] == '1') ?'checked="checked" ' :'' )) + ' id="FLAG_ENTRADA_PRICING_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']+'"/><br/>'+
						'<div class="titulo_form">Valor de Entrada:</div><input type="text" class="decimal" id="ENTRADA_PRICING_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']+'" value="'+(($('ENTRADA_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA'])) ? $('ENTRADA_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA']).value : pricing[i]['ENTRADA_PRICING'])+'" /><br/>'+
						'<div class="titulo_form">Parcelas:</div><input id="PARCELAS_PRICING_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']+'" type="text" value="'+(($('PARCELAS_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA'])) ? $('PARCELAS_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA']).value : pricing[i]['PARCELAS_PRICING'])+'" /><br/>' +
						'<div class="titulo_form">Presta&ccedil;&atilde;o:</div><input class="decimal" id="PRESTACAO_PRICING_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']+'" type="text" value="'+(($('PRESTACAO_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA'])) ? $('PRESTACAO_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA']).value : pricing[i]['PRESTACAO_PRICING'])+'" />' +
						'<span style="color:#f00;font-weight:bold;" ><a  style="color:#f00;font-weight:bold;" href="javascript:new Util().vazio();" onclick="javascript:new Pricing().getByParcela(\''+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']+'\');">C&aacute;lculo presta&ccedil;&atilde;o</a></span><br />'+
						'<div class="titulo_form">Juros AM:</div><input type="text" class="decimal" id="JUROS_AM_PRICING_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']+'" value="'+(($('JUROS_AM_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA'])) ? $('JUROS_AM_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA']).value : pricing[i]['JUROS_AM_PRICING'])+'" /><br />'+
						'<div class="titulo_form">Juros AA:</div><input type="text" class="decimal" id="JUROS_AA_PRICING_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']+'" value="'+(($('JUROS_AA_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA'])) ? $('JUROS_AA_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA']).value : pricing[i]['JUROS_AA_PRICING'])+'" /><br />'+
						'<div class="titulo_form">CET:</div><input class="decimal" id="CET_PRICING_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']+'" type="text" value="'+(($('CET_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA'])) ? $('CET_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA']).value : pricing[i]['CET_PRICING'])+'" /><br />'+
						'<div class="titulo_form">Total:</div><input type="text" class="decimal" id="TOTAL_PRICING_'+pricing[i]['ID_PRICING']+'_'+ficha['ID_FICHA']+'" value="'+(($('TOTAL_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA'])) ? $('TOTAL_PRICING_HD_'+pricing[i]['ID_PRICING']+"_"+ficha['ID_FICHA']).value : pricing[i]['TOTAL_PRICING'])+'" /><br />' ;
						
			}
		
		content+='<div id="errors"></div>';
		
		 
		
		//content+='<img src="'+ base_url +'img/salvar.gif" class="pesquisa" alt="Salvar"  title="Salvar" class="pesquisa" onClick="new Pricing().salvarPricing('+ficha['ID_FICHA']+',['+all_pricing+'])" /> ' +				
		content+='<img src="'+ base_url +'img/salvar.gif" class="pesquisa" alt="Salvar"  title="Salvar" class="pesquisa" onClick="new Pricing().validarPricing('+ficha['ID_FICHA']+',['+all_pricing+'],['+all_praca+'])" /> ' +
				'<img src="'+ base_url +'img/cancelar.gif" class="cancelar" alt="Cancelar"  title="Cancelar"  onClick="closeMessage();" />';				
			
		}else{
		content+='<img src="'+ base_url +'img/cancelar.gif" class="cancelar" alt="Cancelar"  title="Cancelar"  onClick="closeMessage();" />';			
		}		

		
		content+='</div>';
		
		displayStaticMessage(content);
		
		var decimals = $$('.decimal');
		for ( var i = 0; i < decimals.length; i++ )
		{
			new Util().formatCurrency(decimals[i], 2, ".", ",");
		}
	},
	validarPricing: function(ID_FICHA,all_pricing,all_praca )
	{
		
			var error= '';
			var e = 0;
			var cont = 0;
			var all_flag_complete = new Array();
			error += '<ul style="color:#f00;font-weight:bold;">';
						 
			for(i=0;i<all_pricing.length;i++)
			{					
					if($('PRECO_AVISTA_PRICING_'+all_pricing[i]+'_'+ID_FICHA).value <= 0 )
					{					
						error += '<li>Informe o pre&ccedil;o a vista correto.</li>';
						e++;
					}
					if($('PRECO_DE_PRICING_'+all_pricing[i]+'_'+ID_FICHA).value <= 0 )					
					{
						error += '<li>Informe o pre&ccedil;o de correto.</li>';
						e++;
					}					
					if($('PRECO_POR_PRICING_'+all_pricing[i]+'_'+ID_FICHA).value <= 0 )					
					{
						error += '<li>Informe o pre&ccedil;o por correto.</li>';
						e++;
					}					
					if($('CUSTO_PRICING_'+all_pricing[i]+'_'+ID_FICHA).value <= 0 )					
					{
						error += '<li>Informe o valor do custo correto.</li>';	
						e++;
					}
					if($('RECOMPOSICAO_PRICING_'+all_pricing[i]+'_'+ID_FICHA).value <= 0 )					
					{
						error += '<li>Informe o valor de recomposi&ccedil;&atilde;o correto.</li>';	
						e++;
					}				
					if($('ID_TIPO_PLANO_'+all_pricing[i]+'_'+ID_FICHA).value <= 0 )					
					{
						error += '<li>Informe o tipo de pagamento.</li>';	
						e++;
					}
						if($('ID_TIPO_PLANO_'+all_pricing[i]+'_'+ID_FICHA).value != 0 &&  $('ID_TIPO_PLANO_'+all_pricing[i]+'_'+ID_FICHA).value != 1)					
					{
						//Definir pricing completo ou incompleto
						if($('MU_PRICING_'+all_pricing[i]+'_'+ID_FICHA).value <= 0 )										
							cont++;
						
						if($('MUA_PRICING_'+all_pricing[i]+'_'+ID_FICHA).value <= 0 )										
							cont++;
							
						if($('FLAG_ENTRADA_PRICING_'+all_pricing[i]+'_'+ID_FICHA).checked )
						{
							if($('ENTRADA_PRICING_'+all_pricing[i]+'_'+ID_FICHA).value <= 0 )										
								cont++;
						}
						
						if($('PARCELAS_PRICING_'+all_pricing[i]+'_'+ID_FICHA).value <= 0 )										
							cont++;	
																
						if($('PRESTACAO_PRICING_'+all_pricing[i]+'_'+ID_FICHA).value <= 0 )										
							cont++;	
							
						if($('JUROS_AM_PRICING_'+all_pricing[i]+'_'+ID_FICHA).value <= 0 )										
							cont++;										
							
						if($('JUROS_AA_PRICING_'+all_pricing[i]+'_'+ID_FICHA).value <= 0 )										
							cont++;	
							
						if($('CET_PRICING_'+all_pricing[i]+'_'+ID_FICHA).value <= 0 )										
							cont++;	
						
						if($('TOTAL_PRICING_'+all_pricing[i]+'_'+ID_FICHA).value <= 0 )										
							cont++;
					}
					
					if(cont==0)
						all_flag_complete[i] = 1;
					else
						all_flag_complete[i] = 0;										
			}
			
			error += '</ul>';				
				
		if(e >0)		
		{
			$('errors').innerHTML = error;
		}else{
			this.salvarPricing(ID_FICHA,all_pricing,all_flag_complete,all_praca );
		}				
	},
	salvarPricing: function(ID_FICHA,all_pricing,all_flag_complete,all_praca)
	{		
		if(all_pricing)
		{
			for(i=0;i<all_pricing.length;i++)
			{
				
				//$('container_'+ID_FICHA).innerHTML+='<input type="hidden" id="ID_PRICING_HD_'+all_pricing[i]+'_'+ID_FICHA+'" name="FICHAS['+ID_FICHA+'][PRICING]['+all_pricing[i]+'][ID_PRICING]" value="'+$('ID_PRICING_'+all_pricing[i]+'_'+ID_FICHA).value+'" />';
				//$('container_'+ID_FICHA).innerHTML+='<input type="hidden" id="QTD_PRICING_HD_'+all_pricing[i]+'_'+ID_FICHA+'" name="FICHAS['+ID_FICHA+'][PRICING]['+all_pricing[i]+'][QTD_PRICING]" value="'+$('QTD_PRICING_'+all_pricing[i]+'_'+ID_FICHA).value+'" />';
				$('container_'+ID_FICHA).innerHTML+='<input type="hidden" id="FLAG_COMPLETE_PRICING_HD_'+all_pricing[i]+'_'+ID_FICHA+'" name="FICHAS['+ID_FICHA+'][PRICING]['+all_pricing[i]+'][FLAG_COMPLETE_PRICING]" value="'+all_flag_complete[i]+'" />';
				$('container_'+ID_FICHA).innerHTML+='<input type="hidden" id="PRECO_AVISTA_PRICING_HD_'+all_pricing[i]+'_'+ID_FICHA+'" name="FICHAS['+ID_FICHA+'][PRICING]['+all_pricing[i]+'][PRECO_AVISTA_PRICING]" value="'+$('PRECO_AVISTA_PRICING_'+all_pricing[i]+"_"+ID_FICHA).value+'" />';
				$('container_'+ID_FICHA).innerHTML+='<input type="hidden" id="PRECO_DE_PRICING_HD_'+all_pricing[i]+'_'+ID_FICHA+'" name="FICHAS['+ID_FICHA+'][PRICING]['+all_pricing[i]+'][PRECO_DE_PRICING]" value="'+$('PRECO_DE_PRICING_'+all_pricing[i]+"_"+ID_FICHA).value+'" />';
				$('container_'+ID_FICHA).innerHTML+='<input type="hidden" id="PRECO_POR_PRICING_HD_'+all_pricing[i]+'_'+ID_FICHA+'" name="FICHAS['+ID_FICHA+'][PRICING]['+all_pricing[i]+'][PRECO_POR_PRICING]" value="'+$('PRECO_POR_PRICING_'+all_pricing[i]+"_"+ID_FICHA).value+'" />';
				$('container_'+ID_FICHA).innerHTML+='<input type="hidden" id="ECONOMIZE_PRICING_HD_'+all_pricing[i]+'_'+ID_FICHA+'" name="FICHAS['+ID_FICHA+'][PRICING]['+all_pricing[i]+'][ECONOMIZE_PRICING]" value="'+$('ECONOMIZE_PRICING_'+all_pricing[i]+"_"+ID_FICHA).value+'" />';
				$('container_'+ID_FICHA).innerHTML+='<input type="hidden" id="CUSTO_PRICING_HD_'+all_pricing[i]+'_'+ID_FICHA+'" name="FICHAS['+ID_FICHA+'][PRICING]['+all_pricing[i]+'][CUSTO_PRICING]" value="'+$('CUSTO_PRICING_'+all_pricing[i]+"_"+ID_FICHA).value+'" />';
				$('container_'+ID_FICHA).innerHTML+='<input type="hidden" id="RECOMPOSICAO_PRICING_HD_'+all_pricing[i]+'_'+ID_FICHA+'" name="FICHAS['+ID_FICHA+'][PRICING]['+all_pricing[i]+'][RECOMPOSICAO_PRICING]" value="'+$('RECOMPOSICAO_PRICING_'+all_pricing[i]+"_"+ID_FICHA).value+'" />';
				$('container_'+ID_FICHA).innerHTML+='<input type="hidden" id="MU_PRICING_HD_'+all_pricing[i]+'_'+ID_FICHA+'" name="FICHAS['+ID_FICHA+'][PRICING]['+all_pricing[i]+'][MU_PRICING]" value="'+$('MU_PRICING_'+all_pricing[i]+"_"+ID_FICHA).value+'" />';
				$('container_'+ID_FICHA).innerHTML+='<input type="hidden" id="MUA_PRICING_HD_'+all_pricing[i]+'_'+ID_FICHA+'" name="FICHAS['+ID_FICHA+'][PRICING]['+all_pricing[i]+'][MUA_PRICING]" value="'+$('MUA_PRICING_'+all_pricing[i]+"_"+ID_FICHA).value+'" />';
				$('container_'+ID_FICHA).innerHTML+='<input type="hidden" id="ID_TIPO_PLANO_HD_'+all_pricing[i]+'_'+ID_FICHA+'" name="FICHAS['+ID_FICHA+'][PRICING]['+all_pricing[i]+'][ID_TIPO_PLANO]" value="'+$('ID_TIPO_PLANO_'+all_pricing[i]+"_"+ID_FICHA).value+'" />';				
				$('container_'+ID_FICHA).innerHTML+='<input type="hidden" id="ENTRADA_PRICING_HD_'+all_pricing[i]+'_'+ID_FICHA+'" name="FICHAS['+ID_FICHA+'][PRICING]['+all_pricing[i]+'][ENTRADA_PRICING]" value="'+$('ENTRADA_PRICING_'+all_pricing[i]+"_"+ID_FICHA).value+'" />';
				$('container_'+ID_FICHA).innerHTML+='<input type="hidden" id="PARCELAS_PRICING_HD_'+all_pricing[i]+'_'+ID_FICHA+'" name="FICHAS['+ID_FICHA+'][PRICING]['+all_pricing[i]+'][PARCELAS_PRICING]" value="'+$('PARCELAS_PRICING_'+all_pricing[i]+"_"+ID_FICHA).value+'" />';
				$('container_'+ID_FICHA).innerHTML+='<input type="hidden" id="PRESTACAO_PRICING_HD_'+all_pricing[i]+'_'+ID_FICHA+'" name="FICHAS['+ID_FICHA+'][PRICING]['+all_pricing[i]+'][PRESTACAO_PRICING]" value="'+$('PRESTACAO_PRICING_'+all_pricing[i]+"_"+ID_FICHA).value+'" />';
				$('container_'+ID_FICHA).innerHTML+='<input type="hidden" id="JUROS_AM_PRICING_HD_'+all_pricing[i]+'_'+ID_FICHA+'" name="FICHAS['+ID_FICHA+'][PRICING]['+all_pricing[i]+'][JUROS_AM_PRICING]" value="'+$('JUROS_AM_PRICING_'+all_pricing[i]+"_"+ID_FICHA).value+'" />';
				$('container_'+ID_FICHA).innerHTML+='<input type="hidden" id="JUROS_AA_PRICING_HD_'+all_pricing[i]+'_'+ID_FICHA+'" name="FICHAS['+ID_FICHA+'][PRICING]['+all_pricing[i]+'][JUROS_AA_PRICING]" value="'+$('JUROS_AA_PRICING_'+all_pricing[i]+"_"+ID_FICHA).value+'" />';
				$('container_'+ID_FICHA).innerHTML+='<input type="hidden" id="CET_PRICING_HD_'+all_pricing[i]+'_'+ID_FICHA+'" name="FICHAS['+ID_FICHA+'][PRICING]['+all_pricing[i]+'][CET_PRICING]" value="'+$('CET_PRICING_'+all_pricing[i]+"_"+ID_FICHA).value+'" />';
				$('container_'+ID_FICHA).innerHTML+='<input type="hidden" id="TOTAL_PRICING_HD_'+all_pricing[i]+'_'+ID_FICHA+'" name="FICHAS['+ID_FICHA+'][PRICING]['+all_pricing[i]+'][TOTAL_PRICING]" value="'+$('TOTAL_PRICING_'+all_pricing[i]+"_"+ID_FICHA).value+'" />';
				$('container_'+ID_FICHA).innerHTML+='<input type="hidden" id="FLAG_ENTRADA_PRICING_HD_'+all_pricing[i]+'_'+ID_FICHA+'" name="FICHAS['+ID_FICHA+'][PRICING]['+all_pricing[i]+'][TOTAL_PRICING]" value="'+$('TOTAL_PRICING_'+all_pricing[i]+"_"+ID_FICHA).value+'" />';
				
				
			}
		}
		
		if(all_praca)
		{
			for(j=0;j<all_praca.length;j++)
			{
				$('container_'+ID_FICHA).innerHTML+='<input type="hidden" id="QTD_Q_E_P_HD_'+all_praca[j]+'_'+ID_FICHA+'" name="FICHAS['+ID_FICHA+'][EXPECTATIVA]['+all_praca[j]+']" value="'+$('ID_PRACA_'+all_praca[j]+'_'+ID_FICHA).value+'" />';
			}
		}
		closeMessage();
	},
	remover: function(div, id_pricing)
	{		
		div.parentNode.remove();
		
		if( !isNaN( id_pricing))
			new Util().call('pricing/delete', {'id_pricing':id_pricing}, new Util().vazio);
	}	
	
});