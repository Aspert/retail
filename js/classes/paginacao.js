var Paginacao = new Class({

	selecaoFicha: function(tipo)
	{				
		if(tipo == 'carrinho')
		{
			$('formulario').style.display='none';
			$('carrinho').style.display='block';
		}
		if(tipo == 'formulario')
		{		
			$('formulario').style.display='block';
			$('carrinho').style.display='none';
		}
	},
	
	/*Cria o link para as paginas*/
	getNPagina: function(qtdPagina)
	{
		var linkPagina = '';
		var divPagina = '';
		var pagina = new Array();

		for( var i= 1; i <=qtdPagina ; i++ )
		{	
			if ( !$('fieldset_pagina_' + i) )
			{
				linkPagina+='<a href="javascript:new Util().vazio()" class="bt_pagina" onClick="new Paginacao().atualizaInput(\''+ i +'\', this);">'+ i +'</a> ';
				
				divPagina+='' +
				'<fieldset style="display:none;visibility:hidden" id="fieldset_pagina_' + i + '">' +
					'<legend>Fichas selecionadas para p&aacute;gina <b>' + i + '</b></legend>' +
					'<table id="lista_pagina_' + i + '">' +
						'<tbody>' +			
							'<tr>' +			
								'<th>Produto</th>' +
								'<th>Detalhes Ficha</th>' +
								'<th>Excluir</th>' +
								'<th>Ficha</th>' +
								'<th>PV</th>' +
								'<th>Pricing</th>' +
							'</tr>' +
						'</tbody>' +
					'</table>' +
				'</fieldset>';
			}
			
		}
		if ( linkPagina != '' )
			$('linkPagina').empty().innerHTML = linkPagina;
		if ( divPagina != '' )
			$('paginas').innerHTML+= divPagina;
		
		var bt_pagina = $$('.bt_pagina');

		new Paginacao().atualizaInput(1, bt_pagina[0]);
	},
	
	/*Atualiza o valor do input para identificar a pagina que a ficha vai se relacionar*/
	atualizaInput: function(n_pagina, bt) 
	{			
		
		bt_pagina = $$('.bt_pagina');
		for ( var i = 0; i < bt_pagina.length; i++ )
		{
			bt_pagina[i].style.fontWeight = 'normal';
			bt_pagina[i].style.color = '';
		}
			
		bt.style.fontWeight = 'bold';
		bt.style.color = '#000';		
		
		$('NPAGINA').value = n_pagina;
		
		var paginas = $('paginas');		
		
		if(paginas != null)
		{
			for ( i = 0; i < paginas.childNodes.length; i++ )
			{
				var pagina = paginas.childNodes[i];
				
				if ( pagina.tagName.toLowerCase() == 'fieldset' )
				{
					pagina.style.display = 'none';
					pagina.style.visibility = 'hidden';
				}		
			}
		}
		$('fieldset_pagina_' + n_pagina).style.display = "";
		$('fieldset_pagina_' + n_pagina).style.visibility = "visible";		
	},
	
	
	selecionaPagina: function(id_f_c_p)
	{				
		new Util().call('fcp/getByIdAndFicha', {'ID_F_C_P':id_f_c_p}, new Paginacao().mountListFicha);
	},	
	mountListFicha: function (response)
	{				
		var pagina = $('NPAGINA').value;
		
		var ficha = response['ficha'];
		
		var tr = document.createElement('TR');
		tr.id = "ficha_" + ficha['ID_FICHA'];
		
		var td0 = document.createElement('TD');
		td0.align="center";
		td0.innerHTML = '' +
		'<input type="hidden" name="FICHAS[' + pagina + '][' + ficha['ID_FICHA'] + '][FICHA][ID_FICHA]" value="' + ficha['ID_FICHA'] + '" />' +
		'<input type="hidden" name="FICHAS[' + pagina + '][' + ficha['ID_FICHA'] + '][F_C_P][ID_F_C_P]" value="' + ((ficha['ID_F_C_P']!=null) ?ficha['ID_F_C_P'] :'' ) + '" />' +		
		'<img height="50" src="'+ ficha['THUMB_OBJ_FICHA']+ '" border="0"/>';
		tr.appendChild(td0);
		
		var td1 = document.createElement('TD');
		td1.align="center";
		td1.innerHTML = '<a href="javascript:new Util().vazio()" onClick="new Ficha().detalhe('+ ficha['ID_FICHA'] +')"><img src="' + new Util().options.base_url + 'img/visualizar.gif" border="0"/></a>'
		tr.appendChild(td1);
		
		var td2 = document.createElement('TD');
		td2.align="center";
		td2.innerHTML = '<a href="javascript:new Util().vazio()" onClick="$(this.parentNode.parentNode).remove(); new Paginacao().countFichas(\'som\');new Util().alteraDisplay(\'carrinho_'+ ficha['ID_F_C_P'] +'\', \'\')"><img src="'+ new Util().options.base_url + 'img/excluir.gif" border="0"/></a>';
		tr.appendChild(td2);
		
		var td3 = document.createElement('TD');
		td3.innerHTML = ficha['COD_FICHA'] +' - '+ ficha['NOME_FICHA'];
		tr.appendChild(td3);
		
		var td4 = document.createElement('TD');
		td4.innerHTML = ficha['PV_ENC_FICHA'];
		tr.appendChild(td4);
				
		if(!$("ficha_" + ficha['ID_FICHA']))
		{
			$('lista_pagina_' + pagina).childNodes[0].appendChild(tr);
		}		
			
		new Paginacao().countFichas('sub');	

	},
	countFichas: function(oper)
	{
		if(oper=='sub')
		{
			if($('qtd_fichas').value>0)
			{
				$('qtd_fichas').value = parseInt($('qtd_fichas').value)-1;
			}
		}else{
			$('qtd_fichas').value = parseInt($('qtd_fichas').value)+1;
		}
	},
	selecionaAll: function()
	{
		var inputs = $$('.selecionaAll');		
	
		for ( var i = 0; i < inputs.length; i++ )
		{
			new Paginacao().selecionaPagina(inputs[i].value); 
			new Util().alteraDisplay('carrinho_' + inputs[i].value,'none');
			new Paginacao().countFichas('sub');
		}
			
	},
	habilitasalvar: function()
	{
		/*if($('briefing').value!=''&& $('descricao').value!='')
		{
			$('salvar_agencia_link').style.display = 'block';
		}else{
			$('salvar_agencia_link').style.display = 'none';
		}*/
	},
	salvarPaginacao: function()
	{
		var erro = 0;
		var mensagem = "";
		if($('briefing').value=="")
		{
			erro++;
			mensagem+='Necessário informar o briefing\n';
		}
		if($('descricao').value=='')
		{
			erro++;
			mensagem+='Necessário informar a descricao\n';
		}
		if($('qtd_fichas').value!=0)
		{
			erro++;	
			mensagem+='Necessário selecionar a quantidade de fichas configurada\n';
		}
		if(erro==0)
		{
			new Util().confirmBox('Confirma salvar e enviar o email ?','submit',0,true);
		}else{
			alert(mensagem);			
		}
	}

});


/******************************************************************************/
/*************** OBJETO ESTATICO DE RICH UI DE PAGINACAO **********************/
/*************** Adicionado por Hugo Silva em 2009-07-06 **********************/
/******************************************************************************/
/**
 * Inicia os elementos de tela da paginacao
 */
Paginacao.init = function(){
	// lista de instancias de ordenacao
	this.sortItems = [];
	// indica a pagina ativa
	this.paginaAtiva = -1;
	// esconde as paginas com as fichas
	var pgs = $$('.paginas');
	if(pgs.length > 0){
		pgs.setStyle('display','none');
		$(pgs[0]).setStyle('display','');
	}
	
	// coloca as animacoes nos botoes das paginas
	$$('ul.paginacao li').each(function(el){
		$(el).setOpacity(.3);
		$(el).addEvent('mouseover', function(el){
			var id = this.id.replace('pagina','');
			if(Paginacao.paginaAtiva != id){
				new Fx.Styles($(this),{duration: 100}).start({opacity: 1});
			}
			
		}).addEvent('mouseout', function(el) {
			var id = this.id.replace('pagina','');
			if(Paginacao.paginaAtiva != id){
				new Fx.Styles($(this),{duration: 100}).start({opacity: .3});
			}
		});
	});
	
	// escolhe as areas onde os itens poderao
	// ser dropados
	var dragOpt = {
		droppables: $$('.paginas'),
		onComplete: function(){
			clone.remove();
		}
	};
	
	// alteras as opcoes das areas de dropagem
	$$('.paginas').addEvents({
		'drop': function(){
			// pega os dados da ficha
			var ficha = Paginacao.getFicha(clone.ficha);
			// oculta o elemento da lista de fichas
			$('item_'+ficha.ID_FICHA).setStyle('display','none');
			// adiciona o item na pagina
			Paginacao.addItem( this, ficha );
			// volta a opacidade ao normal
			this.setOpacity(1);
		},
		
		'over': function(){
			this.setOpacity(.3);
		},
		
		'leave': function(){
			this.setOpacity(1);
		}
	});	
	
	var clone;
	
	// deixa os itens arrastaveis
	$$('.fichaItem').addEvent('mousedown', function(e){
		// paramos o evento padrao
		e = new Event(e).stop();
		// clonamos o item escolhido
		clone = this.clone();
		// codigo da ficha
		clone.ficha = clone.id.replace('item_','');
		// altera o id do clone
		clone.id = 'clone_'+(new Date().getTime());
		// pega as coordenadas do elemento original
		var coord = this.getCoordinates();
		// adiciona o scrollTop do container
		coord.top -= $($$('.fichasContainer')[0]).scrollTop;

		// acertamos a posicao para a mesma do original
		clone.setStyles( coord )
			// colocamos a posicao em absoluta e alteramos a opacidade
			.setStyles({position: 'absolute', opacity: .7})
			// coloca o clone no documento
			.inject(document.body);
			
		// torna o clone arrastavel
		// e inicia o drag manualmente
		clone.makeDraggable(dragOpt).start(e);
	});
	
	// abre a pagina 1
	this.abrePagina(1);
	
	///////////////////////////////////////////////////////////////////////
	// aqui iniciamos os itens que vieram do banco
	///////////////////////////////////////////////////////////////////////
	// excluir item
	$$('.excluir').addEvent('click', Paginacao.removeItemHandler);
	// alterar layout
	$$('.layout').addEvent('click', Paginacao.abreLayoutHandler);
	// visualizar os detalhes
	$$('.visualizar').addEvent('click', Paginacao.mostraDetalheHandler);
	// para cada item adicionado do banco
	$$('.linha_banco').each(function(item) {
		// esconde o item criado dinamicamente na lista de disponiveis
		$('item_' + item.id.replace('ficha_','')).setStyle('display','none');
	});
	// para cada item da paginacao, quando clicar
	$$('ul.paginacao li').addEvent('click', function(){
		// pega o id
		var id = this.id.replace('pagina','');
		// se nao for a pagina ativa
		if(Paginacao.paginaAtiva != id){
			// abre a pagina
			Paginacao.abrePagina( id );
		}
	});
	
	// permite que os itens sejam reposicionados
	this.startSort();
}

/**
 * Recupera os dados de uma ficha pelo ID
 * @param int id Codigo da ficha
 * @return Object dados da ficha
 */
Paginacao.getFicha = function(id){
	// elemento a ser retornado
	var el = null;
	// para cada elemento da lista
	this.itens.each(function(item){
		// se os id forem iguais
		if(item.ID_FICHA == id){
			// indica o elemento a ser retornado
			el = item;
		}
	});
	// retorna o elemento encontrado
	return el;
}

/**
 * Adiciona um item na tabela
 * @param container local que recebera o item
 * @param ficha dados da ficha 
 * @return void
 */
Paginacao.addItem = function(container, ficha){
	// pega a tabela onde sera colocado o item
	var table = $(container).getElement('.listagem');
	// clona o modelo
	var line = this.modelo.clone();
	// pega o numero da pagina
	var pg = table.id.replace(/^.*(\d+)$/, '$1');
	
	// ajusta os atributos do item
	line.id='ficha_'+ficha.ID_FICHA;
	line.getElement('.idFicha').innerHTML = ficha.ID_FICHA;
	line.getElement('.thumb').src = ficha.THUMB_OBJ_FICHA;
	line.getElement('.codFicha').value = ficha.ID_FICHA;
	line.getElement('.codFicha').name = 'FICHAS['+pg+']['+ficha.ID_FICHA+'][FICHA][ID_FICHA]';
	line.getElement('.ordem').name = 'FICHAS['+pg+']['+ficha.ID_FICHA+'][FICHA][ORDEM]';
	line.getElement('.fcp').name = 'FICHAS['+pg+']['+ficha.ID_FICHA+'][F_C_P][ID_F_C_P]';
	line.getElement('.nome').innerHTML = ficha.COD_FICHA + ' - ' + ficha.NOME_FICHA;
	line.getElement('.pv').innerHTML = ficha.PV_ENC_FICHA ? ficha.PV_ENC_FICHA : '';
	line.getElement('.excluir').addEvent('click',this.removeItemHandler);
	line.getElement('.layout').addEvent('click',this.abreLayoutHandler);
	line.getElement('.visualizar').addEvent('click',this.mostraDetalheHandler);
	line.getElement('.handler').addClass('handler_'+pg);
	// insere a linha
	line.inject(table);
	
	// inicia a opcao de ordenacao dos itens
	this.startSort();
}

/**
 * Abre uma pagina contendo uma lista de itens
 */
Paginacao.abrePagina = function(numero){
	// oculta todas as paginas
	$$('.paginas').setStyle('display','none');
	// exibe a pagina selecionada
	$('itens_pagina_'+numero).setStyle('display','');
	// deixa todos os elementos da paginacao com opacidade baixa
	$$('ul.paginacao li').setOpacity(.3);
	// deixa o elemento selecionado com opacidade normal
	$('pagina'+numero).setOpacity(1);
	// indica a pagina ativa
	this.paginaAtiva = numero;
}

/**
 * Handler para remover um item da lista
 */
Paginacao.removeItemHandler = function(){
	// pega o elemento
	var p = $(this);
	// loop ate achar a linha
	while(true){
		// se encontrou a linha
		if(p.tagName.toLowerCase() == 'tr'){
			// pega o ID da ficha
			var id = p.id.replace('ficha_','');
			// mostra o item na lista de itens disponiveis
			$('item_'+id).setStyle('display','');
			// remove o item da tabela
			p.remove();
			// para o loop
			break;
		}
		// se ainda nao encontrou, pega o item pai
		p = $(p).parentNode;
	}
}

/**
 * Abre o flex de layout da fichaa
 */
Paginacao.abreLayout = function(idficha){
	displayMessagewithparameter((new Util().options.site_url)+'/ficha/ficha_flex/'+idficha,990,650);
}

/**
 * mostra os detalhes da ficha
 */
Paginacao.mostraDetalhe = function(idficha){
	new Ficha().detalhe(idficha);
}

/**
 * handler para quando clicar no icone de layout
 */
Paginacao.abreLayoutHandler = function(){
	Paginacao.abreLayout(this.parentNode.parentNode.id.replace('ficha_',''));
}

/**
 * handler para quando clicar no icone detalhes
 */
Paginacao.mostraDetalheHandler = function(){
	Paginacao.mostraDetalhe(this.parentNode.parentNode.id.replace('ficha_',''));
}

/**
 * Inicia a opcao de ordenacao dos itens
 * @return void
 */
Paginacao.startSort = function(){
	// enquanto houverem instancias na lista
	while(this.sortItems.length > 0){
		// remove da lista e desfaz os bindings
		this.sortItems.shift().detach();
	}
	// para cada pagina encontrada
	$$('.listagem').each(function(tbl) {
		// cria uma nova instancia de sortabel
		var sr = new Sortables(tbl.id, {
			// indica os elementos que servirao de handlers 
			// para mover a linha
			handles: '.handler_' + tbl.id.replace(/^.*(\d+)$/,'$1'),
			// quando completar (drop)
			onComplete: function(el){
				// altera o valor do campo ordem
				var i = 1;
				$(tbl).getChildren().each(function(row){
					$(row).getElement('.ordem').value = i++;
				})
			}
		});

		Paginacao.sortItems.push(sr);
	});
	
	// agora vamos atualizar a ordem dos elementos
	$$('.listagem').each(function(tbl){
		var i=1;
		$(tbl).getChildren().each(function(row){
			$(row).getElement('.ordem').value = i++;
		})
	})
}