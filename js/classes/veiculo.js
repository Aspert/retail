var Veiculo = new Class({
	getAllByMidia: function(id_midia, response, obj)
	{
		obj = (obj!=null) ?obj :'';
		
		id_praca = $('ID_PRACA').value;
		
		new Util().call('veiculo/getAllByMidia', {'id_midia': id_midia, 'obj': obj,'id_praca':id_praca}, response);
	},
	
	mountDropdown: function(response)
	{
		var veiculos = response['veiculos'];
		 
		var obj = $(response['obj']).empty();
		var option = document.createElement('OPTION');
		option.value = '';
		option.innerHTML = '[Selecione]';
		obj.appendChild(option);

		if( veiculos )
		{
			for ( var i = 0; i < veiculos.length; i++ )
			{
				var option = document.createElement('OPTION');
				option.value = veiculos[i]['ID_VEICULO'];
				option.innerHTML = veiculos[i]['DESC_VEICULO'];
				obj.appendChild(option);
			}
		}
	},
	getAllByTarget: function(id_target,response,obj)
	{
		obj = (obj!=null) ?obj :'';
		
		id_praca = $('ID_PRACA').value;
		
		id_midia = $('ID_MIDIA').value;
		
		new Util().call('veiculo/getAllByTarget', {'id_target': id_target,'id_midia': id_midia ,'obj': obj,'id_praca':id_praca}, response);
		
	}
});