var Objeto = new Class({
	
	formPesquisa: function(response)
	{
		var content = ''+
			'<div style="width:800px;height:500px;overflow:auto; margin:5px;">'+
			'<div class="titulo_view">Pesquisa de objetos</div><br />' +
			'<div class="titulo_form">Nome do Arquivo:</div><input type="text" class="inputs" id="NOME_OBJETO" /><br/>' +
			'<div class="titulo_form">C&oacute;digo Produto:</div><input type="text" class="inputs" id="COD_OBJETO" /><br/>' +
			'<div class="titulo_form">Marca:</div><input type="text" class="inputs" id="MARCA_OBJETO" /><br/>' +
			'<div class="titulo_form">Per&iacute;odo cria&ccedil;&atilde;o:</div>' +
			'<input type="text" class="inputs" name="PERIODO_INICIAL" id="PERIODO_INICIAL" size="10" readonly="readonly" /> ' +
			'<input type="button" value="..." onclick="displayCalendar($(\'PERIODO_INICIAL\'),\'dd/mm/yyyy\',this);"/>' +
			' at&eacute; ' +
			'<input type="text" class="inputs" id="PERIODO_FINAL" size="10" readonly="readonly" /> ' +
			'<input type="button" value="..." onclick="displayCalendar($(\'PERIODO_FINAL\'),\'dd/mm/yyyy\',this);"/>' +
			'<br/>' +
			'<div class="titulo_form">Tipo:</div>' +
			'<select class="inputs" id="TIPO">'+
				'<option selected="selected" value="">[Selecione]</option>' + 
				'<option value="SELO">Selo</option>' +
				'<option value="LOGO">Logo</option>' +
				'<option value="SPLASH">Splash</option>' +
				'<option value="PRODUTO">Produto</option>' +
				'<option value="ACESSORIO">Acess&oacute;rio</option>' +
			'</select> <br/>' +
			'<div class="titulo_form">Categoria:</div>' +
			'<select class="inputs" id="_ID_CATEGORIA" onChange="new Categoria().getAllSub(new Categoria().responseSub , this.value, \'_ID_SUBCATEGORIA\', \'DESC_SUBCATEGORIA\');"></select><br />' +
			'<div class="titulo_form">Subcategoria:</div>'+
			'<select class="inputs" id="_ID_SUBCATEGORIA">'+
				'<option selected="selected" value="">[Selecione]</option>' + 				
			'</select> <br/>' +
			'<div class="titulo_form">Key Words:</div>'+
			'<textarea class="inputs" id="KEYWORD_OBJETO" cols="40" rows="2"></textarea><br/>' +
			'<input type="image" alt="Pesquisa" title="Pesquisa" src="'+ new Util().options.base_url +'img/lupa.gif" value="Pesquisar" id="pesquisa" style="margin-bottom:0px; margin-right:5px" class="pesquisa_add btn" onClick="new Objeto().pesquisar()">' +
			'<a href="javascript:new Util().vazio()" alt="Cancelar" title="Cancelar" onClick="closeMessage()"><img src="'+ new Util().options.base_url +'img/cancelar.gif" border="0" /></a>' +
			'<div class="titulo_view">Sele&ccedil;&atilde;o de objetos</div><br /><br />' +
			'<div id="resultado">' + 
			'</div>' +
			'</div>';	
		
		displayStaticMessage(content);
		
		var categorias = response['categorias'];
		
		var comboCategoria = $('_ID_CATEGORIA').empty();
		
		var option = document.createElement('OPTION');
		option.innerHTML = '[Selecione]';
		option.value = '';
		comboCategoria.appendChild(option);
		
		for ( var i = 0; i < categorias.length ; i++ )
		{										
			var option = document.createElement('OPTION');
			
			option.innerHTML = categorias[i]['DESC_CATEGORIA'];
			option.value = categorias[i]['ID_CATEGORIA'] + '#' + categorias[i]['DESC_CATEGORIA'];
			comboCategoria.appendChild(option);
		}
	},
	
	pesquisar: function(pagina)
	{
		var inputs = $$('.inputs');
		
		var content = '{';
		
		for ( var i = 0; i < inputs.length; i++ )
			content+= ( (i==0) ?'' :',' ) + '\'' + inputs[i].id + '\':' + '\'' + inputs[i].value + '\'';
		
		if(pagina != null)
			content+= ',\'pagina\':\'' + pagina + '\'';
		
		content+= '}';
		
		eval('var data = ' + content);
		
		new Util().call('objeto/pesquisar', data, this.responsePesquisar, 'post');
	},
	
	responsePesquisar: function(response)
	{
		var objetos = response['objetos'];
		var j = 0;		
		var linhas = '';
		
		for( var i= 0; i < objetos.length ; i++ )
		{			
			linhas+='<tr>';
			linhas+='<td align="center"><input type="checkbox" class="selecao" value="'+ j +' " >';
			
			for ( var indice in objetos[i] )
				linhas+='<input type="hidden" class="' + indice + '_'+ j +'" value="'+ objetos[i][indice] +' " >';
				
			linhas+='</td>';
			linhas+='<td><img height="25" width="25" src="' + objetos[i]['THUMB'] + '#' + Math.random() + '"></td>';
			linhas+='<td>' + objetos[i]['NOME'] + '</td>';
			linhas+='<td>' + objetos[i]['CODIGO'] + '</td>';
			linhas+='<td>' + objetos[i]['MARCA'] + '</td>';
			linhas+='<td>' + objetos[i]['DATA_DE_CRIACAO'] + '</td>';
			linhas+='<td>' + objetos[i]['CATEGORIA'] + '</td>';						
			linhas+='<td>' + objetos[i]['SUBCATEGORIA'] + '</td>';						
			linhas+='<td>' + objetos[i]['KEYWORDS'] + '</td>';						
			linhas+='<td>' + objetos[i]['TIPO'] + '</td>';						
			linhas+='</tr>';
			
			j++;
		}
		
		var content = '' +							
			'<table>' +			
				'<tr>' +
					'<th>Selecionar</th>' +
					'<th>Produto</th>' +
					'<th>Nome</th>' +
					'<th>C&oacute;digo Produto</th>' +
					'<th>Marca</th>' +
					'<th>Data Cria&ccedil;&atilde;o</th>' +
					'<th>Categoria</th>' +
					'<th>Sub-Categoria</th>' +
					'<th>Keywords</th>' +
					'<th>Tipo</th>' +
				'</tr>' +
				linhas +
			'</table> ' +
			'<div id="paginacao"></div>' +
			'<br/>' +
			'<img src="'+ new Util().options.base_url +'img/check.gif" class="pesquisa" alt="Confimar"  title="Confirmar"  onClick="(new Objeto().getSelected());"/>' +			
		'';
				
		$('resultado').innerHTML = content;
		
		var content = '';
		var totalPagina = response['totalPagina'];
		var pagina = response['pagina'];
		/*	
		for ( var i = 0; i < totalPagina; i++ )
		{
			if (pagina == i)
				content+= "<b>" + (i+1) + "</b>\n";
			else
				content+= '<a href="javascript:new Util().vazio()" onClick="new Objeto().pesquisar(' + i + ')">' + (i+1) + "</a>\n"; 
		}
		
		$('paginacao').innerHTML = content;
		*/
		$('paginacao').innerHTML = new Util().doPaginacao('<a href="javascript:new Util().vazio()" onClick="new Objeto().pesquisar(#PAGINA#)">#LABEL#</a>', totalPagina, pagina);
	},
	
	responseSub: function(response)
	{
		
		
		var subCategorias =  response['data'];
		
		var option = document.createElement('OPTION');
		option.innerHTML = '[Selecione]';
		
		$('ID_SUBCATEGORIA').empty().appendChild(option);
		
		for ( var i = 0; i < subCategorias.length ; i++ )
		{													
			var option = document.createElement('OPTION');
			option.value = subCategorias[i]['ID_SUBCATEGORIA'];
			option.innerHTML = subCategorias[i]['DESC_SUBCATEGORIA'];

			$('ID_SUBCATEGORIA').appendChild(option);
		}			
	} ,
	getSelected: function()
	{
		var selecao = $$('.selecao');		
		var j = 0 ;
		var objetos = new Array();		
		var linhas = '';
		
		for ( var i = 0; i < selecao.length; i++ )
		{				
			if(selecao[i].checked)
			{					
				j = selecao[i].value;					 					
				
				var tr = document.createElement("TR");
				
				var td0 = document.createElement("TD");
				td0.innerHTML = '' +
					'<input type="hidden" name="OBJ_FICHA[FILE_OBJ_FICHA][]" value="' + $$('.FILE_'+j)[0].value + '" />' +
					'<input type="hidden" name="OBJ_FICHA[PATH_OBJ_FICHA][]" value="' + $$('.PATH_'+j)[0].value + '" />' +
					'<input type="hidden" name="OBJ_FICHA[THUMB_OBJ_FICHA][]" value="' + $$('.THUMB_'+j)[0].value + '" />' +
					'<input type="hidden" name="OBJ_FICHA[ID_TIPO_OBJETO][]" value="' + $$('.TIPO_'+j)[0].value + '" />' +
					'<img src="' + $$('.THUMB_'+j)[0].value +  '" height="50" border="0" />';
				td0.align = "center";
				tr.appendChild(td0);
				
				var td1 = document.createElement("TD");
				td1.innerHTML = $$('.NOME_'+j)[0].value;
				tr.appendChild(td1);
				
				var td2 = document.createElement("TD");
				td2.innerHTML = $$('.CODIGO_'+j)[0].value;
				tr.appendChild(td2);
				
				var td3 = document.createElement("TD");
				td3.innerHTML = $$('.MARCA_'+j)[0].value;
				tr.appendChild(td3);
				
				var td4 = document.createElement("TD");
				td4.innerHTML = $$('.DATA_CRIACAO_'+j)[0].value;
				tr.appendChild(td4);
				
				var td5 = document.createElement("TD");
				td5.innerHTML = $$('.ORIGEM_'+j)[0].value;
				tr.appendChild(td5);
				
				var td6 = document.createElement("TD");
				td6.innerHTML = $$('.TIPO_'+j)[0].value;
				tr.appendChild(td6);
				
				var td7 = document.createElement("TD");
				td7.innerHTML = $$('.CATEGORIA_'+j)[0].value;
				tr.appendChild(td7);
				
				var td8 = document.createElement("TD");
				td8.innerHTML = $$('.SUBCATEGORIA_'+j)[0].value;
				tr.appendChild(td8);
				
				var td9 = document.createElement("TD");
				td9.innerHTML = $$('.KEYWORDS_'+j)[0].value;
				tr.appendChild(td9);
				
				var td10 = document.createElement("TD");
				td10.innerHTML = '<input class="objFicha" type="radio" value="' + $$('.THUMB_'+j)[0].value + '" name="OBJ_FICHA[FLAG_MAIN_OBJ_FICHA]" />';
				tr.appendChild(td10);

				var td11 = document.createElement("TD");
				var base_url = new Util().options.base_url;
				td11.innerHTML = '<a href="javascript:new Util().vazio()" onClick="$(this.parentNode.parentNode).remove()"><img src="' + base_url + 'img/excluir.gif" border="0" /></a>';
				tr.appendChild(td11);
					
				$('tabela_resultLista').childNodes[0].appendChild(tr);				 					
			}												
		}
				
		closeMessage();
	}
});