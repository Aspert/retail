var Fabricante = new Class({
	
	/*busca todos os fabricantes*/
	getAll: function(response)
	{				
		new Util().call('fabricante/getAll', null, response);					
	},
		
	/*busca todas as marcas*/
	getAllMarca: function(response, id, combo, value)
	{								
		combo = (combo!=null) ?combo :'ID_MARCA';
		value = (value!=null) ?value :'ID_MARCA';
		
		new Util().call('fabricante/getAllMarca', {'id':id, 'combo': combo, 'value': value}, response, 'post');
	},
	
	/*resposta da marcas */
	responseMarca: function(response)
	{		
		var marcas =  response['data'];
	
		var option = document.createElement('OPTION');
		option.innerHTML = '[Selecione]';
		option.value = '';
		
		$(response['combo']).empty().appendChild(option);
		
		for ( var i = 0; i < marcas.length ; i++ )
		{																  						
	
			var option = document.createElement('OPTION');
			option.value = marcas[i][response['value']];
			option.innerHTML = marcas[i]['DESC_MARCA'];

			$(response['combo']).appendChild(option);			
		}
	}	
					
});