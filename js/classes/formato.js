var Formato = new Class({
	getAllFormato: function(id_veiculo, response, obj)
	{
		obj = (obj!=null) ?obj :'';
		
		new Util().call('veiculo/getAllFormato', {'id_veiculo': id_veiculo, 'obj': obj}, response);
	},
	
	mountDropdown: function(response)
	{
		var formatos = response['formatos'];
		 
		var obj = $(response['obj']).empty();
		var option = document.createElement('OPTION');
		option.value = '';
		option.innerHTML = '[Selecione]';
		obj.appendChild(option);

		if( formatos )
		{
			for ( var i = 0; i < formatos.length; i++ )
			{
				var option = document.createElement('OPTION');
				option.value = formatos[i]['ID_FORMATO'];
				option.innerHTML = formatos[i]['DESC_FORMATO'];
				obj.appendChild(option);
			}
		}
	}
});
