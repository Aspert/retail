(function($){
	$(function () {
        $('.tooltip').each(function () {
            var distance = 30;
            var time = 250;
            var hideDelay = 100;
            var hideDelayTimer = null;
            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger_tooltip', this);
            var info = $('.popup', this).css('opacity', 1);
            $('.popup', this).css('display', 'block');
            $('.popup', this).css('top', '-30px');
            $('.popup', this).css('left', '14px');

            $([trigger.get(0), info.get(0)]).mouseover(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;
                    info.css({
                        top: 0,
                        left: 14,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }
                return false;
            }).mouseout(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });
                }, hideDelay);
                return false;
            });
        });
    });
})(jQuery);
// JavaScript Document