/***************************** Verificação do Browser e Sistema Operacional ******************************/
/*BrowserDetect.browser = Nome do Browser
*BrowserDetect.version = Versão do Browser
*BrowserDetect.OS = Sistema Operacional
*/

var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "Browser desconhecido";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "Versão desconhecida";
		this.OS = this.searchString(this.dataOS) || "Sistema Operacional desconhecido";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{
			string: navigator.userAgent,
			subString: "Chrome",
			identity: "Chrome"
		},
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari",
			versionSearch: "Version"
		},
		{
			prop: window.opera,
			identity: "Opera"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Internet_Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			   string: navigator.userAgent,
			   subString: "iPhone",
			   identity: "iPhone/iPod"
	    },
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]

};
BrowserDetect.init();

/************************************************* Flash Player Versão ********************************/

function getFlashVersion(){ 
	// Internet Explorer
	try { 
		try { 
			var axo = new ActiveXObject('ShockwaveFlash.ShockwaveFlash.6'); 
			try { axo.AllowScriptAccess = 'always'; } 
				catch(e) { return '6,0,0'; } 
		} catch(e) {} 
			return new ActiveXObject('ShockwaveFlash.ShockwaveFlash').GetVariable('$version').replace(/\D+/g, ',').match(/^,?(.+),?$/)[1]; 
	// Outros Browsers 
	} catch(e) { 
		try { 
			if(navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin){ 
				return (navigator.plugins["Shockwave Flash 2.0"] || navigator.plugins["Shockwave Flash"]).description.replace(/\D+/g, ",").match(/^,?(.+),?$/)[1]; 
		} 
		} catch(e) {} 
	} 
	return '0,0,0'; 
} 

var flashVersion = getFlashVersion().split(',').shift(); 
if(flashVersion >= 11){ 
	//plugins['flash'] = '11';	 
}

/************************************************** Java Versão ******************************************/
/************************** PRECISA DO SCRIPT deployJava.js PARA VERIFICAÇÃO DO JAVA ********************/

var javaVersion = '';

if( BrowserDetect.browser == 'Safari' && BrowserDetect.version >= 3 && deployJava.versionCheck('1.5.0_16+')){
	javaVersion = '6';
}

if ( deployJava.versionCheck('1.6+') ){ 
	javaVersion = '6';
}




/********************************************************************************************************/

var IE = 8;
if( BrowserDetect.version >= IE){
	IE = BrowserDetect.version;
}

var SAFARI = 5;
if( BrowserDetect.version >= SAFARI){
	SAFARI = BrowserDetect.version;
}

var FIREFOX = 8;
if( BrowserDetect.version >= FIREFOX){
	FIREFOX = BrowserDetect.version;
}

var CHROME = 16;
if( BrowserDetect.version >= CHROME){
	CHROME = BrowserDetect.version;
}

var plugins = {};

plugins.usuario = {
	java: javaVersion,
	flash: flashVersion,
	browser: BrowserDetect.browser,
	browserVersion: BrowserDetect.version,
	os: BrowserDetect.OS
}

plugins.minimo = {
	java: '6',
	flash: '11',
	browser: {
		firefox: 'Firefox',
		safari: 'Safari',
		ie: 'Internet_Explorer',
		chrome: 'Chrome'
	},
	browserVersion: {
		firefox: FIREFOX,
		safari: SAFARI,
		ie: IE,
		chrome: CHROME
	},
	os: {
		linux: 'Linux',		
		mac: 'Mac',
		windows: 'Windows'	
	}
}


