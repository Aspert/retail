function insRow(id,url,tipoficha){
	if(tipoficha == undefined){
		tipoficha = 'PRODUTO';
	}

	var campoValue = $j('#Campo').val();
	var valorValue = $j('#Valor').val();
	var subfichaValue = $j('#Subficha').attr('checked');
	
	var existe = false;
	$j('.campo').each(function(){
		if($j(this).val() == campoValue) {
			existe = true;
		}
	});
	
	if(existe){
		alert("Não é permitido inserir campos repetidos.");
		return;
	}

	if(campoValue != '' && valorValue != ''){
		
		var item = $j(modelo_pv.clone());
		item.find('.campo').val(campoValue);
		item.find('.valor').val(valorValue);
		
		item.find('.lbl_campo').html(campoValue.replace('\n', '<br />'));
		item.find('.textarea_valor').html(valorValue);
		
		if(tipoficha != 'SUBFICHA'){
			if(subfichaValue){
				item.find('.chk_subficha').attr('checked', true);
				item.find('.subficha').val('1');
			}
		}
		else{
			item.find('.rad_subficha').attr('value', campoValue);
		}
		
		$j('#'+id).append(item);
		
		$j('#Campo').val('');
		$j('#Valor').val('');
		$j('#Subficha').attr('checked', false);
		
		if(tipoficha == 'SUBFICHA'){
			$j('.chk_subficha').hide();
			$j('.rad_subficha').show();
		}
		else{
			$j('.chk_subficha').show();
			$j('.rad_subficha').hide();
		}
	}

}

function addLinha(){
	var campo = $j('#Campo').val();
	var valor = $j('#Valor').val();
	
	if(campo != '' && valor != ''){
		var line = modelo_pv.clone();
		line.attr('id','').find('.campoLbl').html(campo);
		line.find('.valorLbl').html(valor);
		
		$j('#tbl').append(line);
	}
}

var Base64 = {
 
	// private property
	_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
 
	// public method for encoding
	encode : function (input) {
		var output = "";
		var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		var i = 0;
 
		input = Base64._utf8_encode(input);
 
		while (i < input.length) {
 
			chr1 = input.charCodeAt(i++);
			chr2 = input.charCodeAt(i++);
			chr3 = input.charCodeAt(i++);
 
			enc1 = chr1 >> 2;
			enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			enc4 = chr3 & 63;
 
			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			} else if (isNaN(chr3)) {
				enc4 = 64;
			}
 
			output = output +
			this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
			this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
 
		}
 
		return output;
	},
 
	// public method for decoding
	decode : function (input) {
		var output = "";
		var chr1, chr2, chr3;
		var enc1, enc2, enc3, enc4;
		var i = 0;
 
		input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
 
		while (i < input.length) {
 
			enc1 = this._keyStr.indexOf(input.charAt(i++));
			enc2 = this._keyStr.indexOf(input.charAt(i++));
			enc3 = this._keyStr.indexOf(input.charAt(i++));
			enc4 = this._keyStr.indexOf(input.charAt(i++));
 
			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;
 
			output = output + String.fromCharCode(chr1);
 
			if (enc3 != 64) {
				output = output + String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				output = output + String.fromCharCode(chr3);
			}
 
		}
 
		output = Base64._utf8_decode(output);
 
		return output;
 
	},
 
	// private method for UTF-8 encoding
	_utf8_encode : function (string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";
 
		for (var n = 0; n < string.length; n++) {
 
			var c = string.charCodeAt(n);
 
			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}
 
		}
 
		return utftext;
	},
 
	// private method for UTF-8 decoding
	_utf8_decode : function (utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;
 
		while ( i < utftext.length ) {
 
			c = utftext.charCodeAt(i);
 
			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i+1);
				c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}
 
		}
 
		return string;
	}
 
}

function removeLinha(obj){
	$j(obj).closest('tr').remove();
}

function deleteCurrentRow(obj)
{
	var delRow = obj.parentNode.parentNode;
	var tbl = delRow.parentNode.parentNode;
	var rIndex = delRow.sectionRowIndex;
	var rowArray = new Array(delRow);
	deleteRows(rowArray);
}
function deleteRows(rowObjArray)
{
	for (var i=0; i<rowObjArray.length; i++) {
		var rIndex = rowObjArray[i].sectionRowIndex;
		rowObjArray[i].parentNode.deleteRow(rIndex);
	}

}
function rmCampoObj(url) {
	http_request = false;
	if (window.XMLHttpRequest) { // Mozilla, Safari,...
			http_request = new XMLHttpRequest();
			if (http_request.overrideMimeType) {
					http_request.overrideMimeType('text/html');
					// See note below about this line
			}
	} else if (window.ActiveXObject) { // IE
			try {
					http_request = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
					try {
							http_request = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (e) {}
			}
	}

	if (!http_request) {
			alert('Giving up :( Cannot create an XMLHTTP instance');
			return false;
	}

	http_request.open('GET', url, true);

	http_request.onreadystatechange =
		function (){
			if (http_request.readyState == 4) {
				if (http_request.status == 200) {
					if(http_request.responseText != ''){
						return true;
					}
				} else {
					alert('There was a problem with the request.');
				}
			}
		}
	http_request.send(null);
}

function limpaForm(id){
	$j(id + " input:text").val("");
	$j(id + " input:password").val("");
	$j(id + " input:radio").attr("checked",false);
	$j(id + " input:checkbox").attr("checked",false);
	$j(id + " input:file").val("");
	$j(id + " select option:first-child").attr("selected","selected");
	$j(id + " select").change();
	$j(id + " textarea").val("");
}

function atribuiCalendarios(selector){
	$j(selector)
		.mask('99/99/9999')
		.datepicker({dateFormat: 'dd/mm/yy',showOn: 'button', buttonImage: new Util().options.theme + 'img/calendario.png', buttonImageOnly: true});
}


function formatCurrency(num) {
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num))
	num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
	cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
	num = num.substring(0,num.length-(4*i+3))+'.'+
	num.substring(num.length-(4*i+3));
	return (((sign)?'':'-') + num + ',' + cents);
}


function mascarasFormato(selector){
	$j(selector).maskMoney({
		showSymbol: false,
		thousands: '',
		decimal: ','
	});
}




























